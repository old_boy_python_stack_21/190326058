# 函数

###### 1. 通过代码实现如下转换:

```python
二进制转换成十进制: v  = "0b1111011"
print(int("0b1111011",2))
```

```python
十进制转换成二进制: v = 18
print(bin(18))
print(bin(int("18",10)))
```

```python
八进制转换成十进制: v="011"
print(int("011",8))
```

```python
十进制转换成八进制: v =30
print(oct(int("30",10)))
print(oct(v))
```

```python
十六进制转换成十进制: v="0x12"
print(int("0x12",16))
```

```python
十进制转换成十六进制: v=87
print(hex(int("87",10)))
```

###### 2. python递归的最大层数

```python
1000,可以通过sys.setrecursionlimit()进行设置
```

###### 3.列举常见的内置函数

```python
getattr()
setattr()
filter()
map()
len()
range()  ...

```

###### 4. filter,map,reduce的作用

```python
req = filter(lambda x:x>10,[1,2,3,3,6,666,9,8888])
print(list(req))
```

```python
req = map(lambda x : x*2, [1,22,34,54,677])
print(list(req))
```

```python
from functools import reduce
req = reduce(lambda x,y:x*y,[1,2,3,4,5])
print(list(req))

```

###### 5. 一行代码实现9*9乘法表

```python
print('\n'.join(['\t'.join(["%2s*%2s=%2s"%(j,i,i*j) for j in range(1,i+1)]) for i in range(1,10)]))
```

###### 6. 什么是闭包

```python
外层函数将值传递给内层函数保存而不执行的函数
```

###### 7.简述生成器,迭代器,装饰器以及应用场景

### 待完成

```python
生成器,生成器函数执行得到的对象,生成器执行 next函数取值   ,yield from  可以调用另一个生成器,得到另一个生成器的yield值
```

```python
迭代器: 一个一个的取值,拥有__iter__方法 和__next__方法 ,可迭代对象执行__iter__方法得到一个迭代器
```

```python
装饰器: 带有将函数当做从参数传递给内层函数,且返回内层函数
```

###### 8.使用生成器编写fib函数，函数声明为fib（max）输入一个参数max值，使得该函数可以这样调用。

```python
def fib(max):
    
```

###### 9.一行代码通过filter和lambda函数输出alist=[1,22,2,33,23,32]中索引为奇数的值

```python
a = [1,22,2,33,23,32]
req =[i[1] for i in filter(lambda x: divmod(x[0],2)[1] != 0 ,[(a.index(i),i) for i in a])]
```

###### 10.编写一个函数实现十进制转62进制，分别用0-9A-Za-z,表示62位字母

```python
s=['0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z', 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z']
```

###### 11.写一个装饰器,限制该函数被调用的频率,如10秒1次

```python
def naw(func):
    def inner(*args,**kwargs):
        a = time.times()
        while time.times() - a > 10:
            func(*args,**kwargs)
        return
    return inner
```

###### 12.实现一个装饰器，通过一次调用，使函数重复执行5次

```python
def naw(func):
    def inner(*args,**kwargs):
        for i in range(5):
            func(*args,**kwargs)
        return
    return
```

###### 13 .python一行print 1-100 偶数列表

```python
print([i for i in range(2,101,2)])
```

###### 14. 解释生成器与函数的不同,并实现和简单实用generator

```python
函数执行完的到一个return 的对象,  生成器函数执行完的到生成器,.next方法得到yield的值
```

###### 15. 列表推导式[i for i in range(10)]和生成式表达式(i for i in range(10))的区别

```python
列表推导式的结果是一个列表。
生成器表达式的结果是一个生成器，它和列表推导式类似，它一次处理一个对象，而不是一口气处理和构造整个数据结构，可以节约内存。
```

###### 16. map(str,[1,2,3,4,5,6,7,8,9])结果

```python
字符串列表
```

###### 17. python中定义函数时如何书写可变参数和关键字参数

```python
*args   **kwargs
```

###### 18. python3.5 中enumerate 意思:

```python
枚举,用法
#enumerate(sequence, [start=0])
for index,item in enumerate(lis):   #index为索引,item为值
    print(index,item)
    
```

###### 19.说说python中的装饰器,迭代器的用法:描述下dict的item方法与iteritems方法的不同

```python
1.装饰器:
    1. 装饰器本身就是一个函数
    2.在不改变其他函数源代码以及原调用方式下, 提供额外的功能.
场景:
    它经常用于有切面需求的场景，比如：插入日志、性能测试、事务处理、缓存、权限校验等场景。

2.迭代器:
Python中如果一个对象有__iter__( )方法和__next__( )方法，这个对象是迭代器.

py3  item 返回一个列表包元祖 
py2  iteritems  返回字典的迭代器
```

###### 20. 是否使用过functools 中的函数? 其作用是什么?

```python
reduce()
接收两个参数,
```

###### 21如何判断一个值是函数还是方法

```python
from types import MethodType, FunctionType
isinstance(funcname,MethodType)   判断是不是函数
isinstance(funcname,FunctionType)     判断是不是方法
```



###### 22,编写一个函数实现将IP地址转换成一个整数

```python
a = '10.3.9.12'
lis = a.split('.')
lir = []
for i in lis:
    s = bin(int(i))[2:]
    if len(s) < 8:
        s = (8 - len(s))*'0' + s
    lir.append(s)
result = ' '.join(lir)
print(result)
```

###### 23. lambda 表达式的格式以及应用场景

```python
filter,reduce,map函数内当参数,   lambda x : x+1
```

###### 24. pass的作用

```python
占位符,啥也不执行
```

###### 25. *args,**kwargs的作用

```python
位置参数,关键字参数
```

###### 26. 如何在函数中设置一个全局变量

```python
glabal 全局变量
```

###### 27. 写出打印结果

```python
例1:
def func(a, b=[]):
    b.append(a)      # 对于默认参数来说,[]只定义了一次.
    print(b)

func(1)        # [1]
func(1)        # [1, 1]
func(1)        # [1, 1, 1]
func(1)        # [1, 1, 1, 1]
例2:
def func(a, b={}):
    b[a] = 'v'
    print(b)    
func(1)        # {1: 'v'}
func(2)        # {1: 'v', 2: 'v'}
可变数据类型：value值改变, id值不变
```

###### 28.求结果 :lambda

```python
def num():
    return [lambda x:i*x for i in range(4)]
print([m(2) for m in num()])
```

###### 29.简述yield 和 yield from 关键字

```python
yield:
	yield用于生成器函数中替代 return 返回结果。迭代一次遇到yield时就返回yield后面的值。下一次迭代时，从上一次迭代遇到的yield后面的代码开始执行。
	
	一个带有 yield 的函数就是一个生成器，它和普通函数不同，生成一个 生成器看起来像函数调用，但不会执行任何函数代码，直到对其调用 next()（在 for 循环中会自动调用 next()）才开始执行。虽然执行流程仍按函数的流程执行，但每执行到一个 yield 语句就会中断，并返回一个迭代值，下次执行时从 yield 的下一个语句继续执行。看起来就好像一个函数在正常执行的过程中被 yield 中断了数次，每次中断都会通过 yield 返回当前的迭代值。

当函数执行结束时，generator 自动抛出 StopIteration 异常，表示迭代完成。在 for 循环里，无需处理 StopIteration 异常，循环会正常结束。

```

###### 30.有processFunc变量, 初始化为 processFunc=collapse and (lambda s:"".join(s.split())) or (lambda s:s)

```PYTHON
1.
collapse = True
processFunc = collapse and (lambda s: " ".join(s.split())) or (lambda s: s)
print processFunc("i\tam\ntest\tobject !")
答案:i am test object !

2.
collapse = False
processFunc = collapse and (lambda s: " ".join(s.split())) or (lambda s: s)
print processFunc("i\tam\ntest\tobject !")
答案:
i	am
test	object !

解答:
\t = tab键 = 4个空格    \n = 回车换行
1.当collapse=True时, processFunc=lambda s: " ".join(s.split()) or (lambda s: s)
processFunc=lambda s: " ".join(s.split())  # i am test object !

2.当collapse=False, processFunc=lambda s:s
```

###### 31. 请给出下面代码的输出结果

```python
 a = 1
 print(id(a))
 def func(a):
     a = 2
     print(id(a))         #局部变量和全局变量的地址不相同
 func(a)
 print(a)                 # 1 对于a变量 在函数中定义了a=2 在函数执行完之后就销毁了 此时打印的a以旧是全局变量的a
 a = []
 def func(a):
     a.append(1)
 func(a)
 print(a)                    #[1]   对于列表a 将a传入了函数中为a增加了一个数值 a的地址不变
#可变数据类型与不可变数据类型 
```

###### 32.全局变量和局部变量的区别 如何给function里面的一个全局变量赋值

```python
作用域不同   global
```

###### 33. 什么是lambda函数,下面这段代码的输出是什么

```python
 num = range(2,20)
 for i in num:
     num =filter(lambda x:x==i or x%i ,num)
 print(list(num))

#[2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19]
```

###### 34 指出下面程序存在的问题

```python
a = 'adsad dada \dasdas'
 def func(s,i):
 请返回传入src使用空格或者\切分的倒数第i个字符串
     return s.split("\")[-index]    #斜杠转义 并且空格没有切分
 def func(s,m):
     f = s.split('\\')
     l = []
     print(f)
     for i in f:
         print(i)
         i = i.split()
         l = l+i
     return l[-m]
 print(func(a,1))
```

###### 35.有一个数组[3,4,1,2,5,6,6,5,4,3,3]请写一个函数找出该数组中没有重复的数的总和 上面数据的没有重复的总和为1+2=3

```python
a = set(a)
sum(a)
```

###### 36. 求打印结果

```python
 arr =[1,2,3]
 def bar():
     arr+=5
 bar()
 print(arr)
 未传参 报错  
```

## 37. 请写一个函数,计算出如下几个字母代表的数字

```python

```

###### 38. 请给出下面代码的输出

```python
 def say_hi(func):
     def wrapper(*args,**kwargs):
         print('HI')
         ret = func(*args,**kwargs)
         print('BYE')
         return ret
     return wrapper

 def say_yo(func):
     def wrapper(*args,**kwargs):
         print('YO')
         return func(*args,**kwargs)
     return wrapper

 @say_hi
 @say_yo
 def func():
     print('rock&roll')
 func()
# HI
YO
rock&roll
BYE
```

###### 39. 简述标准库中functools.wraps的作用

```python
使装饰器装饰的函数保存原有的信息(名称)
```

###### 40. 请给出下面代码片段的输出

```python
 def test():
     try:
         raise ValueError('something wrong')  #制造一个错误
     except ValueError as e:   #出现错误执行这个代码
         print('Error occurred')
        return
     finally:   #无论出不出错最后都执行这个代码
         print('Done')
test()
#
Error occurred
Done
```

###### 41. 下面函数,那些会输出1,2,3 三个数字

```python
for i in range(3):
     print(1)     #错
 alist = [0,1,2]
 for i in alist:
     print(i+1)    # 对
 i = 1
 while i<3:
     print(i)   #错
     i+=1
 for i in range(3):
     print(i+1)    #对
```

###### 42. 以下函数需要在其中引用一个全局变量k,请填写语句

```python
def fun():
    global k
    k = k + 1
```

###### 43. 请把以下函数转化为python  lambda  匿名函数

```python
def add(x,y):
    return x+y
p = lambda x,y:x+y
```

###### 44. 阅读以下代码,并写出程序的输出结果

```python
my_dict ={'a':0,'b':1}
def func(d):
     d['a'] = 1
     return d
 func(my_dict)
 my_dict['c'] = 2
 print(my_dict)
 #{'a': 1, 'b': 1, 'c': 2}
```

###### 45. 填空题

```python
#有函数定义如下
def calc(a,b,c,d=1,e=2):
    return (a+b)*(c-d)+e
#请分别写出以下标号代码的输出结果 如果出错请写出Error
print(calc(1,2,3,4,5))     #(1+2)*(3-4)+2   2
print(calc(1,2,3))         #8
# print(calc(1,2))#出错
print(calc(1,2,3,e=4))    #10
print(calc(e=4,c=5,a=2,b=3))  #    (2+3)*(5-1)+4         24
# print(calc(1,2,3,d=5,4))#出错
```

###### 46. def (a,b=[])这种写法有什么陷阱

```python
因为列表是可变类型,如果不传关键字参数b永远指向这个列表,如果往里面加入值,则,以后如在调用函数不传关键字,b就不再是空列表了
```

###### 47. 函数

```python
def add_end(l=[]):
    l.append("end")
    return l

add_end() # 什么都不输出
add_end() # 什么都不输出
```

###### 48. 函数参数 *args  **kwargs 的作用是什么

```python
位置参数,关键字参数
```

###### 49. 可变参数定义 *args,**kwargs 的区别是什么?并写出下面代码的输出内容

```python
def foo(*args, **kwargs):
    print("args=", args)
    print("kwargs=", kwargs)
    print("-----------------------")


if __name__ == '__main__':
    foo(1, 2, 3, 4)     # args=(1, 2, 3, 4)  kwargs={}
    foo(a=1, b=2, c=3)    # args=()  kwargs={a:1,b:2,c:3}
    foo(1, 2, 3, 4, a=1, b=2, c=3)    # args=(1, 2, 3, 4)  kwargs={a:1,b:2,c:3}
    foo("a", 1, None, a=1, b="2", c=3)    # args=("a", 1, None)  kwargs={a:1,b:"2",c:3}
```

###### 50. 请写出log实现(主要功能时打印函数名)

```python
from functools import wraps
def log(func):
    @wraps(func)
    def inner(*args,**kwargs):
        print(func.__name__)
        return func(*args,**kwargs)
    return inner

@log
def fuac():
    print('666')

fuac()     
```

###### 51. python 如何定义一个函数

```python
A. class <name>(<Type> arg1, <Type> arg2, ...)
B. function <name>(arg1, arg2, ...)
C. def <name>(arg1, arg2, ...)
D. def <name>(<Type> arg1, <Type> arg2, ...)

答案：C
```

###### 52. 选择代码运行结果

```python
country_counter = {}
def addone(country):
    if country in country_counter:
        country_counter[country] += 1
    else:
        country_counter[country] = 1

addone("China")
addone("Japan")
addone("china")
print(len(country_counter))
# 3 
```

###### 53.选择输出结果

```python
def doff(arg1,*args):
    print(type(args))
    
doff('applea','bababas','cherry')

tuple
```

###### 54. 下面程序的输出结果是

```python
d = lambda p: p * 2
t = lambda p: p * 3

x = 2
x = d(x)
x = t(x)
x = d(x)
print(x)

答案：24
```

###### 55. 什么是lambda表达式

```python
匿名函数 不需要函数名，不能写复杂的逻辑的函数
```

###### 56. 以下代码输出是什么,请给出答案并解释

```python
def multipliers():
    return [lambda x: x*i for i in range(4)]

print([m(2) for m in multipliers()])
[6, 6, 6, 6]
```

###### 57.有0<x<=10,10<x<=20...190<x<=200,200<x这样的21个区间分别对应1-21二十一个级别，请编写一个函数level（x）根据输入数值返回对应级别

```python
def level(x):
    return f'{(x-1)*10}<x<{x*10}'
```

###### 58.写函数。有一个数据结构如下所示，请编写一个函数从该结构数据中返回由指定的字母和对应的值组成的字典。如果指定字段不存在，则跳过该字段

```python
data:{"time":"2016-08-05T13:13:05",
"some_id":"ID1234",
"grp1":{"ﬂd1":1,"ﬂd2":2},
"xxx2":{"ﬂd3":0,
    "test":{‘ﬂd5’:0.4}},"ﬂd6":11,"ﬂd7":7,"ﬂd46":8 } 
    
ﬁelds:由"|"连接的以"ﬂd"开头的字符串,如:ﬂd2|ﬂd3|ﬂd7|ﬂd19   
def select(data,ﬁelds):                
    return result

def func(dic, lis,fld):
    for key, val in dic.items():
        if type(val) is dict:
            print('kkkkk')
            func(val, lis,fld)
        elif type(val) == str:
            if len(val) >= 4:
                if val[:3] == fld:
                    lis.append(val)
                    print(lis)
        print(key[:2])
        if key[:2] == fld:
            print('***')
            lis.append(key)
            print(lis)
    a = '|'.join(lis)
    return a
ret = func(data, lis,'ﬂd')
print(ret)
```

