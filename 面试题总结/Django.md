### Django

###### 1.简述http协议及常用请求头

```python
http协议：超文本传输协议，是基于tcp协议之上的应用层协议
http协议基于响应、请求的模式，是无状态保存、无连接以及媒体独立的
http协议包含了请求协议（请求行、请求头、请求体）、响应协议（状态行、响应头、响应体）等
```

###### 2.列举常用的请求方法

```python
http请求中的8种请求方法
1、opions   返回服务器针对特定资源所支持的HTML请求方法   或web服务器发送*测试服务器功能（允许客户端查看服务器性能）

2、Get   向特定资源发出请求（请求指定页面信息，并返回实体主体）

3、Post   向指定资源提交数据进行处理请求（提交表单、上传文件），又可能导致新的资源的建立或原有资源的修改

4、Put   向指定资源位置上上传其最新内容（从客户端向服务器传送的数据取代指定文档的内容）

5、Head  与服务器索与get请求一致的相应，响应体不会返回，获取包含在小消息头中的原信息（与get请求类似，返回的响应中没有具体内容，用于获取报头）

6、Delete   请求服务器删除request-URL所标示的资源*（请求服务器删除页面）

7、Trace   回复服务器收到的请求，用于测试和诊断

8、Connect   HTTP/1.1协议中能够将连接改为管道方式的代理服务器
```

###### 3.列举常见的状态码

```python
200 OK：客户端请求成功。
400 Bad Request：客户端请求有语法错误，不能被服务器所理解。
403 Forbidden：服务器收到请求，但是拒绝提供服务。
404 Not Found：请求资源不存在，举个例子：输入了错误的URL。
500 Internal Server Error：服务器发生不可预期的错误。
503 Server Unavailable：服务器当前不能处理客户端的请求，一段时间后可能恢复正常，举个例子：HTTP/1.1 200 OK（CRLF）。
```

###### 4.http和https的区别

```python
　　1、https协议需要到ca申请证书，一般免费证书较少，因而需要一定费用。

　　2、http是超文本传输协议，信息是明文传输，https则是具有安全性的ssl加密传输协议。
	3、http和https使用的是完全不同的连接方式，用的端口也不一样，前者是80，后者是443。

　　4、http的连接很简单，是无状态的；HTTPS协议是由SSL+HTTP协议构建的可进行加密传输、身份认证的网络协议，比http协议安全。
```

###### 5.简述websocket协议及实现原理

```python
 WebSocket是HTML5下一种新的协议。它实现了浏览器与服务器全双工通信，能更好的节省服务器资源和带宽并达到实时通讯的目的。
   它与HTTP一样通过已建立的TCP连接来传输数据，但是它和HTTP最大不同是：WebSocket是一种双向通信协议。在建立连接后，WebSocket服务器端和客户端都能主动向对方发送或接收数据，就像Socket一样；WebSocket需要像TCP一样，先建立连接，连接成功后才能相互通信。
   相对于传统HTTP每次请求-应答都需要客户端与服务端建立连接的模式，WebSocket是类似Socket的TCP长连接通讯模式。一旦WebSocket连接建立后，后续数据都以帧序列的形式传输。在客户端断开WebSocket连接或Server端中断连接前，不需要客户端和服务端重新发起连接请求。在海量并发及客户端与服务器交互负载流量大的情况下，极大的节省了网络带宽资源的消耗，有明显的性能优势，且客户端发送和接受消息是在同一个持久连接上发起，实时性优势明显。

```

###### 6.django中如何实现websocket

```python
django实现websocket大致上有两种方式，一种channels，一种是dwebsocket。channels依赖于redis，twisted等，相比之下使用dwebsocket要更为方便一些

pip3 install dwebsocket

dwebsocket配置
#复制代码
INSTALLED_APPS = [
    .....
    .....
    'dwebsocket',
]
 
MIDDLEWARE_CLASSES = [
    ......
    ......
    'dwebsocket.middleware.WebSocketMiddleware'  # 为所有的URL提供websocket，如果只是单独的视图需要可以不选
]
WEBSOCKET_ACCEPT_ALL=True   # 可以允许每一个单独的视图实用websockets
```

```python
#dwebsocket有两种装饰器：require_websocket和accept_websocekt，使用require_websocket装饰器会导致视图函数无法接收导致正常的http请求，一般情况使用accept_websocket方式就可以了，
# 
# dwebsocket的一些内置方法：
# 
# request.is_websocket（）：判断请求是否是websocket方式，是返回true，否则返回false
# request.websocket： 当请求为websocket的时候，会在request中增加一个websocket属性，
# WebSocket.wait（） 返回客户端发送的一条消息，没有收到消息则会导致阻塞
# WebSocket.read（） 和wait一样可以接受返回的消息，只是这种是非阻塞的，没有消息返回None
# WebSocket.count_messages（）返回消息的数量
# WebSocket.has_messages（）返回是否有新的消息过来
# WebSocket.send（message）像客户端发送消息，message为byte类型

https://www.cnblogs.com/sui776265233/p/10176275.html
```

###### 6.1 什么是websocket

```python
WebSocket与HTTP的关系
相同点
1. 都是一样基于TCP的，都是可靠性传输协议。
2. 都是应用层协议。
不同点
1. WebSocket是双向通信协议，模拟Socket协议，可以双向发送或接受信息。HTTP是单向的。
2. WebSocket是需要握手进行建立连接的。
联系
WebSocket在建立握手时，数据是通过HTTP传输的。但是建立之后，在真正传输时候是不需要HTTP协议的。

WebSocket与Socket的关系
Socket其实并不是一个协议，而是为了方便使用TCP或UDP而抽象出来的一层，是位于应用层和传输控制层之间的一组接口。

Socket是应用层与TCP/IP协议族通信的中间软件抽象层，它是一组接口。在设计模式中，Socket其实就是一个门面模式，它把复杂的TCP/IP协议族隐藏在Socket接口后面，对用户来说，一组简单的接口就是全部，让Socket去组织数据，以符合指定的协议。

当两台主机通信时，必须通过Socket连接，Socket则利用TCP/IP协议建立TCP连接。TCP连接则更依靠于底层的IP协议，IP协议的连接则依赖于链路层等更低层次。

WebSocket则是一个典型的应用层协议。
区别
Socket是传输控制层协议，WebSocket是应用层协议。

https://www.cnblogs.com/Javi/p/9303020.html
```



### 7.python web开发中，跨域问题的解决思路是

```python
https://www.cnblogs.com/yzxing/p/9440191.html
```

### 8.请简述http缓存机制

```python
https://www.cnblogs.com/chenqf/p/6386163.html
```

###### 9.谈谈你所知道的python web框架

```python
Django,flask,synic
```

###### 10.http和https的区别

```python
第四题
```

###### 11.django、flask、tornado框架的比较

```python
Django：Python 界最全能的 web 开发框架，battery-include 各种功能完备，可维护性和开发速度一级棒。常有人说 Django 慢，其实主要慢在 Django ORM 与数据库的交互上，所以是否选用 Django，取决于项目对数据库交互的要求以及各种优化。而对于 Django 的同步特性导致吞吐量小的问题，其实可以通过 Celery 等解决，倒不是一个根本问题。Django 的项目代表：Instagram，Guardian。

Tornado：天生异步，性能强悍是 Tornado 的名片，然而 Tornado 相比 Django 是较为原始的框架，诸多内容需要自己去处理。当然，随着项目越来越大，框架能够提供的功能占比越来越小，更多的内容需要团队自己去实现，而大项目往往需要性能的保证，这时候 Tornado 就是比较好的选择。Tornado项目代表：知乎。

Flask：轻量级框架,第三方组件齐全,内置组件没有,但是第三方组件可能会随着版本跟新不支持 Flask 可以自由选择自己的数据库交互组件（通常是 Flask-SQLAlchemy），而且加上 celery +redis 等异步特性以后
```

###### 12.什么是wsgi

```python
WSGI：Web Server Gateway Interface，wsgi是说在做Web应用的时候，需要去处理HTTP请求、响应。但是我们不会自己去实现这些底层的东西，而希望专注于业务代码的撰写。因此，需要一个统一的接口，它来帮我们处理http相关的协议，这个接口就是wsgi。
```

###### 13.列举django的内置组件

```python
form组件
django-res-framework 
session
```

###### 14.简述django下的(内建的)缓存机制

```python
缓存是将一些常用的数据保存内存或者memcache中,在一定的时间内有人来访问这些数据时,则不再去执行数据库及渲染等操作,而是直接从内存或memcache的缓存中去取得数据,然后返回给用户.django提供了6种内建缓存机制，分别为：
开发调试缓存（为开发调试使用，实际上不使用任何操作）；

内存缓存（将缓存内容缓存到内存中）；

文件缓存（将缓存内容写到文件 ）；

数据库缓存（将缓存内容存到数据库）；

memcache缓存（包含两种模块，python-memcached或pylibmc.）。

以上缓存均提供了三种粒度的应用。
```

###### 15.django中的model的SlugField类型字段有什么用途

```python
slug是一个新闻行业的术语。一个slug就是一个某种东西的简短标签，包含字母、数字、下划线或者连接线，通常用于URLs中。可以设置max_length参数，默认为50。
https://stackoverflow.com/questions/427102/what-is-a-slug-in-django
```

###### 16.django中想要验证表单提交是否格式正确需要用到form中的哪个方法

```python
✔ D.form.is_valid()
```

###### 17.django中常见的线上部署方式有哪几种

```python
我们学过的nginx + uwsgi +Django： https://blog.csdn.net/wl21787/article/details/80066616
    dockor部署
```

###### 18.django对数据查询结果排序怎么做，降序怎么做

```python
Query_set.order_by("-age")
```

###### 19.下面关于http协议中的get和post方式区别，哪些是正确的？

```python
  A.他们都可以被收藏以及缓存
✔ B.get请求参数放在url中
  C.get只用于查询请求，不能用于数据请求
✔ D.get不应该处理敏感数据请求
```

###### 20.django中使用memcashed作为缓存的具体方法？优缺点说明？

```python
https://blog.csdn.net/zl834205311/article/details/51217760
    
Memcached 进程运行之后，会预申请一块较大的内存空间，自己进行管理，用完之后再申请，不是每次需要的时候去向操作系统申请。Memcached将对象保存在一个巨大的Hash表中，它还使用NewHash算法来管理Hash表，从而获得进一步的性能提升。所以当分配给Memcached的内存足够大的时候，Memcached的时间消耗基本上只是网络Socket连接了
       下面来说说Memcached的不足：
1、数据是保存在内存当中的，一旦服务进程重启，数据会全部丢失
对策：可以采取更改Memcached的源代码，增加定期写入硬盘的功能
2、Memcached以root权限运行，而且Memcached本身没有任何权限管理和认证功能，安 全性不足
对策：可以将Memcached服务绑定在内网IP上，通过防火墙进行防护
下面简单介绍下memcache安装，关键步骤如下：
```

###### 21.django的orm中如何查询id不等于5的元素

```python
xxx.exclude(id=5)
```

###### 22.使用django中model filter条件过滤方法，把下面的sql语句转换为python代码

```python
1.select * from company where title like "%adc%" or mecount>999;
-- model.Company.filter(Q(title__contains='adc')|Q(mecount__gt=999))
2.order by createtime desc;  asc
-- xxx.order_by('-createtime')
```

###### 23.从输入http://www.baidu.com/到页面返回，中间都发生了什么？

```python
将域名解析成IP
浏览器发送请求到建立TCP连接
走wsgi--中间件--路由--视图--中间件--wsgi,浏览器
```

###### 24.django请求的生命周期

```python
首先，浏览器发来的http请求经过Django中的wsgi被解析生成request对象,再经过Django的中间件,之后根据url对应路由映射表,在路由中一条一条进行匹配,一旦其中一条匹配成功就执行对应的视图函数,后面的路由就不再继续匹配了。
视图函数根据客户端的请求查询相应的数据.返回给Django,然后Django把客户端想要的数据（页面）做为一个Httpresponse对象返回给客户端.
```

###### 25.django中如何在model保存前做一定的固定操作，比如写一句日志？

```python
信号:
在定义model类的时候定义django.db.models.signals.pre_save方法，具体见下方博客：
https://blog.csdn.net/qq_37049050/article/details/81746046
```

###### 26.简述django中间件机及应用场景

```python
1、process_request(self, request) : 请求进来时,权限认证 。
    
2、process_view(self, request, view_func, view_args, view_kwargs) : 路由匹配之后,能够得到视图函数
3、process_exception(self, request, exception) : 异常时执行
4、process_template_response(self, request, response) : 模板渲染时执行
5、process_response(self, request, response) : 请求有响应时执行
```

###### 27.简述django中FBV和CBV

```python
python视图中定义视图的两种方式，FBV代表函数视图，CBV代表的是类视图。
在FBV中，函数名要与路由中的视图名对应，第一个参数是request对象，通过request对象的method属性判断请求方法。
在CBV中，类名与路由中的视图名对应，类继承views.View的类，视图通过as_view方法调用类中的dispatch方法（继承自View类，也可以自己自定义）进行路由分发，类中的方法名按照http请求方法定义，如get方法就在类中定义get的方法。
```

###### 28.如何给django CBV 的函数设置添加装饰器

```python
# @method_decorator(cookie,name='dispatch')    # dispatch的便捷写法
class CBVtest(views.View):
    @method_decorator(cookie)   # 给dispatch方法添加装饰器，那么下面所有的get，post都会添加
    def dispatch(self, request, *args, **kwargs):
        return super(CBVtest,self).dispatch(request,*args,**kwargs)
    # @method_decorator(cookie)  # 单独添加
    def get(self,request):
         u = request.get_signed_cookie('username',salt='user',default=None)
         return render(request,'houtai.html',{'user':u})
需要导入django.utils.decorators 中的method_decorator
第一种方式，可以直接在类上添加，@method_decorator,name为dispatch
第二种方式，直接重写dispatch方法，在这个方法上面添加
第三种方式，在单独的函数上添加
```

###### 29.django如何链接多个数据库并进行读写分离

```python
1.在settings文件里面配置多个数据库的配置
	DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': os.path.join(BASE_DIR, 'db.sqlite3'),
    },
    'db2': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': os.path.join(BASE_DIR, 'db2.sqlite3'),
    },
}
2.迁移默认有参数python manage.py migrate --database default ，在默认数据库上创建表。因此完成以上迁移后，执行python manage.py --database db2，再迁移一次，就可以在db2上创建相同的表。
3.读写分离
	1.手动读写分离
	在使用数据库时，通过.using(db_name)来手动指定要使用的数据库
	2.自动读写分离
	  自定义文件
	  class Router:
    def db_for_read(self, model, **hints):
        return 'db2'

    def db_for_write(self, model, **hints):
        return 'default'
	配置Router
	settings.py中指定DATABASE_ROUTERS
	DATABASE_ROUTERS = ['myrouter.Router',]  
```

###### 30.列举django中orm中你了解的所有方法

| all()               | 查询所有结果                                                 |
| :------------------ | ------------------------------------------------------------ |
| filter(**kwargs)    | 它包含了与所给筛选条件相匹配的对象。获取不到返回None         |
| get(**kwargs)       | 返回与所给筛选条件相匹配的对象，返回结果有且只有一个。获取不到会抱胸 #如果符合筛选条件的对象超过一个或者没有都会抛出错误 |
| exclude(**kwargs)   | 它包含了与所给筛选条件不匹配的对象                           |
| order_by(*field)    | 对查询结果排序                                               |
| reverse()           | 对查询结果反向排序，在已经排序好的基础上进行操作             |
| count()             | 返回数据库中匹配查询(QuerySet)的对象数量                     |
| first()             | 返回第一条记录                                               |
| last()              | 返回最后一条记录                                             |
| exists()            | 如果QuerySet包含数据，就返回True，否则返回False              |
| values(*field)      | 返回一个特殊的QuerySet，运行后得到是一个可迭代的字典序列     |
| values_list(*field) | 它与values()非常相似，它返回的是一个元组序列，values返回的是一个字典序列 |
| distinct()          | 从返回结果中剔除重复纪录                                     |

###### 31.django中F的作用

```python
##### F比较两个字段之间的关系及对一个字段的数据进行加减乘除等运算
from django.db.models import F
ret =models.Book.objects.filter(sale__gt=F('kucun')).values()   # 查询销量大于库存的图书
models.Book.objects.all().update(sale=F('sale') * 2)  #更新销量是原来的两倍
```

###### 32.django中Q的作用

```python
#Q可以同时查询多个条件出来
 ~Q表示非的意思 
| 表示或的意思  
&表示and的意思
from django.db.models import F, Q
ret = models.Book.objects.filter(~Q(Q(id__lt=2) | Q(id__gt=4)) & Q(id__gt=3))
print(ret)

执行结果：结果是只有ID为4的对象
<QuerySet [<Book: <Book object: 4 跟egon学喊麦>>]>
```

###### 33.django中如何执行原生sql

```python
2种方式：直接执行自定义Sql
(这种方式完全不依赖model,前两种还是要依赖于model)
from django.db import connection
cursor=connection.cursor()
#插入操作
cursor.execute('insert into app01_publisher(id,name,city) values(6,"激动出版社","甘肃")')
#更新操作      
cursor.execute("update app01_book set title='跟金老板学开车' where title='开车'")
# #删除操作
cursor.execute("delete from app01_book where title='开车'")
# #查询操作
print(cursor.execute('select publish_date from app01_book '))
raw=cursor.fetchone() #返回结果行游标直读向前，读取一条
print(raw)
print(cursor.fetchall())#读取所有
```

### 34.only和defer的区别

```python

```

## 35.selectrelated和prefetchrelated的区别

```python

```

###### 36.django中filter和exclude的区别

```python
filter 获取满足条件的所有对象   QuerySet   对象列表
exclude 获取不满足条件的所有对象  QuerySet   对象列表 
```

###### 37.django中values和value_list的区别

```python

```







