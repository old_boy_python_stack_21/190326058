from django.shortcuts import render,redirect,HttpResponse
from bookapp import models

# Create your views here.
def show_publisher(request):
    all_publishers  = models.Publisher.objects.all().order_by('pk')
    all_bookobj = models.Book.objects.all().order_by('pk')
    return render(request,'hhh.html',{'all_publishers':all_publishers,'all_bookobj':all_bookobj})

def del_publisher(request):
    pk = request.GET.get('id')
    obj_lis = models.Publisher.objects.filter(pk=pk)
    if not obj_lis:
        return HttpResponse('要删除的信息不存在')
    obj = obj_lis[0]
    obj.delete()
    return redirect('/show_publisher/')
    pass
def create_publisher(request):
    flag=''
    pk = request.GET.get('id')
    obj_lis = models.Publisher.objects.filter(pk=pk)
    if not obj_lis:
        return HttpResponse('要编辑的数据不存在')
    obj = obj_lis[0]
    if request.method == 'POST':
        name= request.POST.get('name')
        print(name)
        if models.Publisher.objects.filter(name=name):
            flag = '修改的名字以存在'
        if name==obj.name:
            flag = '没有对名字进行修改'
        if not name:
            flag = '名字不能为空'
        if not flag:
            print(obj.name,name)
            obj.name = name
            obj.save()
            return redirect('/show_publisher/')

    return render(request,'create_publisher.html',{'obj':obj_lis[0],'flag':flag})

def push_publisher(request):
    if request.method == 'POST':
        flag = ''
        new_name= request.POST.get('name')
        if not new_name:
            return HttpResponse('不能为空')
        if models.Publisher.objects.filter(name=new_name):
            return HttpResponse('已存在')
        obj = models.Publisher.objects.create(name=new_name)

        return redirect('/show_publisher/')
    return render(request, 'push_publisher.html')

def aaa(request):
    all_publishers = models.Publisher.objects.all().order_by('pk')
    return render(request,'hhh.html',{'all_publishers':all_publishers})

def add_book(request):
    all_publishers = models.Publisher.objects.all().order_by('pk')
    if request.method=='POST':
        bookname=request.POST.get('bookname')
        book_publisher = request.POST.get('pub_name')
        models.Book.objects.create(title=bookname,pub=models.Publisher.objects.get(name=book_publisher))
        return redirect('/show_publisher/')

    return render(request,'add_book.html',{'all_publishers':all_publishers})
    pass

def create_book(request):
    print(request.GET)
    pk = request.GET.get('id')
    obj = models.Book.objects.get(pk=pk)
    all_publishers = models.Publisher.objects.all().order_by('pk')
    all_book_obj = models.Book.objects.all()
    if request.method=='POST':
        new_bookname = request.POST.get('new_bookname').strip()
        for every_obj in all_book_obj:
            print(obj.pk,every_obj.pk)
            if obj.pk != every_obj.pk:
                if new_bookname == every_obj.title:
                    return HttpResponse('书名以存在')
        pub_name = request.POST.get('publisher_name')
        publisher = models.Publisher.objects.filter(name=pub_name)[0]
        print(new_bookname,pub_name)
        obj.title = new_bookname
        obj.pub_id = publisher.pk
        obj.save()
        return redirect('/show_publisher/')
    return render(request,'create_book.html',{'obj':obj,'all_publishers':all_publishers})


def del_book(request):
    book_id = request.GET.get('id')
    obj = models.Book.objects.filter(pk=book_id)[0]
    obj.delete()
    return redirect('/show_publisher/')


def show_author(request):
    author_obj_lis = models.Author.objects.all()
    return render(request,'show_author.html',{'author_obj_lis':author_obj_lis})

def create_author(request):
    author_id = request.GET.get('id')
    author_obj = models.Author.objects.get(id=author_id)
    book_obj_lis = models.Book.objects.all()
    if request.method == 'POST':
        author_name = request.POST.get('new_author_name')
        print(request.POST)
        book_id_lis = request.POST.getlist('book_name')
        print(book_id_lis)
        author_obj.name = author_name
        author_obj.save()
        author_obj.books.set(book_id_lis)
        return redirect('/show_author/')

    return render(request,'create_author.html',{'author_obj':author_obj,'book_obj_lis':book_obj_lis})

def del_author(request):
    author_id = request.GET.get('id')
    author_obj = models.Author.objects.get(id=author_id)
    author_obj.delete()
    return redirect('/show_author/')

def add_author(request):
    flag = ''
    book_obj_lis = models.Book.objects.all()
    if request.method == 'POST':
        new_author_name = request.POST.get('new_author_name')
        if models.Author.objects.filter(name=new_author_name):
            flag = '作者以存在'
        else:
            book_id_lis = request.POST.getlist('book_list')
            author_obj = models.Author.objects.create(name=new_author_name)
            author_obj.books.set(book_id_lis)
            return redirect('/show_author/')

    return render(request,'add_author.html',{'book_obj_lis':book_obj_lis,'flag':flag})