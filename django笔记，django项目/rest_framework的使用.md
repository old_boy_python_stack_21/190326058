## 1. feedback的url和models设计

## 1. url设计

```python
url(r'^(learning_notes|student_evaluation|student-interview).html', views.feedback_list, name='feedback_list')
```

## 2. 静态页面

- web中的template
- 继续使用模板和继承

## 3. pycharm

```python
compare with         比较俩文件有啥不同
```

## 4. 使用字典取值传参

- 使用字典进行banner和title的分类

- 使用模版进行渲染

## 5. models设计

- 数据库迁移

```python
# repository中的models.py
from django.db import models
class Feedback(models.Model):
  	type_choice = ((0, '未分类'), (1,'学习笔记'), (2, '学员评价'), (3, '入职邀约'),)
  	img = models.ImageField(upload_to='img/feedback/')
    img_type = models.IntegerField(choices=type_choice, default=0)
```

## 2. 滚动尾页请求数据

### 1. 设计url

```python
# urls.py
urlpatterns = [
  	url(r'^ajax_feedback/', views.AjaxFeedback.as_view()),
]
```

### 2. 请求分页使用流程

#### 1. 下载和注册

- djangorestframework

```python
# 一般使用pycharm安装
pip install djangorestframework
```

- 注册

```python
# 注册app
'rest_framwork'
```

#### 2. 使用modelserializer

- **类变量(5)**：queryset、serializer_class、pagination_class、filter_backends和filter_fields

```python
# views.py
from repository import models
from rest_framework import generics
from web.serializer import FeedbackSerializer
from web.pagination import DefaultPagination
from django_filters.rest_framework import DjangoFilterBackend

class AjaxFeedback(generics.ListAPIView):
    queryset = models.Feedback.objects.all()
    # 序列化器
    serializer_class = FeedbackSerializer
    # 分页器
    pagination_class = DefaultPagination
    # 过滤条件，和过滤依据
    filter_backends = [DjangoFilterBackend, ]
    filter_fields = ['img_type']
```

#### 3. 序列化器

```python
# serializer里的__init__.py中
from rest_framework import serializers
from repository import models

class FeedbackSerializer(serializers.ModelSerializer):
  	class Meta:
      	model = models.Feedback
        fields = ['img']
```

#### 4. 分页器

```python
# pagination里的__init__.py中
from rest_framwork.pagination import PageNumberPagination
class DefaultPagination(PageNumberPagination):
  	# 一页的数据
  	page_size = 8
    # 分页查询条件的key
    page_query_param = 'page'
    # 当定义page_size有效
    page_size_query_param = 'size'
    max_page_size = 4
```

### 3. 筛选条件使用流程

#### 1. 下载django-filter

```python
# 一般使用pycharm
pip install django-filter
```

#### 2. 注册app

```python
'django_filters'
'rest_framework'
```

#### 3. models中使用使用

```python
和  django_filters   有关的
from django_filters.rest_framework import DjangoFilterBackend

class AjaxFeedback(serializers.ModelSerializer):
		# 过滤条件，和过滤依据
    filter_backends = [DjangoFilterBackend, ]
    filter_fields = ['img_type']
```

##  3. 滚动触发ajax

- ajax属于异步请求，需要使用**tag**标志，转换成同步

```js
$(function () {
    var img_type = $('.thisli').attr('img_type');
    var page = 1;		// 请求的页码
    var num = 0;		// 当前请求数据的总条数
    var next = 1;		// data中的字段 next是下次请求的url
    var tag = 1;		// 用于标示当前是否有ajax请求

    function get_info() {

        if (next && tag) {
            tag = 0;
            $.ajax({
                url: `/ajax_feedback/?img_type=${img_type}&page=${page}`,
                success: function (data) {
                    // console.log(data);
                    var results = data.results;
                    next = data.next;

                    for (var i in  results) {
                        var row = results[i];
                        var col = $('.column')[num % 4];
                      	// 需要加入的 html 标签
                        var item = `<div class="item">
                                    <div class="animate-box">
                                    <a href="${row.img}"class="image-popup fh5co-board-img">
                                     <img src="${row.img}"alt="Free HTML5 Bootstrap template">
                                    </a>
                                    </div>
                                    </div>`;
                      
                        $(col).append(item);
                        magnifPopup();
                        animateBoxWayPoint();
                        num += 1
                    }

                    page += 1;
                    tag = 1;
                }
            })
        }
    }

    get_info();
    //同学说分批加载
    $(window).scroll(function () {
        //$(document).scrollTop() 滚动条位置距页面顶部的距离；
        //$(document).height() 整个页面的总高度；
        //$(window).height() 当前窗口的高度；
        //判断是否已经滚动到页面底部；
        if ($(document).scrollTop() >= $(document).height() - $(window).height() - 180) {
            get_info()
        }
    });
}());
```

