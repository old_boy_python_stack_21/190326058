# DRF

### 1. 认证

1. 认证组件基本使用

   ```python
   #可继承BaseAuthentication类
   class Authtication(object):
       def authenticate(self,request):
           token = request._request.GET.get('token')
           token_obj = models.User.objects.filter(token=token).first()
           if not token_obj:
               raise exceptions.AuthenticationFailed('用户认证失败')
           return (token_obj.user,token_obj)
       def authenticate_header(self,request):
           #认证失败之后给浏览器返回的响应头
           pass
   ```

   ```PYTHON
   可在setting配置文件写入:
       REST_FRAMEWORK = {
           "DEFAULT_AUTHENTICATION_CLASSES":["api.utils.auth.Authication",]
       }
   #应用到所有类:如果有的类不需要认证(这个类免认证),则在类中写入
   authentication_classes = []
   ```

   匿名用户:

   ```python
   可在setting配置文件写入:
       REST_FRAMEWORK = {
           "DEFAULT_AUTHENTICATION_CLASSES":["api.utils.auth.Authication",],
           "UNAUTHENTICATED_USER":lambda:"匿名用户",
           "UNAUTHENTICATED_USER":None,#匿名,request.user = None
           "UNAUTHENTICATED_TOKEN":None,#匿名,request.auth = None
       }
   ```

   

2. 认证组件源码执行流程

   ```python
   请求进来先执行对应视图类的dispath方法,dispatch方法内部将request进行进一步封装,内部包含
   return Request(
              request,
               parsers=self.get_parsers(),
               authenticators=self.get_authenticators(),
       		#authenticators内部为一个列表[auth() for auth in self.authentication_classes]
               negotiator=self.get_content_negotiator(),
               parser_context=parser_context
           )
   进行校验:
       self.initial(request, *args, **kwargs)
       内部校验执行:self.perform_authentication(request)
       执行到request.user
       执行到:
           with wrap_attributeerrors():
                self._authenticate()
   
      内部self._authenticate()执行for循环request内部封装			      authenticators
               调用authenticate(self)得到一个 user_auth_tuple
   			设置request的_authenticator属性赋值为循环对象
               如果user_auth_tuple(返回值)不为空,将元祖内两个属性分别				封装到request的user 和request的auth内部直return	如果循环结束依旧为空:
             执行:self._not_authenticated()
               内部request的_authenticator属性赋值为None
           ###执行下面判断,如果有就用,user默认为AnonymousUSER,AUTH默认为None
           if api_settings.UNAUTHENTICATED_USER:
               self.user = api_settings.UNAUTHENTICATED_USER()
           else:
               self.user = None
   
           if api_settings.UNAUTHENTICATED_TOKEN:
               self.auth = api_settings.UNAUTHENTICATED_TOKEN()
           else:
               self.auth = None
    #如果抛出异常认证是吧,如果为None 为匿名用户,如果,对应tocken表里的属性为我的用户
   ```

### 2. 权限

###### 1.使用

```python
#可以继承 BasePermission
from rest_framework.authentication import BaseAuthentication
class Mypermission1(object):
    def  has_permission(self,request,view):
        if request.user.user_typpe == 3:
            return False   #没权限
        return True    #有权限

```

```python
给视图函数加入权限:permission_classes = [Mypermission1]
    
```

###### 2. 源码解析

```python
进入dispath方法,封装request及属性,
调用self.initial(request, *args, **kwargs)方法 
执行权限认证方法:self.check_permissions(request)
    内部执行:for循环request封装的get_permissions(self)
        得到自定义的权限类对象
    执行权限类对象中的has_permission方法,
    如果为True,通过权限结束
    如果不为True,抛出异常,权限认证失败
```

### 3. 控制用户访问频率

###### 1. 使用:

```python
from rest_framework.throttling import BaseThrottle
#可以继承BaseThrottle
import time
VISIT_RECORD = {}

class VisitThrottle(object):
    def allow_request(self,request,view):
        #取出用户的IP  设置1分钟内不超过3次
        remote_addr = request.META.get('REMOTE_ADDR')
        #将用用户访问的时间存入列表,做判断,最新一次请求过来的时间减去倒数第5次访问的时间,如果大于多少,可一访问,反之不可以
        ctime = time.time()
        if remote_addr not in VISIT_RECORD:
            VISIT_RECORD[remote_addr] = [ctime,]
        	return True #访问成功
        history = VISIT_RECORD.get(remote_addr)
    	#return False #访问频率太高,被限制 
        
        while history and history[-1] < ctime - 60:
            history.pop()
        if len(history) > 3:
            
            return False
        history.insert(0,ctime)
        return True
    def wait(self):
        #还需要等多少秒才能访问
        ctime = time.time()
        return 60 - (ctime - self.history[-1])
```

```python
单独给某个类加:
    throttle_classes = [VisitThrottle]
```

###### 2. 执行流程

```python
进入dispath方法,封装request及属性,
调用self.initial(request, *args, **kwargs)方法 
执行权限认证方法:self.check_throttles(request)
    内部执行:for循环request封装的get_throttles():
        得到自定义的对象
    执行权限类对象中的allow_request(request, self):方法,
    如果为True,可以访问
    如果不为True,抛出异常,访问超过限制
```

###### 3. 继承SimpleRateThrottle内置以实现的访问频率

```python
from rest_framework.throttling import SimpleRateThrottle
#代码实现流程
class MyRateThrottle(SimpleRateThrottle):
    scope = 'Luffy'
    def get_cache_key(request, view):
        return self.get_ident(request)#BaseThrott类方法(父类)
#########方法详细解
    def get_ident(self, request):
        """
        Identify the machine making the request by parsing HTTP_X_FORWARDED_FOR
        if present and number of proxies is > 0. If not use all of
        HTTP_X_FORWARDED_FOR if it is available, if not use REMOTE_ADDR.
        """
        xff = request.META.get('HTTP_X_FORWARDED_FOR')#使用透明代理服务器时的真是IP
        remote_addr = request.META.get('REMOTE_ADDR')   #获取IP
        num_proxies = api_settings.NUM_PROXIES

        if num_proxies is not None:
            if num_proxies == 0 or xff is None:
                return remote_addr
            addrs = xff.split(',')
            client_addr = addrs[-min(num_proxies, len(addrs))]
            return client_addr.strip()

        return ''.join(xff.split()) if xff else remote_addr
```

```python
可在setting配置文件写入:
    REST_FRAMEWORK = {
        "DEFAULT_AUTHENTICATION_CLASSES":["api.utils.auth.Authication",],
        "UNAUTHENTICATED_USER":lambda:"匿名用户",
        "UNAUTHENTICATED_USER":None,#匿名,request.user = None
        "UNAUTHENTICATED_TOKEN":None,#匿名,request.auth = None
        "Luffy":'3/m'
    }
```



### 4. wsgi相关

```python
wsgi  #协议
wsgiref #实现了wsgi协议的一个模块,模块本质:一个socket服务端(Django)
werkzeug #实现了wsgi协议的一个模块,模块本质:一个socket服务端(Flask)
tornado #实现了wsgi协议的一个模块,模块本质:一个socket服务端(tornado)
uwsgi #实现了wsgi协议的一个模块,模块本质:一个socket服务端(线上)
```

1. Django的生命周期(rest framework)

   ```python
   请求进来,先走uwsgi,然后走Django的中间件,顺序执行process_request 方法,如果返回值不为None,值执行当前中间件的process_response方法,倒叙执行之前的process_response方法,如果返回值都为None,进入路由匹配,在执行视图之前,先执行中间件中的process_view方法,如果出现错误,则执行process_excption方法,返回一个response的话,则直接倒叙执行process_response方法,如果返回None,则执行视图,视图类先执行as_view方法,然后会返回一个view,view内部会调用
   dispath方法,将resquest封装,然后校验,走验证组件,权限,节流组件进行验证,视图内部走数据库返回模板之类的,然后依次倒叙执行中间件process_response方法.在走uwsgi,然后返回个客户端
   ```

2.中间件回答详解

```python
中间件是用于对所有的请求批量做操作的时候用中间件来实现
,比如说权限认证,csrf校验,还有session的校验等
```



### 5. 版本控制

```python

在URL中传参
urlpatterns = [
    url(r'^(?P<version>[v1|v2]+)/users/$',views.UsersView.as_view(),name='uuu'),
]

可在setting配置文件写入:
    REST_FRAMEWORK = {
        "DEFAULT_AUTHENTICATION_CLASSES":["api.utils.auth.Authication",],
        "UNAUTHENTICATED_USER":lambda:"匿名用户",
        "UNAUTHENTICATED_USER":None,#匿名,request.user = None
        "UNAUTHENTICATED_TOKEN":None,#匿名,request.auth = None
        "VERSION_PAPAM":'version'
    }
    

from rest_farmework.versioning import QueryParameterVersioning,URLPathVersioning

#两个类分别对应在URL传参数,一种实在结尾?加版本参数
class UsersView(APIView):
    versioning_class = URLPathVersioning
    def get(self,request,*args,**kwargs):
        print(request.version)#版本
        print(request.versioning_scheme)#处理版本的对象
        ul = request.versioning_scheme.reverse(viewname = 'uuu',request=request)#反向解析URL
        #此处反解析是类内封装的reverse方法,必须传request,不传默认为None,会出错
        return HttpResponse('用户列表')
```

### 6.解析器

- 依靠请求头contenttype对用户请求体中的数据进行解析，解析到request.data中

###### 1. request.post前戏

```python
request.post内有值得条件
1. 请求头要求:
    Content-Type:application/x-www-form-urlencoded  #请求头有这个,才回去请求体中取解析内容
2. 数据格式要求:
    name=alex&age=18&gender=男
```

###### 2.使用

```python
from rest_framework.request import Request
from rest_framework.parsers import FormParser,JSONParser
class ParserView(APIView):
    parser_classes = [JSONParser,FormParser]
    #JSONParser:表示只能解析content-type:application/json头
    #FormParser:表示只能解析content-type:application/x-www-form-urlencoded头
    def post(self,request,*args,**kwargs):
      '''
      request.data执行以下操作
      获取用户请求
      获取用户请求体
      根据用户请求头和parser_classes = [JSONParser,FormParser]中支持的格式将信息转换成我们需要的格式
      '''
      print(request.data)
```

```python
#可在settings做全局配置
    REST_FRAMEWORK = {
        "DEFAULT_AUTHENTICATION_CLASSES":		  ["api.utils.auth.Authication",],
        "UNAUTHENTICATED_USER":lambda:"匿名用户",
        "UNAUTHENTICATED_USER":None,#匿名,request.user = None
        "UNAUTHENTICATED_TOKEN":None,#匿名,request.auth = None
        "VERSION_PAPAM":'version',
        "DEFAULT_PARSER_CLASSES":['rest_framework.parsers.JSONParser','rest_framework.parsers.FormParser']
    }
```

###### 2.源码实现流程:

```python
from rest_framework.request import Request
#从request.data开始看起,点进Request类
执行self._load_data_and_files()方法:进入执行self._parse()方法,self为request,为request赋值content_type
执行select_parser(self, self.parsers),内部循环self.parsers中的对象,判断如果符合条件,则返回这个对象,(如果都不符合返回None,则函数执行结束抛出异常)

如果为1个对象,尝试执行这个对象的parse()方法,则实现解析器

#解决了返回的类型和数据格式读取不到的问题
```

### 7. 序列化

###### 1. 基本使用

```python
from rest_formwork import serializers

class RolesSerializer(serializers.Serializer):
    id = serializers.IntegerField()
    title = serializers.CharField()
    
class RilesView(APIView):
    def get(self,request,*args,**kwargs):
        #方式一：
        #roles = models.Roles.objects.all().values(id,title)
        #roles = list(roles)
        #ret = json.dumps(roles,ensure_ascii=False)
        
        #方式二 序列化器多个对象：
        #roles = models.Roles.objects.all()
        #ser = RolesSerializer(instance=roles,many=True)
        #ret = json.dumps(ser.data,ensure_ascii=False)
        
        #方式2.1  序列化器单个对象：
        role = models.Roles.objects.all().first()
        ser = RolesSerializer(instance=role,many=False)
        ret = json.dumps(ser.data,ensure_ascii=False)
        
        return HttpResponse(ret)
```

###### 1.1基本使用之（多对多） source参数

数据库user_info表

```python
class UserInfo(models.Model)：
	user_type_choices = (
    	(1,'普通用户'),
        (2,'VIP'),
        (3,'SVIP')
    )
    user_type = models.IntegerField(chocies=user_type_choice)
    username = models.CharField(max_length=32,unique=True)
    password = models.CharField(max_length=64)
    group = models.ForeignKey('UserGroup')
    roles = models.ManyToManyField('Role')
```

自定义的序列化类

```python
class UserSerializer(serializers.Serializer):
    xxx = serializers.CharField(source='user_tpe')#得到数据库中存的值
    ooo = serializers.CharField(source='get_user_type_display')#row.get_user_type_display()
    username = serializers.CharField()
    password = serializers.CharField()
    gp = serializers.CharField(source='group.title')
    rls = serializers.SerializerMethodField()#自定义显示
    
    def get_rls(self,row):
        role_obj_list = row.roles.all()
        ret = []
        for item in role_obj_list:
            ret.append('id':item.id,'tittle':item.title)
        return ret
```

###### 2. ModelSerializer类用法

```python
#类中将数据库字段类型都对应好了，所以直接写字段就好
class UserinfoSerializer(serializers.ModelSerializer):
    rls = serializers.SerializerMethodField() #自定义显示
    
    class Meta:
        model = models.Userinfo
        fields = ['id','username','password','oooo','rls']
        
     def get_rls(self,row): 
        role_obj_list = row.roles.all()
        ret = []
        for item in role_obj_list:
            ret.append('id':item.id,'tittle':item.title)
        return ret
```

```PYTHON
class UserinfoSerializer(serializers.ModelSerializer):
    
    class Meta:
        model = models.Userinfo
        fields = '__all__'
        depth = 1 #表示深度为1，例如外检关联的去到所有外检关联表的字段   最大深度最好不要超过3，查询速度慢
```

###### 3. HyperlinkedIdentityField,返回链接

```python
#views.py
class Userinfoserializer(serializers.ModelSerializer):
    group = serializers.HyperlinkedIdentityField(view_name='gp',lookup_field='group_id',lookup_url_kwarg='pk')
    #生成链接，而不是生成数据库中字段的值，两个参数必须带，后面可能会解释参数
    class Meta:
        model = models.UserInfo
        fields = ['id','username','password','group','roles']
        depth = 0

class Userinfoview(APIView):
    def get(self,request,*args,**kwargs):
        users = models.UserInfo.objects.all()
        ser = Userinfoserializer(instance=users,many=True,context={'request':request})
        ret = json.dumps(ser.data,ensure_ascii=False)
        return HttpResponse(ret)
```

```python
#models.py
class UserInfo(models.Model):
    user_type_choices = (
        (1, '普通用户'),
        (2, 'VIP'),
        (3, 'SVIP'))
    user_type = models.IntegerField(choices=user_type_choices)
    username = models.CharField(max_length=32, unique=True)
    password = models.CharField(max_length=64)
    group = models.ForeignKey('UserGroup')
    roles = models.ManyToManyField('Role')

class Role(models.Model):
    role = models.CharField(max_length=32)

class UserGroup(models.Model):
    title = models.CharField(max_length=32)
```

```python
#urls.py
urlpatterns = [
    url(r'^admin/', admin.site.urls),
   url(r'^api/v1/(?P<pk>\d+)',views.Userinfoview.as_view(),name='gp'),
]
```

###### 4.源码解析

```python
先从自定义的Userinfoserializer类实例化开始看，找父类的__new__方法，扎到basexxx类，执行判断many是否为True，为True则调用many_init(cls, *args, **kwargs):得到一个ListSerializer类实例化的对象，
    若为Flase，则实例化一个Filed对象
    然后走对象.data方法进入源码：
    第一种情况：
    	ListSerializer类实例：进入ListSerializer类，寻找data方法，方法内部调用父类data方法，找到父类方法，执行判断if self.instance is not None and not getattr(self, '_errors', None):
调用self.to_representation(self.instance)，从自定义类Userinfoserializer按继承顺序一层一层寻找到后执行field.get_attribute(instance)，返回一个get_attribute(instance, self.source_attrs)，方法内部执行判断给instance重新赋值为字段的值返回给
```

