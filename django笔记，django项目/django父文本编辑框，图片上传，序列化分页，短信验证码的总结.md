## 第一天

配置环境

local settings

```python

try:
    from .local_settings import *
except ImportError:
    pass
```

# 第二天

使用ckeditor 的流程

- 下载包

- 注册包

- 在models中引入

  from ckeditor_uploader.fields import RichTextUploadingField

  放在使用的字段

- 在前端使用到富文本编辑框的form

  ```html
  <form class="form-horizontal" action="" method="post" novalidate enctype="multipart/form-data">
  ```

- settings的配置

  将ckeditor 的根路由配置（与media连用，不加前/）

  配置media路径

- url的分发

- 导入两个固定的js文件

  <script src="{% static 'ckeditor/ckeditor/ckeditor.js' %}"></script>
  <script src="{% static 'ckeditor/ckeditor-init.js' %}"></script>



ckeditor、media环境的配置

```python
# settings.py
INSTALLED_APPS = [
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'web.apps.WebConfig',
    'backend.apps.BackendConfig',
    'repository.apps.RepositoryConfig',
    'ckeditor',    # 注册ckeditor
    'ckeditor_uploader',   # 使富文本编辑框可以上传文件
]

CKEDITOR_UPLOAD_PATH = 'ckeditor/'    # ckeditor上传的路径，注意不能加前/
  
MEDIA_URL = '/media/'   # 媒体文件上传的别名

MEDIA_ROOT = os.path.join(BASE_DIR, 'media')   # 媒体文件的储存路径
```

```python
# 根url.py
from django.conf.urls import url, include
from django.contrib import admin
from django.views.static import serve  # 引入一个serve给media提供服务
from django.conf import settings
from ckeditor_uploader import views   # 使用uploader的views中的函数

urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^', include('web.urls')),
    url(r'^backend/', include('backend.urls', namespace='backend')),
	# 配置media文件的存储路径
    url(r'^media/(?P<path>.*)', serve, {'document_root': settings.MEDIA_ROOT}),
	# 允许富文本编辑框的上传
    url(r'^ckeditor/upload/', views.upload),
    # url分发到ckeditor_uploader的url，执行上传方法和浏览方法
    url(r'^ckeditor/', include('ckeditor_uploader.urls')),

]
```

```python
# models.py
from ckeditor_uploader.fields import RichTextUploadingField


class ArticleDetail(models.Model):
    # 使用富文本编辑框字段，其他使用方法和models一致
    content = RichTextUploadingField(verbose_name='文章详情')
```



富文本编辑框上传的文件的目录会以时间的形式保存在media文件中

![1564213283947](D:\documents\netyun\S21SHP\updates\项目相关\ckeditor的文件存储.png)

# 第三天

