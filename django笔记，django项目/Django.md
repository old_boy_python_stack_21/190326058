# Django

### 1.原生socket

###### 1.  socket 

```python
#套接字  位于应用层和传输层之间的虚拟层  一组接口
c/s    ——》   b/s 
```

###### 2. 百度的服务器   socket服务端

```python
1. 创建socket服务端
2. 绑定IP和端口
3. 监听
4. 等待链接
5. 接收数据
6. 返回数据
7. 断开链接
```

###### 3. 浏览器   socket客户端

```python
5. 链接服务端
6. 发送数据
7. 接收数据
8. 断开链接
```

### 2. web 框架——  socket服务端

![1560219121217](E:\homework2\img\1560219121217.png)

### 3. HTTP协议

请求和应答 应用层协议

###### 1. 状态码

```python
1. **1xx消息**——请求已被服务器接收，继续处理
   1. 101 switching procotols
   2. 102 processing 扩展的状态
2. **2xx成功**——请求已成功被服务器接收、理解、并接收
3. **3xx重定向**——需要后续操作才能完成这一请求，当前服务器无法处理，响应另一个服务地址
4. **4xx请求错误**——请求含有词法错误或者无法被执行，没有该资源，**401表示认证错误、402 预留、403（forbidden）表示权限不够、405 method not allow**
5. **5xx服务器错误**——服务器在处理某个正确请求时发生错误，500服务端，代码问题。
```

###### 2. 请求方式  8种

```python
get    ——》 获取一个页面、图片（资源）
post ——》 提交数据
option
put
head
patch
...
```

###### 3. url 组成

```python
http://www.cnblogs.com/maple-shaw/articles/9060408.html
#协议  域名和端口  HTTP（80） HTTPS(443)    路径   参数 
```

###### 4. 请求组成（request）

```python
浏览器  —— 》 服务器  
GET 请求没有请求数据
“请求方式 url路径 协议版本\r\n
k1:v1\r\n
k2:v2\r\n
\r\n
数据”
```

###### 5. 响应（response）

```python
服务器  —— 》 浏览器 

“协议版本 状态码 状态码描述\r\n
k1:v1\r\n
k2:v2\r\n
\r\n
响应数据（响应体）”
```

###### 6.web框架的功能

```python
1.socket收发消息    - wsgiref   uwsgi 
2.根据不同的路径返回不同的内容
3.返回动态页面（字符串的替换） —— jinja2
```

```python
django  2 3
flask 2
tornado  1 2 3 
```

### 4. django框架

###### 1. 下载安装

```python
1.命令行
pip3 install django==1.11.21 -i https://pypi.tuna.tsinghua.edu.cn/simple
2.pycharm
```

###### 2. 创建项目

1.命令行：

```python

找一个文件夹存放项目文件：

打开终端：
django-admin startproject 项目名称

项目目录
```

![1560226751493](E:\homework2\img\1560226751493.png)

2. pycharm

![](E:\homework2\img\1560227015628.png)

###### 3. 启动

1. 命令行启动

   切换到项目的根目录下  manage.py

   `python36 manage.py runserver `  —— <http://127.0.0.1:8000/>

   `python36 manage.py runserver 80 `   在80端口启动

   `python36 manage.py runserver 0.0.0.0:80`   0.0.0.0:80

2.  pycharm 

   点击绿三角启动   可配置

5. 简单使用

   模板——》HTML文件夹

   urls.py

   ```python
   # 导入
   from django.shortcuts import HttpResponse,render
   
   # 函数
   def index(request):
       # return HttpResponse('index')
       return render(request,'index.html')
   
   # url和函数对应关系
   urlpatterns = [
       url(r'^admin/', admin.site.urls),
       url(r'^index/', index),
   ]
   ```


### 5. 静态文件的配置和使用

1. 静态文件的配置和使用

   settings.py

   ```python
   STATIC_URL = '/static/'   # 别名
   STATICFILES_DIRS = [
       os.path.join(BASE_DIR, 'static1'),
       os.path.join(BASE_DIR, 'static'),
       os.path.join(BASE_DIR, 'static2'),
   ]
   ```

```python
<link rel="stylesheet" href="/static/css/login.css">   # 别名开头
```

按照STATICFILES_DIRS列表的顺序进行查找。

2. 简单的登录的实例

   form表单递交数据注意的问题

   1. 提交的地址action=""  请求的方式 method="post"
   2. 所有的input框有name属性
   3. 有一个input框的type="submit" 或者 有一个button

   提交POST请求，把settings中MIDDLEWARE的'django.middleware.csrf.CsrfViewMiddleware'注释掉

3. app

   ![1560310077499](E:\homework2\img\1560310077499.png)

### 6.创建APP

```python
命令行
python manage.py startapp app名称

pycharm工具
tools ——run manage.py task  ——》  startapp  app名称
```

注册APP

```python
INSTALLED_APPS = [
	...
    'app01',
    'app01.apps.App01Config',  # 推荐写法
]
```

### 6. ORM  对象关系映射

##### 1. Django 中使用Mysql 数据库的流程

1. 创建一个Mysql数据库

2. 在settings中配置，Django链接Mysql数据库

   ```python
   DATABASES = {
       'default': {
           'ENGINE': 'django.db.backends.mysql',    # 引擎	
           'NAME': 'day53',						# 数据库名称
           'HOST': '127.0.0.1',					# ip地址
           'PORT':3306,							# 端口
           'USER':'root',							# 用户
           'PASSWORD':'123'						# 密码
           
       }
   }
   ```

3. 在与settings同级目录下的 init文件中写：

   ```python
   import pymysql
   pymysql.install_as_MySQLdb()
   ```

4. 创建表（在app下的models.py中写类）:

   ```python
   from django.db import models
   
   class User(models.Model):
       username = models.CharField(max_length=32)   # username varchar(32)
       password = models.CharField(max_length=32)   # username varchar(32)
   ```

5.执行数据库迁移的命令

python manage.py makemigrations   #  检测每个注册app下的model.py   记录model的变更记录

python manage.py migrate   #  同步变更记录到数据库中

##### 2. orm操作

```python
# 获取表中所有的数据
ret = models.User.objects.all()  # QuerySet 对象列表  【对象】
# 获取一个对象（有且唯一）
obj = models.User.objects.get(username='alex')   # 获取不到或者获取到多个对象会报错
# 获取满足条件的对象
ret = models.User.objects.filter(username='alex1',password='dasb')  # QuerySet 对象列表
```

###### 1. 新增

```python
obj = models.Publisher.objects.create(name=publisher_name)
models.Book.objects.create(title=book_name,pub=出版社的对象)
models.Book.objects.create(title=book_name,pub_id=pub_id)
```

###### 2. 删除

```python
obj_list = models.Publisher.objects.filter(pk=pk)
obj_list.delete()

obj = models.Publisher.objects.get(pk=pk)
obj.delete()
```

###### 3. for 循环

```html
{% for publisher in all_publishers %}
        <tr>
            <td>{{ forloop.counter }}</td>
            <td>{{ publisher.pk }}</td>
            <td>{{ publisher.name }}</td>
            <td><a href="/create_publisher/?id={{ publisher.pk }}">编辑</a></td>
            <td><a href="/del_publisher/?id={{ publisher.pk }}">删除</a></td>
        </tr>
    {% endfor %}
```

###### 4. {{}}用法

```html
def show_publishers(request):
    # 从数据库中查询到出版社的信息
    all_publishers = models.Publisher.objects.all().order_by('pk')
    # 返回一个包含出版社信息的页面
    return render(request, 'publisher_list.html', {'all_publishers': all_publishers})
```

###### 5. 编辑 if 条件

```python
{% if book_obj.pub == publisher %}
    <option selected value="{{ publisher.pk }}"> {{ publisher.name }} </option>
{% else %}
    <option  value="{{ publisher.pk }}"> {{ publisher.name }} </option>
{% endif %}
```

###### 6.  修改数据

```python
# 修改数据
book_obj.title = book_name
# book_obj.pub_id = pub_id
book_obj.pub = models.Publisher.objects.get(pk=pub_id)
book_obj.save()
```

###### 7. 多对多，第三张表

1. 简单方案:Django 帮忙创建这张表

   ```python
   class Author(models.Model):
       name = models.CharField(max_length=32)
       books = models.ManyToManyField('Book')  # 不在Author表中生产字段，生产第三张表
       
   # author_obj.books       拿到这个作者对应的第三者对象
   # author_obj.books.all()    通过这个第三者对象拿到这个作者对应的所有书的对象
   
   关于写入第三张表
   获取到返回给服务器的列表用getlist()
   
   #将列表同步到第三张表中的方法
   对象.books.set方法
   author_obj.books.set(book_lis)
   ```

2. 自己创建第三张表

   ```python
   class AuthorBook(models.Model):
       author = models.ForeignKey(Author, on_delete=models.CASCADE)
       book = models.ForeignKey(Book, on_delete=models.CASCADE)
       date = models.DateField()
   ```

3. 自建的表和 ManyToManyField 联合使用

   ```python
   class Author(models.Model):
       name = models.CharField(max_length=32)
       books = models.ManyToManyField('Book',through='AuthorBook')  # 不在Author表中生产字段，生产第三张表
   class AuthorBook(models.Model):
       author = models.ForeignKey(Author, on_delete=models.CASCADE)
       book = models.ForeignKey(Book, on_delete=models.CASCADE)
       date = models.DateField()
   ```

### 8. MVC  和 MTV

```python
MVC
M:model  模型
V: view 视图 -HTML
C：controller   控制器  ——路由传递指令 业务逻辑

MTV:
M： model   模型 ORM
T ： tempalte  模板 -HTML
V:     view   业务逻辑 
```

### 9. 模板  - 变量

```python
变量{{ }}
.索引  .key .属性 .方法

Filter  过滤器
语法 ：{{value|filter_name:参数}}
```

### 8. 内置过滤器方法

###### 1. default

```
{{ value|default:"nothing"}}  # 变量不存在或者为空  显示默认值

#在setting文件里的 TEMPLATES的OPTIONS可以增加一个选项：string_if_invalid：'找不到'，可以替代default的的作用。
```

###### 2. filesizeformat

 文件大小

###### 3. add    

相当于+    加法    字符串的拼接   列表的拼接

add 负数，相当于减法

###### 4. widthratio

注意一个大括号{一个大括号}{一个大括号}{一个大括号}

```python
{% widthratio  5 1 100 %}    第一个参数会除以第二个参数乘第三个参数  如果乘法 则将第二个参数设置成1
如果算除法  则将第三个参数设置成1
```

###### 5. slice

```
{{ hobby|slice:'-2:0:-1' }}
```

###### 6. date

```
{{ now|date:'Y-m-d H:i:s' }}
```

- setting中的配置：

```python
USE_L10N = False
DATETIME_FORMAT = 'Y-m-d H:i:s'
```

###### 7. safe 

告诉django不需要转义

### 9. 自定义filter

1. 在app下创建一个名为templatetags的包；（名不能变）

2. 在python中创建py文件，文件名自定义（my_tags.py）

3. 在py文件中写：

   ```python
   from django import template
   
   register = template.Library()  # register也不能变
   
   #写函数 + 装饰器
   @register.filter
   def add_xx(value, arg):  # 最多有两个
   
       return '{}-{}'.format(value, arg)
   ```

4. 使用：

   ```html
   {% load my_tags %}   #导入这个文件夹的名字
   {{ 'alex'|add_xx:'dsb' }}    #使用文件夹的方法
   ```


### 10. 模板 循环语句

```python
1. for if
2. for
3. forloop
4. for   empty
5. {% with  xxx as sss%}     给变量取个名字
   {% endwith %}
```

```html
{% for book in all_books %}
    <tr>
		.....
    </tr>
{% empty %}    #如果for循环没有值 则执行这条内容，反之有，不执行
    <td colspan="5" style="text-align: center">没有相关的数据</td>
{% endfor %}
```

###### - 连续判断

```python
python  10>5>1  =》   10>5  and 5>1   true 
js     10>5>1  =》   10>5  =》 true   =》   1>1  false
模板中  不支持连续连续判断  也不支持算数运算（过滤器）
```


### 11. 母版和继承

```python
母版
就是一个普通的HTML提取多个页面的公共部分  定义block块

2. 继承
   1. {% extends ‘base.html’  %}
   2. 重写block块——写子页面独特的内容

注意的点：
1. {% extends 'base.html' %} 写在第一行   前面不要有内容 有内容会显示
2. {% extends 'base.html' %}  'base.html' 加上引号   不然当做变量去查找
3. 把要显示的内容写在block块中
4. 定义多个block块，定义 css  js 块
```

### 12. 组件

一小段HTML代码段——nva.html

{%  include  ‘nva.html’ %}

### 13. 静态文件

```html
{% load static %}

 <link rel="stylesheet" href="{% static '/plugins/bootstrap-3.3.7/css/bootstrap.css' %}">
 <link rel="stylesheet" href="{% static '/css/dsb.css' %}">

{% static '/plugins/bootstrap-3.3.7/css/bootstrap.css' %}  
{% get_static_prefix %}   ——》 获取别名
```

### 14. 自定义simpletag

```python
![1560917495352](E:\homework2\img\asset\1560917495352.png)@register.simple_tag
def join_str(*args, **kwargs):
    return '{} - {} '.format('*'.join(args), '$'.join(kwargs.values()))
```

1. 使用

   ```html
   {% load my_tags %}
   {% join_str '1' '2' k1='3' k2='4' %}
   ```


### 15. 自定义 inclusion_tag

返回HTML代码段

![1560917495352](E:\homework2\img\asset\1560917495352.png)

### 16. with  定义中间变量

```html
# 只能在with 框里面用，出去了就不好使了
{% with total=business.employees.count %}
    {{ total }} employee{{ total|pluralize }}
{% endwith %}
```

### 17. csrf  跨站请求伪造

```python
csrf_token 标签用于跨站请求伪造保护
在页面的form表单里面写上{%  csrf_token  %}
有一个隐藏的input标签   name = ‘csrfmiddlewareroken’
```

### 18. 关于forloop

- 2层嵌套

```python
<table border="1">
    <tbody>
    {% for hobby_list1 in hobby_list2 %}
        <tr>
            {% for hobby in hobby_list1 %}
                {% if forloop.counter|divisibleby:2 and  forloop.parentloop.counter|divisibleby:2 %}
                    <td style="color: red"> {{ hobby }}</td>
                {% else %}
                    <td> {{ hobby }}</td>
                {% endif %}
            {% endfor %}
        </tr>
    {% endfor %}
    </tbody>
</table>
```

![forloop2层循环](E:\homework2\img\forloop2层循环.jpg)

```python
父级循环为parentloop下的集合  通过forloop.parentloop.内部方法取值

本级循环在内部 直接点方法即可实现
```

![](E:\homework2\img\QQ图片20190619174757.png)

### 19. CBV和FBV

1. 含义

```python
FBV   function based  view    
CBV   class based  biew
```

2. 定义CBV:

```python
from django.views import View

class AddPublisher(View):
    def get(self,request):
        """处理get请求"""
        return response  
    
    def post(self,request):
        """处理post请求"""
        return response
```

3.  使用CBV：

   ```python
   url(r'^add_publisher/', views.AddPublisher.as_view()),
   ```


### 20. as_view 的流程

1. 项目启动 加载ur.py时，执行类as_view()  ——》 view函数

2. 请求到来的时候执行view函数：

   1. 实例化类——》self

      self.request = request

   2. 执行self.dispatch（request，*args，**kwargs）
      1. 判断请求方式是否被允许：
      2. 允许 通过反射获取到对应请求方式的方法——》handler
      3. 不允许    self.http_method_not_allowed——》handler
      4. 执行handler(request, *args,**kwargs)
      5. 返回响应——浏览器

### 21. 视图加装饰器

1. FBV  直接加装饰器

2. CBV

   ```python
   from django.utils.decorators import method_decorator
   ```

   1. 加在方法上

      ```python
      @method_decorator(timer)
      def get(self, request, *args, **kwargs):
          """处理get请求"""
      ```

   2. 加在dispath方法上(此方法为重写父类方法，supper继承父类方法)

      ```python
      @method_decorator(timer)
      def dispatch(self, request, *args, **kwargs):
          # print('before')
          ret = super().dispatch(request, *args, **kwargs)
          # print('after')
          return ret
      
      
      @method_decorator(timer,name='dispatch')
      class AddPublisher(View):
      ```

   3. 加在类上

      ```python
      @method_decorator(timer,name='post')
      @method_decorator(timer,name='get')
      class AddPublisher(View):
      ```

   4. 区别：

      ```python
      不适用method_decorator
      #打印函数名
      func   ——》 <function AddPublisher.get at 0x000001FC8C358598>
      #打印参数
      args  ——》 (<app01.views.AddPublisher object at 0x000001FC8C432C50>, <WSGIRequest: GET '/add_publisher/'>)
      
      
      
      使用method_decorator之后：
      
      func ——》 <function method_decorator.<locals>._dec.<locals>._wrapper.<locals>.bound_func at 0x0000015185F7C0D0>
      
      args ——》 (<WSGIRequest: GET '/add_publisher/'>,)
      ```


### 22.  request对象

```python
# 属性
request.methot   请求方式  GET
request.GET    url上携带的参数
request.POST   POST请求提交的数据
request.path_info   URL的路径    不包含ip和端口  不包含参数
request.body    请求体  b''
request.FILES   上传的文件  前端form + 		enctype='multipart/form-data'
request.META    请求头 

request.COOKIES  cookie
request.session	 session

requests.META   头信息

# 方法
request.get_full_path()   URL的路径   不包含ip和端口 包含参数
request.is_ajax()    判断是都是ajax请求
request.get_host（）             获取主机的IP和端口
```

### 23. response 对象

```python
from django.shortcuts import render, redirect, HttpResponse
HttpResponse('字符串')    ——》  ’字符创‘
render(request,'模板的文件名',{k1:v1})   ——》 返回一个完整的TML页面
redirect('重定向的地址')    ——》 重定向   Location ： 地址

from django.http.response import JsonResponse
JsonResponse({})
JsonResponse([],safe=False)  非字典 加safe参数
```

### 24. upload  下载post请求

```python
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="x-ua-compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Title</title>
</head>
<body>
<form action="" method="post" enctype="multipart/form-data">
    {% csrf_token %}
    <input type="file"  name="f1">
    <button>上传</button>
</form>
</body>
</html>
```

### 25. 路由

##### 1.urlconf

```python
from django.conf.urls import url
from . import views

urlpatterns = [
    # url(r'^admin/', admin.site.urls),
    url(r'^home/$', views.home,{}, name='home'),

]
```

##### 2.正则表达式

r  ^   $  [0-9]  \d  \w   + ?   * {4} 

##### 3.分组和命名分组

- url上捕获参数  都是字符串

1. 分组
   - 捕获的参数会按照位置传参传递给视图函数

```python
url(r'^blog/([0-9]{4})/(\d{2})$', views.blogs, name='blogs'),
```

2. 命名分组
   - 捕获的参数会按照关键字传参传递给视图函数

```python
url(r'^blog/(?P<year>[0-9]{4})/(?P<month>\d{2})$', views.blogs, name='blogs'),
```

##### 4.路由分发 include

```python
from django.conf.urls import url, include
from django.contrib import admin
from app01 import views

urlpatterns = [
    # url(r'^admin/', admin.site.urls),
    url(r'^app01/', include('app01.urls', namespace='app01')),
    url(r'^app02/', include('app02.urls', namespace='app02')),
]
```

##### 5.传递参数

```python
  url(r'^home/(?P<year>\d{4})$', views.home,{'year':2019}, name='home'),
```

##### 6.url的命名和反向解析

静态路由

1. 命名：

```python
	url(r'^home/$', views.home, name='home'),
```

- 反向解析：

```python
模板
{% url 'home' %}    ——》   '/app01/home/'
```

```python
py文件
from django.urls import reverse
reverse('home')    ——》    '/app01/home/'
```

2. 分组命名：

```python
url(r'^blog/([0-9]{4})/(\d{2})$', views.blogs, name='blogs'),
```

- 反向解析：

```python
模板
{% url 'blogs' '2019' '06' %}    ——》    '/app01/blog/2019/06'

py文件
from django.urls import reverse
reverse('blogs,'args=('2019','07'))    ——》    '/app01/blog/2019/07'
```

3. 命名分组命名：

```python
url(r'^blog/(?P<year>[0-9]{4})/(?P<month>\d{2})$', views.blogs, name='blogs'),
```

- 反向解析：

```python
模板
{% url 'blogs' '2019' '06' %}    ——》    '/app01/blog/2019/06'     按照位置传参
{% url 'blogs'  month='06' year='2019' %}    ——》    '/app01/blog/2019/06'    按照关键字传参 

py文件
from django.urls import reverse
reverse('blogs,'args=('2019','07'))    ——》    '/app01/blog/2019/07'    按照位置传参
reverse('blogs',kwargs={'year':'2018','month':'08'})     ——》   '/app01/blog/2018/08'     按照关键字传参 
```

##### 7.namespace

```python
 url(r'^app01/', include('app01.urls', namespace='app01')),
    
 #{% url 'app01:home' %}    ——》   '/app01/home/'
#reverse('app01:home')    ——》    '/app01/home/'
```

### 26. ORM操作

##### 1. orm的字段和参数

```python\
AutoField    主键(必须制定为主键,如果不指定会报错)
IntegerField  整数
CharField    字符串
BooleanField  布尔值
TextField    文本
DateTimeField DateField  日期时间
    auto_now_add=True    # 新增数据的时候会自动保存当前的时间
    auto_now=True        # 新增、修改数据的时候会自动保存当前的时间
DecimalField   十进制的小数
	max_digits       小数总长度   5
    decimal_places   小数位长度   2
```

1. null       数据库中字段是否可以为空
2. blank=True  form表单填写时可以为空
3. default    字段的默认值
4. db_index    创建索引
5. unique   唯一
6. choices=（（0，‘女’），（1，‘男’））可填写的内容和提示

### 27. 表的参数

```python
class Meta:
        # 数据库中生成的表名称 默认 app名称 + 下划线 + 类名
        db_table = "table_name"
 
        # 联合索引 
        index_together = [
            ("pub_date", "deadline"),   # 应为两个存在的字段
        ]
 
        # 联合唯一索引
        unique_together = (("driver", "restaurant"),)   # 应为两个存在的字段
```

### 28.  必知必会13条

```python
#  all()   获取所有的数据   ——》 QuerySet   对象列表
ret = models.Person.objects.all()

# get()    获取满足条件的一个数据   ——》 对象
#  获取不到或者多个都报错
ret = models.Person.objects.get(pk=1)

# filter()    获取满足条件的所有数据   ——》 QuerySet   对象列表
ret = models.Person.objects.filter(pk=1)

# exclude()    获取不满足条件的所有数据   ——》 QuerySet   对象列表
ret = models.Person.objects.exclude(pk=1)

# values()        拿到对象所有的字段和字段的值    QuerySet  [ {} ,{} ]
# values('字段')   拿到对象指定的字段和字段的值    QuerySet  [ {} ,{} ]
ret = models.Person.objects.values('pid','name')

# values_list()     拿到对象所有的字段的值    QuerySet  [ () ,() ]
# values('字段')   拿到对象指定的字段的值      QuerySet  [ {} ,{} ]
ret = models.Person.objects.values_list('name','pid')

# order_by  排序   - 降序    ——》 QuerySet  [ () ,() ]
ret = models.Person.objects.all().order_by('age','-pid')

# reverse  反向排序   只能对已经排序的QuerySet进行反转
ret = models.Person.objects.all()
ret = models.Person.objects.all().reverse()

# distinct 去重  完全相同的内容才能去重
ret = models.Person.objects.values('age').distinct()

#  count()  计数
ret = models.Person.objects.all().count()

# first  取第一元素   没有元素 None
ret = models.Person.objects.filter(pk=1).values().first()

# last  取最后一元素   没有元素 None

# exists 查询的数据是否存在
ret = models.Person.objects.filter(pk=1000).exists()
```

```python
#返回queryset
all()    
filter()
exclude()
values()  
values_list() 
order_by() 
reverse() 
distinct()
```

```python
#返回对象
get() 
first()
last()
```

```python
#返回数字
count()
```

```python
#返回布尔值的
exists()
```

### 29. 表单的双下划线

```python
ret = models.Person.objects.filter(pk__gt=1)   # gt  greater than   大于
ret = models.Person.objects.filter(pk__lt=3)   # lt  less than   小于
ret = models.Person.objects.filter(pk__gte=1)   # gte  greater than   equal    大于等于
ret = models.Person.objects.filter(pk__lte=3)   # lte  less than  equal  小于等于

ret = models.Person.objects.filter(pk__range=[2,3])   # range  范围
ret = models.Person.objects.filter(pk__in=[1,3,10,100])   # in  成员判断

ret = models.Person.objects.filter(name__contains='A')       #里面含有'A'的项
ret = models.Person.objects.filter(name__icontains='A')   # 忽略大小写

ret = models.Person.objects.filter(name__startswith='a')  # 以什么开头
ret = models.Person.objects.filter(name__istartswith='A')

ret = models.Person.objects.filter(name__endswith='a')  # 以什么结尾
ret = models.Person.objects.filter(name__iendswith='I')    #以什么结尾不区分大小写

ret  = models.Person.objects.filter(birth__year='2019')     #日期年份为2019的
ret  = models.Person.objects.filter(birth__contains='2018-06-24')      #里面含有'2018-06-24'的项
ret  = models.Person.objects.filter(phone__isnull=False)#筛选电话为空的
```

### 30. 外键操作

```python
class Book(models.Model):
    title = models.CharField(max_length=32)
    pub = models.ForeignKey(Publisher, related_name='books',related_query_name='xxx',on_delete=models.CASCADE)#

    def __str__(self):
        return self.title
    
# 基于字段的查询
ret = models.Book.objects.filter(title='菊花怪大战MJJ')
#  查询老男孩出版的书
ret = models.Book.objects.filter(pub__name='老男孩出版社')

# 查询出版菊花怪大战MJJ的出版社
***************反向查询****************

# related_name='books'
# ret= models.Publisher.objects.filter(books__title='菊花怪大战MJJ')

# 没有指定related_name   类名的小写
# ret= models.Publisher.objects.filter(book__title='菊花怪大战MJJ')

# related_query_name='xxx'

ret= models.Publisher.objects.filter(xxx__title='菊花怪大战MJJ')
```



### 31. 创建超级用户（Django后台管理）

###### *主要跟表的字段（26知识点）参数对应使用*

```python
命令语句
python36  manage.py  createsuperuser
用户名：   root
密码：  root1234
再次输入密码：root1234
```

admin注册类在管理页面显示

```python
app下的admin文件    导入

from django.contrib import admin
from app01 import models

Person类将在管理页面中显示
Person表就注册完成了
admin.site.register(models.Person)

```

### 32. 多对多的操作

```python
class Book(models.Model):
    title = models.CharField(max_length=32)
    price = models.DecimalField(max_digits=6, decimal_places=2)  # 9999.99
    sale = models.IntegerField()
    kucun = models.IntegerField()
    pub = models.ForeignKey(Publisher, null=True,
                            on_delete=models.CASCADE)

    def __str__(self):
        return self.title

class Author(models.Model):
    name = models.CharField(max_length=32, )
    books = models.ManyToManyField(Book)

    def __str__(self):
        return self.name
```

1. 基于对象的查询

   ```python
   mjj = models.Author.objects.get(pk=1)
   print(mjj.books)  #  ——》  关系管理对象
   print(mjj.books.all())
   
   book_obj = models.Book.objects.filter(title='桃花侠大战菊花怪').first()
   不指定related_name
   print(book_obj.author_set)  #  ——》  关系管理对象
   print(book_obj.author_set.all())
   related_name='authors'
   print(book_obj.authors)  #  ——》  关系管理对象
   print(book_obj.authors.all())
   
   ret  =  models.Author.objects.filter(books__title='菊花怪大战MJJ')
   print(ret)
   
   不指定related_name
   ret = models.Book.objects.filter(author__name='MJJ')
   related_name='authors'
   ret = models.Book.objects.filter(authors__name='MJJ')
   related_query_name='xxx'
   ret = models.Book.objects.filter(xxx__name='MJJ')
   print(ret)
   ```

2. 基于字段的查询

   ```python
   mjj = models.Author.objects.get(pk=1)
   
   all()  所关联的所有的对象
   
   print(mjj.books.all())
   set  设置多对多的关系    [id,id]    [ 对象，对象 ]
   mjj.books.set([1,2])
   mjj.books.set(models.Book.objects.filter(pk__in=[1,2,3]))
   
   add  添加多对多的关系   (id,id)   (对象，对象)
   mjj.books.add(4,5)
   mjj.books.add(*models.Book.objects.filter(pk__in=[4,5]))
   
   remove 删除多对多的关系  (id,id)   (对象，对象)
   mjj.books.remove(4,5)
   mjj.books.remove(*models.Book.objects.filter(pk__in=[4,5]))
   
   clear()   清除所有的多对多关系
   mjj.books.clear()
   
   create()
   obj = mjj.books.create(title='跟MJJ学前端',pub_id=1)
   print(obj)
   book__obj = models.Book.objects.get(pk=1)
   
   obj = book__obj.authors.create(name='taibai')
   print(obj)
   ```
   
   

### 33. 聚合和分组聚合

###### 1.聚合

```python
from app01 import models
from django.db.models import Max, Min, Avg, Sum, Count

ret = models.Book.objects.filter(pk__gt=3).aggregate(Max('price'),avg=Avg('price'))
print(ret)

# 分组
# 统计每一本书的作者个数
ret = models.Book.objects.annotate(count=Count('author')) # annotate 注释

# 统计出每个出版社的最便宜的书的价格
# 方式一     表示以Publisher主键分组反向查询BOOK类名小写book双下划线加字段  的values 的值
ret = models.Publisher.objects.annotate(Min('book__price')).values()
# 方式二
ret = models.Book.objects.values('pub_id').annotate(min=Min('price'))

#！！！！！！第一个values表示以什么字段为分组，第二个values表示取值，（但是只能取第一个values分组里的字段或聚合函数的字段）  如果不在这两个范围内，则会默认为分组的条件

```

### 34. F 和Q

```python
#Django项目某个py文件内写上这些代码就可以直接操作orm
import os

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "orm_practice.settings")
import django

django.setup()

from app01 import models

# F
ret = models.Book.objects.filter(price__gt=100)
from django.db.models import F

# 比较两个字段的值
# ret=models.Book.objects.filter(sale__gt=F('kucun'))

#  更新所有的字段
# obj = models.Book.objects.get(pk=1)
# obj.sale =100
# obj.save()

# 只更新sale字段
# models.Book.objects.all().update(sale=100)

# 取某个字段的值进行操作
# models.Book.objects.all().update(sale=F('sale')*2+10)

# Q

from django.db.models import Q

ret = models.Book.objects.filter(~Q(Q(pk__gt=3) | Q(pk__lt=2)) & Q(price__gt=50))
print(ret)
```

###### 2. 条件

~  表示非
|  表示或
&  表示and
， 表示两个条件

```python
from django.db.models import Q
ret = models.Book.objects.filter(Q(Q(pk__gt=3) | Q(pk__lt=2)) & Q(price__gt=50))
print(ret)
```

### 35. 事务

```python
异常处理捕获一定是在事务外部

from django.db import transaction

try:
    with transaction.atomic():
        # 进行一系列的ORM操作

        models.Publisher.objects.create(name='xxxxx')
        models.Publisher.objects.create(name='xxx22')

except Exception as e :
    print(e)
```

### 36. django打印 mysql语句

1. 代码复制到setting文件

```python
LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
    'handlers': {
        'console':{
            'level':'DEBUG',
            'class':'logging.StreamHandler',
        },
    },
    'loggers': {
        'django.db.backends': {
            'handlers': ['console'],
            'propagate': True,
            'level':'DEBUG',
        },
    }
}
```

### 37 . COOKIE

1. 定义

   保存在浏览器本地上的一组组键值对

   确切的说是由服务器返回的reponse的响应头中的键值对

   浏览器会每次将这个键值对都会包含在请求头里面发送给服务器

2. 特点
   1. 由服务器让浏览器进行设置的
   2. 浏览器保存在浏览器本地
   3. 下次访问时自动携带

3. 应用“

   1. 登录
   2. 保存浏览习惯
   3. 简单投票


### 38. Django中操作cookie

为什么要有cookie

因为HTTP协议是无状态的，应用cookie就是为了使HTTP变相的拥有状态

1. 设置Cookie及一些参数：

   ret = reditrect('index')

   ret.set_cookie(key,value)   

    set-cookie:  key=value

   ret.set_signed_cookie('is_login', '1', 's21')  

   Set-Cookie: is_login=1; Path=/

   ```python
   rep = HttpResponse(...)
   rep ＝ render(request, ...)
   
   rep.set_cookie(key,value,...)
   rep.set_signed_cookie(key,value,salt='加密盐',...)
   参数：
   
   key, 键
   value='', 值
   max_age=None, 超时时间
   expires=None, 超时时间(IE requires expires, so set it if hasn't been already.)
   path='/', Cookie生效的路径，/ 表示根路径，特殊的：根路径的cookie可以被任何url的页面访问
   domain=None, Cookie生效的域名
   secure=False, https传输
   httponly=False 只能http协议传输，无法被JavaScript获取（不是绝对，底层抓包可以获取到也可以被覆盖）
   ```

   ```python
 def post(self, request, *args, **kwargs):
           username = request.POST.get('username')
           pwd = request.POST.get('pwd')
           if username == 'alex' and pwd == '123':
               url = request.GET.get('return_url')
               if url:
                   ret = redirect(url)
               else:
                   ret = redirect('/index/') 
        ********************设置cookie***********************
               # ret['Set-Cookie'] = 'is_login=100; Path=/'       
               # ret.set_cookie('is_login', '1')  # Set-Cookie: is_login=1; Path=/
               # ret.set_signed_cookie('is_login', '1', 's21',max_age=10000,)  # Set-Cookie: is_login=1; Path=/
               # 设置设session
               request.session['is_login'] = 1
               # request.session.set_expiry(0)#过期时间
               return ret
           return render(request, 'login.html', {'error': '用户名或密码错误'})
   ```
   
2. 获取cookie

   ```python
   request.COOKIES['key']
   request.get_signed_cookie('key', default=RAISE_ERROR, salt='', max_age=None)
   get_signed_cookie方法的参数：
   
   default: 默认值
   salt: 加密盐
   max_age: 后台制过期时间
   ```

   ```python
#例子
   
   def login_required(func):
       def inner(request, *args, **kwargs):
           is_login = request.COOKIES.get('is_login')
           is_login = request.get_signed_cookie('is_login', salt='s21', default='')
           is_login = request.session.get('is_login')
           print(is_login)
           url = request.path_info
           if is_login != 1:
               return redirect('/login/?return_url={}'.format(url))
           # 已经登录
           ret = func(request, *args, **kwargs)
   
           return ret
   
       return inner
   
   ```
   
3. 删除cookie

   ```python
   def logout(request):
       ret = redirect('/login/')
       # ret.delete_cookie('is_login')
       # request.session.delete()    # 删除session数据  不删除cookie
       request.session.flush()      # 删除session数据  删除cookie
       return ret
   ```


### 39. session   (可以认为是COOKIE的一种)

##### 1. 与cookie区别

cookie是直接将键值对包含在响应头中传递给浏览器

而session 是将键值对存储在服务器，在生成一个随机的字符串为键（存储方式随机字符串为key，生成的内容为值）  包含在响应头内发送给浏览器

##### 2. 为什么要有session？

1. cookie 保存在浏览器本地（存在安全问题）
2. 大小个数受到限制（值的长度若过长则会出现问题）

### 40. django 中操作session

```python
# 获取、设置、删除Session中数据
request.session['k1']
request.session.get('k1',None)
request.session['k1'] = 123
request.session.setdefault('k1',123) # 存在则不设置
del request.session['k1']

# 所有 键、值、键值对
request.session.keys()
request.session.values()
request.session.items()
request.session.iterkeys()
request.session.itervalues()
request.session.iteritems()

# 会话session的key
request.session.session_key

# 将所有Session失效日期小于当前日期的数据删除
request.session.clear_expired()

# 检查会话session的key在数据库中是否存在
request.session.exists("session_key")

# 删除当前会话的所有Session数据
request.session.delete()
　　
# 删除当前的会话数据并删除会话的Cookie。
request.session.flush() 
    这用于确保前面的会话数据不可以再次被用户的浏览器访问
    例如，django.contrib.auth.logout() 函数中就会调用它。

# 设置会话Session和Cookie的超时时间
request.session.set_expiry(value)
    * 如果value是个整数，session会在些秒数后失效。
    * 如果value是个datatime或timedelta，session就会在这个时间后失效。
    * 如果value是0,用户关闭浏览器session就会失效。
    * 如果value是None,session会依赖全局session失效策略。
```

### 41. session的配置

见全局默认配置

from django.conf import global_settings

### 42. CSRF  跨站请求伪造 校验（中间件）

##### 1. 装饰器

```python
from django.views.decorators.csrf import csrf_exempt,csrf_protect，ensure_csrf_cookie
1. csrf_exempt   某个视图不需要进行csrf校验    只能加到dispatch方法上（类中或重写dispatch方法）
2. csrf_protect  某个视图需要进行csrf校验    局部使用csrf校验

ensure_csrf_cookie  确保生成csrf的cookie     一般加在get请求上，确保生成有效的csrf需要的cookies    #通常在注释掉中间件的情况下，与csrf_protect 装饰器连用  表示需要进行校验，且
```

```python
from django.views.decorators.csrf import csrf_exempt, csrf_protect, ensure_csrf_cookie
from django.utils.decorators import method_decorator
# csrf_exempt,只能加到dispatch方法上
@method_decorator(csrf_exempt, name='dispatch')
class Login(View):
  # 或者
  @method_decorator(csrf_exempt)
  def dispatch(View):
    ret = super().dispatch(request, *args, **kwargs)
    return ret
  # 确保生成csrf校验的cookie
  @method_decorator(ensure_csrf_cookie)
  def get(self, request):
    pass
```

```python
# csrf_exempt，某个视图不需要csrf校验
# scrf_protect,某个视图需要csrf校验(方法没有限制)
# ensure_csrf_cookie，确保生成csrf校验的cookie
class Login(View):
  @method_decorator(scrf_protect)
  def post(View):
    pass
```

##### 2. csrf 功能

1. csrf中间件中执行process_request
   1. 从cookie中获取到scrftoken值
   2. csrftoken的值放入request.META中
2. 执行process_view，执行流程
   1. 查询视图函数是否使用**csrf_exempt**装饰器，若使用则不用csrf校验
   2. 判断请求方式：
      - 如果是（GET，HEAD、OPTIONS，TRACE）**不进行校验**
   3. 其他请求方式(POST,PUT)，进行csrf校验
      1. 获取cookies中的csrftoken值
      2. 获取post请求中的csrfmiddlewaretoken值
         - 能获取到赋值给—> **request_csrf_token**
         - 获取不到—>获取请求头**x-csrftoken**的值赋值给—>  **request_csrf_token**
      3. 比较request_csrf_token和csrf_token两个值，成功则接受请求，否则拒绝

### 43.  ajax简介

##### 1. 什么是json

1. JSON 指的是 JavaScript 对象表示法（JavaScript Object Notation）
2. JSON 是轻量级的文本数据交换格式
3. JSON 独立于语言 
4. JSON 具有自我描述性，更易理解
5. JSON 使用 JavaScript 语法来描述数据对象，但是 JSON 仍然独立于语言和平台。JSON 解析器和 JSON 库支持许多不同的编程语言。

##### 2. XML

json出现之前使用

和json一样是一种数据格式（并没有什么卵用，不重要）

##### 3. ajax

发送请求的方式：地址栏输入url、form表单、a标签、**ajax

##### 4.定义

- ajax：使用js的技术发送和接受请求响应
- 特点：**异步(不用等待响应)**、局部刷新、传输的数据量小
- 基于js的

##### 5. 简介

1. AJAX（Asynchronous Javascript And XML）翻译成中文就是“异步的Javascript和XML”。即使用Javascript语言与服务器进行异步交互，传输的数据为XML（当然，传输的数据不只是XML）。
2. AJAX 不是新的编程语言，而是一种使用现有标准的新方法。
3. AJAX 最大的优点是在不重新加载整个页面的情况下，可以与服务器交换数据并更新部分网页内容。（这一特点给用户的感受是在不知不觉中完成请求和响应过程）
4. AJAX 不需要任何浏览器插件，但需要用户允许JavaScript在浏览器上执行。
   - 同步交互：客户端发出一个请求后，需要等待服务器响应结束后，才能发出第二个请求；
   - 异步交互：客户端发出一个请求后，无需等待服务器响应结束，就可以发出第二个请求。

### 44. ajax示例

##### 1.input数值相加

```python
<input type="text" name="num1">+
<input type="text" name="num2">=
<input type="text" name="num3">
<button type="button" id="b1">计算</button>
<script src="/static/js/jquery.js"></script>
<script>
    $('#b1').click(function () {
        $.ajax({
            url: '/sum/',
            type: 'post',
            data: {
                a: $('[name="num1"]').val(),
                b: $('[name="num2"]').val(),
            },
            success: function (res) {
                $('[name="num3"]').val(res);
            }, error: function (error) {
                console.log(error);
            }
        })
    })
</script>
```

```python
# views.py
def sum(request):
    if request.method == 'POST':
        a = request.POST.get('a')
        b = request.POST.get('b')
        c = int(a) + int(b)
        print(request.POST)
        return HttpResponse(c)
    return render(request, 'sum.html')
```

- 使用json格式发送数据，可以通过JsonResponse方式发送。浏览器会自动序列化和反序列化

```python
hobby:JSON.stringify(['movies', 'reading'])
import json
data = JSON.loads(request.POST.get('hobby'))
print(data)
```

##### 2. 上传文件

```python
// upload.html
<input type='file' id='f1'>
<button id='b1'>上传</button>
$('#b1').click(function(){
  var form_data = new FormData();
  // 只有dom对象有file属性
  form.append('file', document.getElementById('f1').files[0]);
  // 先把jquery对象转为，js对象
  //  form.append('file', $('#f1')[0].files[0]
  
  $.ajax({
    url:'/uplaod/',
    type:'post',
    data:form_data,
    // 按照enctype='mulipart/form-data',如果不写content-type=form表单的第一格式提交
    processData:false,
    // 不让浏览器处理请求头，如果不写默认true，即content-type=true
    contentType:false,
    success:function(res){
      console.log(res)
    }
  });
})
```

 ```python
# views.py
def upload(request):
    if request.method == 'POST':
        data = request.FILES.get('file')
        print(data, type(data),'*'*8, data.name, type(data.name))
        with open(data.name, mode='wb') as f:
            for i in data.chunks():
                f.write(i)
        return HttpResponse('ok')

    return render(request, 'upload.html')
 ```

##### 3. ajax请求通过csrf校验

###### 1.通过csrf条件

1. 确保有csrftoken的cookie

   - 在页面中使用{% csrf_token %}
   - 加装饰器ensure_csrf_token

2. **给data中添加csrfmiddlewaretoken**

   ```python
   $('#b1').click(function () {
           $.ajax({
               url: '/sum/',
               type: 'post',
               data: {
                   'csrfmiddlewaretoken': $('[name="csrfmiddlewaretoken"]').val(),
                   a: $('[name="num1"]').val(),
                   b: $('[name="num2"]').val(),
               },
               success: function (res) {
                   $('[name="num3"]').val(res);
               }, error: function (error) {
                   console.log(error);
               }
   
           })
       });
   ```


###### 2.或者设置请求头

- headers: { 'x-csrftoken': $('[name="csrfmiddlewaretoken"]').val()},

```python
// ajax中的参数；
{% csrf_token %}
$('#b2').click(function () {

        $.ajax({
            url: '/test/',
            type: 'post',
            headers: {
                'x-csrftoken': $('[name="csrfmiddlewaretoken"]').val()
            },
            data: {
                name: 'henry',
                age: 19,
                hobby: JSON.stringify(["movies", "reading"]),
            },
            success: function (res) {
                console.log(res);
            }, error: function (error) {
                console.log(error);
            }

        })
    })
```

###### 3. 使用文件

1.新建js文件

2.script引入即可

###### getCookie方法

```python
function getCookie(name) {
    var cookieValue = null;
    if (document.cookie && document.cookie !== '') {
        var cookies = document.cookie.split(';');
        for (var i = 0; i < cookies.length; i++) {
            var cookie = jQuery.trim(cookies[i]);
            // Does this cookie string begin with the name we want?
            if (cookie.substring(0, name.length + 1) === (name + '=')) {
                cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                break;
            }
        }
    }
    return cookieValue;
}
var csrftoken = getCookie('csrftoken');
```

- 使用$.ajaxSetup()方法为ajax请求统一设置

  ```python
  function csrfSafeMethod(method) {
    // these HTTP methods do not require CSRF protection
    return (/^(GET|HEAD|OPTIONS|TRACE)$/.test(method));
  }
  
  $.ajaxSetup({
    beforeSend: function (xhr, settings) {
      if (!csrfSafeMethod(settings.type) && !this.crossDomain) {
        xhr.setRequestHeader("X-CSRFToken", csrftoken);
      }
    }
  });
  ```


#### Note(3)

1. 如果使用从cookie中取csrftoken的方式，需要确保cookie存在csrftoken值。
2. 如果你的视图渲染的HTML文件中没有包含 {% csrf_token %}，Django可能不会设置CSRFtoken的cookie。
3. 这个时候需要使用ensure_csrf_cookie()装饰器强制设置Cookie。

### 45. form 组件

1. ##### form简单应用

   ###### 1.1普通手写功能

   ```python
   # views.py
   def register(request):
       msg = ''
       if request.method == 'POST':
           username = request.POST.get('username')
           print(username, len(username))
           if len(username) < 6:
               msg = '用户名长度至少6位'
           else:
               # 把用户名和密码写入数据库
               return HttpResponse('注册成功')
       return render(request, 'register.html', {'msg': msg})
   ```

   ```python
   {# register.html #}
   <!DOCTYPE html>
   <html lang="en">
   <head>
       <meta charset="UTF-8">
       <title>Register</title>
   </head>
   <body>
   <form action='/register/' method='post'>
       {% csrf_token %}
       <p>
           <input type='text' name='username' placeholder="用户名" autofocus>{{ msg }}
       </p>
       <p>
           <input type='password' name='pwd' placeholder="密码">
       </p>
       <p>
           <button>提交</button>
       </p>
   </form>
   </body>
   </html>
   ```

   ##### 2. 使用form组件实现

   ```python
   # views.py
   from django import forms
   
   class RegForm(forms.Form):
       username = forms.CharField()
       pwd = forms.CharField()
   def register(request):
       obj = RegForm()
       if request.method == 'POST':
           obj = RegForm(request.POST)
           if obj.is_valid():
               return HttpResponse('注册成功')
       return render(request, 'register.html', {'obj': obj})
   ```
   
```python
   {# register.html #}
   {# form表单中的novalidate属性表示浏览器不进行校验 #}
   <form action="/register/" method="post" novalidate>
       {% csrf_token %}
       {{ obj.as_p }}  {# 使用默认方式生成input和label标签 #}
           
     	{# 指定label的值 #}
       <p>
           <label for="{{ obj.username.id_for_label }}">用户名:</label>
           {{ obj.username }} {{ obj.username.errors.0 }}
       </p>
       <p>
           <label for="{{ obj.pwd.id_for_label }}">密码:</label>
           {{ obj.pwd }} {{ obj.pwd.errors.0 }}
       </p>
     	{{ obj.errors }}
       <button>提交</button>
       </p>
   </form>
```

form  表单的功能

- 前端页面是form类的对象生成的                                      -->生成HTML标签功能
   - 当用户名和密码输入为空或输错之后 页面都会提示        -->用户提交校验功能
   - 当用户输错之后 再次输入 上次的内容还保留在input框   -->保留上次输入内容

### 46.  form组件字段与插件

- 创建Form类时，主要涉及到 【字段】 和 【插件】，**字段用于对用户请求数据的验证**，**插件用于自动生成**HTML;
- **常用字段**
  - CharField
  - ChoiceField
  - MultipleChoiceField

##### 2.1 initial

- 初始值，input框中的默认值

```python
from django import forms
class RegForm(forms.Form):
   username = forms.CharField(
     min_length=6,
     # 给username字段设置默认值
     label = '用户名',
     initial = 'henry',
   )
    pwd = forms.CharField(min_length=6, label='密码')
```

##### 2.2 error_messages

重写错误信息

```python
from django import forms
class RegForm(forms.Form):
   username = forms.CharField(
     min_length=6,
     # 给username字段设置默认值
     label = '用户名',
     initial = 'henry',
     error_messages = {
       'required': '不能为空',
       'invalid': '格式有误',
       'min_length': '用户名最短6位'
     }
   )
    pwd = forms.CharField(min_length=6, label='密码')
```

##### 2.3 password

```python
from django import forms
class RegForm(forms.Form):
    pwd = forms.CharField(
      min_length=6, 
      label='密码',
      # 表示输入密码时，为密文显示
      widget = forms.widgets.PasswordInput,
    )
```

##### 2.4 radioSelect

- 单radio值为字符串，单选点击框
- **生成ul标签**

```python
from django import forms
class RegForm(forms.Form):
   username = forms.CharField(
     min_length=6,
     # 给username字段设置默认值
     label = '用户名',
     initial = 'henry',
     error_messages = {
       'required': '不能为空',
       'invalid': '格式有误',
       'min_length': '用户名最短6位'
     }
   )
  pwd = forms.CharField(min_length=6, label='密码',)
	gender = forms.fields.ChoiceField(
  	choices=((0, 'female'), (1, 'male'), (3, 'secret')),
    label = '性别',
    initial = 3,
    widget = forms.widgets.RadioSelect()
  )

```

##### 2.5 单选select

- 单位下拉框

  ```python
  from django import forms
  class RegForm(forms.Form):
    ...
    hobby = forms.ChoiceField(
    	choices = ((1, 'travelling'), (2, 'reading'), (3, 'listening'),),
      label = '爱好',
      initial = 3,
      widget=forms.widgets.Select(),
    )
  ```


##### 2.6 多选select

```python
from django import forms
class RegForm(forms.Form):
  ...
  hobby = forms.MultipleChoiceField(
          choices=(('1', 'travelling'), ('2', 'reading'), ('3', 'listening'),),
          label='爱好',
          initial=['3'],
          widget=forms.widgets.SelectMultiple(),
      )
```

##### 2.7 单选checkbox

```python
from django import forms
class RegForm(forms.Form):
  ...
  keep = forms.ChoiceField(
  	label = '是否记住密码',
    initial = 'checked',
    widget=forms.widgets.CheckboxInput(),
      )
```

##### 2.8 多选 checkbox

```python
from django import forms
class RegForm(forms.Form):
  ...
   hobby = forms.fields.MultipleChoiceField(
       choices=((1, 'travelling'), (2, 'reading'), (3, 'listening'),),
        label="爱好",
        initial=[1, 3],
        widget=forms.widgets.CheckboxSelectMultiple()
    )
```

##### 关于choice的注意事项

1. 在使用选择标签时，需要注意choices的选项可以从数据库中获取，但是由于是静态字段 **获取的值无法实时更新**，那么需要自定义构造方法从而达到此目的。

##### 2.9 自定义

- 使用MutipleChoiceField

  ```python
  from django import forms
  hobby = forms.MutipleChoiceField(
    # 从数据库中读取
  	choices=models.Hobby.objects.all().values_list('pk', 'name')
  )
  ```

- 使用ModelChoiceField

  ```python
  from django import forms
  hobby = forms.ModelChoiceField(
    # 从数据库中读取
  	queryset=models.Hobby.objects.all()
  )
  ```

- 刷新页面更新数据

  ```python
  class RegForm(forms.Form):
    def __init__(self, *args, **kwargs):
      super(RegForm, self).__init__( *args, **kwargs):
      	self.fields['hobby'].choices = models.Hobby.objects.values_list()
      hobby = forms.ModelChoiceField(
      # 从数据库中读取
      queryset=models.Hobby.objects.all()
    	)
  ```


### 47. 校验

#### 3.1 内置校验

```python
from django.forms improt Form
from django.core.validators import RegexValidator

class MyForm(From):
  phone = forms.CharField(
    # 正则校验器中，第二个参数是提示信息
    validators=[RegexValidator(r'1[3-9]\d{9}$', '手机号不合法')]
    )
```

#### 3.2 自定义校验

```python
from django.core.exceptions import ValidationError
def checkname(value):
  if 'o' in value:
    rasie ValidationError('用户名不合法')
    
class RegForm(forms.Form):
   username = forms.CharField(
     min_length=6,
     # 给username字段设置默认值
     label = '用户名',
     initial = 'henry',
     validators = [checkname,...]
    )
    pwd = forms.CharField(
    	widget = forms.widgets.PasswordInput(),
    )
```

#### 3.3 钩子

- 局部钩子

```python
class RegForm(forms.Form):  
  username = forms.CharField(label='用户名')
  def clean_username(self):
        if 'o' in self.cleaned_data.get('username'):
            raise ValidationError('用户名不合法。。。。。')
```

- 全局钩子

```python
class RegForm(forms.Form):  
  pwd = forms.CharField(
        label='密码',
        widget=forms.widgets.PasswordInput,
    )
  re_pwd = forms.CharField(
        label='密码',
        widget=forms.widgets.PasswordInput,
    )
  def clean(self):
    if not self.cleaned_data.get('pwd') == self.cleaned_data.get('re_pwd'):
      self.add_error('re_pwd','两次密码不一致')
      raise ValidationError('两次密码不一致')
```

#### 3.4 批量添加样式

```python
class LoginForm(forms.Form):
    username = forms.CharField(
        min_length=5,
        label="用户名",
        initial="henry",
        error_messages={
            "required": "不能为空",
            "invalid": "格式错误",
            "min_length": "用户名最短5位"
        }
    ...

    def __init__(self, *args, **kwargs):
        super(LoginForm, self).__init__(*args, **kwargs)
        for field in iter(self.fields):
            self.fields[field].widget.attrs.update({
                'class': 'form-control'
            })
```

#### 3.5 ModelForm !!!!!!!!!!!!!!!!!!!!!!!!!

```python
class BookForm(forms.ModelForm):

    class Meta:
        model = models.Book
        fields = "__all__"
        labels = {
            "title": "书名",
            "price": "价格"
        }
        widgets = {
            "password": forms.widgets.PasswordInput(attrs={"class": "c1"}),
        }
```

```python
model = models.Student  # 对应的Model中的类
fields = "__all__"      # 字段，如果是__all__,就是表示列出所有的字段
exclude = None          # 排除的字段
labels = None           # 提示信息
help_texts = None       # 帮助提示信息
widgets = None          # 自定义插件
error_messages = None   # 自定义错误信息
```

#### 3.6 is_valid执行流程

1. 执行full_clean方法
   1. 定义错误字典
   2. 存放清洗过数据的字典
2. 执行clean_field方法，
   1. 循环所有字段，获取当前字段值，
   2. 对值校验(内置和自定义校验)
      - 通过校验self.clean_data[name] = value
        1. 如果有局部钩子，就要执行校验
        2. 通过则，self.clean_data[name] = value
        3. 不通过，self._errors添加当前字段错误，并且删除：del self.clean_data[name]
      - 没有通过self._errors添加当前字段错误
   3. 执行全局钩子clean方法

### 48. 中间件

<https://www.cnblogs.com/maple-shaw/articles/9333824.html>

###### 1. 定义

中间件是处理django的请求和响应的框架级别的钩子，本质上就是一个类。

###### 2. 中间件可以定义的5个方法

- process_request(self,request)
- process_view(self, request, view_func, view_args, view_kwargs)
- process_template_response(self,request,response)
- process_exception(self, request, exception)
- process_response(self, request, response)

###### 3. 方法中的4个特征

执行时间，执行顺序，参数，返回值

###### 4. process_request

```python
执行时间：视图函数之前  
参数 ：request——》和视图函数中是同一个request对象
执行顺序： 按照注册的顺序   顺序执行
返回值：
​	none ：正常流程
​	Httpresponse  ：后面的中间的process_request  函数视图都不执行，直接执行当前中间件中的process_reponse方法，倒叙执行之前的中间件中的process_response方法。
```

###### 5. process_response

```python
执行时间：视图函数之后
参数：
​	requesst——》和视图函数中是同一个request对象
​	response——》返回给浏览器响应的对象
执行顺序：按照注册的顺序  倒叙执行
返回值：
​	HttpResponse：  必须返回response对象
```

###### 6. process_view

```python
执行时间：视图函数之前，process_request之后
参数：
​	request——》和视图函数中是同一个request对象
​	view_func——》 视图函数
​	view_args——》 视图函数的位置参数
​	view_kwargs——》视图函数的关键字参数

执行顺序：按照注册的顺序  顺序执行
返回值：
​	None：正常流程
​	HttpResponse： 后面的中间的process_view   视图函数都不执行，直接执行中间件中的最后一个process_response方法，倒叙执行之前的中间的process_response方法。
```

###### 7.   process_exception

```python
执行时间（除法条件）:视图层面有错时才执行
参数：
​	request——》和视图函数中是同一个request对象
​	exception——错误对象	

执行顺序：按照注册的顺序  倒叙执行
返回值：
​	None： 交给下一个中间件 '取'  处理异常，都没有处理交由django处理异常
​	HttpResponse ：  后面的中间j件的process_exception方法都不执行，直接执行最后一个中间件的
process_response方法，倒叙执行执行之前的中间件中process_response方法
```

###### 8. process_template_response

```python
执行时间(触发条件)：视图返回的是一个templateResponse对象
参数：
​	request——》 和视图函数中是同一个request对象
​	response——》templateResponse对象

执行顺序：按照注册的顺序  倒叙执行

返回值：
​	HttpResponse：必须返回response对象
```

### 49. django请求的生命周期（流程顺序）

![](E:\homework2\img\1561783661582.png)

### 50. 终版CRM数据结构

```python
字典           menu id做key   {  第一层的  title  icon   children  { 第二层的title   url   id } }
权限        反解析的name做key  { 反解析name：{ url，title ，pid ，pname，id}}
```

### 51. django-debug-toolbar       debug调试工具,开发测试可用

<https://www.cnblogs.com/maple-shaw/articles/7808910.html>

### 52. 缓存

###### 1. 缓存到内存配置

```
# 内存
CACHES = {
    'default': {
        'BACKEND': 'django.core.cache.backends.locmem.LocMemCache',
        'LOCATION': 'unique-snowflake',
        'TIMEOUT': 300,  # 缓存超时时间（默认300，None表示永不过期，0表示立即过期）
        'OPTIONS': {
            'MAX_ENTRIES': 100,  # 最大缓存个数（默认300）
            'CULL_FREQUENCY': 3,  # 缓存到达最大个数之后，剔除缓存个数的比例，即：1/CULL_FREQUENCY（默认3）
        },
    }
}
```

- 应用

- 缓存视图

```Python
from django.views.decorators.cache import cache_page

@cache_page(5)
def student_list(request, *args, **kwargs):
    students = models.Student.objects.all()
    print('students')
    return render(request, 'student_list.html', {'students': students})
```

###### 2. 全栈缓存

```
MIDDLEWARE = [
    'django.middleware.cache.UpdateCacheMiddleware',
		....
    'django.middleware.cache.FetchFromCacheMiddleware',
]
```

![1568689511987](D:/老男孩视频/django串讲/django串讲/day02/asset/1568689511987.png)

###### 3. 局部缓存

```
{% load cache %}
{% cache 5 'xxx' %}

    缓存
    {{ now }}
{% endcache %}
```

###### 4. 缓存使用redis

<https://django-redis-chs.readthedocs.io/zh_CN/latest/>

- 下载django-redis

```
pip install django-redis
```

```
CACHES = {
    "default": {
        "BACKEND": "django_redis.cache.RedisCache",
        "LOCATION": "redis://127.0.0.1:6379/1",
        "OPTIONS": {
            "CLIENT_CLASS": "django_redis.client.DefaultClient",
        }
    }
}
```

```
SESSION_ENGINE = "django.contrib.sessions.backends.cache"
SESSION_CACHE_ALIAS = "default"
```

### 53. 信号

###### 1. 内置的信号

```python
 from django.core.signals import request_finished   #请求结束后，自动触发
    from django.core.signals import request_started  # 请求到来前，自动触发
    from django.core.signals import got_request_exception # 请求异常后，自动触发

    from django.db.models.signals import class_prepared # 程序启动时，检测已注册的app中modal类，对于每一个类，自动触发
    from django.db.models.signals import pre_init, post_init
    	#pre_init          # django的model执行其构造方法前，自动触发
    	#post_init         # django的model执行其构造方法后，自动触发

    from django.db.models.signals import pre_save, post_save
    # pre_save     # django的model对象保存前，自动触发
    #post_save     # django的model对象保存后，自动触发
   
    from django.db.models.signals import pre_delete, post_delete
    #pre_delete      django的model对象删除前，自动触发
    #post_delete     django的model对象删除后，自动触发
    from django.db.models.signals import m2m_changed
    # django的model中使用m2m字段操作第三张表（add,remove,clear）前后，自动触发
    from django.db.models.signals import pre_migrate, post_migrate
	# pre_migrate        # 执行migrate命令前，自动触发
	# post_migrate         # 执行migrate命令后，自动触发
    from django.test.signals import setting_changed
    	# 使用test测试修改配置文件时，自动触发
    from django.test.signals import template_rendered
		# 使用test测试渲染模板时，自动触发	
    from django.db.backends.signals import connection_created
    	# 创建数据库连接时，自动触发
```

2. ###### 注册信号

```python
2种方法
from django.db.models.signals import pre_save, post_save

def callback(sender, **kwargs):
    print("xxoo_callback")
    print(sender, kwargs)    #sender<class'django.core.handlers.wsgi.WSGIHandler'>  固定参数
post_save.connect(callback)

from django.dispatch import receiver
@receiver(post_save)
def callback1(sender, **kwargs):
    print("xxoo_callback1111111")
    print(sender, kwargs)
```

### 54.  orm性能相关

1. 尽量不查对象,能用values()

2. select_related('classes')     连表查询    多对一   一对一8

   ```python
   
   ```

3. prefetch_related('classes')    子查询    多对一   多对多

   ```python
   
   ```

4. only('name')   指定某些字段     defer  指定排除某些字段  

   queryset 特性

###### 1. 多个数据库

- 配置

```
DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': os.path.join(BASE_DIR, 'db.sqlite3'),
    },
    'db2': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': os.path.join(BASE_DIR, 'db2.sqlite3'),
    },
}
```

###### 2. 迁移其他的数据库

python manage.py migrate --database db2

###### 3. 读写分离

- 手动

```
models.Student.objects.using('db2').all()

obj = models.Student.objects.using('db2').get(name='zhazha')
obj.name = 'star'
obj.save(using='default')
```

- 自动

settings配置:

```
DATABASE_ROUTERS = ['myrouter.Router']
```

```
class Router:
    """
    读写分离
    
    """
    def db_for_write(self, model, **kwargs):
        return 'db2'

    def db_for_read(self, model, **kwargs):
        return 'default'
```

- 一主多从

```
class Router:
    """
    一主多从
    """
    def db_for_write(self, model, **kwargs):
        return 'db1'
        
    def db_for_read(self, model, **kwargs):
        return random.choices['db2', 'db3', 'db4']
```

###### 3. 分库分表

```Python
class Router:
    """
    分库分表

    app01  model   db1
    app02  model   db2
    """
    def db_for_write(self, model, **kwargs):
        app_name = model._meta.app_label
        if app_name == 'app01':
            return 'db1'
        elif app_name == 'app02':
            return 'db2'

    def db_for_read(self, model, **kwargs):
        app_name = model._meta.app_label
        if app_name == 'app01':
            return 'db1'
        elif app_name == 'app02':
            return 'db2'
```

### 55. django执行原生SQL的方法

```Python
# 1. extra
ret = models.Student.objects.all().extra(where=['id > %s'], params=['1'], order_by=['-id'])
# print(ret)
# for i in ret:
#     print(i)

# 2. raw
ret = models.Student.objects.raw('select * from main.app01_classes where id <= 2')
print(ret)
for i in ret:
    print(i.name)

# 3 connection
from django.db import connection, connections

# cursor = connection.cursor()
cursor = connections['db2'].cursor()
cursor.execute("""SELECT * from main.app01_classes where id > %s""", [1])
row = cursor.fetchall()
print(row)
```

### 56. crm+权限

crm

客户关系管理系统 

功能:

- 销售
  - 客户的信息
    - 公户和私户
  - 跟进记录的管理
  - 报名表的管理 
  - 缴费记录的管理
- 班主任
  - 班级管理
  - 课程记录的管理
  - 学生学习记录的管理

公户和私户

避免抢单的情况

有没有现成的CRM系统?  

<http://www.xuebangsoft.com/pro_boss.html>   学邦

自己开发  可定制化程度比较高

项目用了多少张表?

- crm  (10张)  
- 6张表(4个model)

权限

如何实现权限控制?

- 在web开发中,url地址当做权限  
- 表结构的设计
- 登录成功后保存用户的权限  + 中间件 校验用户的权限

技术点

- 登录成功后保存权限信息

  - 查询  跨表查询   去空   去重
  - session
    - json的序列化
    - 字典的key 是数字  序列化之后是字符串

- 中间件

  - url     request.path_info

  - request.current_men_id = None   # 当前展示的二级菜单id

  - request.breadcrumb_list = [  { url:'/index/','title':'首页'  } ]

  - 白名单

    - settings   
    - re  正则匹配

  - 登录状态的校验

  - 免认证的校验

  - 权限的校验

    - 从session中获取权限的信息
    - for 循环进行校验
      - 判断 有没有 pid
        - 有pid 当前权限是子权限
          - request.current_men_id = pid
          - request.breadcrumb_list.append({ p_url  p_title   })  #  从权限字典中找父权限信息
          - request.breadcrumb_list.append({ url  title   })
        - 没有pid 当前权限是父权限  二级菜单
          - request.current_men_id = id
          - request.breadcrumb_list.append({ url  title   })

    

  - 最后返回HttpResponse('没有权限')

- 动态生成菜单

  - inclusion_tag

  - 排序   有序字典   sorted 

  - ```
    m['class'] = 'active'   # 选中
    i['class'] = ''   # 一级菜单展开
    ```

- 路径导航

  - inclusion_tag

- 权限控制到按钮级别

  - filter
  - if    name   in  permision_dict

表结构

```Python
# 简单权限控制
class Permission(models.Model):
    url = models.CharField(max_length=64, verbose_name='权限')

class Role(models.Model):
    name = models.CharField(max_length=32, verbose_name='角色')
    permissions = models.ManyToManyField('Permission')

class User(models.Model):
    name = models.CharField(max_length=32, verbose_name='用户名')
    pwd = models.CharField(max_length=32, verbose_name='密码')
    roles = models.ManyToManyField('Role')
    
# 生成一级菜单
class Permission(models.Model):
    url = models.CharField(max_length=64, verbose_name='权限')
    title = models.CharField(max_length=32, verbose_name='标题')
    icon = models.CharField(max_length=32, verbose_name='图标')
    is_menu = models.BooleanField(default=False, verbose_name='是否是菜单')

class Role(models.Model):
    name = models.CharField(max_length=32, verbose_name='角色')
    permissions = models.ManyToManyField('Permission')

class User(models.Model):
    name = models.CharField(max_length=32, verbose_name='用户名')
    pwd = models.CharField(max_length=32, verbose_name='密码')
    roles = models.ManyToManyField('Role')   
    
# 生成二级菜单
class Menu(models.Model):
    """一级菜单"""
    title = models.CharField(max_length=32, verbose_name='标题')
    icon = models.CharField(max_length=32, verbose_name='标题')
    weight = models.IntegerField(default=1)

class Permission(models.Model):
    url = models.CharField(max_length=64, verbose_name='权限')
    title = models.CharField(max_length=32, verbose_name='标题')
    menu = models.ForeignKey(Menu, null=True, blank=True)  # menu_id 

class Role(models.Model):
    name = models.CharField(max_length=32, verbose_name='角色')
    permissions = models.ManyToManyField('Permission')

class User(models.Model):
    name = models.CharField(max_length=32, verbose_name='用户名')
    pwd = models.CharField(max_length=32, verbose_name='密码')
    roles = models.ManyToManyField('Role')
    
#  非菜单权限归属
class Menu(models.Model):
    """一级菜单"""
    title = models.CharField(max_length=32, verbose_name='标题')
    icon = models.CharField(max_length=32, verbose_name='标题')

class Permission(models.Model):
    """
    有parent_id  当前的权限就是子权限
    有menu_id     当前的权限就是父权限  二级菜单 
    """
    url = models.CharField(max_length=64, verbose_name='权限')
    title = models.CharField(max_length=32, verbose_name='标题')
    menu = models.ForeignKey(Menu, null=True, blank=True)  # menu_id
    parent = models.ForeignKey('self', null=True, blank=True)  # parent_id

class Role(models.Model):
    name = models.CharField(max_length=32, verbose_name='角色')
    permissions = models.ManyToManyField('Permission')

class User(models.Model):
    name = models.CharField(max_length=32, verbose_name='用户名')
    pwd = models.CharField(max_length=32, verbose_name='密码')
    roles = models.ManyToManyField('Role')
        
#  权限控制到按钮级别
class Menu(models.Model):
    """一级菜单"""
    title = models.CharField(max_length=32, verbose_name='标题')
    icon = models.CharField(max_length=32, verbose_name='标题')

class Permission(models.Model):
    """
    有parent_id  当前的权限就是子权限
    有menu_id     当前的权限就是父权限  二级菜单
    """
    url = models.CharField(max_length=64, verbose_name='权限')
    title = models.CharField(max_length=32, verbose_name='标题')
    name = models.CharField(max_length=32, unique=True, verbose_name='URL的别名')
    menu = models.ForeignKey(Menu, null=True, blank=True)  # menu_id
    parent = models.ForeignKey('self', null=True, blank=True)  # parent_id

class Role(models.Model):
    name = models.CharField(max_length=32, verbose_name='角色')
    permissions = models.ManyToManyField('Permission')

class User(models.Model):
    name = models.CharField(max_length=32, verbose_name='用户名')
    pwd = models.CharField(max_length=32, verbose_name='密码')
    roles = models.ManyToManyField('Role')
```

数据结构

```python
# 简单权限控制
permissions = [ {'permissions__url':'xxxxxx'} ,{'permissions__url':'xxxxxx'}   ]

# 生成一级菜单
permission_list = [ {'url':'xxxxx'}  ]
menu_list= [{'url','title' ,'icon'}]

# 生成二级菜单
permission_list = [ {'url':'xxxxx'}  ]
menu_dict ={
    一级菜单的id : {  
			title : 'xxx',
        	icon:'xxxx',
        	weight:1,
        	chilidern: [
                { url   title  }
			]
    	}	
}

# 非菜单权限归属
permission_list = [ {'url':'xxxxx','pid','id'}  ]
menu_dict ={
    一级菜单的id : {  
			title : 'xxx',
        	icon:'xxxx',
        	weight:1,
        	chilidern: [
                { url   title  id  }
			]
       }	
}

# 路径导航
permission_dict = { id :  {'url':'xxxxx','pid','id','title'}  }
menu_dict ={
    一级菜单的id : {  
			title : 'xxx',
        	icon:'xxxx',
        	weight:1,
        	chilidern: [
                { url   title  id  }
			]
       }	
}

# 权限控制到按钮级别
permission_dict = { name :{'url':'xxxxx','pid','id','title','p_name'}  }
menu_dict ={
    一级菜单的id : {  
			title : 'xxx',
        	icon:'xxxx',
        	weight:1,
        	chilidern: [
                { url   title  id  }
			]
       }	
}
```

用户的权限变更后,不重新登录,怎么应用最新的权限?

1. 用户表加字段  session_key
2. 查询用户的session数据  把最新的权限保存进去

```Python
from django.contrib.sessions.backends.db import SessionStore
from django.contrib.sessions.models import Session  # session表
ret = Session.objects.filter(session_key='ld5w252hf6b6pe72y2b0cmc9yh6kdyen').first()
print(ret.session_data)

ret = request.session.decode(ret.session_data)   # 解密数据
print(ret,type(ret))
ret = request.session.encode(ret)   # 加密数据
print(ret,type(ret))
```

权限控制到按钮级别,怎么控制到行级别?

![1568713539344](D:/老男孩视频/django串讲/django串讲/day02/asset/1568713539344.png)

### 57. 你在开发中遇到什么印象深刻的问题?

- json序列化
- 表结构的变换
- 功能上的取舍
