# 1. 不共享，通过文件 或者队列实现通讯
''''''
'''
from multiprocessing import Process,Queue
def func(q):
    a = 1*2*3*4*5
    q.put(a)
if __name__ == '__main__':
    q = Queue()
    p = Process(target=func,args=(q,))
    p.start()
    print(q.get())
'''
# 2.-----------------------------------------------------------
# 队列特点先进先出，数据安全，可以实现进程之间的通讯
# 主进程将 队列的对象传入子进程要执行的函数，通过子进程队列的get方法传值，主进程通过get方法收值

# 就绪 ---运行----io操作或时间片结束进入阻塞 ----到就绪排队--------然后运行---------结束
# 3.
# 进程感觉应该是个排长级别的官，手底下只有最小的兵（线程）可一指挥分配，最小的资源分配单位
import socket
from multiprocessing import Process
def server(conn):

    while True:

        msg = conn.recv(1024).decode('utf-8')
        print(msg)
        if msg.upper() == 'Q':
            break
        a = '123156'
        conn.send(a.encode('utf-8'))
        if a.upper() == 'Q':
            break
    conn.close()
    sk.close()
if __name__ == '__main__':

    sk = socket.socket()
    sk.bind(('127.0.0.1',9000))
    sk.listen()

    while True:
        conn, addr = sk.accept()
        p = Process(target=server,args=((conn,)))
        p.start()
