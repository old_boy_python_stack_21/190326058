# 正则表达式练习
# 1、匹配一篇英文文章的标题 类似 The Voice Of China
''''''
'''
([A-Z][a-z]+\s)*
'''
# 2、匹配一个网址
    # 类似 https://www.baidu.com http://www.cnblogs.com
'''
(https|http):.*?com    
'''
# 3、匹配年月日日期 类似 2018-12-06 2018/12/06 2018.12.06
'''
\d+(?P<name>[^\da-zA-Z])(0[1-9]|1[0-2])(?p = name)([0-2]\d|3[0-1])
'''
import re
# ret = re.compile('\d+?(?P<name>[^\da-zA-Z])(?:0[1-9]|1[0-2])(?P=name)(?:[0-2][1-9]|3[0-1])')
# a = ret.search('2018-12-06 2018/12/06 2018.12.06')
# print(a.group())
# 4、匹配15位或者18位身份证号-
'''
1[1-3](\d|\d{4})(19\d{2}|20[0-1]\d)(0\d|1[0-2])([0-2][0-9]|3[0-1])\d{4}
'''

# 5、从lianjia.html中匹配出标题，户型和面积，结果如下：
# [('金台路交通部部委楼南北大三居带客厅   单位自持物业', '3室1厅', '91.22平米'), ('西山枫林 高楼层南向两居 户型方正 采光好', '2室1厅', '94.14平米')]

# with open(r'C:\Users\Administrator\Desktop\lianjia.html','r',encoding='utf-8') as f1:
#     page = f1.read()
#     ret = re.compile('data-is_focus="1" data-sl="">(.*?)</a>.*?<span class="divide">/</span>(.*?)<span class="divide">/</span>(.*?)<span',re.S)
#     a=ret.findall(page)
#     print(a)



# 6、1-2*((60-30+(-40/5)*(9-2*5/3+7/3*99/4*2998+10*568/14))-(-4*3)/(16-3*2))
# 从上面算式中匹配出最内层小括号以及小括号内的表达式
import re
ret = re.compile('\([^\(\)]*?\)')
a = '1-2*((60-30+(-40*5)*(9-2*5/3+7/3*99/4*2998+10*568/14))-(-4*3)/(16-3*2))'
lis = [a]
def func(con):
    if '*' in con:
        print(con)
        la = re.search('(-?\d+(\.\d+)?)\*(\d+(\.\d+)?)',con)
        la = la.group()
        print(la)
        a,b = re.split('\*',la)
        new_con = str(int(a)*int(b))
        print(new_con)
        con.replace(new_con,la)
        print(con)
    elif '/' in con:
        la = re.search('(-?\d+(\.\d+)?)/(\d+(\.\d+)?)', con)
        la = la.group()
        a, b = re.split('/', la)
        new_con = int(a) / int(b)
        print(new_con)
while True:
    if ret.search(lis[-1]):
        con = ret.search(lis[-1])
        con = con.group()
        print(con)
        func(con)
    #     a = lis.pop()
    #     a.replace('con')
    #     print(con)
    # else:
        break



# 7、从类似9-2*5/3+7/3*99/4*2998+10*568/14的表达式中匹配出乘法或除法
# a = '9-2*5/3+7/3*99/4*2998+10*568/14'
# s1 = re.findall('\d+\*\d+',a)
# s2 = re.findall('\d+/\d+',a)
# for i in s1:
#     print(i)
# for i in s2:
#     print(i)


# 8、通读博客，完成三级菜单
# http://www.cnblogs.com/Eva-J/articles/7205734.html


# 9、大作业：计算器