#!/usr/bin/env python
# -*- coding:utf-8 -*-

# 1.计算任意一个文件夹的大小（考虑绝对路径的问题）
    # 基础需求
        # 这个文件夹中只有文件
    # 进阶需求
#         # 这个文件夹中可能有文件夹，并且文件夹中还可能有文件夹...不知道有多少层
''''''
'''
import os
def Get_Dir_Size(path):
    size = 0
    for root, dirs, files in os.walk(path):
        size += sum([os.path.getsize(os.path.join(root, name)) for name in files])
    return size

count = Get_Dir_Size(r'C:\Users\Administrator\PycharmProjects\xuexi\计算机进制')

print(count)
'''
# 2.校验大文件的一致性
    # 基础需求
        # 普通的文字文件
    # 进阶需求
#         # 视频或者是图片
'''
import hashlib
def get_md5(data):
    obj = hashlib.md5("asdagfgdfgd".encode('utf-8'))
    obj.update(data.encode('utf-8'))
    result = obj.hexdigest()
    return result
with open(r'E:\思维导图\图片.png', 'rb')as f1:
    md5 = hashlib.md5()
    while True:
        a = f1.read(10000)
        md5.update(a)
        if not a:
            break
    s = md5.hexdigest()
with open(r'E:\思维导图\图片1.png', 'rb')as f1:
    md5 = hashlib.md5()
    while True:
        a = f1.read(10000)
        md5.update(a)
        if not a:
            break
    b = md5.hexdigest()
if s == b:
    print('相同')
'''
# 3.发红包
    # 每一个人能够抢到的金额的概率都是平均的
    # 小数的不准确性的问题
'''
import random
def func(much,num):
    for i in range(num):
        if num == 1:
            yield much
        start = much // num * 0.8
        end = much //num * 1.2
        number = round(random.uniform(end,start),1)
        much = much - number
        num -= 1
        yield number
        func(much,num)

a = func(100,10)
for i in a:
    print(i)
'''