from  lib.log_n import logger
from config.config_n import ARTICLE_LIST
from src.dianzanpinglun import article_comment,article_up
from src.login_n import login
def article_list():
    """
    文章列表
    :return:
    """
    while True:
        try:
            print('=================== 文章列表 ===================')
            for i in range(len(ARTICLE_LIST)):
                row = ARTICLE_LIST[i]
                msg = """%s.%s \n  赞(%s) 评论(%s)\n""" % (i + 1, row['title'], row['up'], len(row['comment']))
                print(msg)
            choice = input('请选择要查看的文章(N返回上一级)：')
            if choice.upper() == 'N':
                return
            choice = int(choice)
            choice_row_dict = ARTICLE_LIST[choice - 1]
            article_detail(choice_row_dict)
        except Exception as e:
            msg = str(e)
            logger.error(msg, exc_info=True)


def article_detail(row_dict):
    """
    文章详细
    :param row_dict:
    :return:
    """
    try:
        show_article_detail(row_dict)
        func_dict = {'1': article_up, '2': article_comment}
        while True:
            print('1.赞；2.评论；')
            choice = input('请选择（N返回上一级）：')
            if choice.upper() == 'N':
                return
            func = func_dict.get(choice)
            if not func:
                print('选择错误，请重新输入。')
            result = func(row_dict)
            if result:
                show_article_detail(row_dict)
                continue

            print('用户未登录，请登录后再进行点赞和评论。')
            to_login = input('是否进行登录？yes/no：')
            if to_login == 'yes':
                login()
    except NotImplementedError as e:
        msg = str(e)
        logger.error(msg,exc_info=True)
def show_article_detail(row_dict):
    try:
        print('=================== 文章详细 ===================')
        msg = '%s\n%s\n赞(%s) 评论(%s)' % (row_dict['title'], row_dict['content'], row_dict['up'], len(row_dict['comment']))
        print(msg)
        if len(row_dict['comment']):
            print('评论列表(%s)' % len(row_dict['comment']))
            for item in row_dict['comment']:
                comment = "    %s - %s" % (item['data'], item['user'])
                print(comment)
    except NotImplementedError as e:
        msg = str(e)
        logger.error(msg,exc_info=True)