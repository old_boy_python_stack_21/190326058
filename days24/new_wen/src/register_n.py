from config import config_n
from lib.md5_pasword import encrypt_md5
from db.user_obj import USER_DICT
def register():
    """
    用户注册
    :return:
    """
    print('用户注册')
    while True:
        user = input('请输入用户名(N返回上一级)：')
        if user.upper() == 'N':
            return
        pwd = input('请输入密码：')
        if user in config_n.USER_DICT:
            print('用户已经存在，请重新输入。')
            continue
        config_n.USER_DICT[user] = encrypt_md5(pwd)
        print('%s 注册成功' % user)
        print(USER_DICT)