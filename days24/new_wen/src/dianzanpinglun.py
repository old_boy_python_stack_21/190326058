from lib.auth_n import auth
from src import login_n
from lib.log_n import logger
@auth
def article_up(row_dict):
    """
    点赞文章
    :param row_dict:
    :return:
    """
    row_dict['up'] += 1
    print('点赞成功')
    return True


@auth
def article_comment(row_dict):
    """
    评论文章
    :param row_dict:
    :return:
    """

    try:
        while True:
            comment = input('请输入评论（N返回上一级）：')
            if comment.upper() == 'N':
                return True
            row_dict['comment'].append({'data': comment, 'user': login_n.CURRENT_USER})
            print('评论成功')
    except NotImplementedError as e:
        msg = str(e)
        logger.error(msg,exc_info=True)
