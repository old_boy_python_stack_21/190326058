from lib.log_n import logger
from src.login_n import login
from src.register_n import register
from src.new_word import article_list
import logging
def run():
    """
    主函数
    :return:
    """
    try:
        print('=================== 系统首页 ===================')
        func_dict = {'1': register, '2': login, '3': article_list}
        while True:
            print('1.注册；2.登录；3.文章列表')
            choice = input('请选择序号：')
            if choice.upper() == 'N':
                return
            func = func_dict.get(choice)
            if not func:
                print('序号选择错误')
                continue
            func()
    except NotImplementedError as e:
        msg = str(e)
        logger.error(msg,exc_info=True)
