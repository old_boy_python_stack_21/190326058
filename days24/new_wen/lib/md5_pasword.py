import hashlib
def encrypt_md5(arg):
    """
    md5加密
    :param arg:
    :return:
    """
    m = hashlib.md5()
    m.update(arg.encode('utf-8'))
    return m.hexdigest()