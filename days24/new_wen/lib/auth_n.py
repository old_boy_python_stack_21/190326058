from src import login_n
def auth(func):
    def inner(*args, **kwargs):
        if login_n.CURRENT_USER:
            return func(*args, **kwargs)
        return False

    return inner