# PYTHON 学习笔记完整版

# 人-软件-解释器-系统-硬件

操作系统

windows要钱  能打游戏  gbk

linux  效率高不要钱  不能打游戏   utf-8  图形界面垃圾

mac   适用于办公  utf-8

# 1，编码

1. Unicode  万国码    32位  4字节          所有的都能表示出来还有剩余
2. ascii     8位4字节        只能表示英文字母 数字下划线
3. utf-8    万国码的压缩版    从左到右每8位为0就舍去  汉字占3字符
4. gbk   汉字占2字符

# 2. 解释器

1. 在linux上有种特别的调用方式

   ./a.py

   遇到这个就会去头文件  找解释器路径

   头文件 #！udasdoi/asjdlak/asldja/ljal  python36

# 3. 输出输入

1. print（''）
2. input(' ')

# 4. 数据类型

#### 4.1 字符串   之间可以加     可以和数字乘

- .upper()  字母变大写

- lower()

- .isdigit()   返回布尔值

- replace（'老值','新的',次数）  替换

- split('标志'，次数)    切割

- strip()  去空格   

- startswith('**')   判断开头是否为 **

- endswith('')   判断结尾是否为          返回布尔值

- ‘ ’.join（a）  用空字符连接a的每一个元素

- formate     formate（'啊可接受的看见{0},{1},{2}',a,b,c)

- encode  改变编码格式得到2进制的编码

  

##### 4.1.1共有方法

len(a)      计算a的长度

1. 索引

   a[0]   代表a的第一个元素

   a[n]    代表第n+1个元素

   a[-1]  代表最后一个元素

- 切片

  a = 'asjkdha'

  a[:3]   代表前3个元素   asj

  a[2:3]  表示坐标为2的   j

  a[3:]     表示kdha

  a[3:-1] 表示  kdh

单引号  双引号   三引号（可以换行打印）

#### 4.1 整形   

1. ###### 4.1.1共有方法

   1.索引

   ```
   2. 切片
   3. 步长
   4. for
   5. 删除
   6. 修改
   7. len
   ```

##### 4.1.2独有方法

​	1. . appended()     末尾加入一个元素

​	2. insret('元素位置'，‘元素’)    加入一个元素     

###### 	3. remove（'元素名'）  删除元素

###### 	4. pop（括号里天下标）      							

5. clear   清空

6. extend  将一个可迭代序列中的元素追加到列表中

7. everse（）    反转列表元素排列位置

   ​			   

8. .sort（）       将有序的列表从小到大排列    reverse默认参数为flase

##### 4.1.3 元祖：

​	1.共有方法

1. 切片
2. 索引
3. len
4. for
5. 步长
6. 布尔类型

##### 4.1.4 字典

###### -  字典的建一定为可哈希类型，列表集合字典不能作为建

1. 独有方法keys（），values（），items（）
2. for i in dict.keys（）：   所有建
3. for i in  dict  .values（）： 所有值
4. for a,b in dict.items()：     建和值
5. update（）  将一个字典的键值对更新到字典中
6. get（） 取值  如果建不在返回None

###### 1. 共有功能

1. 索引
2. len
3. for
4. 修改
5. 删除

#### 4.6集合

###### 特点：无序，无重复，集合内元素一定为可哈希类型，列表

###### 集合  字典 不可以作为集合的元素

4.6.1独有功能：

1. add（）    添加
2. discard（）  删除
3. update（）  批量添加
4. intersection（）取交集      返回一个新的集合接收
5. union（）  并集      
6. difference（）差集
7. symmetric_difference    对称差集

2.共有功能：

1. len
2. for

##### 4.1.4步长

a[::-1]      从右到左  

a[::2]       从左到右  隔一个取一个

a[:9:-2]     只取第一个

a[0:9:-2]   什么都不取

##### 4.1.5 变量命名条件建议

1. 只能数字字母下划线组成
2. 数字不能开头
3. 不能跟python关键字相同
4. 建议   见名知意思

# 5. python2  和3区别

- py2默认解释器编码格式     ascii            修改头文件加
- py3默认解释器编码格式  utf-8

```
	# -*- coding:utf-8 -*-

```

- py3默认解释器编码格式  utf-8

- py2输出  print加空格

- py3输出print（）

- py2输入  raw_input（）

- py3输入    input（）
- 在python 2中  存在整形跟长整形  int 跟 long
- 在python3中只有整形
- 在python2中   除法只会取整
- 在python3中可以得出小数部分

# 6. if判断

1. if  条件 and 条件：

   ​	print（）

   elif 条件 ：

   ​	print（）

   else：

   ​	print（）

## 7. while循环语句 for循环语句  if条件语句

- 7.1 格式

  ```python
  while 条件：
  	成立则执行函数内部 
  else：
  	条件不成立执行内部
  
  	
  ```

  ```python
  for i in 可迭代的一个序列；
  	执行函数内部
  ```

  ```python
  if 条件：
  	成立执行内部
  elif 条件：
  	成立执行内部
  else：
  	上面都不成立执行
  ```

  

- 7.2 break  跳出循环

  continue  结束当前循环并进入下一次循环

# 8. 字符串格式化

1. 占位符   %s %d    字母 数字

   

   ```python
   name = input('输入你的名字')
   count = int(input('输入你的期数'))
   message = '%s是老男孩%d期学员'%(name，count，)
   ```

2. 如果想打印%   字需要用  %%来实现百分号

# 9. 运算符 

1. ```
   算数运算符 + - * / %取余 //取整
   ```

2. 逻辑运算符     not >and> or      优先级   vale =  0 and 1 or 6 and 9

3. vale = 0 or 9

   如果第一个值为假 则取后面的

   如果第一个值为真，则取第一个值

   vale = 0 and 9

   如果第一个值为假  则取第一个值

   如果第一个值为真 则取第二个值

4. 1.in      

   ​		

   ```
   		a = '面对疾风吧'
   		b = '疾风'
   		c = b in a
   		print(c)   #c为一个布尔值    
   ```

   

5. not in

6. 优先级：

   	- not > and > or

   

# 

# 10. 编码

1.Unicode   

​	ecs2    最开始2字节

​	ecs4    现在4字节

2.ascii

3utf-8   中文3字节

4utf-16   最少16位

5. gbk 中文占2字符
6. 8bit = 1byte
7. 1024byte = 1kb
8. 1024kb = 1Mb
9. 1024M = 1Gb
10. 1024Gb = 1Tb

# 11. 内存相关

a， b  指向同一个内存地址，如果 a 改变的事内存地址存储的内容，b也会改变

如果  a  改变指向别的内存地址 ，则b 不变  ，当以个内存没人指向他的时候，python解释器就会

把他当做垃圾回收掉，否则一直存在内存中

# 12. 深浅拷贝

1. 不可变类型的深浅拷贝都一样

2. 区别：嵌套类型的可变数据类型，浅拷贝值只复制第一层的内存地址

   深拷贝，把所有嵌套的可变数据类型都复制

## 13. 文件操作

​	1.文件的打开

```python
	f = open('目录'，‘打开模式’，encoding = utf-8)
```

2. 打开模式：

   'r'  'w' 'a'   'r+' 'w+'  'a+'

   - 'r'   只读模式   
   - ‘w’  只写模式   每次打开都会把里面原有内容清空  在写入
   - ‘a’   追加
   - 'r+'   读写模式，每次打开光标都在最开始的位置，直接写入会从0开始写入
   - 'w+'  写读模式 ，每次打开都会清空，啥也没有，写入后才能读
   - ‘a+’   追加    默认光标在最后 读取内容需要先调整光标，但每次写入光标都会跳到结尾
   - rb     二进制只读
   - wb    二进制只写    
   - ab     二进制追加
   - w+b    二进制写读
   - a+b     二进制追加
   - r+b  二进制读写

   3.方法

   ###### 1. read(2)   读取两个字符

   2. readline  读取一行
   3. write
   4. 文件的关闭    f.colse（）

   ###### 13.3 高逼格打开方式

    - with open（‘文件名’，‘模式’，encoding = ‘utf-8’） as f1:     不需要关闭，代码缩进段执行完自动关闭

   for i  in f1    :     每次循环一行 文件中的内容

   f1.seek(0)    将光标移动到0字节位置  ‘字节’



##### 三元运算符  三目运算符

```python  
v = '结果1' if 条件 else 结果
#如果if 成立  v= 结果1  不成立 v= 结果

```

## 14. 函数.

为什么要写函数？

没有函数体的约束，编程性质为面向过程编程，可读性差，可重用性差。

对于函数编程：

- 本质：将N行代码拿到别处并给他起个名字，以后通过名字就可以找到这段代码并执行。
- 场景：
  - 代码重复执行。
  - 代码量特别多超过一屏，可以选择通过函数进行代码的分割。

##### 面向过程编程

user_input = input('请输入角色：')

if user_input == '管理员':
    import smtplib
    from email.mime.text import MIMEText
    from email.utils import formataddr

```
msg = MIMEText('管理员，我想演男一号，你想怎么着都行。', 'plain', 'utf-8')
msg['From'] = formataddr(["李邵奇", '15776556369@163.com'])
msg['To'] = formataddr(["管理员", '344522251@qq.com'])
msg['Subject'] = "情爱的导演"

server = smtplib.SMTP("smtp.163.com", 25)
server.login("15776556369@163.com", "qq1105400511")
server.sendmail('15776556369@163.com', ['管理员', ], msg.as_string())
server.quit()
```

elif user_input == '业务员':
    import smtplib
    from email.mime.text import MIMEText
    from email.utils import formataddr

```
msg = MIMEText('业务员，我想演男一号，你想怎么着都行。', 'plain', 'utf-8')
msg['From'] = formataddr(["李邵奇", '15776556369@163.com'])
msg['To'] = formataddr(["业务员", '业务员'])
msg['Subject'] = "情爱的导演"

server = smtplib.SMTP("smtp.163.com", 25)
server.login("15776556369@163.com", "qq1105400511")
server.sendmail('15776556369@163.com', ['业务员', ], msg.as_string())
server.quit()
```

elif user_input == '老板':
    import smtplib
    from email.mime.text import MIMEText
    from email.utils import formataddr

```
msg = MIMEText('老板，我想演男一号，你想怎么着都行。', 'plain', 'utf-8')
msg['From'] = formataddr(["李邵奇", '15776556369@163.com'])
msg['To'] = formataddr(["老板", '老板邮箱'])
msg['Subject'] = "情爱的导演"

server = smtplib.SMTP("smtp.163.com", 25)
server.login("15776556369@163.com", "qq1105400511")
server.sendmail('15776556369@163.com', ['老板邮箱', ], msg.as_string())
server.quit()
```

函数式编程

def send_email():
	import smtplib
    from email.mime.text import MIMEText
    from email.utils import formataddr

```
msg = MIMEText('老板，我想演男一号，你想怎么着都行。', 'plain', 'utf-8')
msg['From'] = formataddr(["李邵奇", '15776556369@163.com'])
msg['To'] = formataddr(["老板", '老板邮箱'])
msg['Subject'] = "情爱的导演"

server = smtplib.SMTP("smtp.163.com", 25)
server.login("15776556369@163.com", "qq1105400511")
server.sendmail('15776556369@163.com', ['老板邮箱', ], msg.as_string())
server.quit()
```

user_input = input('请输入角色：')

if user_input == '管理员':
    send_email()
elif user_input == '业务员':
    send_email()
elif user_input == '老板':
    send_email()

### 14.2. 函数的参数

- 位置参数，关键字参数 

- 位置参数只能出现在关键字参数之前

  - *args （位置参数）   以一个元祖的形式   

  - **kwargs  （关键字参数）  以赋值形式  或者  ’ 星星 ‘   字典形式  见例子

    ```python
    def func(**kwargs):
        print(kwargs)
    func(**{'k1':'v2','k2':'v2'}) # kwargs={'k1':'v2','k2':'v2'}
    
    func(k1=1,k2="alex") # kwargs={'k1': 1, 'k2': 'alex'}
    func(k1={'k1':99,'k2':777},k2="alex") # kwargs={'k1': {'k1': 99, 'k2': 777}, 'k2': 'alex'}
    func(**{'k1':'v2','k2':'v2'}) # kwargs={'k1':'v2','k2':'v2'}
    ```

    

### 14.3. 函数的作用域

函数外部  ：全局作用域

函数内部：局部作用域

作用域查找规则，先在自己里的找，找不到去上一层找，在找不到再去上上层找，知道找到全局作用域，还没有就报错。

###### 3.1关键字

- global    将全局变量中的变量重新赋值
- nonlocal   将上一层作用域中的变量重新赋值

#### 4. 潜规则

##### 全局变量命名以后全部改为大写字母   方便查看