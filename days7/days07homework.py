# v1 = [1,2,3,4,5]
# v2 = [v1,v1,v1]
#
# v1.append(6)
# print(v1)
# print(v2)
'''
# v1与v2的元素都指向同一内存地址中的值，值发生改变，都改变
[1,2,3,4,5,6]
[[1,2,3,4,5,6],[1,2,3,4,5,6],[1,2,3,4,5,6],[1,2,3,4,5,6]]
'''
# v1 = [1,2,3,4,5]
# v2 = [v1,v1,v1]
#
# v2[1][0] = 111
# v2[2][0] = 222
# print(v1)
# print(v2)
'''
[222,2,3,4,5]
[[222, 2, 3, 4, 5], [222, 2, 3, 4, 5], [222, 2, 3, 4, 5]]
'''
# v1 = [1,2,3,4,5,6,7,8,9]
# v2 = {}
#
# for item in v1:
#     if item < 6:
#         continue
#     if 'k1' in v2:
#         v2['k1'].append(item)
#     else:
#         v2['k1'] = [item ]
# print(v2)
{'k1':[6,7,8,9]}
# item 循环v1的值 ，小于6从新循环下一项，等于6 赋值给 字典的'k1' 以列表的形式，
# 然后循环7 列表加入7一直到9
'''
# 简述深浅拷贝
# 浅拷贝只是复制了他的第一层的的内存地址，深拷贝复制了第一层后，入股还有嵌套的可变类型地址
# # 继续复制嵌套可变类型地址
'''
'''
import copy

v1 = "alex"
v2 = copy.copy(v1)
v3 = copy.deepcopy(v1)

print(v1 is v2)
print(v1 is v3)

true
true
'''
'''
import copy

v1 = [1,2,3,4,5]
v2 = copy.copy(v1)
v3 = copy.deepcopy(v1)

print(v1 is v2)
print(v1 is v3)
flase
flase
'''
'''
import copy

v1 = [1,2,3,4,5]

v2 = copy.copy(v1)
v3 = copy.deepcopy(v1)

print(v1[0] is v2[0])
print(v1[0] is v3[0])
print(v2[0] is v3[0])
true
true
true
'''
'''
import copy

v1 = [1,2,3,4,5]

v2 = copy.copy(v1)
v3 = copy.deepcopy(v1)

print(v1[0] is v2[0])
print(v1[0] is v3[0])
print(v2[0] is v3[0])
true
true
true
'''
'''
import copy

v1 = [1,2,3,{"name":'武沛齐',"numbers":[7,77,88]},4,5]

v2 = copy.copy(v1)

print(v1 is v2)

print(v1[0] is v2[0])
print(v1[3] is v2[3])

print(v1[3]['name'] is v2[3]['name'])
print(v1[3]['numbers'] is v2[3]['numbers'])
print(v1[3]['numbers'][1] is v2[3]['numbers'][1])
# flase
# true
# true
# true
# true
# true
'''
'''
import copy

v1 = [1,2,3,{"name":'武沛齐',"numbers":[7,77,88]},4,5]

v2 = copy.deepcopy(v1)

print(v1 is v2)

print(v1[0] is v2[0])
print(v1[3] is v2[3])

print(v1[3]['name'] is v2[3]['name'])
print(v1[3]['numbers'] is v2[3]['numbers'])
print(v1[3]['numbers'][1] is v2[3]['numbers'][1])
# flase
# true
# flase
# true
# flase
# true
'''
# 简述文件操作的打开模式
# r  只读模式
# w只写模式
# a追加模式
# r+  读写模式
# w+写读模式
# a+ 追加模式

# 请将info中的值使用 "_" 拼接起来并写入到文件 "readme.txt" 文件中。
'''
info = ['骗子，','不是','说','只有',"10",'个题吗？']
a = '_'.join(info)
f = open('readme.txt','w',encoding='utf-8')
f.write(a)
f.close()
'''

# 请将info中的值使用 "_" 拼接起来并写入到文件 "readme.txt" 文件中。
'''
info = ['骗子，','不是','说','只有',10,'个题吗？']
for i  in range(len(info)):
    if type(info[i]) == int:
        info[i] = str(info[i])
print(type(info[4]))
a = '_'.join(info)
f = open('readme.txt','w',encoding='utf-8')
f.write(a)
f.close()
'''

# 请将info中的所有键 使用 "_" 拼接起来并写入到文件 "readme.txt" 文件中。
info = {'name':'骗子','age':18,'gender':'性别'}

# 1. 请将info中的所有键 使用 "_" 拼接起来并写入到文件 "readme.txt" 文件中。
# 2. 请将info中的所有值 使用 "_" 拼接起来并写入到文件 "readme.txt" 文件中。
# 3. 请将info中的所有键和值按照 "键|值,键|值,键|值" 拼接起来并写入到文件 "readme.txt" 文件中。
'''
info = {'name':'骗子','age':18,'gender':'性别'}
l = []
for i in info:
    l.append(i)
a = '_'.join(l)
print(a)
f = open('readme.txt','w',encoding='utf-8')
f.write(a)
f.close()
'''
'''
l = []
for i in info:
    if type(info[i]) == int:
        info[i] = str(info[i])
    l .append(info[i])

print(l)
a = '_'.join(l)
f = open('readme.txt','w',encoding='utf-8')
f.write(a)
f.close()
'''
'''
info = {'name':'骗子','age':18,'gender':'性别'}
b = []
for i in info:
    l = []
    if type(info[i]) == int:
        info[i] = str(info[i])
    l.append(i)
    l.append(info[i])
    a = '|'.join(l)
    b.append(a)
s = ','.join(b)
f = open('readme.txt','w',encoding='utf-8')
f.write(s)
f.close()
'''
# 要求：
#     如文件 data.txt 中有内容如下：
#
#     wupeiqi|oldboy|wupeiqi@xx.com
#     alex|oldboy|66s@xx.com
#     xxxx|oldboy|yuy@xx.com
#
#     请用代码实现读入文件的内容，并构造成如下格式：
# info = [
#         {'name':'wupeiqi','pwd':'oldboy','email':'wupeiqi@xx.com'},
#         {'name':'alex','pwd':'oldboy','email':'66s@xx.com'},
#         {'name':'xxxx','pwd':'oldboy','email':'yuy@xx.com'},
#     ]
'''
f = open('data.txt','r',encoding='utf-8')
info = []
for line in f:
    line = line.strip()
    dic = {}
    a,b,c = line.split('|')
    dic['name'] = a
    dic['pwd'] = b
    dic['email'] = c
    info.append(dic)
print(info)
'''