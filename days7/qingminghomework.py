# 操作系统的作用?
'''

'''
'''
接收解释器的命令，并向硬件发送命令，执行操作
'''
# 列举你听过的操作系统及区别？
'''
windows  收费，图形化界面好，可以玩游戏
linux   免费 ，图形化界面差，不能玩游戏   centos  乌班图
mac   收费，图形化界面好，适合办公
'''
# 列举你了解的编码及他们之间的区别？
'''
ascii   8位，主要表示英文字母符号
Unicode  万国码 3''''''
2位 可以表示地球所有的东西，而且还有剩余，目前用到21位
utf-8   万国码的压缩版  中文占3个字节
gbk   中文占两个字节
'''
# 列举你了解的Python2和Python3的区别？
'''
2默认解释器编码为ascii码，3为utf-8
2的输入raw_input   3为input()
2输入为  print+空格   3位input（）
2有int  跟long     3只有int
2的除法取整      3可精确计算到小数
'''
# 你了解的python都有那些数据类型
'''
int   str  bool list  tuple  dict  set  
'''
# 补充代码，实现以下功能。
# value =  _____
# print(value)  # 要求输出  alex"烧饼
'''
value = 'alex"烧饼'
print(value)
'''
# 用print打印出下面内容：
# ⽂能提笔安天下,
# 武能上⻢定乾坤.
# ⼼存谋略何⼈胜,
# 古今英雄唯是君。
''''''
# print('''⽂能提笔安天下,
# 武能上⻢定乾坤.
# ⼼存谋略何⼈胜,
# 古今英雄唯是君。''')

''''''
# 变量名的命名规范和建议
'''
1. 由数字字母下划线组成
2.数字不能开头
3.不能跟python关键字相同
建议 建名知意   下划线链接
'''
# 如下那个变量名是正确的？
'''
name = '武沛齐'   对
_ = 'alex'    对
_9 = "老男孩"   对
9name = "景女神"  错
oldboy(edu = 666   错
'''
# 简述你了解if条件语句的基本结构
'''
if 条件：
    成立执行什么
elif条件：
    成立执行什么
else：
    执行什么
'''
# 设定一个理想数字比如：66，让用户输入数字，如果比66大，则显示猜测的结果大了；
# 如果比66小，则显示猜测的结果小了;只有等于66，显示猜测结果正确。
'''
a = 66
num = int(input('输入数字：'))
if num < a:
    print('小了')
elif num > a:
    print('大了')
else:
    print('正确')
'''
# 提⽰⽤户输入⿇花藤. 判断⽤户输入的对不对。如果对, 提⽰真聪明, 如果不对, 提⽰你 是傻逼么。
'''
a = input('请输入马化腾：')
if a == '马化腾':
    print('真聪明')
else:
    print('你是傻逼吗')
'''
# 猜数字，设定一个理想数字比如：66，让用户输入数字，如果比66大
# ，则显示猜测的结果大了；如果比66小
# ，则显示猜测的结果小了;只有等于66，显示猜测结果正确，然后退出循环。
'''
b = 66
while True:
    a = input('输入数字：')
    if int(a) == b:
        break
    elif a < b:
        print('小了')
    else:
        print('大了')
'''
# 在上一题的基础，设置：给用户三次猜测机会，如果三次之内猜测对了，则显示猜测正确，
# 退出循环，如果三次之内没有猜测正确，则自动退出循环，并显示‘大笨蛋’。
'''
count = 3
a = 66
while count > 0 :
    b = input('输入数字：')
    if int(b) == a:
        break
    elif int(b)<= a:
        print('小了')
    else:
        print('大了')
    count -= 1
'''
# 使用两种方法实现输出 1 2 3 4 5 6 8 9 10
'''
for i in range(1,11):
    print(i)
'''
'''
count = 0
while True:
    count += 1
    if count == 11:
        break
    print(count)
'''
# 求1-100的所有数的和
'''
a = 0
for i in range(1,101):
    a += i
print(a)
'''
# 输出 1-100 内的所有奇数
'''
a = 0
for i in range(1,101):
    if i % 2 != 0:
        a += i
print(a)
'''
# 输出 1-100 内的所有偶数
'''
a = 0
for i in range(1,101):
    if i % 2 == 0:
        a += i
print(a)
'''
# 求1-2+3-4+5 ... 99的所有数的和
'''
a = 0
for i in range(1,101):
    if i % 2 == 0:
        a -= i
    else:
        a += i
print(a)
'''
# ⽤户登陆（三次输错机会）且每次输错误时显示剩余错误次数（提示：使⽤字符串格式化
'''
count = 3
while count> 0:
    a = input('亲输入：')
    if int(a) == 666:
        print('登陆成功')
        break
    count -= 1
    print('您还剩余%d次机会'%(count,))
'''
# 简述ASCII、Unicode、utf-8编码
'''
ascii   8位主要表示数字英文符号
Unicode 32位 地球所有都能表示 还有剩余
utf-8   万国码压缩版  3个字节一个汉字
'''
# 简述位和字节的关系？
'''
8位一个字节
'''
# 猜年龄游戏升级版 要求：允许用户最多尝试3次，每尝试3次后
# ，如果还没猜对，就问用户是否还想继续玩，如果回答Y，就继续让其猜3次，
# 以此往复，如果回答N，就退出程序，如何猜对了，就直接退出。
'''
count = 3
b = 66
while count > 0:
    a = input('输入猜想：')
    if int(a) == b:
        print('duile')
        break
    elif int(a) <= b:
        print('xiaole ')
    else:
        print('大了')
    count -= 1
    if count == 0:

        c = input('继续输入Y，退出输入N')
        if c.upper() == 'Y':
            count = 3
        elif c.upper() == 'N':
            break
'''
# 判断下列逻辑语句的True,False
'''

# 1 > 1 or 3 < 4 or 4 > 5 and 2 > 1 and 9 > 8 or 7 < 6   True
# not 2 > 1 and 3 < 4 or 4 > 5 and 2 > 1 and 9 > 8 or 7 < 6   flase
'''
'''
8 or 3 and 4 or 2 and 0 or 9 and 7    8
0 or 2 and 3 and 4 or 6 and 0 or 3     4
'''
'''

6 or 2 > 1    6
3 or 2 > 1      3
0 or 5 < 4          flase
5 < 4 or 3          3
2 > 1 or 6          true
3 and 2 > 1         true
0 and 3 > 1         0
2 > 1 and 3         3
3 > 1 and 0          0
3 > 1 and 2 or 2 < 3 and 3 and 4 or 3 > 2      2
'''
# 有变量name = "aleX leNb " 完成如下操作：
name = "aleX leNb "
#
# 移除 name 变量对应的值两边的空格,并输出处理结果
'''
name = name.strip()
print(name)
'''
# 判断 name 变量是否以 "al" 开头,并输出结果（用切片）
'''
if name[:1] == 'al':
    print('zai')
    '''
# 判断name变量是否以"Nb"结尾,并输出结果（用切片）
'''
if name[-2:] == 'Nb':
    print('zai')
'''
# 将 name 变量对应的值中的 所有的"l" 替换为 "p",并输出结果
'''
name = name.replace('l','p')
print(name)
'''
# 将name变量对应的值中的第一个"l"替换成"p",并输出结果
'''
name = name.replace('l','p',1)
print(name )
'''
# 将 name 变量对应的值根据 所有的"l" 分割,并输出结果
'''
a = name.split('l')
print(a)
'''
# 将name变量对应的值根据第一个"l"分割,并输出结果
'''
a = name.split('l',1)
print(a)
'''
# 将 name 变量对应的值变大写,并输出结果
'''
name = name.upper()
print(name)
'''
# 将 name 变量对应的值变小写,并输出结果
'''
name = name.lower()
print(name)
'''
# 请输出 name 变量对应的值的第 2 个字符?
'''
print(name[1])
'''
# 请输出 name 变量对应的值的前 3 个字符?
'''
print(name[:3])
'''
# 请输出 name 变量对应的值的后 2 个字符?
'''
print(name[-2:])
'''
# 有字符串s = "123a4b5c"
s = "123a4b5c"
#
# 通过对s切片形成新的字符串 "123"
'''
s1 = s[:3]
print(s1)
'''
# 通过对s切片形成新的字符串 "a4b"
'''
s1 = s[3:6]
print(s1)
'''
# 通过对s切片形成字符串s5,s5 = "c"
'''
s5 = s[-1:]
print(s5)
'''
# 通过对s切片形成字符串s6,s6 = "ba2"
'''
s6 = s[-3::-2]
print(s6)
'''
# 使用while循环字符串 s="asdfer" 中每个元素
s="asdfer"
'''
count = 0
while count< len(s):
    print(s[count])
    count += 1
'''
# 使用while循环对s="321"进行循环，打印的内容依次是
# ："倒计时3秒"，"倒计时2秒"，"倒计时1秒"，"出发！"。
'''
count = 3
while count > 0:
    print('倒计时%d秒'%(count))
    count -= 1
print('出发')
'''
# 实现一个整数加法计算器(两个数相加)：
#
# 如：content = input("请输入内容:") 用户输入：5+9或5+ 9或5 + 9（含空白），
# 然后进行分割转换最终进行整数的计算得到结果。
'''
content = input("请输入内容:")
new_content = content.split('+')
result = int(new_content[0].strip()) + int(new_content[1].strip())
print(result)
'''
# 计算用户输入的内容中有几个 h 字符？
#
# 如：content = input("请输入内容：") # 如fhdal234slfh98769fjdla
'''
content = input("请输入内容：")
count = 0
for i in content:
    if i == 'h':
        count += 1
print(count)
'''
# 计算用户输入的内容中有几个 h 或 H 字符？
# #
# # 如：content = input("请输入内容：") # 如fhdal234slfH9H769fjdla
'''
content = input("请输入内容：")
count = 0
for i in content:
    if i.upper() == 'H':
        count += 1
print(count)
'''
# 使用while循环分别正向和反向对字符串 message = "伤情最是晚凉天，憔悴厮人不堪言。" 进行打印。
'''
message = "伤情最是晚凉天，憔悴厮人不堪言。"
count = 0
s = ''
while count < len(message):
    s += message[count]
    count += 1
print(s)
a = ''

count = 0
while count < len(message):
    a = message[count] + a 
    count += 1
print(a)
'''
# 获取用户输入的内容中 前4个字符中 有几个 A ？
'''
count = input('请输入：')
num = 0
for i in count[:4]:
    if i == 'A':
        num += 1
print(num)
'''
# 获取用户输入的内容，并计算前四位"l"出现几次,并输出结果
'''
count = input('请输入：')
num = 0
for i in count[:4]:
    if i == 'L':
        num += 1
print(num)
'''
# 获取用户两次输入的内容，并将所有的数据获取并进行相加，如：
# # """
# # 要求：
# # 	将num1中的的所有数字找到并拼接起来：1232312
# # 	将num1中的的所有数字找到并拼接起来：1218323
# # 	然后将两个数字进行相加。
# # """
# #
# # num1 = input("请输入：") # asdfd123sf2312
# # num2 = input("请输入：") #
# # # 请补充代码
'''
num1 = input("请输入：")
num2 = input("请输入：")
s = ''
b = ''
for i in  num1:
    if i.isdigit():
        s += i
for i in num2:
    if i.isdigit():
        b += i
result = int(s) + int(b)
print(result)
'''
# 简述解释性语言和编译型语言的区别？
'''
解释性语言是写一句解释器给你解释一句 
编译性语言是 写完代码交给编译器 编译器编译成一个文件在  取解释这个文件
'''
# 列举你了解的Python的数据类型？
'''
str()  int()   bool  list tuple dict set
'''
# 写代码，有如下列表，按照要求实现每一个功能。

li = ["alex", "WuSir", "ritian", "barry", "wenzhou"]
# 计算列表的长度并输出
'''
print(len(li))
'''
# 请通过步长获取索引为偶数的所有值，并打印出获取后的列表
'''
print(li[::2])
'''
# 列表中追加元素"seven",并输出添加后的列表
'''
li.append('seven')
print(li)
'''
# 请在列表的第1个位置插入元素"Tony",并输出添加后的列表
'''
li.insert(1,'tony')
print(li)
'''
# 请修改列表第2个位置的元素为"Kelly",并输出修改后的列表
'''
li[2] = 'Kelly'
print(li)
'''
# 请将列表的第3个位置的值改成 "太白"，并输出修改后的列表
'''
li[3] = '太白'
print(li)
'''
# 请将列表 l2=[1,"a",3,4,"heart"] 的每一个元素追加到列表li中，并输出添加后的列表
'''
l2=[1,"a",3,4,"heart"]
li.extend(l2)
print(li)
'''
# 请将字符串 s = "qwert"的每一个元素添加到列表li中，一行代码实现，不允许循环添加。
'''
s = "qwert"
li.extend(s)
print(li)
'''

# 请删除列表中的元素"ritian",并输出添加后的列表
'''
li.remove("ritian")
print(li)
'''
# 请删除列表中的第2个元素，并输出删除元素后的列表
'''
li.pop(1)
print(li)
'''
# 请删除列表中的第2至第4个元素，并输出删除元素后的列表
'''
del li[1:4]
print(li)
'''
# 请用三种方法实现字符串反转 name = "小黑半夜三点在被窝玩愤怒的小鸟"（步长、while、for）
name = "小黑半夜三点在被窝玩愤怒的小鸟"
'''
name = name[::-1]
print(name)
'''
'''
count = 0
s = ''
while count< len(name):
    s = name[count] + s
    count += 1
print(s)
'''
'''
s = ''
for i in name:
    s = i + s
print(s)
'''
# 写代码，有如下列表，利用切片实现每一个功能
li = [1, 3, 2, "a", 4, "b", 5,"c"]
# 通过对li列表的切片形成新的列表 [1,3,2]
'''
l1 = li[:3]
print(l1)
'''
# 通过对li列表的切片形成新的列表 ["a",4,"b"]
'''
l2 = li[3:6]
print(l2)
'''
# 通过对li列表的切片形成新的列表 [1,2,4,5]
'''
l3 = li[::2]
print(l3)
'''
# 通过对li列表的切片形成新的列表 [3,"a","b"]
'''
l4 = li[1:-2:2]
print(l4)
'''
# 通过对li列表的切片形成新的列表 [3,"a","b","c"]
'''
l5 = li[1::2]
print(l5)
'''
# 通过对li列表的切片形成新的列表 ["c"]
'''
l6 = li[-1:]
print(l6)
'''
# 通过对li列表的切片形成新的列表 ["b","a",3]
'''
l7 = li[-3::-2]
print(l7)
'''
# 请用代码实现循环输出元素和值：users = ["武沛齐","景女神","肖大侠"] ，如：
#
# 0 武沛齐
# 1 景女神
# 2 肖大侠
'''
users = ["武沛齐","景女神","肖大侠"]
count = 0
for i in users:
    print(count,i)
    count += 1
'''
# 请用代码实现循环输出元素和值：users = ["武沛齐","景女神","肖大侠"] ，如：
#
# 1 武沛齐
# 2 景女神
# 3 肖大侠
'''
users = ["武沛齐","景女神","肖大侠"]
count = 1
for i in users:
    print(count,i)
    count += 1
'''
# 写代码，有如下列表，按照要求实现每一个功能。

lis = [2, 3, "k", ["qwe", 20, ["k1", ["tt", 3, "1"]], 89], "ab", "adv"]
# 将列表lis中的"k"变成大写，并打印列表。
'''
lis[2] = 'K'
print(lis)
'''
# 将列表中的数字3变成字符串"100"
'''
lis[1] = '100'
lis[3][2][1][1] = '100'
print(lis)
'''
# 将列表中的字符串"tt"变成数字 101
'''
lis[3][2][1][0] = 101
print(lis)
'''
# 在 "qwe"前面插入字符串："火车头"
'''
lis[3].insert(0,'火车头')
print(lis)
'''
# 如有变量 googs = ['汽车','飞机','火箭'] 提示用户可供选择的商品：
#
# 0,汽车
# 1,飞机
# 2,火箭
# 用户输入索引后，将指定商品的内容拼接打印，如：用户输入0，则打印 您选择的商品是汽车。
'''
googs = ['汽车','飞机','火箭']
count = 0
for i in googs:
    print(count,i)
    count += 1
a = input('请输入商品号')
print('您选择的商品是%s'%(googs[int(a)]))
'''

# 请用代码实现
# #
# # li = "alex"
# # 利用下划线将列表的每一个元素拼接成字符串"a_l_e_x"
'''
li = "alex"
a = '_'.join(li)
print(a)
'''
# 利用for循环和range找出 0 ~ 100 以内所有的偶数，并追加到一个列表。
'''
l = []
for i in range(101):
    if i % 2 == 0:
        l.append(i)
print(l)
'''
# 利用for循环和range 找出 0 ~ 50 以内能被3整除的数，并追加到一个列表。
'''
l = []
for i in range(51):
    if i % 3 == 0:
        l.append(i)
print(l)
'''
# 利用for循环和range 找出 0 ~ 50 以内能被3整除的数
# ，并插入到列表的第0个索引位置，最终结果如下：
# [48,45,42...]
'''
l = []
for i in range(51):
    if i % 3 == 0:
        l.insert(0,i)
print(l)
'''
# 查找列表li中的元素，移除每个元素的空格，并找出以"a"开头
# ，并添加到一个新列表中,最后循环打印这个新列表。
'''
li = ["TaiBai ", "alexC", "AbC ", "egon", " riTiAn", "WuSir", "  aqc"]
ll = []
for i in li:
    i = i.strip()
    if i.startswith('a'):
        ll.append(i)
print(ll)
'''
# 判断是否可以实现，如果可以请写代码实现。
li = ["alex",[11,22,(88,99,100,),33] ,"WuSir", ("ritian", "barry",), "wenzhou"]
# 请将 "WuSir" 修改成 "武沛齐"
'''
li[2] = '武沛齐'
print(li)
'''
# 请将 ("ritian", "barry",) 修改为 ['日天','日地']
'''
li[3] = ['日天','日地']
print(li)
'''
# 请将 88 修改为 87
'''
不能修改
'''
# 请将 "wenzhou" 删除，然后再在列表第0个索引位置插入 "文周"
'''
li.pop()
li.insert(0,'文周')
print(li)
'''
# 请将列表中的每个元素通过 "_" 链接起来。
'''
users = ['李少奇','李启航','渣渣辉']
a = '_'.join(users)
print(a)
'''
# 请将列表中的每个元素通过 "_" 链接起来。
'''

users = ['李少奇','李启航',666,'渣渣辉']
for i in range(len(users)):
    users[i] = str(users[i])
a = '_'.join(users)
print(a)
'''
# 请将元组 v1 = (11,22,33) 中的所有元素追加到列表 v2 = [44,55,66] 中。
'''
v1 = (11,22,33)
v2 = [44,55,66]
for i in v1:
    v2.append(i)
print(v2)
'''
'''
v1 = (11,22,33)
v2 = [44,55,66]
v2.extend(v1)
print(v2)
'''

# 请将元组 v1 = (11,22,33,44,55,66,77,88,99)
# 中的所有偶数索引位置的元素 追加到列表 v2 = [44,55,66] 中。
'''
v1 = (11,22,33,44,55,66,77,88,99)
v2 = [44,55,66]
for i in v1[::2]:
    v2.append(i)
'''
'''
v1 = (11,22,33,44,55,66,77,88,99)
v2 = [44,55,66]
v2.extend(v1[::2])
print(v2)
'''
# 将字典的键和值分别追加到 key_list 和 value_list 两个列表中，如：
'''

key_list = []
value_list = []
info = {'k1':'v1','k2':'v2','k3':'v3'}
for i in info:
    key_list.append(i)
    value_list.append(info[i])
print(key_list,value_list)
'''
# 字典dic = {'k1': "v1", "k2": "v2", "k3": [11,22,33]}
dic = {'k1': "v1", "k2": "v2", "k3": [11,22,33]}
# a. 请循环输出所有的key
'''
for i in dic:
    print(i)
'''
# b. 请循环输出所有的value
'''
for i in dic:
    print(dic[i])
'''
# c. 请循环输出所有的key和value
'''
for key,value in dic.items():
    print(key,value)
'''
# d. 请在字典中添加一个键值对，"k4": "v4"，输出添加后的字典
'''
dic['k4'] = 'v4'
print(dic)
'''
# e. 请在修改字典中 "k1" 对应的值为 "alex"，输出修改后的字典
'''
dic['k1'] = 'alex'
print(dic)
'''
# f. 请在k3对应的值中追加一个元素 44，输出修改后的字典
'''
dic["k3"].append(44)
print(dic)
'''
# g. 请在k3对应的值的第 1 个位置插入个元素 18，输出修改后的字典
'''
dic["k3"].insert(1,18)
print(dic)
'''
# 请循环打印k2对应的值中的每个元素。
'''

info = {
    'k1':'v1',
    'k2':[('alex'),('wupeiqi'),('oldboy')]
}
for i in info['k2']:
    print(i)
'''
# 有字符串"k: 1|k1:2|k2:3 |k3 :4" 处理成字典 {'k':1,'k1':2....}
'''
dic1 = {}
stttr = "k: 1|k1:2|k2:3 |k3 :4"
lir = stttr.split('|')
for i in lir:
    key,vall = i.split(':')
    dic1[key] = vall
print(dic1)
'''
"""
有如下值 li= [11,22,33,44,55,66,77,88,99,90] ,
将所有大于 66 的值保存至字典的第一个key对应的列表中，
将小于 66 的值保存至第二个key对应的列表中。

result = {'k1':[],'k2':[]}
"""
'''
a = []
b = []
li= [11,22,33,44,55,66,77,88,99,90]
for i in li:
    if i > 66:
        a.append(i)
    elif i < 66:
        b.append(i)
result  = {}
result['k1'] = a
result['k2'] = b
print(result)
'''
#输出商品列表，用户输入序号，显示用户选中的商品

"""
商品列表：
  goods = [
		{"name": "电脑", "price": 1999},
		{"name": "鼠标", "price": 10},
		{"name": "游艇", "price": 20},
		{"name": "美女", "price": 998}
	]
要求:
1：页面显示 序号 + 商品名称 + 商品价格，如：
      1 电脑 1999 
      2 鼠标 10
	  ...
2：用户输入选择的商品序号，然后打印商品名称及商品价格
3：如果用户输入的商品序号有误，则提示输入有误，并重新输入。
4：用户输入Q或者q，退出程序。
"""
goods = [
		{"name": "电脑", "price": 1999},
		{"name": "鼠标", "price": 10},
		{"name": "游艇", "price": 20},
		{"name": "美女", "price": 998}
	]
'''
count = 0
for i in goods:
    print(count+1,goods[count]['name'],goods[count]['price'])
    count += 1

while True:
    a = input('输入序号Q退出：')
    if a.upper() == 'Q':
        break
    if not a.isdigit():
        print('请输入数字')
        continue
    if int(a)>4 or int(a) < 1 :
        print('输入错误，请重新输入')
        continue
    else:
        print(goods[int(a)-1]['name'],goods[int(a)-1]['price'])
        break
'''
# v = {}
# for index in range(10):
#     v['users'] = index
# print(v)
'''
{'users':9}
'''
# 列举你了解的字典中的功能（字典独有）。
'''
keys()  values()  items()  get()  update()
'''
# 列举你了解的集合中的功能（集合独有）
'''
add()  discard()  update()  intersecition()  uinon() difference()
'''
# 列举你了解的可以转换为 布尔值且为False的值。
'''
0 ,'', [] , (), {} ,set()  None
'''
# 请用代码实现

info = {'name':'王刚蛋','hobby':'铁锤'}
# 循环提示用户输入，根据用户输入的值为键去字典中获取对应的值并输出。
# 循环提示用户输入，根据用户输入的值为键去字典中获取对应的值并输出
# （如果key不存在，则获取默认“键不存在”，并输出）。 注意：无需考虑循环终止（写死循环即可）
'''
while True:
    a = input('输入内容')
    info.get(a,'建不存在')
'''
# 请用代码验证 "name" 是否在字典的键中？
'''

info = {'name':'王刚蛋','hobby':'铁锤','age':'18',}
print(info.get('name','建不存在'))
'''
# 请用代码验证 "alex" 是否在字典的值中？
'''

info = {'name':'王刚蛋','hobby':'铁锤','age':'18'}
count = False
for i in info.values():
    if i == 'alex':
        count = True
        break
if count == True:
    print('在里面')
else:
	print('不在里面')
'''
# v1 = {'武沛齐','李杰','太白','景女神'}
# v2 = {'李杰','景女神}
# 请得到 v1 和 v2 的交集并输出
# 请得到 v1 和 v2 的并集并输出
# 请得到 v1 和 v2 的 差集并输出
# 请得到 v2 和 v1 的 差集并输出
'''
v1 = {'武沛齐','李杰','太白','景女神'}
v2 = {'李杰','景女神'}
v3 = v1.intersection(v2)
v4 = v1.union(v2)
v5 = v1.difference(v2)
v6 = v2.difference(v1)
print(v3,v4,v5,v6)
'''
# 循环提示用户输入，并将输入内容追加到列表中（如果输入N或n则停止循环
'''
l = []
while True:
	a = input('请输入内容，或N退出：')
	if a.upper() == 'N':
		break
	else:
		l.append(a)
'''
# 循环提示用户输入，并将输入内容添加到集合中（如果输入N或n则停止循环）
'''
l = set()
while True:
	a = input('请输入内容，或N退出：')
	if a.upper() == 'N':
		break
	else:
		l.add(a)
'''
v1 = {'alex','武sir','肖大'}
v2 = []

# 循环提示用户输入，如果输入值在v1中存在，则追加到v2中，
# 如果v1中不存在，则添加到v1中。（如果输入N或n则停止循环）
'''
while True:
	a = input('请输入内容,或N退出：')
	if a.upper() == 'N':
		break
	elif a in v1:
		v2.append(a)
	else:
		v1.add(a)
print(v1,v2)
'''
# 判断以下值那个能做字典的key ？那个能做集合的元素？
'''

1   可做建  可做元素
-1   可做建 可做元素
""    可做建 可做元素
None  可做建 可做元素
[1,2]  不能做建不能做元素
(1,)   可做建 可做元素
{11,22,33,4}		都不能做
{'name':'wupeiq','age':18}   都不能做
'''
# is 和 == 的区别？
'''
is 比较的是指向的是不是同一个内存
== 比较的是内存中的值
'''
# type使用方式及作用？
'''
type()
查看 数据类型  
'''
# id的使用方式及作用？
'''
id()
查看内存地址
'''
# 看代码写结果并解释原因

# v1 = {'k1':'v1','k2':[1,2,3]}
# v2 = {'k1':'v1','k2':[1,2,3]}
#
# result1 = v1 == v2
# result2 = v1 is v2
# print(result1)
# print(result2)
'''
True Flase
因为他们内存地址指向不同，但是内存地址中的值相同
'''
# 看代码写结果并解释原因

# v1 = {'k1':'v1','k2':[1,2,3]}
# v2 = v1
#
# result1 = v1 == v2
# result2 = v1 is v2
# print(result1)
# print(result2)
'''
都为True
因为他们都指向同一个内存地址，所以内存地址中的值相同
'''
# 看代码写结果并解释原因
#
# v1 = {'k1':'v1','k2':[1,2,3]}
# v2 = v1
#
# v1['k1'] = 'wupeiqi'
# print(v2)
'''
{'k1': 'wupeiqi', 'k2': [1, 2, 3]}
因为v1 v2 都指向同一个内存地址，这个内存地址的值变化，v1v2都变化
'''
# 看代码写结果并解释原因

# v1 = '人生苦短，我用Python'
# v2 = [1,2,3,4,v1]
#
# v1 = "人生苦短，用毛线Python"
#
# print(v2)
'''
[1, 2, 3, 4, '人生苦短，我用Python']
y因为v1的内存地址修改了，但是v2元素中的内存地址还是指向原来的
'''
# info = [1,2,3]
# userinfo = {'account':info, 'num':info, 'money':info}
#
# info.append(9)
# print(userinfo)
#
# info = "题怎么这么多"
# print(userinfo)
'''
{'account': [1, 2, 3, 9], 'num': [1, 2, 3, 9], 'money': [1, 2, 3, 9]}
{'account': [1, 2, 3, 9], 'num': [1, 2, 3, 9], 'money': [1, 2, 3, 9]}
因为 字典中对应的内存中的值加入了9，所以列表中值多了个9
因为 字典对应的内存地址没发生改变，所以，字典 不变
'''
# 看代码写结果并解释原因

# info = [1,2,3]
# userinfo = [info,info,info,info,info]
#
# info[0] = '不仅多，还特么难呢'
# print(info,userinfo)
'''
['不仅多，还特么难呢', 2, 3] [['不仅多，还特么难呢', 2, 3], ['不仅多，还特么难呢', 2, 3]
因为 info 指向的内存地址中的值放生了改变，所以指向他的都改变

'''


# info = [1,2,3]
# userinfo = [info,info,info,info,info]
#
# userinfo[2][0] = '闭嘴'
# print(info,userinfo)

'''
['闭嘴', 2, 3] [['闭嘴', 2, 3], ['闭嘴', 2, 3]
因为内存地址中的值发生了改变，所以指向内存地址中的值都改变
'''
# 看代码写结果并解释原因

# info = [1, 2, 3]
# user_list = []
# for item in range(10):
# 	user_list.append(info)
#
# info[1] = "是谁说Python好学的？"
#
# print(user_list)
'''
[[1, '是谁说Python好学的？', 3], [1, '是谁说Python好学的？', 3], [1, '是谁说Python好学的？', 3]
因为他们都指向同一个内存地址，然后内存地址中的值加入了一个元素，所以都改变
'''
# 看代码写结果并解释原因
#
# data = {}
# for i in range(10):
#     data['user'] = i
# print(data)
'''
{'user': 9}
因为每次都是在修改user'的值  所以最后一次=9

'''
# 看代码写结果并解释原因

data_list = []
data = {}
for i in range(10):
    data['user'] = i
    data_list.append(data)
print(data_list)
'''
[{'user': 9}, {'user': 9}, {'user': 9}, {'user': 9}
因为 每次加入的都是指向同一个内存地址的字典
所以最后一次内存地址的值改变得值为准
'''
# 看代码写结果并解释原因

data_list = []
for i in range(10):
    data = {}
    data['user'] = i
    data_list.append(data)
print(data_list)
'''
[{'user': 0}, {'user': 1}, {'user': 2}, {'user': 3}, {'u
每次for循环data都会指向一个新的地址，每次加入的都是新地址指向的值，
所以列表元素 不相同
'''
