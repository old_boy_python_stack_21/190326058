# 人-软件-解释器-系统-硬件

操作系统

windows要钱  能打游戏  gbk

linux  效率高不要钱  不能打游戏   utf-8  图形界面垃圾

mac   适用于办公  utf-8

# 1，编码

1. Unicode  万国码    32位  4字节          所有的都能表示出来还有剩余
2. ascii     8位4字节        只能表示英文字母 数字下划线
3. utf-8    万国码的压缩版    从左到右每8位为0就舍去  汉字占3字符
4. gbk   汉字占2字符

# 解释器

 1. 在linux上有种特别的调用方式

    ./a.py

    遇到这个就会去头文件  找解释器路径

    头文件 #！udasdoi/asjdlak/asldja/ljal  python36

# 输出输入

1. print（''）

2. input(' ')

# 数据类型

1. 字符串   之间可以加     可以和数字乘

   单引号  双引号   三引号（可以换行打印）

2. 整形   

3. 布尔类型

   # 变量命名条件建议

   1. 只能数字字母下划线组成
   2. 数字不能开头
   3. 不能跟python关键字相同
   4. 建议   见名知意思

# python2  和3区别

2默认解释器编码格式     ascii            修改头文件加

```
# -*- coding:utf-8 -*-

```

3默认解释器编码格式  utf-8

2输出  print加空格

3输出print（）

2输入  raw_input（）

3输入    input（）

# if判断

1. if  条件 and 条件：

   ​	print（）

   elif 条件 ：

   ​	print（）

   else：

   ​	print（）

int（）  整形