# day6    MongoDB

### mongDB 中特性

1. 使用json结构存储

2. 使用了不存在的对象即创建该对象

3. Mysql                   MongoDB

   database                 database

   Tables                        Collections

   Colum(数据列)            Field

   Row  (数据行)             Documents     

4. MongoDB  启动

-   -cmd  27017 默认监听端口

-  mongod                  --启动MongoDB 服务
- 修改默认文件存放位置   mongod --dbpath='D:\data\db'
- 启动客户端   mongo

```python
1. show databases  #展示所有数据库
2. use 库名   #切换到数据库
3. db   #当前使用的数据库
4. db.表名    #可以点输出来各种表
5. db(内存中数据库).tablename(内存创建表名)
6. show tables #查看看当前数据库活动的数据表
MongoDB use到不存在的表中  使用了不存在的表  都是在内存中创建
只有在表中存在数据,数据库级数据均会写入到磁盘
```

### 2. 增删改查

##### 1. 增:

```python
#野生程序员操作方法
db.tablename.insert(  {  }  ) 
#官方推荐:
db.表名.insert_one({})   增加一条数据
db.表名.insert_Many([{}]) 必须列表套字典
```

### 3. 简单查:

```python
db.tablename.find( )    查找当前数据表中的所有数据   
{ "_id" : ObjectId("5d50bdd1399ab50efdf765de") }
{ "_id" : ObjectId("5d50be7c399ab50efdf765df"), "name" : "alexander.dsb.li" }
db.tablename.find({age:999})    查询等于999的
db.tablename.find({age:{999})  
 
官方写法
db.表名.findOne({})   查询符合条件的第一条数据
$gt  大于
$lt   小于
$gte   大于等于
$lte     小于等于
$eq   等于   :         使用在并列  ,或者  , 等条件中
$ne   不等于    不存在和不等于    都是不等于
$and  并列
$or    或者
$in  包含或者      同一字段     或者条件
$all  子集或者        必须是子集    交集不可查询
object 查询可以使用 对象.属性  的方式作Key
**** 当areey 中出现了object 会自动遍历object中的属性
mongoDB   中的数据结构
##### 如果差_id   必须用ObjectId(字符串)   这种的值取查询
```

### 4. 改: $修改器

- 针对单个字段进行修改

db.users.update({name:'alex.dsb'},{'$set':{name:'DSB',age:99}})

```python
修改符合查询条件的第一条数据
1. $set  强制修改   如果使用了不存在的字段     会将字段创建
db.users.update({name:"DSB"},{"$set":{age:73,gender:1}})
2. $unset    强制删除一个字段
db.users.update({age:73},{"$unset":{gender:0}})
3. $inc   引用增加
db.users.update({name:"DSB"},{"$inc":{age:1}})
db.users.update({name:"DSB"},{"$inc":{age:-2}})
db.users.update({age:73},{$inc:{age:-2}})
```

### 5. 针对$array(列表)的修改器  以及 $关键字的用法和特殊性

```python
list   append   remove  pop  extends
Array   $push   $pull   $pop   $pushALL   $pullAll
$push 在array中追加数据 
	db.users.update({name:"MJJ"},{"$push":{"hobby":"抽烟"}})	
$pushAll 在Array中批量增加数据 [] == list.extends
	db.users.update({name:"MJJ"},{"$pushAll":{"hobby":["喝酒","烫头"]}})
$pull   删除符合条件的所有数据   
	db.users.update({name:"MJJ"},{"$pull":{"hobby":"烫头"}})
$pop  只能删除第一条或者最后一条    负数第一个      正数最后一个  (0是一个正整数)
	db.users.update({name:"MJJ"},{"$pull":{"hobby":"烫头"}})
$pullall  批量删除符合条件的元素[]
	db.users.update({name:"MJJ"},{"$pullAll":{"hobby":[3,4,5]}})
hobey:{3,4,5}
$关键字  
储存符合条件的下标索引   -只能存储一层遍历的索引位置
$ = 9
	db.users.update({"hobby":10},{"$set":{"hobby.$":0}})
db.users.update()
### 官方推荐
db.表名.updateone({},{})   修改符合套件的第一条数据
db.表名.updateone({},{})    修改符合条件的所有数据   如果不写条件   更新所有数据
```

### 6. 删:

```python
db.Tables.remove(条件)   如果不写  删除所有   不推荐容易误操作  官方准备废弃掉
(如果条件没找到,就会删除所有数据)
### 官方推荐
db.表名.deleteOne({查询条件})
db.表名.deleteMany({查询条件})   删除符合条件的所有数据
```

### 7. 高级函数:

1. 排序 :sort

2. 筛选:limit

3. 跳过:skip

```python
db.users.find({}).sort({age:-1}) 依照age字段进行倒序
db.users.find({}).sort({age:1}) 依照age字段进行正序
```

```python
db.user.find({}).limit(1)    选取前1条    是几选取前几条
```

```python
db.user.find({}).skip(3)    跳过前3条数据  显示之后的所有数据
```

优先级

排序>跳过>筛选

page = 页码 = 1

count = 条目 = 2 

```python
db.users.find({}).limit(2).skip(2).sort({ age:-1 })
#公式
limit = count
skip = (page-1) * count
	
db.users.find({}).limit(count).skip((page-1)*count).sort({ age:-1 })
```

### 3. 数据类型:

​	ObjectID ：Documents 自生成的 _id # 自增ID 全世界唯一的计数ID
​	String： 字符串，必须是utf-8
​	Boolean：布尔值，true 或者false (这里有坑哦~在我们大Python中 True False 首字母大写)
​	Integer：整数 (Int32 Int64 你们就知道有个Int就行了,一般我们用Int32)
​	Double：浮点数 (没有float类型,所有小数都是Double)
​	Arrays：数组或者列表，多个值存储到一个键 (list哦,大Python中的List哦)
​	Object：如果你学过Python的话,那么这个概念特别好理解,就是Python中的字典,这个数据类型就是字典
​	Null：空数据类型 , 一个特殊的概念,None Null
​	Timestamp：时间戳
​	Date：存储当前日期或时间unix时间格式 (我们一般不用这个Date类型,时间戳可以秒杀一切时间类型)
​	看着挺多的,但是真要是用的话,没那么复杂,很简单的哦

### 4. python中应用MongoDB

pip install pymongo

```python
from pymango import MongoClient
from bson import ObjectId
MC = MongoClient('127.0.0.1',27017)
MongoDB = MC['S21DAY93']

res = MongoDB.Users.insert_one({'name':'YWB','age':999})

res.inserted_id   那个全宇宙的ID  
MongoDB.Users.find({"name":'YWB'})   
#find  获取一个生成器对象 ,find直接得到一个字典
MongoDB.Users.find_one({"_id":ObjectId(res.inserted_id)})
#上面等价于下面,结果
MongoDB.Users.find_one({"name":'YWB'})
res = MongoDB.Users.insert_many([{'name':'YWB','age':999},{'name':'YWB','age':999}])
res.inserted_ids   返回一个列表   链表元素为Object('6546164asdhakdhak')
ret = MongoDB.Users.find({'_id'}:ObjectId('askdjaksjdhasjdhkajd'))
ret  为一个生成器
```

### 改:

```python
MongoDB.Users.update_many({''})
```

### 删:

```python
delete_one({})
delete_many([{},{}])
```

### 高级函数

```python
#所有的key都必须带引号  除sort
from pymongo import DESCENDING,ASCENDING
res = MongoDB.Users.find({}).sort('age',-1)
for row in res:
    print(row)
```

- 排序：sort > 跳过：skip > 筛选：limit (优先级次序，mongodb内部逻辑)
- count()：计算查找到数据的条数

```python
# 升序排序，-1则为逆序
db.users.find({}).sort({age:1})
# 筛选前 -3(n) 条
db.users.find({}).limit(-3)
# 跳过前 3(n) 条
db.users.find({}).skip(3)
# 顺序任意
db.users.find({}).skip(4).limit(2)
db.users.find({}).limit(2).skip(4)
# 分页
page = 页码 = 1
count = 条目 = 2
db.users.find({}).limit(count).skip((page-1)*count).so

rt({字段:-1})
```

### 为什么用mongodb

读取很快  -json存储  不用原生sql语句 -数据存储方便

数据后期   非常方便   (后期数据整理卖给家长)

### 插入单条与多条优势劣势

优势,一次数据库操作,即可完成,全部内容添加

劣势 在29个断掉了,全部结束 需要 try  exception   Finally(进行回滚)

