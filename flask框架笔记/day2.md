## day2

1. ### 解决一个装饰器同时装饰2个以上视图函数

   - 使用functools wraps()  装饰inner  

     ```python
     内部将inner的__name__修改为当前传入的func函数的__name__
     ```

   - 使用 endpoint  参数,flask通过endpoint查找view函数   endpoint必须唯一

     ```python
     @app.route('/a', endpoint='end_a')
     def a():
         pass
     
     @app.route('/b', endpoint='end_b')
     def b():
         pass
     @app.route('/')
     def home():
         pass
     ```

     

### 2. route参数

1.  methods = [ ]/()

   - 请求方式,覆盖且不区分大小写

   ```python
   @app.route(rule, methods=['get', 'POST', 'options'])
   ```

   - 源码  getattr()    or  ('GET',)
   - set(item.upper()  for item  in  methods)  集合去重全大写得到传进来的methods

2. endpoint = None

   - 如果不传,则默认为endpoint = None 是  内部将赋值为装饰的函数的_ _name__
   - 可解决装饰器不能装饰多个函数的问题
   - 路由地址和视图函数之间的mapping

   ```python
   @app.route(rule, endpoint=None)
   def home():
       return 'ok!'
   ```

   

3. defaults = {'count':20 }

   - 默认值是20,且函数必须接受count参数

     ```python
     from flask import Flask
     url_for('end_a')
     url_for('home')
     # {'end_a':'/a', 'home': '/'}
     ```

     ```python
     @app.route(rule, endpoint=None, defaults={'count':20})
     def home(count):
         count = request.args.get('count', count)
         return f'200 ok!{count}'
     ```

4. strict_slashes=Flase
   - 是否严格遵循地址匹配
   - 严格url末尾有/ 匹配不上,非严格有/没/ 都能匹配上

5. redirect_to='/'

   - 永久重定向,状态码 308    根据HTTP协议版本不同还可能是301

   ```python
   @app.route(rule, endpoint=None, redirect_to='/')
   ```

### 3. 动态参数路由

- str: 可以接受一切,默认就是str

  ```python
  rule：`/ge t_music/<filename>`， `/home/<int:page>`， `/home/<ty>_<page>_<id>`，分页、获取文件、解决分类，解决正则路由
  ```

​	

- send_file(): 需要限定文件目录

  ```python
  @app.route('/home/<int:page>', endpoint=None,)
  def home(page):
      print(type(page))
      return '200 ok!'
  
  @app.route('/home/<ty>_<page>_<id>', endpoint=None,)
  def home(ty, page, id):
      pass
  ```

## 2. flask 中的配置

### 1. 初始化配置    实例化中可传参数

###### 1. template_folder

- 指定模板存放路径

```python
app = Flask(__name__, template_folder='template', static_folder='img', static_url_path='/static')
```

###### 2. static_folder='static'

- 设置静态文件的存放目录

###### 3. static_url_path = '/static'

- 静态文件访问路径

  ```python
  若设置  static_url_path = '/static'    ,
  static_folder='img'
  前端访问:
      img  src = "http://127.0.0.1:9000/static/img/1.jpg"    会去img/img/1.jpg  读取文件
  ```

- 自动拼接host

  ```python
  <img src='访问地址'>
  <img src='/img/1.jpg'>
  ```

###### 4. static_host = None 

-  其他服务器 获取静态文件的地址

###### 5. instance_path 

- 多app  未详细讲

## 3. Flask实例配置

### 1. 配置

1. default_config

- default_config = {}:默认配置 

  ```python
  'TESTING'：False，日志级别为Debug
  若为True 则为线上模式,修改代码无法重启服务器
  ```

  

- ```python
  "PERMANENT_SESSION_LIFETIME": timedelta(days=31),  #session生命周期31days
  ```

- JSONIFY_MIMETYPE='application/json'

  ```python
  "JSONIFY_MIMETYPE": "application/json"  #jsonify  的content-type 设置的属性
  # 开启 debug 模式
  app.debug = True
  # 使用session
  app.secret_key = 'R&w34hr*&%^R7ysdjh9qw78r^*&A%863'
  # session名称
  app.session_cookie_name = 'ah'
  # session生命周期，20s过期为 None
  app.permanent_session_lifetime = 20
  
  # respone头中content-type:xxx
  app.config['JSONIFY_MIMETYPE']='xxx
  
  ```

  

### 2. 自定义settings.py

```python
import hashlib,time

class DebugConfig(object):
    DEBUG = True
    SECRET_KEY = "#$%^&*($#$%^&*%$#$%^&*^%$#$%"
    PERMANENT_SESSION_LIFETIME = 3600
    SESSION_COOKIE_NAME = "I am Not Session"


class TestConfig(object):
    TESTING = True
    SECRET_KEY = hashlib.md5(f"{time.time()}#$%^&*($#$%^&*%$#$%^&*^%$#$%{time.time()}".encode("utf8")).hexdigest()
    PERMANENT_SESSION_LIFETIME = 360000
    SESSION_COOKIE_NAME = "#$%^&*($#$%^&*%$#$%^&*^%$#$%"
```

配置生效

```python
from settings.py import DubugConfig,TestConfig
app.config.from_object(DubugConfig)
app.config.from_object(TestConfig)
```

### 3. Blueprint    蓝图

- 不能被run的flask实例,没有config
- url_prefix  类似于django中的路由分发,防止多蓝图发生冲突

```python
from flask import Blueprint

bp = Blueprint('app01', __name__,url_prefix='/car')

@bp.route('/user')
def user():
    return 'I am app01!'
```

```python
from app01 import bp
app.register_blueprint(bp)
```

## 4. 特殊装饰器

- 类似于django中间件
- requset校验没通过过时，绕过view 执行全部after_request
- 只要有响应返回，af全部执行

### 1. @app.before_request

- 在请求进入视图函数之前,作出处理(顺序执行)

```python
@app.before_reqeust
def be1():
    print('i am be1')
    
@app.before_reqeust
def be2():
    print('i am be2')
```

### 2 . @app.after_request

- 在响应返回浏览器之前,得到响应之后
- 函数必须接受参数,res对象,必须返回res对象

```python
@app.after_request
def af1(res):
    print('i am in res1')
    return ers

@app.after_request
def af2(res):
    print('i am in res2')
    return ers
```

### 3. @app.errhandler(404)

- 监听状态码只能是4xx 和 5xx
- 需要接受错误信息,否则抛出异常

```python
@app.errorhandler(404)
def error404(error_message):
    print(error_message)
    return 'xxx' 		# 5种类型
```

