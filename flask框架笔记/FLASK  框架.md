# FLASK  框架

### 1. 框架对比

Django：

- Admin - Model

- Model

- Form Form

- Session

  评价   ：教科书式框架

Flask  

上面有的下面都没有

除了Session      有

评价： 以简为基准，一切从简

Django

​	优势：组件全，功能全，教科书

​	劣势： 占用资源 ，创建复杂度较高

Flask的优势

​	轻，快

​	劣势：先天不足，第三方组件稳定性较差（版本更新可能会不兼容）

不要使用工具中的插件创建Flask项目

Flask 框架中  的组件

jinja2     末班语言

Werkzeug   德语命名  

​     ：应用程序网关接口

​	wsgi  承载Flask框架



## 2. return类型

### 1. send_file    

返回一个文件

自动识别文件类型，并且在content-type中添加文件类型

content-type  ：文件类型

二进制文件第一行都有文件类型信息  所以都能被识别出来

类型:

```python
audio：mpeg        audio：表示音频    mpeg ： 表示音频格式
```

浏览器特性     可识别的Content-type   自动渲染

不认识的Content-type   会自动下载

### 2. jsonify

返回标准json格式字符串

本质：先序列化json字典，Content-type 中加入Appliaction/json ：文件类型

Flask 1.1.1 版本 可以直接返回字典，元祖，字符串，response instance，无需jsonify

## 装饰器

### 1. app.route()  路由匹配

视图函数装饰器

@app.route（‘login’，methods=['GET','POST']）

### 2. @app.template_global()

```
@app.template_global()
def ab(a,b):
    return a+b
    
#前端页面直接调用  类似simple_tag
{{ ab(1,5) }}
```

### 3. 自定义装饰器

```python
def war(func): # func = home
    def inner(*args,**kwargs):
        # 校验session
        if session.get("user"):
            ret = func(*args,**kwargs) # func = home
            return ret
        else:
            return redirect("/login")
    return inner

@app.route("/")
@war           # war(home) -> inner == == home
# 先要经过一次校验 才能执行或不执行视图函数
def home():
    # 校验用户登录 session
    #如果session。get（“user”） 存在
    # 如果不存在 返回Login
    return render_template("stu.html",stu=STUDENT,stu_l = STUDENT_LIST,stu_d = STUDENT_DICT)
```

解决flask中装饰器不能同时装饰两个视图函数的问题

- 装饰器inner 添加装饰器  @wraps(func)

  ```python
  from functools import wraps
  from flask import Flask
  app = Flask(__name__)
  # 基于 functools 更改内部函数的属性 __name__
  def war(func): # func = home
      @wraps(func)
      def inner(*args,**kwargs):
          ret = func(*args,**kwargs) # func = home
          return ret
      return inner
  ```

- 函数装饰器设置不同的   endpoint   属性 

  ```python
  @app.route("/a")
  @war
  def a():
      pass
  
  @app.route("/b")
  @war
  def a():
      pass
  ```

## 3. request      

利用了请求上下文管理（具体原理未知）

### 1. method 请求方式

##### 2. 从浏览器form提交的数据 （FormData）

​	request.form  获取FormData 中的数据  

​	request.form.to_dict()

​	request.form.get('key')

​	request.form['key']

##### 3. 获取URL上的参数

​	request.args   

​	request.args.to_dict()

​	request.args.get('key')

​	request.args['key']

### 4. 获取url和FormData中的数据

​	request.values     坑:   如果有key重复的话，url中的会覆盖掉Formdata中的

##### 5. 其他：

​	request.host      主机位   获取IP和端口

​	request.url      整个url   包含参数

​	request.cookies	获取浏览器请求时带上的字典

​	request.path   请求路径  路由地址  (第一次请求网页时会获取一个网页图标)

##### 6. 特殊：

- request.json # 请求中带有 content-type:application/json
  - 前端传送json串时,或ajax带有content-type:application/json时

- request.files  获取Form中的文件

```python
 # request.files # request中的文件 返回 FileStorage 中存在 方法 save 保存文件 ，filename 原始文件名
my_file = request.files.get("my_file")
filename = my_file.filename # 获取原始文件名
file_path = os.path.join("avatar",filename)
my_file.save(file_path)
```

- request.data
  - 如果是不能处理的类型,flask会将内容以json的格式存入到data中 ,可以通过request.data    的json.loads  得到浏览器传过来的不知道什么格式的内容  



### 4. 宏指令

- 在jinja2模版中使用

```jinja2
{% macro my_input(na, ty) %}
		<input type="{{ ty }}" name="{{ na }}">
{% endmacro %}
{{ my_input('username', 'text') }}
```

### jinja2      template 模板语言

{{  }}    应用或执行

{%    %}    逻辑引用

### dubug设置

debug=True  把后端的错误，映射到前端

### session设置

app.secret_key = "jkgogiklkjhnuihbkihn.i"   秘钥   

设置session

```
#  交由客户端保管机制
# {"user":"alexander.dsb.li"}
# Flask理念 - 一切从简 为服务器减轻压力
```

### 5. Markup

- 在view 函数中先生成标签在使用

```python
from flask import Markup
@app.route('/a')

def homea():
  	inp = Markup("<input type='submit' value='xxx'>")
    return render_template('a.html', btn=inp)

def my_input(na, ty):
  	s = f"<input type='{ty}' value='{na}'>"
    return Markup(s)
```

```jinja2
{{ btn | safe }} 或者使用
{{ my_input('username', 'text') }}
```

## day2

1. ### 解决一个装饰器同时装饰2个以上视图函数

   - 使用functools wraps()  装饰inner  

     ```python
     内部将inner的__name__修改为当前传入的func函数的__name__
     ```

   - 使用 endpoint  参数,flask通过endpoint查找view函数   endpoint必须唯一

     ```python
     @app.route('/a', endpoint='end_a')
     def a():
         pass
     
     @app.route('/b', endpoint='end_b')
     def b():
         pass
     @app.route('/')
     def home():
         pass
     ```


### 2. route参数

1. methods = [ ]/()

   - 请求方式,覆盖且不区分大小写

   ```python
   @app.route(rule, methods=['get', 'POST', 'options'])
   ```

   - 源码  getattr()    or  ('GET',)
   - set(item.upper()  for item  in  methods)  集合去重全大写得到传进来的methods

2. endpoint = None

   - 如果不传,则默认为endpoint = None 是  内部将赋值为装饰的函数的_ _name__
   - 可解决装饰器不能装饰多个函数的问题
   - 路由地址和视图函数之间的mapping

   ```python
   @app.route(rule, endpoint=None)
   def home():
       return 'ok!'
   ```

   

3. defaults = {'count':20 }

   - 默认值是20,且函数必须接受count参数

     ```python
     from flask import Flask
     url_for('end_a')
     url_for('home')
     # {'end_a':'/a', 'home': '/'}
     ```

     ```python
     @app.route(rule, endpoint=None, defaults={'count':20})
     def home(count):
         count = request.args.get('count', count)
         return f'200 ok!{count}'
     ```

4. strict_slashes=Flase

   - 是否严格遵循地址匹配
   - 严格url末尾有/ 匹配不上,非严格有/没/ 都能匹配上

5. redirect_to='/'

   - 永久重定向,状态码 308    根据HTTP协议版本不同还可能是301

   ```python
   @app.route(rule, endpoint=None, redirect_to='/')
   ```

### 3. 动态参数路由

- str: 可以接受一切,默认就是str

  ```python
  rule：`/ge t_music/<filename>`， `/home/<int:page>`， `/home/<ty>_<page>_<id>`，分页、获取文件、解决分类，解决正则路由
  ```

- send_file(): 需要限定文件目录

  ```python
  @app.route('/home/<int:page>', endpoint=None,)
  def home(page):
      print(type(page))
      return '200 ok!'
  
  @app.route('/home/<ty>_<page>_<id>', endpoint=None,)
  def home(ty, page, id):
      pass
  
  @app.route('/get_file')
  def get_file()
  	#前端name 为   my_file ,value  为文件
      my_file = request.files.get("my_file")
      filename = my_file.filename # 获取原始文件名
      file_path = os.path.join("avatar",filename)
      my_file.save(file_path)
      print(request.form.to_dict())
      return '200 ok'
          
  ```

## 2. flask 中的配置

### 1. 初始化配置    实例化中可传参数

###### 1. template_folder

- 指定模板存放路径

```python
app = Flask(__name__, template_folder='template', static_folder='img', static_url_path='/static')
```

###### 2. static_folder='static'

- 设置静态文件的存放目录

###### 3. static_url_path = '/static'

- 静态文件访问路径

  ```python
  若设置  static_url_path = '/static'    ,
  static_folder='img'
  前端访问:
      img  src = "http://127.0.0.1:9000/static/img/1.jpg"    会去img/img/1.jpg  读取文件
  ```

- 自动拼接host

  ```python
  <img src='访问地址'>
  <img src='/img/1.jpg'>
  ```

###### 4. static_host = None 

- 其他服务器 获取静态文件的地址

###### 5. instance_path 

- 多app  未详细讲

## 3. Flask实例配置

### 1. 配置

1. default_config

- default_config = {}:默认配置 

  ```python
  'TESTING'：False，日志级别为Debug
  若为True 则为线上模式,修改代码无法重启服务器
  ```

- "PERMANENT_SESSION_LIFETIME": timedelta(days=31),  #session生命周期31days

- JSONIFY_MIMETYPE='application/json'

  ```python
  "JSONIFY_MIMETYPE": "application/json"  #jsonify  的content-type 设置的属性
  # 开启 debug 模式    编码模式
  app.debug = True  #修改代码不需要重启,透传错误信息到浏览器页面
  # 使用session
  app.secret_key = 'R&w34hr*&%^R7ysdjh9qw78r^*&A%863'
  # session名称
  app.session_cookie_name = 'ah'
  # session生命周期，20s过期为 None
  app.permanent_session_lifetime = 20
  
  # respone头中content-type:xxx
  app.config['JSONIFY_MIMETYPE']='xxx
  
  ```
  
  ```python
  TWSTING  TRUE   测试模式  除了日志跟debug相同,其他完全相反
  ```


### 2. 自定义settings.py

```python
import hashlib,time

class DebugConfig(object):
    DEBUG = True
    SECRET_KEY = "#$%^&*($#$%^&*%$#$%^&*^%$#$%"
    PERMANENT_SESSION_LIFETIME = 3600
    SESSION_COOKIE_NAME = "I am Not Session"


class TestConfig(object):
    TESTING = True
    SECRET_KEY = hashlib.md5(f"{time.time()}#$%^&*($#$%^&*%$#$%^&*^%$#$%{time.time()}".encode("utf8")).hexdigest()
    PERMANENT_SESSION_LIFETIME = 360000
    SESSION_COOKIE_NAME = "#$%^&*($#$%^&*%$#$%^&*^%$#$%"
```

配置生效

```python
from settings.py import DubugConfig,TestConfig
app.config.from_object(DubugConfig)
app.config.from_object(TestConfig)
```

### 3. Blueprint    蓝图

- 不能被run的flask实例,没有config
- url_prefix  类似于django中的路由分发,防止多蓝图发生冲突

```python
from flask import Blueprint

bp = Blueprint('app01', __name__,url_prefix='/car')

@bp.route('/user')
def user():
    return 'I am app01!'
```

```python
from app01 import bp
app.register_blueprint(bp)
```

## 4. 特殊装饰器

- 类似于django中间件
- requset校验没通过过时，绕过view 执行全部after_request
- 只要有响应返回，af全部执行

### 1. @app.before_request

- 在请求进入视图函数之前,作出处理(顺序执行)

```python
@app.before_reqeust
def be1():
    print('i am be1')
    
@app.before_reqeust
def be2():
    print('i am be2')
```

### 2 . @app.after_request

- 在响应返回浏览器之前,得到响应之后
- 函数必须接受参数,res对象,必须返回res对象

```python
@app.after_request
def af1(res):
    print('i am in res1')
    return ers

@app.after_request
def af2(res):
    print('i am in res2')
    return ers
```

### 3. @app.errhandler(404)

- 监听状态码只能是4xx 和 5xx
- 需要接受错误信息,否则抛出异常

```python
@app.errorhandler(404)
def error404(error_message):
    print(error_message)
    return 'xxx' 		# 5种类型
```

### 5. CBV

1. CBV - RestAPI

   get post

```python
from flask import views 

class Login(views.MethodView):
	def get(*args,**kwargs):
		pass
app.add_url_rule("url",endpoint=None,view_func=Login.as_view(name="当前视图函数名,必须唯一,因为他是endpoint"))
```

### 6. redis

- 数据库  共16个库 库名 0-15  ,用来数据隔离

```python
select 8					# 切换 8 号库，默认 0 号库
set key value			# 设置一个健值对，哈希存储结构{key:value}
keys pattern			# 查询当前数据库中所有的key,如keys * 查询当前数据库中所有key
		 a*						# 查询以 a开头
  	 *n*					# 包含 n
...
get key						# 查询 key 对应的 value
# 多次设置同一个key 会被覆盖
```

- python 操作redis

```python
下载包 redis
from redis import Redis
redis_cli = Redis(port=6379, db=6)
redis_cli.set('name', 'echo')
```

### 7. flask-session\

- 通过对app的config 中的一些参数的设置达到第三方组件的实现
- 三方组件：pip install flask-session
- app.config最终在 app.default_config中
- settings.py中的DebugConfig中，不是大写英文的一律不生效

```python
from flask import Flask, request, session
from flask_session import Session

app = Flask(__name__)
# app.secret_key = '%^&*JBHJ%$*lkdsj'
app.config['SESSION_TYPE'] = 'redis'
app.config['SESSION_REDIS'] = Redis('127.0.0.1', 6379, db=10)
Session(app)

@app.route('/sets')
def sets():
  	session['key'] = 'henry'
  	return 'set ok!'
 
@app.route('/gets')
def gets():
  	return session.get('key')

if __name__ == '__main__':
		app.run()

```

A