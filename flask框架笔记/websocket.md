# websocket

### 1.定义：

WebSocket是通过单个TCP连接提供全双工（双向通信）通信信道的计算机通信协议。

### 1. 多人通讯的websocket

- pip install gevent-websocket

```python
#后端逻辑
from flask import Flask, request, render_template
from geventwebsocket.handler import WebSocketHandler
from geventwebsocket.server import WSGIServer
from geventwebsocket.websocket import WebSocket

app = Flask(__name__)
lis = []
count = 0


@app.route('/ws')
def func():
    print('777')
    my_socket = request.environ.get('wsgi.websocket')  # type:WebSocket
    lis.append(my_socket)
    print('888')
    while True:
        try:
            print(666)
            msg = my_socket.receive()
        except:
            return
        print(msg)
        for i in lis:
            if i == my_socket:
                continue
            try:
                i.send(msg)
            except:
                pass


@app.route('/soc')
def aaa():
    return render_template('wechat.html')


if __name__ == '__main__':
    http_server = WSGIServer(('0.0.0.0', 9999), app, handler_class=WebSocketHandler)
    http_server.serve_forever()
```

```python
#html  文件   
#  知识点 :   onmessage  当接收到服务器发送的数据时,执行
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Title</title>
</head>
<body>
<input type="text" id="content" value="">
<button onclick="send_message()">发送</button>
<div id="content_list">
    <p id="msg">

    </p>
</div>
<script type="application/javascript">
    var ws = new WebSocket('ws://127.0.0.1:9999/ws');
    ws.onmessage = function (messageEvent) {
        console.log(messageEvent.data);

        var ptag = document.createElement('p');
        ptag.innerText = messageEvent.data;
        document.getElementById('content_list').appendChild(ptag);
    };
    function send_message() {
        var msg = document.getElementById('content').value;
        {#console.log(msg);#}
        ws.send(msg);
    }
</script>
</body>
</html>
```

### 2. 一对一的通讯

- 后端构建字典的数据结构

  - 用户名对应 为key  对应于服务器的链接

  

- 前后端字典数据传送需json

- handler_class  替换为Websocket对象(此对象支持HTTP请求和websocket请求)

  

```python
import json

from flask import Flask, request, render_template
from geventwebsocket.handler import WebSocketHandler
from geventwebsocket.server import WSGIServer
from geventwebsocket.websocket import WebSocket


app = Flask(__name__)

dic={}

@app.route('/ws/<username>')
def funa(username):
    print('666666666')
    my_socket = request.environ.get('wsgi.websocket')   #type:WebSocket
    dic[username] = my_socket
    print(my_socket,dic)
    while True:
        msg = my_socket.receive()
        print(msg)
        messg = json.loads(msg)
        sender = messg.get('sender')
        dic[sender].send(msg)

@app.route('/ind')
def index():
    return render_template('onetoone.html')


if __name__ == '__main__':
    http_server = WSGIServer(('127.0.0.1', 8827), app, handler_class=WebSocketHandler)
    http_server.serve_forever()

```

```python
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Title</title>
</head>
<body>
<p>发送人<input type="text" id="reciver" value="">
    <button onclick="login()"> 登录聊天室</button>
</p>

接收人<input type="text" id="sender">
data<input type="text" id="data">
<button onclick="send_message()">发送</button>
<div id="content_list">
    <p id="msg">

    </p>
</div>
<script type="application/javascript">


    function login() {
        var reciver = document.getElementById('reciver').value;
        ws = new WebSocket('ws://127.0.0.1:8827/ws/'+reciver);
        ws.onmessage = function (messageEvent) {


            var ptag = document.createElement('p');
            var obj = JSON.parse(messageEvent.data);
            ptag.innerText = obj.sender + ':' + obj.data;

            document.getElementById('content_list').appendChild(ptag);
        };
    }

    function send_message() {
        console.log('666');
        var data = document.getElementById('data').value;
        var sender = document.getElementById('sender').value;
        var reciver = document.getElementById('reciver').value;
        var msg = {
            'data': data,
            'reciver': reciver,
            'sender': sender

        };

        var pag = document.createElement('p');


        pag.innerText = data + ':我';
        pag.style.cssText = "text-align: right";
        document.getElementById('content_list').appendChild(pag);
        ws.send(JSON.stringify(msg));
    }
</script>
</body>
</html>
```

