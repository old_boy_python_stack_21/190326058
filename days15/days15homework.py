# sys.path.append("/root/mods")的作用？
''''''
'''
将"/root/mods"路径添加到sys.path 然后引用模块的时候可以从这个路径下查找引用
'''
# 字符串如何进行反转？
'''
切片 步长为-1
'''
# 不用中间变量交换a和b的值
# a = 1
# b = 2
# a,b = b,a
# *args和**kwargs这俩参数是什么意思？我们为什么要用它。
'''
*args 为0个或若干个位置参数
**kwargs  为0个或者多个关键字参数
不确定要传几个参数的时候
'''
# 函数的参数传递是地址还是新值？
'''
传的是地址
'''
# 看代码写结果：
# my_dict = {'a':0,'b':1}
# def func(d):
#     d['a'] = 1
#     return d
#
# func(my_dict)
# my_dict['c'] = 2
# print(my_dict)
'''
{'a':1,'b':1,'c':2}
'''
# 什么是lambda表达式
'''
匿名函数   lamada 参数：返回值
'''
# range和xrang有什么不同？
'''
在python2中xrange 不会直接开辟内存，会放在哪知道用的时候一点一点开辟
range 会直接开辟出 内存
python3只有range 跟py2的xrange原理相同
'''
# "1,2,3" 如何变成 ['1','2','3',]
a = '1,2,3'.split(',')
print(a)
# ['1','2','3'] 如何变成 [1,2,3]
# lis = ['1','2','3']
# for i in range(3):
#     a = lis.pop()
#     lis.insert(0,int(a))
# print(lis)
# def f(a,b=[]) 这种写法有什么陷阱？
'''
如果函数调用后没传B的参数，且在内部向b列表中加入了元素
那么下次再调用函数 不传b参数 ，列表则不再是空列表 
'''
# 如何生成列表 [1,4,9,16,25,36,49,64,81,100] ，尽量用一行实现
'''
v= [i*i for i in range(1,11)]
print(v)
'''
# python一行print出1~100偶数的列表, (列表推导式, filter均可)
'''
v = filter(lambda i:i%2==0 ,range(1,101))
print(list(v))
'''
'''
v = [ i for i in range(1,100) if i % 2 == 0]
print(v)
'''
# 把下面函数改成lambda表达式形式
# ------------------------------------------------------------------

# def func():
#     result = []
#     for i in range(10):
#         if i % 3 == 0:
#             result.append(i)
#     return result
#
'''
result = lambda :[i for i in range(10)if i % 3 == 0]
print(result)
'''
# -------------------------------------------------------------
# 如何用Python删除一个文件？
'''
import os
os.remove(path)
'''
# 如何对一个文件进行重命名？
'''
import os
os.rename('nb','sb')
'''
# python如何生成随机数？
'''
import random
while True:
    a = random.randint(1,100)
    print(a)
'''
# 从0-99这100个数中随机取出10个数，要求不能重复，可以自己设计数据结构。
'''
import random
s = set()
while True:
    a = random.randint(0,99)
    s.add(a)
    if len(s) == 10:
        break
print(s)
'''
# 用Python实现 9*9 乘法表 （两种方式）
'''
for i in range(1,10):
    for r in range(1,i+1):
        print(str(i)+'*'+str(r),end='   ')
    print('')
'''
'''
num = 1

for i in range(1,10):
    num = 1
    while num <= i:


        print(str(i)+ '*' + str(num),end= '   ')
        num += 1
    print('')
'''
# 请给出下面代码片段的输出并阐述涉及的python相关机制。
#
# def dict_updater(k,v,dic={}):
#     dic[k] = v
#     print(dic)
# dict_updater('one',1)
# dict_updater('two',2)
# dict_updater('three',3,{})
'''
{'one':1}
{'one':1,'two':2}
{'three':3}
如果不传dic参数 dic参数会默认为空字典  每次调用 都会往这个空字典里加入建和值
'''
# 写一个装饰器出来
'''
def func(f):
    def inner(*args,**kwargs):
        a = f(*args,**kwargs)
        return a
    return inner

'''
# 用装饰器给一个方法增加打印的功能
'''
def func(f):
    def inner(*args,**kwargs):
        print('千鸟2122')
        a = f(*args,**kwargs)
        return a
    return inner

'''
# as 请写出log实现(主要功能时打印函数名)
'''
def log(f):
    def inner(*args,**kwargs):
        print('call %s()'%(f.__name__))
        a = f(*args,**kwargs)
        return a
    return inner

@log
def now():
    print("2013-12-25")



now()

# 输出
# call now()
# 2013-12-25
'''
# 向指定地址发送请求，将获取到的值写入到文件中。

import requests # 需要先安装requests模块：pip install requests
import json
'''
response = requests.get('https://www.luffycity.com/api/v1/course_sub/category/list/')
print(response.text)
v = json.loads(response.text,encoding='utf8')
print(v)
s = str(v['data'])
with open('catelog.txt','w',encoding='utf-8')as f1:
    f1.write(s)


# 获取结构中的所有name字段，使用逗号链接起来，并写入到 catelog.txt 文件中。
"""
[
    {'id': 1, 'name': 'Python', 'hide': False, 'category': 1}, 
    {'id': 2, 'name': 'Linux运维', 'hide': False, 'category': 4}, 
    {'id': 4, 'name': 'Python进阶', 'hide': False, 'category': 1}, 
    {'id': 7, 'name': '开发工具', 'hide': False, 'category': 1}, 
    {'id': 9, 'name': 'Go语言', 'hide': False, 'category': 1},
    {'id': 10, 'name': '机器学习', 'hide': False, 'category': 3}, 
    {'id': 11, 'name': '技术生涯', 'hide': False, 'category': 1}
]
"""

'''
# 请列举经常访问的技术网站和博客
# 博客园  码云

# 请列举最近在关注的技术
# python

# 请列举你认为不错的技术书籍和最近在看的书（不限于技术）
# python企业面试题锦集