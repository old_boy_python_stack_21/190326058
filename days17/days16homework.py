# 列举你常见的内置函数
''''''

'''
list()
dict()
str()
set()
print()input()
bin()
oct()
int()
hex()
ord()
chr()
max()
min()
sum()

`
'''
# 列举你常见的内置模块？
import time    #常用
import datetime   #常用
from datetime import datetime,timezone
import os      #常用
import shutil
import hashlib
import random
import getpass
import json

# json序列化时，如何保留中文？
'''
a = 'lka阿拉'
v = json.dumps(a,ensure_ascii=False)
'''
# 程序设计：用户管理系统

"""
功能：
	1.用户注册，提示用户输入用户名和密码，然后获取当前注册时间，最后将用户名、密码、注册时间写入到文件。
	2.用户登录，只有三次错误机会，一旦错误则冻结账户（下次启动也无法登录，提示：用户已经冻结）。
	
"""

import time
import datetime
from datetime import datetime,timezone,timedelta
import sys
import re
def register():
    '''
    用户注册
    :return:
    '''
    dic = {'con':3}
    name = input('输入用户名：')
    pasword = input('输入密码：')
    pasword1 = input('再次输入确密码：')
    if pasword != pasword1:
        print('密码不同请重新注册')
        return register()
    print('注册成功')
    with open('data.txt','a',encoding='utf-8')as f:
        ctime = datetime.now()
        ctime1 = ctime.strftime('%Y-%m-%D-%H-%M-%S')
        dic[name] = pasword
        mesage = str(dic) + '|' + ctime1
        f.write(mesage+'\n')
def login():
    '''
    用户登录
    :return:
    '''
    num = 3
    count = False
    with open('data.txt','r',encoding='utf-8') as  f1:
        lab = []
        for line in f1:
            line = line.strip()
            a,b=line.split('|')
            dicc = eval(a)
            lab.append(dicc)
    while True:
        with open('freeze.txt', 'a+', encoding='utf-8')as f2:
            name = input('请输入账号：')
            pasword = input('请输入密码:')
            f2.seek(0)
            for line in f2:
                if name in line:
                    print('您的账户已冻结')
                    sys.exit(1)
            for i in lab:
                if name in i and pasword == i[name]:
                    print('登录成功')
                    count = True
                elif name in i :
                    if pasword != i[name]:
                        print('密码错误')
                        i['con'] = i['con'] + 1
                        if i['con'] == 6:
                            print('您的账户已冻结')
                            f2.write(str(i)+ '\n')
                            sys.exit(1)
                    count = True
        if count == False:
            print('您的账户不存在,请重新输入')
            continue
def shop():
    while True:
        print('''欢迎来到老子购物商城
        1.注册
        2.登录
        ''')
        dic = {'1':register,'2':login}
        choice = input('请选择序号或者N退出：')
        if choice.upper() == 'N':
            return
        if  dic.get(choice) == None:
            print('您输入的选择有误，请重新输入')
            continue
        else:
            dic.get(choice)()
shop()
# --------------------------------------------------------------------------------
# 有如下文件，请通过分页的形式将数据展示出来。【文件非常小】
#
# 商品|价格
# 飞机|1000
# 大炮|2000
# 迫击炮|1000
# 手枪|123
# ...
'''
with open('wolailuo.txt','r',encoding='utf-8') as f1:
    lis = []
    for line in f1:
        line = line.strip()
        lis.append(line)
    count,nvl = divmod(len(lis),10)
    if nvl != 0:
        count += 1
    data = input('请输入查看的页码0-%d页'%count)
    data = int(data)
    start = (data-1)* 10
    end = data * 10
    if end < len(lis):
        print(lis[start:])
    else:
        print(lis[start:end])

'''
# 有如下文件，请通过分页的形式将数据展示出来。【文件非常大】

# 商品|价格
# 飞机|1000
# 大炮|2000
# 迫击炮|1000
# 手枪|123
...
'''
def func():
    with open(r'F:\qianniaoleiqie\woailuo.txt','r',encoding='utf-8') as f1:
        count = 0
        while True:

            lis = []
            for  i in range(10):
                f1.seek(count)
                a = f1.readline()
                lis.append(a)
                count = f1.tell()
            for i in lis:
                print(i)
            yield '下一页'

a = func()

a.__next__()
while True:
    s = input('输入Q查看下一页或任意键退出')
    if s.upper() == 'Q':
        a.__next__()
    else:
        break

'''
# 程序设计：购物车

"""
有如下商品列表 GOODS_LIST，用户可以选择进行购买商品并加入到购物车 SHOPPING_CAR 中且可以选择要购买数量，购买完成之后将购买的所有商品写入到文件中【文件格式为：年_月_日.txt】。

注意：重复购买同一件商品时，只更改购物车中的数量。
"""
# 购物车
'''
SHOPPING_CAR = {}


# 商品列表
GOODS_LIST = [
    {'id':1,'title':'飞机','price':1000},
	{'id':3,'title':'大炮','price':1000},
	{'id':8,'title':'迫击炮','price':1000},
	{'id':9,'title':'手枪','price':1000},
]
def func():
    while True:
        count = False
        dicc = {}
        name = input('输入商品号或者N退出：')
        if name.upper() == 'N':
            break
        for i in GOODS_LIST:
            if int(name)  in i.values():
                number = int(input('输入商品数量：'))
                if name in SHOPPING_CAR:
                    dicc[name] = number
                    SHOPPING_CAR[name] = dicc[name] + SHOPPING_CAR[name]
                else:
                    SHOPPING_CAR[name] = number
                count = False
                break
            else:
                count = True
        if count == True:
            print('您输入的商品号不存在，请重新输入')
            continue
    with open('woailuo.txt', 'a', encoding='utf-8') as f3:
            for key, val in SHOPPING_CAR.items():
                ctime = datetime.now()
                ctime1 = ctime.strftime('%Y-%m-%D-%H-%M-%S')
                s = key + ' ' + str(val) + ' ' + ctime1
                f3.write(s+'\n')

func()

'''''
# 程序设计：沙河商城
"""
功能：
	1.用户注册，提示用户输入用户名和密码，然后获取当前注册时间，最后将用户名、密码、注册时间写入到文件。
	2.用户登录，只有三次错误机会，一旦错误则冻结账户（下次启动也无法登录，提示：用户已经冻结）。
	3.商品浏览，分页显示商品（小文件）； 用户可以选择商品且可以选择数量然后加入购物车（在全局变量操作），
	  不再购买之后，需要讲购物车信息写入到文件，文件要写入到指定目录：
		shopping_car(文件夹)
			- 用户名A(文件夹)
				2019-11-11-09-59.txt
				2019-11-12-11-56.txt
				2019-12-11-11-47.txt
			- 用户B(文件夹)
				2019-11-11-11-11.txt
				2019-11-12-11-15.txt
				2019-12-11-11-22.txt
	  注意：重复购买同一件商品时，只更改购物车中的数量。
	4.我的购物车，查看用户所有的购物车记录，即：找到shopping_car目录下当前用户所有的购买信息，并显示：
		2019-11-11-09-59
			飞机|1000|10个
			大炮|2000|3个
		2019-11-12-11-56.txt
			迫击炮|10000|10个
			手枪|123|3个

	5.用户未登录的情况下，如果访问 商品流程 、我的购物车 时，提示登录之后才能访问，让用户先去选择登录（装饰器实现）。
"""
import os
import time
import datetime
from datetime import datetime,timezone,timedelta
import sys
import re
count = False
name = ''
lis = []
def register():
    '''
    用户注册
    :return:
    '''
    dic = {'con':3}
    name = input('输入用户名：')
    pasword = input('输入密码：')
    pasword1 = input('再次输入确密码：')
    if pasword != pasword1:
        print('密码不同请重新注册')
        return register()

    with open('data.txt','a+',encoding='utf-8')as f:
        f.seek(0)
        for line in f:
            if line:
                line = line.strip()
                a,b = line.split('|')
                a = eval(a)
                for r in a:
                    if r == name:
                        print('账户已存在')
                        sys.exit(1)
        ctime = datetime.now()
        ctime1 = ctime.strftime('%Y-%m-%d-%H-%M-%S')
        dic[name] = pasword
        mesage = str(dic) + '|' + ctime1 + '\n'
        f.write(mesage+'\n')
    print('注册成功')
def login():
    '''
    用户登录
    :return:
    '''
    num = 3
    global count, name

    with open('data.txt','r',encoding='utf-8') as  f1:
        lab = []
        for line in f1:
            line = line.strip()
            a,b=line.split('|')
            dicc = eval(a)
            lab.append(dicc)
    while True:
        with open('freeze.txt', 'a+', encoding='utf-8')as f2:   #freeze 冻结   冻结名单文件
            name = input('请输入账号：')
            pasword = input('请输入密码:')
            f2.seek(0)
            for line in f2:
                if name in line:
                    print('您的账户已冻结')
                    sys.exit(1)
            for i in lab:
                if name in i and pasword == i[name]:
                    print('登录成功')

                    count = 'alex'
                    return count
                elif name in i :
                    if pasword != i[name]:
                        print('密码错误')
                        i['con'] = i['con'] + 1
                        if i['con'] == 6:
                            print('您的账户已冻结')
                            f2.write(str(i)+ '\n')
                            sys.exit(1)
                    count = True
        if count == False:
            print('您的账户不存在,请重新输入')
            continue

def append_goods():
    '''
    添加商品程序
    :return:
    '''

    li = []
    while True:
        val = False
        con = False
        goods_name = input('输入添加的商品名或者N退出：')
        if goods_name.upper() == 'N':
            break
        number = input('输入添加的商品数量')
        for r in li:
            if goods_name in r:
                r = r.strip()
                c = r.split('|')
                d = str(int(number) + int(c[1]))
                s = c[0] + '|' + d + '\n'
                li.insert(0,s)
                r = r + '\n'
                li.remove(r)
                print(li)
                break
        for i in lis:
            if goods_name in i:
                i = i.strip()
                con = True
                b = i.split('|',-1)
                s = b[0] + '|' + number + '\n'
                print(li)
                for r in li:
                    if goods_name in r:
                        val = True
                if val == False:
                    li.append(s)
                print(li)
        if con == False:
            print('输入的商品不存在，请重新输入')
            continue
        else:
            print('添加成功')
    ctime = datetime.now()
    ctime2 = ctime.strftime('%Y-%m-%d-%H-%M-%S')
    file_path = r'F:\qianniaoleiqie\shopping_car\%s' % (name,)
    if not os.path.exists(file_path):
        os.makedirs(file_path)
    file_pp = file_path + '\\' + ctime2 + 'txt'
    with open(file_pp,'a',encoding='utf-8')as f3:
        for i in li:
            f3.write(i)
def ornament(f):
    '''
    装饰器，装饰查看购物车函数等
    :return:
    '''
    def inner(*args,**kwargs):
        if count != 'alex':
            print('请先登录')
            return
        a = f(*args,**kwargs)
        return a
    return inner
@ornament
def show_goods():
    '''
    分页展示商品
    :return:
    '''
    global lis
    lis = []

    with open('goods_list.txt','r',encoding='utf-8') as f1:
        lis = []
        for line in f1:
            line = line.strip()
            lis.append(line)
        count,nvl = divmod(len(lis),10)
        if nvl != 0:
            count += 1
        while True:
            data = input('请输入查看的页码0-%d页或输入Q添加商品或输入N退出'%count)
            if data.upper() == 'N':
                return
            elif data.upper() == 'Q':
                return append_goods()
            data = int(data)
            start = (data-1)* 10
            end = data * 10
            if end < len(lis):
                for i in lis[start:end]:
                    print(i)
            else:
                for i in lis[start:]:
                    print(i)
@ornament
def shopping_car():
    '''
    查看购物车商品时间等
    :return:
    '''
    result = os.walk(r'F:\qianniaoleiqie\shopping_car\%s'%name)
    for a,b,c in result:
        for item in c:
            print(item)
            with open(r'F:\qianniaoleiqie\shopping_car\%s\%s'%(name,item),'r',encoding='utf-8') as f1:
                for line in f1:
                    print(line)
def shop():
    while True:
        print('''欢迎来到老子购物商城
        1.注册
        2.登录
        3.浏览商品
        4.查看购物车
        ''')
        dic = {'1':register,'2':login,'3':show_goods,'4':shopping_car}
        choice = input('请选择序号或者N退出：')
        if choice.upper() == 'N':
            return
        if  dic.get(choice) == None:
            print('您输入的选择有误，请重新输入')
            continue
        else:
            a = dic.get(choice)()
shop()



# 9.请使用第三方模块xlrd读取一个excel文件中的内容。【课外】
'''
import xlrd
data = xlrd.open_workbook(r'C:\Users\Administrator\Desktop\data.xlsx')
# print(data.sheets())
# for i in data.sheets():
#     print(i)
# v = data.sheet_names()
# print(v)
'''
# 按行读取
'''
sheet1 = data.sheet_by_name('Sheet1')
# a = sheet1.row(0)#读取第一行返回一个列表
# for i in a:
#     print(i)
for i in range(10):
    a = sheet1.row(i)
    print('  ')
    print('第%d行数据' % (i + 1))

    for r in a:
        print(r,end='    ')
'''