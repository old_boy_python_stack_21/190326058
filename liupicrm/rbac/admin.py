from django.contrib import admin

# Register your models here.

from django.contrib import admin
from rbac import models



class PermissionConf(admin.ModelAdmin):
    list_display = ['id','name','url_title','menu']
    list_editable = ['name', 'url_title','menu']


admin.site.register(models.Power, PermissionConf)
admin.site.register(models.Status)
admin.site.register(models.Userbod)
admin.site.register(models.Menu)