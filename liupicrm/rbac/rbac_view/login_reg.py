from django.shortcuts import render, redirect,reverse
from django.http import FileResponse
from rbac.servers.quanxian import get_power

from rbac import models

def login(request):
    error = ''
    if request.method == 'POST':
        name = request.POST.get('username')
        password = request.POST.get('password')
        obj = models.Userbod.objects.filter(username=name,password=password).first()
        if obj:
            request.session['is_login'] = True
            url_i = url = obj.status.filter(powers__url_title__isnull=False).values('powers__url_title').distinct()
            url = obj.status.filter(powers__url_title__isnull=False).values('powers__url_title','powers__name','powers__menu_id','powers__menu__title','powers__menu__i_con').distinct()
            lis = []
            print(url)
            for i in url_i:
                lis.append(i['powers__url_title'])
            request.session['sta'] = lis
            print('+'*10,lis,'+'*10)
            dat = get_power(list(url))
            request.session['pom'] = dat
            request.session['user_id'] = obj.pk
            return redirect(reverse('index'))
        error = '账号或密码错误'
    return render(request,'login.html',{'error':error})

def index(request):


    return render(request,'layout.html')