from django.utils.deprecation import MiddlewareMixin
from django.urls import reverse
from django.shortcuts import redirect, HttpResponse
from django.conf import settings
import re

class Md1(MiddlewareMixin):
    def process_request(self,request):
        path = request.path_info
        if path in [reverse('login')]:
            return
        if re.match('/admin/',path):
            return
        is_login = request.session.get('is_login')
        if not is_login:
            return redirect(reverse('login'))
        if path in [reverse('index')]:
            return
        sta = request.session.get('sta')

        for i in sta:
            if re.match(i,path):
                print(i,path)
                return
        #
        # for i in pom.keys():
        #     for r in pom[i]['children']:
        #         if re.match(r['url'],path):
        #             return
        return HttpResponse('访问权限不够')