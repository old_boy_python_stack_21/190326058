from django.shortcuts import render, redirect, reverse, HttpResponse
from django.core.exceptions import ValidationError
from liupiapp import models
from django import forms
import hashlib
from liupiapp import form_view
from django.db.models import Q
from utils.pagination import Pagination
from django.views import View
from django.forms import modelformset_factory
from django.db import transaction
from django.views.decorators.cache import cache_page

# def login(request):
#     '''
#     登录功能
#     :param request:
#     :return:
#     '''
#     error = ''
#     if request.method == 'POST':
#         username = request.POST.get('username')
#         password = request.POST.get('password')
#         print(username, password)
#         md5 = hashlib.md5()
#         md5.update(password.encode('utf-8'))
#         # print(md5.hexdigest())
#         objlis = models.UserProfile.objects.all()
#         for obj in objlis:
#             print(obj.username, obj.password)
#             if obj.username == username and obj.password == md5.hexdigest():
#                 ret = redirect(reverse('customer_list'))
#                 ret.set_cookie('use', username)
#                 return ret
#             else:
#                 print('777')
#                 error = '用户名或密码错误'
#     return render(request, 'login.html', {'error': error})
def login(request):
    if request.method == 'POST':
        username = request.POST.get('username')
        password = request.POST.get('password')
        md5 = hashlib.md5()
        md5.update(password.encode('utf-8'))
        obj = models.UserProfile.objects.filter(username=username, password=md5.hexdigest(), is_active=True).first()
        if obj:
            # 登录成功
            request.session['is_login'] = True
            request.session['user_id'] = obj.pk
            return redirect(reverse('customer_list'))
            # return redirect('index')
        else:
            return render(request, 'login.html', {'error': '用户名或密码错误'})
    return render(request, 'login.html')

def reg(request):
    '''
    注册功能
    :param request:
    :return:
    '''
    regform = Regform()
    if request.method == 'POST':
        form_obj = Regform(request.POST)
        if form_obj.is_valid():
            form_obj.save()
            return redirect(reverse('login'))
    return render(request, 'reg.html', {'regform': regform})


# def index(request):
#     url_path = request.path_info
#     cus_obj_lis = ''
#     if not request.COOKIES.get('use'):
#         return redirect(reverse('login'))
#     user_name = request.COOKIES.get('use')
#
#     if request.path_info == reverse('index'):
#         cus_obj_lis = models.Customer.objects.filter(consultant=None)
#         if request.GET.get('search'):
#             mo = request.GET.get('search')
#             print(mo)
#             cus_obj_lis = models.Customer.objects.filter(Q(name__contains=mo)|Q(phone__contains=mo)|Q(qq__contains=mo),Q(consultant=None))
#     elif request.path_info == reverse('my_customer'):
#         cus_obj_lis = models.Customer.objects.filter(consultant__username=user_name)
#         if request.GET.get('search'):
#             mo = request.GET.get('search')
#             cus_obj_lis = models.Customer.objects.filter(Q(name__contains=mo)|Q(phone__contains=mo)|Q(qq__contains=mo),Q(consultant__name=user_name))
#     elif request.path_info == reverse('mohu'):
#         mo = request.GET.get('search')
#
#         cus_obj_lis = models.Customer.objects.filter(
#             Q(Q(name__contains=mo) | Q(phone__contains=mo) | Q(qq__contains=mo)) & Q(consultant=None))
#
#     page = Pagination(request.GET.get('page', 1), len(cus_obj_lis), 20, 15)
#     # max_page, float_age = divmod(len(cus_obj_lis), 10)
#     # if float_age:
#     #     max_page += 1
#     # num = 11
#     # page_num = request.GET.get('page', '1')
#     # if not page_num:
#     #     page_num = 1
#     # page_num = int(page_num)
#     # if page_num < 1:
#     #     page_num = 1
#     # if page_num > max_page:
#     #     page_num = max_page
#     # start_index = (page_num - 1) * 10
#     # end_index = page_num * 10
#     # if end_index > max_page * 10:
#     #     end_index = max_page
#     # start_page = 1
#     # end_page = max_page
#     # if page_num < 6:
#     #     start_page = 1
#     #     end_page = 10
#     # elif page_num + 5 > max_page and max_page - 10 > 0:
#     #     end_page = max_page
#     #     start_page = max_page - 10
#     # elif page_num >= 6:
#     #     start_page = page_num - 5
#     #     end_page = page_num + 5
#     # lis = []
#     # for i in range(start_page, end_page + 1):
#     #     if i == page_num:
#     #         lis.append('<li class="active" ><a href="?page={}">{}</a></li>'.format(i, i))
#     #     else:
#     #         lis.append('<li><a href="?page={}">{}</a></li>'.format(i, i))
#     # print(start_index, start_page, end_page, end_index, page_num)
#     # if start_index < 0:
#     #     start_index = 1
#     return render(request, 'index.html',
#                   {'cus_obj_lis': cus_obj_lis[page.start:page.end], 'page_html': page.page_html, 'url_path': url_path})
class CustomerList(View):

    def get(self, request, *args, **kwargs):

        # print(request.GET,type(request.GET))
        # print(request.GET.urlencode())
        # request.GET._mutable = True  # 可编辑
        # request.GET['page'] = 1
        # print(request.GET.urlencode())

        q = self.search(['qq', 'name', 'phone', 'consultant__name'])

        if request.path_info == reverse('customer_list'):
            all_customer = models.Customer.objects.filter(q, consultant__isnull=True)
        else:
            all_customer = models.Customer.objects.filter(q, consultant=request.user_obj)

        page = Pagination(request.GET.get('page', 1), all_customer.count(), request.GET.copy(), 2)

        return render(request, 'customer_list.html',
                      {'all_customer': all_customer[page.start:page.end], 'page_html': page.page_html})

    def post(self, request, *args, **kwargs):
        action = request.POST.get('action')
        if not hasattr(self, action):
            return HttpResponse('非法操作')
        getattr(self, action)()
        return self.get(request, *args, **kwargs)

    def multi_apply(self):
        # 公户转私户
        pk = self.request.POST.getlist('pk')
        if len(pk)+ models.Customer.objects.filter(consultant=self.request.user_obj).count() > 20:
            return HttpResponse('要脸不，还抢')
        # 方法一
        try:
            with transaction:
                queryset = models.Customer.objects.filter(pk__in=pk,consultant=None).select_for_update()
                if queryset.count()==len(pk):
                    queryset.update(consulant=self.request.user_obj)
                else:
                    return HttpResponse('手速太慢')
        except Exception as e:
            print(e)
        # 方法二
        # self.request.user_obj.customers.add(*models.Customer.objects.filter(pk__in=pk))

    def multi_pub(self):
        # 私户转公户
        pk = self.request.POST.getlist('pk')

        # 方法一
        models.Customer.objects.filter(pk__in=pk).update(consultant=None)

        # 方法二
        # self.request.user_obj.customers.remove(*models.Customer.objects.filter(pk__in=pk))

    def search(self, field_list):
        # 构建Q对象
        # Q(Q(qq__contains=query) | Q(name__contains=query) | Q(phone__contains=query))

        query = self.request.GET.get('query', '')
        q = Q()
        q.connector = 'OR'
        for field in field_list:
            q.children.append(Q(('{}__contains'.format(field), query)))
        return q

class Regform(forms.ModelForm):
    password = forms.CharField(min_length=6,
                               widget=forms.PasswordInput(attrs={'placeholder': '您的密码', 'autocomplete': 'off'}))
    re_password = forms.CharField(min_length=6,
                                  widget=forms.PasswordInput(attrs={'placeholder': '您的确认密码', 'autocomplete': 'off'}))

    class Meta:
        model = models.UserProfile
        fields = '__all__'
        exclude = ['is_active']
        widgets = {
            'username': forms.EmailInput(attrs={'placeholder': '您的用户名', 'autocomplete': 'off'}),
            'mobile': forms.TextInput(attrs={'placeholder': '您的手机号', 'autocomplete': 'off'}),
            'name': forms.TextInput(attrs={'placeholder': '您的真实姓名', 'autocomplete': 'off'}),
        }

    def clean(self):
        password = self.cleaned_data.get('password')
        re_password = self.cleaned_data.get('re_password')
        print(password, re_password)
        if password == re_password:
            md5 = hashlib.md5()
            md5.update(password.encode('utf-8'))
            self.cleaned_data['password'] = md5.hexdigest()
            return self.cleaned_data
        else:
            self.add_error('re_password', '两次密码不一致')
            raise ValidationError('两次密码不一致!!')


def add_customer(request, user_obj=None):
    cus_obj = models.Customer.objects.filter(pk=user_obj).first()
    for_obj = form_view.Form_arm(instance=cus_obj)
    next = request.GET.get('next')
    if request.method == 'POST':
        for_obj = form_view.Form_arm(data=request.POST, instance=cus_obj)
        if for_obj.is_valid():
            for_obj.save()
            return redirect(next)
    return render(request, 'add_customer.html', {'for_obj': for_obj})


def del_abc(request):
    pk = request.POST.get('username')
    print(pk, '*' * 8)
    models.Customer.objects.filter(pk=int(pk)).delete()
    return HttpResponse('666')


def add_my_customer(request):
    pk = request.POST.get('obj_id')
    cus_obj = models.Customer.objects.get(pk=pk)
    user = request.COOKIES.get('use')
    user_obj = models.UserProfile.objects.get(username=user)
    print(user, cus_obj)
    cus_obj.consultant = user_obj
    cus_obj.save()
    return redirect(reverse('index'))


def del_my_customer(request):
    pk = request.POST.get('obj_id')
    cus_obj = models.Customer.objects.get(pk=pk)

    cus_obj.consultant = None
    cus_obj.save()
    return HttpResponse('')


def piliang(request):
    print('12356')
    print(request.POST)
    choi = request.POST.get('choi')
    username = request.COOKIES.get('use')
    useobj = models.UserProfile.objects.get(username=username)
    if choi == 'a':
        lis = []
        for i in request.POST:
            print(i)
            if i.isdecimal():
                pk = int(request.POST.get(i))
                obj = models.Customer.objects.get(pk=pk)
                obj.consultant = useobj
                obj.save()
    if choi == 'b':
        lis = []
        for i in request.POST:
            if i.isdecimal():
                pk = int(request.POST.get(i))
                obj = models.Customer.objects.get(pk=pk)
                obj.consultant = None
                obj.save()
    return redirect(reverse('customer_list'))

def add_gerenjinjilu(request,pk=None,customer_id=0):
    print(request.get_full_path())
    obj = models.ConsultRecord.objects.filter(pk=pk).first()
    for_obj = form_view.Genjin_form(request,customer_id,instance=obj)
    next = request.GET.get('next')
    print(next)
    if request.method == 'POST':
        for_obj = form_view.Genjin_form(request,customer_id,data=request.POST, instance=obj)
        if for_obj.is_valid():
            for_obj.save()
            return redirect(reverse('genjinjilu_lis'))
    return render(request, 'form.html', {'for_obj': for_obj})



def genjinjilu_lis(request,customer=0):
    form_obj = form_view.Genjin_form(request=request,customer_id=customer)
    if request.GET.get('id'):
        pk = request.GET.get('id')
        print(pk,'6666')
        con_lis = models.ConsultRecord.objects.filter(customer_id=pk)
    else:
        print(request.user_obj)
        con_lis = models.ConsultRecord.objects.filter(consultant=request.user_obj)
        print(con_lis)
    return render(request,'genjinjilu_lis.html',{'form_obj':form_obj,'con_lis':con_lis})



def baoming_lis(request,pk=None):
    if pk:
        baoming_obj_lis = models.Enrollment.objects.filter(customer_id=pk)
    else:
        baoming_obj_lis = models.Enrollment.objects.filter(customer__consultant=request.user_obj)

    # obj = form_view.Enrollmentform(request,customer_id=customer_id,instance=baoming_obj)




    return render(request,'baoming_lis.html',{'baoming_obj_lis':baoming_obj_lis})


def add_baoming_lis(request,pk=None,customer_id=0):
    '''
    :param request:
    :param pk:  销售主键
    :param customer_id:  用户主键
    :return:
    '''
    if pk:
        enr_obj = models.Enrollment.objects.filter(pk=pk).first()
        for_obj = form_view.Enrollmentform(request, customer_id)
    else:
        enr_obj = models.Enrollment.objects.filter(customer_id=customer_id).first()
        for_obj = form_view.Enrollmentform(request, customer_id, instance=enr_obj)
    if request.method == 'POST':
        if not pk:
            for_obj = form_view.Enrollmentform(request, customer_id, data=request.POST, instance=enr_obj)
            print('777')
            if for_obj.is_valid():
                print('771')
                for_obj.save()
                return redirect(reverse('my_baoming_lis'))
        else:

            for_obj = form_view.Enrollmentform(request,customer_id,data=request.POST)
            print('666')
            if for_obj.is_valid():
                print('661')
                for_obj.save()
                return redirect(reverse('my_baoming_lis'))

    return render(request,'form.html',{'for_obj':for_obj})


def classlist_lis(request):
    class_obj_lis = models.ClassList.objects.filter(teachers=request.user_obj)

    return render(request,'classlist_lis.html',{'class_obj_lis':class_obj_lis})

def add_class_lis(request,pk=None):

    for_obj = form_view.Classlist(request,pk)
    if pk:
        for_obj = form_view.Classlist(request, pk,instance=models.ClassList.objects.get(pk=pk))
    if request.method == 'POST':
        for_obj = form_view.Classlist(request,pk,data=request.POST)
        if pk:
            for_obj = form_view.Classlist(request, pk, data=request.POST,instance=models.ClassList.objects.get(pk=pk))
        if for_obj.is_valid():
            for_obj.save()
            return redirect(reverse('classlist_lis'))


    return  render(request,'add_classlist.html',{'for_obj':for_obj})

def courserecord_lis(request):
    courserecord__obj_lis  = models.CourseRecord.objects.all()


    return render(request, 'courserecord_lis.html', {'courserecord__obj_lis':courserecord__obj_lis})
@cache_page(5)
def studyrecord_lis(request,pk=None):
    Modelformset = modelformset_factory(models.StudyRecord,form_view.StudyRecordForm,extra=0)
    form_set_obj = Modelformset(queryset=models.StudyRecord.objects.filter(course_record_id=pk))
    if request.method == 'POST':
        form_set_obj = Modelformset(queryset=models.StudyRecord.objects.filter(course_record_id=pk),data=request.POST)
        if form_set_obj.is_valid():
            form_set_obj.save()
            return redirect(reverse('courserecord_lis'))


    return render(request, 'studyrecord_lis.html', {'form_set_obj':form_set_obj})

def add_courserecord(request,pk=None):
    if not pk:
        for_obj = form_view.CourseRecordForm(request,pk)
    else:
        obj = models.CourseRecord.objects.filter(pk=pk).first()
        for_obj = form_view.CourseRecordForm(request,pk,instance=obj)
    if request.method == 'POST':
        for_obj = form_view.CourseRecordForm(request,pk,data=request.POST)
        if pk:
            obj = models.CourseRecord.objects.filter(pk=pk).first()
            for_obj = form_view.CourseRecordForm(request,pk,data=request.POST,instance=obj)
        if for_obj.is_valid():
            for_obj.save()
            return redirect(reverse('courserecord_lis'))

    return render(request,'form.html',{'for_obj':for_obj})




def multi_init(request):
    cours_record_ids = request.POST.getlist('box1')
    cours_obj_lis = models.CourseRecord.objects.filter(pk__in=cours_record_ids)
    for cours_obj in cours_obj_lis:
        students = cours_obj.re_class.customer_set.filter(status='studying')

        lis = []
        for student in students:
            lis.append(models.StudyRecord(course_record=cours_obj,student=student))
        models.StudyRecord.objects.bulk_create(lis)
    return redirect(reverse('courserecord_lis'))




