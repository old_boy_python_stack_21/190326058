from django.utils.deprecation import MiddlewareMixin
from django.shortcuts import render,redirect,reverse,HttpResponse

class Md1(MiddlewareMixin):
    def process_request(self,request):
        if request.path_info in [reverse('login'),reverse('reg')]:
            return

        if request.path_info.startswith('/admin'):
            return
        if request.COOKIES.get('use'):
            return
        return redirect(reverse('login'))