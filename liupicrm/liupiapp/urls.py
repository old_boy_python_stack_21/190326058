from django.conf.urls import url
from liupiapp import views
from rbac.rbac_view import login_reg
urlpatterns = [
    url(r'^reg/', views.reg,name='reg'),
url(r'^login/', login_reg.login,name='login'),
url(r'^index/', login_reg.index,name='index'),
url(r'^add_customer/', views.add_customer,name='add_customer'),
url(r'^edit_customer/(\d+)/', views.add_customer,name='edit_customer'),
# url(r'^my_customer/', views.index,name='my_customer'),
url(r'^del_abc/', views.del_abc,name='del_abc'),
url(r'^add_my_customer/', views.add_my_customer,name='add_my_customer'),
url(r'^del_my_customer/', views.del_my_customer,name='del_my_customer'),
url(r'^piliang/', views.piliang,name='piliang'),
# url(r'^mohu/', views.index,name='mohu'),
    url(r'^customer_list/', views.CustomerList.as_view(), name='customer_list'),
    url(r'^my_customer/', views.CustomerList.as_view(), name='my_customer'),
url(r'^add_gerenjinjilu/$', views.add_gerenjinjilu, name='add_gerenjinjilu'),

url(r'^genjinjilu_lis/$', views.genjinjilu_lis, name='genjinjilu_lis'),
url(r'^edit_gerenjinjilu/(?P<customer_id>\d+)/', views.add_gerenjinjilu, name='edit_gerenjinjilu'),


url(r'^my_baoming_lis/', views.baoming_lis, name='my_baoming_lis'),
url(r'^baoming_lis/(\d+)/', views.baoming_lis, name='baoming_lis'),
url(r'^add_baoming_lis/(\d+)/$', views.add_baoming_lis, name='add_baoming_lis'),
url(r'^edit_baoming_lis/(?P<customer_id>\d+)/', views.add_baoming_lis, name='edit_baoming_lis'),
url(r'^classlist_lis/', views.classlist_lis, name='classlist_lis'),
url(r'^add_class_lis/', views.add_class_lis, name='add_class_lis'),
url(r'^edit_class_lis/(\d+)/', views.add_class_lis, name='edit_class_lis'),
url(r'^courserecord_lis/', views.courserecord_lis, name='courserecord_lis'),
url(r'^edit_courserecord/(\d+)$', views.add_courserecord, name='edit_courserecord'),
url(r'^add_courserecord/$', views.add_courserecord, name='add_courserecord'),
url(r'^studyrecord_lis/(\d+)/', views.studyrecord_lis, name='studyrecord_lis'),
url(r'^multi_init/', views.multi_init, name='multi_init'),

]