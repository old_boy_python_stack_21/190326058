# 一、正则表达式练习
''''''
# 1、匹配整数或者小数（包括正数和负数）
''''''
r = '\d+\.?\d+'

# 2、匹配年月日日期 格式2018-12-6
'''
[0-2]\d{3}-0[0-9]-[1-30]|[0-2]\d{3}-1[0-2]-[1-30]
'''
# 3、匹配qq号
'''
[1-9]\d{5,10}
'''
# 4、11位的电话号码
'''
13\d{5,9}
'''
# 5、长度为8-10位的用户密码 ： 包含数字字母下划线
'''
[a-zA-Z_][\w]{7-9}
'''

# 6、匹配验证码：4位数字字母组成的
'''
[\da-zA-Z]{4}
'''
# 7、匹配邮箱地址
'''
[\da-zA-Z]{8,11}@[\da-zA-Z]+\.com
'''
# 8、1-2*((60-30+(-40/5)(9-25/3+7/399/42998+10568/14))-(-43)/(16-32))
# 从上面算式中匹配出最内层小括号以及小括号内的表达式
'''
\([^\(\)]+\)
'''

# 9、从类似9-25/3+7/399/42998+10*568/14的表达式中匹配出乘法或除法
'''
\d+[\*/]\d+
'''
# 10、从类似
# <a>wahaha</a>
# <b>banana</b>
# <h1>qqxing</h1>
# 这样的字符串中，
# 1）匹配出<a>,<b>,<h1>这样的内容
'''
<[^<>]+?>
'''
# 2）匹配出wahaha，banana，qqxing内容。(思考题)
'''
import re
ret = re.search('>(?P<name>\w+)<','<h1>hello</h1>')
print(ret.group('name'))
'''
# 自学一下内容，完成10、2)
# ret = re.search("<(?P<tag_name>\w+)>\w+</w+>","<h1>hello</h1>")
# #还可以在分组中利用?的形式给分组起名字
# #获取的匹配结果可以直接用group('名字')拿到对应的值
# print(ret.group('tag_name')) #结果 ：h1
# print(ret.group()) #结果 ：<h1>hello</h1>

# 二、使用listdir完成计算文件夹大小
import os
def get_size(file):
    size = 0
    ret = os.listdir(file)
    for i in ret:
        i = os.path.join(file,i)
        if os.path.isfile(i):
            size += os.path.getsize(i)
        else:
            i = os.path.join(file,i)
            size += get_size(i)
    return size
v = r'C:\Users\Administrator\PycharmProjects\xuexi\模块'
a = get_size(v)
print(a)
