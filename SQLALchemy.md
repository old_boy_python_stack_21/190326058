# SQLALchemy--ORM框架

###### 1.基础使用(建表)

```python
from sqlalchemy import Cloumn,INT,INTEGER,Integer,CHAR,NCHAR,VARCHAR,String
class User(BaseModel):	
    __tablename__  = 'user'   #创建数据库表名
    id = Cloumn(INT,primary_key=True,autoincrement=True)
    name = Cloumn(String(32),nullable=False,index=True,unique=True)
    						#不能为空       索引        唯一
     #Cloumn  定义数据列

#创建数据库引擎:
from sqlalchemy.engine import create_engine
engine = create_engine("mysql+pymyssql://root:132@127.0.0.1:3306/库名?charset=utf-8")  #数据库连接驱动语句
        
#利用User类取数据库创建数据表
BaseModel.metadata.create_all(engine)#数据库引擎
```

###### 2. 增删改查

```python
#模拟 navcat 操作
# 1.选择数据库
from sqlalchemy.engine import create_engine
engine = create_engine("mysql+pymysql://root:123@127.0.0.1:3306/s21?charset=utf8")
# 2.选择表
# 3.创建查询窗口
from sqlalchemy.orm import sessionmaker
from sqlalchemy.engine import create_engine
engine = create_engine("mysql+pymyssql://root:132@127.0.0.1:3306/库名?charset=utf-8")  #数据库连接驱动语句

from createTable import User

from sqlalchemy.orm import sessionmaker
select_db = sessionmaker(engine)   #选中数据库
db_session = select_db() #已经打开的查询窗口

user = User(name='alexDSB')   # 等价于 insert into user('name') value ('alexDSB')

user_list = [User(name="Alex123"),User(name='peiqi')]

#放入查询窗口
db_session.add(user)
db_session.add_all(user_list)
#提交sql语句   

#简单无条件查询
select * from user      user表 == class_User

res = db_session.query(User).all()
print(res[0].id,res[0].name)

res = db_session.query(User).all() # 查询全部符合条件的数据
res = db_session.query(User).first() # 查询符合条件的第一条数据
print(res.id,res.name)

#简单条件查询
select * from user where id=3

res = db_session.query(User).filter(User.id==3).all()
#res = db_session.query(User).filter_by(id=3).all()
```

修改数据

```python
db_session.quert(User.id == 1).update({"name":"李亚历山大"})
print(res)
db_session.commit()     #提交修改
db_session.close()
```

删除数据

```python
res = db_sesion.query(User).filter(User.id == 2).delete()
db_session.commit()
db_session.close()
```

###### 3. Foreign key 多对一

- 多对1的表结构

```python
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import Column,Integer,String,ForeignKey
from sqlalchemy.engine import create_engine

#ORM精髓 relationship
from sqlalchemy.orm import relationship

engine = create_engine("mysql+pymysql://root:123@127.0.0.1:3306/s21?charset=utf8")
BaseModel = declarative_base()

# 一对多
class School(BaseModel):
    __tablename__ = "school"
    id = Column(Integer,primary_key=True)
    name = Column(String(32),nullable=False)

class Student(BaseModel):
    __tablename__ = "student"
    id = Column(Integer,primary_key=True)
    name = Column(String(32),nullable=False)
    sch_id = Column(Integer,ForeignKey("school.id"))

    # 关系映射
    stu2sch = relationship("School",backref="sch2stu")

BaseModel.metadata.create_all(engine)
```

- 关系映射

```python
orm层面的
stu2sch = relationsip('school',backref=sch2stu)
#写在哪,那个表查是正向的
```

```python
#增加数据
from sqlalchemy.orm import sessionmaker

select_db = sessionmaker(engine)
db_session = select_db()

#增加数据(正向)
stu = Student(name="dragonfire",stu2sch=School(name="oldboy"))

db_session.add(stu)
db_session.commit()
db_session.close()

#反向
sch  = School(name="oldboyshanghai")
sch.sch2stu = [
    Student(name='赵丽颖'),
    Student(name='冯绍峰')
]
db_session.add(sch)
db_session.commit()
db_session.close()
```

查询

```python
查询 relationship 正向

res = db_session.query(Student).all()
for stu in res:
    print(stu.name,stu.stu2sch.name)
    
反向
res = db_session.query(Student).all()
for sch in res:
    print(sch.name,sch.sch2stu)
    for stu in sch.schstu:
        print(sch.name,stu.name)
```

###### 4. 多对多的用法

1. 表结构

```python
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import Column,Integer,String,ForeignKey
from sqlalchemy.engine import create_engine
from sqlalchemy.orm import relationship

BaseModel = declarative_base()
engine = create_engine("mysql+pymysql://root:123@127.0.0.1:3306/s21?charset=utf8")

class Girl(BaseModel):
    __tablename__ = "girl"
    id = Column(Integer,primary_key=True)
    name = Column(String(32),nullable=False)

    gyb = relationship("Boy",backref="byg",secondary="hotel") # secondary="hotel" 数据表中的数据才能证明两者关系

class Boy(BaseModel):
    __tablename__ = "boy"
    id = Column(Integer,primary_key=True)
    name = Column(String(32),nullable=False)

class Hotel(BaseModel):
    __tablename__ = "hotel"
    id = Column(Integer,primary_key=True)
    bid = Column(Integer,ForeignKey("boy.id"))
    gid = Column(Integer,ForeignKey("girl.id"))
BaseModel.metadata.create_all(engine)
```

增加数据

```python
select_db = sessionmaker(engine)
db_session = select_db()

增加数据 relationship 正向添加
g = Girl(name="赵丽颖",gyb=[Boy(name="DragonFire"),Boy(name="冯绍峰")])
db_session.add(g)
db_session.commit()
db_session.close()

增加数据 relationship 反向添加
b = Boy(name="李杰")
b.byg = [
    Girl(name="罗玉凤"),
    Girl(name="朱利安"),
    Girl(name="乔碧萝")
]
db_session.add(b)
db_session.commit()
db_session.close()
```

查询数据

```python
查询 relationship 正向
res = db_session.query(Girl).all()
for g in res:
    print(g.name,len(g.gyb))

查询 relationship 反向
res = db_session.query(Boy).all()
for b in res:
    print(b.name,len(b.byg))
```

# Flask中应用

###### **1.*** app01 包文件

```python
#__init__ 文件
from flask import Flask
from flask_sqlalchemy import SQLAlchemy

db = SQLAlchemy()

# 创建db 时一定要注意导入蓝图的顺序
from app01.views import user

def create_app():
    app = Flask(__name__)
    app.config["DEBUG"] = True
    app.config["SESSION_COOKIE_NAME"] = "I am Not SESSION"
    app.config["SQLALCHEMY_DATABASE_URI"] = "mysql+pymysql://root:123@127.0.0.1:3306/s21?charset=utf8"
    app.config["SQLALCHEMY_TRACK_MODIFICATIONS"] = False
    app.register_blueprint(user.user)

    db.init_app(app=app)  # app = Flask
    return app
```

###### 2.***** 蓝图文件夹 app01.views.user.py

```python
from flask import Blueprint
from app01.models import Users
from app01 import db

user = Blueprint("user",__name__)

@user.route("/reg/<username>")
def reg(username):
    u = Users(name=username)
    db.session.add(u)
    db.session.commit()
    return "reg 200 OK"

@user.route("/user_list")
def user_list():
    res = Users.query.filter(Users.id>1).all()
    print(res)
    return f"当前有{len(res)}个用户"

```

###### 3. ***** models.py文件

```python
from app01 import db
# db 全新的SQLAlchemy对象
# db.Model == BaseModel == 使用的SQLAlchemy

class Users(db.Model):
    __tablename__ = "users"
    id = db.Column(db.Integer,primary_key=True)
    name = db.Column(db.String(32),nullable=False)

if __name__ == '__main__': # 当前文件被当成模块或者是库导入时不会加载一下内容
    from app01 import create_app
    app = create_app()
    db.drop_all(app=app)
    db.create_all(app=app)
```

###### 命令行代码配置

```python
#manage.py文件
# 启动Flask
from app01 import create_app,db
from flask_script import Manager
from flask_migrate import Migrate,MigrateCommand
app = create_app()
mig = Migrate(app,db)
manager = Manager(app)
manager.add_command("db",MigrateCommand)
# python manager.py db init 初始化数据库
#
# Flask-Migrate 依赖组件 Flask-Script
"""
数据库迁移指令:
python manager.py db init 
python manager.py db migrate   # Django中的 makemigration
python manager.py db upgrade  # Django中的 migrate
"""

@manager.command
def oldboys21(args):
    print(args)
    return args
#指令集
@manager.option("-w","--who",dest="who")   
@manager.option("-s","--sss",dest="ss")
def func(who,ss):
    print(f"{who},你好{ss}")
    
if __name__ == '__main__':
    manager.run()
```

