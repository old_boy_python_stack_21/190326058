import time
from threading import Lock
from concurrent.futures import ThreadPoolExecutor
def func(f):
    with lock:
        for i in range(100):
            a = f.readline()
            print(a)


if __name__ == '__main__':
    tp = ThreadPoolExecutor(20)
    lock = Lock()
    with open(r'D:\py21\草稿纸相关\days38homework\a.txt', 'r', encoding='gbk')as f:

            lis = []
            for i in range(200):
                ret = tp.submit(func,f)
                lis.append(ret)
            tp.shutdown()


# ------------------------------------------------------------
'''
	进程 : 内存隔离 操作系统级别 开销大 可以利用多核 计算机中资源分配的最小单位
	应用:高计算
	线程 : 内存共享 操作系统级别 开销中 Cpython解释器下不能利用多核 计算机中CPU调度的最小单位
	应用:爬虫
	协程 : 内存共享 用户级别     开销小 不能利用多核
相同点 : 都能够帮助我们实现并发操作,规避IO时间,提高程序执行效率
    应用:爬虫 

'''




