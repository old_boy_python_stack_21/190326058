# 为函数写一个装饰器，在函数执行之后输入 after

# def wrapper(f):
#     def inner(*args,**kwargs):
#         a = f(*args,**kwargs)
#         print('after')
#         return a
#     return inner
# @wrapper
# def func():
#     print(123)
#
# func()

# 为函数写一个装饰器，把函数的返回值 +100 然后再返回。

# def wrapper(f):
#     def inner(*args,**kwargs):
#         a = f(*args,**kwargs)
#         return a + 100
#     return inner
# @wrapper
# def func():
#     return 7
#
# result = func()
# print(result)

# 为函数写一个装饰器，根据参数不同做不同操作。
#
# flag为True，则 让原函数执行后返回值加100，并返回。
# flag为False，则 让原函数执行后返回值减100，并返回。
''''''
'''
def x(count):
    def wrapper(f):
        def inner(*args,**kwargs):
            if count == True:
                a = f(*args,**kwargs)
                return a + 100
            elif count == False:
                a = f(*args, **kwargs)
                return a - 100
        return inner
    return wrapper
@x(True)
def f1():
    return 11

@x(False)
def f2():
    return 22

r1 = f2()
print(r1)
'''
# 写一个脚本，接收两个参数。
#
# 第一个参数：文件
# 第二个参数：内容
# 请将第二个参数中的内容写入到 文件（第一个参数）中。

# 执行脚本： python test.py oldboy.txt 你好
'''
def func(file,a):
    with open(file,'w',encoding='utf-8') as f1:
        f1.write(a)
func('oldboy.txt','你好')
'''
# 递归的最大次数是多少？
1000
# 看代码写结果

# print("你\n好")#你换行 好
# print("你\\n好")#你\n好
# print(r"你\n好")#你\n好


# 写函数实现，查看一个路径下所有的文件【所有】。
import os
# def func(path):
#     result =  os.walk(path)
#     for a,b,c in result:
#         # a, 正在查看的目录
#         # b, 此目录下的文件夹
#         # c, 此目录下的文件
#         for item in c:
#             path = os.path.join(a,item)
#             print(path)
# a = func(r'F:\woailuo2012')
# 写代码

path = r"D:\code\test.pdf"

# 请根据path找到code目录下所有的文件【单层】，并打印出来。
'''
a = os.listdir(path)
for item in  a:
    print(item)
'''

# 9.1
'''
def func(c,d = 2,a=0,b=1):

    b = a + b
    a = b - a
    if a + b < c:
        d += 1
        return func(c,d,a,b)
    else:
        print('%d是400万以内最大的数，是第%d个'%(b,d,))
func(4000000)
'''
'''
dicta = {'a':1,'b':2,'c':3,'d':4,'f':'hello'}
dictb = {'b':3,'d':5,'e':7,'m':9,'k':'world'}
dictc = {}
for i in dictb:
    dictc[i] = dictb[i]
for i in dicta:
    dictc[i] = dicta[i]
    if i in dictb:
        dictc[i] = dicta[i] + dictb[i]
print(dictc)
'''
# 10.
# [10,'a']
# [123]
# [10,'a']

# 面试题
# 1.abc

# 2.
'''
tupleA = ('a','b','c','d','e')
tupleB = (1,2,3,4,5)
res = {}
for i in range(5):
    res[tupleA[i]] = tupleB[i]
print(res)
'''
# 3.
import sys
sys.argv
# 4.
ip = '192.168.0.100'
li = ip.split('.')

# 5.
AList = ['a','b','c']
s = ','.join(AList)

# 6.
# 1. StrA[-2:]
# 2. StrA[1:3]

# 7
Alist= [1,2,3,1,3,1,2,1,3]
AList[:3]

# 编程题
# 1.
'''
import os
def func(file):
    result = os.walk(file)
    for a,b,c  in result:
        for i in b:
            print(os.path.join(a,i))
        for item in c:
            d = os.path.join(a,item)
            print(d)
func(r'F:\woailuo2012')
'''
'''
def func():

    for i in range(1,1000):
        li = []
        for r in range(1,i-1):

            if i % r == 0:
                li.append(r)
        if i == sum(li):
            print(i)
func()
'''
'''
'''
# def func(a,b):
#     c= []
#     d = False
#     for i in range(len(a)):
#         if a[i] > b[i]:
#             c.append(a[i])
#             d = True
#         elif a[i] < b[i]:
#             c.append(b[i])
#             d = True
#     if d == False:
#             return a
#     else:
#         return b
'''
with open('etl_log.txt','r',encoding='utf-8') as f1:
    for line in f1:
        pass
'''