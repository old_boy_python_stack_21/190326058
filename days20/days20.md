```python
class Person:      #类名
    country = 'China' #创造了一个只要是这个类就一定有的属性
                       #类属性，静态属性
    def __init__(self,*args):   #初始化方法，self是对象，是一个必须传的参数
        #self就是一个可以存储很多属性的大字典
        self.name = args[0]    #往字典里添加属性的方式发生了一些变化
        self.hp = args[1]
        self.aggr = args[2]
        self.sex = args[3]
    def walk(self,n):           #方法，一般请胯下必须传self参数，且必须写在第一个
                                #后面还可以传其他参数，是自由的
        print('%s走走走，走了%s步'%(self.name,n))
alex = Person('狗剩',100,1,'不详')  #类名可以实例化对象，alex对象   #实例化
print(alex.name)
print(alex.hp)
print(alex.__dict__)   #等价于print（Person.__dict__)
alex.walk(5)  #等价于Person.walk（alex，5） 调用方法 类名.方法名（对象名）
print(Person.country)   #类名可以查看类中的静态属性，不需要实例化就可以查看

print(Person.__dict__['country'])
#print(alex.__dict__['name'])
#alex.__dict__['name'] = '二哥'
##等价于
alex.name = '二哥'
alex.__dict__['name'] = '二哥'
alex.age = 83

#对象 = 类名（）
#过程：
    #类名（）首先会创造出一个对象，创造了一个self变量
    #调用__init__方法，类名括号里面的参数会被这里接收
    #执行init方法
    #返回self
#对象能做的事：
    #查看属性
    #调用方法
    #__dict__对于对象的增删改查操作都可以通过字典的语法进行
#类名能做的事
    #调用类中的静态属性
    #__dict__对于类中的名字只能看，不能操作

#类中的静态变量可以被类和对象调用
#对于不可变数据类型来说，类变量最好用类名操作


#绑定方法
    #对象调用类的方法，对象自身当做self传入给方法
#例如：
class Foo:
    def func(self):
        print('func')
    def funl(self):
        pass
f1 = Foo()
f1.func()    #f1绑定func方法
```

##### 类成员

1. 类变量
2. 绑定方法
3. 静态方法
4. 类方法
5. 属性

##### 对象

私有属性，私有方法等

```python
#广义上面向对象的封装:代码的保护，面向对象的思想本身就是一种
#只让自己的对象能调用自己类中的方法

#狭义上的封装-----面向对象的三大特性之一
#属性和方法都藏起来 不让你看见
class Person:
    def __init__(self,name,passwd):
        self.name = name
        self.__passwd = passwd   #私有方法

    def __get_pwd(self):    #私有方法
        print(self.__dict__)
        return self.__passwd   #只要在类的内部使用私有属性，就会自动的带上类名
    def login(self):
        return self.__get_pwd()
alex = Person('alex','alex3714')
print(alex._Person__passwd)    #_类名__属性名
alex.__high = 1
print(alex.login())
print(alex.__high)

#所有的私有 都是在变量的左边加上双下划綫
    #对象的私有属性
    #对象的私有方法
    #对象的静态私有属性
#所有的私有的 都不能再类的外部使用
print(alex.__dict__)
```