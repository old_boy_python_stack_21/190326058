'''

'''
# 简述面向对象三大特性并用代码表示。
''''''
# 继承
'''
class A:
    pass
class B(A):
    pass
B 继承A  A 默认继承object
'''
# 封装
'''
class Pa:
    def __init__(self,name):
        self.name = name
    def func(self):
        pass
    def func1(self):
        pass
将函数封装到类
a = Pa('孙悟空')
将属性封装到对象
'''
# 多态
class AA:
    def func(self,a):
        a.append('lalal')
# python本身就是一种多态语言
# 只要他有append方法，就是我们需要的类型

# 什么是鸭子模型？
'''
只要会呱呱叫，我们就可以认为他是我们所需要的鸭子
'''
# 列举面向对象中的类成员和对象成员。
'''
属性
绑定方法
静态方法
类方法
类变量
'''
'''
实例变量
'''
# @methodclass和@staticmethod的区别?
'''
一个是类方法 
必须传一个参数cls 代表这个类
一个是 静态方法
参数可传可不传 
两种方法  类和对象都可调用
'''
# Python中双下滑 __ 有什么作用？
'''
# 定义所有的属性
'''
# 看代码写结果

#
# class Base:
#     x = 1
#
#
# obj = Base()
#
# print(obj.x)
# obj.y = 123
# print(obj.y)
# obj.x = 123
# print(obj.x)
# print(Base.x)
'''
1
123
123
1
'''
# 看代码写结果


class Parent:
    x = 1


class Child1(Parent):
    pass


class Child2(Parent):
    pass


print(Parent.x, Child1.x, Child2.x)
Child2.x = 2
print(Parent.x, Child1.x, Child2.x)
Child1.x = 3
print(Parent.x, Child1.x, Child2.x)
'''
1,1,1
1,1,2
1,3,2
'''
# 看代码写结果

class Foo(object):
    n1 = '武沛齐'
    n2 = '金老板'
    def __init__(self):
        self.n1 = '女神'

obj = Foo()
print(obj.n1)
print(obj.n2)
'''
女神
金老板
'''
# 看代码写结果【如果有错误，则标注错误即可，并且假设程序报错可以继续执行】

# class Foo(object):
#     n1 = '武沛齐'
#     def __init__(self,name):
#         self.n2 = name
# obj = Foo('太白')
# print(obj.n1)
# print(obj.n2)
#
# print(Foo.n1)
# print(Foo.n2)
'''
武沛齐
太白
武沛齐
报错，绑定方法只有对象才能调用
'''
# 看代码写结果【如果有错误，则标注错误即可，并且假设程序报错可以继续执行】

# class Foo(object):
#     a1 = 1
#     __a2 = 2
#
#     def __init__(self, num):
#         self.num = num
#         self.__salary = 1000
#
#     def show_data(self):
#         print(self.num + self.a1)
# obj = Foo(666)
# print(obj.num)
# print(obj.a1)
# print(obj.__salary)
# print(obj.__a2)
# print(Foo.a1)
# print(Foo.__a2)
'''
666
1
私有属性 不能再外部被调用
私有属性不能再外部被调用
1
私有属性不能再外部被调用
'''
# 看代码写结果【如果有错误，则标注错误即可，并且假设程序报错可以继续执行】
#
# class Foo(object):
#     a1 = 1
#
#     def __init__(self, num):
#         self.num = num
#
#     def show_data(self):
#         print(self.num + self.a1)
#
#
# obj1 = Foo(666)
# obj2 = Foo(999)
# print(obj1.num)
# print(obj1.a1)
#
# obj1.num = 18
# obj1.a1 = 99
#
# print(obj1.num)
# print(obj1.a1)
#
# print(obj2.a1)
# print(obj2.num)
# print(obj2.num)
# print(Foo.a1)
# print(obj1.a1)
'''
666
1
18
99
1
999
999
1
99
'''
# 看代码写结果，注意返回值。

# class Foo(object):
#
#     def f1(self):
#         return 999
#
#     def f2(self):
#         v = self.f1()
#         print('f2')
#         return v
#
#     def f3(self):
#         print('f3')
#         return self.f2()
#
#     def run(self):
#         result = self.f3()
#         print(result)
#
#
# obj = Foo()
# v1 = obj.run()
# print(v1)
'''
f3
f2
999
None
'''
# 看代码写结果【如果有错误，则标注错误即可，并且假设程序报错可以继续执行】
#
# class Foo(object):
#
#     def f1(self):
#         print('f1')
#
#     @staticmethod
#     def f2():
#         print('f2')
#
#
# obj = Foo()
# obj.f1()
# obj.f2()
#
# Foo.f1()
# Foo.f2()
'''
f1
f2
类不能调用绑定方法
f2
'''
# 看代码写结果【如果有错误，则标注错误即可，并且假设程序报错可以继续执行】

# class Foo(object):
#
#     def f1(self):
#         print('f1')
#
#     @classmethod
#     def f2(cls):
#         print('f2')
#
#
# obj = Foo()
# obj.f1()
# obj.f2()
#
# Foo.f1()
# Foo.f2()
'''
f1
f2
类不能调用绑定方法
f2

'''
# 看代码写结果【如果有错误，则标注错误即可，并且假设程序报错可以继续执行】

class Foo(object):
    def f1(self):
        print('f1')
        self.f2()
        self.f3()
    @classmethod
    def f2(cls):
        print('f2')
    @staticmethod
    def f3():
        print('f3')

obj = Foo()
obj.f1()
'''
f1
f2
f3

'''
# 看代码写结果【如果有错误，则标注错误即可，并且假设程序报错可以继续执行】

# class Base(object):
#     @classmethod
#     def f2(cls):
#           print('f2')
#
#     @staticmethod
#     def f3():
#           print('f3')
#
# class Foo(object):
#     def f1(self):
#         print('f1')
#         self.f2()
#         self.f3()
#
# obj = Foo()
# obj.f1()
'''
f1
找不到f2方法
找不到f3方法
'''
# 看代码写结果


class Foo(object):
    def __init__(self, num):
        self.num = num


v1 = [Foo for i in range(10)]
v2 = [Foo(5) for i in range(10)]
v3 = [Foo(i) for i in range(10)]

print(v1)
print(v2)
print(v3)
'''
10个类
10个对象参数为5
10个对象参数为0-10
'''
# 看代码写结果

class StarkConfig(object):

    def __init__(self, num):
        self.num = num

    def changelist(self, request):
        print(self.num, request)


config_obj_list = [StarkConfig(1), StarkConfig(2), StarkConfig(3)]
for item in config_obj_list:
    print(item.num)
'''
1
2
3
'''
# 看代码写结果

class StarkConfig(object):

    def __init__(self, num):
        self.num = num

    def changelist(self, request):
        print(self.num, request)


config_obj_list = [StarkConfig(1), StarkConfig(2), StarkConfig(3)]
for item in config_obj_list:
    item.changelist(666)
'''
1 666
2 666
3 666
'''
# 看代码写结果
class Department(object):
    def __init__(self, title):
        self.title = title
class Person(object):
    def __init__(self, name, age, depart):
        self.name = name
        self.age = age
        self.depart = depart
d1 = Department('人事部')
d2 = Department('销售部')
p1 = Person('武沛齐', 18, d1)
p2 = Person('alex', 18, d1)
p3 = Person('安安', 19, d2)
print(p1.name)
print(p2.age)
print(p3.depart)
print(p3.depart.title)
'''
武沛齐
18
d2对象
销售部
'''
# 看代码写结果


class Department(object):
    def __init__(self, title):
        self.title = title
class Person(object):
    def __init__(self, name, age, depart):
        self.name = name
        self.age = age
        self.depart = depart
    def message(self):
        msg = "我是%s,年龄%s,属于%s" % (self.name, self.age, self.depart.title)
        print(msg)
d1 = Department('人事部')
d2 = Department('销售部')
p1 = Person('武沛齐', 18, d1)
p2 = Person('alex', 18, d1)
p1.message()
p2.message()
'''
我是武沛齐，年龄18，属于人事部
我是alex，年龄18，属于人事部
'''
'''

'''
"""
角色：学校、课程、班级
要求：
	1. 创建北京、上海、深圳三所学校。
	2. 创建课程
		北京有三种课程：Linux、Python、Go
		上海有两种课程：Linux、Python
		深圳有一种课程：Python
	3. 创建班级(班级包含：班级名称、开班时间、结课时间、班级人数)
		北京Python开设：21期、22期
		北京Linux开设：2期、3期
		北京Go开设：1期、2期
		上海Python开设：1期、2期
		上海Linux开设：2期
		深圳Python开设：1期、2期
"""
from datetime import datetime,timedelta
import random
class School:
    def __init__(self,place,classroom_obj,subject_obj):
        self.place = place
        self.class_room = classroom_obj
        self.subject1 = subject_obj
    def show(self):
        print(self.subject1.obj)
        for i in range(len(self.class_room)):
            print('学校地点%s，课程班级%s，课程人数%d，开课时间%s，节课时间%s'%(self.place,self.class_room[i].number,self.class_room[i].person_number,self.class_room[i].open_time,self.class_room[i].close_time,))
class Object:
    def __init__(self,lis):
        self.obj = lis
class Classroom:
    def __init__(self,number,peopel_num,open_time,close_time):
        self.number = number
        self.person_number = peopel_num
        self.open_time = open_time
        self.close_time = close_time
obj1 = Object(['python','Linuex','GO'])
obj2 = Object(['python','Linuex'])
obj3 = Object(['python'])
def ctime():
    ctime  = datetime.now()

    number = random.randint(30,45)
    ctime1 = ctime - timedelta(days= number)
    ctime2 = ctime1.strftime('%Y-%m-%d')
    ctime3 = ctime1 + timedelta(days=130)
    ctime3 = ctime3.strftime('%Y-%m-%d')
    return [ctime2,ctime3]
classlistbeijing = []

beijing_dic = {'python21':55,'python22': 65,'linuex21': 75,'linuex22': 85,'GO2': 95}
for i in beijing_dic:
    time_time = ctime()
    number = i
    people_num = beijing_dic[i]
    open_time = time_time[0]
    close_time = time_time[1]
    a = Classroom(number,people_num,open_time,close_time)
    classlistbeijing.append(a)

classlistshanghai = []

shanghai_dic = {'python1期':55,'python2期': 65,'linuex2期': 75,}
for i in shanghai_dic:
    time_time = ctime()
    number = i
    people_num = shanghai_dic[i]
    open_time = time_time[0]
    close_time = time_time[1]
    a = Classroom(number,people_num,open_time,close_time)
    classlistshanghai.append(a)



shenzhen_dic = {'python1期':55}
for i in shenzhen_dic:
    time_time = ctime()
    number = i
    people_num = shenzhen_dic[i]
    open_time = time_time[0]
    close_time = time_time[1]
    a = Classroom(number,people_num,open_time,close_time)
    classlistshanghai.append(a)
school1 = School('北京',classlistbeijing,obj1)
school1.show()
shanghai = School('上海',classlistshanghai,obj2)
shanghai.show()
shenzhen = School('深圳',classlistshanghai,obj3)
shenzhen.show()