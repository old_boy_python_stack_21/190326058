Python学习笔记

## 第一章 计算机基础

### 1.1  硬件

计算机基本的硬件由：CPU / 内存 / 主板 / 硬盘 / 网卡  / 显卡 等组成，只有硬件但硬件之间无法进行交流和通信。

### 1.2 操作系统

操作系统用于协同或控制硬件之间进行工作，常见的操作系统有那些:

- windows：可视化界面好，适合办公。
- linux
  - centos 【公司线上一般用】
  - ubuntu 可视化界面好，适合个人开发。
  - hatred 企业级别用
- mac
  - mac os 个性化软件，好用。

### 1.3  解释器或编译器

编程语言的开发者写的一个工具，将用户写的代码转换成010101交给操作系统去执行。

#### 1.3.1  解释和编译型语言

解释型语言就类似于：代码重上到下一条条边解释边执行。代表：Python / PHP / Ruby / Perl

编译型语言类似于：编译器将代码编译成二进制文件，再交由计算机执行。代表：C / C++ / Java / Go ...

### 1.4 软件（应用程序）

软件又称为应用程序，就是我们在电脑上使用的工具，类似于：记事本 / 图片查看 / 游戏

### 1.5 进制

对于计算机而言无论是文件存储 / 网络传输输入本质上都是：二进制（010101010101），如：电脑上存储视频/图片/文件都是二进制； QQ/微信聊天发送的表情/文字/语言/视频 也全部都是二进制。

进制：

- 2进制，计算机内部识别
- 8进制，以前应用。。
- 10进制，人来进行使用一般情况下计算机可以获取10进制，然后再内部会自动转换成二进制并操作。
- 16进制，一般用于表示二进制（用更短的内容表示更多的数据），一般是：\x 开头。

| 二进制 | 八进制 | 十进制 | 十六进制 |
| ------ | ------ | ------ | -------- |
| 0      | 0      | 0      | 0        |
| 1      | 1      | 1      | 1        |
| 10     | 2      | 2      | 2        |
| 11     | 3      | 3      | 3        |
| 100    | 4      | 4      | 4        |
| 101    | 5      | 5      | 5        |
| 111    | 6      | 6      | 6        |
| 1000   | 7      | 7      | 7        |
| 1001   | 10     | 8      | 8        |
| 1011   | 11     | 9      | 9        |
| 1111   | 12     | 10     | A        |
| 10000  | 13     | 11     | B        |
| 10001  | 14     | 12     | C        |
| 10011  | 15     | 13     | D        |
| 10111  | 16     | 14     | E        |
| 11111  | 17     | 15     | F        |



## 第二章 Python入门

### 2.1 环境的安装

- 环境变量的作用：方便在命令行（终端）执行可执行程序，将可执行程序所在的目录添加到环境变量，那么以后无需再输入路经。

- 解释器：python2 / python3 （环境变量）
  - 注意：更改python2与python3的'.exe'名称，用于区分开启的解释器。
- 开发工具：pycharm安装

### 2.2 编码

#### 2.2.1 编码基础

- ascii：用于表示字母，符号等。一个字符最多能表示256个字符，目前有127个字符。
- unicode：万国码，将世界上所有的文字编写在内，一般需要占用4个字节，用于计算机内存。
- utf-8：unicode的优化，它将表示字符分类，acsii的用于一个字节，欧洲2个字节表示，东亚的一般3个字节表示。
- gbk：国标编码方式，到127为ascii编码，128开始为汉字编码，一般占用2个字节。
- gb2312：同上

如果想要修改默认编码，则可以使用：

```python
# -*- coding:utf-8 -*- 
```

#### 2.2.2 python2&python3区别

**1**.**对于Python默认解释器编码：**

- python2的默认编码： ascii
- python3的默认编码： utf-8

注意：对于操作文件时，要按照：以什么编写写入，就要用什么编码去打开。

**2.range   xrange区别**

python2:

- xrange:不会在内存中立即创建，而是在循环时，边循环边创建

- range,在内存立即把所有的值都创建

python3:

- range:功能与python2的xrange一样。

3. **类的区别：**

   ```python
   class Foo:
       pass
   #如果再python2中这样定义，则称为经典类
   class Foo(object):
       pass
   #在如果再python2中这样定义，则称为新式类
   
   
   #在python3中这两个写法是一样的，因为所有的类默认都会继承object，全都是新式类
   ```

4. **python2&python3 字符串区别**

   python2有Unicode类型和str字节，python3有字符串类型（默认存储Unicode）相当于python2的Unicode类型，Bytes类型相当于python2的str字节类型。

5. **输入输出区别**

   ```python
   #python2:
   a = raw_input('请输入')
   print '这是',a
   #python3:
   b = input('请输入')
   print('这是',b)
   ```

6. **整型/长整型，整除区别**

   ```python
   #python2有长整型，python3只有整型。
   
   #python2整除返回一个整型数（类似于python3的地板除），python3中整除返回一个浮点数。
   #若想python2中与python中一样，需引入模块：
   from __future__ import division
   ```

7. 用于调用基类的一个方法。Python3.x 和 Python2.x 的一个区别是: Python 3 可以使用直接使用 **super().xxx** 代替 **super(Class, self).xxx

8. 未完待续

### 2.3 变量

- 问：为什么要有变量？变量时为某个值创建一个“代号”，以后在使用时候通过此“代号”就可以直接调用。

- 变量的命名规则：
  - 以数字、字母、下划线组成。
  - 数字不能作为变量开头。
  - 不能使用python内置关键字（如：and,if,else...）
  - 起变量名要见名知意，多个单词组合设一个变量，可用下划线连接。

### 2.4 常量

- 常量是不允许修改的值，python中没有常量，只是默认执行的约定，命名全部大写。

### 2.4注释

- 单行注释：#
- 多行注释：'''  ''' 或" " "      " " "

```python

#这是单行注释

"""
这是多行注释1
这是多行注释2
这是多行注释3
"""
```



## 第三章 数据类型

### 3.1 整型（int）（有序不可变）

#### 3.1.1 整型的长度

python2中int有长整型。

python3中只有int。

#### 3.1.2 整除

python2整除返回一个整型数（类似于python3的地板除），python3中整除返回一个浮点数。

若想python2中与python中一样，需引入模块：

```python
from __future__ import division
```

### 3.2 布尔（bool）

布尔值就是用于表示真假。True和False。

其他类型转换成布尔值：

| 数据类型 | 表示形式   | 布尔值 |
| -------- | ---------- | ------ |
| str()    | "",srt()   | False  |
| int()    | int(),0    | False  |
| list()   | list(),[]  | False  |
| dict()   | dict(),{}  | False  |
| tuple()  | tuple(),() | False  |
| set()    | set()      | False  |
| None     | None       | False  |

### 3.3 字符串（str）（有序不可变）

- 字符串是写代码中最常见的，python内存中的字符串是按照：unicode 编码存储。对于字符串是不可变。

- python2&python3字符串的区别：

  python2有Unicode类型和str字节，python3有字符串类型（默认存储Unicode）相当于python2的Unicode类型，Bytes类型相当于python2的str字节类型。

- 字符串自己有很多方法，如：

1. 大写： upper()/isupper()

   - upper(x) ： 用于将字符串x转换成大写
   - isupper(x) ：判断字符串x是不是都大写，返回一个布尔值。

   ```python
   v = 'alex'
   v1 = v.upper()
   print(v1)
   #"ALEX"
   v2 = v.isupper() # 判断是否全部是大写
   print(v2)
   #False
   ```

2. 小写：lower()/islower()

   - lower(x) :用于将字符串x转换成小写。
   - islower(x) : 判断字符串是不是都小写，返回一个布尔值。

   ```python
   v = 'ALEX'
   v1 = v.lower()
   print(v1)
   #"alex"
   v2 = v.islower() # 判断是否全部是小写
   print(v2)
   #False
   ```

3. isdecimal()

   - 判断是否是十进制的数字。

   ```python
   v = '1'
   # v = '二'
   # v = '②'
   v1 = v.isdigit()  # '1'-> True; '二'-> False; '②' --> True
   v2 = v.isdecimal() # '1'-> True; '二'-> False; '②' --> False
   v3 = v.isnumeric() # '1'-> True; '二'-> True; '②' --> True
   print(v1,v2,v3)
   # 以后推荐用 isdecimal 判断是否是 10进制的数。
   
   # ############## 应用 ##############
   #输入数字，能找到列表对应的索引号。
   v = ['alex','eric','tony']
   
   for i in v:
       print(i)
   
   num = input('请输入序号：')
   if num.isdecimal():
       num = int(num)
       print(v[num])
   else:
       print('你输入的不是数字')
   ```

4. strip()

   - 去除字符串两边的空格，换行符(\n)，制表符(\t)或指定字符串。

   ```python
   
   v1 = "alex "
   print(v1.strip())
   #"alex"
   v2 = "alex\t"
   print(v2.strip())
   #"alex"
   v3 = "alex\n"
   print(v3.strip())
   #"alex"
   v1 = "alexa"
   print(v1.strip('al'))
   #"ex"
   ```

5. replace(old,new,index)

   - 将字符串旧地内容用新的替换掉，index为替换几个。

   ```python
   v = "oldboy"
   v1 = v.replace('l','a')
   print(v1)
   #0'oadboy'
   
   v = "oldboy"
   v1 = v.replace('o','b',1)#<------此处替换掉了一个。
   print(v1)
   #'bldboy'
   ```

6. startswith / endswith

   - startswith判断一个字符串是否以指定字符或字符串开头。
   - endswith判断一个字符串是否以指定字符或字符串结尾。

   ```python
   v = 'alex'
   v1 = v.startswith('al')
   print(v1)
   #True
   v2 = v.endswith('ex')
   print(v2)
   #True
       
   ```

7. encode()

   - 以指定的编码格式编码字符串。

   ```python
   c = '你好'
   utf_name = c.encode('UTF-8')
   print(utf_name)
   #b'\xe4\xbd\xa0\xe5\xa5\xbd'
   gbk_name = c.encode('GBK')
   print(gbk_name)
   #b'\xc4\xe3\xba\xc3'
   ```

8. format

   - 格式化字符的函数

   ```python
   message = "我是{}，今年{}岁".format('alex',18)
   print(message)
   #我是alex，今年18岁
   ```

9. join()

   - 将指定元素进行字符拼接

   ```python
   a = "alex"
   a1 = "_".join(a)
   print(a1)
   #"a_l_e_x"
   ```

10. split 

    - split(str,num)将字符串按照str进行分割，如果参数 num 有指定值，则分隔 num+1 个子字符串，返回为一个列表。

    ```python
    a = 'alblcldl'
    a1 = a.split('l')
    print(a1)
    #['a', 'b', 'c', 'd', '']
    
    a2 = a.split('l',1)
    print(a2)
    #['a', 'blcldl']
    
    a3 = a.rsplit('l',1)
    print(a3)
    #['alblcld', '']
    ```

11. casefold

    - 将字符串中所有大写字符转换为小写后生成的字符串。其他语言也可以。

    ```python
    s = "ß"
    a = s.casefold()
    print(a)
    #ss
    ```

12. center

    - 一个原字符串居中,并使用空格填充至长度 width 的新字符串。默认填充字符为空格。

    ```python
    v = 'jk'
    t = v.center(8)
    print(t)
    #'   jk   '
    
    t = v.center(8,"*")
    #'***jk***'
    ```

    

13. count

    - 用于统计字符串里某个字符出现的次数。可选参数为在字符串搜索的开始与结束位置。
    - 语法：str.count(sub, start=None, end=None)
    - 参数：
      - sub     搜索的子字符串
      - start    字符串开始搜索的位置。默认为第一个字符,第一个字符索引值为0
      - end      字符串中结束搜索的位置。字符中第一个字符的索引为 0。默认为字符串的最后一个位置。
    - 返回值：返回该字符和字符串出现次数
    - 示例：

    ```python
    info = 'welcome to oldboy!'
    num1 = info.count('o')
    print(num)
    #4
    num2 = info.count('o',5,10)
    print(num2)
    #1
    ```

    

14. isalpha

    - 检测字符串是否只由字母组成
    - 语法：str.isalpha()
    - 参数：无
    - 返回值：如果字符串至少有一个字符并且所有字符都是字母则返回 True,否则返回 False
    - 示例：

    ```python
    a = '你好'
    value = a.isalpha()
    print(value)
    #True
    
    a = '1a2b3c'
    value = a.isalpha()
    print(value)
    #False
    
    ```

15. partition

    - 分隔符将字符串进行分割。如果字符串包含指定的分隔符，则返回一个3元的元组，第一个为分隔符左边的子串，第二个为分隔符本身，第三个为分隔符右边的子串。
    - 语法：str.aprtition(str)
    - 参数：str指定的分隔符。
    - 返回值：返回一个3元的元组，第一个为分隔符左边的子串，第二个为分隔符本身，第三个为分隔符右边的子串。
    - 示例

    ```python
    a = 'k1|v1'
    t = a.partition('|')
    print(t)
    #('k1', '|', 'v1')
    ```

16. isalnum

- 检测字符串是否由字母和数字组成
- 语法：str.isalnum()
- 参数：无
- 返回值： 至少有一个字符并且所有字符都是字母或数字则返回 True,否则返回 False
- 示例：

```python
strs = 'abc23'
values = strs.isalnum()
print(values)
#True
```

### 3.4 列表（列表有序可变）

1. append()

   - 列表后面追加元素（可以是字符串，布尔值，整型，集合，列表，元组）

   ```python
   a = [1,2,3]
   a.append(4)
   print(a)
   #[1,2,3,4]
   a.append('3')
   print(a)
   #[1,2,3,4,'3']
   a.append({1,2,3})
   print(a)
   #[1,2,3,4,'3',{1,2,3}]
   ```

2. insert()

   - 向列表中插入一个元素。第一个参数为索引位置，第二个参数为插入内容。

   ```python
   a = [1,2,,3]
   a.insert(0.'alex')
   print(a)
   #['alex',1,2,3]
   ```

3. remove()

   - 列表中移除某个元素。

   ```python
   a = [1,2,3]
   a.remove(3)
   print(a)
   #[1,2]
   v = ['a','b','c']
   v.remove('a')
   print(v)
   #['b','c']
   ```

4. pop()

   - 删除列表一个元素，括号内为索引，默认删除最后一个元素，如果带索引，为删除指定索引位置的值，并返回删除值。

   ```python
   a = ['a','b','c','d']
   a.pop(1)   
   #'b'
   print(a)
   #['a','c','d']
   
   ```

   

5. clear()

   - 清空列表

   ```python
   a = [1,2,3,4,5]
   a.clear()
   print(a)
   #[]
   ```

6. extend()

   - 函数末尾追加一个序列的值。

   ```python
   a = [1,2,3,4]    
   a.extend([7,8,9])    
   a
   #[1, 2, 3, 4, 7, 8, 9]
   ```

7. reverse()

   - 列表反转。

   ```python
   a = ['a','b','c','d']
   a.reverse()
   print(a)
   #['d', 'c', 'b', 'a']
   ```

8. sort()

   - 列表排序，reverse默认为False,升序排列。当reverse为True，降序排列。

   ```python
   a = [4,2,3,1]
   a.sort()
   print(a)
   #[1,2,3,4]
   a.sort(reverse=True)
   print(a)
   #[4, 3, 2, 1]
   ```

### 3.5集合（集合无序可变）

- 集合是可变的

- 集合是无序的

- 集合内的元素是不可变的

- 集合是可迭代的

  ```python
  #集合推导式
  a = {x+1 for x in range(10) if x%2==0}
  print(a)
  {1, 3, 5, 7, 9}
  ```

1. add()

   - 向集合中添加一个元素，此元素为不可变类型：str,set,int,bool。

   ```python
   a = {'a','b','c'}	     
   a.add('e')	     
   a	     
   #{'c', 'b', 'e', 'a'}
   ```

2. discard()

   - 删除集合中一个值。

   ```python
   a = {'a','b','c'}
   a.discard('a')
   print(a)
   #{'b','c'}
   ```

3. update()

   - 更新呢集合中的值。可以是str/int/tuple/bool/list/set

   ```python
   a = {'g', 'e', 'f', 'a'}
   a.update([1,2,3])
   print(a)
   #{'g', 1, 2, 'e', 3, 'f', 'a'}
   ```

4. intersection()

   - 求两个集合的交集。

   ```python
   a = {1,2,3}
   b = {3,4,5}
   c = a.intersection(b)
   print(c)
   #{3}
   
   a = {1,2,3}
   b = {3,4,5}
   t = a&b
   print(t)
   #{3}
   ```

5. union()

   - 求两个集合的并集。

   ```python
   a = {1,2,3}
   b = {3,4,5}
   c =  a.union(b)
   #{1, 2, 3, 4, 5}
   
   a = {1,2,3}
   b = {3,4,5}
   c = a|b
   print(c)
   #{1, 2, 3, 4, 5}
   ```

6. difference()

   - 求两个集合的差集。

   ```python
   a = {1,2,3}
   b = {3,4,5}
   c = a.difference(b)
   print(c)
   #{1,2}
   ```

7. symmetric_difference()

   - 求两个集合交集的补集。

   ```python
   a = {1,2,3}
   b = {3,4,5}
   c = a.symmetric_difference(b)  
   print(c)
   #{1, 2, 4, 5}
   
   t = a^b
   print(t)
   #{1,2,4,5}
   
   print((a|b)-(a&b)) #{1,2,4,5}
   print((a-b)|(b-a)) #{1,2,4,5}
   ```

8. 求两个集合补集。

   ```python
   a = {1,2,3}
   b = {3,4,5}
   a - b    #b的补集
   #{1,2}
   b - a    #a的补集
   #{3,4}
   ```

   9.超集，子集

```python
a = {1,2,3}
b = {2,3}
a>b  #True
b<a  #True
```

### 3.6字典（字典无序可变）

1. keys()

   - 获取字典中所有的键，在python2中返回是列表，python3中返回迭代器，可循环但不索引。

   ```python
   a = {'k1':'v1','k2':'v2','k3':'v3'}
   print(a.keys())
   #dict_keys(['k1', 'k2', 'k3'])
   ```

2. values()

   - 获取字典中所有的值，在python2中返回是列表，python3中返回迭代器，可循环但不索引。

   ```python
   a = {'k1':'v1','k2':'v2','k3':'v3'}
   print(a.values())
   #dict_values(['v1', 'v2', 'v3'])
   ```

3. items()

   - 获取字典中所有的键值对，在python2中返回是列表，python3中返回迭代器，可循环但不索引。

   ```python
   a = {'k1':'v1','k2':'v2','k3':'v3'}
   for key,value in a.items():
   	print(key,values)
   #k1 v1
   #k2 v2
   #k3 v3
   ```

4. get()

   - 获取字典中的值，若字典没有查到返回一个None,也可以自己设置值。

   ```python
   a = {'k1':'v1','k2':'v2','k3':'v3'}
   b = a.get('k1')
   print(b)
   #'v1'
   c = a.get('k4')
   print(c)
   #None
   
   d = a.get('k4',666)
   print(d)
   #666
   ```

5. pop()

   - 删除字典键值对。括号内必须有键，并返回删除的值。

   ```python
   a = {'k1':'v1','k2':'v2','k3':'v3'}
   a.pop('k1')
   #'v1'
   ```

6. update()

   - 更新一个字典，若两个字典键相同只更新值。

   ```python
   a1 = {'k1':'v1','k2':'v2','k3':'v3'}
   a2 = {'k2':'vv','k3':'vvv','k4':'vvvv'}
   a1.update(a2)
   print(a1)
   #{'k2': 'vv', 'k3': 'vvv', 'k4': 'vvvv'}
   ```

7. fromkeys()

   - 函数用于创建一个新字典，以序列seq中元素做为字典的键，value为字典所有键对应的初始值
   - 语法：dict.fromkeys(seq[,value])
   - 参数：
     - seq    字典键值列表
     - value    可选参数，设置键序列的值
   - 返回值：返回一个新字典

   ```python
   seq = ['baidu','sina','jingdong','alibaba']
   temp = dict.fromkeys(seq)
   print(temp)
   #{'baidu': None, 'sina': None, 'jingdong': None, 'alibaba': None}
   
   seq = ['baidu','sina','jingdong','alibaba']
   temp = dict.fromkeys(seq,1)
   #{'baidu': 1, 'sina': 1, 'jingdong': 1, 'alibaba': 1}
   ```

   

### 3.7元组（元组有序不可变）

1. 无。

### 3.8 公共功能

1. len（字符串，列表，元组，字典，集合）

   - 计算长度。

   ```python
   a = [1,2,3]
   print(len(a))
   #3
   b = {'k1':1,'k2':2,'k3':3}
   print(len(b))
   #3
   a = {1,2,3}
   print(len(a))
   #3
   ```

2. 索引（列表，元组，字符串,，字典）

   - 取值

   ```python
   a = [1,2,3]
   print(a[0])
   #1
   a = (1,2,3)
   print(a[0])
   #1
   a = {'k1':1,'k2':2,'k3':3}
   print(a['k1'])
   #1
   ```

3. 切片(列表，元组，字符串)

   - 提取数值

   ```python
   a = [1,2,3,4,5]
   print(a[:2])
   #[1,2]
   a = 'oldboy'
   print(a[1:4])
   #'ldb'
   ```

4. 步长(列表，元组，字符串)

   ```python
   a = [1,2,3,4,5,6]
   print(a[::2])
   #[1,3,5]
   ```

5. for 循环(列表，元组，字符串，集合，字典)

   ```python
   a = 'abc'
   for i in a:
   	print(i)
   #'a'
   #'b'
   #'c'
   ```

6. 删除del(列表，字典)

   - 删除。

   ```python
   a = {'k1':1,'k2':2,'k3':3}
   del a['k1']
   print(a)
   #{'k2': 2, 'k3': 3}
   
   a = [1,2,,3]
   del a[0]
   print(a)
   #[2,3]
   ```

### 3.9 嵌套

#### 3.9.1列表/字典/集合---->不能放在集合中，并且不能作为字典的key.

#### 3.9.2哈希

- 是在内部会将值进行哈希算法并得到一个数值（对应内存地址），以后便于快速查找。hash函数可以应用于数字，字符串和对象，不能直接应用于list,set,dictionary。

## 第四章 文件操作

### 4.1 文件基本操作

```python
obj = open('路径',mode='模式',encoding='编码')
obj.write()
obj.read()
obj.close()
```

### 4.2  打开模式

- r / w / a

  r只读

  w只写

  a追加

- r+ / w+ / a+

  - r+读写模式
  - w+写读模式
  - a+追加写读

- rb / wb / ab

  - rb以二进制方式读
  - wb以二进制方式写
  - ab以二进制方式追加

- r+b / w+b / a+b 

  - r+b以二进制方式读写
  - w+b以二进制方式写读
  - a+b以二进制方式追加

### 4.3 操作

- read() , 全部读到内存

- read(1) 

  - 1表示一个字符

    ```python
    obj = open('a.txt',mode='r',encoding='utf-8')
    data = obj.read(1) # 1个字符
    obj.close()
    print(data)
    ```

  - 1表示一个字节

    ```python
    obj = open('a.txt',mode='rb')
    data = obj.read(3) # 1个字节
    obj.close()
    ```

- write(字符串)

  ```python
  obj = open('a.txt',mode='w',encoding='utf-8')
  obj.write('中午你')
  obj.close()
  ```

- write(二进制)

  ```python
  obj = open('a.txt',mode='wb')
  
  # obj.write('中午你'.encode('utf-8'))
  v = '中午你'.encode('utf-8')
  obj.write(v)
  
  obj.close()
  ```

- seek(光标字节位置)，无论模式是否带b，都是按照字节进行处理。

  ```python
  obj = open('a.txt',mode='r',encoding='utf-8')
  obj.seek(3) # 跳转到指定字节位置
  data = obj.read()
  obj.close()
  
  print(data)
  
  
  
  
  obj = open('a.txt',mode='rb')
  obj.seek(3) # 跳转到指定字节位置
  data = obj.read()
  obj.close()
  
  print(data)
  ```

- tell(), 获取光标当前所在的字节位置

  ```python
  obj = open('a.txt',mode='rb')
  # obj.seek(3) # 跳转到指定字节位置
  obj.read()
  data = obj.tell()
  print(data)
  obj.close()
  ```

- flush，强制将内存中的数据写入到硬盘

  ```python
  v = open('a.txt',mode='a',encoding='utf-8')
  while True:
      val = input('请输入：')
      v.write(val)
      v.flush()
  
  v.close()
  ```



### 4.4 关闭文件

第一种打开关闭文件方式：

```python
v = open('a.txt',mode='a',encoding='utf-8')

v.close()
```

第二种打开关闭文件方式：

```python
with open('a.txt',mode='a',encoding='utf-8') as v:
    data = v.read()
	# 缩进中的代码执行完毕后，自动关闭文件
```



### 4.5  文件内容的修改

```python
with open('a.txt',mode='r',encoding='utf-8') as f1:
    data = f1.read()
new_data = data.replace('飞洒','666')

with open('a.txt',mode='w',encoding='utf-8') as f1:
    data = f1.write(new_data)
```

大文件修改

```
f1 = open('a.txt',mode='r',encoding='utf-8')
f2 = open('b.txt',mode='w',encoding='utf-8')

for line in f1:
    new_line = line.replace('阿斯','死啊')
    f2.write(new_line)
f1.close()
f2.close()
```

```
with open('a.txt',mode='r',encoding='utf-8') as f1, open('c.txt',mode='w',encoding='utf-8') as f2:
    for line in f1:
        new_line = line.replace('阿斯', '死啊')
        f2.write(new_line)
```

## 第五章 函数

### 5.1三元运算

- 表示形式：表达式1 if 条件表达式 else 表达式2

- 当表达式返回True时，返回结果表达式1，否则返回结果表达式2。示例：c = a if a < b else b

```python
#让用户输入值，如果值是整数，则转换成整数，否则赋值为None
data = input('>>>>')
value = int(data) if data.isdecimal() else None
```

### 5.2函数

	目前我们学习的编程都是面向过程编程，现在使用函数式编程。这样可以增加代码的重用行，可读性。适用于代码重复执行，代码量特别多，可以选择通过函数进行代码分割。

- 函数的参数传递的是什么？【内存地址=引用】

  ```python
  v = [11,2,33,4]
  def func(args):
      print(id(args))
  print(id(v))
  func(v)
  #传递内存地址
  ```

  

### 5.3函数的基本结构

```python
#函数的定义
def 函数名():
    """
    函数式干什么的...
    name:
    age:
    return:
    """
    #函数内容
    pass
#函数的执行
函数名()

#例题：
def get_list():
    v = [1,2,3,4]
    print(v[0])
get_list()
#1
#这里需要注意，若函数不被调用，则内部代码永远不会被执行。
```

### 5.4参数设置

- 在用def关键字定义函数时函数名后面括号里的变量称作为形式参数。实参全称为实际参数，在调用函数时提供的值或者变量称作为实际参数。
- 传参：给函数传递信息的时候将实际参数交给形式参数的过程被称为传参。
- 口诀：形参就是变量名，实参是值，传参就是赋值。

```python
def get_list(index): #此处index为形参
    v = [1,2,3,4,5]
    print(v[index])
get_list(0)   #此处0为实参
#1
```

- 练习

```python
# 1. 请写一个函数，函数计算列表 info = [11,22,33,44,55] 中所有元素的和。
def sum_num():
    sum = 0
    info = [11, 22, 33, 44, 55]
    for i in info:
        sum += i
    print(sum)
sum_num()
#165
# 2. 请写一个函数，函数计算列表中所有元素的和
def sum_num(a):
    sums = 0
    for i in a:
        sums +=i
    print(sums)

sum_num([1,2,3,4,5])
#15
# 3. 请写一个函数，函数将两个列表拼接起来。
def joins(a1,a2):
    result = []
    result.extend(a1)
    result.extend(a2)
    print(result)

joins([1,2,3,4],[5,6,7,8])
#[1,2,3,4,5,6,7,8]
# 4. 计算一个列表的长度.
def lens(arg):
    count = 0
    for i in arg:
        count +=1
    print(count)

lens([1,2,3,4,5])
#5
```

### 5.5返回值

- python 函数使用 return 语句返回 "返回值"，可以将其赋给其它变量作其它的用处。
- 所有函数都有返回值，如果没有 return 语句，会隐式地调用 return None 作为返回值。
- 一个函数可以存在多条 return 语句，但只有一条可以被执行，如果没有一条 reutrn 语句被执行，同样会隐式调用 return None 作为返回值。
- 如果有必要，可以显式调用 return None 明确返回一个None(空值对象)作为返回值，可以简写为 return，不过 python 中懒惰即美德，所以一般能不写就不写。
- 如果函数执行了 return 语句，函数会立刻返回，结束调用，return 之后的其它语句都不会被执行了。

```python
def func(arg):
    return 1 #返回值为1,不写默认：return None
val = func('alex')

# 1. 让用户输入一段字符串，计算字符串中有多少A字符的个数。有多少个就在文件a.txt中写多少个“alex”。
def calculate(content):
    count = 0
    for i in content:
        if i == 'A':
            count +=1
    return count

def writes(count):
    with open('a.txt',mode = 'w',encoding='utf-8') as f:
        message = 'Alex'*count
        f.write(message)
    return True

info = input('请输入信息：')
num = calculate(info)
status = writes(num)
if status:
    print('写入成功')
else:
    print('写入不成功')
```

### 5.6函数格式

```python
#情况1
def f1():
    pass
f1()

#情况2
def f2(a1):
    pass
f2(123)

#情况3
def f3():
    return 1
v1 = f3()

#情况4
def f4(a1,a2):
    return 999
v2 = f4(1,7)
```

### 5.7函数阶段性练习题

```python
# 1. 写函数，计算一个列表中有多少个数字，打印： 列表中有%s个数字。 #    提示：type('x') == int 判断是否是数字。
def len_list(lst):
    count = 0
    for i in lst:
        if type(i) == int:
            count +=1
    msg = '列表中共有%d个数字.'%count
    print(msg)

len_list(['1',22,'aa','3',4,5])
# 2. 写函数，计算一个列表中偶数索引位置的数据构造成另外一个列表，并返回。 
def odd_list(lst):
    new_lst = lst[::2]
    return new_lst

a = odd_list([1,2,3,4,5,6])
print(a)
# 3. 读取文件，将文件的内容构造成指定格式的数据，并返回。 
""" 
a.log文件    
alex|123|18   
eric|uiuf|19    
	... 
目标结构： 
a.  ["alex|123|18","eric|uiuf|19"] 并返回。 
b. [['alex','123','18'],['eric','uiuf','19']] 
c. [    
{'name':'alex','pwd':'123','age':'18'},    {'name':'eric','pwd':'uiuf','age':'19'}, 
] 
"""
def lst_a():
    l = []
    with open('a.log',mode='r',encoding='utf-8') as f:
        for line in f:
            value = line.strip()
            l.append(value)
    return l

def lst_b():
    l = []
    with open('a.log',mode='r',encoding='utf-8') as f:
        for line in f:
            new_list = (line.strip()).split('|')
            l.append(new_list)
    return l

def dic_c():
    l = []
    with open('a.log',mode='r',encoding='utf-8') as f:
        for line in f:
            new_list = (line.strip()).split('|')
            print(new_list)
            dic = {}
            dic['name'] = new_list[0]
            dic['pwd'] = new_list[1]
            dic['age'] = new_list[2]
            l.append(dic)
    return l
```

### 5.8参数的分类

- 位置参数（调用函数执行过程）

  ```python
  def func(a,b,c,d,e,f,g,h): 
      print(a,b,c,d,e,f,g,h)
  func('hello',1,True,[1,2,3],(1,2,3),{1,2,3},{'k1':'v1'},None)#<------实参为任意类型
  #hello 1 True [1, 2, 3] (1, 2, 3) {1, 2, 3} {'k1': 'v1'} None
  ```

- 关键字参数（调用函数执行过程）

```python
def func(a,b):
    print(a,b)
func(b=1,a=2) #<------无顺序要求，因为参数赋值
#1 2
#注意：关键字传参和位置传参混用时，位置传参必须在关键字传参前面，并且位置传参和关键字传参总和等于总参数个数。
def func(a,b,c):
    print(a,b,c)
#func(1,2,c=3)    ✔
#func(1,b=2,c=3)  ✔
#func(a=1,b=2,c=3)✔
#func(a=1,2,3)    ✖
#func(1,2,a=3)    ✖
```

- 混合参数（默认参数）

  - 对于函数的默认值慎用可变类型

    ```python
    #如果想给value设置默认是空列表
    #不推荐（坑）
    def func(data,value=[]):
        pass
    #推荐
    def func(data,value=None):
        if not value:
            value = []
    #此处有坑#####################################################
    def func(a,b = []):
        b.append(a)
        return b
    l1 = func(1)
    l2 = func(2,[11,22])
    l3 = func(3)
    print(l1,l2,l3)
    #[1,3] [11,22,2] [1,3]
    
    ```

    

```python
def func(a1,a2,a3=9,a4=10):
    print(a1,a2,a3,a4)
func(11,22)
func(11,22,10)
func(11,22,10,100)
func(11,22,10,a4=100)
func(11,22,a3=10,a4=100)
func(11,a2=22,a3=10,a4=100)
func(a1=11,a2=22,a3=10,a4=100)

```

- 万能参数

  - 动态接收位置参数：可以接收任意个数的位置参数（只能是位置参数），并将参数转换成元组。

    ```python
    #当调用函数实参没有加*
    def func(*args):
        print(args)
    func(1,2,3,4)  #<----------传入数值打印出的是元组
    #(1, 2, 3, 4)
    func([1,2,3,4]) #<----------传入列表，打印是元组套列表
    #([1, 2, 3, 4],)
    func((1,2,3,4)) #<----------传入元组，打印的是元组套元组
    #((1, 2, 3, 4),)
    
    #当调用函数实参加*
    def func(*args):
        print(args)
    
    func(*(1,2,3,4))
    #(1, 2, 3, 4)
    func(*[1,2,3,4])
    #(1, 2, 3, 4)
    func(*'abcd')
    #('a', 'b', 'c', 'd')
    func(*range(1,5))
    #(1, 2, 3, 4)
    #传入参数必须为可迭代对象，否则报错
    ```

  - 动态接收关键字参数：可以接收任意个数的关键字参数，并将参数转换成字典。

    ```python
    #调用函数实参无**
    def func(**kwargs):
        print(kwargs)
    
    func(k1=1,k2='alex')
    #{'k1': 1, 'k2': 'alex'}
    # 如果这样调用：func({'k1':'v1','k2':'v2'})会报错，无**只能赋值方式调用
    
    #调用函数实参有**
    def func(**kwargs):
        print(kwargs)
    func(**{'k1':'v2','k2':'v2'}) 
    #{'k1':'v2','k2':'v2'}
    #如果这样调用 func(**{'a'=1,'b'=2})报错，有**字典方式调用。
    ```

  - 动态接收位置参数+动态接收关键字参数:可以接收任意个数的位置参数和关键字参数，但位置参数要在关键字参数前面。

    ```python
    def func(*args,**kwargs):
        print(args,kwargs)
    
    func(*[1,2,3],k1=1,k2=5,k3=9)
    # (1, 2, 3) {'k1': 1, 'k2': 5, 'k3': 9}
    func(11,22,*[33,44],k3='wusir',**{'k1':1,'k2':2})
    # (11, 22, 33, 44) {'k3': 'wusir', 'k1': 1, 'k2': 2}
    #键不能重复否则报错
    ```

  - 参数相关重点(常见函数传参格式)

    ```python
    def func1(a1,a2):
        pass 
    
    def func2(a1,a2=None):
        pass 
    
    def func3(*args,**kwargs):
        pass 
    ```

  - 优先级：位置参数 > *args(动态位置参数) > 默认值参数 > **kwargs(动态默认参数)

### 5.9作用域与函数嵌套

1. python文件中是全局作用域

2. 函数内部是局部作用域（python内一个函数就是一个作用域）

3. 作用域中查找数据规则：优先在自己的作用域找数据，自己没有就去“父级”-->"父级"-->直到全局，全部没有报错，注意：父级作用域中的值。

   ```python
   a = 1
   def s1():
       x1 = 666
       print(x1)
       print(a)
       print(b)
   
   b = 2
   print(a)
   s1()
   a = 88888
   def s2():
       print(a,b)
       s1()
   s2()
   #1
   #666
   #1
   #2
   #88888,2
   #666
   #88888
   #2
   ```

- 练习题

  ```python
  #第一题
  x = 10
  def func():
      x = 9
      print(x)
      def x1():
          x = 999
          print(x)
  
  func()
  #9
  #第二题
  x = 10
  def func():
      x = 9
      print(x)
      def x1():
          x = 999
          print(x)
      x1()
  
  func()
  #9
  #999
  #第三题
  x = 10
  def func():
      x = 9
      print(x)
      def x1():
          x = 999
          print(x)
      print(x)
      x1()
  
  func()
  #9
  #9
  #999
  #第四题
  x = 10
  def func():
      x = 8
      print(x)
      def x1():
          x = 999
          print(x)
      x1()
      print(x)
  
  func()
  #8
  #999
  #8
  #第五题
  x = 10
  def func():
      x = 8
      print(x)
      def x1():
          print(x)
      x1()
      print(x)
  
  func()
  #8
  #8
  #8
  #第六题
  x = 10
  def func():
      x = 8
      print(x)
      def x1():
          print(x)
      x = 9
      x1()
      x = 10
      print(x)
  
  func()
  #8
  #9
  #10
  #第七题
  x = 10
  def func():
      x = 8
      print(x)
      def x1():
          print(x)
  
      x1()
      x = 9
      x1()
      x = 10
      print(x)
  
  func()
  #8
  #8
  #9
  #10
  ```

### 5.10 global/nonlocal

- 子作用域中只能找到父级中的值，默认无法重新为父级的变量进行赋值。global/nonlocal可以强制。
- global :在函数内部修改全局的变量，如果全局中不存在就创建一个变量。
- nonlocal:只修改上一层变量，如果上一层中没有变量就往上层找，只会找到函数的最外层,不会找到全局进行修改。

```python
# #####################
name = 'oldboy'
def func():
    name = 'alex' # 在自己作用域再创建一个这样的值。
    print(name)
func()
print(name)
#'alex'
#'oldboy'
#未调用global，对于函数传入参数为不可变类型，只能在函数内部使用
# #####################
name = [1,2,43]
def func():
    name.append(999)
    print(name)
func()
print(name)
#[1,2,43,999]
#[1,2,43,999]
#未调用global，对于函数传入参数为可变类型，可修改参数值。

# ###################### global
#如果非要对全局的变量进行赋值参照如下：
#示例一：
name = ["老男孩",'alex']
def func():
    global name
    name = '我'
func()
print(name)
#我

# 示例二
name = "老男孩"
def func():
    name = 'alex'
    def inner():
        global name
        name = 999
    inner()
    print(name)
func()
print(name)
#alex
#999
# ############################## nonlocal
#示例一
name = "老男孩"
def func():
    name = 'alex'
    def inner():
        nonlocal name # 找到上一级的name
        name = 999
    inner()
    print(name)
func()
print(name)
#999
#"老男孩"
#示例二：
name = "老男孩"
def func():
    name = 'alex'
    def inner():
        def inner_s():
            nonlocal name
            name = 123
        inner_s()
    inner()
    print(name)
func()
print(name)
#123
#老男孩
```

 - 注意：为书写规范，全局变量以后必须全部是大写。

### 5.11函数的小高级

#### 5.11.1函数名当作变量来使用

```python
#示例一
def func():
    print(123)
v1 = func
func()
v1()
#123
#123

#示例二
def func():
    print(123)
    
func_list = [func, func, func]
# func_list[0]()
# func_list[1]()
# func_list[2]()
for item in func_list:
    v = item()
    print(v)
#123
#None
#123
#None
#123
#None
#item 是func_list内的func,v = item()：item()等效于func(),并打印123，但func()返回值是None,v得到是func()的返回值，也就是None.

#示例三：
def func():
    print(123)

def bar():
    print(666)

info = {'k1': func, 'k2': bar}

info['k1']()
info['k2']()

a = info['k1']()
#123
#666

#123
#None

#示例四：（带坑题）
def func():
    return 123

func_list1 = [func,func,func]
func_list2 = [func(),func(),func()]

print(func_list1)
print(func_list2)
# [<function func at 0x0000016CDF631EA0>, <function func at 0x0000016CDF631EA0>, <function func at 0x0000016CDF631EA0>]
# [123, 123, 123]
#注意：func_list1列表内的func没有括号，表示只调用了func的内存地址。
info = {
    'k1':func,
    'k2':func(),
}

print(info)
#{'k1':<function func at 0x000002534C211EA0>,'k2':123}
```

#### 5.11.2函数当作参数进行传参

```python
#示例一
def func(arg):
    print(arg)
    
func(1)
func([1,2,3,4])
#1
#[1,2,3,4]
def show():
    return 999
func(show)
#<function show at 0x0000022B0A08CA60>
#func参数show传入func函数内，但打印show没有括号，所以只打印出show内存地址。

#示例二
def func(arg):
    v1 = arg()
    print(v1)
    
def show():
    print(666)
    
result = func(show)
print(result)
#666
#None
#None
#参数show传入func，v1=arg()等同于v1 = show(),执行show(),打印出666，因为show没有设置返回值，默认返回None,v1=None,并打印出None,最后result = func(show)，因为func函数没有return，默认返回None,把result打印出来就是None.
```



```python
#面试题：
#用户输入f1,f2,f3,f4,f5,将下列函数功能实现。不要用if条件语句。
def func():
    print('话费查新')
def bar():
    print('语音沟通')
def base():
    print('积分管理')
def show():
    print('宽带服务')
def test():
    print('人工服务')
#答案
info = {
    'f1': func,
    'f2': bar,
    'f3':base,
    'f4':show,
    'f5':test
}
choice = input('请选择要选择功能：')
function_name = info.get(choice)
if function_name:
    function_name()
else:
    print('输入错误')
```

### 5.12函数的中高级

#### 5.12.1函数做为返回值

#### 5.12.2闭包

- 闭包概念：为函数创建一块区域并为其维护自己数据，以后执行时方便调用【应用场景：装饰器、SQLAlchemy源码】（函数执行的流程分析需要知道到底谁创建的?）
- 在一个外函数中定义了一个内函数，内函数里运用了外函数的临时变量，并且外函数的返回值是内函数的引用。这样就构成了一个闭包。

```python
def fun():
    a = '123'
    def inner():
        print(a)
    return inner
v = func()
v()
#是闭包， fun函数开辟新空间，虽然内部没有内容，但v=func()  v在调用inner内部元素，fun函数内存没有释放销毁。
def fun():
    def inner():
        print(123)
    return inner()
func()
#不是闭包，fun()直接执行inner(),执行完毕，并且内部元素不被其他人使用，内存释放。
```



```python
#示例三：
def bar():
    def inner():
        print(123)
    return inner
v = bar()
v()
#123

#示例四：
name = 'oldboy'
def bar():
    name = 'alex'
    def inner():
        print(name)
    return inner
v = bar()
v()
#'alex'

#示例五：
name = 'oldboy'
def bar(name):
    def inner():
        print(name)
    return inner
v1= bar('alex')  #{name='alex',inner}
v2 = bar('eric') #{name='eric',inner}
v1()   #'alex'
v2()   #'eric'
#闭包，为函数创建一块区域（内部变量提供自己使用），为他以后执行提供数据。
```

```python
#练习题
#第一题
name = 'alex'
def base():
    print(name)
def func():
    name = 'eric'
    base()
func()
#'alex'
#坑，base()在全局创建的的函数，当func调用base,base本身函数没有name,它会向它的父集全局作用域找。name = 'alex'

#第二题：
name = 'alex'
def func():
    name = 'eric'
    def base():
        print(name)
    base()
func()
#'eric'

#第三题：
name = 'alex'
def func():
    name = 'eric'
    def base():
        print(name)
    return base
base = func()
base()
#'eric'

#第四题
info = []
def func():
    print(item)
for item in range(10):
    info.append(func)

info[0]()
#9
#for 循环item=9,列表info有10个func函数，当info[0]()时，调用第一个函数，此时打印9

info = []
def func(i):
    def inner():
        print(i)
    return inner
for item in range(10):
    info.append(func(item))
info[0]()
info[1]()
info[4]()
#0
#1
#4
#分析：for 循环后此时info=[func(0),func(1)...func(9)],info[0]相当于func(0),这样等同于：
def func(i):
    def inner():
        print(i)
    return inner
b = func(0)
b()
#0
#后面的1，4都与这个一样。

def func(name):
    def inner():
        print(name)
    return inner
v1 = func('alex')
v1()
v2 = func('eric')
v2()
#'alex'
#'eric'
```

#### 5.12.3 lambda方法(匿名函数)

- 用于表示简单的函数，简洁代码块，通常被创建匿名函数，省去了函数定义过程，直接返回需要的数据。

```python
#示例一:对比
def func1(a1,a2):
    return a1+a2

func2 = lambda a1,a2:a1+a2


#示例二：
func1 = lambda : 100 
print(func1)   #<-----------不加括号返回值为func1的内存地址
print(func1())
#<function <lambda> at 0x000002F1487D1EA0>
#100
func2 = lambda x1: x1 * 10
print(func2(2))
#20
func3 = lambda *args,**kwargs: len(args) + len(kwargs)
fu = func3(11,22,[3,4],k1=5,k2=7)
print(fu)
#5

DATA = 100
func4 = lambda a1: a1 + DATA
v = func4(1)
print(v)
#101

DATA = 100
def func():
    DATA = 1000
    func4 = lambda a1: a1 + DATA
    v = func4(1)
    print(v)
func()
#1001

func5 = lambda n1,n2: n1 if n1 > n2 else n2
v = func5(1111,2)
print(v)
#1111
```

- 练习题

```python
# 练习题1
USER_LIST = []
def func0(x):
    v = USER_LIST.append(x)
    return v 

result = func0('alex')
print(result)
#None
#列表append方法在原列表添加，不会返回新值.

# 练习题2

def func0(x):
    v = x.strip()
    return v 

result = func0(' alex ')
print(result)
#"alex"

############## 总结：列表所有方法基本上都是返回None；字符串的所有方法基本上都是返回新值 #################
# 练习题3
USER_LIST = []
func1 = lambda x: USER_LIST.append(x)

v1 = func1('alex')
print(v1)
print(USER_LIST)
#None
#['alex',]

# 练习题4
func1 = lambda x: x.split('l')

v1 = func1('alex')
print(v1)
#['a','ex']


# 练习题5
func_list = [lambda x:x.strip(), lambda y:y+199,lambda x,y:x+y]

v1 = func_list[0]('alex ')
print(v1)
#'alex'
v2 = func_list[1](100)
print(v2)
#299
v3 = func_list[2](1,2)
print(v3)
#3
```

- lambda表达式扩展用法：

```python
v1 = lambda *args: sum(args)
print(v1(1,2,3,4,5))
#15
#与for循环使用：
li = []
for x in range(5):
    li.append(lambda x:x**2)
print(li[0](2)) #4
print(li[1](2)) #4
print(li[0](3)) #9
print(li[1](3)) #9
print(li[2](3)) #9
print(li[3](3)) #9
print(li[4](3)) #9
print(li[0]())
#li里面存的是5个lambda函数

for x in range(5):
    li.append(lambda :x**2)
print(li[0]())
#16


v1 = lambda **kwargs:kwargs
p1 = v1(k=5)
print(p1)
#{'k': 5}

#filter函数
filter(lambda x: x % 3 == 0, [1, 2, 3])
#sorted函数
sorted([1, 2, 3, 4, 5, 6, 7, 8, 9], key=lambda x: abs(5-x))
#map函数
map(lambda x: x+1, [1, 2,3])
#reduce函数
reduce(lambda a, b: '{}, {}'.format(a, b), [1, 2, 3, 4, 5, 6, 7, 8, 9])

fs = map(lambda i:(lambda j: i*j),range(6))
print([f(2) for f in fs])
# [0, 2, 4, 6, 8, 10]


fs = [lambda j:i*j for i in range(6)]
print([f(2) for f in fs])
# [10, 10, 10, 10, 10, 10]

```

#### 5.12.4递归

- 函数内部自己调用自己，缺点效率低

- 注意：

  - 归其实就在过程或函数里调用自身
  - 在使用递归时，必须有一个明确的递归结束条件，称为递归出口

  ```python
  def func():
      print(1)
      func()
  func()
  #1
  #1
  #1
  #...  死循环
  #没有出口，死循环
  def fib(a,b):
      #1
      #1
      #2
      #3
      #5
      print(b)
      func(b,a+b)
  func(0,1)
  #斐波那契函数，无限循环
  #没有出口，死循环
  
  
  #输入一串字符串，然后用递归实现反转:
  def f(x):
      if x == -1:
          return ''
      else:
          return 'abcdef'[x] +f(x-1)
  print(f(len('abcdef')-1))
  ```

  - 尾递归

    - 先执行某个部分的计算，而这个结果也是做为参数传入下一次递归，这也就是说函数调用出现在调用者函数的尾部，他相对于传统递归的优点无序保存任何的局部变量，从内存消耗上实现节约

      ```python
      #传统递归
      def func(n):
          if n == 1:
              return n
          else:
              return n+func(n-1)
      # func(5)
      # 5+fucn(4)
      # 5+(4+fucn(3))
      # 5+(4+(3+func(2)))
      
      #尾递归
      def func1(n,a=0):
          if n==0:
              return a
          else:
              return func1(n-1,a+n)
      #a = func1(5)
      #print(a)
      ```

      

### 5.13高阶函数

- map

  - 用函数和可迭代对象中的每个元素作为参数计算出新的可迭代对象，当最短的一个可迭代对象完成迭代后，此迭代生成结束。

  - 语法：map(function,iterable,...)

  - 参数：function    函数

           iterable     一个或多个序列

  - 返回值：python2返回列表，python3返回迭代器。（py2与py3区别）
  - 示例

  ```python
  v1 = [11,22,33,44]
  result = map(lambda x:x+100,v1)
  print(list(result))
  #[111,122,133,144]
  
  def square(x):
  	return x**2
  temp = map(square,[1,2,3,4])
  print(list(temp))
  #[1, 4, 9, 16]
  
  map(lambda x,y:x+y,[1,2,3],[3,4,5])
  #[4,6,8]
  
  #列表数值转换字符串再拼接（方法2）
  v1 = [11,22,33,44]
  result = ''.join(map(str,v1))
  print(result)
  #'11223344'
  
  #列表字符串转数值
  arr = ['22','44','66','88']
  arr = list(map(int,arr))
  print(arr)
  #[22, 44, 66, 88]
  ```

- filter

  - 筛选iterable中的数据，返回一个可迭代的对象，此可迭代对象将iterable进行筛选。
  - 语法：filter(function or None, iterable)
  - 参数：
    - function  函数
    - iterable  一个或多个序列
  - 返回值：列表，迭代对象
  - 示例：

  ```python
  v1 = [11,22,33,'asd',44,'xf']
  def func(x):
      if type(x) == int:
          return True
      return False
  result = filter(func,v1)
  print(list(result))
  #[11, 22, 33, 44]
  
  def is_odd(n):
      return n % 2 == 1
  newlist = filter(is_odd, [1, 2, 3, 4, 5, 6, 7, 8, 9, 10])
  print(newlist)
  #[1, 3, 5, 7, 9]
  
  ```

- reduce

  - 对参数序列中元素进行累计。用传给 reduce 中的函数 function（有两个参数）先对集合中的第 1、2 个元素进行操作，得到的结果再与第三个数据用 function 函数运算，最后得到一个结果。
  - 语法：reduce(function,iterable)
  - 参数：
    - function        函数，要求有两个参数
    - iterable         可迭代对象
  - 返回值：返回函数计算结果
  - 示例：
  - 注意：在python3使用reduce需要导入模块functools

  ```python
  import functools
  def add(x,y):
      return x+y
  v1 = [1,2,3,4,5]
  result = functools.reduce(add,v1)
  print(result)
  #15
  
  v1 = ['wo','hao','e']
  result = functools.reduce(lambda x,y:x+y,v1)
  print(result)
  #wohaoe
  ```

### 5.14内置函数

1. 自定义函数

2. 内置函数

   - 其他

     - len
     - open
     - id
     - type

   - 输入输出

     - print
     - input

   - 强制转换

     - dict()
     - list()
     - tuple()
     - int()
     - str()
     - bool()
     - set()

        见第三章数据类型。

   - 数学相关：

     - abs() 函数返回数字的绝对值。

       ```python
       v = abs(-5)
       print(v)
       #5
       ```

     - float() 转换成浮点型（小数）

       ```python
       v = 55
       v1 = float(55)
       print(v1)
       #55.0
       ```

     - max() 返回给定参数的最大值，参数可以为序列。

       ```python
       v = [1,2,311,21,3,]
       result = max(v)
       print(result)
       #311
       ```

     - min()，返回给定参数的最小值，参数可以为序列。

       ```python
       v = [1,2,311,21,3,]
       result = min(v)
       print(result)
       #1
       ```

     - sum()  求和

       ```python
       v = [1,2,311,21,3,]
       result = sum(v)
       print(result)
       #338
       ```

       sum方法列表,元组降维

       ```python
       a = [[1,2,3],[4,5,6],[7,8,9]]
       sum(a,[])
       #[1, 2, 3, 4, 5, 6, 7, 8, 9]
       b = ((1,2,3),(4,5,6),(7,8,9))
       sum(b,())
       #(1, 2, 3, 4, 5, 6, 7, 8, 9)
       ```

       

     - divmod()  两数相除的商和余数

       ```python
       a,b = divmod(1001,5)
       print(a,b)
       #200 1
       ```

       ```python
       #练习题，将数据分页进行展示
       """
       要求：
       	每页显示10条数据
       	让用户输入要查看的页面：页码
       """
       USER_LIST = []
       for i in range(1,836):
           temp = {'name':'你少妻-%s' %i,'email':'123%s@qq.com' %i }
           USER_LIST.append(temp)
       
       toltal_len = len(USER_LIST)
       pops = 10
       max_page,num = divmod(toltal_len,pops)
       if num != 0:
           max_page += 1
       
       content = int(input('请输入你要看的页码:'))
       if content<1 or content>84:
           print('请正确输入页码要求1-%s'%max_page)
       else:
           for item in USER_LIST[(content-1)*pops:content*pops]:
               print(item)
       ```

- type 查看类型

  ```python
  class Foo:
      pass
  
  obj = Foo()
  if type(obj) == Foo:
      print('Y')
  ```

- issubclass

  ```python
  class Base:
      pass
  class Base1(Base):
      pass
  class Foo(Base1):
      pass
  class Bar:
      pass
  print(issubclass(Bar,Base))
  print(issubclass(Foo,Base))
  # False
  # True
  ```

- isinstance

  - 函数来判断一个对象是否是一个已知的类型，类似type()
  - isinstance() 与type()区别。
    - type()  不会认为子类是一种父类类型，不考虑继承关系。
    - isinstance()会认为子类是一种父类类型，考虑继承关系。
  - 如果要判断两个类型是否相同推荐使用isinstance()

  ```python
  class Base(object):
      pass
  
  class Foo(Base):
      pass
  
  obj = Foo()
  
  print(isinstance(obj,Foo))  # 判断obj是否是Foo类或其基类的实例（对象）
  print(isinstance(obj,Base)) # 判断obj是否是Foo类或其基类的实例（对象）
  ```

- super

  ```python
  class A:
      def add(self,a):
          b = a+1
          print(b)
  
  class B(A):
      def add(self,x):
          super().add(x)
  b = B()
  b.add(2)
  
  
  class Base(object):
      def func(self):
          print('base func')
          return 123
  class Foo(Base):
      def func(self):
          v1 = super().func()
          print('foo.func',v1)
  obj = Foo()
  obj.func()
  #'base func'
  #'foo.func' 123
  
  class Base(object): # Base -> object
      def func(self):
          super().func()
          print('base.func')
  
  class Bar(object):
      def func(self):
          print('bar.func')
  
  class Foo(Base,Bar): # Foo -> Base -> Bar
      pass
  
  obj = Foo()
  obj.func()
  
  # super().func() 根据self对象所属类的继承关系，按照顺序挨个找func方法并执行(找到第一个就不在找了)
  ```

  

### 5.15进制的转换

#### 5.15.1.hex()函数

- 函数用于将10进制整数转换为16进制，以字符串形式表示。

- 语法：hex(x)

- 参数：x     10进制整数

- 返回值：16进制数，以字符串形式表示

- 示例

  ```python
  #将十进制转化成十六进制
  >>> hex(97)
  '0x61'
  >>> hex(65)
  '0x41'
  >>> hex(49)
  '0x31'
  ```

#### 5.15.2.oct()函数

- 函数将一个整数转换成8进制字符串。

- 语法：oct(x)

- 参数：x   整数

- 返回值：返回8进制字符串。

- 实例

  ```python
  #将十进制转八进制
  >>> oct(255)
  '0o377'
  >>> oct(97)
  '0o141'
  >>> oct(12)
  '0o14'
  ```

#### 5.15.3.bin()函数

- 返回一个整数 int 或者长整数 long int 的二进制表示

- 语法：bin(x)

- 参数： x  整数

- 返回值:返回2进制字符串

- 实例

  ```python
  #十进制转二进制
  >>> bin(65)
  '0b1000001'
  >>> bin(97)
  '0b1100001'
  ```

#### 5.15.4.二进制、八进制、十六进制转换十进制：

- 二进制转换十进制

  ```python
  >>> int('1010101010',base=2)
  682
  >>> int('100000',base=2)
  32
  ```

  

- 八进制转换十进制

  ```python
  >>> int('0o14',base=8)
  12
  >>> int('0o141',base=8)
  97
  >>> int('0o377',base=8)
  255
  ```

- 十六进制转换十进制

  ```python
  >>> int('0x61',base=16)
  97
  >>> int('0x41',base=16)
  65
  ```

#### 5.15.5编码补充chr()和ord()

- chr 将十进制数字转换成unicode编码中的对应字符串

  ```python
  v = chr(97)
  print(v)
  #'a'
  v = chr(65)
  #'A'
  ```

- ord 根据字符在unicode编码中找到其对应的十进制

  ```python
  num = ord('中')
  print(num)
  #20013
  ```

#### 5.15.6常用数据类型方法中返回值？

1. 无返回值

   ```python
   #例子：
   l = [1,2,3]
   result = l.append(4)
   print(l)
   print(result)
   #[1,2,3,4]
   #None
   ```

2. 有返回值(字符串常用函数都有返回值)

   ```python
   #例子
   ip = '192.168.1.1'
   result = ip.split('.')
   print(ip)
   print(result)
   #192.168.1.1
   #['192', '168', '1', '1']
   ```

3. 有返回值并且修改数据

   ```python
   l = [1,2,3,4]
   result = l.pop()
   print(l)
   print(result)
   #[1, 2, 3]
   #4
   ```

   4. 常用数据类型返回值：

      1.字符串（str）

      - strip()	返回字符串
      - split()        返回列表
      - replace()   返回字符串
      - join()         返回字符串
      - upper/lower  返回字符串

      2.列表（list）

      - append()	无
      - insert()           无
      - pop()              返回要删除的数据
      - remove()        无
      - find()/index()   返回索引位置
      - extend            无

      3.字典（dict）

      - get	          返回值或None
      - keys                返回键的列表
      - values             返回值得列表
      - items               返回键值对的列表

      4.集合（set）

      - add	                                无
      - discard                                  无
      - intersection                           有
      - difference                              有
      - union                                     有
      - symmetric_difference           有

    5.思考题：当同时运行一个函数时函数内部数据是否混乱

```python
def fun(x):
    print(x)
    
fun(1)
fun(2)
#当fun(1)与fun(2)同时运行，不会数据混乱
#函数在执行时，会重新开辟一个内存空间， 函数之间同时执行互不影响
#当函数执行完，并且内部元素无其他人使用 进行垃圾回收。
```

#### 5.16装饰器

##### 5.16.1装饰器：

- 装饰器本质：就是一个闭包函数

- 装饰器功能：在不修改原函数及其调用方式的情况下对原函数功能进行扩展。

  ```python
  ###############################引入装饰器前奏#############################
  def base():
      print(1)
  
  def bar():
      print(2)
  
  bar = base()
  bar
  #1
  #########################################################################
  def func():
      def inner():
          pass
      return inner
  
  v = func()   #inner函数
  print(v)
  #########################################################################
  def func(arg):
      def inner():
          print(arg)
      return inner
  
  v1 = func(1)
  v2 = func(2)
  
  v1()
  v2()
  #1
  #2
  
  #########################################################################
  def func(arg):
      def inner():
          arg()
      return inner
  
  def f1():
      print(123)
  
  v1 = func(f1)
  v1()
  #123
  
  #########################################################################
  def func(arg):
      def inner():
          arg()
      return inner
  
  def f1():
      print(123)
      return 666
  v1 = func(f1)  #执行inner函数，相当于等于inner
  result = v1()  #inner没有返回值，result为None
  print(result)
  # 123
  # None
  
  #########################################################################
  def func(arg):
      def inner():
          return arg()
      return inner
  
  def f1():
      print(123)
      return 666
  
  v1 = func(f1)
  result = v1()
  print(result)
  #123
  #666
  #解析：v1 = func(f1)，执行func函数，func函数重新开辟内存空间，传入f1（函数）为参数,在函数func内部创建inner函数，最后将inner返回给v1.当执行result=v1()时，v1()执行inner函数内的arg(),但inner内部无函数参数，向父级找到f1，并执行f1(),执行f1(),先打印123，返回值给result,最后打印result，666
  
  
  #################################装饰器################################
  #示例一：
  
  """代码参见示例二"""
  
  v1 = index()  
  #执行index函数，打印123并返回666赋值给v1
  #示例二
  def func(arg):
      def inner():
          print('before')
          v = arg()
          print('after')
          return v
      return inner
  
  def index():
      print('123')
      return '666'
  
  # v1 = index()
  
  v2 = func(index)
  v3 = v2()
  print(v3)
  #'before'
  #'123'
  #'after'
  #'666'
  #解析：执行func(index)函数，func函数重新开辟内存空间，并将index函数做为参数传入到函数内，然后生成inner函数，最后inner函数返回给v2.v3=v2(),v2()执行inner函数，打印第一条信息‘before’,再执行arg(),但是inner内部没有arg参数，向父级调用index，函数index执行打印第二条信息‘123’,并将返回值‘666’给v,再打印第三条信息‘after’,最后将返回值v赋值给v3,最后打印‘666’
  
  #示例三：
  
  """代码参见示例二"""
  
  v4 = func(index) #v4 = inner
  index = v4
  index()
  #'before'
  #'123'
  #'after'
  #解析：与上题基本一致，v4重新赋值给index（此处是变量，不是函数），index()执行inner函数。
  
  #示例四：
  
  """代码参见示例二"""
  
  index=func(index)
  index()
  #'before'
  #'123'
  #'after'
  #解析：与上题基本一致
  #########################################################################
  def func(arg):
      def inner():
          v = arg()
          return v
      return inner
  @func   #等同于index = func(index)
  def index():
      print(123)
      return 666
  index()
  #123
  #第一步：执行func函数并将下面的函数参数传递，相当于：func(index)
  #第二步：将func的返回值重新赋值给下面的函数名。index = func(index)
  ```

  ```python
  #计算函数执行时间
  import time
  def wrapper(func):
      def inner():
          start_time = time.time()
          v = func()
          end_time = time.time()
          print(end_time-start_time)
          return v
      return inner
  @wrapper
  def func1():  #func1 = wrapper(func1)
      time.sleep(2)
      print(123)
  @wrapper
  def fun2():
      time.sleep(1)
      print(123)
  def fun3():
      time.sleep(1.5)
      print(123)
  
  func1()
  #2.000645
  
  ```

##### 5.16.2装饰器小结

- 目的：在不改变原函数的基础上，在函数执行前后自定义功能

- 编写装饰器 和 应用

  ```python
  #装饰器的编写
  def x(func):
      def y():
          #前面内容
          ret = func()
          #后面内容
          return ret
     	return y
  #装饰器的应用
  @x
  def index():
      return 10
  #执行函数，将自动触发装饰器
  v = index()
  print(v)
  
  ```

- 应用场景：为函数扩展功能时，可以选择用装饰器。

- 装饰器格式：

  ```python
  def 外层函数(参数):
      def 内层函数(*args,**wargs):
          return 参数(*args,**kwargs)
      return 内层函数
  @外层函数
  def index():
      pass
  index()
  #*args,**wargs可装饰没有参数或一个参数或多个参数
  ```

##### 5.16.3被装饰的函数参数问题

```python
#当有一个参数时候
def deco(arg):
    def inner(a1):
        return arg(a1)
    return inner
@deco
def func(a1):
    pass
#当有两个参数时候
def deco(arg):
    def inner(a1,a2):
        return arg(a1,a2)
    return inner
@deco
def func(a1,a2):
    pass
#如果有多个函数，且参数个数不统一，怎样写一个统一装饰器
def x1(func):
    def inner(*args,**kwargs):
        return func(*args,**kwargs)
    return inner
@x1
def f1():
    pass
@x1
def f2(a1):
    pass
@x1
def f3(a1,a2):
    pass

```

##### 5.16.4装饰函数若有返回值

```python
def x1(func):
    def inner(*args,**kwargs):
        data = func(*args,**kwargs)
        return data
    return inner
@x1
def f1():
    print(123)
v1 = f1()
print(v1)
#123
#None   <---f1返回值为None


def x1(func):
    def inner(*args,**kwargs):
        data = func(*args,**kwargs)
        return data
    return inner
@x1
def f1():
    print(123)
    return 666
v1 = f1()
print(v1)
#123
#666

def x1(func):
    def inner(*args,**kwargs):
        data = func(*args,**kwargs)
    return inner
@x1
def f1():
    print(123)
    return 666
v1 = f1()
print(v1)
#123
#None
```

##### 5.16.5调用函数前后

```python
def x1(func):
    def inner(*args,**kwargs):
        print('调用原函数之前')
        data = func(*args,**kwargs) #执行原函数并获取返回值
        print('调用原函数之后')
    return inner
@x1
def index():
    print(123)
index()
#'调用原函数之前'
#123
#'调用原函数之后'
```

##### 5.16.6装饰器带有参数

```python
#不带参数装饰器
@xxx
def index():
    pass
#第一步：执行xxx(index),第二步将返回值赋值给index
#带参数的装饰器
@uuu(9)
def index():
    pass
#第一步：执行v1=uuu(9)，第二步：执行v1(index),第三步将返回值给index

# 写一个带参数的装饰器，实现：参数是多少，被装饰的函数就要执行多少次，把每次结果添加到列表中，最终返回列表。 
def deco(flag):
    print('deco函数')
    def wrapper(arg):
        print('wraooer函数')
        def inner():
            lst = []
            for item in range(flag):
                data = arg()
                lst.append(data)
            return lst
        return inner
    return wrapper


@deco(5)
def func():
    return 8
result = func()
print(result)
#deco函数
#wraooer函数
#[8, 8, 8, 8, 8]

 #写一个带参数的装饰器，实现：参数是多少，被装饰的函数就要执行多少次，并返回最后一次执行的结果【面试题】 
def deco(flag):
    print('deco函数')
    def wrapper(arg):
        print('wraooer函数')
        def inner(*args,**kwargs):
            for item in range(flag):
                data = arg(*args,**kwargs)
            return data
        return inner
    return wrapper


@deco(5)
def func():
    return 8
result = func()
print(result)
## 写一个带参数的装饰器，实现：列表中元素并返回执行结果中最大的值。 
def deco(flag):
    print('deco函数')
    def wrapper(arg):
        print('wraooer函数')
        def inner(*args,**kwargs):
            value = 0
            for item in args:
                if item >value:
                    value = item
            return value
        return inner
    return wrapper


@deco(5)
def func(*args,**kwargs):
    return 8
result = func(*[1,2,3,4])
print(result)
#deco函数
#wraooer函数
#4

```



#### 5.17推导式

- 列表推导式

  - 基本结构：方便的生成一个列表

    ```python
    """
    格式：
    	v1 = [i for i in 可迭代对象]
    	v2 = [i for i in 可迭代对象 if 条件]   #条件为true才进行append
    """
    v1 = [i for i in 'alex']
    v2 = [i+100 for i in range(10)]
    v3 = [99 if i>5 else 66 for i in range(10)]
    ```

    ```python
    def func():
        return 100
    v4 = [func for i in range(10)]
    #[func,func,func.....]
    
    v5 = [lambda:100 for i in range(10)]
    result = v5[9]()
    print(result)
    #100
    
    #def func():
    #    return i
    #v6 = [func for i in range(10)]
    #reslut = v6[5]()
    
    v7 = [lambda:i for i in range(10)]
    result = v7[5]()
    print(result)
    #9
    
    v8 = [lambda x:x*i for i in range(10)]
    #请问v8是什么？列表里头有函数
    #请问v8[0](2)的结果是什么？18
    
    #面试题
    def num():
        return [lambda x:i*x for i in range(4)]
    #num() --->[函数，函数，函数，函数]
    print(m(2) for m in num())
    #[6,6,6,6]
    
    v9 = [i for i in range(10) if i>5]
    print(v9)
    #[6, 7, 8, 9]
    ```

- 集合推导式

  ```python
  v1 = {i for i in 'alex'}
  print(v1)
  #{'a', 'x', 'l', 'e'}
  ```

- 字典的推导式

  ```python
  v1 = {'k'+str(i):i for i in range(10)}
  print(v1)
  #{k0': 0, 'k1': 1, 'k2': 2, 'k3': 3, 'k4': 4, 'k5': 5, 'k6': 6, 'k7': 7, 'k8': 8, 'k9': 9}
  ```

- 生成器推导式

  ```python
  def func():
      for i in range(10):
          yield i
  v = func()
  v = (i for i in range(10))
  
  def func():
      for i in range(10):
          def f():
              return i
          yield f
  v1 = func()
  for item in v1:
      print(item())
  ```



#### 5.18   异常处理

```python
#异常处理的格式
try:
    pass
except Exception as e:
    pass


import requests
try:
    rel = requests.get('http://www.google.com')
    print(rel.text)
except Exception as e:
    print('异常操作')
#################################################################################
def fun1(url_list):
    result = []
    try:#循环放入异常判断里面，一旦出现异常将终止循环，列表后面元素无法执行循环
        for url in url_list:
            response = requests.get(url)
            result.append(response.text)
    except Exception as e:
        pass
    return result
func1(['http://www.baidu.com','http://www.google.com','http://www.bing.com']) 
    
def fun2(url_list):
    result = []
    for url in url_list:
        try:
            response = requests.get(url)
            result.append(response.text)
        except Exception as e:
            pass
    return result
func2(['http://www.baidu.com','http://www.google.com','http://www.bing.com'])
```

#### 5.19迭代器、可迭代对象、生成器

- 迭代器

  - 迭代器是对某种对象如list,str,tuple,dict,set类创建对象，可迭代对象中的元素进行逐一获取，具有'''_ _next_ _'''方法且每次调用都获取可迭代对象中的元素（从前到后一个一个获取）

  - 迭代器可以记住遍历的位置的对象，迭代器对象从集合第一个元素开始访问，直到所有的元素被访问结束，迭代器只能往前不能往后。

    ```python
    #列表转换成迭代器：
    v1 = iter([11,22,33,44])
    v1 = [11,22,33,44].__iter__()
    #迭代器想要回去每个值：反复调用val = v1.__next__()即可
    v2 = iter(v1) 
    result1 = v2.__next__() 
    print(result1)
    result2 = v2.__next__() 
    print(result2) 
    result3 = v2.__next__() 
    print(result3) 
    result4 = v2.__next__() 
    print(result4) 
    result5 = v2.__next__() 
    print(result5) 
    #到result5会报错，错误类型StioIteration错误，表示迭代完成。咱们可进行一下处理，不报错
    v1 = 'alex'
    v2 = iter(v1)
    while True:
        try:
            val = v2.__next__()
            print(val)
        except Exception as e:
            break
    #迭代完成结束循环。
    
    ```

  - for 循环迭代器应用

    ```python
    v1 = [11,22,33,44]
    #内部会将v1转换成迭代器
    #内部反复执行 迭代器：__next__()
    #取完值不会报错
    for item in v1:
        print(item)
    ```

- 可迭代对象

  - 表象是可以被for循环对象就可以称为是可迭代对象。

  - 内部具有_ _iter_ _() 方法且返回一个迭代器,可以被for循环。

    ```python
    v1 = [11,22,33,44]
    result = v1.__iter__()
    ```

- 如何让一个对象变成可迭代对象？在类中实现`__iter__`方法且返回一个迭代器（或生成器）

  ```python
  class Foo:
    def __iter__(self):
          return iter([1,2,3,4])
  obj = Foo()
  class Foo:
      def __iter__(self):
          yield 1
          yield 2
          yield 3
          
  obj = Foo()
  ```

  - 只要能被for循环就需要去看它内部的iter方法。

- 生成器

  - 函数中如果存在yield，那么该函数就是一个生成器函数，调用生成器函数会返回一个生成器，生成器只有被 for循环时，生成器函数内部的代码才会执行，每次循环都会获取yield返回的值。
  - 在调用生成器运行的过程中，每次遇到yield时函数会暂停并保存当前所有的运行信息，返回yield的值，并在下一次执行next()方法时从当前位置继续进行。

  ```python
  #生成器函数(内部是否包含yield)
  def func():
      print('F1')
      yield 1
      print('F2')
      yield 2
      print('F3')
      yield 100
      print('F4')
  #函数内部代码不会执行，返回一个  生成器对象
  v1 = func()
  #生成器是可以被for循环，一旦开始循环那么函数内部代码就会开始执行
  for item in v1:
      print(item)
  ```

  ```python
  #生成器打印1~99数字
  def func():
      count = 1
      while True:
          yield count
          count += 1
          if count == 100:
              return
  
  val = func()
  for item in val:
      print(item)
  ```

  ```python
  #读取文件
  def func():
      """
      分批去读取文件中的内容，将文件的内容返回给调用者
      """
      cursor = 0
      while True:
          f = open('db','r',encoding='utf-8')
          f.seek(cursor)
          date_list = []
          for i in range(10):#将文件内容读取10条
              line = f.readline()
              if not line:
                  return
              data_list.append(line)
          cursor = f.tell()#记录当前光标位置，以供下次再读
          f.close()
          
      for row in data_list:#将读取数据yield
          yield row
  for item in func():#打印获取的数据
      print(item)
  ```

- 生成器send方法

  ```python
  def func():
      print('abc')
      a = yield c
      print('666',a)
      yield b
  g = func()
  g.send('peiqi')
  def func():
      print('abc')
      a = yield 'c'
      print('666',a)
      yield 'b'
  g = func()
  g.__next__()
  g.send('peiqi')
  '''
  abc
  666 peiqi
  '''
  ```

  

- 总结：
  - 迭代器：对可迭代对象中的元素进行逐一获取，迭代器对象的内部都会有一个next方法，用于一个个获取数据。
  - 可迭代对象，可以被for循环且此类对象中有iter方法且要返回一个迭代器（生成器）。
  - 生成器，函数内部有yield则就是生成器函数，调用函数则返回一个生成器，循环生成器时，则函数内部代码才会执行。

## 第六章 模块

### 6.0 模块的基本知识：

- 注意的是：文件和文件夹的命名不能与导入的模块名称相同，否则就会直接在当前目录中查找。

- 内置模块

  - 内置模块，python内部提供的功能，内置模块是Python自带的功能，在使用内置模块相应的功能时，需要【先导入】再【使用】（sys,os,time,random,json...）。

    ```python
    import sys
    print(sys.argv)
    #在pycharm下运行，打印出当前文件路经，是一个列表。
    
    ```

- 第三方模块

  - 需要下载、安装、使用（pip包管理安装）

    ```python
    把pip.exe所在的目录添加到环境变量中，C:/python36/Scripts/ 
    然后在终端输入：pip install 模块名称 
    #eg:pip install xlrd
    
    ```

  - 源码安装

    - 下载源码包

      ```
      压缩文件至"C:\Python36\Lib\site-packages"(此路径为python3放模块地方),cmd终端cd "C:\Python36\Lib\site-packages"。
      执行python3 setup.py build
      执行python3 setup.py install
      ```

      

  - 常见安装问题：

    ![1555480605189](C:\Users\Xu jk\AppData\Roaming\Typora\typora-user-images\1555480605189.png)

           上图网速慢造成。


​            
​    
​    1.Fatal error in launcher:Unable to create process using....
​    
​    由于更改了python文件名称，需要重新装pip
​    
​    2.you are using pip version 18.1.howerver version 19.0.3 is available
​    
​    pip版本过低
​    
​    以上问题可执行 python36 -m pip install --upgrade pip用于更新或重新安装pip,安装完成重新启动软件即可。

- 自定义模块

  ```python
  #jkmodule.py文件
  
  def fn():
      print('调用JK模块')
  ```

  ```python
  #test.py
  
  import jkmodule
  
  jimodule.fn()
  #执行
  '调用JK模块'
  ```

### 6.0 模块的调用

- 定义模块时可以把一个py文件或一个文件夹（包）当作一个模块，以方便以后其他py文件使用。

- 导入模块形式

  ```python
  import 模块         #调用方式：模块.函数()
  from 模块 import 函数 【或者*】        #调用方式：函数()
  from 模块 import 函数 as 别名         #调用方式：别名()
  import jd # 第一次加载：会加载一遍jd中所有的内容。
  import jd # 由已经加载过，就不在加载。
  print(456)
  
  ```

  ```python
  #多次导入重新加载
  import importlib
  import jd
  importlib.reload(jd)  #重新加载某模块方法
  print(456)
  ```

  

- python2&python3区别:

  python2 :文件中必须有 _ _ init _ _.py 文件

  python3：不需要_ _ init _ _.py 文件

  #推荐以后写代码，加上此文件。

  ```python
  #示例：
  import sys
  #此方式，先将sys文件从上到下执行一遍，并加载到内存中
  
  from math import pi
  #此方法导入模块math的pi方法。只将pi方法加载到内存中
  
  from random import *
  #导入模块所有方法
  
  from xx import xx as f
  #调用方法直接f()
  ```

  ```python
  #对于包内模块调用：
  bin(文件夹)
  	- jd.py
      - pdd.py
      - td.py
  #如何导入和调用
  import bin.jd
  bin.jd.f1()
  
  from bin import jd
  jd.f1()
  
  from bin.jd import f1
  f1()
  ```

![1555574257589](C:\Users\Xu jk\AppData\Roaming\Typora\typora-user-images\1555574257589.png)

### 6.1  random模块

- 模块导入：import random

6.1.1random.random()

- 生成大于0小于1之间的小数。

  ```python
  s = random.random()
  print(s)
  #0.9566568136938641
  ```

6.1.2random.uniform(1，3)

- 生成1~3之间的小数

  ```python
  s = random.uniform(1,3)
  print(s)
  #1.772605268739017
  ```

6.1.3random.randint(1，5)

- 生成1~5之间随机整数

  ```python
  s = random.randint(1,5)
  print(s)
  #2
  ```

6.1.4random.randrange(1,10,2)

- 生成大于1小于10之间的奇数

  ```python
  s = random.randrange(1,10,2)
  print(s)
  #9
  ```

  

6.1.5random.choice([1,'23',[4,5]])

- 随机返回一个值

  ```python
  s = random.choice([1,'23',[4,5]])
  print(s)
  #'23'
  ```

6.1.6random.sample([1,'23',[4,5]],2)

- 随机选择多个返回。

  ```python
  s = random.sample([1,'23',[4,5]],2)
  print(s)
  #['23', [4, 5]]
  ```

6.1.7random.shuffle(item)

- 打乱顺序

  ```python
  l=[1,2,3,4,5,6,7]
  random.shuffle(l)
  print(l)
  #[1, 3, 7, 4, 6, 2, 5]
  ```

```python
#练习题
#随机生成8位的验证码：可以是大写字母，小写字母，数值
a = [x for x in range(97,123)]
A = [x for x in range(65,91)]
num = [x for x in range(48,58)]
total = a + A + num
def get_random_code(length=8):
    code = ''
    for i in range(length):
        # if i%2 == 0:
        #     one_code = random.randint(65,90)
        #     code += chr(one_code)
        # else:
        #     one_code = random.randint(48,57)
        #     code += chr(one_code)
        one_code = chr(random.choice(total))
        code += one_code
    return code

codes = get_random_code()
print(codes)
#lZdTdrfM
```

### 6.2 hashlib模块

```python
#将字符串加密
import hashlib
def get_md5(data):
    obj = hashlib.md5()
    obj.update(data.encode('utf-8'))
    result = obj.hexdigest()
    return result
val = get_md5('123')
print(val)

#加盐
import hashlib
def get_md5(data):
    obj = hashlib.md5("asdagfgdfgd".encode('utf-8'))
    obj.update(data.encode('utf-8'))
    result = obj.hexdigest()
    return result
val = get_md5('123')
print(val)
```

- 校验文件一致性

```python
# md5 = hashlib.md5()
# md5.update('hello'.encode())
# md5.update(',wusir'.encode())
# 46879507bf541e351209c0cd56ac160e
# 46879507bf541e351209c0cd56ac160e
# print(md5.hexdigest())

# 两个文件
    # md5 = hashlib.md5()
    # md5.update(文件1 的内容)
    # md5.hexdigest()

    # md5 = hashlib.md5()
    # md5.update(文件2 的内容)
    # md5.hexdigest()
```

```python
#例子2  图片或文件的一致性
import hashlib
def get_md5(data):
    obj = hashlib.md5("asdagfgdfgd".encode('utf-8'))
    obj.update(data.encode('utf-8'))
    result = obj.hexdigest()
    return result
with open(r'E:\思维导图\图片.png', 'rb')as f1:
    md5 = hashlib.md5()
    while True:
        a = f1.read(10000)
        md5.update(a)
        if not a:
            break
    s = md5.hexdigest()
with open(r'E:\思维导图\图片1.png', 'rb')as f1:
    md5 = hashlib.md5()
    while True:
        a = f1.read(10000)
        md5.update(a)
        if not a:
            break
    b = md5.hexdigest()
if s == b:
    print('相同')
```



### 6.3 getpass模块

```python
import getpass
pwd = getpass.getpass('请输入密码：')
if pwd == '123':
    print('输入正确')
```

### 6.4  sys模块

- sys.getrefcount

  获取一个值的应用计数

  ```python
  a = [1,2,3]
  b = a
  print(sys.getrefcount(a))
  #2
  #b调用一次，sys调用一次
  ```

- sys.getrecursionlimit

  默认支持的递归数量

  ```python
  print(sys.getrecursionlimit())
  #1000
  #默认1000层
  
  ```

- sys.stdout.write

  与print()相似，都是输出，但print输出后的内容末尾有\n

  ```python
  sys.stdout.write('你好')
  sys.stdout.write('你好')
  #你好你好
  ```

  - 转义字符 \r

    回车(CR) ，将当前位置移到本行开头

    ```python
    # D:\code\day14_lianxi.py
    import time
    for i in range(1,101):
        msg = "%s%%\r"%i
        print(msg,end = '')
        time.sleep(0.05)
    #在终端输入 python3  D:\code\day14_lianxi.py
    #可执行程序
    ```

- sys.argv

  获取用户执行脚本时，传入的参数

  ```python
  #test.py
  
  import sys
  a = sys.argv
  print(a)
  
  #在终端输入：python3 test.py a b c d
  #['test.py', 'a', 'b', 'c', 'd']
  ```

- sys.path

  默认python去导入模块时，会按照sys.path中的路经挨个查找。可以在python 环境下使用sys.path.append(path)添加相关的路径

  ```python
  import sys
  
  print(sys.path)
  #打印模块默认存储地址
  sys.path.append('D:\code\db')
  #将'D:\code\db'加入到默认模块存储模块地址内。
  
  
  #['D:\\code', 'D:\\code', 'C:\\', 'C:\\Python36\\python36.zip', 'C:\\Python36\\DLLs', 'C:\\Python36\\lib', 'C:\\Python36', 'C:\\Python36\\lib\\site-packages', 'D:\\pycharm\\PyCharm 2018.3.5\\helpers\\pycharm_matplotlib_backend']
  #第一个最后一个路经是pycharm内调整的，所以不是。
  ```

  

- sys.exit()

  ```python
  sys.exit(0) #正常退出
  sys.exit(1) #异常退出
  ```

  



### 6.5  OS模块

- os.stat(文件路经).st_size

  读取文件大小,读的是字节

  ```python
  #终端上运行，可以出现读取进度。
  file_obj = os.stat(r'D:\code\xuxi.mp4').st_size
  # print(file_obj)
  
  read_size = 0
  
  with open(r'D:\code\xuxi.mp4','rb') as f1,open('av.mp4','wb') as f2:
      while read_size < file_obj:
          reads = f1.read(1024)
          f2.write(reads)
          read_size += len(reads)
          val = int(read_size/file_obj *100)
          print('%s%%\r'%val,end='')
   
  ```

- os.path.exists(path)

  如果path存在，返回True；如果path不存在，返回False 

  ```python
  print(os.path.exists(r'D:\code\av.mp4'))
  #True
  ```

- os.path.abspath() 

  获取一个文件的绝对路径

  ```python
  print(os.path.abspath('你好'))
  #D:\code\你好
  ```

- os.path.dirname()

  获取路经的上级目录

  ```python
  print(os.path.dirname(r'D:\code\a1.txt'))
  #D:\code
  
  ```

- os.path.join()

  路径拼接

  ```python
  path = 'D:\code\s21day14'
  v = 'n.txt'
  result = os.path.join(path,v)
  print(result)
  #'D:\code\s21day14\n.txt'
  ```

- os.listdir

  查看一个目录下所有文件[第一层]

  ```python
  print(os.listdir(r'D:\code'))
  #['.idea', 'a.log', 'a.txt', 'a1.txt',...]
  ```

- os.walk

  查看一个目标下所有的文件【所有层】

  ```python
  import os
  result = os.walk(r'D:\code\s21day14') 
  for a,b,c in result:    
      # a,正在查看的目录 b,此目录下的文件夹  c,此目录下所有层的文件    
      for item in c:        
          path = os.path.join(a,item)        
          print(path)
  ```

- os.makedirs

  创建目录和子目录

  ```python
  import os
  file_path = r'db\xx\xo\xxxxx.txt'
  paths = os.path.dirname(file_path)
  # print(paths)
  if not os.path.exists(paths):
      os.makedirs(paths)
  
  with open(file_path,'w',encoding='utf-8') as f:
      f.write('absd')
  ```

- os.rename

  重命名

  ```python
  import os
  os.rename('db','sb')
  ```

### 6.6  shutil模块

- 强制删除文件夹及其内的内容

  ```python
  #D:\code\day14_lianxi.py
  import sys
  import shutil
  a = sys.argv[1]
  # print(a)
  
  shutil.rmtree(a)
  
  #打开终端输入：python3 D:\code\day14_lianxi.py D:\code\text
  #可自动删除
  ```

- shutil.move

  ```python
  import shutil
  #重新命名
  shutil.move('lk','files')
  ```

- shutil.make_archive压缩文件

  ```python
  shutil.make_archive('dabao','zip','D:\code\lib')
  #将  'D:\code\lib'文件压缩，并生成dabao.zip,再当前目录中
  ```

- shutil.unpack_archive解压文件

  ```python
  shutil.unpack_archive('dabao.zip',extract_dir=r'D:\code\bin',format='zip')
  #简写
  shutil.unpack_archive('dabao.zip',r'D:\code\bin','zip')
  
  #将当前目录中的'dabao.zip'，解压到'D:\code\bin'文件中
  ```

  ```python
  import os
  import shutil
  from datetime import datetime
  ctime = datetime.now().strftime("%Y-%m-%d-%H-%M-%S") #秒转换成时间
  if not os.path.exists('code'):
      os.makerdirs('code')
  shutil.make_archive(os.path.join('code',ctime),'zip',r'D:\code\s21day16\lizhongwei')
  #将lizhongwei文件压缩，并生成code+时间，在当前目录
  
  file_path = os.path.join('code',ctime) + '.zip' shutil.unpack_archive(file_path,r'D:\x1','zip')
  #将code+时间+zip解压到D盘x1文件中
  ```

  

### 6.7  json  模块*****

- json是一个特殊的字符串，它格式类型可以是：列表，字典，字符串，数字，布尔值，元组（格式会转变成列表）

- json优点：所有语言通用，缺点：只能序列化基本的数据类型：列表，字典，字符串，数字，布尔值。

- json.dumps

  序列化将python的值转换为json格式的字符串

  ```python
  v = [12,3,4,{'k1':'v1'},True,'asd']
  import json
  #序列化，将python的值转换为json格式的字符串
  v1 = json.dumps(v)
  print(v1)
  #[12, 3, 4, {"k1": "v1"}, true, "asd"]
  
  
  import json
  v4 = ('alex',1,True)
  v5 = json.dumps(v4)
  print(v5)
  #["alex", 1, true]
  #json没有元组，如果有元组自动转换列表。
  #json不支持集合。
  ```

  

- json.loads

  反序列化，将json格式的字符串转换成python的数据类型

  ```python
  v1 = '["alex",123]'
  #反序列化，将json格式的字符串转换为python
  v2 = json.loads(v1)
  print(v3)
  #['alex', 123]
  
  v3 = "{'a':1}"
  v4 = json.loads(v3)
  print(v4)
  #报错,原因是a必须为双引号。
  ```

- json.dump

  ```python
  import json
  t = {1:'徐俊凯',2:'alex'}
  
  f = open('a.txt','w',encoding='utf-8')
  
  json.dump(t,f,ensure_ascii=False)
  f.close()
  #将字典t序列化写入a.txt文件内，若ensure_ascii=False,写入汉字正常显示，若ensure_ascii=True(默认)，写入汉字为编码后的：\u5f90\u4fca\u51ef
  ```

- json.load

  ```python
  import json
  f = open('a.txt','r',encoding='utf-8')
  data = json.load(f)
  f.close()
  print(data)
  #{1:'徐俊凯',2:'alex'}
  ```

### 6.8 pickle模块

- pickle 优点：python中所有的东西几乎都能被它序列化（socket对象不可以），缺点：序列化的内容只有python能使用

```python
import pickle
################dumps/loads###############
import pickle
v = {1,2,3,4}
c = pickle.dumps(v)
print(c,type(c))
#b'\x80\x03cbuiltins\nset\nq\x00]q\x01(K\x01K\x02K\x03K\x04e\x85q\x02Rq\x03.' <class 'bytes'>

#对函数序列化
def f():
    print('f')

v1 = pickle.dumps(f)
print(v1)
#b'\x80\x03c__main__\nf\nq\x00.'
v2 = pickle.loads(v1)
print(v2)
#<function f at 0x000001775964CA60>

################dump/load################
#dump序列化
import pickle
v = {1,2,3,4}
f = open('x.txt','wb')

pickle.dump(v,f)
f.close()

#反序列化
import pickle
f = open('x.txt','rb')
val = pickle.load(f)
f.close()
print(val)
```

```
#pickle.dump(obj, file[, protocol])——序列化，将对象保存到文件中。参数protocol是序列化模式，默认值#0，表示以文本的形式序列化。protocol的值还可以是1或2，表示以二进制的形式序列化。
#pickle.load(file)——反序列化，从文件中读取一个对象
#clear_memo()——清空pickler的“备忘”。使用Pickler实例在序列化时，会自动读取下一个对象，而不会重复读取# #同一个对象
import pickle
t = []
for i in range(2):
    inp = input()
    t.append(inp)
with open('./the_first_pickle.pickle','w') as p:
    pickle.dump(t,p)   #将列表t保存起来
with open('./the_first_pickle.pickle','r') as r:
    a = pickle.load(r)  #将列表读取 
print t
print type(t)

print a
print type(a)

--------------------- 
作者：Qistar_Eva 
来源：CSDN 
原文：https://blog.csdn.net/Qistar_Eva/article/details/79012596 
版权声明：本文为博主原创文章，转载请附上博文链接！
```



### 6.9 time模块*

UTC/GMT 世界时间

- time.time()

  ```python
  time.time()#时间戳 从1970-1-1 0：0：0开始
  ```

- time.sleep(10）

  ```python
  等待10数
  ```

- time.timezone

  ```python
  time.timezone
  #-28800
  
  #时区差距秒数（与电脑设置有关系）
  ```

### 6.10 datetime模块*

datetime  ---->类型是datetime.datetime 

time 与 datetime 与 字符串之间的转换

```python
from datetime import datetime,timezone,timedelta
######################## 获取datetime格式时间 ##############################
#获取当前本地时间
print(datetime.now())  #2019-04-18 17:13:05.971097

#获取当前UTC时间
v3 = datetime.utcnow()
print(v3)#2019-04-18 09:18:01.903667

# 获取其他时区时间，如当前东七区时间
tm = timezone(timedelta(hours=7))
v2 = datetime.now(tm)
print(v2)#2019-04-18 16:16:36.949862+07:00

######################## 把datetime格式转换成字符串 ##############################
v1 = datetime.now()

val = v1.strftime("%Y-%m-%d %H:%M:%S")
print(val)
# 2019-04-18 17:20:42

path = datetime.now()
val = path.strftime("%Y{}%m{}%d{}").format('年','月','日')
print(val)
#2019年04月19日
# ######################## 字符串转成datetime ##############################
strs = "2018-11-15 12:22:22"
val = datetime.strptime(strs,'%Y-%m-%d %H:%M:%S')
print(val)
 

################## datetime时间的加减 #############################
#字符串转换成datetime
v1 = datetime.strptime('2011-11-11','%Y-%m-%d') 
#datetime加减运算
v2 = v1 - timedelta(days=140) 
#再生成字符串
date = v2.strftime('%Y-%m-%d') 
print(date)
#2018-11-19 17:40:25
###################### 时间戳转换datetime关系 ##############################
ctime = time.time()  
v1 = datetime.fromtimestamp(ctime) 
print(v1)
#2019-04-18 17:36:55.457132
######################## datetime转换时间戳 ##############################
v1 = datetime.now() 
val = v1.timestamp() 
print(val)
#1555580123.215816
```

### 6.11importlib

- importlib模块支持传入字符串来引入一个模块。

```python
#原有方法
from utils.redis import Redis
obj = Redis()
obj.connect()

#提升方法
import importlib

middleware_classes = [
    'utils.redis.Redis',
    # 'utils.mysql.MySQL',
    'utils.mongo.Mongo'
]
for path in middleware_classes:
    module_path,class_name = path.rsplit('.',maxsplit=1)
    module_object = importlib.import_module(module_path)# from utils import redis
    cls = getattr(module_object,class_name)
    obj = cls()
    obj.connect()


# # 用字符串的形式导入模块。
# redis = importlib.import_module('utils.redis')
#
# # 用字符串的形式去对象（模块）找到他的成员。
```

```python
src_____
    |___pinduoduo.py_____class Foo____fn
    
import importlib
temp = 'src.pinduoduo.Foo'
module_object,cls = temp.rsplit('.',1)

files = importlib.import_module(module_object)
arrt = getattr(files,cls)
obj = arrt()
obj.fn()
#'这是拼多多'
```

### 6.12 logging模块

- 日志处理本质：Logger/FileHandler/Formatter

  ```python
  # logging模块
      # 记录日志的
      # 用户 ：
      # 程序员 ：
          # 统计用的
          # 用来做故障排除的 debug
          # 用来记录错误，完成代码的优化的
      # logging.basicconfig
          # 使用方便
          # 不能实现 编码问题；不能同时向文件和屏幕上输出
          # logging.debug,logging.warning
      # logger对象
          # 复杂
              # 创建一个logger对象
              # 创建一个文件操作符
              # 创建一个屏幕操作符
              # 创建一个格式
  
              # 给logger对象绑定 文件操作符
              # 给logger对象绑定 屏幕操作符
              # 给文件操作符 设定格式
              # 给屏幕操作符 设定格式
  
              # 用logger对象来操作
  ```

  ```python
  import logging
  
  logger = logging.getLogger()
  fh = logging.FileHandler('log.log')
  sh = logging.StreamHandler()
  logger.addHandler(fh)
  logger.addHandler(sh)
  formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
  fh.setFormatter(formatter)
  sh.setFormatter(formatter)
  logger.warning('message')
  ```

  

- 推荐处理日志方式

```python
import logging

file_handler = logging.FileHandler(filename='x1.log', mode='a', encoding='utf-8',)
logging.basicConfig(
    format='%(asctime)s - %(name)s - %(levelname)s -%(module)s:  %(message)s',  #日志输出格式
    datefmt='%Y-%m-%d %H:%M:%S %p',                                             #日志时间
    handlers=[file_handler,],												#日志文件
    level=logging.ERROR														#错误的截取级别
)

logging.error('你好')
```

```python
#推荐处理日志方式 + 日志分割
import time
import logging
from logging import handlers
# file_handler = logging.FileHandler(filename='x1.log', mode='a', encoding='utf-8',)
file_handler = handlers.TimedRotatingFileHandler(filename='x3.log', when='s', interval=5, encoding='utf-8')
logging.basicConfig(
    format='%(asctime)s - %(name)s - %(levelname)s -%(module)s:  %(message)s',
    datefmt='%Y-%m-%d %H:%M:%S %p',
    handlers=[file_handler,],
    level=logging.ERROR
)

for i in range(1,100000):
    time.sleep(1)
    logging.error(str(i))
```

- 保留堆栈信息

  ```python
  # 在应用日志时，如果想要保留异常的堆栈信息。
  import logging
  import requests
  
  logging.basicConfig(
      filename='wf.log',
      format='%(asctime)s - %(name)s - %(levelname)s -%(module)s:  %(message)s',
      datefmt='%Y-%m-%d %H:%M:%S %p',
      level=logging.ERROR
  )
  
  try:
      requests.get('http://www.xxx.com')
  except Exception as e:
      msg = str(e) # 调用e.__str__方法
      logging.error(msg,exc_info=True)  # exc_info 参数保留堆栈信息
  ```

### 6.13  Collection 模块

- 坐标元祖，不可变

```python
from collections import namedtuple         # 坐标元祖 例子
point = namedtuple ('point',['x','y','z'])
p  = point (1,2,3)
p1 = point(5,6,7)
print(p.y)
```

- 双端队列

```python

from _collections import deque     ##双端队列
dq = deque ()        ###创建一个双端队列
dq.append(1)         ###在末尾加入一个元素
dq .append(3)
dq.appendleft(66)        ###在左侧加入一个元素
print(dq.pop())    ###从末尾读取一个元素
print(dq.popleft() )   ##从左侧读取一个元素
print(dq)
```

- 有序字典

```python
###有序字典
from _collections import OrderedDict
od = OrderedDict ([('a',1),('b',6),('c',96),('d',55)])
print(od)     ##########字典的key是有序的
for key in od:
    print(key)      ####打印每一个key
```

### 6.14  re 模块

###### 1.正则表达式

```python
#1. 元字符
#\d    0-9数字   \D   非数字
#\w   字母数字下划线   \W    非数字字母下划线
#\s   空字符           \S    非空字符
#\n    换行            \t    制表符
#[]  字符组            [^]   非字符组，里面有啥匹配的东西不能要啥
#^  以什么开头          $ 以什么结尾
# .除了换行符都能匹配
#| 什么或什么 
#（） 限定|的作用域
      # 限定一组正则的量词约束
```

```python
#量词   正则表达式破刃匹配贪婪模式 
#? 0次或次
#+ 1次或多次
#* 0次或多次
#{5} 出现5次
#{3,5}出现3次-5次都可以
#{5，}出现5次或多次
#量词后面加？表示非贪婪模式匹配

```

```python
#关于分组
ret = re.search('<(\w+)>(.*?)</\w+>',s1)
print(ret)
print(ret.group(0))   # group参数默认为0 表示取整个正则匹配的结果
print(ret.group(1))   # 取第一个分组中的内容
print(ret.group(2))   # 取第二个分组中的内容
```

```python
关于分组的命名
ret = re.search('<(?P<tag>\w+)>(?P<cont>.*?)</\w+>',s1)
print(ret)
print(ret.group('tag'))   # 取tag分组中的内容
print(ret.group('cont'))   # 取cont分组中的内容

# 分组命名
# (?P<名字>正则表达式)
# 引用分组  (?P=组名)   这个组中的内容必须完全和之前已经存在的组匹配到的内容一模一样
```

```python
关于分组
ret = re.findall('\d(\d)','aa1alex83')
# # findall遇到正则表达式中的分组，会优先显示分组中的内容
# print(ret)

# ret = re.findall('\d+(?:\.\d+)?','1.234+2')
# print(ret)

# 分组
# split
    # 会保留分组中本来应该被切割掉的内容
# 分组命名
# (?P<名字>正则)  search group('组名')
# 引用分组
# (?P=组命) 表示这个组中的内容必须和之前已经存在的一组匹配到的内容完全一致
# 分组和findall
    # 默认findall 优先显示分组内的内容
    # 取消分组优先显示 (?:正则)

```





###### 2.re模块

```python
#ret = re.compile(正则)
#将正则处理成一个对象，这个对象可以调用re模块的所有方法
import re
ret = re.compile('\d')
print(type(ret))
print(ret)
'''
<class '_sre.SRE_Pattern'>
re.compile('\\d')
'''
a = ret.findall('字符串')

```



```python
#a = re.findall(正则，'待匹配字符串') 返回一个列表
#a = re.search(正则，'待匹配字符串')  只匹配找到的第一个符合的值
	#取到值返回一个对象   没取到返回None  a.group方法调用这个值
#a = finditer(正则，'待匹配字符串')  返回一个迭代器，且迭代出来的每一个都是对象
	#通过对象调用group方法得到值
```

```python
#re.split(正则，'字符串')   以符合正则的字符串部分为分割点分给字符串返回一个列表
#re.sub  
# ret = re.sub('\d','D','alex83wusir74taibai',1)
# print(ret)
#将'D'替换里面的数字。替换次数为1次
#返回里面替换后得到的字符串
#re.subn
# ret = re.subn('\d','D','alex83wusir74taibai')
# print(ret)
#返回一个元祖 元祖中为替换后的字符串 和替换的次数

```



```python
# re.match
# ret = re.match('\d','alex83') == re.match('^\d','alex83')
# print(ret)
# 会从头匹配字符串中取出从第一个字符开始是否符合规则
# 如果符合，就返回对象，用group取值
# 如果不符合，就返回None
# match = search + ^正则
```

```python
关于转义符
# print('\\\\n')
# print('\\n')

# print(r'\\n')
# print(r'\n')

# 正则表达式中的转义符在python的字符串中也刚好有转移的作用
# 但是正则表达式中的转义符和字符串中的转义符并没关系
# 且还容易有冲突
# 为了避免这种冲突
# 我们所有的正则都以在工具中的测试结果为结果
# 然后只需要在正则和待匹配的字符串外面都加r即可
```

```python
#简单爬虫示例
import requests
import json
import re

def parser_page(par,content):
    res = par.finditer(content)
    for i in res:
        yield {'id': i.group('id'),
               'title': i.group('title'),
               'score': i.group('score'),
               'com_num': i.group('comment_num')}

def get_page(url):
    ret = requests.get(url)
    return  ret.text

def write_file(file_name):
    with open(file_name,mode = 'w',encoding='utf-8') as f:
        while True:
            dic = yield
            f.write('%s\n' % json.dumps(dic, ensure_ascii=False))

pattern = '<div class="item">.*?<em class="">(?P<id>\d+)</em>.*?<span class="title">(?P<title>.*?)</span>.*?' \
              '<span class="rating_num".*?>(?P<score>.*?)</span>.*?<span>(?P<comment_num>.*?)人评价</span>'
par = re.compile(pattern,flags=re.S)
num = 0
f = write_file('move2')
next(f)
for i in range(10):
    content = get_page('https://movie.douban.com/top250?start=%s&filter=' % num)
    g = parser_page(par,content)
    for dic in g:
        f.send(dic)
    num += 25
f.close()
```

### 6.15 struct模块

```python
import struct
ret = struct.pack('i',100000)   #将1万编写成4字节2进制的东西
print(ret)
con = struct.unpack('i',ret)	#将他解码成数字
print(con)
```

### 6.16 socket模块

```python
import socket
sk = socket.socket()
#详细见网编-网络基础
```

### 6.17 socketserver模块

```python
import socketserver

class Myserver(socketserver.BaseRequestHandler):
    def handle(self):  # 自动触发了handle方法，并且self.request == conn
        msg = self.request.recv(1024).decode('utf-8')
        self.request.send('1'.encode('utf-8'))
        msg = self.request.recv(1024).decode('utf-8')
        self.request.send('2'.encode('utf-8'))
        msg = self.request.recv(1024).decode('utf-8')
        self.request.send('3'.encode('utf-8'))

server = socketserver.ThreadingTCPServer(('127.0.0.1',9000),Myserver)
server.serve_forever()
```

```python
import socket
import time
sk = socket.socket()
sk.connect(('127.0.0.1',9000))
for i in range(3):
    sk.send(b'hello,alex')
    msg = sk.recv(1024)
    print(msg)
    time.sleep(1)
sk.close()
```



### 9.0包的定义详解

```python
什么是模块
      py文件 写好了的 对程序员直接提供某方面功能的文件
     import
      from import 名字

  什么是包
      文件夹 存储了多个py文件的文件夹
       如果导入的是一个包，这个包里的模块默认是不能用的
       导入一个包相当于执行 __init__.py文件中的内容
```



## 第七章 面向对象

- 什么时候用面向对象？
  - 函数（业务功能）比较多，可以使用面向对象来进行归类。
  - 想要做数据封装（创建字典存储数据时，面向对象）。

### 7.1面向对象的基本格式：

```python
###### 定义类 ######
class 类名：
	def 方法名(self,name):
        print(name)
        return 123
    def 方法名(self,name):
        print(name)
        return 123
    def 方法名(self,name):
        print(name)
        return 123
####### 调用类中的方法 #####
#1.创建该类的对象
obj = 类名()
#2.通过对象调用方法
result = obj.方法名('alex')
print(result)
```

- 对象的作用：存储一些值，以后方便自己使用。

```python
class File:
    def read(self):
        with open(self.xxxxx,mode='r',encoding='utf-8') as f:
            data = f.read()
        return data
    def write(self,content):
        with open(self.xxxxx,mode='a',encoding='utf-8') as f:
            f.write(content)
 # 实例化一个File类的对象
obj1 = File()
#在对象中写了一个xxxxx='test.log'
obj1.xxxxx = 'test.log'
#通过对象调用类中的read方法，read方法中的self就是obj.
#obj1.read()
obj1.write('alex')

#实例化了一个File类的对象
obj2 = File()
#在对象中写了一个xxxxx = 'test.log'
obj2.xxxxx = 'info.txt'
#通过对象调用中read方法，read方法中的self就是obj.
obj2.write('alex')


class Person():
    def show(self):
        temp = "我是%s,年龄:%s,性别:%s" %(self.name,self.age,self.gender)
        print(temp)
p1 = Person()
p1.name = 'xdd'
p1.age = 19
p1.gender = '男'
p1.show()
#我是xdd,年龄:19,性别:男
```

- 初始化方法：给对象的内部做初始化，将数据封装到对象中，方便使用

```python
class Person:
    def __init__(self,n,a,g): #初始化方法（构造方法），给对象的 内部做初始化
        self.name = n
        self.age = a
        self.gender = g
    def show(self):
        temp = "我是%s,年龄%s,性别：%s" %(self.name,self.age,self.gender)
        print(temp)
#类()  实例化对象，自动执行此类中的__init__方法。
p1 = Person('方世玉',19,'男')
p1.show()
#我是方世玉,年龄19,性别：男
p2 = Person('流川枫',20,'男')
p2.show()
#我是流川枫,年龄20,性别：男
```

### 7.2 面向对象三大特性

- 对面向对象的了解？先谈类的三大特性：封装，继承，多态。
- 类和对象是什么关系？对象是类的一个实例化
- self是什么？
  - self是形参，对象调用方法时，python内部会将对象传给这个函数。
- 类成员和对象成员，它们之间的关系。
  - 类成员：绑定方法、类方法、类变量、静态方法、属性
  - 对象成员：实例变量
- 类、方法、对象  都可以当做变量或嵌套到其他类型中

```python
class Foo:
    def run(self):
        pass
v = [Foo,Foo]#类嵌套在列表中
v = [Foo(),Foo()]#实例对象嵌套在列表
obj = Foo()
v = [obj.run,obj.run,obj.run]#类的方法嵌套在列表中
```

#### 7.2.1 封装

- 将相关功能封装到一个类中
- 将数据封装到一个对象中

```python
class A:
    def __init__(self,name):
        self.n = name
    def a(self):	#类内的a方法
        print('你好,%s'%self.n)
    def b(self):
        print(self.n+self.n)
obj = A('小明')	#创建一个实例化对象obj
print(obj.n)
#小明
obj.a()
#你好，小明
```

```python
#1 . 循环让用户输入：用户名、密码、邮箱。输入完成后再进行数据打印
USER_LIST = []
class Person:
    def __init__(self,name,pwd,email):
        self.name = name
        self.pwd = pwd
        self.email = email

    def show(self):
        return '我是%s，密码是%s，邮箱是%s'%(self.name,self.pwd,self.email)
        

while True:
    name = input('请输入姓名：')
    if name.upper() == 'N':
        break
    pwd = input('请输入密码：')
    email = input('请输入邮箱：')
    p = Person(name,pwd,email)
    USER_LIST.append(p)

for item in USER_LIST:
    msg = item.show()
    print(msg)
```

- 小总结：如果写代码时，函数比较多比较乱，可以将函数归并放到同一个类中，函数如果一个反复使用的公共值，则可以放到对象中。

```python
class Police:
    def __init__(self,name):
        self.name = name
        self.hp = 1000

    def knife(self,other):
        msg = "%s给了%s一刀"%(self.name,other.nickname)
        self.hp = self.hp -10
        other.hp -= 50
        print(msg)

    def gun(self):
        msg = "%s去战斗"%self.name

    def fist(self,other):
        msg = "%s给了%s一拳"%(self.name,other.nickname)
        self.hp = self.hp -2
        other.hp -= 10
        print(msg)
class Bandit:
    def __init__(self,nickname):
        self.nickname = nickname
        self.hp = 1000

    def gun(self,other):
        msg = "%s给了%s一枪"%(self.nickname,other.name)
        print(msg)
        self.hp -= 20
        other.hp -= 500

p1 = Police('警察')
f1 = Bandit('匪徒')
p1.knife(f1)
print(p1.hp)
print(f1.hp)
f1.gun(p1)

print(p1.hp)
print(f1.hp)
#警察给了匪徒一刀
#990
#950
#匪徒给了警察一枪
#490
#930
```

#### 7.2 .2继承

- 支持多继承（先找左边，再找右边），Java,PHP,C#  没有多继承
- 使用继承可以提高代码重用性。

```python
#父类（基类）
class Base:
    def f1(self):
        pass
#子类（派生类）
class Foo(Base):
    def f2(self):
        pass
#创建一个子类的对象
obj = Foo()
#执行对象方法时，优先在自己的类中找，如果没有就是父类中找。
obj.f2()
obj.f1()
```

```python
#继承关系中的查找方法的顺序
#示例1：
class Base:
    def f1(self):
        print('base.f1')

class Foo(Base):
    def f2(self):
        self.f1()
        print('foo.f2')

obj = Foo()
obj.f2()
#base.f1
#foo.f2

#示例2
class Base:
    def f1(self):
        print('base.f1')

class Foo(Base):
    def f2(self):
        self.f1()
        print('foo.f2')
    def f1(self):
        print('foo.f1')

obj = Foo()
obj.f2()
#foo.f1
#foo.f2

#示例3
class Base:
    def f1(self):
        self.f2()
        print('base.f1')
    def f2(self):
        print('base.f1')
class Foo(Base):
    def f2(self):
        print('foo.f2')
obj = Foo()
obj.f1()
#foo.f2
#base.f1

class BaseServer：
	def serve_forever(self, poll_interval=0.5):        
        self._handle_request_noblock()    
    def _handle_request_noblock(self):        
        self.process_request(request, client_address)           
    def process_request(self, request, client_address):        
        pass     
class TCPServer(BaseServer):    
    pass
class ThreadingMixIn:    
    def process_request(self, request, client_address):        
        pass     
class ThreadingTCPServer(ThreadingMixIn, TCPServer):     
    pass
obj = ThreadingTCPServer()
obj.server_forever()

#注意事项：
self到底是谁？
self是哪个类创建的，就从此类开始找，自己没有找父类、基类
```

#### 7.2.3多态（多种形态/多种类型）

```python
#Python默认支持多种形态
def func(arg):#arg传入参数类型不确定，具有索引功能均可以执行。
    v = arg[-1]
    print(v)
    
#由于pyhthon原生支持多态，所以没有特殊性
class F1:
    def f1(self):
        pass
class F2:
    def f1(self):
        pass
class F3:
    def f1(self):
        pass
    
def func(arg):
    arg.f1()
obj = F1()
func(obj)
```

- 鸭子模型

```python
#鸭子模型
对于一个函数而言，Python对于参数的类型不会限制，那么传入参数时就可以是各种类型，在函数中如果有例如arg.send方法，那么就是对于传入类型的一个限制（类型必须有send方法）
这就是鸭子模型，类似于上述的函数我们认为只要能呱呱叫的就是鸭子（只有有send方法，就是我们想要的类型）
```

### 7.3 成员

#### 7.3.1实例变量

```python
class Foo:
    def __init__(self,name):
        self.name = name

    def info(self):
        pass

obj1 = Foo('alex')  #实例化一个对象，传入的参数为实例变量
obj2 = Foo('eric')
```

#### 7.3.2类变量

```python
class Foo:
    city = '北京'

    def __init__(self,name):
        self.name = name

    def func(self):
        pass

obj1 = Foo('xdd')
obj2 = Foo('Ddd')
#调用方法：

print(Foo.city)
print(obj1.city)
print(obj2.city)
#调用可以是：对象.类变量名称  或  类.类变量名称    推荐后者
```

```python
#练习题：
class Base:
    x = 1

obj = Base()

print(obj.x)#1  先去对象中找，如果没有再去类中找
obj.y = 123    #在对象中添加一个y=123的变量
print(obj.y)#123
obj.x = 123
print(obj.x)#123
print(Base.x)#1


class Parent:
    x = 1
class Child1(Parent):
    pass
class Child2(Parent):
    pass
print(Parent.x,Child1.x,Child2.x)#1  1  1
Child1.x = 2
print(Parent.x,Child1.x,Child2.x)#1  2  1
Child2.x = 3
print(Parent.x,Child1.x,Child2.x)#1  2  3
```

- 总结：找变量优先找自己，自己没有 再找类或者基类，修改或赋值只能在自己的内部设置。

#### 7.3.3方法（普通方法/绑定方法）

- 定义：至少有一个self参数
- 执行：先创建对象，由对象.方法()  调用。

```python
class Foo:
    def func(self,a,b):
        print(a,b)
obj = Foo()
obj.func(1,2)
#### #### #### ####
class Foo:    
    def __init__(self):        
        self.name = 123
	def func(self, a, b): 
         print(self.name, a, b)
obj = Foo()
obj.func(1, 2)
```

#### 7.3.4 静态方法

- 定义：
  - @staticmethod装饰器
  - 参数无限制
- 执行
  - 类.静态方法名()     #推荐
  - 对象.静态方法名()

```python
class Foo(object):
    def __init__(self,name):
        self.name = name
    @staticmethod
    def show(*args,**kwargs):
        print('你好',args,kwargs)

        
Foo.show(1,2,3,4,k ='v1')
obj = Foo('hello')
obj.show(2)
```

#### 7.3.5类方法

- 定义
  - @classmethod装饰器
  - 至少有cls这个参数，此为当前类
- 执行方法：
  - 类.类方法（）					#推荐
  - 对象.类方法（）

```python
class Foo(object):
    def __init__(self,name):
        self.name = name
    @classmethod
    def show(cls,a):
        print(cls)
        print(a)

Foo.show('这个a往类方法传参数')
#<class '__main__.Foo'>
#这个a往类方法传参数

obj = Foo('alex')
obj.show('这个a也往类方法传参数')
#<class '__main__.Foo'>
#这个a也往类方法传参数
```

```python
#面试题
#问题：@classmethod和@staticmethod的区别？
1.一个是类方法，一个事静态方法
2.类方法：用@classmethod做装饰器，并且至少有一个cls参数
  静态方法：用@staticmethod做装饰器且参数无限制
3.调用方法：
	类.方法()	#推见
    对象.方法()
```

#### 7.3.6属性

- 定义：
  - @property装饰器
  - 只有一个self参数
- 执行：
  - 对象.方法   不用加括号

```python
class Foo:

    @property
    def func(self):
        print('你好！')
        return '123'

obj = Foo()
result = obj.func
print(result)
```

```python
#属性的应用场景
class Page:
    def __init__(self,total_count,currenct_page,per_page_count=10):
        self.total_count = total_count
        self.current_page = currenct_page
        self.per_page_count = per_page_count
    @property
    def start_index(self):
        return (self.current_page-1)* self.per_page_count
    @property
    def end_index(self):
        return self.current_page *self.per_page_count

USER_LIST = []

#分页的实现
current_page = int(input('请输入要查看的页码:'))
p = Page(500,current_page)
for i in range(p.total_count):
    USER_LIST.append('alex-%s'%(i,))
data_list = USER_LIST[p.start_index:p.end_index]
for item in data_list:
    print(item)
```

### 7.4成员修饰符

- 公有：所有地方都能访问
- 私有：只有自己可以访问

```python
#实例变量的私有
class Foo:
    def __init__(self,name):
        self.__name = name
        
    def func(self):
        print(self.__name)
obj = Foo('alex')
#print(obj.__name)		报错
obj.func()

#类变量私有的调用
class Foo:
    __x = 1

    @staticmethod
    def func():
        print(Foo.__x)
        return '类变量的私有调用'

# print(Foo.__x)	报错
print(Foo.func())

#方法的私有
class Foo:
    def __func(self):
        print('msg')

    def show(self):
        self.__func()

obj = Foo()
# obj.__func()	#报错
obj.show()
```

- 拓展：强制访问私有变量

```python
class Foo:
    def __init__(self,name):
        self.__x = name


obj = Foo('alex')
print(obj._Foo__x)
```

### 7.5类的嵌套

- 函数：参数可以任意类型
- 字典：对象和类都可以做字典的key和value
- 继承查找

```python
#示例1：
class Foo:
    def __init__(self,num):
        self.num = num
         
cls_list = []

for i in range(10):
    cls_list.append(Foo) #将类内存地址放入到列表中
    
for i in range(len(cls_list)):
    obj = cls_list[i](i)        #创建实例化
    print(obj.num)              #调用实例变量
    
#示例2：	
class Foo:
    def __init__(self,num):
        self.num = num

B = Foo
obj = B('alex')
#示例3

class Foo:
    def f1(self):
        print('f1')
        
    def f2(self):
        print('f2')

obj = Foo()
v = [obj.f1,boj.f2]		#将实例化方法未执行，放到列表中

for item in v:			#循环执行。
    item()
    
#示例4：
class Foo:    
    def f1(self):        
        print('f1')        
    def f2(self):        
        print('f2')           
    def f3(self):        
        v = [self.f1 , self.f2 ]        
        for item in v:            
            item()             
obj = Foo() 
obj.f3()		#与示例3相似，将f3执行f1,f2方法。

#示例5
class Account:
    def login(self):
        pass
    
    def register(self):
        pass
    
    def run(self):
        info = {'1':self.register,'2':self.login}
        choice = input('请选择')
        method = info.get(choice)
        method()
```

```python
class School(object):
    def __init__(self,title,addr):
        self.title = title
        self.address = addr

class ClassRoom(object):
    def __init__(self,name,school_object):
        self.name = name
        self.school = school_object
s1 = School('北京','沙河')
s2 = School('上海','浦东')
s3 = School('深圳','南山')

c1 = ClassRoom('全栈21期',s1)
print(c1.name)
print(c1.school.title)
print(c1.school.address)
```

```tex
#鸭子类型：动态语言风格，只关注方法和属性，不关注类型

关注的不是对象的类型本身，而是它是如何使用的。例如，在不使用鸭子类型的语言中，我们可以编写一个函数，它接受一个类型为鸭的对象，并调用它的走和叫方法。在使用鸭子类型的语言中，这样的一个函数可以接受一个任意类型的对象，并调用它的走和叫方法。如果这些需要被调用的方法不存在，那么将引发一个运行时错误。任何拥有这样的正确的走和叫方法的对象都可被函数接受的这种行为引出了以上表述，这种决定类型的方式因此得名。
```

```python
class StarkConfig(object):
    pass
class AdminSite(object):
    def __init__(self):
        self.data_list = []
        
    def register(self,arg):
        self.data_list.append(arg)
site = AdminSite()
obj = StarkConfig()
site.register(obj)
#通过类的嵌套将实例变量obj加入到data_list列表内。
```

```python
class StarkConfig(object):
    def __init__(self,name,age):
        self.name = name
        self.age = age 
class AdminSite(object):
    def __init__(self):
        self.data_list = []
        self.sk = None
	def sek_sk(self,arg):
        self.sk = arg
        
site = AdminSite()
site.set_sk(StarkConfig)
site.sk('alex',19)
#嵌套，将StarkConfig类加入sek_sk方法内，可以通过AdminSite实例对象调用
```

```python
class StackConfig(object):
    pass
class Foo(object):
    pass
class Base(object):
    pass
class AdminSite(object):
    def __init__(self):
        self._register = {}
    def registry(self,key,arg):
        self._register[key] = arg
site = AdminSite()
site.registry(1,StackConfig)
site.registry(2,StackConfig)
site.registry(3,StackConfig)
site.registry(4,Foo)
site.registry(5,Base)
for k,v in site._register.items():
    print(k,v())
#将类的内存地址加入到字典内，然后循环打印。
```

```python
class StackConfig(object):
    pass

class UserConfig(StackConfig):
    pass


class AdminSite(object):
    def __init__(self):
        self._register = {}

    def registry(self,key,arg=StackConfig):
        self._register[key] = arg

    def run(self):
        for key,value in self._register.items():
            obj = value()
            print(key,obj)
site = AdminSite()
site.registry(1)
site.registry(2,StackConfig)
site.registry(3,UserConfig)
site.run()
```

```python
class StackConfig(object):
    list_display = 'a'

class UserConfig(StackConfig):
    list_display = 'b'


class AdminSite(object):
    def __init__(self):
        self._register = {}

    def registry(self,key,arg=StackConfig):
        self._register[key] = arg

    def run(self):
        for key,value in self._register.items():
            obj = value()
            print(key,obj.list_display)
site = AdminSite()
site.registry(1)
site.registry(2,StackConfig)
site.registry(3,UserConfig)
site.run()
#1 a
#2 a
#3 b
```

```python
class StackConfig(object):
    list_display = 'a'
    
    def changelist_view(self):
        print(self.list_display)
        
class UserConfig(StackConfig):
    list_display = 'b'

class AdminSite(object):
    def __init__(self):
        self._register = {}

    def registry(self,key,arg=StackConfig):
        self._register[key] = arg

    def run(self):
        for key,value in self._register.items():
            obj = value()
            obj.changelist_view()
site = AdminSite()
site.registry(1)
site.registry(2,StackConfig)
site.registry(3,UserConfig)
site.run()
#a
#a
#b
```

### 7.6特殊成员

#### 7.6.1 `__init__`方法的初始化

```python
#方法的初始化
class Foo:
    """
    此处写类是干什么用的...
    """
    def __init__(self,a1):
        """
        初始化方法
        """
        self.a1 = a1
obj = Foo('alex')
```

#### 7.6.2 `__new__`构造方法

```python
#实例化对象，自动执行
class Foo(object):
    def __init__(self):
        """
        用于给对象中赋值，初始化方法
        """
        self.x = 123
    def __new__(cls,*args,**kwargs):
        """
        用于创建空对象，构造方法
        """
        return object.__new__(cls)
obj = Foo()
```

```python
#单例方法的实现
class Earth(object):
    __instance = None
    def __new__(cls):
        if cls.__instance == None:#如果类属性为空，证明第一次创建
            cls.__instance = object.__new__(cls)#通过__new__(cls)创建空对象
            return cls.__instance
        else:
            return cls.__instance

a = Earth()
print(id(a))
b = Earth()
print(id(b))
```

#### 7.6.3  `__call__`一个类的实例也可以变成一个可调用对象

```python
#对象加括号，自动执行__call__方法
class Foo(object):
    def __call__(self,*args,**kwargs):
        print('执行call方法')
obj = Foo()
obj()
Foo()()
```

```python
#练习：改进一下前面定义的斐波那契数列，加一个__call__,调用更加简单。
class Fib(object):
    def __init__(self):
        pass
    def __call__(self,num):
        a,b = 0,1
        self.l = []

        for i in range(num):
            self.l.append(a)
            a,b = b,a+b
        return self.l
    def __str__(self):
        return str(self.l)
    __repr__ = __str__
f = Fib()
print(f(10))
```

```python
#拓展····

#!/usr/bin/env python
# -*- coding:utf-8 -*-
from wsgiref.simple_server import make_server

def func(environ,start_response):
    start_response("200 OK", [('Content-Type', 'text/plain; charset=utf-8')])
    return ['你好'.encode("utf-8")  ]

class Foo(object):

    def __call__(self, environ,start_response):
        start_response("200 OK", [('Content-Type', 'text/html; charset=utf-8')])
        return ['你<h1 style="color:red;">不好</h1>'.encode("utf-8")]


# 作用：写一个网站，用户只要来方法，就自动找到第三个参数并执行。
server = make_server('127.0.0.1', 8000, Foo())
server.serve_forever()
```

```python
#call 方法实现fib
class Fib:

    def __call__(self, num):
        a,b=0,1
        self.l = []
        for i in range(num):
            self.l.append(a)
            a,b = b,a+b
        return self.l
    def __str__(self):
        return str(self.l)

f = Fib()
print(f(10))
```

#### 7.6.4 `__getitem__`  `__setitem__` `__delitem__`

```python
class Foo(object):
    def __setitem__(self,key,value):
        pass
    def __getitem__(self,item):
        return item + 'uuu'
    def __delitem__(self,key):
        pass
obj1 = Foo()
obj1['k1'] = 123			#内部自动调用__setitem__方法
val = obj1['xxx']			#内部自动调用__getitem__方法
print(val)
del obj1['ttt']				#内部会自动调用__delitem__方法
```

```python
class MyList:
    def __init__(self, iterable):
        print("__init__被调用")
        self.data = list(iterable)

    def __repr__(self):
        return 'MyList(%r)' % self.data

    def __getitem__(self, i):
        print("__getitem__被调用, i=", i)
        if type(i) is int:
            print("正在做索引操作")
        elif type(i) is slice:
            print("正在做切片操作")
            print("切片的起始值:", i.start)
            print("切片的终止值:", i.stop)
            print("切片的步长:", i.step)
        else:
            raise KeyError
        return self.data[i]


L1 = MyList([1, -2, 3, -4, 5, -6])

print(L1[2:5:2])
```

#### 7.6.5`__str__`

```python
#输出打印自动调用
class Foo(object):
    def __str__(self):
        """
        只有正在打印会自动调用此方法，并将其返回值在页面显示出来
        """
        return 'abcdefghi'
    obj = Foo()
    print(obj)
    #'abcdefghi'
```

```python
class User(object):
    def __init__(self,name,email):
        self.name = name
        self.email = email
    def __str__(self):
        return '%s %s'%(self.name,self.email)
user_list = [User('二狗','2g@qq.com'),User('二蛋','2d@qq.com'),User('狗蛋','xx@qq.com')]
for item in user_list:
    print(item)  
```

7.6.6 `__dict__`

```python
class Foo(object):
    def __init__(self,name,age,email):
        self.name = name
        self.age = age 
        self.email = email
obj = Foo('alex',19,'xxx@qq.com')
val = obj.__dict__
print(val)
#{'name': 'alex', 'age': 19, 'email': 'xxx@qq.com'}
#返回一个字典。
```

#### 2.7 上下文管理【面试题】

```python
class Foo(object):
    def __enter__(self):
        self.x = open('log.txt','r',encoding='utf-8')
        return self.x
    def __exit__(self, exc_type, exc_val, exc_tb):
        self.x.close()
with open Foo() as file:
    file.write('alex')
```

```python
# class Context:
#     def __enter__(self):
#         print('进入')
#         return self
#
#     def __exit__(self, exc_type, exc_val, exc_tb):
#         print('退出')
#
#     def do_something(self):
#         print('内部执行')
#
# with Context() as ctx:
#     print('内部执行')
#     ctx.do_something()


class Foo(object):
    def do_something(self):
        print('内部执行')

class Context:
    def __enter__(self):
        print('进入')
        return Foo()

    def __exit__(self, exc_type, exc_val, exc_tb):
        print('推出')

with Context() as ctx:
    print('内部执行')
    ctx.do_something()
```

#### 7.6.8 两个对象相加

```python
class Foo(object):
    def __add__(self,other):
        return 123
obj1 = Foo()
obj2 = Foo()
val = obj1 +boj2
print(val)
#123
```

### 7.7 异常的处理

#### 7.7.1 基本格式

```python
try:
    pass
except Exception as e:
    pass
```

```python
try:
    v = []
    v[111]
except ValueError as e:
    pass
except IndexError as e:
    pass
except Exception as e:
    print(a)
#e 是Exception类的对象，中有一个错误信息
```

```python
try:
    int('asdf')
except Exception as e:
    print(e) # e是Exception类的对象，中有一个错误信息。
finally:
    print('最后无论对错都会执行')
    
# #################### 特殊情况 #########################
def func():
    try:
        # v = 1
        # return 123
        int('asdf')
    except Exception as e:
        print(e) # e是Exception类的对象，中有一个错误信息。
        return 123
    finally:
        print('最后')

func()
```

#### 7.7.2 主动触发异常

```python
try:
    int('123')
    raise Exception('我想让他错误')#代码中主动抛出异常
except Exception as e:
    print(e)
```

```python
def func():
    result = True
    try:
        with open('x.log',mode='r',encoding='utf-8') as f:
            data = f.read()
        if 'alex' not in data:
            raise Exception()
    except Exception as e:
        result = False
    return result
```

#### 7.7.3自定义异常

```python
class MyException(Exception):
    pass

try:
    raise MyException('asdf')
except MyException as e:
    print(e)
    
class MyException(Exception):
    def __init__(self,message):
        super().__init__()
        self.message = message

try:
    raise MyException('asdf')
except MyException as e:
    print(e.message)
```

### 7.8类执行方式

```python
class Foo:
    print('你好')
    def func(self):
        pass
#看代码能否打印你好？
答案：能打印
```

```python
class Foo:
    x = 1
    print(x)
    def func(self):
        pass

    class Meta:
        y = 123
        print(y)
        def show(self):
            pass
#上述代码执行，均能打印。
#1
#123
```

### 7.9栈

- 栈的实现，是把列表包装成一个类，再添加方法做为栈的基本操作，其他的数据结构在python也是以类似的方式实现。遵从原则后进先出。

```python
class Stack(object):
    """
    后进先出
    """
    def __init__(self):
        self.data_list = []

    def push(self,val):
        """
        向栈中压入一个数据（入栈）
        :param val:
        :return:
        """
        self.data_list.append(val)

    def pop(self):
        """
        从栈中拿走一个数据出栈
        :return:
        """
        return self.data_list.pop()
obj = Stack()
obj.push('曹操')
obj.push('刘备')
obj.push('孙策')

print(obj.pop())
print(obj.pop())
print(obj.pop())
#孙策
#刘备
#曹操
```

### 7.10 约束

```python
#约束子类中必须写send方法，如果不写，则调用的时候就抛出NotImplementedError
class BaseMessage(object):
    def send(self):
        raise NotImplementedError('如果子类没send触发错误')
class Msg(BaseMessage):
    def send(self):
        pass
class Email(BaseMessage):
    def send(self):
        pass
class Wechat(BaseMessage):
    pass
class DingDing(BaseMessage):
    def send(self):
        print('钉钉')
obj = Wechat()
obj.send()
#raise NotImplementedError('如果子类没send触发错误')
#NotImplementedError: 如果子类没send触发错误
```

### 7.11反射

- 根据字符串的形式去某个对象中操作他的成员。

- getattr  根据字符串的形式去某个对象中获取对象的成员

  ```python
  反射
      # 通过 对象 来获取 实例变量、绑定方法
      # 通过 类   来获取  类变量、类方法、静态方法
      # 通过 模块名 来获取 模块中的任意变量（普通变量 函数  类）
      # 通过 本文件 来获取 本文件中的任意变量
          # getattr(sys.modules[__name__],'变量名')
  ```

  

```python
class Foo(object):
    def __init__(self,name):
        self.name = name
    def login(self):
        print('你好')

obj = Foo('小明')
#获取变量
v1 = getattr(obj,'name')
print(v1)
# 获取方法
method_name = getattr(obj,'login')
method_name()
#小明
#你好
```

- hasattr 根据字符串的形式去某个对象中判断是否有该成员。

```python
from wsgiref.simple_server import make_server

class View(object):
    def login(self):
        return '登陆'

    def logout(self):
        return '等处'

    def index(self):
        return '首页'


def func(environ,start_response):
    start_response("200 OK", [('Content-Type', 'text/plain; charset=utf-8')])
    #
    obj = View()
    # 获取用户输入的URL
    method_name = environ.get('PATH_INFO').strip('/')
    if not hasattr(obj,method_name):
        return ["sdf".encode("utf-8"),]
    response = getattr(obj,method_name)()
    return [response.encode("utf-8")  ]

# 作用：写一个网站，用户只要来方法，就自动找到第三个参数并执行。
server = make_server('192.168.12.87', 8000, func)
server.serve_forever()
```

```python
#x1.py
num = 1
def func():
    print('hello')

class Foo:
    def fs(self):
        print('你好')
        
#base.py
# method_name()
import x1

v1 = getattr(x1,'num')
print(v1)
#1
v2 = getattr(x1,'func')
v2()
#hello
v3 = getattr(x1,'Foo')
print(v3)
#<class 'x1.Foo'>    相当于v3 = Foo
v4 = getattr(v3(),'fs')
v4()
#你好
```

- setattr  根据字符串的形式去某个对象中设置成员

```python
class Foo:
    pass
obj = Foo()
obj.k1 = 999
setattr(obj,'k1',123)
print(obj.k1)
```

- delattr  根据字符串的形式去某个对象中删除

```python
class Foo:
    pass
obj = Foo()
obj.k1 = 999

delattr(obj,'k1')
print(obj.k1)


class Foo:
    a = 100
    b = 50
obj = Foo()
#delattr(obj,'a')与delattr(Foo,'a') 前者会报错求原因？

```



### 7.12  23种设计模式  单例模式

```python
class Singleton(object):
    instance = None
    def __new__(cls, *args, **kwargs):
        if not cls.instance:
            cls.instance = object.__new__(cls)
        return cls.instance

obj1 = Singleton()
obj2 = Singleton()
# 不是最终，加锁。
```

```python
#单例模式，文件的连接池（连接池是创建和管理一个连接的缓冲池的技术，这些连接准备好被任何需要它们的线程使用。）

class FileHelper(object):
    instance = None
    def __init__(self, path):
        self.file_object = open(path,mode='r',encoding='utf-8')

    def __new__(cls, *args, **kwargs):
        if not cls.instance:
            cls.instance = object.__new__(cls)
        return cls.instance

obj1 = FileHelper('x')
obj2 = FileHelper('x')

```

```python
import time
from threading import Lock
class A:
    __instance = None
    lock = Lock()
    def __new__(cls, *args, **kwargs):
        with cls.lock:
            if not cls.__instance:
                time.sleep(0.1)
                cls.__instance = super().__new__(cls)
        return cls.__instance
    def __init__(self,name,age):
        self.name = name
        self.age = age

def func():
    a = A('alex', 84)
    print(a)

from threading import Thread
for i in range(10):
    t = Thread(target=func)
    t.start()
```

### 8. 项目结构

```python
# 项目名
    # bin  start
        # import os
        # import sys
        # base_path = os.path.dirname(os.path.dirname(__file__))
        # sys.path.append(base_path)

        # from src import core
    # conf 配置 settings
    # src  业务逻辑
        # student.py
            # from src import core
        # core.py
            # from conf import settings
    # db   数据文件
    # lib  扩展模块
    # log  日志文件
```



## 第八章 网络编程

### 1.网络基础

```python
# 1.两个运行中的程序如何传递信息？
    # 通过文件
# 2.两台机器上的两个运行中的程序如何通信？
    # 通过网络
# 网络应用开发架构
    # C/S  迅雷 qq 浏览器 飞秋 输入法 百度云 pycharm git VNC 红蜘蛛 各种游戏
        # client 客户端
        # server 服务端
    # B/S   淘宝 邮箱 各种游戏 百度 博客园 知乎 豆瓣 抽屉
        # browser 浏览器
        # server  服务端
    # 统一程序的入口
    # B/S和C/S架构的关系
        # B/S是特殊的C/S架构
# 网卡 ：是一个实际存在在计算机中的硬件
# mac地址 ：每一块网卡上都有一个全球唯一的mac地址
# 交换机 ：是连接多台机器并帮助通讯的物理设备，只认识mac地址
# 协议 ：两台物理设备之间对于要发送的内容，长度，顺序的一些约定
# ip地址
    # ipv4协议  位的点分十进制  32位2进制表示
    # 0.0.0.0 - 255.255.255.255
    # ipv6协议 6位的冒分十六进制 128位2进制表示
    # 0:0:0:0:0:0-FFFF:FFFF:FFFF:FFFF:FFFF:FFFF
# 公网ip
# 为什么你的外地朋友的电脑我们访问不了
    # 每一个ip地址要想被所有人访问到，那么这个ip地址必须是你申请的
# 内网ip
    # 192.168.0.0 - 192.168.255.255
    # 172.16.0.0 - 172.31.255.255
    # 10.0.0.0 - 10.255.255.255
# 交换机实现的arp协议
    # 通过ip地址获取一台机器的mac地址
# 网关ip 一个局域网的网络出口，访问局域网之外的区域都需要经过路由器和网关
# 网段 指的是一个地址段 x.x.x.0  x.x.0.0  x.0.0.0
# ip地址
# 子网掩码 判断两台机器是否在同一个网段内的
#在使用TCP/IP的两台计算机之间进行通信时，TCP/IP是通过将本机的子网掩码与接收方主机的IP地址进行“与”运算，得到目标主机所在的网络ID。通过与本机所在的网络ID进行比较，就可以知道接收方主机是否在本网络上，如果网络ID相同，表明接收方在本网络上，那么TCP/IP就可以通过相关的协议把数据包直接发送到目标主机。如果网络ID不同，目标主机在远程网络上，那么数据包将会发送给本网络上的路由器，由路由器将数据包发送到其他网络，直至达到目的地。
#区分不同主机是不是在同一个网络的决定性因素就是对应主机IP地址中的网络ID。而决定IP地址类型的却是网络ID长度。只要网络ID长度一样，就属于同一类网络，这样在一个网段中就只能有一个网络，因为它们的网络子网掩码是一样的。网络标识部分包括网络ID和子网ID。
# 192.168.12.87
# 11000000.10101000.00001100.01010111
# 11111111.11111111.11111111.00000000
# 11000000.10101000.00001100.00000000   192.168.12.0
# 192.168.12.7
# 11000000.10101000.00001100.00000111
# 11111111.11111111.11111111.00000000
# 11000000.10101000.00001100.00000000  192.168.12.0
# ip 地址能够确认一台机器
# port 端口
	#每个端口只会同事被一个程序使用，不会同时用
    # 0-65535
    # 80
# ip + port 确认一台机器上的一个应用
```

- 基于TCP协议的黏包问题

```python
#TCP特点：  tcp为提高性能，发送端会将需要发送的数据发送到缓冲区，会稍作等待，看还有没有小数据会发送（举例海南送快递，拿到一件，会等你半天，看还有没有去海南的件，没有就发，或者收满了，直接就发），再将缓冲中的数据发送到接收方。同理，接收方也有缓冲区这样的机制，来接收数据。

    # 发生在发送端的粘包
        # 由于两个数据的发送时间间隔短+数据的长度小
        # 所以由tcp协议的优化机制将两条信息作为一条信息发送出去了
        # 为了减少tcp协议中的“确认收到”的网络延迟时间
    # 发生再接收端的粘包
        # 由于tcp协议中所传输的数据无边界，所以来不及接收的多条
        # 数据会在接收放的内核的缓存端黏在一起
    # 本质： 接收信息的边界不清晰
```

- 解决黏包问题

```python
#struct模块
#server端
import struct
import socket

sk = socket.socket()
sk.bind(('127.0.0.1',9000))
sk.listen()

conn,_ = sk.accept()
byte_len = conn.recv(4)
size = struct.unpack('i',byte_len)[0]
msg1 = conn.recv(size)
print(msg1)
byte_len = conn.recv(4)
size = struct.unpack('i',byte_len)[0]
msg2 = conn.recv(size)
print(msg2)
conn.close()
sk.close()
```

```python
#client端
import struct
import socket

sk = socket.socket()
sk.connect(('127.0.0.1',9000))

msg = b'hello'
byte_len = struct.pack('i',len(msg))
sk.send(byte_len)   #  1829137
sk.send(msg)        #  1829139
msg = b'world'
byte_len = struct.pack('i',len(msg))
sk.send(byte_len)
sk.send(msg)

sk.close()
```



2.TCP协议

```python
#3次握手，4次挥手（底层实现）
#握手                                        挥手	
#client端                                   1----一端向另一端发送断开请求
1---发送链接请求							2-----同意
#server端								3-----向另一方发送断开请求
2---同意并发送链接请求						4 -----同意
#client端
3---同意 
# 三次握手
    # accept接受过程中等待客户端的连接
    # connect客户端发起一个syn链接请求
        # 如果得到了server端响应ack的同时还会再收到一个由server端发来的syc链接请求
        # client端进行回复ack之后，就建立起了一个tcp协议的链接
    # 三次握手的过程再代码中是由accept和connect共同完成的，具体的细节再socket中没有体现出来

# 四次挥手
    # server和client端对应的在代码中都有close方法
    # 每一端发起的close操作都是一次fin的断开请求，得到'断开确认ack'之后，就可以结束一端的数据发送
    # 如果两端都发起close，那么就是两次请求和两次回复，一共是四次操作
    # 可以结束两端的数据发送，表示链接断开了
#TCP协议的特点  （类似于打电话）
    # 可靠 慢 全双工通信
    # 建立连接的时候 ： 三次握手
    # 断开连接的时候 ： 四次挥手
    # 在建立起连接之后
        # 发送的每一条信息都有回执
        # 为了保证数据的完整性，还有重传机制
    # 长连接 ：会一直占用双方的端口

```

3. UDP协议

```python
# udp协议  -- 发短信
    # 无连接的 速度快
    # 可能会丢消息
    # 能够传递的数据的长度是有限的，是根据数据传递设备的设置有关系
```

4. 关于IO操作

   ```python
       # IO（input,output）操作,输入和输出是相对内存来说的
           # write send  -  output    从内存写入到文件
           # read recv   -  input		从文件读入到内存
       # 能够传递的数据长度几乎没有限制
   ```

5. OSI  7层模型(必背)

   ```python
   # osi七层模型
   # 应用层
       # 表示层
       #解决用户信息的语法表示问题
       # 会话层
       #认为是一次通讯的过程，比如两个人打电话，称为一次会话。会话层不参与具体的传输，它提供包括访问验证和会话管理在内的建立和维护应用之间通信的机制。
       # 传输层（数据段）
      	#传输层的服务一般要经历传输连接建立阶段、数据传送阶段、传输连接释放阶段3个阶段才算完成一个完整的服务过程
       # 网络层（数据包）
       #网络层的任务就是选择合适的网间路由和交换结点，确保数据及时传送。网络层将解封数据链路层收到的帧，提取数据包，包中封装有网络层包头，其中含有逻辑地址信息- -源站点和目的站点地址的网络地址。
       # 数据链路层（帧）
       #mac地址  arp协议      网卡 、交换机
       # 物理层
       
   
   # osi五层协议     协议                  物理设备
       # 应用层      http https ftp smtp
       #             python代码  hello
       # 传输层      tcp/udp协议 端口      四层交换机、四层路由器
       # 网络层      ipv4/ipv6协议         路由器、三层交换机
       # 数据链路层  mac地址  arp协议      网卡 、交换机
       # 物理层
   ```

6. 关于socket

   ```python
   # socket  套接字
   # python     socket模块    完成socket的功能
   # 工作在应用层和传输层之间的抽象层
       # 帮助我们完成了所有信息的组织和拼接
   # sokcet对于程序员来说 已经是网络操作的底层了
   # socket历史
       # 同一台机器上的两个服务之间的通信的
           # 基于文件
       # 基于网路的多台机器之间的多个服务通信
   ```

7. 非阻塞io模型

   ```python
   #server端  可实现TCP连接多个用户
   import socket
   sk = socket.socket()
   sk.bind(('127.0.0.1',9000))
   sk.setblocking(False)
   sk.listen()
   
   conn_l = []
   del_l = []
   while True:
       try:
           conn,addr = sk.accept()   # 阻塞，直到有一个客户端来连我
           print(conn)
           conn_l.append(conn)
       except BlockingIOError:
           for c in conn_l:
               try:
                   msg = c.recv(1024).decode('utf-8')
                   if not msg:
                       del_l.append(c)
                       continue
                   print('-->',[msg])
                   c.send(msg.upper().encode('utf-8'))
               except BlockingIOError:pass
           for c in del_l:
               conn_l.remove(c)
           del_l.clear()
   sk.close()
   ```

8. 校验客户端的合法性

   - client端第一次连接后，server端会发送一个随机字符串给client端，client端会通过密匙加密这个字符串，然后发送给server端，server端也会加密这个随机的字符串，然后将server端的发送过来的加密结果进行对比，如果不匹配，强制关闭这个连接

```python
import socket
import time
import hmac

def get_md5(secret_key,randseq):
    h = hmac.new(secret_key,randseq)
    res = h.digest()
    return res
def chat(sk):
    while True:
        sk.send(b'hello')
        msg = sk.recv(1024).decode('utf-8')
        print(msg)
        time.sleep(0.5)

sk = socket.socket()
sk.connect(('127.0.0.1',9000))

secret_key = b'alexsb'
randseq = sk.recv(32)
hmaccode = get_md5(secret_key,randseq)

sk.send(hmaccode)
chat(sk)

sk.close()
```



## 第九章 并发编程

### 1.0 关于操作系统的一些基础知识与历史

1.1 早先计算机存在的问题

- CPU利用率低

### 1.2 解决CPU利用低方案

1.2.1多道操作系统 --在一个任务遇到IO的时候主动让出CPU

  - 数据隔离
  - 时空复用
  - 能够在一个任务遇到IO操作的时候主动把CPU让出来给其他的任务使用
      - 切换要不要占用时间：要专用
      - 切换这件事是谁做的：操作系统

1.2.2 分时操作系统 --给时间分片，多个任务轮流使用cpu

 - 短作业优先算法
 - 先来先服务算法
 - 时间分片
    - cpu的轮转
    - 每个程序分配一个时间片
- 要切换  要占用时间
- 反而降低了cpu的利用率
- 提高了用户体验

1.2.3 分时操作系统 + 多道操作系统 + 实时 操作系统

- 多个程序一起在计算机中执行
- 一个程序没遇到IO，但是时间片到时了，切出去让出CPU
- 一个程序遇到IO。切出去让出CPU

1.2.4网络操作系统

1.2.5 分布式操作系统

### 1.3  进程的概念

1.3.1进程概念

```python
#进程：运行中的程序
#程序和进程之间的区别
	1.程序只是个文件
    2.今晨是这个文件被CPU利用起来了
#进程 是计算机中最小的资源分配单位
#在操作系统中的唯一表示符：pid
```

1.3.2 操作系统调度进程的算法

```python
#短作业优先
#先来先服务
#时间片轮转
#多级反馈算法
```

1.3.3 并发与并行

```python
#并行
	#两个程序，两个CPU 每个程序分别占用一个CPU自己执行自己的
    #看起来是同事执行，实际在每个时间点上都在各自执行着
#并发
	# 两个程序，一个CPU 每个程序交替的在一个cpu上执行 
    #  看起来在同时执行，但是实际上仍然是串行
# 并发 ：并发是指多个程序 公用一个cpu轮流使用
# 并行 ：多个程序 多个cpu 一个cpu上运行一个程序，
	# 在一个时间点上看，多个程序同时在多个cpu上运行

```



1.3.4 同步 与 异步

```python
#同步  
	正在烧水，提下烧水这个动作 吹头发，吹完头发继续烧水
# 异步 
	正在烧水 吹头发 烧水也在继续
 # 同步 ：调用一个方法要等待这个方法结束
        # 异步 ：调用一个方法 不等待这个方法结束 也不关心这个方法做了什么
```

```python
#异步： strat（）
```



1.3.5阻塞非阻塞

```python
#阻塞  ：cpu不工作
#非阻塞：cpu工作
```

1.3.6同步阻塞

```python
#conn,recv
#socket 非阻塞的TCP协议的时候
#调度函数（这个函数内部不存在io操作）
1. join()
```

1.3.7 同步非阻塞

```python
#func()  没有io操作
#socket  非阻塞的TCP协议的时候
#调用函数 （这个函数内部不存在io操作）
```

1.3.8 异步非阻塞

```python
#把func扔到其他任务里去执行
#我本身的任务和func任务各自执行各自的，没有io操作
```

1.3.9 异步阻塞(待总结)

```python

```

1.4.0进程的三状态图

```python
# -->就绪 -system call-><-时间片到了- 运行 -io-> 阻塞-IO结束->就绪
        # 阻塞 影响了程序运行的效率
```



![](D:\py21\关于图片\进程的三状态图_WPS图片.png)

​	

### 1.4.1 子进程与进程相关模块

```python
# 子进程
# 父进程 在父进程中创建子进程
# 在pycharm中启动的所有py程序都是pycharm的子进程
# 操作系统创建进程的方式不同
# windows操作系统执行开启进程的代码
    # 实际上新的子进程需要通过import父进程的代码来完成数据的导入工作
    # 所以有一些内容我们只希望在父进程中完成，就写在if __name__ == '__main__':下面
# ios linux操作系统创建进程 fork
```

- 函数创建子进程multiprocessing 进程

  ```python
  import os
  import time
  from multiprocessing import Process
  
  def func():
      print('start',os.getpid())
      time.sleep(10)
      print('end',os.getpid(),os.getppid())
  
  if __name__ == '__main__':
      p = Process(target=func)
      p.start()   # 异步 调用开启进程的方法 但是并不等待这个进程真的开启
      print('main :',os.getpid(),os.getppid())
  ```

- 创建两个子进程

  ```python
  import os
  import time
  from multiprocessing import Process
  
  def eat():
      print('start eating',os.getpid())
      time.sleep(1)
      print('end eating',os.getpid())
  
  def sleep():
      print('start sleeping',os.getpid())
      time.sleep(1)
      print('end sleeping',os.getpid())
  
  if __name__ == '__main__':
      p1 = Process(target=eat)    # 创建一个即将要执行eat函数的进程对象
      p1.start()                  # 开启一个进程
      p2 = Process(target=sleep)  # 创建一个即将要执行sleep函数的进程对象
      p2.start()                  # 开启进程
      print('main :',os.getpid())
  
  ```

- 主进程和子进程之间的关系

  ```python
  import os
  import time
  from multiprocessing import Process
  def func():
      print('start',os.getpid())
      time.sleep(10)
      print('end',os.getpid())
  
  if __name__ == '__main__':
      p = Process(target=func)
      p.start()   # 异步 调用开启进程的方法 但是并不等待这个进程真的开启
      print('main :',os.getpid())
       # 主进程没结束 ：等待子进程结束
      # 主进程负责回收子进程的资源
      # 如果子进程执行结束，父进程没有回收资源，那么这个子进程会变成一个僵尸进程
  ```

  ```python
  # 主进程的结束逻辑
          # 主进程的代码结束
          # 所有的子进程结束
          # 给子进程回收资源
          # 主进程结束
  # 主进程怎么知道子进程结束了的呢？
      # 基于网络、文件
  ```

- join方法

  ```python
  # join方法 ：阻塞，直到子进程结束就结束
  # import time
  # from multiprocessing import Process
  # def send_mail():
  #     time.sleep(3)
  #     print('发送了一封邮件')
  # if __name__ == '__main__':
  #     p = Process(target=send_mail)
  #     p.start()   # 异步 非阻塞
  #     # time.sleep(5)
  #     print('join start')
  #     p.join()    # 同步 阻塞 直到p对应的进程结束之后才结束阻塞
  #     print('5000封邮件已发送完毕')
  
  ```

- 开启10个进程，给公司的5000个人发邮件，发送完邮件之后，打印一个消息“5000封邮件已发送完毕”

  ```python
  import time
  import random
  from multiprocessing import Process
  def send_mail(a):
      time.sleep(random.random())
      print('发送了一封邮件',a)
  
  if __name__ == '__main__':
      l = []
      for i in range(10):
          p = Process(target=send_mail,args=(i,))
          p.start()
          l.append(p)
      print(l)
      for p in l:p.join()
      # 阻塞 直到上面的十个进程都结束
      print('5000封邮件已发送完毕')
  ```

  ##### 1.4.1  总结

  ```python
  总结
  # 1.开启一个进程
      # 函数名(参数1,参数2)
      # from multiprocessing import Process
      # p = Process(target=函数名,args=(参数1,参数2))
      # p.start()
      # 2.父进程  和 子进程
  # 3.父进程会等待着所有的子进程结束之后才结束
      # 为了回收资源
  # 4.进程开启的过程中windows和 linux/ios之间的区别
      # 开启进程的过程需要放在if __name__ == '__main__'下
          # windows中 相当于在子进程中把主进程文件又从头到尾执行了一遍
              # 除了放在if __name__ == '__main__'下的代码
          # linux中 不执行代码,直接执行调用的func函数
  # 5.join方法
      # 把一个进程的结束事件封装成一个join方法
      # 执行join方法的效果就是 阻塞直到这个子进程执行结束就结束阻塞
  
      # 在多个子进程中使用join
      # p_l= []
      # for i in range(10):
          # p = Process(target=函数名,args=(参数1,参数2))
          # p.start()
          # p_l.append(p)
      # for p in p_l:p.join()
      # 所有的子进程都结束之后要执行的代码写在这里
  ```



### 1.4 线程 

1.4.1 进程的缺点

```python
#进程是计算机最小的资源分配单位
	#数据隔离的
    #YY,陌陌 飞秋 QQ 微信 腾讯视频
    	#一个进程
        #和一个人通信
        #一遍缓存，一遍看另一个电影的直播
 #进程
	#创建进程，时间开销大
    #销毁进程 时间开销大
    # 进程之间切换 时间开销大
 #如果两个程序 分别做两件事
	#起两个进程
 #如果是一个程序  要分别做两件事
	#例如 视频软件分下载3个电影
    #启动3个进程来完成上面的三件事情，但是开销大	
```

1.4.2 线程

```python
#线程是进程中的一部分，每一个进程中至少有一个线程
# 进程是计算机中最小的资源分配单位（进程是负责圈资源的）
#线程是计算机中能被cpu调度的最小单位（线程是负责执行具体代码的）
#开销
	# 线程的创建，也需要一些开销（一个存储局部变量的结构，记录状态）
    #创建销毁 切换开销远远小于进程
python 中的线程比较特殊，所以进程也有可能被用到
# 进程：数据隔离，开销大，同事执行几段代码
# 线程：数据共享，开销小。同事执行几段代码
# 线程：开销小 数据共享 是进程的一部分，不能独立存在 本身可以利用多核

```

### 1.5.0 from   mutiprocessing    import    Process   模块下的类

```python
# Process类
# 开启进程的方式
    # 面向函数
        # def 函数名:要在子进程中执行的代码
        # p = Process(target= 函数名,args=(参数1，))
    # 面向对象
        # class 类名(Process):
            # def __init__(self,参数1，参数2):   # 如果子进程不需要参数可以不写
                # self.a = 参数1
                # self.b = 参数2
                # super().__init__()
            # def run(self):
                # 要在子进程中执行的代码
        # p = 类名(参数1，参数2)
    # Process提供的操作进程的方法
        # p.start() 开启进程      异步非阻塞
        # p.terminate() 结束进程  异步非阻塞

        # p.join()     同步阻塞
        # p.isalive()  获取当前进程的状态

        # daemon = True 设置为守护进程，守护进程永远在主进程的代码结束之后自动结束
```

1.5.1 类方法创建进程例子

```python
import os
import time
from multiprocessing import Process

class MyProcecss2(Process):
    def run(self):
        while True:
            print('is alive')
            time.sleep(0.5)

class MyProcecss1(Process):
    def __init__(self,x,y):
        self.x = x
        self.y = y
        super().__init__()
    def run(self):
        print(self.x,self.y,os.getpid())
        for i in range(5):
            print('in son2')
            time.sleep(1)

if __name__ == '__main__':
    mp = MyProcecss1(1,2)
    mp.daemon = True
    mp.start()
    print(mp.is_alive())
    mp.terminate()
    mp2 = MyProcecss2()
    mp2.start()
    print('main :',os.getpid())
    time.sleep(1)
```



1.5.2 守护进程

```python
# 有一个参数可以把一个子进程设置为一个守护进程
import time
from multiprocessing import Process

def son1(a,b):
    while True:
        print('is alive')
        time.sleep(0.5)

def son2():
    for i in range(5):
        print('in son2')
        time.sleep(1)

if __name__ == '__main__':
    p = Process(target=son1,args=(1,2))
    p.daemon = True
    p.start()      # 把p子进程设置成了一个守护进程
    p2 = Process(target=son2)
    p2.start()
    time.sleep(2)
# 守护进程是随着主进程的代码结束而结束的
    # 生产者消费者模型的时候
    # 和守护线程做对比的时候
# 所有的子进程都必须在主进程结束之前结束，由主进程来负责回收资源
```

```python
import time
from multiprocessing import Process

def son1():
    while True:
        print('is alive')
        time.sleep(0.5)

if __name__ == '__main__':
    p = Process(target=son1)
    p.start()      # 异步 非阻塞
    print(p.is_alive())
    time.sleep(1)
    p.terminate()   # 异步的 非阻塞
    print(p.is_alive())   # 进程还活着 因为操作系统还没来得及关闭进程
    time.sleep(0.01)
    print(p.is_alive())   # 操作系统已经响应了我们要关闭进程的需求，再去检测的时候，得到的结果是进程已经结束了

# 什么是异步非阻塞？
    # terminate
```

### 1.6 锁

1.6.1 锁的概念用法

```python
# 并发 能够做的事儿
# 1.实现能够响应多个client端的server
# 2.抢票系统
import time
import json
from multiprocessing import Process,Lock

def search_ticket(user):
    with open('ticket_count') as f:
        dic = json.load(f)
        print('%s查询结果  : %s张余票'%(user,dic['count']))

def buy_ticket(user,lock):
    # with lock:
    # lock.acquire()   # 给这段代码加上一把锁
        time.sleep(0.02)
        with open('ticket_count') as f:
            dic = json.load(f)
        if dic['count'] > 0:
            print('%s买到票了'%(user))
            dic['count'] -= 1
        else:
            print('%s没买到票' % (user))
        time.sleep(0.02)
        with open('ticket_count','w') as f:
            json.dump(dic,f)
    # lock.release()   # 给这段代码解锁

def task(user, lock):
    search_ticket(user)
    with lock:
        buy_ticket(user, lock)

if __name__ == '__main__':
    lock = Lock()
    for i in range(10):
        p = Process(target=task,args=('user%s'%i,lock))
        p.start()


# 1.如果在一个并发的场景下，涉及到某部分内容
    # 是需要修改一些所有进程共享数据资源
    # 需要加锁来维护数据的安全
# 2.在数据安全的基础上，才考虑效率问题
# 3.同步存在的意义
    # 数据的安全性

# 在主进程中实例化 lock = Lock()
# 把这把锁传递给子进程
# 在子进程中 对需要加锁的代码 进行 with lock：
    # with lock相当于lock.acquire()和lock.release()
# 在进程中需要加锁的场景
    # 共享的数据资源（文件、数据库）
    # 对资源进行修改、删除操作
# 加锁之后能够保证数据的安全性 但是也降低了程序的执行效率

```

### 1.6  进程之间的通信

1.6.1 进程的数据隔离

```python
# 进程之间的数据隔离
# from multiprocessing import Process
# n = 100
# def func():
#     global n
#     n -= 1
#
# if __name__ == '__main__':
#     p_l = []
#     for i in range(10):
#         p = Process(target=func)
#         p.start()
#         p_l.append(p)
#     for p in p_l:p.join()
#     print(n)
```

1.6.2 进程之间的通讯

```python
# 进程之间的通信 - IPC（inter process communication）
# ipc，常见的ipc机制
    # 进程之间通信
    # ipc机制 ：队列 管道
    # 第三方工具（软件）提供给我们的IPC机制
        # redis
        # memcache
        # kafka
        # rabbitmq

        # 并发需求
        # 高可用
        # 断电保存数据
        # 解耦
# from multiprocessing import Queue,Process
# # 先进先出
# def func(exp,q):
#     ret = eval(exp)
#     q.put({ret,2,3})
#     q.put(ret*2)
#     q.put(ret*4)
#
# if __name__ == '__main__':
#     q = Queue()
#     Process(target=func,args=('1+2+3',q)).start()
#     print(q.get())
#     print(q.get())
#     print(q.get())
```

1.6.3   Queue 

```python
# Queue基于 天生就是数据安全的
    # 文件家族的socket pickle lock
# pipe 管道(不安全的) = 文件家族的socket pickle
# 队列 = 管道 + 锁
# from multiprocessing import Pipe
# pip = Pipe()
# pip.send()
# pip.recv()
```

```python
import queue

# from multiprocessing import Queue
# q = Queue(5)
# q.put(1)
# q.put(2)
# q.put(3)
# q.put(4)
# q.put(5)   # 当队列为满的时候再向队列中放数据 队列会阻塞
# print('5555555')
# try:
#     q.put_nowait(6)  # 当队列为满的时候再向队列中放数据 会报错并且会丢失数据
# except queue.Full:
#     pass
# print('6666666')
#
# print(q.get())
# print(q.get())
# print(q.get())   # 在队列为空的时候会发生阻塞
# print(q.get())   # 在队列为空的时候会发生阻塞
# print(q.get())   # 在队列为空的时候会发生阻塞
# try:
#     print(q.get_nowait())   # 在队列为空的时候 直接报错
# except queue.Empty:pass
```

### 1.7 生产者消费者模型   * * * * *

```python
# 解耦 ：修改 复用 可读性
# 把写在一起的大的功能分开成多个小的功能处理
# 登陆 注册

# 进程
    # 一个进程就是一个生产者
    # 一个进程就是一个消费者
# 队列
    # 生产者和消费者之间的容器就是队列
# 什么是生产者消费者模型
        # 把一个产生数据并且处理数据的过程解耦
        # 让生产的数据的过程和处理数据的过程达到一个工作效率上的平衡
        # 中间的容器，在多进程中我们使用队列或者可被join的队列，做到控制数据的量
            # 当数据过剩的时候，队列的大小会控制这生产者的行为
            # 当数据严重不足的时候，队列会控制消费者的行为
            # 并且我们还可以通过定期检查队列中元素的个数来调节生产者消费者的个数
```

```python
import time
import random
from multiprocessing import Process,Queue

def producer(q,name,food):
    for i in range(10):
        time.sleep(random.random())
        fd = '%s%s'%(food,i)
        q.put(fd)
        print('%s生产了一个%s'%(name,food))

def consumer(q,name):
    while True:
        food = q.get()
        if not food:break
        time.sleep(random.randint(1,3))
        print('%s吃了%s'%(name,food))


def cp(c_count,p_count):
    q = Queue(10)
    for i in range(c_count):
        Process(target=consumer, args=(q, 'alex')).start()
    p_l = []
    for i in range(p_count):
        p1 = Process(target=producer, args=(q, 'wusir', '泔水'))
        p1.start()
        p_l.append(p1)
    for p in p_l:p.join()
    for i in range(c_count):
        q.put(None)
if __name__ == '__main__':
    cp(2,3)

```

```python
#爬虫例子
from multiprocessing import Process,Queue
import requests

import re
import json
def producer(q,url):
    response = requests.get(url)
    q.put(response.text)

def consumer(q):
    while True:
        s = q.get()
        if not s:break
        com = re.compile(
            '<div class="item">.*?<div class="pic">.*?<em .*?>(?P<id>\d+).*?<span class="title">(?P<title>.*?)</span>'
            '.*?<span class="rating_num" .*?>(?P<rating_num>.*?)</span>.*?<span>(?P<comment_num>.*?)评价</span>', re.S)
        ret = com.finditer(s)
        for i in ret:
            print({
                "id": i.group("id"),
                "title": i.group("title"),
                "rating_num": i.group("rating_num"),
                "comment_num": i.group("comment_num")}
            )

if __name__ == '__main__':
    count = 0
    q = Queue(3)
    p_l = []
    for i in range(10):
        url = 'https://movie.douban.com/top250?start=%s&filter='%count
        count+=25
        p = Process(target=producer,args=(q,url,)).start()
        p_l.append(p)
    p = Process(target=consumer, args=(q,)).start()
    for p in p_l:p.join()
    q.put(None)
```

1.7.1 joinablequeue  下的生产者消费者模型

```python
#特点是 每次队列中有一个计数器，消费者方每次去走一个后不会计数器不会立刻减1，而是等到消费者执行task_done才会在计数器减少1.而且每次生产者在放满10个后，会进入阻塞状态，直到队列中任意被取走且task_done 才会继续往队列里put
import time
import random
from  multiprocessing import JoinableQueue,Process

def producer(q,name,food):
    for i in range(10):
        time.sleep(random.random())
        fd = '%s%s'%(food,i)
        q.put(fd)
        print('%s生产了一个%s'%(name,food))
    q.join()       #当队列里为空的时候退出阻塞

def consumer(q,name):
    while True:
        food = q.get()
        time.sleep(random.random())
        print('%s吃了%s'%(name,food))
        q.task_done()

if __name__ == '__main__':
    jq = JoinableQueue()
    p =Process(target=producer,args=(jq,'wusir','泔水'))
    p.start()
    c = Process(target=consumer,args=(jq,'alex'))
    c.daemon = True
    c.start()
    p.join()
```

### 1.8 进程之间的数据共享

```python
from multiprocessing import Manager,Process,Lock

def func(dic,lock):
    with lock:
        dic['count'] -= 1

if __name__ == '__main__':
    # m = Manager()
    with Manager() as m:
        l = Lock()
        dic = m.dict({'count':100})
        p_l = []
        for i in range(100):
            p = Process(target=func,args=(dic,l))
            p.start()
            p_l.append(p)
        for p in p_l:p.join()
        print(dic)


# mulprocessing中有一个manager类
# 封装了所有和进程相关的 数据共享 数据传递
# 相关的数据类型
# 但是对于 字典 列表这一类的数据操作的时候会产生数据不安全
# 需要加锁解决问题，并且需要尽量少的使用这种方式
```

### 1.9 线程的理论    GIL 锁 * * * * 

```python

# 线程 开销小 数据共享 是进程的一部分
# 进程 开销大 数据隔离 是一个资源分配单位

# cpython解释器 不能实现多线程利用多核


# 锁 ：GIL 全局解释器锁
# GIL锁
        # 全局解释器锁
        # cpython解释器中的机制
        # 导致了在同一个进程中多个线程不能同时利用多核 —— python的多线程只能是并发不能是并行
    # 保证了整个python程序中，只能有一个线程被CPU执行
    # 原因：cpython解释器中特殊的垃圾回收机制
    # GIL锁导致了线程不能并行，可以并发
# 所以使用所线程并不影响高io型的操作
# 只会对高计算型的程序由效率上的影响
# 遇到高计算 ： 多进程 + 多线程
              # 分布式

# cpython pypy jpython iron python

# 遇到IO操作的时候
    # 5亿条cpu指令/s
    # 5-6cpu指令 == 一句python代码
    # 几千万条python代码
# web框架 几乎都是多线程
```

- #### trreading模块

```python
# threading模块
    # 创建线程 ：面向函数 面向对象
    # 线程中的几个方法：
        # 属于线程对象t.start(),t.join()
        # 守护线程t.daemon = True 等待所有的非守护子线程都结束之后才结束
            # 非守护线程不结束，主线程也不结束
            # 主线程结束了，主进程也结束
   * * * 面试题* * * *          # 结束顺序 ：非守护线程结束 -->主线程结束-->主进程结束
                        #-->主进程结束 --> 守护线程也结束
        # threading模块的函数 ：
            # current_thread 在哪个线程中被调用，就返回当前线程的对象
            # 活着的线程，包括主线程
                # enumerate 返回当前活着的线程的对象列表
                # active_count 返回当前或者的线程的个数
        # 测试
            # 进程和线程的效率差，线程的开启、关闭、切换效率更高
            # 线程的数据共享的效果
```

### 2.0 锁   ---终章详解

2.1 线程中为什么要用锁

```python
# 即便是线程 即便有GIL 也会出现数据不安全的问题
    # 1.操作的是全局变量
    # 2.做一下操作
        # += -= *= /+ 先计算再赋值才容易出现数据不安全的问题
        # 包括 lst[0] += 1  dic['key']-=1
 # 加锁会影响程序的执行效率，但是保证了数据的安全
# 锁 - 都可以维护线程之间的数据安全
    # 互斥锁 ：一把锁不能在一个线程中连续acquire，开销小
    # 递归锁 ：一把锁可以连续在一个线程中acquire多次，acquire多少次就release多少次，开销大

    # 死锁现象
        # 在某一些线程中出现陷入阻塞并且永远无法结束阻塞的情况就是死锁现象
        # 出现死锁：
            # 多把锁+交替使用
            # 互斥锁在一个线程中连续acquire
    # 避免死锁
        # 在一个线程中只有一把锁，并且每一次acquire之后都要release

    # += -= *= /= ，多个线程对同一个文件进行写操作
```

2.2 互斥锁

```python
# 互斥锁是锁中的一种:在同一个线程中，不能连续acquire多次
# from threading import Lock
# lock = Lock()
# lock.acquire()
# print('*'*20)
# lock.release()
# lock.acquire()
# print('-'*20)
# lock.release()
```

2.3  死锁现象

```python
# 锁
    # 互斥锁
        # 在一个线程中连续多次acquire会死锁
    # 递归锁
        # 在一个线程中连续多次acquire不会死锁
    # 死锁现象
        # 死锁现象是怎么发生的？
            # 1.有多把锁，一把以上
            # 2.多把锁交替使用
    # 怎么解决
        # 递归锁 —— 将多把互斥锁变成了一把递归锁
            # 快速解决问题
            # 效率差
        # ***递归锁也会发生死锁现象，多把锁交替使用的时候
        # 优化代码逻辑
            # 可以使用互斥锁 解决问题
            # 效率相对好
            # 解决问题的效率相对低
import time
from threading import Thread,Lock
noodle_lock = Lock()
fork_lock = Lock()
def eat1(name,noodle_lock,fork_lock):
    noodle_lock.acquire()
    print('%s抢到面了'%name)
    fork_lock.acquire()
    print('%s抢到叉子了' % name)
    print('%s吃了一口面'%name)
    time.sleep(0.1)
    fork_lock.release()
    print('%s放下叉子了' % name)
    noodle_lock.release()
    print('%s放下面了' % name)

def eat2(name,noodle_lock,fork_lock):
    fork_lock.acquire()
    print('%s抢到叉子了' % name)
    noodle_lock.acquire()
    print('%s抢到面了'%name)
    print('%s吃了一口面'%name)
    time.sleep(0.1)
    noodle_lock.release()
    print('%s放下面了' % name)
    fork_lock.release()
    print('%s放下叉子了' % name)

lst = ['alex','wusir','taibai','yuan']
Thread(target=eat1,args=(lst[0],noodle_lock,fork_lock)).start()
Thread(target=eat2,args=(lst[1],noodle_lock,fork_lock)).start()
Thread(target=eat1,args=(lst[2],noodle_lock,fork_lock)).start()
Thread(target=eat2,args=(lst[3],noodle_lock,fork_lock)).start()
```

2.3.1 互斥锁解决死锁现象

```python
import time
from threading import Lock,Thread
lock = Lock()
def eat1(name):
    lock.acquire()
    print('%s抢到面了'%name)
    print('%s抢到叉子了' % name)
    print('%s吃了一口面'%name)
    time.sleep(0.1)
    print('%s放下叉子了' % name)
    print('%s放下面了' % name)
    lock.release()

def eat2(name):
    lock.acquire()
    print('%s抢到叉子了' % name)
    print('%s抢到面了'%name)
    print('%s吃了一口面'%name)
    time.sleep(0.1)
    print('%s放下面了' % name)
    print('%s放下叉子了' % name)
    lock.release()

lst = ['alex','wusir','taibai','yuan']
Thread(target=eat1,args=(lst[0],)).start()
Thread(target=eat2,args=(lst[1],)).start()
Thread(target=eat1,args=(lst[2],)).start()
Thread(target=eat2,args=(lst[3],)).start()
```

2.4 递归锁

```python
#递归锁解决死锁现象
# 在同一个线程中，可以连续acuqire多次不会被锁住

# 递归锁：
    # 好 ：在同一个进程中多次acquire也不会发生阻塞
    # 不好 ：占用了更多资源
import time
from threading import RLock,Thread
# noodle_lock = RLock()
# fork_lock = RLock()
noodle_lock = fork_lock = RLock()
print(noodle_lock,fork_lock)
def eat1(name,noodle_lock,fork_lock):
    noodle_lock.acquire()
    print('%s抢到面了'%name)
    fork_lock.acquire()
    print('%s抢到叉子了' % name)
    print('%s吃了一口面'%name)
    time.sleep(0.1)
    fork_lock.release()
    print('%s放下叉子了' % name)
    noodle_lock.release()
    print('%s放下面了' % name)

def eat2(name,noodle_lock,fork_lock):
    fork_lock.acquire()
    print('%s抢到叉子了' % name)
    noodle_lock.acquire()
    print('%s抢到面了'%name)
    print('%s吃了一口面'%name)
    time.sleep(0.1)
    noodle_lock.release()
    print('%s放下面了' % name)
    fork_lock.release()
    print('%s放下叉子了' % name)

lst = ['alex','wusir','taibai','yuan']
Thread(target=eat1,args=(lst[0],noodle_lock,fork_lock)).start()
Thread(target=eat2,args=(lst[1],noodle_lock,fork_lock)).start()
Thread(target=eat1,args=(lst[2],noodle_lock,fork_lock)).start()
Thread(target=eat2,args=(lst[3],noodle_lock,fork_lock)).start()
```

### 2.1 队列 from   queue   import   Queue

```python
import queue
# 线程之间的通信 线程安全
from queue import Queue  # 先进先出队列
# q = Queue(5)
# q.put(0)
# q.put(1)
# q.put(2)
# q.put(3)
# q.put(4)
# print('444444')
#
#
# print(q.get())
# print(q.get())
# print(q.get())
# print(q.get())
# print(q.get())

# 使用多线程 实现一个请求网页 并且把网页写到文件中
# 生产者消费者模型来完成

# 5个线程负责请求网页 把结果放在队列里
# 2个线程 负责从队列中获取网页代码 写入文件

from queue import LifoQueue  # 后进先出队列
# last in first out 栈
# lfq = LifoQueue(4)
# lfq.put(1)
# lfq.put(3)
# lfq.put(2)
# print(lfq.get())
# print(lfq.get())
# print(lfq.get())

# 先进先出
    # 写一个server，所有的用户的请求放在队列里
        # 先来先服务的思想
# 后进先出
    # 算法
# 优先级队列
    # 自动的排序
    # 抢票的用户级别 100000 100001
    # 告警级别
# from queue import PriorityQueue
# pq = PriorityQueue()
# pq.put((10,'alex'))
# pq.put((6,'wusir'))
# pq.put((20,'yuan'))
# print(pq.get())
# print(pq.get())
# print(pq.get())
```

### 2.2池

```python
# 池
    # 进程池
    # 线程池
# 为什么要有池？
# 10000
# 池
# 预先的开启固定个数的进程数，当任务来临的时候，直接提交给已经开好的进程
# 让这个进程去执行就可以了
# 节省了进程，线程的开启 关闭 切换都需要时间
# 并且减轻了操作系统调度的负担
# from concurrent.futrues import ThreadPoolExecutor
    # 1.是单独开启线程进程还是池？
        # 如果只是开启一个子线程做一件事情，就可以单独开线程
        # 有大量的任务等待程序去做，要达到一定的并发数，开启线程池
        # 根据你程序的io操作也可以判定是用池还是不用池？
            # socket的server 大量的阻塞io   recv recvfrom socketserver
            # 爬虫的时候 池
    # 2.回调函数add_done_callback
            # 执行完子线程任务之后直接调用对应的回调函数
            # 爬取网页 需要等待数据传输和网络上的响应高IO的 -- 子线程
            # 分析网页 没有什么IO操作 -- 这个操作没必要在子线程完成，交给回调函数
    # 3.ThreadPoolExecutor中的几个常用方法
        # tp = ThreadPoolExecutor(cpu*5)
        # obj = tp.submit(需要在子线程执行的函数名,参数)
        # obj
            # 1.获取返回值 obj.result() 是一个阻塞方法
            # 2.绑定回调函数 obj.add_done_callback(子线程执行完毕之后要执行的代码对应的函数)
        # ret = tp.map(需要在子线程执行的函数名,iterable)
            # 1.迭代ret，总是能得到所有的返回值
        # shutdown
            # tp.shutdown()
```

```python
# concurrent.futures
import os
import time
import random
from concurrent.futures import ProcessPoolExecutor
# submit + shutdown
def func():
    print('start',os.getpid())
    time.sleep(random.randint(1,3))
    print('end', os.getpid())
if __name__ == '__main__':
    p = ProcessPoolExecutor(5)
    for i in range(10):
        p.submit(func)
    p.shutdown()   # 关闭池之后就不能继续提交任务，并且会阻塞，直到已经提交的任务完成
    print('main',os.getpid())
```

- 传参与返回值

```python
# 任务的参数 + 返回值
# def func(i,name):
#     print('start',os.getpid())
#     time.sleep(random.randint(1,3))
#     print('end', os.getpid())
#     return '%s * %s'%(i,os.getpid())
# if __name__ == '__main__':
#     p = ProcessPoolExecutor(5)
#     ret_l = []
#     for i in range(10):
#         ret = p.submit(func,i,'alex')
#         ret_l.append(ret)
#     for ret in ret_l:
#         print('ret-->',ret.result())  # ret.result() 同步阻塞
#     print('main',os.getpid())
```

- 线程池

```python
# 线程池
# from concurrent.futures import ThreadPoolExecutor
# def func(i):
#     print('start', os.getpid())
#     time.sleep(random.randint(1,3))
#     print('end', os.getpid())
#     return '%s * %s'%(i,os.getpid())
# tp = ThreadPoolExecutor(20)
# ret_l = []
# for i in range(10):
#     ret = tp.submit(func,i)
#     ret_l.append(ret)
# tp.shutdown()
# print('main')
# for ret in ret_l:
#     print('------>',ret.result())
```

```python
from concurrent.futures import ThreadPoolExecutor
def func(i):
    print('start', os.getpid())
    time.sleep(random.randint(1,3))
    print('end', os.getpid())
    return '%s * %s'%(i,os.getpid())
tp = ThreadPoolExecutor(20)
ret = tp.map(func,range(20))
for i in ret:
    print(i)
ret_l = []
for i in range(10):
    ret = tp.submit(func,i)
    ret_l.append(ret)
tp.shutdown()
print('main')
```

- 回调函数

```python
# 回调函数
import requests
from concurrent.futures import ThreadPoolExecutor
def get_page(url):
    res = requests.get(url)
    return {'url':url,'content':res.text}

def parserpage(ret):
    dic = ret.result()
    print(dic['url'])
tp = ThreadPoolExecutor(5)
url_lst = [
    'http://www.baidu.com',   # 3
    'http://www.cnblogs.com', # 1
    'http://www.douban.com',  # 1
    'http://www.tencent.com',
    'http://www.cnblogs.com/Eva-J/articles/8306047.html',
    'http://www.cnblogs.com/Eva-J/articles/7206498.html',
]
ret_l = []
for url in url_lst:
    ret = tp.submit(get_page,url)
    ret_l.append(ret)
    ret.add_done_callback(parserpage)
```

```python
# ThreadPoolExcutor
# ProcessPoolExcutor

# 创建一个池子
# tp = ThreadPoolExcutor(池中线程/进程的个数)
# 异步提交任务
# res = tp.submit(函数,参数1，参数2....)
# 获取返回值
# res.result()
# 在异步的执行完所有任务之后，主线程/主进程才开始执行的代码
# tp.shutdown() 阻塞 直到所有的任务都执行完毕
# map方法
# ret = tp.map(func,iterable) 迭代获取iterable中的内容，作为func的参数，让子线程来执行对应的任务
# for i in ret: 每一个都是任务的返回值
# 回调函数
# -------------------------------------------------------------------
# res.add_done_callback(函数名)
# 要在ret对应的任务执行完毕之后，直接继续执行add_done_callback绑定的函数中的内容，并且ret的结果会作为参数返回给绑定的函数

# 5个进程
# 20个线程
# 5*20 = 100个并发
```

### 2.3 携程

```python
# 只要是线程里的代码 就都被CPU执行就行
# 线程是由 操作系统 调度,由操作系统负责切换的
# 协程:
    # 用户级别的,由我们自己写的python代码来控制切换的
    # 是操作系统不可见的
# 在Cpython解释器下 - 协程和线程都不能利用多核,都是在一个CPU上轮流执行
    # 由于多线程本身就不能利用多核
    # 所以即便是开启了多个线程也只能轮流在一个CPU上执行
    # 协程如果把所有任务的IO操作都规避掉,只剩下需要使用CPU的操作
    # 就意味着协程就可以做到题高CPU利用率的效果
# 多线程和协程
    # 线程 切换需要操作系统,开销大,操作系统不可控,给操作系统的压力大
        # 操作系统对IO操作的感知更加灵敏
    # 协程 切换需要python代码,开销小,用户操作可控,完全不会增加操作系统的压力
        # 用户级别能够对IO操作的感知比较低
```

##### 1. 携程之间的切换 greenlet 模块

```python
# 协程 :能够在一个线程下的多个任务之间来回切换,那么每一个任务都是一个协程
# 两种切换方式
    # 原生python完成   yield  asyncio
    # C语言完成的python模块  greenlet  gevent

# greenlet
# import time
# from  greenlet import greenlet
#
# def eat():
#     print('wusir is eating')
#     time.sleep(0.5)
#     g2.switch()
#     print('wusir finished eat')
#
# def sleep():
#     print('小马哥 is sleeping')
#     time.sleep(0.5)
#     print('小马哥 finished sleep')
#     g1.switch()
#
# g1 = greenlet(eat)
# g2 = greenlet(sleep)
# g1.switch()
```

##### 2. gevent 模块

```python
import time
print('-->',time.sleep)
import gevent
from gevent import monkey
monkey.patch_all()
def eat():
    print('wusir is eating')
    print('in eat: ',time.sleep)
    time.sleep(1)
    print('wusir finished eat')

def sleep():
    print('小马哥 is sleeping')
    time.sleep(1)
    print('小马哥 finished sleep')

g1 = gevent.spawn(eat)  # 创造一个协程任务
g2 = gevent.spawn(sleep)  # 创造一个协程任务
g1.join()   # 阻塞 直到g1任务完成为止
g2.join()   # 阻塞 直到g1任务完成为止
```



```python
import time
import gevent
from gevent import monkey
monkey.patch_all()
def eat():
    print('wusir is eating')
    time.sleep(1)
    print('wusir finished eat')

def sleep():
    print('小马哥 is sleeping')
    time.sleep(1)
    print('小马哥 finished sleep')

# g1 = gevent.spawn(eat)  # 创造一个协程任务
# g3 = gevent.spawn(eat)  # 创造一个协程任务
# g2 = gevent.spawn(sleep)  # 创造一个协程任务
# # g1.join()   # 阻塞 直到g1任务完成为止
# # g2.join()   # 阻塞 直到g1任务完成为止
# gevent.joinall([g1,g2,g3])
g_l = []
for i in range(10):
    g = gevent.spawn(eat)
    g_l.append(g)
gevent.joinall(g_l)
```

```python
import time
import gevent
from gevent import monkey
monkey.patch_all()
def eat():
    print('wusir is eating')
    time.sleep(1)
    print('wusir finished eat')
    return 'wusir***'

def sleep():
    print('小马哥 is sleeping')
    time.sleep(1)
    print('小马哥 finished sleep')
    return '小马哥666'

g1 = gevent.spawn(eat)
g2 = gevent.spawn(sleep)
gevent.joinall([g1,g2])
print(g1.value)
print(g2.value)
```

##### 3. asyncio 模块 实现携程，最接近底层

3.1 起一个任务

```python
import asyncio

# 起一个任务
# async def demo():   # 协程方法
#     print('start')
#     await asyncio.sleep(1)  # 阻塞
#     print('end')
#
# loop = asyncio.get_event_loop()  # 创建一个事件循环
# loop.run_until_complete(demo())  # 把demo任务丢到事件循环中去执行
```

3.2 起多个任务，并没有返回值方法

```python
# 启动多个任务,并且没有返回值
# async def demo():   # 协程方法
#     print('start')
#     await asyncio.sleep(1)  # 阻塞
#     print('end')
#
# loop = asyncio.get_event_loop()  # 创建一个事件循环
# wait_obj = asyncio.wait([demo(),demo(),demo()])
# loop.run_until_complete(wait_obj)
```

3.3 多个任务，有返回值

```python
async def demo():   # 协程方法
    print('start')
    await asyncio.sleep(1)  # 阻塞
    print('end')
    return 123

loop = asyncio.get_event_loop()
t1 = loop.create_task(demo())    #时间循环的返回值
t2 = loop.create_task(demo())
tasks = [t1,t2]                
wait_obj = asyncio.wait(tasks)      #把这俩返回值的事件组合，以列表的形式扔进 时间循环里
loop.run_until_complete(wait_obj)   #直到事件循环结束得到返回值
for t in tasks:
    print(t.result())    #t.result() 得到返回值
```

3.5 谁先回来，先取谁的结果

```python
# 谁先回来先取谁的结果
# import asyncio
# async def demo(i):   # 协程方法
#     print('start')
#     await asyncio.sleep(10-i)  # 阻塞
#     print('end')
#     return i,123
#
# async def main():
#     task_l = []
#     for i in range(10):
#         task = asyncio.ensure_future(demo(i))
#         task_l.append(task)
#     for ret in asyncio.as_completed(task_l):
#         res = await ret
#         print(res)
#
# loop = asyncio.get_event_loop()
# loop.run_until_complete(main())
```

```python
# import asyncio
#
# async def get_url():
#     reader,writer = await asyncio.open_connection('www.baidu.com',80)
#     writer.write(b'GET / HTTP/1.1\r\nHOST:www.baidu.com\r\nConnection:close\r\n\r\n')
#     all_lines = []
#     async for line in reader:
#         data = line.decode()
#         all_lines.append(data)
#     html = '\n'.join(all_lines)
#     return html
#
# async def main():
#     tasks = []
#     for url in range(20):
#         tasks.append(asyncio.ensure_future(get_url()))
#     for res in asyncio.as_completed(tasks):
#         result = await res
#         print(result)
#
#
# if __name__ == '__main__':
#     loop = asyncio.get_event_loop()
#     loop.run_until_complete(main())  # 处理一个任务
```

```python
# python原生的底层的协程模块
    # 爬虫 webserver框架
    # 题高网络编程的效率和并发效果
# 语法
    # await 阻塞 协程函数这里要切换出去，还能保证一会儿再切回来
    # await 必须写在async函数里，async函数是协程函数
    # loop 事件循环
    # 所有的协程的执行 调度 都离不开这个loop
```

## 第十章 数据库

### 1.0 数据库的概念

```python
# 数据库
    # 很多功能如果只是通过操作文件来改变数据是非常繁琐的
        # 程序员需要做很多事情
    # 对于多台机器或者多个进程操作用一份数据
        # 程序员自己解决并发和安全问题比较麻烦
    # 自己处理一些数据备份，容错的措施

# C/S架构的 操作数据文件的一个管理软件
    # 1.帮助我们解决并发问题
    # 2.能够帮助我们用更简单更快速的方式完成数据的增删改查
    # 3.能够给我们提供一些容错、高可用的机制
    # 4.权限的认证

# 数据库管理系统 —— 专门用来管理数据文件，帮助用户更简洁的操作数据的软件
    # DBMS
# 数据 data
# 文件
# 文件夹 -- 数据库database db
# 数据库管理员 —— DBA
```

### 1.1 数据库的分类

```python
 #数据库管理系统
    # 关系型数据库
        # sql server
        # oracle 收费、比较严谨、安全性比较高
            # 国企 事业单位
            # 银行 金融行业
        # mysql  开源的
            # 小公司
            # 互联网公司
        # sqllite
    # 非关系型数据库
        # redis
        # mongodb
```



```python
# 安装mysql遇到的问题
    # 操作系统的问题
        # 缺失dll文件
    # 安装路径
        # 不能有空格
        # 不能有中文
        # 不能带着有转义的特殊字符开头的文件夹名
    # 安装之后发现配置有问题
        # 再修改配置往往不能生效
    # 卸载之后重装
        # mysqld remove
        # 把所有的配置、环境变量修改到正确的样子
        # 重启计算机 - 清空注册表
        # 再重新安装

# mysql的CS架构
    # mysqld install  安装数据库服务
    # net start mysql 启动数据库的server端
        # 停止server net stop mysql
    # 客户端可以是python代码也可以是一个程序
        # mysql.exe是一个客户端
        # mysql -u用户名 -p密码
# mysql中的用户和权限
    # 在安装数据库之后，有一个最高权限的用户root
    # mysql 192.168.12.87 eva/123
        # mysql -h192.168.12.87 -uroot -p123

# 我们的mysql客户端不仅可以连接本地的数据库
# 也可以连接网络上的某一个数据库的server端

# mysql>select user();
    # 查看当前用户是谁
# mysql>set password = password('密码')
    # 设置密码
# 创建用户
# create user 's21'@'192.168.12.%' identified by '123';
# mysql -us21 -p123 -h192.168.12.87

# 授权
    # grant all on day37.* to 's21'@'192.168.12.%';
    # 授权并创建用户
    # grant all on day37.* to 'alex'@'%' identified by '123';

# 查看文件夹
    # show databases;
# 创建文件夹
    # create database day37;

# 库 表 数据
    # 创建库、创建表  DDL数据库定义语言
    # 存数据，删除数据,修改数据,查看  DML数据库操纵语句
    # grant/revoke  DCL控制权限
# 库
    # create database 数据库名;  # 创建库
    # show databases; # 查看当前有多少个数据库
    # select database();# 查看当前使用的数据库
    # use 数据库的名字; # 切换到这个数据库(文件夹)下
# 表操作
    # 查看当前文件夹中有多少张表
        # show tables;
    # 创建表
        # create table student(id int,name char(4));
    # 删除表
        # drop table student;
    # 查看表结构
        # desc 表名;

# 操作表中的数据
    # 数据的增加
        # insert into student values (1,'alex');
        # insert into student values (2,'wusir');
    # 数据的查看
        # select * from student;
    # 修改数据
        # update 表 set 字段名=值
        # update student set name = 'yuan';
        # update student set name = 'wusir' where id=2;
    # 删除数据
        # delete from 表名字;
        # delete from student where id=1;
```

##### 3.0 总结

```python
# 数据库
    # 关系型数据库 ：mysql oracle sqlserver sqllite
    # 非关系型数据库 ：mongodb redis
# sql ：
    # ddl 定义语言
        # 创建用户
            # create user '用户名'@'%'  表示网络可以通讯的所有ip地址都可以使用这个用户名
            # create user '用户名'@'192.168.12.%' 表示192.168.12.0网段的用户可以使用这个用户名
            # create user '用户名'@'192.168.12.87' 表示只有一个ip地址可以使用这个用户名
        # 创建库
            # create database day38;
        # 创建表
            # create table 表名(字段名 数据类型(长度)，字段名 数据类型(长度)，)
    # dml 操作语言
            # 数据的
            # 增 insert into
            # 删 delete from
            # 改 update
            # 查 select
                # select user(); 查看当前用户
                # select database(); 查看当前所在的数据库
            # show
                # show databases:  查看当前的数据库有哪些
                # show tables；查看当前的库中有哪些表
            # desc 表名；查看表结构
            # use 库名；切换到这个库下
    # dcl 控制语言
        # 给用户授权
        # grant select on 库名.* to '用户名'@'ip地址/段' identified by '密码'
        # grant select,insert
        # grant all
```

##### 4.0创建方式 存储 —— 存储引擎

```python
# 表的存储方式
    # 存储方式1：MyISAM 5.5以下 默认存储方式
        # 存储的文件个数：表结构、表中的数据、索引
        # 支持表级锁
        # 不支持行级锁 不支持事务 不支持外键
    # 存储方式2：InnoDB 5.6以上 默认存储方式
        # 存储的文件个数：表结构、表中的数据
        # 支持行级锁、支持表锁
        # 支持事务
        # 支持外键
    # 存储方式3： MEMORY 内存
        # 存储的文件个数：表结构
        # 优势 ：增删改查都很快
        # 劣势 ：重启数据消失、容量有限
# 索引 - 数据库的目录


# 查看配置项:
    # show variables like '%engine%';

# 创建表
# create table t1 (id int,name char(4));
# create table t2 (id int,name char(4)) engine=myisam;
# create table t3 (id int,name char(4)) engine=memory;

# 查看表的结构
    # show create table 表名; 能够看到和这张表相关的所有信息
    # desc 表名               只能查看表的字段的基础信息
        # describe 表名


# 用什么数据库 ： mysql
# 版本是什么 ：5.6
# 都用这个版本么 ：不是都用这个版本
# 存储引擎 ：innodb
# 为什么要用这个存储引擎：
    # 支持事务 支持外键 支持行级锁
                         # 能够更好的处理并发的修改问题
```

```
# 
# 表的约束

# 创建表
# 查看表结构
# 修改表
# 删除表
```

##### 5.0 基础数据类型

5.1数字

```python
# 整数 int
# create table t4 (id1 int(4),id2 int(11));
    # int默认是有符号的
    # 它能表示的数字的范围不被宽度约束
    # 它只能约束数字的显示宽度
# create table t5 (id1 int unsigned,id2 int);

# 小数  float
# create table t6 (f1 float(5,2),d1 double(5,2));

# create table t7 (f1 float,d1 double);
# create table t8 (d1 decimal,d2 decimal(25,20));
```

5.2 字符串

```python
# char(15)  定长的单位
    # alex  alex
    # alex
# varchar(15) 变长的单位
    # alex  alex4

# 哪一个存储方式好？
    # varchar ：节省空间、存取效率相对低
    # char ：浪费空间，存取效率相对高 长度变化小的

# 手机号码、身份证号  char
# 用户名、密码  char
# 评论  微博 说说 微信状态 varchar

# create table t11 (name1 char(5),name2 varchar(5));
```

5.3 时间和日期

```python
# 类型
    # year 年
    # date 年月日
    # time 时分秒
    # datetime、timestamp 年月日时分秒

# create table t9(
# y year,d date,
# dt datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
# ts timestamp);
```

5.4 其他

```python
# create table t12(
# name char(12),
# gender ENUM('male','female'),
# hobby set('抽烟','喝酒','烫头','洗脚')
# );

# insert into t12 values('alex','不详','抽烟,喝酒,洗脚,洗脚,按摩');
```

### 1.2 约束

```python
1.not null 某一个字段不能为空
2.default   设置默认值
3.unique   设置某一个字段不能重复
4.联合唯一：
	create table t4(id int,ip char(15),
	server char(10),port int,unique(ip,port)
	)
5. 自增：
	auto_increment
	自增字段 必须是数字，且 必须是唯一的
6. primary key 主键 
	#一张表只能设置一个主键
	#规范：一张表最好设置一个主键
	#约束这个字段 非空（not null）且 唯一 （unique）
	3，若你设置主键，你指定的第一个非空且为1的为主键
7. 联合主键 


8.外键  foreign key 涉及到两张表
员工表
id age gender  salary hire_data postname postid post_comment post_email post_phone
create table staff(id int primary key, age int , gender  salary enum('male','female'),hire_data data, postname postid int);
```

```python
# 约束
    # unsigned  设置某一个数字无符号
    # not null 某一个字段不能为空
    # default 给某个字段设置默认值
    # unique  设置某一个字段不能重复
        # 联合唯一
    # auto_increment 设置某一个int类型的字段 自动增加
        # auto_increment自带not null效果
        # 设置条件 int  unique
    # primary key    设置某一个字段非空且不能重复
        # 约束力相当于 not null + unique
        # 一张表只能由一个主键
    # foreign key    外键
        # references
        # 级联删除和更新

# not null
# default
# create table t2(
#   id int not null,
#   name char(12) not null,
#   age int default 18,
#   gender enum('male','female') not null default 'male'
# )

# unique 设置某一个字段不能重复
# create table t3(
#     id int unique,
#     username char(12) unique,
#     password char(18)
# );

# 联合唯一
# create table t4(
#     id int,
#     ip char(15),
#     server char(10),
#     port int,
#     unique(ip,port)
# );

# 自增
    # auto_increment
    # 自增字段 必须是数字 且 必须是唯一的
# create table t5(
#     id int unique auto_increment,
#     username char(10),
#     password char(18)
# )
# insert into t5(username,password) values('alex','alex3714')

# primary key  主键
    # 一张表只能设置一个主键
    # 一张表最好设置一个主键
    # 约束这个字段 非空（not null） 且 唯一（unique）

# create table t6(
#     id int not null unique,     # 你指定的第一个非空且唯一的字段会被定义成主键
#     name char(12) not null unique
# )

# create table t7(
#     id int primary key,     # 你指定的第一个非空且唯一的字段会被定义成主键
#     name char(12) not null unique
# )

# 联合主键
# create table t4(
#     id int,
#     ip char(15),
#     server char(10),
#     port int,
#     primary key(ip,port)
# );

# 外键 foreign key 涉及到两张表
# 员工表
# create table staff(
# id  int primary key auto_increment,
# age int,
# gender  enum('male','female'),
# salary  float(8,2),
# hire_date date,
# post_id int,
# foreign key(post_id) references post(pid)
# )


# 部门表
#  pid postname post_comment post_phone
# create table post(
#     pid  int  primary key,
#     postname  char(10) not null unique,
#     comment   varchar(255),
#     phone_num  char(11)
# )

# update post set pid=2 where pid = 1;
# delete from post where pid = 1;

# 级联删除和级联更新
# create table staff2(
# id  int primary key auto_increment,
# age int,
# gender  enum('male','female'),
# salary  float(8,2),
# hire_date date,
# post_id int,
# foreign key(post_id) references post(pid) on update cascade on delete set null
# )
```

### 1.3 修改表结构

```python
# 创建项目之前
# 项目开发、运行过程中

# alter table 表名 add 添加字段
# alter table 表名 drop 删除字段
# alter table 表名 modify 修改已经存在的字段  的类型 宽度 约束
# alter table 表名 change 修改已经存在的字段  的类型 宽度 约束 和 字段名字

# alter table 表名 add 字段名 数据类型(宽度) 约束 first/after name
# alter table 表名 drop 字段名
# alter table 表名 modify name varchar(12) not null
# alter table 表名 change name new_name varchar(12) not null

# id name age
# alter table 表名 modify age int not null after id;
# alter table 表名 modify age int not null first;
```

### 1.4 表关系

```python
# 两张表中的数据之间的关系
# 多对一   foreign key  永远是在多的那张表中设置外键
    # 多个学生都是同一个班级的
    # 学生表 关联 班级表
    # 学生是多    班级是一
# 一对一   foreign key +unique  # 后出现的后一张表中的数据 作为外键，并且要约束这个外键是唯一的
    # 客户关系表 ： 手机号码  招生老师  上次联系的时间  备注信息
    # 学生表 ：姓名 入学日期 缴费日期 结业
# 多对多  产生第三张表，把两个关联关系的字段作为第三张表的外键
    # 书
    # 作者

    # 出版社
    # 书
```

### 1.5 数据操作

```python
# 增加 insert
# 删除 delete
# 修改 update
# 查询 select

# 增加 insert
# insert into 表名 values (值....)
    # 所有的在这个表中的字段都需要按照顺序被填写在这里
# insert into 表名(字段名，字段名。。。) values (值....)
    # 所有在字段位置填写了名字的字段和后面的值必须是一一对应
# insert into 表名(字段名，字段名。。。) values (值....),(值....),(值....)
    # 所有在字段位置填写了名字的字段和后面的值必须是一一对应

# value单数            values复数
# 一次性写入一行数据   一次性写入多行数据

# t1 id,name,age
# insert into t1 value (1,'alex',83)
# insert into t1 values (1,'alex',83),(2,'wusir',74)

# insert into t1(name,age) value ('alex',83)
# insert into t1(name,age) values ('alex',83),('wusir',74)

# 第一个角度
    # 写入一行内容还是写入多行
    # insert into 表名 values (值....)
    # insert into 表名 values (值....)，(值....)，(值....)

# 第二个角度
    # 是把这一行所有的内容都写入
    # insert into 表名 values (值....)
    # 指定字段写入
    # insert into 表名(字段1，字段2) values (值1，值2)


# 删除 delete
    # delete from 表 where 条件;

# 更新 update
    # update 表 set 字段=新的值 where 条件；

# 查询
    # select语句
        # select * from 表
        # select 字段,字段.. from 表
        # select distinct 字段,字段.. from 表  # 按照查出来的字段去重
        # select 字段*5 from 表  # 按照查出来的字段去重
        # select 字段  as 新名字,字段 as 新名字 from 表  # 按照查出来的字段去重
        # select 字段 新名字 from 表  # 按照查出来的字段去重
```

### 1.6 筛选 

1.where 语句 范围筛选

```python
1 select * from where 字段 = ‘’
2 select * from where 字段 in （11,12,13,45,5,6）;
模糊范围
一个数值区间，1W-2W 
select * from employee where salary between 10000 and 20000
字符串模糊查询   %   _  正则
select * from employee where emp_name like '程%'； 陪陪任意长度任意内容
select * from employee where emp_name like '程_'； 匹配一个字符长度任意内容
什么什么结尾
select * from employee where emp_name like '%程'
含有的
select * from employee where emp_name like '%程%'；
正则匹配  regexp
	select *  from  biao where 字段 regexp 正则表达式

```

逻辑运算 ---条件的拼接

```python
与 and
或 or
非 not
	2 select * from where 字段 not in （11,12,13,45,5,6）;	

```

### 1.7 分组聚合

```n
1 分组  group by
会吧在group by 后面的这个字段，也就是post 字段中的每一个不同的项都保留下来
并且会把值是这一项的所有行归为一组
select * from employee group by post;
```

```python
聚合  把很多航的用一个字段进行一些统计，最终得到一个结果
count（字段） 统计这个字段有多少项
sum（字段）  统计这个字段对应的数值的和
avg（字段）      拿到的平均值
min(字段)    最下
max（）

```

```python
select count(*) from employee group by post
select post,count(*) from employee group by post
```

```python
总是根据会重复的项来进行分组
分组
```

### 1.8 having 语句       过滤语句    

```python
过滤的是组  总是跟group 一起用           where 过滤的是行
执行顺序重视先执行where 在执行group by 分组
所以相关先分组 之后再根据分组做某些条件筛选的时候 where 都用不上
只能用having 来完成
# 部门人数大于3的部门
# select post from employee group by post having count(*) > 3

# 1.执行顺序 总是先执行where 再执行group by分组
#   所以相关先分组 之后再根据分组做某些条件筛选的时候 where都用不上
# 2.只能用having来完成

# 平均薪资大于10000的部门
# select post from employee group by post having avg(salary) > 10000


# select * from employee having age>18

# order by
    # order by 某一个字段 asc;  默认是升序asc 从小到大
    # order by 某一个字段 desc;  指定降序排列desc 从大到小
    # order by 第一个字段 asc,第二个字段 desc;
        # 指定先根据第一个字段升序排列，在第一个字段相同的情况下，再根据第二个字段排列

# limit
    # 取前n个  limit n   ==  limit 0,n
        # 考试成绩的前三名
        # 入职时间最晚的前三个
    # 分页    limit m,n   从m+1开始取n个
    # 员工展示的网页
        # 18个员工
        # 每一页展示5个员工
    # limit n offset m == limit m,n  从m+1开始取n个

```

```python
# 分组聚合
    # 求各个部门的人数
    # select count(*) from employee group by post
    # 求公司里 男生 和女生的人数
    # select count(id) from employee group by sex
    # 求各部门的平均薪资
    # 求各部门的平均年龄
    # 求各部门年龄最小的
        # select post,min(age) from employee group by post
    # 求各部门年龄最大的
    # 求各部门薪资最高的
    # 求各部门薪资最低的
    # 求最晚入职的
    # 求最早入职的
    # 求各部门最晚入职的
    # 求各部门最早入职的

# 求部门的最高薪资或者求公司的最高薪资都可以通过聚合函数取到
# 但是要得到对应的人，就必须通过多表查询

# 总是根据会重复的项来进行分组
# 分组总是和聚合函数一起用 最大 最小 平均 求和 有多少项
```

order by 

```python
ord by  某个字段 asc  默认是升序  从小到大
ord by 字段 desc  指定降序排列 desc  从大到小
order by  第一个字段asc 第二个字段desc 指定先根据第一个字段排列 一样的话再跟第二个字段排序
```

limit

```python
取前 n个 limit n = limit 0，n
分页功能 limit m,n  从m+1 开始取n个
员工展示的网页

limit n offset m ==limit m,n  从m+1 开始取n个
```

总结

```python
# select distinct 需要显示的列 from 表
#                             where 条件
#                             group by 分组
#                             having 过滤组条件
#                             order by 排序
#                             limit 前n 条
```

### 1.9 pymysql

```python
import pymysql
#
# conn = pymysql.connect(host='127.0.0.1', user='root', password="123",
#                  database='day40')
# cur = conn.cursor()   # 数据库操作符 游标
# # cur.execute('insert into employee(emp_name,sex,age,hire_date) '
# #             'values ("郭凯丰","male",40,20190808)')
# # cur.execute('delete from employee where id = 18')
# conn.commit()
# conn.close()


# conn = pymysql.connect(host='127.0.0.1', user='root', password="123",
#                  database='day40')
# cur = conn.cursor(pymysql.cursors.DictCursor)   # 数据库操作符 游标
# cur.execute('select * from employee '
#             'where id > 10')
# ret = cur.fetchone()
# print(ret['emp_name'])
# # ret = cur.fetchmany(5)
# ret = cur.fetchall()
# print(ret)
# conn.close()

# select * from employee where id > 10


# sql注入风险
```



### 2.0 索引

```python
#索引的重要性 （提高查询的速度）
	#读写比例 10:1
    #读的速度就至关重要了
#索引的原理
	#block 磁盘预读原理
    	# for line in f
    	#一个 block  linuex  为 4096字节
     #读硬盘的IO操作的事件非常的长，比CPU执行指令的时间长很多
    # 尽量的减少IO次数 才是读写数据的主要解决的问题
```

### 

### 2.1 数据库的存储方式

```python
什么是索引 -- 目录
    # 就是建立起的一个在存储表阶段
    # 就有的一个存储结构能在查询的时候加速
# 索引的重要性
    # 读写比例 ： 10：1
    # 读（查询）的速度就至关重要了
# 索引的原理
    # block 磁盘预读原理
        # for line in f
        # 4096个字节
    # 读硬盘的io操作的时间非常的长，比CPU执行指令的时间长很多
    # 尽量的减少IO次数才是读写数据的主要要解决的问题

    # 数据库的存储方式
        # 新的数据结构 —— 树
        # 平衡树 balance tree - b树
        # 在b树的基础上进行了改良 - b+树
            # 1.分支节点和根节点都不再存储实际的数据了
                # 让分支和根节点能存储更多的索引的信息
                # 就降低了树的高度
                # 所有的实际数据都存储在叶子节点中
            # 2.在叶子节点之间加入了双向的链式结构
                # 方便在查询中的范围条件
        # mysql当中所有的b+树索引的高度都基本控制在3层
            # 1.io操作的次数非常稳定
            # 2.有利于通过范围查询
        # 什么会影响索引的效率 —— 树的高度
            # 1.对哪一列创建索引，选择尽量短的列做索引
            # 2.对区分度高的列建索引，重复率超过了10%那么不适合创建索引

# 聚集索引和辅助索引
    # 在innodb中 聚集索引和辅助索引并存的
        # 聚集索引 - 主键 更快
            # 数据直接存储在树结构的叶子节点
        # 辅助索引 - 除了主键之外所有的索引都是辅助索引 稍慢
            # 数据不直接存储在树中
    # 在myisam中 只有辅助索引，没有聚集索引

# 索引的种类
    # primary key 主键 聚集索引  约束的作用：非空 + 唯一
        # 联合主键
    # unique 自带索引 辅助索引   约束的作用：唯一
        # 联合唯一
    # index  辅助索引            没有约束作用
        # 联合索引
# 看一下如何创建索引、创建索引之后的变化
    # create index 索引名字 on 表(字段)
    # DROP INDEX 索引名 ON 表名字;
# 索引是如何发挥作用的
    # select * from 表 where id = xxxxx
        # 在id字段没有索引的时候，效率低
        # 在id字段有索引的之后，效率高

 
```

索引不生效的原因

  1. 要查询的数据的范围大

     ```python
     索引不生效的原因
         # 要查询的数据的范围大
             # > < >= <= !=
             # between and
                 # select * from 表 order by age limit 0，5
                 # select * from 表 where id between 1000000 and 1000005;
             # like
                 # 结果的范围大 索引不生效
                 # 如果 abc% 索引生效，%abc索引就不生效
         # 如果一列内容的区分度不高，索引也不生效
             # name列
         # 索引列不能在条件中参与计算
             # select * from s1 where id*10 = 1000000;  索引不生效
         # 对两列内容进行条件查询
             # and and条件两端的内容，优先选择一个有索引的，并且树形结构更好的，来进行查询
                 # 两个条件都成立才能完成where条件，先完成范围小的缩小后面条件的压力
                 # select * from s1 where id =1000000 and email = 'eva1000000@oldboy';
             # or or条件的，不会进行优化，只是根据条件从左到右依次筛选
                 # 条件中带有or的要想命中索引，这些条件中所有的列都是索引列
                 # select * from s1 where id =1000000 or email = 'eva1000000@oldboy';
         # 联合索引  # create index ind_mix on s1(id,name,email);
             # select * from s1 where id =1000000 and email = 'eva1000000@oldboy';
             # 在联合索引中如果使用了or条件索引就不能生效
                 # select * from s1 where id =1000000 or email = 'eva1000000@oldboy';
             # 最左前缀原则 ：在联合索引中，条件必须含有在创建索引的时候的第一个索引列
                 # select * from s1 where id =1000000;    能命中索引
                 # select * from s1 where email = 'eva1000000@oldboy';  不能命中索引
                 # (a,b,c,d)
                     # a,b
                     # a,c
                     # a
                     # a,d
                     # a,b,d
                     # a,c,d
                     # a,b,c,d
             # 在整个条件中，从开始出现模糊匹配的那一刻，索引就失效了
                 # select * from s1 where id >1000000 and email = 'eva1000001@oldboy';
                 # select * from s1 where id =1000000 and email like 'eva%';
     
     # 什么时候用联合索引
         # 只对 a 对abc 条件进行索引
         # 而不会对b，对c进行单列的索引
     
     # 单列索引
         # 选择一个区分度高的列建立索引，条件中的列不要参与计算，条件的范围尽量小，使用and作为条件的连接符
     # 使用or来连接多个条件
         # 在满上上述条件的基础上
         # 对or相关的所有列分别创建索引
     
     # 覆盖索引
         # 如果我们使用索引作为条件查询，查询完毕之后，不需要回表查，覆盖索引
         # explain select id from s1 where id = 1000000;
         # explain select count(id) from s1 where id > 1000000;
     # 合并索引
         # 对两个字段分别创建索引，由于sql的条件让两个索引同时生效了，那么这个时候这两个索引就成为了合并索引
     # 执行计划 : 如果你想在执行sql之前就知道sql语句的执行情况，那么可以使用执行计划
         # 情况1：
             # 30000000条数据
                 # sql 20s
                 # explain sql   --> 并不会真正的执行sql，而是会给你列出一个执行计划
         # 情况2：
             # 20条数据 --> 30000000
                 # explain sql
     
     # 原理和概念
         # b树
         # b+树
         # 聚集索引 - innodb
         # 辅助索引 - innodb myisam
     # SQL索引的创建（单个、联合）、删除
     # 索引的命中：范围，条件的字段是否参与计算(不能用函数)，列的区分度(长度)，条件and/or，联合索引的最左前缀问题
     # 一些名词
         # 覆盖索引
         # 合并索引
     # explain执行计划
     # 建表、使用sql语句的时候注意的
         # char 代替 varchar
         # 连表 代替 子查询
         # 创建表的时候 固定长度的字段放在前面
     ```

### 2.0.1  数据备份和事物

```python
# mysqldump -uroot -p123  day40 > D:\code\s21day41\day40.sql
# mysqldump -uroot -p123 --databases new_db > D:\code\s21day41\db.sql


# begin;  # 开启事务
# select * from emp where id = 1 for update;  # 查询id值，for update添加行锁；
# update emp set salary=10000 where id = 1; # 完成更新
# commit; # 提交事务
```



### 2.2 sql注入

```python
# create table userinfo(
# id int primary key auto_increment,
# name char(12) unique not null,
# password char(18) not null
# )
#
# insert into userinfo(name,password) values('alex','alex3714')

# 输入用户
# 输入密码
#
# 用户名和密码到数据库里查询数据
# 如果能查到数据 说明用户名和密码正确
# 如果查不到，说明用户名和密码不对
# username = input('user >>>')
# password = input('passwd >>>')
# sql = "select * from userinfo where name = '%s' and password = '%s'"%(username,password)
# print(sql)
# -- 注释掉--之后的sql语句
# select * from userinfo where name = 'alex' ;-- and password = '792164987034';
# select * from userinfo where name = 219879 or 1=1 ;-- and password = 792164987034;
# select * from userinfo where name = '219879' or 1=1 ;-- and password = '792164987034';

import pymysql

conn = pymysql.connect(host = '127.0.0.1',user = 'root',
                       password = '123',database='day41')
cur = conn.cursor()
username = input('user >>>')
password = input('passwd >>>')
sql = "select * from userinfo where name = %s and password = %s"
cur.execute(sql,(username,password))
print(cur.fetchone())
cur.close()
conn.close()
```





## 第十一章 前端开发

```html
<!DOCTYPE html>  #开头的这一行，就是文档声明头，DocType Declaration，简称DTD。此标签可告知浏览器文档使用					#哪种 HTML 或 XHTML 规范
<html>
    <head>
        <!--基本的网站元信息-->
        <meta charset="UTF-8">
        <title>MJJ</title>
        <link rel="stylesheet" href="css/index.css">
        <style>
            body{
                background-color: #0e8b60;
            }
        </style>
        <script src="js/index.js">

        </script>
    </head>
    <body>
       wusir
    </body>
</html>
```

```python
第一行注释附加内容
XHTML：Extensible Hypertext Markup Language，可扩展超文本标注语言。

XHTML的主要目的是为了取代HTML，也可以理解为HTML的升级版。

HTML的标记书写很不规范，会造成其它的设备(ipad、手机、电视等)无法正常显示。

XHTML与HTML4.0的标记基本上一样。

XHTML是严格的、纯净的HTML
```

1. 标题标签<h1> --<h6>  

```python
标题使用<h1>至<h6>标签进行定义。<h1>定义最大的标题，<h6>定义最小的标题。具有align属性，属性值可以是：left、center、right

<h6 align="center">wusirsb</h6>  居中

```

2.字体标签 <font>  (已废弃)

```python
1 color="红色"或color="#ff00cc"或color="new rgb(0,0,255)"：设置字体颜色。设置方式：单词   #ff00cc   rgb(0,0,255)
2 size：设置字体大小。 取值范围只能是：1至7。取值时，如果取值大于7那就按照7来算，如果取值小于1那就按照1来算。如果想要更大的字体，那就只能通过css样式来解决
3 face="微软雅黑"：设置字体类型。注意在写字体时，“微软雅黑”这个字不能写错。

例子：
<font face="微软雅黑" color="green" size="8">字体</font>
```

3. <b>或<strong> 已废弃  

```python
#加粗
小马哥
<b>小马哥</b>
<strong>小马哥</strong>
```

4. 下划线标记<u>中划线标记<s> (已废弃)

```python
<u>小马哥</u>
<s>小马哥</s>
```

5. 斜体标记<i>或<em> (已废弃)

```python
<i>小马哥</i>
<em>小马哥</em>
```

2. ### 特殊符号

```python
&nbsp;：空格 （non-breaking spacing，不断打空格）
&lt;：小于号（less than）
&gt;：大于号（greater than）
&amp;：符号&
&quot;：双引号
&apos;：单引号
&copy;：版权©
&trade;：商标™
```

3. ### 排版标签

3.1  段落标签 <p>

```python
<p>这是一个段落</p>
<p align="center">这是另一个段落</p>
```

3.2 块级标签 <div> 和<span> 

```python
div：把标签中的内容作为一个块儿来对待(division)。必须单独占据一行。

div标签的属性：

align="属性值"：设置块儿的位置。属性值可选择：left、right、 center
<span>和<div>唯一的区别在于：<span>是不换行的，而<div>是换行的。

如果单独在网页中插入这两个元素，不会对页面产生任何的影响。这两个元素是专门为定义CSS样式而生的。或者说，DIV+CSS来实现各种样式。
```



3.3  水平线标签<hr>  换行标签<dr> (都已废弃)

3.4 内容居中标签<center>

```python
此时center代表是一个标签，而不是一个属性值了。只要是在这个标签里面的内容，都会居于浏览器的中间
<center>
       <p>小马哥</p>
</center>
```

3.5 预定义(预格式化)  标签：<pre>

```python
   <pre>
        鹅鹅鹅

            作者：李白
        曲项向天歌
        白毛浮绿水
        
    </pre>
```

### 4.超链接

1.  外部链接：连接到外部文件.举例：

   ```python
   <a href="new.html">点击进入到新网页</a>
   
   ```

2. 锚链接：

   ```python
   指给超链接起一个名字，作用是在本页面或者其他页面的的不同位置进行跳转。比如说，在网页底部有一个向上箭头，点击箭头后回到顶部，这个就是利用到了锚链接。
   首先我们要创建一个锚点，也就是说，使用name属性或者id属性给那个特定的位置起个名字
   ```

3. 超链接的属性

   ```python
   href：目标URL
   title：悬停文本。
   name：主要用于设置一个锚点的名称。
   target：告诉浏览器用什么方式来打开目标页面。target属性有以下几个值：
   _self：在同一个网页中显示（默认值）
   _blank：在新的窗口中打开(空白页)。
   _parent：在父窗口中显示
   _top：在顶级窗口中显示
   ```

4.  特殊链接

   - 邮件链接

     ```python
     <a href="mailto:zhaoxu@tedu.cn">联系我们</a>
     ```

   - 返回页面顶部的位置

     ```python
     <a href="#">跳转到顶部</a>
     ```

   - 关于javascript

     ```python
     <a href="javascript:alert(1)">内容</a>
       <a href="javascript:;">内容</a>
     javascript:;表示什么都不执行，这样点击<a>时就没有任何反应 例如：<a href="javascrip:;">内容</2
     javascript:是表示在触发<a>默认动作时，执行一段JavaScript代码。 例如：<ahref="javascript:alert()">内容</a>
     ```

   ##### 5 图片标签

   <img/>  是自封闭标签，也称为单标签。

   ```python
   width：宽度
   height：高度
   title：提示性文本。公有属性。也就是鼠标悬停时出现的文本。
   align：指图片的水平对齐方式，属性值可以是：left、center、right
   alt：当图片显示不出来的时候，代替图片显示的内容。alt是英语 alternate “替代”的意思。（有的浏览器不支持）
   ```

   

### 5. body 标签中相关标签

```python
ul：unordered list，“无序列表”的意思。
li：list item，“列表项”的意思。
属性：

type="属性值"。属性值可以选： disc(实心原点，默认)，square(实心方点)，circle(空心圆)。
示例：

复制代码
<ul>
         <li>张三</li>    
         <li>李四</li>    
         <li>王五</li>    
</ul>
```

```python
英文单词：Ordered List。
type="属性值"。属性值可以是：1(阿拉伯数字，默认)、a、A、i、I。结合start属性表示从几开始。

例如：

<ol>   
        <li>嘿哈</li>
       <li>哼哈</li>
        <li>呵呵</li>
</ol>
```

```python
type="属性值"。属性值可以是：1(阿拉伯数字，默认)、a、A、i、I。结合start属性表示从几开始。
```

6. ### 表格标签

   ```python
   表格标签用<table>表示。
   一个表格<table>是由每行<tr>组成的，每行是由<td>组成的。
   所以我们要记住，一个表格是由行组成的（行是由列组成的），而不是由行和列组成的。
   在以前，要想固定标签的位置，唯一的方法就是表格。现在可以通过CSS定位的功能来实现。但是现在在做页面的时候，表格作用还是有一些的。
   <table>的属性：
   
   border：边框。像素为单位。
   style="border-collapse:collapse;"：单元格的线和表格的边框线合并
   width：宽度。像素为单位。
   height：高度。像素为单位。
   bordercolor：表格的边框颜色。
   align：表格的水平对齐方式。属性值可以填：left right center。
   注意：这里不是设置表格里内容的对齐方式，如果想设置内容的对齐方式，要对单元格标签<td>进行设置）
   cellpadding：单元格内容到边的距离，像素为单位。默认情况下，文字是紧挨着左边那条线的，即默认情况下的值为0。
   注意不是单元格内容到四条边的距离哈，而是到一条边的距离，默认是与左边那条线的距离。如果设置属性dir="rtl"，那就指的是内容到右边那条线的距离。
   cellspacing：单元格和单元格之间的距离（外边距），像素为单位。默认情况下的值为0
   bgcolor="#99cc66"：表格的背景颜色。
   background="路径src/..."：背景图片。
   背景图片的优先级大于背景颜色。
   ```

```python
例如，3行4列的单元格：

复制代码
  <table>
        <tr>
            <td>小马哥</td>
            <td>18</td>
            <td>男</td>
            <td>山东</td>
        </tr>

        <tr>
            <td>小岳岳</td>
            <td>45</td>
            <td>男</td>
            <td>河南</td>
        </tr>

        <tr>
            <td>邓紫棋</td>
            <td>23</td>
            <td>女</td>
            <td>香港</td>
        </tr>

    </table>
```

```python
tr 行
dir：公有属性，设置这一行单元格内容的排列方式。可以取值：ltr：从左到右（left to right，默认），rtl：从右到左（right to left）
bgcolor：设置这一行的单元格的背景色。
注：没有background属性，即：无法设置这一行的背景图片，如果非要设置，可以用css实现。
height：一行的高度
align="center"：一行的内容水平居中显示，取值：left、center、right
valign="center"：一行的内容垂直居中，取值：top、middle、bottom
```

```python 
td 单元格
align：内容的横向对齐方式。属性值可以填：left right center。
如果想让每个单元格的内容都居中，这个属性太麻烦了，以后用css来解决。
valign：内容的纵向对齐方式。属性值可以填：top middle bottom
width：绝对值或者相对值(%)
height：单元格的高度
bgcolor：设置这个单元格的背景色。
background：设置这个单元格的背景图片。
```

## 6 . 表单标签

<from>

```python
属性
name：表单的名称，用于JS来操作或控制表单时使用；
id：表单的名称，用于JS来操作或控制表单时使用；
action：指定表单数据的处理程序，一般是PHP，如：action=“login.php”
method：表单数据的提交方式，一般取值：get(默认)和post
```

### 7 . 输入标签

```python
type="属性值"：文本类型。属性值可以是：
text（默认）
password：密码类型
radio：单选按钮，名字相同的按钮作为一组进行单选（单选按钮，天生是不能互斥的，如果想互斥，必须要有相同的name属性。name就是“名字”。
）。非常像以前的收音机，按下去一个按钮，其他的就抬起来了。所以叫做radio。
checkbox：多选按钮，名字相同的按钮作为一组进行选择。
checked：将单选按钮或多选按钮默认处于选中状态。当<input>标签的type="radio"时，可以用这个属性。属性值也是checked，可以省略。
hidden：隐藏框，在表单中包含不希望用户看见的信息
button：普通按钮，结合js代码进行使用。
submit：提交按钮，传送当前表单的数据给服务器或其他程序处理。这个按钮不需要写value自动就会有“提交”文字。这个按钮真的有提交功能。点击按钮后，这个表单就会被提交到form标签的action属性中指定的那个页面中去。
reset：重置按钮，清空当前表单的内容，并设置为最初的默认值
image：图片按钮，和提交按钮的功能完全一致，只不过图片按钮可以显示图片。
file：文件选择框。
提示：如果要限制上传文件的类型，需要配合JS来实现验证。对上传文件的安全检查：一是扩展名的检查，二是文件数据内容的检查。


value="内容"：文本框里的默认内容（已经被填好了的）
size="50"：表示文本框内可以显示五十个字符。一个英文或一个中文都算一个字符。
注意size属性值的单位不是像素哦。
readonly：文本框只读，不能编辑。因为它的属性值也是readonly，所以属性值可以不写。
用了这个属性之后，在google浏览器中，光标点不进去；在IE浏览器中，光标可以点进去，但是文字不能编辑。
disabled：文本框只读，不能编辑，光标点不进去。属性值可以不写。
```

### 8. <select标签

```python
<select>标签里面的每一项用<option>表示。select就是“选择”，option“选项”。

select标签和ul、ol、dl一样，都是组标签。

<select>标签的属性：

multiple：可以对下拉列表中的选项进行多选。没有属性值。
size="3"：如果属性值大于1，则列表为滚动视图。默认属性值为1，即下拉视图。
<option>标签的属性：

selected：预选中。没有属性值。
```

9 . textarea标签

```python
<textarea>标签：多行文本输入框
text就是“文本”，area就是“区域”。

属性：

value：提交给服务器的值。
rows="4"：指定文本区域的行数。
cols="20"：指定文本区域的列数。
readonly：只读。
```



## 第十二章 Django框架



## 附录 A常见错误和单词

### 单词

| upper | 大写 | ...  |
| ----- | ---- | ---- |
|       |      |      |
|       |      |      |
|       |      |      |
|       |      |      |
|       |      |      |
|       |      |      |
|       |      |      |
|       |      |      |
|       |      |      |
|       |      |      |
|       |      |      |
|       |      |      |
|       |      |      |
|       |      |      |
|       |      |      |
|       |      |      |
|       |      |      |
|       |      |      |
|       |      |      |
|       |      |      |
|       |      |      |
|       |      |      |
|       |      |      |
|       |      |      |
|       |      |      |
|       |      |      |
|       |      |      |
|       |      |      |
|       |      |      |
|       |      |      |
|       |      |      |
|       |      |      |
|       |      |      |
|       |      |      |
|       |      |      |
|       |      |      |
|       |      |      |
|       |      |      |
|       |      |      |
|       |      |      |
|       |      |      |
|       |      |      |
|       |      |      |
|       |      |      |
|       |      |      |
|       |      |      |
|       |      |      |
|       |      |      |
|       |      |      |
|       |      |      |
|       |      |      |
|       |      |      |
|       |      |      |
|       |      |      |
|       |      |      |
|       |      |      |
|       |      |      |
|       |      |      |
|       |      |      |
|       |      |      |
|       |      |      |
|       |      |      |
|       |      |      |
|       |      |      |
|       |      |      |
|       |      |      |
|       |      |      |
|       |      |      |
|       |      |      |
|       |      |      |
|       |      |      |
|       |      |      |
|       |      |      |
|       |      |      |
|       |      |      |
|       |      |      |
|       |      |      |
|       |      |      |
|       |      |      |
|       |      |      |
|       |      |      |
|       |      |      |
|       |      |      |
|       |      |      |
|       |      |      |
|       |      |      |
|       |      |      |
|       |      |      |
|       |      |      |
|       |      |      |
|       |      |      |
|       |      |      |
|       |      |      |
|       |      |      |
|       |      |      |
|       |      |      |
|       |      |      |
|       |      |      |

### 错误记录

#### 1. 缩进错误

![1554698249295](C:\Users\oldboy-python\AppData\Roaming\Typora\typora-user-images\1554698249295.png)

2. #### 键错误

   ![1554698317790](C:\Users\oldboy-python\AppData\Roaming\Typora\typora-user-images\1554698317790.png)

3. 

## 附录B：面试题环节

- 公司常用线上常用系统：centos

- python2与python3区别？

  - 输入输出
  - 默认编码器
  - int与整除
  - py2:Unicode类型，str字节类型----->python3 :str类型，bytes类型
  - 包、模块
  - py2:xrange,（边循环边创建）。range是一个列表（创建就放入内存）,py3只有range,功能与xrange一样。
  - map,filter:py2返回列表，py3是迭代器
  - 字典：items,keys,values在py2返回是列表，py3返回迭代器
  - reduce 在py3使用前需要引用functools

- 每种数据类型，列举你了解的方法。

- 逻辑运算符：or,and,not--->   3 or 9 and  8

- 字符串反转：通过索引步长实现

- is 和 == 的区别？

  - ==比较2个值是否相等，is比较2个内存地址是否相等

- v1 = (1)、v2 = 1 、v1 = (1,)区别。

  - 第一个第二个都是数值，第三个是元组，第一个与第二个相等
  - 

- 深浅拷贝：针对于可变类型，浅拷贝只拷贝最外层，深拷贝拷贝所有层，不会出现一个改变而另一个改变。

- 文件操作，文件如何读取内容？

- 一行实现：9*9乘法表。

  ```python
  print("\n".join([' '.join(["%d*%d=%d"%(x,j,x*j) for j in range(1,x+1)]) for x in range(1,10)]))
  ```

- git流程

  - git init 文件初始化
  - git status 查看当前文件状态
  - git add 对指定文件进行版本控制
  - git commit -m 创建提交记录

- 请将 ip = "192.168.12.79" 中的每个十进制数转换成二进制并通过,连接起来生成一个新的字符串，转换成十进制。

```python
#方法一
ip = '192.168.12.79'

ip_list= ip.split('.') #将ip分割，成一个列表[192，168，12，79]
l = []
for i in ip_list:
    l.append(bin(int(i)))
print(l)  # 二进制数字放入列表l= [0b010101,...]
str = ''
for item in l:
    if len(item[2:]) == 8:
        str+=item[2:]
    else:
        str +=item[2:].rjust(8,'0')
print(int(str,base=2))
#3232238671

#方法二：（一行实现代码）
print(int("".join([data[2:].rjust(8,'0') if data[2:]!=8 else data[2:] for data in [bin(int(item)) for item in ip.split('.')]]),base=2))
#3232238671
#方法三：
print(int("".join([data[2:].rjust(8,'0') for data in [bin(int(item)) for item in ip.split('.')]]),base=2))
```

- 看代码写结果【新浪面试题】

```python
def func():
    for num in range(10):
        pass
    v4 = [lambda :num+10,lambda :num+100,lambda :num+100,]
    result1 = v4[1]()
    result2 = v4[2]()
    print(result1,result2)
func()
# 109 109
#分析，num经过for循环，func的函数为9.v4列表里面有3个函数，函数内部没有num值，去func函数找到num,
```

- 看代码写结果

```python
def func(a,b=[]):
    b.append(a)
    return b
l1 = func(1)
l2 = func(2,[11,22])
l3 = func(3)
print(l1,l2,l3)
#[1,3] [11,22,2] [1,3]
#执行l1=func(1),函数重新开辟内存空间，函数让列表b添加元素1，再执行l2=func(2,[11,22])将b的值重新赋值[11,22]，并添加元素2，再执行l3 = func(3),因l3没有传入b参数，所以在添加3元素的列表时，找到之前b=[]的列表内存地址，但b由于执行l1现在已经是[1,]，再添加元素3，为[1,3]，最后打印l1,l2,l3。因l1,l3两个b用是相同内存地址。所以l1,l3输出结果[1,3],l2由于重新赋值新的列表，所以得到结果[11,,22,2]
```

- 写一个带参数的装饰器，实现：参数是多少，被装饰的函数就要执行多少次，并返回最后一次执行的结果。

```python
def xxx(counter):
    print('x')
    def wrapper(func):
        print('wrapper')
        def inner(*args,**kwargs):
            for i in range(counter):
                data = func(*args,**kwargs)
            return data
        return inner
    return wrapper
@xxx(5)
def index():
    return 8
v = index()
print(v)
#8
```

- 9.斐波那契数列 1，2，3，5，8，13，21...根据这样规律求出400万以内最大的斐波那契数，

  并求出它是第几个斐波那契数。

  

```python
def fib():
    fib_list = [1,1]
    while True:
        last_num = fib_list[-1]+fib_list[-2]
        fib_list.append(last_num)
        if fib_list[-1] >4000000 and fib_list[-2]<4000000:
            break

    print('400万最大斐波那契数为：%s'%fib_list[-2])
    print('它是第%s个数'%fib_list.index(fib_list[-2]))
    # print(fib_list)
```

- dicta = {'a':1,'b':2,'c':3,'d':4,'f':'hello'}
  dictb = {'b':3,'d':5,'e':7,'m':9,'k':'world'}
  #要求写一段代码，实现两个字典相加，不同的key对应的值保留
  #如果是字符串就拼接，如上示例得到结果为：

  dictc = {'a':1,'b':5,'c':3,'d':9,'e':7,'m':9,'f':'hello','k':'world'}

```python
def sum_dict():
    dicta = {'a': 1, 'b': 2, 'c': 3, 'd': 4, 'f': 'hello'}
    dictb = {'b': 3, 'd': 5, 'e': 7, 'm': 9, 'k': 'world'}
    for key,value in dicta.items():
        if dictb.get(key) == None:
            dictb[key] = value
        else:
            dictb[key] += dicta[key]

    dictc = dictb
    print(dictc)

sum_dict()
```

- 以下的代码的输出将是什么

```python
def extendList(val,list=[]):
    list.append(val)
    return list
list1 = extendList(10)
list2 = extendList(123,[])
list3 = extendList('a')
print(list1,list2,list3)
#[10,'a'] [123,] [10,'a']
```

- tupleA = ('a','b','c','d','e')
  tupleB = (1,2,3,4,5)

  写出由tupleA和tupleB得到的res = {'a': 1, 'b': 2, 'c': 3, 'd': 4, 'e': 5}

```python
tupleA = ('a','b','c','d','e')
tupleB = (1,2,3,4,5)
res = {}
for i in range(len(tupleA)):
    res[tupleA[i]] = tupleB[i]

print(res)
```

- python代码获取命令行参数

```python
import sys
sys.argv
```

- 编写一个函数，这个函数接受一个文件夹名称做为参数,显示文件夹中文件的路经，以及其中包含文件夹中的文件的路经

```python
import os
import sys
def file_path(file):
    files = os.path.abspath(file)
    files1 = os.walk(files)
    for a,b,c in files1:
        print(a)
        for i in c:
            print(os.path.join(a,i))

file_path('a')
```

- 2.1000以内的完美数（如果一个数恰好等于它的因子之和，则称该数为完美数）

  eg  6=1*2*3 = 1+2+3

```python
def func(n):
    for item in range(1,1001):
        str = 1
        lst = 0
        for index in range(1,item):
            if item%index == 0:
                lst+= index
                str *= index

        if lst == item and str == item:
            print(item)

func(1000)
```

- 一个大小为100G的文件etl_log.txt,要读取文件中的内容学出具体过程代码

```python
file_size = os.stat('etl_log.txt').st_size
read_size = 0
with open('etl_log.txt','r',encoding='utf-8') as file:
    while read_size<file_size:
        data = file.read(1024)
        read_size += len(data)
```

- 写一个脚本，接收两个参数。第一个参数：文件,第二个参数：内容..请将第二个参数中的内容写入到 文件（第一个参数）中。

```python
import os
import sys
a = sys.argv
head_path = r"G:\homework\day14"
paths=os.path.join(head_path,a[1])
with open(paths,'w',encoding='utf-8') as f1:
    c = f1.write(a[2])
    if c:
        print('成功打入内部')
```

- 请比较[i for i in range(10)] 和 (i for i in range(10))的区别？

```python
[i for i in range(10)] 立刻循环创建所有元素，返回的是一个列表。
(i for i in range(10)）返回的是一个生成器，通过for循环调用。
```

- 上下文管理，实现文件操作

```python
class Foo(object):
    def __enter__(self):
        self.x = open('log.txt','r',encoding='utf-8')
        return self.x
    def __exit__(self, exc_type, exc_val, exc_tb):
        self.x.close()
with open Foo() as file:
    file.write('alex')
```