# MYSQL

### 1.0 数据库的概念

```python
# 数据库
    # 很多功能如果只是通过操作文件来改变数据是非常繁琐的
        # 程序员需要做很多事情
    # 对于多台机器或者多个进程操作用一份数据
        # 程序员自己解决并发和安全问题比较麻烦
    # 自己处理一些数据备份，容错的措施

# C/S架构的 操作数据文件的一个管理软件
    # 1.帮助我们解决并发问题
    # 2.能够帮助我们用更简单更快速的方式完成数据的增删改查
    # 3.能够给我们提供一些容错、高可用的机制
    # 4.权限的认证

# 数据库管理系统 —— 专门用来管理数据文件，帮助用户更简洁的操作数据的软件
    # DBMS
# 数据 data
# 文件
# 文件夹 -- 数据库database db
# 数据库管理员 —— DBA
```

### 1.1 数据库的分类

1. 关系型数据库
   - sql server
   - oracle 收费、比较严谨、安全性比较高
     国企 事业单位
     银行 金融行业
   - mysql  开源的
     小公司
      互联网公司
   - sqllite

2. 非关系型数据库

   - redis

   - mongodb

```python
# 安装mysql遇到的问题
    # 操作系统的问题
        # 缺失dll文件
    # 安装路径
        # 不能有空格
        # 不能有中文
        # 不能带着有转义的特殊字符开头的文件夹名
    # 安装之后发现配置有问题
        # 再修改配置往往不能生效
    # 卸载之后重装
        # mysqld remove
        # 把所有的配置、环境变量修改到正确的样子
        # 重启计算机 - 清空注册表
        # 再重新安装

# mysql的CS架构
    # mysqld install  安装数据库服务
    # net start mysql 启动数据库的server端
        # 停止server net stop mysql
    # 客户端可以是python代码也可以是一个程序
        # mysql.exe是一个客户端
        # mysql -u用户名 -p密码
# mysql中的用户和权限
    # 在安装数据库之后，有一个最高权限的用户root
    # mysql 192.168.12.87 eva/123
        # mysql -h192.168.12.87 -uroot -p123

# 我们的mysql客户端不仅可以连接本地的数据库
# 也可以连接网络上的某一个数据库的server端

# mysql>select user();
    # 查看当前用户是谁
# mysql>set password = password('密码')
    # 设置密码
# 创建用户
# create user 's21'@'192.168.12.%' identified by '123';
# mysql -us21 -p123 -h192.168.12.87

# 授权
    # grant all on day37.* to 's21'@'192.168.12.%';
    # 授权并创建用户
    # grant all on day37.* to 'alex'@'%' identified by '123';

# 查看文件夹
    # show databases;
# 创建文件夹
    # create database day37;

# 库 表 数据
    # 创建库、创建表  DDL数据库定义语言
    # 存数据，删除数据,修改数据,查看  DML数据库操纵语句
    # grant/revoke  DCL控制权限
# 库
    # create database 数据库名;  # 创建库
    # show databases; # 查看当前有多少个数据库
    # select database();# 查看当前使用的数据库
    # use 数据库的名字; # 切换到这个数据库(文件夹)下
# 表操作
    # 查看当前文件夹中有多少张表
        # show tables;
    # 创建表
        # create table student(id int,name char(4));
    # 删除表
        # drop table student;
    # 查看表结构
        # desc 表名;

# 操作表中的数据
    # 数据的增加
        # insert into student values (1,'alex');
        # insert into student values (2,'wusir');
    # 数据的查看
        # select * from student;
    # 修改数据
        # update 表 set 字段名=值
        # update student set name = 'yuan';
        # update student set name = 'wusir' where id=2;
    # 删除数据
        # delete from 表名字;
        # delete from student where id=1;
```

##### 3.0 总结

```python
# 数据库
    # 关系型数据库 ：mysql oracle sqlserver sqllite
    # 非关系型数据库 ：mongodb redis
# sql ：
    # ddl 定义语言
        # 创建用户
            # create user '用户名'@'%'  表示网络可以通讯的所有ip地址都可以使用这个用户名
            # create user '用户名'@'192.168.12.%' 表示192.168.12.0网段的用户可以使用这个用户名
            # create user '用户名'@'192.168.12.87' 表示只有一个ip地址可以使用这个用户名
        # 创建库
            # create database day38;
        # 创建表
            # create table 表名(字段名 数据类型(长度)，字段名 数据类型(长度)，)
    # dml 操作语言
            # 数据的
            # 增 insert into
            # 删 delete from
            # 改 update
            # 查 select
                # select user(); 查看当前用户
                # select database(); 查看当前所在的数据库
            # show
                # show databases:  查看当前的数据库有哪些
                # show tables；查看当前的库中有哪些表
            # desc 表名；查看表结构
            # use 库名；切换到这个库下
    # dcl 控制语言
        # 给用户授权
        # grant select on 库名.* to '用户名'@'ip地址/段' identified by '密码'
        # grant select,insert
        # grant all
```

##### 4.0创建方式 存储 —— 存储引擎

```python
# 表的存储方式
    # 存储方式1：MyISAM 5.5以下 默认存储方式
        # 存储的文件个数：表结构、表中的数据、索引
        # 支持表级锁
        # 不支持行级锁 不支持事务 不支持外键
    # 存储方式2：InnoDB 5.6以上 默认存储方式
        # 存储的文件个数：表结构、表中的数据
        # 支持行级锁、支持表锁
        # 支持事务
        # 支持外键
    # 存储方式3： MEMORY 内存
        # 存储的文件个数：表结构
        # 优势 ：增删改查都很快
        # 劣势 ：重启数据消失、容量有限
# 索引 - 数据库的目录


# 查看配置项:
    # show variables like '%engine%';

# 创建表
# create table t1 (id int,name char(4));
# create table t2 (id int,name char(4)) engine=myisam;
# create table t3 (id int,name char(4)) engine=memory;

# 查看表的结构
    # show create table 表名; 能够看到和这张表相关的所有信息
    # desc 表名               只能查看表的字段的基础信息
        # describe 表名


# 用什么数据库 ： mysql
# 版本是什么 ：5.6
# 都用这个版本么 ：不是都用这个版本
# 存储引擎 ：innodb
# 为什么要用这个存储引擎：
    # 支持事务 支持外键 支持行级锁
                         # 能够更好的处理并发的修改问题
```

```
# 
# 表的约束

# 创建表
# 查看表结构
# 修改表
# 删除表
```

##### 5.0 基础数据类型

5.1数字

```python
# 整数 int
# create table t4 (id1 int(4),id2 int(11));
    # int默认是有符号的
    # 它能表示的数字的范围不被宽度约束
    # 它只能约束数字的显示宽度
# create table t5 (id1 int unsigned,id2 int);

# 小数  float
# create table t6 (f1 float(5,2),d1 double(5,2));    总共5位 3位整数位,2位小数

# create table t7 (f1 float,d1 double);
# create table t8 (d1 decimal,d2 decimal(25,20)); 

1 、double的存储值最大为16位，保留几位小数可以设置，
      比如double（10,2），小数点的左右两边的位数加起来不能超过10位

2 、decimal 的最大存储值为38位，保留几位小数可以设置，
      比如decimal（10,2）的存储值为：左边最大值为7位，小数点候保留2位小数，加起来最大值为10位， 
     小数点的左右两边的位数加起来不能超过10位
```

5.2 字符串

```python
# char(15)  定长的单位
    # alex  alex
    # alex
# varchar(15) 变长的单位
    # alex  alex4

# 哪一个存储方式好？
    # varchar ：节省空间、存取效率相对低
    # char ：浪费空间，存取效率相对高 长度变化小的

# 手机号码、身份证号  char
# 用户名、密码  char
# 评论  微博 说说 微信状态 varchar

# create table t11 (name1 char(5),name2 varchar(5));
```

5.3 时间和日期

```python
# 类型
    # year 年
    # date 年月日
    # time 时分秒
    # datetime、timestamp 年月日时分秒

# create table t9(
# y year,d date,
# dt datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
# ts timestamp);
```

5.4 其他

```python
# create table t12(
# name char(12),
# gender ENUM('male','female'),
# hobby set('抽烟','喝酒','烫头','洗脚')
# );

# insert into t12 values('alex','不详','抽烟,喝酒,洗脚,洗脚,按摩');
```

### 1.2 约束

```python
1.not null 某一个字段不能为空
2.default   设置默认值
3.unique   设置某一个字段不能重复
4.联合唯一：
	create table t4(id int,ip char(15),
	server char(10),port int,unique(ip,port)
	)
5. 自增：
	auto_increment
	自增字段 必须是数字，且 必须是唯一的
6. primary key 主键 
	#一张表只能设置一个主键
	#规范：一张表最好设置一个主键
	#约束这个字段 非空（not null）且 唯一 （unique）
	3，若你设置主键，你指定的第一个非空且为1的为主键
7. 联合主键 


8.外键  foreign key 涉及到两张表
员工表
id age gender  salary hire_data postname postid post_comment post_email post_phone
create table staff(id int primary key, age int , gender  salary enum('male','female'),hire_data data, postname postid int);
```

```python
# 约束
    # unsigned  设置某一个数字无符号
    # not null 某一个字段不能为空
    # default 给某个字段设置默认值
    # unique  设置某一个字段不能重复
        # 联合唯一
    # auto_increment 设置某一个int类型的字段 自动增加
        # auto_increment自带not null效果
        # 设置条件 int  unique
    # primary key    设置某一个字段非空且不能重复
        # 约束力相当于 not null + unique
        # 一张表只能由一个主键
    # foreign key    外键
        # references
        # 级联删除和更新

# not null
# default
# create table t2(
#   id int not null,
#   name char(12) not null,
#   age int default 18,
#   gender enum('male','female') not null default 'male'
# )

# unique 设置某一个字段不能重复
# create table t3(
#     id int unique,
#     username char(12) unique,
#     password char(18)
# );

# 联合唯一
# create table t4(
#     id int,
#     ip char(15),
#     server char(10),
#     port int,
#     unique(ip,port)
# );

# 自增
    # auto_increment
    # 自增字段 必须是数字 且 必须是唯一的
# create table t5(
#     id int unique auto_increment,
#     username char(10),
#     password char(18)
# )
# insert into t5(username,password) values('alex','alex3714')

# primary key  主键
    # 一张表只能设置一个主键
    # 一张表最好设置一个主键
    # 约束这个字段 非空（not null） 且 唯一（unique）

# create table t6(
#     id int not null unique,     # 你指定的第一个非空且唯一的字段会被定义成主键
#     name char(12) not null unique
# )

# create table t7(
#     id int primary key,     # 你指定的第一个非空且唯一的字段会被定义成主键
#     name char(12) not null unique
# )

# 联合主键
# create table t4(
#     id int,
#     ip char(15),
#     server char(10),
#     port int,
#     primary key(ip,port)
# );

# 外键 foreign key 涉及到两张表
# 员工表
# create table staff(
# id  int primary key auto_increment,
# age int,
# gender  enum('male','female'),
# salary  float(8,2),
# hire_date date,
# post_id int,
# foreign key(post_id) references post(pid)
# )


# 部门表
#  pid postname post_comment post_phone
# create table post(
#     pid  int  primary key,
#     postname  char(10) not null unique,
#     comment   varchar(255),
#     phone_num  char(11)
# )

# update post set pid=2 where pid = 1;
# delete from post where pid = 1;

# 级联删除和级联更新
# create table staff2(
# id  int primary key auto_increment,
# age int,
# gender  enum('male','female'),
# salary  float(8,2),
# hire_date date,
# post_id int,
# foreign key(post_id) references post(pid) on update cascade on delete set null
# )
```

### 1.3 修改表结构

```python
# 创建项目之前
# 项目开发、运行过程中

# alter table 表名 add 添加字段
# alter table 表名 drop 删除字段
# alter table 表名 modify 修改已经存在的字段  的类型 宽度 约束
# alter table 表名 change 修改已经存在的字段  的类型 宽度 约束 和 字段名字

# alter table 表名 add 字段名 数据类型(宽度) 约束 first/after name
# alter table 表名 drop 字段名
# alter table 表名 modify name varchar(12) not null
# alter table 表名 change name new_name varchar(12) not null

# id name age
# alter table 表名 modify age int not null after id;
# alter table 表名 modify age int not null first;
```

### 1.4 表关系

```python
# 两张表中的数据之间的关系
# 多对一   foreign key  永远是在多的那张表中设置外键
    # 多个学生都是同一个班级的
    # 学生表 关联 班级表
    # 学生是多    班级是一
# 一对一   foreign key +unique  # 后出现的后一张表中的数据 作为外键，并且要约束这个外键是唯一的
    # 客户关系表 ： 手机号码  招生老师  上次联系的时间  备注信息
    # 学生表 ：姓名 入学日期 缴费日期 结业
# 多对多  产生第三张表，把两个关联关系的字段作为第三张表的外键
    # 书
    # 作者

    # 出版社
    # 书
```

### 1.5 数据操作

```python
# 增加 insert
# 删除 delete
# 修改 update
# 查询 select

# 增加 insert
# insert into 表名 values (值....)
    # 所有的在这个表中的字段都需要按照顺序被填写在这里
# insert into 表名(字段名，字段名。。。) values (值....)
    # 所有在字段位置填写了名字的字段和后面的值必须是一一对应
# insert into 表名(字段名，字段名。。。) values (值....),(值....),(值....)
    # 所有在字段位置填写了名字的字段和后面的值必须是一一对应

# value单数            values复数
# 一次性写入一行数据   一次性写入多行数据

# t1 id,name,age
# insert into t1 value (1,'alex',83)
# insert into t1 values (1,'alex',83),(2,'wusir',74)

# insert into t1(name,age) value ('alex',83)
# insert into t1(name,age) values ('alex',83),('wusir',74)

# 第一个角度
    # 写入一行内容还是写入多行
    # insert into 表名 values (值....)
    # insert into 表名 values (值....)，(值....)，(值....)

# 第二个角度
    # 是把这一行所有的内容都写入
    # insert into 表名 values (值....)
    # 指定字段写入
    # insert into 表名(字段1，字段2) values (值1，值2)


# 删除 delete
    # delete from 表 where 条件;

# 更新 update
    # update 表 set 字段=新的值 where 条件；

# 查询
    # select语句
        # select * from 表
        # select 字段,字段.. from 表
        # select distinct 字段,字段.. from 表  # 按照查出来的字段去重
        # select 字段*5 from 表  # 按照查出来的字段去重
        # select 字段  as 新名字,字段 as 新名字 from 表  # 按照查出来的字段去重
        # select 字段 新名字 from 表  # 按照查出来的字段去重
```

### 1.6 筛选 

1.where 语句 范围筛选

```python
1 select * from where 字段 = ‘’
2 select * from where 字段 in （11,12,13,45,5,6）;
模糊范围
一个数值区间，1W-2W 
select * from employee where salary between 10000 and 20000
字符串模糊查询   %   _  正则
select * from employee where emp_name like '程%'； 陪陪任意长度任意内容
select * from employee where emp_name like '程_'； 匹配一个字符长度任意内容
什么什么结尾
select * from employee where emp_name like '%程'
含有的
select * from employee where emp_name like '%程%'；
正则匹配  regexp
	select *  from  biao where 字段 regexp 正则表达式

```

逻辑运算 ---条件的拼接

```python
与 and
或 or
非 not
	2 select * from where 字段 not in （11,12,13,45,5,6）;	

```

### 1.7 分组聚合

```n
1 分组  group by
会吧在group by 后面的这个字段，也就是post 字段中的每一个不同的项都保留下来
并且会把值是这一项的所有行归为一组
select * from employee group by post;
```

```python
聚合  把很多航的用一个字段进行一些统计，最终得到一个结果
count（字段） 统计这个字段有多少项
sum（字段）  统计这个字段对应的数值的和
avg（字段）      拿到的平均值
min(字段)    最下
max（）
```

```python
select count(*) from employee group by post
select post,count(*) from employee group by post
```

```python
总是根据会重复的项来进行分组
分组
```

### 1.8 having 语句       过滤语句    

```python
过滤的是组  总是跟group 一起用           where 过滤的是行
执行顺序重视先执行where 在执行group by 分组
所以相关先分组 之后再根据分组做某些条件筛选的时候 where 都用不上
只能用having 来完成
# 部门人数大于3的部门
# select post from employee group by post having count(*) > 3

# 1.执行顺序 总是先执行where 再执行group by分组
#   所以相关先分组 之后再根据分组做某些条件筛选的时候 where都用不上
# 2.只能用having来完成

# 平均薪资大于10000的部门
# select post from employee group by post having avg(salary) > 10000


# select * from employee having age>18

# order by
    # order by 某一个字段 asc;  默认是升序asc 从小到大
    # order by 某一个字段 desc;  指定降序排列desc 从大到小
    # order by 第一个字段 asc,第二个字段 desc;
        # 指定先根据第一个字段升序排列，在第一个字段相同的情况下，再根据第二个字段排列

# limit
    # 取前n个  limit n   ==  limit 0,n
        # 考试成绩的前三名
        # 入职时间最晚的前三个
    # 分页    limit m,n   从m+1开始取n个
    # 员工展示的网页
        # 18个员工
        # 每一页展示5个员工
    # limit n offset m == limit m,n  从m+1开始取n个

```

```python
# 分组聚合
    # 求各个部门的人数
    # select count(*) from employee group by post
    # 求公司里 男生 和女生的人数
    # select count(id) from employee group by sex
    # 求各部门的平均薪资
    # 求各部门的平均年龄
    # 求各部门年龄最小的
        # select post,min(age) from employee group by post
    # 求各部门年龄最大的
    # 求各部门薪资最高的
    # 求各部门薪资最低的
    # 求最晚入职的
    # 求最早入职的
    # 求各部门最晚入职的
    # 求各部门最早入职的

# 求部门的最高薪资或者求公司的最高薪资都可以通过聚合函数取到
# 但是要得到对应的人，就必须通过多表查询

# 总是根据会重复的项来进行分组
# 分组总是和聚合函数一起用 最大 最小 平均 求和 有多少项
```

order by 

```python
ord by  某个字段 asc  默认是升序  从小到大
ord by 字段 desc  指定降序排列 desc  从大到小
order by  第一个字段asc 第二个字段desc 指定先根据第一个字段排列 一样的话再跟第二个字段排序
```

limit

```python
取前 n个 limit n = limit 0，n
分页功能 limit m,n  从m+1 开始取n个
员工展示的网页

limit n offset m ==limit m,n  从m+1 开始取n个
```

总结

```python
# select distinct 需要显示的列 from 表
#                             where 条件
#                             group by 分组
#                             having 过滤组条件
#                             order by 排序
#                             limit 前n 条
```

### 1.9 pymysql

```python
import pymysql
#
# conn = pymysql.connect(host='127.0.0.1', user='root', password="123",
#                  database='day40')
# cur = conn.cursor()   # 数据库操作符 游标
# # cur.execute('insert into employee(emp_name,sex,age,hire_date) '
# #             'values ("郭凯丰","male",40,20190808)')
# # cur.execute('delete from employee where id = 18')
# conn.commit()
# conn.close()


# conn = pymysql.connect(host='127.0.0.1', user='root', password="123",
#                  database='day40')
# cur = conn.cursor(pymysql.cursors.DictCursor)   # 数据库操作符 游标
# cur.execute('select * from employee '
#             'where id > 10')
# ret = cur.fetchone()
# print(ret['emp_name'])
# # ret = cur.fetchmany(5)
# ret = cur.fetchall()
# print(ret)
# conn.close()

# select * from employee where id > 10


# sql注入风险
```



### 2.0 索引

```python
#索引的重要性 （提高查询的速度）
	#读写比例 10:1
    #读的速度就至关重要了
#索引的原理
	#block 磁盘预读原理
    	# for line in f
    	#一个 block  linuex  为 4096字节
     #读硬盘的IO操作的事件非常的长，比CPU执行指令的时间长很多
    # 尽量的减少IO次数 才是读写数据的主要解决的问题
```

创建索引

```python
看一下如何创建索引、创建索引之后的变化
    # create index 索引名字 on 表(字段)
    # DROP INDEX 索引名 ON 表名字;
索引是如何发挥作用的
    # select * from 表 where id = xxxxx
        # 在id字段没有索引的时候，效率低
        # 在id字段有索引的之后，效率高
```

### 2.0  mysql执行计划

```python
explain  + sql语句
会提示这个sql语句是否命中索引,命中的索引详细信息
```

### 2.1 数据库的存储方式

```python
什么是索引 -- 目录
    # 就是建立起的一个在存储表阶段
    # 就有的一个存储结构能在查询的时候加速
# 索引的重要性
    # 读写比例 ： 10：1
    # 读（查询）的速度就至关重要了
# 索引的原理
    # block 磁盘预读原理
        # for line in f
        # 4096个字节
    # 读硬盘的io操作的时间非常的长，比CPU执行指令的时间长很多
    # 尽量的减少IO次数才是读写数据的主要要解决的问题

    # 数据库的存储方式
        # 新的数据结构 —— 树
        # 平衡树 balance tree - b树
        # 在b树的基础上进行了改良 - b+树
            # 1.分支节点和根节点都不再存储实际的数据了
                # 让分支和根节点能存储更多的索引的信息
                # 就降低了树的高度
                # 所有的实际数据都存储在叶子节点中
            # 2.在叶子节点之间加入了双向的链式结构
                # 方便在查询中的范围条件
        # mysql当中所有的b+树索引的高度都基本控制在3层
            # 1.io操作的次数非常稳定
            # 2.有利于通过范围查询
        # 什么会影响索引的效率 —— 树的高度
            # 1.对哪一列创建索引，选择尽量短的列做索引
            # 2.对区分度高的列建索引，重复率超过了10%那么不适合创建索引

# 聚集索引和辅助索引
    # 在innodb中 聚集索引和辅助索引并存的
        # 聚集索引 - 主键 更快
            # 数据直接存储在树结构的叶子节点
        # 辅助索引 - 除了主键之外所有的索引都是辅助索引 稍慢
            # 数据不直接存储在树中
    # 在myisam中 只有辅助索引，没有聚集索引

# 索引的种类
    # primary key 主键 聚集索引  约束的作用：非空 + 唯一
        # 联合主键
    # unique 自带索引 辅助索引   约束的作用：唯一
        # 联合唯一
    # index  辅助索引            没有约束作用
        # 联合索引
# 看一下如何创建索引、创建索引之后的变化
    # create index 索引名字 on 表(字段)
    # DROP INDEX 索引名 ON 表名字;
# 索引是如何发挥作用的
    # select * from 表 where id = xxxxx
        # 在id字段没有索引的时候，效率低
        # 在id字段有索引的之后，效率高

 
```

索引不生效的原因

1. 要查询的数据的范围大

   ```python
   索引不生效的原因
       # 要查询的数据的范围大
           # > < >= <= !=
           # between and
               # select * from 表 order by age limit 0，5
               # select * from 表 where id between 1000000 and 1000005;
           # like
               # 结果的范围大 索引不生效
               # 如果 abc% 索引生效，%abc索引就不生效
       # 如果一列内容的区分度不高，索引也不生效
           # name列
       # 索引列不能在条件中参与计算
           # select * from s1 where id*10 = 1000000;  索引不生效
       # 对两列内容进行条件查询
           # and and条件两端的内容，优先选择一个有索引的，并且树形结构更好的，来进行查询
               # 两个条件都成立才能完成where条件，先完成范围小的缩小后面条件的压力
               # select * from s1 where id =1000000 and email = 'eva1000000@oldboy';
           # or or条件的，不会进行优化，只是根据条件从左到右依次筛选
               # 条件中带有or的要想命中索引，这些条件中所有的列都是索引列
               # select * from s1 where id =1000000 or email = 'eva1000000@oldboy';
       # 联合索引  # create index ind_mix on s1(id,name,email);
           # select * from s1 where id =1000000 and email = 'eva1000000@oldboy';
           # 在联合索引中如果使用了or条件索引就不能生效
               # select * from s1 where id =1000000 or email = 'eva1000000@oldboy';
           # 最左前缀原则 ：在联合索引中，条件必须含有在创建索引的时候的第一个索引列
               # select * from s1 where id =1000000;    能命中索引
               # select * from s1 where email = 'eva1000000@oldboy';  不能命中索引
               # (a,b,c,d)
                   # a,b
                   # a,c
                   # a
                   # a,d
                   # a,b,d
                   # a,c,d
                   # a,b,c,d
           # 在整个条件中，从开始出现模糊匹配的那一刻，索引就失效了
               # select * from s1 where id >1000000 and email = 'eva1000001@oldboy';
               # select * from s1 where id =1000000 and email like 'eva%';
   
   # 什么时候用联合索引
       # 只对 a 对abc 条件进行索引
       # 而不会对b，对c进行单列的索引
   
   # 单列索引
       # 选择一个区分度高的列建立索引，条件中的列不要参与计算，条件的范围尽量小，使用and作为条件的连接符
   # 使用or来连接多个条件
       # 在满上上述条件的基础上
       # 对or相关的所有列分别创建索引
   
   # 覆盖索引
       # 如果我们使用索引作为条件查询，查询完毕之后，不需要回表查，覆盖索引
       # explain select id from s1 where id = 1000000;
       # explain select count(id) from s1 where id > 1000000;
   # 合并索引
       # 对两个字段分别创建索引，由于sql的条件让两个索引同时生效了，那么这个时候这两个索引就成为了合并索引
   # 执行计划 : 如果你想在执行sql之前就知道sql语句的执行情况，那么可以使用执行计划
       # 情况1：
           # 30000000条数据
               # sql 20s
               # explain sql   --> 并不会真正的执行sql，而是会给你列出一个执行计划
       # 情况2：
           # 20条数据 --> 30000000
               # explain sql
   
   # 原理和概念
       # b树
       # b+树
       # 聚集索引 - innodb
       # 辅助索引 - innodb myisam
   # SQL索引的创建（单个、联合）、删除
   # 索引的命中：范围，条件的字段是否参与计算(不能用函数)，列的区分度(长度)，条件and/or，联合索引的最左前缀问题
   # 一些名词
       # 覆盖索引
       # 合并索引
   # explain执行计划
   # 建表、使用sql语句的时候注意的
       # char 代替 varchar
       # 连表 代替 子查询
       # 创建表的时候 固定长度的字段放在前面
   ```

### 2.0.1  数据备份和事物

```python
# mysqldump -uroot -p123  day40 > D:\code\s21day41\day40.sql
# mysqldump -h -uroot -p123 --databases new_db > D:\code\s21day41\db.sql


# begin;  # 开启事务
# select * from emp where id = 1 for update;  # 查询id值，for update添加行锁；
# update emp set salary=10000 where id = 1; # 完成更新
# commit; # 提交事务
```



### 2.2 sql注入

```python
# create table userinfo(
# id int primary key auto_increment,
# name char(12) unique not null,
# password char(18) not null
# )
#
# insert into userinfo(name,password) values('alex','alex3714')

# 输入用户
# 输入密码
#
# 用户名和密码到数据库里查询数据
# 如果能查到数据 说明用户名和密码正确
# 如果查不到，说明用户名和密码不对
# username = input('user >>>')
# password = input('passwd >>>')
# sql = "select * from userinfo where name = '%s' and password = '%s'"%(username,password)
# print(sql)
# -- 注释掉--之后的sql语句
# select * from userinfo where name = 'alex' ;-- and password = '792164987034';
# select * from userinfo where name = 219879 or 1=1 ;-- and password = 792164987034;
# select * from userinfo where name = '219879' or 1=1 ;-- and password = '792164987034';

import pymysql

conn = pymysql.connect(host = '127.0.0.1',user = 'root',
                       password = '123',database='day41')
cur = conn.cursor()
username = input('user >>>')
password = input('passwd >>>')
sql = "select * from userinfo where name = %s and password = %s"
cur.execute(sql,(username,password))
print(cur.fetchone())
cur.close()
conn.close()
```

