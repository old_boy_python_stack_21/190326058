# GIT:

什么是GIT：分布式的一个版本控制软件

### 1. 版本控制

  1.进入版本文件夹

2. 初始化      

   ```python
   git init
   ```

   

3. 管理

   ```python
   git status    #检测当前文件夹下的文件状态（修改或新增文件）
   #三种状态
   1. 红色：新增的文件/修改的老文件
   2. 绿色：git 已经管理起来  git commit -m '描述信息'      #git init 将检测不到
   
   git add . #管理所有没有被管理的文件
   git add 文件名   #管理某一个文件
   ```

4. 生成版本

```python
git commit -m 'v1第一个版本'       #生成一个版本
```

### 2. 回滚

```python
git log  显示最近更新的版本信息
git reset --hard 版本号
```

```python
#回滚之后的版本
git reflog     显示所有版本的信息
git reset --hard 版本号
```

### 3. 分支

```python
git branch     查看当前所在的分支

#在master

git branch dev    创建dev分支  

git branch -d bug(分支名)   删除分支

git  checkout dev     切换分支

#在master分支执行
git marge dev  将dev合并到master
```

###### 1. bug修复

```python
创建一个BUG分支，将修复的bug分支代码合并（marge）到master分支中,线上代码稳稳当当
```

```python
在checkout dev 分支   ，继续开发完正在开发的新功能，新功能开发完毕，然后git checkout 到 master分支，将dev分支marge
到master分支上（可能会出现冲突）
解决冲突后，在递交版本，git commit -m '修复bug，和商城功能'

#合并冲突
他不知道怎么合并的时候，会将两个相同的代码同时写入

手动解决：找到代码，手动修复
```

### 4. github递交

```python
git  remote add origin https//github.com/***.git(github仓库)   给仓管起个别名为 origin 
git push -u origin master   将代码递交到master主线
```

2. 下拉

```python
git clone +仓库地址      #将仓库中的内容拉下来
(适用于本地一个文件也没有)

git pull origin master  #适用于更新
（本地有文件，但不是最新的）
```

### 5. 公司与家

在公司进行开发

```python
1. 切换到dev分支进行开发
git checkout dev
2. 把master分支合并到dev  【仅一次】
git merge master
3. 修改代码
4. 递交代码
	git add .
    git commit -m 'xx'
    git push origin dev 
```

回到家中继续写代码

```python
切换到dev分支
git checkout dev
拉代码
git pul origin dev

继续开发

递交代码
  git add .
  git commit -m 'xx'
  git push origin dev
```

去公司继续开发

```python
切换到dev分支
git checkout dev
拉代码
git pul origin dev

继续开发

递交代码
  git add .
  git commit -m 'xx'
  git push origin dev
```

开发完毕上线

```python
1. 将dev分支合并到master ,进行上线
   git checkout master
   git merge  dev
   git push origin master
2. 把dev分支也推送到远程
	git checkout dev
    git merge master
```

```python
git pull origin dev #等价于下面两句

git fetch origin dev   #将远程仓库拉倒版本库
git merge origin/dev   #将版本库和工作区文件合并（基本不用）
```

