# 1、查询男生、女生的人数；
''''''
'''
# mysql> select gender,count(*) from student group by gender;
'''
# 2、查询姓“张”的学生名单；

'''
select * from student where sname like '张%';
'''
# 3、课程平均分从高到低显示
'''
mysql> select avg(num),course_id from score group by course_id order by avg(num) desc;
+----------+-----------+
| avg(num) | course_id |
+----------+-----------+
|  85.2500 |         4 |
|  65.0909 |         2 |
|  64.4167 |         3 |
|  53.4167 |         1 |
+----------+-----------+
4 rows in set (0.00 sec)
'''
# 4、查询有课程成绩小于60分的同学的学号、姓名；
'''
mysql> select student.sid,sname,num from student inner join  score on student.sid = score.student_id where num<60;
+-----+--------+-----+
| sid | sname  | num |
+-----+--------+-----+
|   1 | 理解   |  10 |
|   1 | 理解   |   9 |
|   2 | 钢蛋   |   8 |
|   4 | 张一   |  11 |
|   5 | 张二   |  11 |
|   6 | 张四   |   9 |
|   7 | 铁锤   |   9 |
|   8 | 李三   |   9 |
|   9 | 李一   |  22 |
|  10 | 李二   |  43 |
|  11 | 李四   |  43 |
|  12 | 如花   |  43 |
+-----+--------+-----+
'''
# 5、查询至少有一门课与学号为1的同学所学课程相同的同学的学号和姓名；
'''
mysql> select sname,sid from student inner join (select f.student_id from(select student_id from score inner join (select course_id from score where student_id=1) as d on score.course_id=d.course_id) as f group by student_id) as p on student.sid=p.student_id;
+--------+-----+
| sname  | sid |
+--------+-----+
| 理解   |   1 |
| 钢蛋   |   2 |
| 张三   |   3 |
| 张一   |   4 |
| 张二   |   5 |
| 张四   |   6 |
| 铁锤   |   7 |
| 李三   |   8 |
| 李一   |   9 |
| 李二   |  10 |
| 李四   |  11 |
| 如花   |  12 |
+--------+-----+
'''
# 6、查询出只选修了一门课程的全部学生的学号和姓名；
'''
mysql> select sid,sname from student inner join (select student_id from score group by student_id having sum(student_id)= student_id) as d on student.sid=d.student_id;
+-----+--------+
| sid | sname  |
+-----+--------+
|  13 | 刘三   |
+-----+--------+
'''
# 查询各科成绩最高和最低的分：以如下形式显示：课程ID，最高分，最低分
'''
select course_id,max(num),min(num) from score group by course_id;
+-----------+----------+----------+
| course_id | max(num) | min(num) |
+-----------+----------+----------+
|         1 |       91 |        8 |
|         2 |      100 |        9 |
|         3 |       87 |       43 |
|         4 |      100 |       22 |
+-----------+----------+----------+
'''
# 查询课程编号“2”的成绩比课程编号“1”课程低的所有同学的学号、姓名
'''
mysql> select student.sname,sid from student inner join (select a.student_id from(select * from score where course_id=1)as a
 inner join (select * from score where course_id=2) as b 
on a.student_id=b.student_id where a.num>b.num)as c on student.sid=c.student_id;
'''
# 查询“生物”课程比“物理”课程成绩高的所有学生的学号；
'''
mysql> select b.student_id from (select * from score inner join (select * from course where cid=1) as a on score.course_id=a.cid) as b
inner join (select * from score inner join(select * from course where cid=2) as a 
on score.course_id=a.cid) as c on b.student_id=c.student_id where b.num>c.num;
'''
# 查询平均成绩大于60分的同学的学号和平均成绩;
'''
mysql> select student_id,avg(num) from score group by student_id having avg(num)>60;
+------------+----------+
| student_id | avg(num) |
+------------+----------+
|          3 |  82.2500 |
|          4 |  64.2500 |
|          5 |  64.2500 |
|          6 |  69.0000 |
|          7 |  66.0000 |
|          8 |  66.0000 |
|          9 |  67.0000 |
|         10 |  74.2500 |
|         11 |  74.2500 |
|         12 |  74.2500 |
|         13 |  87.0000 |
+------------+----------+
'''
# 11、查询所有同学的学号、姓名、选课数、总成绩；
'''
mysql> select sname,sid,s.cou,s.sumnum from student inner join
    -> (select student_id,count(* ) as cou ,sum(num)as sumnum from score group by student_id) as s
    -> on student.sid=s.student_id;
+--------+-----+-----+--------+
| sname  | sid | cou | sumnum |
+--------+-----+-----+--------+
| 理解   |   1 |   3 |     85 |
| 钢蛋   |   2 |   3 |    175 |
| 张三   |   3 |   4 |    329 |
| 张一   |   4 |   4 |    257 |
| 张二   |   5 |   4 |    257 |
| 张四   |   6 |   4 |    276 |
| 铁锤   |   7 |   4 |    264 |
| 李三   |   8 |   4 |    264 |
| 李一   |   9 |   4 |    268 |
| 李二   |  10 |   4 |    297 |
| 李四   |  11 |   4 |    297 |
| 如花   |  12 |   4 |    297 |
| 刘三   |  13 |   1 |     87 |
+--------+-----+-----+--------+
'''
# 12、查询姓“李”的老师的个数
'''
mysql> select * from teacher where tname like '张%';
+-----+--------------+
| tid | tname        |
+-----+--------------+
|   1 | 张磊老师     |
+-----+--------------+
'''
# 13、查询没学过“张磊老师”课的同学的学号、姓名；
'''
mysql> select sname,sid from student inner join(select score.student_id as cstudent_id,e.student_id as sstudent from score left join (select student_id from score inner join (select cid,tname from(select *
    -> from teacher where tname  like '张%')as a inner join (select *
    -> from course)as b on a.tid = b.cid)as b on score.course_id=b.cid) as e on score.student_id=e.student_id group by cstudent_id)as h on student.sid=h.cstudent_id where h.sstudent is NULL;
+--------+-----+
| sname  | sid |
+--------+-----+
| 刘三   |  13 |
+--------+-----+
'''
# 查询学过“1”并且也学过编号“2”课程的同学的学号、姓名；
'''
mysql> select sname,student.sid from student inner join (select * from (select * from score where course_id=1 or course_id=2) as a group by student_id having sum(course_id)=3)
    -> as d on student.sid=d.student_id;
+--------+-----+
| sname  | sid |
+--------+-----+
| 理解   |   1 |
| 张三   |   3 |
| 张一   |   4 |
| 张二   |   5 |
| 张四   |   6 |
| 铁锤   |   7 |
| 李三   |   8 |
| 李一   |   9 |
| 李二   |  10 |
| 李四   |  11 |
| 如花   |  12 |
+--------+-----+
'''
# 查询学过“李平老师”所教的所有课的同学的学号、姓名；
'''
mysql> select sname,sid from student inner join( select student_id from(select * from score inner join (select * from teacher inner join (select * from course)as a on teacher.tid=a.teacher_id where teacher.tid=2)as c on score.course_id=c.cid) as f group by student_id)as e on student.sid=e.student_id;
+--------+-----+
| sname  | sid |
+--------+-----+
| 理解   |   1 |
| 钢蛋   |   2 |
| 张三   |   3 |
| 张一   |   4 |
| 张二   |   5 |
| 张四   |   6 |
| 铁锤   |   7 |
| 李三   |   8 |
| 李一   |   9 |
| 李二   |  10 |
| 李四   |  11 |
| 如花   |  12 |
+--------+-----+
'''
# -------------------------------------------------------------------------
# 后25
# 查询没有学全所有课的同学的学号、姓名；
'''
mysql>  select sname,student.sid from student inner join (select * from score group by student_id having count(course_id)<4)as a on student.sid=a.student_id;
+--------+-----+
| sname  | sid |
+--------+-----+
| 理解   |   1 |
| 钢蛋   |   2 |
| 刘三   |  13 |
+--------+-----+
'''
# 查询和“002”号的同学学习的课程完全相同的其他同学学号和姓名；
'''
mysql> select a.student_id from(select student_id,sum(course_id)as aaa  from
    -> score group by student_id)as a inner join
    -> (select student_id,sum(course_id)as bbb from score where student_id=2)as b
    -> on a.aaa=b.bbb;
+------------+
| student_id |
+------------+
|          2 |
+------------+
'''
# 删除学习“叶平”老师课的SC表记录；