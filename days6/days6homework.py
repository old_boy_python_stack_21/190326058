# 列举你了解的字典中的功能（字典独有）
'''

'''
'''
keys()     values()   items()
get()  update()
'''
# 列举你了解的集合中的功能（集合独有）
'''
# crlt建鼠标左键点击set 查看方法
add()
discard()
set
uinon（）
clear（）
intersection（）
difference（）
'''
# 列举你了解的可以转换为 布尔值且为False的值。
'''
'',0,[],(),{},set()
'''
# 请用代码实现
# info = {'name':'王刚蛋','hobby':'铁锤'}
# 循环提示用户输入，根据用户输入的值为键去字典中获取对应的值并输出。
# 循环提示用户输入，根据用户输入的值为键去字典中获取对应的值并输出
# （如果key不存在，则获取默认“键不存在”，并输出）。
# 注意：无需考虑循环终止（写死循环即可）

info = {'name':'王刚蛋','hobby':'铁锤'}
'''
while True:
    key = input('请输入：')
    print(info[key])
'''
'''
while True:
    key = input('请输入：')
    print(info.get(key,'建卜存子'))
'''
# 请用代码验证 "name" 是否在字典的键中？
info = {'name':'王刚蛋','hobby':'铁锤','age':'18'}
'''
result = False
for i in info:
    if i == 'name':
        result = True
        break
if result == True:
    print('在里面')
else:
    print('buzai ')
'''
# 请用代码验证 "alex" 是否在字典的值中？
# #
# # info = {'name':'王刚蛋','hobby':'铁锤','age':'18',...100个键值对}
'''
result = False
for i in info:
    if info.get(i) == 'name':
        result = True
        break
if result == True:
    print('在里面')
else:
    print('buzai ')
'''
# 请得到 v1 和 v2 的交集并输出
# 请得到 v1 和 v2 的并集并输出
# 请得到 v1 和 v2 的 差集并输出
# 请得到 v2 和 v1 的 差集并输出
'''
v1 = {'武沛齐','李杰','太白','景女神'}
v2 = {'李杰','景女神'}
v3 = v1.intersection(v2)
v4 = v1.union(v2)
v5 = v1.difference(v2)
v6 = v2.difference(v1)
print(v3,v4,v5,v6)
'''
# 循环提示用户输入，并将输入内容追加到列表中（如果输入N或n则停止循环）
'''
l = []
while True:
    a = input('请输入内容：')
    if a.upper() == 'N':
        break
    else:
        l.append(a)
print(l)
'''
# 循环提示用户输入，并将输入内容添加到集合中（如果输入N或n则停止循环）
'''
l = set()
while True:
    a = input('请输入内容：')
    if a.upper() == 'N':
        break
    else:
        l.add(a)
print(l)
'''
v1 = {'alex','武sir','肖大'}
v2 = []

# 循环提示用户输入，
# 如果输入值在v1中存在，则追加到v2中，如果v1中不存在，
# 则添加到v1中。（如果输入N或n则停止循环）
'''
v1 = {'alex','武sir','肖大'}
v2 = []
while True:
    a = input('亲输入：')
    if a.upper() == 'N':
        break
    else:
        for i in v1:
            if a == i:
                v2.append(a)
        v1 .add(a)
print(v1,v2)
'''
# 判断以下值那个能做字典的key ？那个能做集合的元素？
#
# 1     字典的建  集合的元素
# -1     字典的建，集合的元素
# ""     字典的建，集合的元素
# None    字典的建 ，集合的元素
# [1,2]    都不能
# (1,)     都能
# {11,22,33,4}    都不能
# {'name':'wupeiq','age':18}      都不能

# is 和 == 的区别？
# is比较的事内存地址
# ==  比较的是值

# type使用方式及作用？
type(v1)   #查看v1的数据类型

# id的使用方式及作用？
id(v1)    #查看vi内存地址

#
v1 = {'k1':'v1','k2':[1,2,3]}
v2 = {'k1':'v1','k2':[1,2,3]}

result1 = v1 == v2
result2 = v1 is v2
print(result1)
print(result2)
'''
v1v2两个内存地址中的值相同，所以result1 为真
v1v2的内存地址不同，所以result2  为假

'''

v1 = {'k1':'v1','k2':[1,2,3]}
v2 = v1

result1 = v1 == v2
result2 = v1 is v2
print(result1)
print(result2)

'''
都是真，v1v2指向的同一个内存地址   所以同一个内存地址值相同，result1 = True
v1v2内存地址相同所以result2 为真
'''

v1 = {'k1':'v1','k2':[1,2,3]}
v2 = v1

v1['k1'] = 'v1'
print(v2)

'''
# v2 跟v1 为同一个地址，所以v1内存地址的值变了，所以v2额变了
# 打印{'k1':'v1','k2':[1,2,3]}
'''


v1 = '人生苦短，我用Python'
v2 = [1,2,3,4,v1]

v1 = "人生苦短，用毛线Python"

print(v2)
'''
# v2 为 [1,,2,3,4,'人生苦短，我用python']
# v1内存地址改变了，列表里的v1 内存地址没有改变
'''

info = [1,2,3]
userinfo = {'account':info, 'num':info, 'money':info}

info.append(9)
print(userinfo)

info = "题怎么这么多"
print(userinfo)
'''
# 第一次改变  因为 修改的是内存地址中的值
# 第二次不变 ，因为他把指向的地址修改了
'''

info = [1,2,3]
userinfo = [info,info,info,info,info]

info[0] = '不仅多，还特么难呢'
print(info,userinfo)
# 都改变，因为内存地址指向的值发生了改变

info = [1,2,3]
userinfo = [info,info,info,info,info]

userinfo[2][0] = '闭嘴'
print(info,userinfo)
''''''
# 都改变，修改的为内存地址指向的值


info = [1, 2, 3]
user_list = []
for item in range(10):
    user_list.append(info)

info[1] = "是谁说Python好学的？"

print(user_list)
'''
[1, '是谁说Python好学的？', 3]
# 列表的都发生改变
'''

data = {}
for item in range(10):
    data['user'] = item
print(data)
'''
打印{'user':9}
因为改变了内存地址中的值
'''

data_list = []
data = {}
for item in range(10):
    data['user'] = item
    data_list.append(data)
print(data,data_list)

'''
字典为   {'user':9}
列表为   [{'user':9},{'user':9},{'user':9},...]
因为每次都是修改的内存地址指向的值   他们都指向同一内存地址，所以都改变
'''

data_list = []
for item in range(10):
    data = {}
    data['user'] = item
    data_list.append(data)
print(data,data_list)
'''
{'user':9}
[{'user':0},{'user':1},{'user':2},...]
因为每次循环data字典的内存地址都会变更
'''


