jupyter  notebook  测试软件是否安装成功



1. Anaconda
   - 集成环境:基于数据分析和机器学习的开发环境
     - 几乎不用安装其他插件,内部都封装好了

2. jupyter  :  俗成  超级终端,就是Anaconda集成环境中提供的一种基于浏览器的可视化开发工具

3. cell  两种模式
   1. - code模式       : 编写python程序
      - markdown模式    :编写笔记

4. 快捷键

   - 插入cell  :a,b
   - 删除cell : x
   - 切换cell 的模式  :y,m
   - 执行:shift +enter
   - tab :补全
   - shift + tab:  打开帮助文档

### 5. http

- 概念 : clinet和serer 进行数据交互的某种形式

6. 常用的头信息

   1. User_Agent  : 请求载体的身份标识     请求载体:浏览器
   2. Connection : keep-alive    表示一个长连接
   3. Connection : close   表示当前请求执行完成之后马上关闭
   4. content-type  服务器返回回来response的类型

### 6. https

 抓取https的请求需要讲folder安装一个证书

- 概念: 安全的http协议
- 证书 :
  - 最早  对称秘钥加密
    - 客户端  加密过后的内容和钥匙  发送给服务器
      - 隐患 :可能中途拦截,获取直接解密
  - 之后  非对称秘钥加密   不一定是指定的服务器发送的秘钥
    - 服务器将 加密方式传给客户端 ,客户端将加密字符串发给服务端
      - 隐患:  可以伪造服务器 向客户端发送加密方式
  - 现在广泛应用 :证书秘钥加密方式
    - 经过防伪的公钥
    - 证书认证机构发送
    - 客户端拿到加密的公钥加密msg 传给服务器

### 7. 爬虫的概念:

- 通过编写程序模拟浏览器上网,然后让其取互联网上爬取/抓取/采集数据的过程
  - 模拟: 浏览器是一款纯天然的爬虫工具

###### 1. 爬虫分类

- 通用爬虫 : 爬取一整张页面的数据,抓取系统(爬虫程序)
- 聚焦爬虫 :  爬取页面中局部数据,一定是建立在通用爬虫基础之上
- 增量式爬虫  :  用来监测 网站数据更新的情况.以便爬取到网站最新更新出来的数据                需求(爬过的不爬了,只爬新更新的)
- 风险分析:
  - 干扰了被访问网站的正常运营
  - 爬取了收法律保护的信息
- 如何避免风险
  - 严格遵守网站设置的robots协议
  - 在规避反爬虫措施的同时,需要优化自己的代码,避免干扰被访问网站的正常运行
  - 在使用,传播抓取到的信息时,应审查抓取的内容,如发现属于用户的个人信息,隐私或他人的商业秘密,应及时停止

###### 2 . 反爬机制

1. UA检测   检测user_Agent 

2. 图片懒加载 :在img标签用了伪属性

3. cookie

4. 动态请求参数,每次请求对应的请求参数是变化的

   

###### 3. 反反爬机制

1. UA伪装    

2. ConectionPool报错原因:

   1. 连接池满了

      contention:close

3. cookie

   1. 手动处理
      - 在抓包工具中捕获cookie,将其封装到headers
      - 应用场景:cookie没有有效时长,且不是动态变化的
   2. 自动处理
      - 使用session机制
      - 应用场景:动态变化的cookie
      - session对象:该对象和requests模块用法几乎一致.如果在请求的过程中产生了cookie,如果该请求使用session发起的,则cookie会自动存储到session中.

4. 通常情况,动态的参数都隐藏在前台页面中

###### 4. robots.txt协议

文本协议,在文本中指定了可爬和不可爬的数据,,

###### 5. 答辩

1. 需求文档
   1. 项目背景介绍
   2. 功能模块的介绍
   3. 业务逻辑
2. 数据量级  >=10000
3. 设计业务逻辑(数据分析 + 机器学习)
4. 数据类型:
   1. 电商
   2. 新闻资讯
   3. 房产,招聘
   4. 医疗
   5. 等等......

###### 6. requests 模块

- 一个基于网络请求的模块
  - 作用:模拟浏览器向服务器请求数据
- 编码流程:
  - 指定url
  - 进行请求的发送
  - 获取响应数据  (爬取到的数据)
  - 持久化存储  (保存到数据库,或文件)
- 如何判定那条请求是如何定位
  - 使用抓包工具做局部搜索,定位到
- 对一个陌生的网站数据进行爬取前一定要判定是否是动态加载的

###### 7.第一个练习

​	爬取搜狗首页对应的页面源码

###### 8.第二个练习 

​	对搜狗的搜索结果进行爬取   UA伪装

```python
wd = input('输入搜索内容')
url = ********/web'
params = {
    'query':wd
}
#将 params 作用大请求中
#params参数表示的是对请求url参数的封装

#即将发起请求对应的头信息
#用来实现UA伪装
headers = {
	User_Agent : 取浏览器复制一个
}
response = requests.get(url=url,params=params,headers=headers)
response.encoding = 'utf-8'
with open(filename,'w',encoding='utf-8') as f:
    f.write(response.text)
```



###### 8. 第三个案例

爬取豆瓣电影详情

```python
动态加载的数据:通过另一个额外的请求请求到的数据
    - ajax 
    - js生成动态加载的数据
```

```python
url = '************'
start = input('askjdaks:')
limit = input('akjdjll:')
params = {
    key:value,
    start:start,
    limit:limit
}
headers = {
	User_Agent : 取浏览器复制一个
}
response = requests.get(url,params=params,headers=headers)
response.encoding='utf-8'
text = response.json()    #返回一个反序列化好的对象
#遍历列表 :
fp = open(filename,'w',encoding='utf-8')
for dic in text:
    name = dic['title']
    score = dic['score']
	fp.write(name+':'+score+'\n')
    print(name,'爬取成功')
fp.close()
```

###### 9. 第四个示例

爬取肯德基官网

```python
#data 表示get方法中的params
data = {
    key:value
}
request.post(url=url,data=data,headers=headers)
response.json()
```

###### 10. 第5个案例

爬取药监总局每家详情数据

```python

```

###### 11. 数据解析的通用原理

需要的数据一定是在标签中或标签属性中

- 通用原理:
  - 标签定位
  - 取文本或属性

###### 12. 正则数据解析

1. 爬取糗事百科

2. 爬取图片数据的知识点

```python
#方式一
import requests
img=requests.get(url,headers).content()   #content 返回的是byte类型的数据
with open(file,'wb') as f:
    f.write(img)
```

```python
#方式二    不可以使用UA伪装的机制
from urllib import request
request.urlretrieve(url,'filename.jpg')
```

3.  urllib 就是一个比较老的网络请求的模块,在requests模块没有出现之前,请求发送的操作使用的都是urllib 

   requests 能实现的urllib不一定能实现

```python
import os
if  not os.pah.exists(dir_name)
os.mkdir(dir_name)
page_text = requests.get(url,headers).text
re.findall()
img_path = dir_name + '/' + img_name 
```

### bs4 解析

环境安装 : bs4   lxml

bs4 的解析原理

​	-实例化一个BeautifulSoup 的对象,并且将即将被解析的页面源码数据加载到该对象中

- 调用BeautifulSoup对象中的相关属性和方法 进行标签定位和数据提取

如何实例化BeautifulSoup 对象:

- BeautifulSoup  (文件句柄,'lxml'):专门用作于解析本地存储的HTML文档中的数据
- BeautifulSoup(page_text,'lxml')  专门用作将互联网上请求到的页面源码数据进行解析

```python
#使用
soup = BeautifulSoup  (文件句柄,'lxml')
soup.p 第一个p标签对应的完整的p标签  
1. soup.tagName   #定位到第一个标签    返回单数
2. soup.find('div',class_='song')   #属性定位,根据属性定位标签
	soup.find('tagName',attrName_='value')   返回单数
    
3. find_all:  与find用法一致,返回列表
    soup.find_all('div',class_='song')
4. select('选择器') 根据选择器定位   返回值为列表
	soup.select('.tang')
5. soup.select('.tang > ul > li')层级  返回所有符合条件的li标签  
li_6 = soup.select('.tang > ul > li')[6]  定位到第6个li标签
	soup.select('.tang  li')  空格表示多个层级,大于号表示1个层级
```

```python
#提取数据
1. 取文本
li.string   获取标签中直系的文本内容
li.text  获取标签中所有的内容   
2. 取属性
soup.find('a',id='feng')['href']    直接取到属性
```

### xpath

```python
环境安装   pip install lxml
#表达式中不可以出现tbody,否则可能会失效
xpath解析原理
	实例化一个etree 类型的对象,且将源码数据加载到该对象中
    需要调用该对象的xpath方法结合着不同形式的xpath表达式进行标签定位和数据提取
1. #对象实例化
	etree.parse(filename)    本地文件
    etree.HTML(patg_text)    爬取的网咯文件
2. xpath 方法 返回的永远是个列表
```

```python
#基于标签定位的xpath方法
- 在xpath表达式中最左侧的/表示的含义是说,当前定位的标签必须从根节点开始定位
- xpath表达式中最左侧的//表示可以从任意位置进行标签定位
- xpath 表达式中非最左侧的//表示多个层级的意思
- xpath 表达式中非最左侧的/表示1个层级的意思


#属性定位
1. xpath("//tagNanme[@class='value']")


#索引定位
xpath("//tagNanme[@class='value']/ul/li[3]")  索引值从1开始


#提取数据
1. 取文本
tree.xpath('//p[1]/text()')  取直系的文本内容
tree.xpath('//div[@class='value']//text()')  取所有的文本内容

2. 取属性
tag/@attrName


```

```python
from lxml import etree
tree = etree.parse('**.html')
tree.xpath('/html/head/meta')[0]   列表取索引    #相当于绝对路径
tree.xpath('//meta')[0]  与上一行结构相同      #相当于相对路径 (相对于html为根)
#属性定位
```

###### xpath练习

1.爬取boss相关招聘信息

- 岗位名称
- 公司名称
- 薪资
- 岗位描述

```python

```

### 代理

代理的匿名度:

- 透明:
- 匿名:
- 高匿名:

代理的类型:

- http
- https

参数:

proxies=('https':'IP+端口')  

### 免费的代理IP网站

- 快代理
- 西祠代理  www.xicidaili.com
- goubanjia   
- 推荐代理精灵   zhiliandaili.cn

###### 代理IP检测

response.code

###### IP被禁解决方案

- 使用代理
- 构建一个代理池
- 拨号服务器

```python
import requests
from requests import Session
from lxml import etree
import time
import random

s = Session()

headers = {

'User-Agent':'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36',
'Connection':'close'
}

url = 'http://t.11jsq.com/index.php/api/entry?method=proxyServer.generate_api_url&packid=1&fa=0&fetch_key=&groupid=0&qty=10&time=1&pro=&city=&port=1&format=html&ss=5&css=&dt=1&specialTxt=3&specialJson=&usertype=2'

response = requests.get(url,headers=headers).text

tree = etree.HTML(response)
ip_list = tree.xpath('//body//text()')
lis = []
for ip in ip_list:
    dic = {}
    dic['https'] = ip
    lis.append(dic)
print(lis)

lib = []
def get_request(count):

        print('555',count)
        try:
            a = random.choice(lis)
            response = requests.get(f'https://www.kuaidaili.com/free/inha/{count}/',headers=headers,proxies=a).text
        except:
            lis.remove(a)
            get_request(count)
        print('6666')
        tree = etree.HTML(response)
        tr_lis = tree.xpath('//tbody/tr')
        for td in tr_lis:
            print('777')
            ip = td.xpath('./td[1]/text()')[0]
            print(ip)
            port = td.xpath('./td[2]/text()')[0]
            type = td.xpath('./td[4]/text()')[0]
            lib.append(f'{type}:{ip}:{port}'+'\n')
            print('完成')


from multiprocessing.dummy import Pool
pool = Pool(10)
response_text_list = pool.map(get_request,list(range(1,1000))) #使用自定义的函数func异步的处理urls列表中的每一个列表元素
print(response_text_list)
f = open('a.txt', 'a', encoding='utf-8')
f.write(lib)
f.close()
```



### session   (更占用资源)

获取session对象

seeion = requests.Session()  #第一次发请求并没有cookie,设置cookie才会保存在session中    

###### 验证码识别

相关打码平台

- 打码兔
- 云打码
- 超级鹰
  - 注册,登录,用户中心的身份认证
  - 登录后:
    - 创建一个软件,点击然健ID,生成软件ID
    - 下载示例代码

### 高效率线程池

```python
def get_request(url):   #必须有一个参数
    return 

from mutiprocessing.dummy  import Pool
pool = Pool(10)
pool.map(func,urls) #使用自定义的函数,异步的处理urls列表中的每一个列表元素,func必须有1个参数
```

### 单线程+多任务异步携程

- 一个线程开500个携程效率最佳

- 携程:对象,可以把携程当做是一个特殊的函数,如果一个函数的定义被asyinc关键字所修饰.该特殊的函数被调用后,函数内部的程序语句不会被立即执行,而是返回一个携程对象

- 任务对象(task):所谓的任务对象就是对携程对象的进一步封装,在任务对象中可以实现显示携程对象的运行状况

  任务对象最终是需要被注册到时间循环对象中.

  - 核心:绑定回调:
    - 回调函数是绑定给任务对象,只有当任务对象对应的特殊函数被执行完毕后,回调函数才开始执行

- 事件循环对象:  无限循环的对象,也可以把其当成是某一种容器.该容器中需要放置多个任务对象.(就是一组待执行的代码块)

  - 异步体现:当事件循环开启后,该对象会按照顺序执行每一个任务,当一个任务对象发生了阻塞,事件循环是不会等待,而是直接执行下一个任务对象,当第一个任务对象阻塞结束,会立刻切回该任务对象.

- await:挂起的操作  ,交出CPU的使用权  ,阻塞操作马上交出CPU使用权,认为设置

- 待执行的代码块中㐓出现不支持异步模块的代码,

- 阻塞必须出现await挂起操作修饰



```python
import asyncio

#回调函数    必须有一个参数
#默认参数:任务对象(调用回调函数对象)
def callback(task):
    print(task.result())  #返回任务对象对应的特殊函数的返回值

async def get_requesst(url):
    print(正在请求,url)
    time.sleep(2)
    print()
    return 'hello'
c = get_request('www.i.com')

#任务对象
task = asyncio.ensure_future(c)

#给任务对象绑定回调函数
task.add_done_callback(callback)

#创建一个事件循环对象
loop = asyncio.get_event_loop()
#将任务对象注册到事件循环对象中,并且开启了事件循环
loop.run_until_complete(task)

```

```python
loop.run_until_complete(asyncio.wait([task,a1,a2]))

```

多任务的异步携程

- 注意事项:

  - 将东哥任务对象存储到一个列表中,然后将该列表注册到时间循环中,在注册的过程张,需要被asyncio.wait方法进行处理

  - 在任务对象对应的特殊函数内部的实现中,不可以出现不支持异步模块的代码,

    否则就会中断整个的异步效果,并且,在该函数内部每一组阻塞的操作都必须使用await关键字进行修饰.

  - requests模块对应的代码不可以出现在特殊函数内部,因为requests是一个不支持异步的模块

### aiohttp: 支持异步操作的网络请求的模块

```python
#代理ip   proxy= "http://ip+端口"
#每一个with前都需要加 async
#在每一步阻塞操作前加上await
async def req(url):
    async with aiohttp.ClientSession()  as s:
        async with await s.get(url) as response:
            #response.read()  返回byte
            page_text = await response.text()  #括号   返回字符串,阻塞操作
            return page_text
```



### 基于aiohttp和 asyincio 异步多任务爬取图片

```python
import aiohttp
import asyncio
import aiofiles
from lxml import html

headers = {
'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36'
    ,'Connection': 'close'
}

#定义异步函数
async def req(url):
    async with aiohttp.ClientSession() as s:
        async with await s.get(url,headers=headers) as response:

            #response.read()  返回byte
            page_text = await response.text()  #括号  返回字符串,阻塞操作

            li_list = html.fromstring(page_text).xpath('//*[@id="main"]/div[3]/ul/li')  # 获取网页标题

            for i in li_list:
                url_pic = 'http://pic.netbian.com' + i.xpath('./a/@href')[0]

                async with await s.get(url_pic,headers=headers) as response1:
                    print(url_pic)
                    new_response = await response1.text()
                    title = html.fromstring(new_response).xpath('//*[@id="img"]/img/@alt')[0]
                    print(title,type(title),'--------------------------------------------------------------------------------------')
                    src = 'http://pic.netbian.com' + html.fromstring(new_response).xpath('//*[@id="img"]/img/@src')[0]
                    print(src)
                    async with await s.get(src,headers=headers) as response:
                        img = await response.content.read()
                        print(img)
                        with open(f'img/{title}.jpg','wb') as f:
                            f.write(img)
                            print(f'{title}下载完成')
            return page_text

#callback函数,未写,应返回二进制遍,callback函数写入文件保存
def callback(task):
    print(task.result())

#生成异步的携程对象  后创建 task任务对象
lis = []
for i in range(2,186):
    c = req(f'http://pic.netbian.com/4kfengjing/index_{i}.html')
    task = asyncio.ensure_future(c)
    lis.append(task)
#创建事件循环,将任务对象列表加入到事件循环中
loop = asyncio.get_event_loop()
loop.run_until_complete(asyncio.wait(lis))
```



### selenium模块

- 概念 : 基于浏览器自动化的一个模块
- 环境安装 :下载模块
- selenium 和爬虫之间的关联是什么
  - 便捷的获取页面中动态加载的数据
  - 实现模拟登陆
- requests模块进行数据爬取:可见非可得
- selenium :可见即可得

###### 基本操作:

​	谷歌浏览器驱动程序下载地址: 

​	驱动程序和谷歌版本的映射关系表:

```python
from selenium import webdriver
#实例化浏览器对象  弹出浏览器
bro = webdriver.Chrome(executalbe_path='')
#executalbe_path  每个浏览器都有这个参数,放谷歌驱动程序的绝对路径


#制定行为动作
bro.get(url) #用户发起请求,显示当前请求页面

#制定窗口录入
#find系列函数用作于标签定位
text_input = bro.find_element_by_id('key')
text_input.send_keys('mac')

btn = bro.find_enement_by_xpath('')#定位到点击按钮
btn.clike()#点击事件

#执行js代码
jscode = window.scrollTo(0,document.body.scrollHeight)  #向下滑动一屏高度

bro.execute_script(jscode) #执行js代码

sleep(3)#睡3秒
bro.qute()#关闭浏览器
```

```python
#返回的是浏览器打开的页面数据
page_text = bro.page_source

tree = etree.HTML(page_text)
tree.xpath('')
```

###### 动作链:一系列的行为动作   

```python
如果定位 的标签是在子页面中     (iframe标签内)
在进行标签定位之前要执行  switch_to的操作
bro.switch_to.frame('iframeResult')
div_tag = bro.bind_element_by_id('')  #定位div

#实例化动作链对象
action = ActionChains(bro)
action.click_and_hold(div_tag)     #保持点击  百度搜索模拟人的滑动

#preform()让动作链立即执行
for i in range(5):
    ActionChains(bro).move_by_offset(17,0).perform()  #向右移动17个像素
    sleep(0.5)
action.release()#释放动作链

```

###### 无头浏览器:无可视化界面的浏览器

- phantomJS: 无可视化界面的浏览器(一般不用,已不再更新)
- Chrom :

```python
#截图功能
bro.save_screenshot('1.png')
#无头使用 谷歌  
from selenium.webdriver.chrome.options import Options
from time import sleep
from selenium import webdriver

# 创建一个参数对象，用来控制chrome以无界面模式打开
chrome_options = Options()
chrome_options.add_argument('--headless')
chrome_options.add_argument('--disable-gpu')

#实例化一个浏览器对象
bro = webdriver.Chrome(executable_path=r'C:\Users\oldboy-python\Desktop\爬虫+数据\day04\chromedriver.exe',chrome_options=chrome_options)
bro.get('https://www.baidu.com')
sleep(2)
bro.save_screenshot('1.png')
print(bro.page_source)
sleep(2)
bro.quit()
```

###### selenuim规避风险(破解反扒机制)

```python
#window.navigator.webdriver   反爬原理 判断这个参数的值
# ChromeOptions,实例化浏览器传option参数,可以反反爬

from time import sleep
from selenium import webdriver
from selenium.webdriver import ChromeOptions
option = ChromeOptions()
option.add_experimental_option('excludeSwitches', ['enable-automation'])

#实例化一个浏览器对象
bro = webdriver.Chrome(executable_path=r'C:\Users\oldboy-python\Desktop\爬虫+数据\day04\chromedriver.exe',options=option)
bro.get('https://www.taobao.com/')
```

### 12306登录

```python
#截取图片
bro.save_screenshot('main.png')
code_img_ele = bro.find_element_by_xpath('img')
#location 图片左下角坐标,size 图片的长宽
location = code_img_ele.location
size = code_img_ele.size
3
#指定区域
rangle =(int(location['x']),int(location['y']),int(location['x']+size['width']),int(location['y']+size['height']))

i  = Image.open('main.png')
frame = i.crop(rangle)
frame.save('code.png')
```

### 抓包工具

##### 推荐:

- fodder
- 青花瓷
- miteproxy

1. 移动端数据爬取

2. fodder和设备应在同一个wifi下进行连接

3. 在手机上浏览器中访问,http://fillder所在的pc机的ip的ip:58888/

   获取子页面进行证书的下载和安装,信任操作

4. 配置手机代理:将手机的代理配置成fiddler所对应的PC机的ip和fillder自己的端口
5. 就可以让fillder捕获手机发起的http和https的请求

### scrapy,pyspider 框架

什么是框架,如何学习框架

- 就是一个集成了各种可以被应用在各种不同的需求中功的一个项目末班能且具有很强通用性()

scrapy 封装的功能: 异步框架

- 高性能的数据解析操作,持久化存储操作,高新更数据下载的操作..

环境的安装:

```python
- 环境的安装:
      a. pip3 install wheel

      b. 下载twisted http://www.lfd.uci.edu/~gohlke/pythonlibs/#twisted

      c. 进入下载目录，执行 pip3 install Twisted‑17.1.0‑cp35‑cp35m‑win_amd64.whl

      d. pip3 install pywin32

      e. pip3 install scrapy
```

创建项目:

```python
- scrapy的基本使用
    - 创建一个工程:scrapy startproject firstBlood
    - 必须在spiders这个目录下创建一个爬虫文件
        - cd proName
        - scrapy genspider spiderName www.xxx.com
    - 执行工程:scrapy crawl spiderName
- settings.py:
    - 不遵从robots协议
    - 进行UA伪装
    - 进行日志等级设定:LOG_LEVEL = 'ERROR'    #只在终端输出
    - LOG_file = '.\log.txt'                #写入到log文件中
```

项目结构:

```python

```

```python
xpath  text 返回的都是对象
#extract 取data的值
/text()取[0].extract()
/text() .extract_first()

直接调用extract表示 将该方法直接作用到每一个元素中
//text()    直接点extract()
```



```python
持久化存储
1. 基于终端指令的持久化存储 
	特性:只可以将parse方法的返回值写入到本地的磁盘文件中
     指令:scrapy  crawl qiubai(爬虫文件名) -o qiubaifile.csv
2. 基于管道的持久化存储
	实现流程:
        1. 数据解析
        2. 将解析的数据存储或者封装到一个item类型的对象中
        3. 将解析的数据存储封装到一个item类型的对象(items文件中对应类的对象)
        4. 向管道递交item
        5. 在管道文件的process_item方法中接收item进行持久化存储
        6. 在配置文件中开启管道
3. 将同一份数据持久化到不同的平台中
	-分析:
        - 1. 管道文件中的一个管道类负责数据的一种持久化存储
        -2 . 爬虫文件向管道提交的item只会提交给优先级最高的哪一个管道类
        3. 在管道类的process_item中的return item 表示的是将当前管道接收的item发挥给下一个即将被执行的管道
```



将数据存储到mysql中的管道

```python
import pymysql
class mysqlPY(object):
    conn = None
    cursor = None
    def open_spider(self,spider):
        self.conn = pymysql.Connect(host='127.0.0.1',port=3306,user)
    def process_item(self,item,spider)
    	author = item['athor']
        content = item['content']
        sql = 'insert into qiubai values ("%s","%s")'%
        self.cursor = self.conn.cursor()
        try:
        	 self.cursor.execute(sql)
             self.conncommit()
        except Exception as e:
            print(e)
            self.conn.rollback()
    	return item  #将item 返回给下一个被执行的管道类
    def close_spider(self,spider):
        self.cursor.close()
        self.conn.close()
```

写入redis

```python
class mysqlPY(object):
    conn = None
    def open_spider(self,spider):
        self.conn = Redis(host='127.0.0.1',port=6379)
    def process_item(self,item,spider)
    	author = item['athor']
        content = item['content']
        self.conn.lpush('all_data',item)

    	return item  #将item 返回给下一个被执行的管道类
    def close_spider(self,spider):
      
        self.conn.close()
```



在scrapy 中如何进行手动请求发送

1. get请求

2. 

```python
yield scrapy.Request(new_url,callback = self.parse)
```

2. post请求,基本不用

```python
#需重写父类方法
def start_requests(self):
    data = {
        'key':'value'
    }
    yield scrapy.FormRequest(url,formdata=data,callback)
```

scrapy 五大核心组件的工作流程:

```python
1. 引擎
   - 事务流处理
     根据上一次返回的response  来按断下一步要执行哪部操作
   - 触发事务,   (所有方法,实例化的调用,都是引擎在做)
2. spider(爬虫文件)
   - 数据解析:parse获取响应数据开始解析
   - 产生url
3. 调度器
   - 过滤器
   - 队列
4. 管道
5. 下载器(异步体现)
   - 将请求所获得的响应数据进行下载
   - 不会等待请求阻塞
```



```python
- scrapy五大核心组件的工作流程:
引擎(Scrapy)
    用来处理整个系统的数据流处理, 触发事务(框架核心)
调度器(Scheduler)
    用来接受引擎发过来的请求, 压入队列中, 并在引擎再次请求的时候返回. 可以想像成一个URL（抓取网页的网址或者说是链接）的优先队列, 由它来决定下一个要抓取的网址是什么, 同时去除重复的网址
下载器(Downloader)
    用于下载网页内容, 并将网页内容返回给蜘蛛(Scrapy下载器是建立在twisted这个高效的异步模型上的)
爬虫(Spiders)
    爬虫是主要干活的, 用于从特定的网页中提取自己需要的信息, 即所谓的实体(Item)。用户也可以从中提取出链接,让Scrapy继续抓取下一个页面
项目管道(Pipeline)
    负责处理爬虫从网页中抽取的实体，主要的功能是持久化实体、验证实体的有效性、清除不需要的信息。当页面被爬虫解析后，将被发送到项目管道，并经过几个特定的次序处理数据。
```

###### 基于scrapy进行图片数据的爬取

```python


response.body   图片的二进制流
from scrapy.piplines.images import ImagesPipeline

class ImgproPipeline(ImagesPipeline):
    #对一个媒体资源请求发送
    #item 就是接收到的spider提交过来的item
    def get_media_request(self,item,info):
        yield scrapy.Request(item['scr'])
    #指定媒体数据存储的名称    
    def file_path(self,request,response=None,info=None):
        return request.url.split('/')[-1]    #请求url切割取图片名称
	
    def item_completed(self,results,item,info):
        return 
```

```python
- settings.py:
    - 不遵从robots协议
    - 进行UA伪装
    - 进行日志等级设定:LOG_LEVEL = 'ERROR'    #只在终端输出
    - LOG_file = '.\log.txt'                #写入到log文件中
    - IMAGS_STORE = '\img'
```

###### 如何提升scrapy爬取数据 效率

```python
1. 增加并发:   scrapy默认开启32个线程
    修改CONCURRENT_REQUESTS = 64
2. 降低日志等级
3. 禁止cookie   注释掉cookie = Flase
4. 禁止重试:默认对请求失败的重新进行发送 
    RETRY_ENABLED = Flase  
5. 减少下载超时: 直接写入setting
    DOWNLOAD_TIMEOUT = 10 
    如果一个文件因为网络问题超时,只等他10秒
```

###### 请求传参

- 深度爬取:爬取多个多个层级对应的页面数据
- 使用场景:爬取的数据没有在同一张页面中
- 手动请求的时候传递item:yield scrapy.Request(url,callback,meta={'item':item})
  - 将meta字典传递给callback
  - 在callback中接收meta:  item = response.meta['item']

- scrapy 中的中间件的应用

  - 爬虫中间件   下载中间件

  - 爬虫中间件:

    - 作用,批量拦截请求和响应

    - 拦截请求

      - UA伪装

        - 配置文件的UA伪装和中间件的UA伪装区别

          - 配置文件的UA伪装 对整个工程项目 都是固定的一个User_AGENT

          - 

          - 中间件,可以将每个拦截到的对象,尽可能多的设定成不同的请求载体身份标识

            ```python
            #UA池 ,可以去网上下载    代理池
            import random
            def process_request(self,request,spilder):
                request.headers['user_agent'] = 		     random.choice(user_agent_list)
                
               
                
                
            user_agent_list = [
                    "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.1 "
                    "(KHTML, like Gecko) Chrome/22.0.1207.1 Safari/537.1",
                    "Mozilla/5.0 (X11; CrOS i686 2268.111.0) AppleWebKit/536.11 "
                    "(KHTML, like Gecko) Chrome/20.0.1132.57 Safari/536.11",
                    "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/536.6 "
                    "(KHTML, like Gecko) Chrome/20.0.1092.0 Safari/536.6",
                    "Mozilla/5.0 (Windows NT 6.2) AppleWebKit/536.6 "
                    "(KHTML, like Gecko) Chrome/20.0.1090.0 Safari/536.6",
                    "Mozilla/5.0 (Windows NT 6.2; WOW64) AppleWebKit/537.1 "
                    "(KHTML, like Gecko) Chrome/19.77.34.5 Safari/537.1",
                    "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/536.5 "
                    "(KHTML, like Gecko) Chrome/19.0.1084.9 Safari/536.5",
                    "Mozilla/5.0 (Windows NT 6.0) AppleWebKit/536.5 "
                    "(KHTML, like Gecko) Chrome/19.0.1084.36 Safari/536.5",
                    "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/536.3 "
                    "(KHTML, like Gecko) Chrome/19.0.1063.0 Safari/536.3",
                    "Mozilla/5.0 (Windows NT 5.1) AppleWebKit/536.3 "
                    "(KHTML, like Gecko) Chrome/19.0.1063.0 Safari/536.3",
                    "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_8_0) AppleWebKit/536.3 "
                    "(KHTML, like Gecko) Chrome/19.0.1063.0 Safari/536.3",
                    "Mozilla/5.0 (Windows NT 6.2) AppleWebKit/536.3 "
                    "(KHTML, like Gecko) Chrome/19.0.1062.0 Safari/536.3",
                    "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/536.3 "
                    "(KHTML, like Gecko) Chrome/19.0.1062.0 Safari/536.3",
                    "Mozilla/5.0 (Windows NT 6.2) AppleWebKit/536.3 "
                    "(KHTML, like Gecko) Chrome/19.0.1061.1 Safari/536.3",
                    "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/536.3 "
                    "(KHTML, like Gecko) Chrome/19.0.1061.1 Safari/536.3",
                    "Mozilla/5.0 (Windows NT 6.1) AppleWebKit/536.3 "
                    "(KHTML, like Gecko) Chrome/19.0.1061.1 Safari/536.3",
                    "Mozilla/5.0 (Windows NT 6.2) AppleWebKit/536.3 "
                    "(KHTML, like Gecko) Chrome/19.0.1061.0 Safari/536.3",
                    "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/535.24 "
                    "(KHTML, like Gecko) Chrome/19.0.1055.1 Safari/535.24",
                    "Mozilla/5.0 (Windows NT 6.2; WOW64) AppleWebKit/535.24 "
                    "(KHTML, like Gecko) Chrome/19.0.1055.1 Safari/535.24"
            ]
            ```

```python
def process_excption(self,request,exception,spider):
     if request.url.split(':')[0] = 'http':
        request.meta['proxy'] = 'http://'+random.choice(proxy_http)
    else:
        request.meta['proxy']='https://'+random.choice(proxy_https)
    
    return request   #将修正只有的请求进行重新发送
```

```python
#拦截所有的响应对象,可篡改,可替换
def process_response(self,request,response,spider):
    #将不满足需求的响应对象找出
    	#1.一个响应对象对应唯一一个请求对象
        #2.如果我么可以董伟到五个响应对象的请求对象后,就可以定位到指定的响				应对象
        #3 通过5个板块的url定位请求对象
    #将这些对象修正(替换)
    if request.url  in spider.five_model_urls:
        response = scrapy.FormRequest()
    
```

### 基于CrawlSpider 的全站数据爬取



基于CrawlSpider创建一个工程:

​	scrapy genspider  -t crawl spiderName  www.asdas.com



LinkExtractor       链接提取器

- 作用:可以根据指定的规则,进行指定链接的提取
- 提取规则: allow = '正则表达式'

Rule  规则解析器    

- rules = (Rule(link,call_back=pase_one,follow=Flase))
  - follow参数:将链接提取器      继续作用到链接提取器提取出的页面链接      所对应的页面中

- 获取链接提取器提取到的链接进行请求发送,根据指定规则对请求到的页面源码数据进行数据解析.

- 链接提取器和规则解析器一对一关系



### 分布式

- 基于多台电脑组件一个分布式机(机器)群 ,然后让急群众的每一台电脑执行同一组程序,然后,让他们对同一个网站的数据 进行分部爬取
- 为什么使用分布式
  - 提升爬取数据的效率    (公司基本不会用,环境搭建太费劲)
- 如何实现分布式爬虫
  - 基于scrapy框架结合scrapy-redis组件        实现分布式爬虫

- 原生scrapy 框架无法实现分布式 
  - 调度器无法被分布式机群共享
  - 管道无法被共享

- scrapy-redsis 组件作用:
  - 提供共享的调度器和管道

编写代码:

- 

```python
表示可被共享调度器调度的队列
redis_key = 'fpsQueue'
```

