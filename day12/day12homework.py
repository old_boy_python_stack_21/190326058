# 写出三元运算的基本格式及作用？
''''''
'''
a = 结果 if 条件 else 不成立结果
使代码更简洁
'''
# 什么是匿名函数？
'''
没名字的函数 lamada函数
'''
# 尽量多的列举你了解的内置函数？【默写】
'''
输入输出 input print
数学相关：abs 绝对值 max最大值 min最小值  sum 加 divmod除 得到商和余数
round 精确小数  
进制相关 bin Oct   int  hex
强制转换  str list tuple dict set 
其他 len range id type  chr ord open 
'''
# -----------------------------------------------------------------------------
# filter/map/reduce函数的作用分别是什么？
'''
filter 传入一个判断类型的函数  和一个可迭代对象
返回成立的值
map 传入一个函数  和一个可迭代对象
将对象中每个元素 用函数处理一遍 返回一个列表
reduce 传入一个函数 和一个可迭代对象
函数需要传入两个参数，第一次 是取可迭代对象第一个跟第二个值，在之后第一个值为上一次
函数运行返回的值，第二个值为对象上取
最后返回一个函数返回值 组成的列表
'''
# ---------------------------------------------------------------------------------------
# 看代码写结果


def func(*args, **kwargs):
    print(args, kwargs)

# a. 执行 func(12,3,*[11,22]) ，输出什么？
# func(12,3,*[11,22])
'''
(12,3,11,22){}
'''
# b. 执行 func(('alex','武沛齐',),name='eric')
# func(('alex','武沛齐',),name='eric')
'''
（('alex','武沛齐',）） {'name':'eric'}
'''
# -------------------------------------------------------------------------------------------------------
# 看代码分析结果

def func(arg):
    return arg.pop(1)

result = func([11,22,33,44])
print(result)
'''
22
'''
# ---------------------------------------------------------------------------------
# 看代码分析结果

# func_list = []
#
# for i in range(10):
#     func_list.append(lambda :i)
#
# v1 = func_list[0]()
# v2 = func_list[5]()
# print(v1,v2)
'''
9  9
'''
# -----------------------------------------------------------------------------
# 看代码分析结果

# func_list = []
#
# for i in range(10):
#     func_list.append(lambda x:x+i)
#
# v1 = func_list[0](2)
# v2 = func_list[5](1)
# print(v1,v2)
'''
11   10
'''
# 看代码分析结果

func_list = []

for i in range(10):
    func_list.append(lambda x:x+i)

for i in range(0,len(func_list)):
    result = func_list[i](i)
    print(result)
'''
0  2  4  6 8 10 12 14 16 18
'''
# 看代码分析结果

# def f1():
#     print('f1')
#
# def f2():
#     print('f2')
#     return f1
#
# func = f2()
# result = func()
# print(result)
'''
f2 f1 none
'''
# 看代码分析结果【面试题】

# def f1():
#     print('f1')
#     return f3()
#
# def f2():
#     print('f2')
#     return f1
#
# def f3():
#     print('f3')
#
# func = f2()
# result = func()
# print(result)
'''
f2  f1  f3  None
'''
# 看代码分析结果

name = '景女神'

def func():
    def inner():
        print(name)
    return inner()

v = func()
print(v)
'''
景女神
None
'''
# 看代码分析结果

name = '景女神'

def func():
    def inner():
        print(name)
        return "老男孩"
    return inner()

v = func()
print(v)
'''
景女神
老男孩
'''
# 看代码分析结果

name = '景女神'

def func():
    def inner():
        print(name)
        return '老男孩'
    return inner

v = func()
result = v()
print(result)
'''
景女神
老男孩
'''
# 看代码分析结果

def func():
    name = '武沛齐'
    def inner():
        print(name)
        return '老男孩'
    return inner

v1 = func()
v2 = func()
print(v1,v2)
'''
<function func.<locals>.inner at 0x000001D5BD571048> <function func.<locals>.inner at 0x000001D5BD571158>
'''
# 看代码写结果

def func(name):
    def inner():
        print(name)
        return '老男孩'
    return inner

v1 = func('金老板')
v2 = func('alex')
print(v1,v2)
'''
<function func.<locals>.inner at 0x000001F405F510D0> <function func.<locals>.inner at 0x000001F405F51048>
'''
# 看代码写结果

def func(name=None):
    if not name:
        name= '武沛齐'
    def inner():
        print(name)
        return '老男孩'
    return inner

v1 = func()
v2 = func('alex')
print(v1,v2)
'''
<function func.<locals>.inner at 0x0000011E672011E0> <function func.<locals>.inner at 0x0000011E672010D0>
'''
# 看代码写结果【面试题】

def func(name):
    v = lambda x:x+name
    return v

v1 = func('武沛齐')
v2 = func('alex')
v3 = v1('银角')
v4 = v2('金角')
print(v1,v2,v3,v4)
'''
俩内存地址，银角武沛齐  ，金角alex
'''
# 看代码写结果

NUM = 100
result = []
for i in range(10):
    func = lambda : NUM      # 注意：函数不执行，内部代码不会执行。
    result.append(func)

print(i)
print(result)
v1 = result[0]()
v2 = result[9]()
print(v1,v2)
'''
9
内存地址
100  100
'''
# 看代码写结果【面试题】

result = []
for i in range(10):
    func = lambda : i      # 注意：函数不执行，内部代码不会执行。
    result.append(func)

print(i)
print(result)
v1 = result[0]()
v2 = result[9]()
print(v1,v2)
'''
9
内存地址
9 9 
'''
# 看代码分析结果【面试题】

def func(num):
    def inner():
        print(num)
    return inner

result = []
for i in range(10):
    f = func(i)
    result.append(f)

print(i)
print(result)
v1 = result[0]()
v2 = result[9]()
print(v1,v2)
'''
9
打印9个内存地址
0
9
None None
'''
# 程序设计题
#
# 请设计实现一个商城系统，商城主要提供两个功能：商品管理、会员管理。
#
# 商品管理：
#
# 查看商品列表
# 根据关键字搜索指定商品
# 录入商品
# 会员管理：【无需开发，如选择则提示此功能不可用，正在开发中，让用户重新选择】
#
# 需求细节：
#
# 启动程序让用户选择进行商品管理 或 会员管理，如： 输入图片说明
# 用户选择 【1】 则进入商品管理页面，进入之后显示商品管理相关的菜单，如： 输入图片说明
# 用户选择【2】则提示此功能不可用，正在开发中，让用户重新选择。
# 如果用户在【商品管理】中选择【1】，则按照分页去文件 goods.txt 中读取所有商品，并全部显示出来【分页功能可选】。
# 如果用户在【商品管理】中选择【2】，则让提示让用户输入关键字，输入关键字后根据商品名称进行模糊匹配，如： 输入图片说明
# 如果用户在【商品管理】中选择【3】，则提示让用户输入商品名称、价格、数量 然后写入到 goods.txt 文件，如： 输入图片说明
'''
def goods_login():
    while True:
        print('*******************欢迎使用老子的购物商城*****************')
        info = ['商品管理','会员管理（不可选正在开发中）']
        for i in range(2):
            print(i+1,info[i])
        choic = input('请选择（输入N返回上一级）：')
        if choic.upper() == 'N':
            break
        elif choic == '1':
            a = True
            goods_manage()
        elif choic == '2':
            print('正在开发请重新选择 ')
            continue
def goods_manage():
    while True:
        print('*******************欢迎使用老子的购物商城【商品管理】*****************')
        info_shop = ['查看商品列表', '根据关键字搜索指定商品', '录入商品']
        for i in range(1,len(info_shop)+1):
            print((i), info_shop[i - 1])
        choic2 = input('请选择（输入N返回上一级）：')
        if choic2.upper() == 'N':
            break
        elif choic2 == '1':
            goods_data()
        elif choic2 == '2':
            goods_search()
        elif choic2 == '3':
            while True:
                name = input('请输入商品名称（输入N返回上一级）：')
                if name.upper() == 'N':
                    break
                price = input('请输入商品价格：')
                num = input('请输入商品数量')

                append_name = name + '  ' + price + '  ' + num
                with open('goods.txt','a',encoding='utf-8') as f1:
                    f1.writelines('\n'+append_name)
                    print('添加成功')
                name = input('请输入商品名称（输入N返回上一级）：')
                if name.upper() == 'N':
                    break

def goods_data():
    while True:
        l1 = []
        with open('goods.txt', 'r', encoding='utf-8')as f1:
            for i in f1:
                l1.append(i)
        page,a = divmod(len(l1),5)
        if a >  0:
            page += 1
        page_num = int(input('请输入查看的页数：'))
        for i in l1[(page_num-1)*5:page_num*5]:
            print(i)
        choic3 = input('请输入要查询的关键字（输入N返回上一级）：')
        if choic3.upper() == 'N':
            break
def goods_search():
    while True:
        choic3 = input('请输入要查询的关键字（输入N返回上一级）：')
        if choic3.upper() == 'N':
            a = False
            break
        else:
            print('******搜索结果如下******')
            with open('goods.txt', 'r', encoding='utf-8')as f1:
                for i in f1:
                    if choic3 in i:
                        print(i)
                s = input('请输入要查询的关键字（输入N返回上一级）：')
                if s.upper() == 'N':
                    break
goods_login()
'''''