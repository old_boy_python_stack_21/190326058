from django import forms
from liupiapp import models
from django.db.models import DateField
from multiselectfield.forms.fields import MultiSelectFormField
import datetime


class Form_arm(forms.ModelForm):
    class Meta:
        model = models.Customer

        fields = '__all__'
        widgets = {
            'username': forms.EmailInput(attrs={'placeholder': '您的用户名', 'autocomplete': 'off'}),
            'qq':forms.TextInput(attrs={'placeholder':'QQ号','lable':'QQ号'}),
            'qq_name':forms.TextInput(attrs={'placeholder':'QQ昵称'},),
            'name':forms.TextInput(attrs={'placeholder':'真实姓名'}),
            'birthday': forms.DateTimeInput(attrs={'type': 'date'}),
            'last_consult_date':forms.DateTimeInput(attrs={'type':'date'})
        }

    def __init__(self,*args,**kwargs):
        super().__init__(*args,**kwargs)
        for filed in self.fields.values():
            if isinstance(filed,MultiSelectFormField):
                continue
            filed.widget.attrs['class']='form-control'


class Genjin_form(forms.ModelForm):

    class Meta:
        model = models.ConsultRecord
        fields = '__all__'
        widgets = {
            'date': forms.DateTimeInput(attrs={'type': 'date'}),
        }

    def __init__(self, request, customer_id, *args, **kwargs):
        super().__init__(*args, **kwargs)
        # print(list(self.fields['customer'].choices))
        # self.fields['customer'].choices = [('', '---------'), (1, '121312321321 - mjj')]
        # self.fields['customer'].choices = request.user_obj.customers.all().values_list('pk','name')
        print(customer_id)
        # 限制咨询客户为当前销售的私户
        if customer_id and customer_id != '0':
            self.fields['customer'].choices = [(i.pk, str(i)) for i in models.Customer.objects.filter(pk=customer_id)]
        else:
            self.fields['customer'].choices = [('', '---------'), ] + [(i.pk, str(i)) for i in
                                                                       request.user_obj.customers.all()]

        # 限制跟进人为当前销售
        self.fields['consultant'].choices = [(request.user_obj.pk, request.user_obj)]


class Enrollmentform(forms.ModelForm):
    class Meta:
        model = models.Enrollment
        fields = '__all__'


    def __init__(self, request, customer_id, *args, **kwargs):
        super().__init__(*args, **kwargs)
        print(customer_id)
        if  customer_id:
            self.fields['customer'].choices = [ (i.pk,str(i)) for i in models.Customer.objects.filter(pk=customer_id)]
        else:
            self.fields['customer'].choices = [  (i.pk,str(i))  for i in models.Customer.objects.filter(consultant=request.user_obj)]


class Classlist(forms.ModelForm):
    class Meta:
        model = models.ClassList
        fields = '__all__'

    def __init__(self,request,pk,*args,**kwargs):
        super().__init__(*args,**kwargs)

class CourseRecordForm(forms.ModelForm):
    class Meta:
        model = models.CourseRecord
        fields = '__all__'
    def __init__(self,request,pk,*args,**kwargs):
        super().__init__(*args,**kwargs)


class StudyRecordForm(forms.ModelForm):
    class Meta:
        # model = models.Enrollment
        fields = '__all__'
        # pass