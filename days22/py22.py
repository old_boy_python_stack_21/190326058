# 请使用面向对象实现栈（后进先出）
'''
'''
'''
class Fun:
    def __init__(self):
        self.list = []
    def push(self,a):
        self.list.append(a)
    def del_m(self):
        self.list.pop()
'''

# 请是用面向对象实现队列（先进先出）/
'''
class Fun:
    def __init__(self):
        self.list = []
    def push(self,a):
        self.list.append(a)
    def del_m(self):
        self.list.pop(0)
'''
# 如何实现一个可迭代对象？
'''
class A:
    def __iter__(self):
        return iter([1,2,3])
obj = A()
for i in obj:
    print(i)
'''
# 看代码写结果

class Foo(object):

    def __init__(self):
        self.name = '武沛齐'
        self.age = 100


obj = Foo()
setattr(Foo, 'email', 'wupeiqi@xx.com')

v1 = getattr(obj, 'email')
v2 = getattr(Foo, 'email')

print(v1, v2)
'''
wupeiqi@xx.com wupeiqi@xx.com
'''
# 请补充代码（提：循环的列表过程中如果删除列表元素，会影响后续的循环，推荐：可以尝试从后向前找）
'''

li = ['李杰','女神','金鑫','武沛齐']

name = input('请输入要删除的姓氏：') # 如输入“李”，则删除所有姓李的人。
# 请补充代码
lis = []
for i in li:
    if name in li:
        lis.append(i)
for i in lis:
    li.remove(i)
'''
# 有如下字典，请删除指定数据。
'''
class User(object):
    def __init__(self, name, age):
        self.name = name
        self.age = age


info = [User('武沛齐', 19), User('李杰', 73), User('景女神', 16)]

name = input('请输入要删除的用户姓名：')
# 请补充代码将指定用户对象再info列表中删除。
lis = []
for i in info:
    if name in i.name:
        lis.append(i)
for i in lis:
    info.remove(i)
'''
# 补充代码实现：校园管理系统。

# !/usr/bin/env python
# -*- coding:utf-8 -*-


# 补充代码实现：校园管理系统。

# !/usr/bin/env python
# -*- coding:utf-8 -*-
class User(object):
    def __init__(self, name, email, age):
        self.name = name
        self.email = email
        self.age = age
    def __str__(self):
        return self.name
class School(object):
    """学校"""
    def __init__(self):
        # 员工字典，格式为：{"销售部": [用户对象,用户对象,] }
        self.user_dict = {'销售部':[],'人事部':[]}
    def invite(self, department, user_object):
        """
        招聘，到用户信息之后，将用户按照指定结构添加到 user_dict结构中。
        :param department: 部门名称，字符串类型。
        :param user_object: 用户对象，包含用户信息。
        :return:
        """
        self.user_dict[department].append(user_object)
        for i in self.user_dict:
            if bool(self.user_dict[i]) != False:
                print('%s有人员'%i)
                for r in self.user_dict[i]:
                    print(r)
        pass
    def dimission(self, username, department=None):
        """
        离职，讲用户在员工字典中移除。
        :param username: 员工姓名
        :param department: 部门名称，如果未指定部门名称，则遍历找到所有员工信息，并将在员工字典中移除。
        :return:
        """
        if department == None:
            for i in self.user_dict:
                for r in self.user_dict[i]:
                    if r.name == username:
                        self.user_dict[i].remove(r)
                        print('%s部都有:'%i)
                        for s in self.user_dict[i]:
                            print(s.name)
                        return
        else:
            for i in self.user_dict[department]:
                if i.name == username:
                    self.user_dict[department].remove(i)
                    print(department)
                    for s in self.user_dict[department]:
                        print(s.name)
        pass
    def run(self):
        """
        主程序
        :return:
        """
        peiqi = User('peiqi','123@com',20)
        alex = User('alex','235@.com',20)
        oldboy = User('oldboy','1112@.com',60)
        obj.invite('人事部',peiqi)
        obj.invite('人事部',alex)
        obj.invite('人事部',oldboy)
        obj.dimission('alex')
        obj.dimission('oldboy','人事部')
        pass
if __name__ == '__main__':
    obj = School()
    obj.run()
# 请编写网站实现如下功能。
# 需求：
#
# 实现 MIDDLEWARE_CLASSES 中的所有类，并约束每个类中必须有process方法。
#
# 用户访问时，使用importlib和反射 让 MIDDLEWARE_CLASSES 中的每个类对 login、logout、index 方法的返回值进行包装，最终让用户看到包装后的结果。
# 如：
#
# 用户访问 : http://127.0.0.1:8000/login/ ，
# 页面显示： 【csrf】【auth】【session】 登录 【session】 【auth】 【csrf】
#
# 用户访问 : http://127.0.0.1:8000/index/ ，
# 页面显示： 【csrf】【auth】【session】 登录 【session】 【auth】 【csrf】
#
# 即：每个类都是对view中返回返回值的内容进行包装。
import importlib
MIDDLEWARE_CLASSES = [
    'utils.session.SessionMiddleware',
    'utils.auth.AuthMiddleware',
    'utils.csrf.CrsfMiddleware',
]
lic = []

for path in MIDDLEWARE_CLASSES:
    module_path,class_name = path.rsplit('.',maxsplit=1)
    module_object = importlib.import_module(module_path)# from utils import redis
    cls = getattr(module_object,class_name)
    obj = cls()
    val = getattr(obj,'process')()
    lic.append(val)


from wsgiref.simple_server import make_server

class View(object):
    def login(self):
        return '登录'

    def logout(self):
        return '登出'

    def index(self):
        return '首页'


def func(environ,start_response):
    start_response("200 OK", [('Content-Type', 'text/plain; charset=utf-8')])
    obj = View()
    method_name = environ.get('PATH_INFO').strip('/')
    if not hasattr(obj,method_name):
        return ["abc".encode("utf-8"),]
    response = getattr(obj,method_name)()
    return [''.join(lic).encode('utf-8')+response.encode('utf-8')+''.join(lic[::-1]).encode('utf-8')  ]


server = make_server('127.0.0.1', 8000, func)
server.serve_forever()