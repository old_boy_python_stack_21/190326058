'''

'''

# 简述编写类和执行类中方法的流程。
'''
定义一个类，定义一个类的方法
然后实例化一个对象，用对象调用类中的方法
'''
# 简述面向对象三大特性?
'''
继承封装多态
'''
# 将以下函数改成类的方式并调用 :
'''
def func(a1):
    print(a1)

class A:
    def func(self,a1):
        print(a1)
name = A()
name.func('佩奇')
'''
# 面向对象中的self指的是什么?
'''
self 指的就是对象
'''
# 以下代码体现 向对象的 么特点?

class Person:
    def __init__(self, name, age, gender):
        self.name = name
        self.age = age
        self.gender = gender


obj = Person('武沛齐', 18, '男')
'''
封装，将一些属性封装到对象里
'''
# 以下代码体现 向对象的 么特点?

class Message:
    def email(self):
        """
        发送邮件
        :return:
        """
        pass

    def msg(self):
        """
        发送短信
        :return:
        """
        pass

    def wechat(self):
        """
        发送微信
        :return:
        """
        pass
'''
封装，将一类函数封装到一个类里面
'''
# 看代码写结果

#
# class Foo:
#     def func(self):
#         print('foo.func')
#
#
# obj = Foo()
# result = obj.func()
# print(result)
'''
foo.func
NOne
'''
# 定义个类，其中有计算周长和面积的方法(圆的半径通过参数传递到初始化方法)。
'''
class Cricle:
    def __init__(self,r,pi):
        self.r = r
        self.pi = pi
    def aera(self):
        return self.pi * self.r * self.r
    def girth(self):
        return self.r * 2 * self.pi
a1 = Cricle(20,3.14)
a1_aera = a1.aera()
print(a1_aera)
a1_girth = a1.girth()
print(a1_girth)
'''
# 面向对象中为什么要有继承?
'''
增加代码的可重用性简洁性
'''
# Python继承时，查找成员的顺序遵循 么规则?
'''
先从左找到底，没有在找第二个位置的，找到底，再找下个位置的找到地
'''
# 看代码写结果

class Base1:
    def f1(self):
        print('base1.f1')

    def f2(self):
        print('base1.f2')

    def f3(self):
        print('base1.f3')
        self.f1()


class Base2:
    def f1(self):
        print('base2.f1')


class Foo(Base1, Base2):
    def f0(self):
        print('foo.f0')
        self.f3()


obj = Foo()
obj.f0()
'''
foo.f0
base1.f3
base1.f1

'''
# 看代码写结果:

class Base:
    def f1(self):
        print('base.f1')

    def f3(self):
        self.f1()
        print('base.f3')


class Foo(Base):
    def f1(self):
        print('foo.f1')

    def f2(self):
        print('foo.f2')
        self.f3()


obj = Foo()
obj.f2()
'''
foo.f2
foo.f1
base.f3
'''
# 补充代码实现

# user_list = []
# while True:
#     user = input(“请输入用户名:”)
#     pwd = input(“请输入密码:”)
#     email = input(“请输入邮箱:”)

"""
# 需求
1. while循环提示 户输 : 户名、密码、邮箱(正则满 邮箱格式)
2. 为每个 户创建 个对象，并添加到 表中。
3. 当 表中的添加 3个对象后，跳出循环并以此循环打印所有 户的姓名和邮箱
"""
'''

class Zhuce:
    def __init__(self,n,p,e):
        self.name = n
        self.pasward = p
        self.email = e

user_list = []
while True:
    user = input('请输入用户名:')
    pwd = input('请输入密码:')
    email = input('请输入邮箱:')
    if '@' not in email:
        continue
    v = Zhuce(user,pwd,email)
    user_list.append(v)
    if len(user_list) == 3:
        break
for i in user_list:
    print(i.name,i.email)
'''
# 补充代码:实现 户注册和登录。

import sys
class User:
    def __init__(self, name, pwd):
        self.name = name
        self.pwd = pwd
class Account:
    def __init__(self):
        # 用户列表，数据格式：[user对象，user对象，user对象]
        self.user_list = []
    def login(self):
        """
        用户登录，输入用户名和密码然后去self.user_list中校验用户合法性
        :return:
        """
        while  True:
            con = False
            name = input('输入用户名：')
            pas = input('输入密码：')
            for i in self.user_list:
                 for k,v in i.items():
                    if name == k and pas == v:
                        con = True
                        print('登录成功')
                        return
            if con == False:
                print('账号或密码错误，请重新登录')
        pass
    def register(self):
        dib = {}
        """
        用户注册，没注册一个用户就创建一个user对象，然后添加到self.user_list中，表示注册成功。
        :return:
        """
        while True:
            print('''
            **********************注册页面*************************
            ''')
            name = input('输入名字：')
            pasward = input('输入密码')
            dib[name] = pasward
            for i in self.user_list:
                if i.keys() == name:
                    print('账户已存')
                    return

            print('注册成功')
            self.user_list.append(dib)
            return


        pass
    def run(self):
        """
        主程序
        :return:
        """
        while True:
            print('''
            1,注册
            2，登录
            ''')
            dii = {'1':self.register,'2':self.login}

            choice = input('请选择序号或N退出：')
            if choice.upper() == 'N':
                sys.exit(0)
            elif dii.get(choice) == None:
                print('输入有误，请重新输入')
                continue
            else:
                dii.get(choice)()



if __name__ == '__main__':
    obj = Account()
    obj.run()
