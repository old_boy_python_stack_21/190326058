# 1. 前端

## 1.html

## 2. css

## 3.JavaScript

### 3.1 js的引入方式

1. 行内

   ```java
   <p id="" class="" style="" onclick="console.log(2);">mjj</p>
   ```

   

2. 内嵌

   ```javascript
   <script type="text/javascript">
   ```

   

3. 基本数据和引用

   1. 字符串 string

      ```js
      字符串转number几种方法
      var  stringNum = '789.123wadjhkd';
      var num2 =  Number(stringNum);
      console.log(num2)     打印一个 NAN  
      
      // parseInt()可以解析一个字符串 并且返回一个整数
      // number（）方法若字符串中含有字母，会得到nan
      console.log(parseInt(stringNum))      789
      console.log(parseFloat(stringNum));     789.123
      
      ```

      

   2. 数字类型    Number

      ```js
      特殊：NaN 属性是代表非数字值的特殊值。该属性用于指示某个值不是数字。可以把 Number 对象设置为该值，来指示其不是数字值。
      ```

      

   3. Undefined   Undefined类型

   4. Null        空

   5. Boolean   布尔类型

   6. Symbol     6版本新定义

### 3.2字符串方法 

2.0 serach（）匹配到返回索引，没匹配到返回-1

2.1 slice  切片

```javascript
当你知道字符串中的子字符串开始的位置，以及想要结束的字符时，slice()可以用来提取 它。 尝试以下：
字符串.slice(0,3);
```

2.2 replace 替换

2.3 concat  字符串拼接，返回新的字符串

```javascript
var stringValue = "hello ";
var result = stringValue.concat("world"); alert(result);
```

2.4 substr()     slice()       substring()    切片方法

```javascript
ar stringValue = "hello world";
alert(stringValue.slice(3));//"lo world"
alert(stringValue.substring(3));//"lo world"
alert(stringValue.substr(3));//"lo world"
alert(stringValue.slice(3, 7));//"lo w"
alert(stringValue.substring(3,7));//"lo w"
alert(stringValue.substr(3, 7));//"lo worl"
```

负值情况

```javascript
var stringValue = "hello world";
alert(stringValue.slice(-3));//"rld" 
alert(stringValue.substring(-3));//"hello world"
alert(stringValue.substr(-3)); //"rld"
alert(stringValue.slice(3, -4));//"lo w" 
alert(stringValue.substring(3, -4));//"hel"
alert(stringValue.substr(3, -4)); //""(空字符串)
```

2.5 indexof()   lastindexof()    索引元素在第几个位置

```javascript
var stringValue = "hello world";
alert(stringValue.indexOf("o"));             //4
alert(stringValue.lastIndexOf("o"));         //7
alert(stringValue.indexOf("o", 6));         //7
alert(stringValue.lastIndexOf("o", 6));     //4
```

2.6  charAt()  获取字符

2.7 charodeAt（）  获取字符的ASCII码

2.8 toUppercase（）tolowercase（）   转大小写

2.9 trim（）  去空格

typeof  查看当前元素类型

### 3.3数组 Array

2.9 清空数组的几种方式

```js
   var array = [1,2,3,4,5,6];

    array.splice(0);      //方式1：删除数组中所有项目
    array.length = 0;     //方式1：length属性可以赋值，在其它语言中length是只读
    array = [];           //方式3：推荐
```



3.0  isArray  判断是否为数组

3.1 toString()  转换成字符串方法会返回逗号拼接的每个元素    join（‘*’）用*拼接字数组元素

3.2  push（）添加，可接受多个参数（末尾） 返回数组的长度

3.3 pop（）删除，，删除一项，返回移除的项

3.4 unshift（） 添加，可接受多个参数（0索引位置） 返回数组的长度

3.5 shift（） 删除，删除第一项    返回删除的项

3.6 reverse（）  反转

3.7 sort（）    排序，从小到大

3.8 concat()   数组合并方法

```js
var north = ['北京','山东','天津'];
var south = ['东莞','深圳','上海'];
        
var newCity = north.concat(south);
console.log(newCity)
```



3.9 slice  切片

4.0  length   他是一个属性，唯一的一个，获取数组的长度

4.0  splice（）方法  

```javascript
3个参数  索引位置    删除个数   插入的项
var colors = ["red", "green", "blue"];
var removed = colors.splice(0,1); 
alert(colors); // green,blue 
alert(removed); // red，返回的数组中只包含一项
removed = colors.splice(1, 0, "yellow", "orange"); 
alert(colors); // green,yellow,orange,blue alert(removed); // 返回的是一个空数组
removed = colors.splice(1, 1, "red", "purple"); 
alert(colors); // green,red,purple,orange,blue alert(removed); // yellow，返回的数组中只包含一项
```

4.1 位置方法  indexof（）  lastindexof（） 

```javascript
从0索引和从末尾查找   没找到都返回-1
var numbers = [1,2,3,4,5,4,3,2,1];
alert(numbers.indexOf(4)); //3
alert(numbers.lastIndexOf(4));// 5
alert(numbers.indexOf(4,4));// 5
alert(numbers.lastIndexOf(4,4));//3
var person = {name:"mjj"};
var people = [{name:'mjj'}];
var morePeople = [person];
alert(people.indexOf(person)); //-1
alert(morePeople.indexOf(person));// 0
```

4.2 filter（）  筛选

```javascript
filter()函数，它利用指定的函数确定是否在返回的数组中包含某一项。例如要返回一个所有数值都大于2的数组，可以使用如下代码。

var numbers = [1,2,3,4,5,4,3,2,1];
var filterResult = numbers.filter(function(item, index, array){
    return (item > 2);
});
alert(filterResult); //[3,4,5,4,3]
```

4.3 map()  方法

把每一个结果都加工返回新的数组

```javascript
var numbers = [1,2,3,4,5,4,3,2,1];
var filterResult = numbers.map(function(item, index, array){
    return item * 2;
});
alert(filterResult); //[2,4,6,8,10,8,6,4,2]
```

4.4 String 

String 类型的每个实例都有一个 length 属性，表示字符串中包含多个字符

```javascript
var stringValue = "hello world"; alert(stringValue.length);     //"11"
```



4.5 迭代方法

for

forEach() 仅能在数组对象中使用
在函数中arguments 这个对象是伪数组





### 3.4函数

```javascript
function sum(a,b){
    //函数体;
}

#例子
function sum(a,b){
    sum = a + b;
    console.log(sum);
}
sum(3,5);//调用函数，直接写函数名
```

```js
1.创建方法
（1）普通函数
   function fn(){
       
   }
   fn();
(2)函数表达式
  var fn = function(){}
  
 (3) 自执行函数
 ;(function(){
    this指向 一定是指向window
 })()

全局作用域下，函数作用域，自执行函数都指向了window,函数作用域中this指向可以发生改变，可以使用call()或者apply()


var obj = {name：'mjj'};
function fn(){
    console.log(this.name);
}
fn();//是空值，因为函数内部this指向了window
fn.call(obj)//此时函数内部的this指向了obj


作用： 解决冗余性代码，为了封装


构造函数
new Object();
new Array();
new String();
new Number();


//使用构造函数来创建对象
function Point(x, y) {
  this.x = x;
  this.y = y;
}

Point.prototype.toString = function () {
  return '(' + this.x + ', ' + this.y + ')';
};

var p = new Point(1, 2);

//es6用class来创建对象
class Person{
    constructor(x,y){
        this.x = x;
        this.y = y
    }
    toString(){
        
    }
    
}
var p = new Person();
```





### 3.5对象

1.0 创建对象方式

```javascript
1. var person = new Object();
	person.name='jack'
	//给对象添加name和age属性
    person.name = 'jack';
    person.age = 28;
    //给对象添加fav的方法
    person.fav = function(){
        console.log('泡妹子');
    }
2. var person = {
    name : 'jack';
    age : 28,
    fav : function(){
        console.log('泡妹子');
    }
}
3. var person = {};//与new Object()相同
4. function fn(name){
    var obj = {};
    obj[name] = 'mjj';
    return obj;

}
fn('age')

//遍历对象
for(var key in obj){
    obj[key]
}
```

1.1 访问属性

```javascript
person.name; //jack
person.fav();
person['name'];
```



### 3.6日期 对象

1.0 创造一个日期对象

```javascript
var now = new Date([parameters]);
```

前边的语法中的参数（parameters）可以是一下任何一种：

- 无参数 : 创建今天的日期和时间，例如： `var today = new Date();`.
- 一个符合以下格式的表示日期的字符串: “月 日, 年 时:分:秒.” 例如： `var Xmas95 = new Date("December 25, 1995 13:30:00")。`如果你省略时、分、秒，那么他们的值将被设置为0。
- 一个年，月，日的整型值的集合，例如： `var Xmas95 = new Date(1995, 11, 25)。`
- 一个年，月，日，时，分，秒的集合，例如： `var Xmas95 = new Date(1995, 11, 25, 9, 30, 0);`.

1.1  getDate()    根据本地时间返回制定日期对象的月份中的第几天（1-31）

1.2  Date()     根据本地时间返回当天的日期和时间

1.3 getMonth    根据本地时间返回制定日期对象的月份（0-11）

1.4 getFullYear（）  根据本地时间返回制定日期对象的年份（四位数年份时返回四位数字）

1.5 getDay（） 根据本地时间返回制定日期对象的星期中的第几天（0-6）

1.6 getHours（） （0-23)

1.7 getMinutes()  分钟（0-59）

1.8 getSeconds（） 秒数（0-59）



#### 5.2  日期格式化方法

```javascript
toDateString()——以特定于实现的格式显示星期几、月、日和年;
toTimeString()——以特定于实现的格式显示时、分、秒和时区;
toLocaleDateString()——以特定于地区的格式显示星期几、月、日和年;
toLocaleTimeString()——以特定于实现的格式显示时、分、秒;
toUTCString()——以特定于实现的格式完整的 UTC 日期。
```

5.2.1 例子

```javascript
var myDate = new Date();
myDate.toDateString();//"Mon Apr 15 2019"
myDate.toTimeString();//"10:11:53 GMT+0800 (中国标准时间)"
myDate.toLocaleDateString();//"2019/4/15"
myDate.toLocaleTimeString();//"上午10:11:53"
myDate.toUTCString();//"Mon, 15 Apr 2019 02:11:53 GMT"

```

5.2.2 返回用数字时钟格式的时间

```javascript
var time = new Date();
var hour = time.getHours();
var minute = time.getMinutes();
var second = time.getSeconds();
var temp = "" + ((hour > 12) ? hour - 12 : hour);
if (hour == 0)
    temp = "12";
temp += ((minute < 10) ? ":0" : ":") + minute;
temp += ((second < 10) ? ":0" : ":") + second;
temp += (hour >= 12) ? " P.M." : " A.M.";
alert(temp);
```









### 3.7数学对象 math

1.  min（）   max（）

   ```javascript
   var max = Math.max(3, 54, 32, 16);
   alert(max);    //54
   var min = Math.min(3, 54, 32, 16);
   alert(min);    //3
   ```

2. 舍入方法  math.ceil（） floor（） round（）

   分别为向上舍入，向下舍入，标准四舍五入

3.  random（）方法

   返回大于等于 0 小于 1 的一个随机数

   取min到max范围的整数

   ```python
   function random(lower, upper) {
       return Math.floor(Math.random() * (upper - lower)) + lower;
   }
   ```

​    获取随机颜色

```javascript
/**
 * 产生一个随机的rgb颜色
 * @return {String}  返回颜色rgb值字符串内容，如：rgb(201, 57, 96)
 */
function randomColor() {
    // 随机生成 rgb 值，每个颜色值在 0 - 255 之间
    var r = random(0, 256),
        g = random(0, 256),
        b = random(0, 256);
    // 连接字符串的结果
    var result = "rgb("+ r +","+ g +","+ b +")";
    // 返回结果
    return result;
}
```





### 3.8流程控制

1. if else 

   ```javascript
   var weather = 'sunny';
   if(weather == 'sunny'){
       if(temperature > 30){
           //还是在家里吹空调吧
       }else if(temperature<=30){
           //天气非常棒，可以出去玩耍了
       }
   }
   for(var i = 0; i < 10; i ++){
       
   }
   
   三元运算
   1 > 3 ? '真的' : '假的';
   ```

   

2. switch

   ```javascript
   switch (expression) {
     case choice1:
       run this code
       break;
     case choice2:
       run this code instead
       break;
     default:
       actually, just run this code
   }
   ```

   

   

### 3.9循环

1. for  使用break退出循环

   ```javascript
   var shopping = ['香蕉','苹果','牛奶','红牛'];
   var x;
   for(var i = 0; i < shopping.length; i ++){
       x = shopping[i];
       console.log(x);
   }
   ```

   

2. while 

   ```javascript
   初始化条件
   while(判断循环结束条件){
       //code to run
       递增条件
   }
   ```

   

### 4.0 定义域  Globle 对象

### 4.1 赋值运算符，逻辑运算符

```js
&&  跟py的and
||   or
!    not

i++

==   比较的值的
===   比较的值和数据类型
```

4.3 数字字符串转换

```js
1.数值转字符串
toString();
数字+空的字符串
2.字符串转数值
parseInt() 转整数
parseFloat() 转浮点型
Number()

3. var a = NaN
isNaN(a)

Infinity 无限大的
```



### 4.2关于 this 指向

```js
谁调用This  this 就指的是谁
```

### 4.3     BOM 

#### 1. 弹窗

1. 警告框

   `window.alert('mjj');`

2. 弹出框

   ```python
   var name = window.prompt('请输入你早晨吃了什么?','mjj');
   console.log(name);
   ```

   

3. 确认框

```js
var a = window.confirm('你确定要离开网站?');
console.log(a);
```

2. ### 方法

1. prompt()

   ```js
   prompt()方法接收两个参数,第一个参数是显示的文本,第二个参数是默认的文本,如果点击了确定,则name的结果为mjj
   ```

2. 定时任务方法

   `setTimeout()`和`setInterval()`方法

   ```js
   <!DOCTYPE html>
   <html>
   	<head>
   		<meta charset="utf-8">
   		<title></title>
   	</head>
   	<body>
   		<script type="text/javascript">
   			// 1.在5秒之后 执行相应的操作,可以做异步行为
   			 window.setTimeout(function(){
   				console.log('111');
   			},2000);
   			console.log('2222');
   			
   			// 2.周期性定时器
   			var num = 0;
   			var timer = null;
   			// 开启定时器
   			timer = window.setInterval(function(){
   				num++;
   				if(num === 10){
   					// 清除定时器
   					clearInterval(timer);
   				}
   				console.log(num);
   			},1000);
   		</script>
   	</body>
   </html>
   
   ```

3. ```js
   location.reload();   刷新页面
   setTimeout(function(){
       location.href = 'https://www.apeland.cn/web';
   },2000)     2秒后跳转到小猿圈
   ```

### 4.4 DOM

D O M 文档 对象 模型

#### 1.0 节点

1. 元素节点

   ```js
   个人理解就是一级一级的标签
   ```

2. 文本节点

   ```js
   文本节点，标签里的文本内容
   ```

3. 属性节点

   ```js
   标签的属性，例如 ID,Class，title等
   ```

#### 2.0   获取元素节点的方式

1. getElementByid（）方法

   ```js
   var oUl = document.getElementById('classList');
   ```

2. getElementByTagName()方法

   ```js
   var listItems = document.getElementsByTagName('li')
   ```

3. getElementsByClassName()

   ```js
   var classItems = document.getElementsByClassName('item');
   ```

4. 如果上面出现多个会是一个伪列表，通过索引取值

5. getAttrbute（）方法

   接收 一个参数，需要查询属性的名字

   ```js
   object.getAttribute(attributeName: DOMString)
   var oP = document.getElementsByTagName('p')[0];
   var title = oP.getAttribute('title');
   console.log(title);
   ```

6. setAttrbute()方法

   ```js
   2个参数，第一个参数是属性名，第二个参数是属性值
   object.setAttribute(attribute,value)
   ```

7. 节点属性 

   1. nodeName    节点的名称
      1. 元素节点的nodeName与标签名相同
      2. 属性节点的nodeName与属性的名称相同
      3. 文本节点的nodeName永远是#text
      4. 文档节点的nodeName永远是#document
   2. nodeValue     节点的值
      1. 元素节点的 nodeValue 是 undefined 或 null
      2. 文本节点的 nodeValue 是文本自身
      3. 属性节点的 nodeValue 是属性的值
   3. nodeType       节点的类型

   

   8.childNodes    元素节点下的所有子节点列表

   9.firstChild     数组中的第一个子节点

   10. lastChild   最后一个子节点

   #### 3.0  创建节点

   1.0 createElement（）方法可创建元素节点。此方法可返回一个Element对象

   ```js
   var newNode = document.createElement(tagName);
   
   ```

   2.0 插入节点 appendChild（）

   在指定的节点的最后一个子节点之后添加一个新的子节点

   ```js
   appendChild(newNode)
   ```

   3.0 insertBefore()

   方法可在已有的子节点前插入一个新的子节点

   ```js
   insertBefore(newNode,node);
   ```

   4.0 删除节点 removeChild（）

   删除成功返回删除节点，失败返回NULL

   ```js
   nodeObject.removeChild(node);
   ```

   5.0 替换元素节点 replaceChild（）

   ```js
   node.replaceChild(newnode,oldnew);
   var oldnew = document.getElementById('text');
   var newNode  = document.createElement('p');
   newNode.innerHTML = 'Vue';
   oldnew.parentNode.replaceChild(newNode,oldnew);
   ```

   6.0 创建文本节点  createTextNode

   ```js
   document.createTextNode(data);
   ```

   7.0 删除节点的属性

   ```js
     元素节点.removeAttribute(属性名);
    myNode.removeAttribute("class");
       myNode.removeAttribute("id");
   ```

   

### 4.5  事件

1. ##### 鼠标经过事件(onmouseover)&&鼠标移开事件(onmouseout)

   鼠标经过事件，当鼠标移到一个元素上时，该对象就出发了onmouseover事件，并执行onmouseover事件调用的程序。

   当鼠标移到一个元素上时，该对象就出发了onmouseout事件，并执行onmouseout事件调用的程序

   2. 鼠标单击事件(onclick)

### 4.6 伪数组 arguments

arguments代表的是实参。有个讲究的地方是：arguments**只在函数中使用**。

```js
    fn(2,4);
    fn(2,4,6);
    fn(2,4,6,8);

    function fn(a,b,c) {
        console.log(arguments);
        console.log(fn.length);         //获取形参的个数
        console.log(arguments.length);  //获取实参的个数

        console.log("----------------");
    }
```



## 4. JQuery

#### 1.0 JQuery两大特点

```
链式编程：比如.show()和.html()可以连写成.show().html()。

隐式迭代：隐式 对应的是 显式。隐式迭代的意思是：在方法的内部进行循环遍历，而不用我们自己再进行循环，简化我们的操作，方便我们调用。
```

#### 2. 0**jQuery入口函数与js入口函数的区别**

```js
区别一：书写个数不同：

Js 的入口函数只能出现一次，出现多次会存在事件覆盖的问题。

jQuery 的入口函数，可以出现任意多次，并不存在事件覆盖问题。

区别二：执行时机不同：

Js的入口函数是在所有的文件资源加载完成后，才执行。这些文件资源包括：页面文档、外部的js文件、外部的css文件、图片等。

jQuery的入口函数，是在文档加载完成后，就执行。文档加载完成指的是：DOM树加载完成后，就可以操作DOM了，不用等到所有的外部资源都加载完成。

文档加载的顺序：从上往下，边解析边执行。
```



#### 3.0 jquery 的使用

```js
<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<title></title>
	</head>
	<body>
		<div id="box" class="box">
			<p class="active">mjj1</p>
			<p class="active">mjj2</p>
			<input type="text">
		</div>
		<!-- 1.导入模块 -->
		<script src="js/jquery.js" type="text/javascript" charset="utf-8"></script>
		<script type="text/javascript">
			
			// console.dir($);
			// 选择器 基础选择器
			console.log($('.box'));//jquery对象 伪数组
			// jquery对象转换js节点对象
			console.log($('#box')[0]);
			// js节点对象转换jq对象
			var box = document.getElementById('box');
			console.log($(box),box);
			console.log($('#box>p'));			
			console.log($('input[type=submit]'));
			
			$('#box .active').click(function(){
				// 样式操作
				console.log(this);
				// this.style
				// 设置单个样式属性
				// $(this).css('color','red');
				// $(this).css('fontSize','20px');
				// 设置多个属性
				 $(this).css({
					 'color':'red',
					 "font-size":40
				 })
				 console.log($(this).css('color'));
			})
		</script>
	</body>
</html>
```

#### 4.0 获取对象方法



```js
　　console.log($('#app'));
　　console.log($('.box'));
　　console.log($('div'));
```

#### 5.0 设置DIV样式

```js
　$('div').css({
                'width': '200px',
                'height': '200px',
                "background-color":'red',
                'margin-top':'20px'
            })
```

#### 6.0 DOM对象转为jQuery 对象

```js
$(js对象);
```

#### 7.0 jQuery对象 转为 DOM 对象**：

```js
  jquery对象[index];      //方式1（推荐）

  jquery对象.get(index);  //方式2

jQuery对象转换成了 DOM 对象之后，可以直接调用 DOM 提供的一些功能。如：

$('div')[1].style.backgroundColor = 'yellow';
$('div')[3].style.backgroundColor = 'green';
```

#### 8.0 JQuery   的基本选择器

```js
$(function(){
            //三种方式获取jquery对象
            var jqBox1 = $("#box");
                   var jqBox2 = $(".box");
            var jqBox3 = $('div');

            //操作标签选择器
            jqBox3.css('width', '100');
            jqBox3.css('height', 100);
            jqBox3.css('background-color', 'red');
            jqBox3.css('margin-top', 10);


            //操作类选择器(隐式迭代，不用一个一个设置)
                    jqBox2.css("background", "green");
                    jqBox2.text('哈哈哈')

                    //操作id选择器
                    jqBox1.css("background", "yellow");
                   
        })
```

#### 9.0 层级选择器

```js
$(function () {
            //获取ul中的li设置为粉色
            //后代：儿孙重孙曾孙玄孙....
            var jqLi = $("ul li");
            jqLi.css("margin", 5);
            jqLi.css("background", "pink");

            //子代：亲儿子
            var jqOtherLi = $("ul>li");
            jqOtherLi.css("background", "red");
        });
```

#### 10.0 基本过滤选择器

![1364810-20180530170210737-2052147195](E:\homework2\img\1364810-20180530170210737-2052147195.png)

#### 11.0 属性选择器

![1364810-20180531183756059-1051347862](E:\homework2\img\1364810-20180531183756059-1051347862.png)



#### 12.0  筛选选择器

![1364810-20180531183756059-1051347862](E:\homework2\img\1364810-20180531183756059-1051347862.png)

#### 13.0 JQuery方法

1. 获取索引 的方法

   ```js
   $(this).index()
   ```

2. 对值得操作用那些方法

   ```js
   text('hello world');//给元素赋值文本
   text();//获取当前的文本
   html('<p>hello world</p>');
   html();//即获取标签又获取文本
   val();//只针对与表单控件
   ```

   

## 4.1 JQuery  动画效果

```js
show(2000，callback);//普通动画显示
hide()
toggle()
slieDown() //卷帘门效果
slideUp()
slideToggle()
fadeIn() //淡入淡出效果
fadeOut()
fadeToggle()
animate({样式属性}，2000，callback)//自定义动画效果  非常重要
stop()
```

自定义动画

- 语法：$(selector).animate({params}, speed, callback);

  作用：执行一组CSS属性的自定义动画。

  - 第一个参数表现：要执行动画的CSS属性（必选）
  - 第二个参数表示：执行动画时长（可选）
  - 第三个参数表示：动画执行完后，立即执行的回调函数（可选）

```js
<!DOCTYPE html>
<html>
<head lang="en">
    <meta charset="UTF-8">
    <title></title>
    <style>
        div {
            position: absolute;
            left: 20px;
            top: 30px;
            width: 100px;
            height: 100px;
            background-color: green;
        }
    </style>
    <script src="js/jquery.js"></script>
    <script>
        jQuery(function () {
            $("button").click(function () {
                var json = {"width": 500, "height": 500, "left": 300, "top": 300, "border-radius": 100};
                var json2 = {
                    "width": 100,
                    "height": 100,
                    "left": 100,
                    "top": 100,
                    "border-radius": 100,
                    "background-color": "red"
                };

                //自定义动画
                $("div").animate(json, 1000, function () {
                    $("div").animate(json2, 1000, function () {
                        alert("动画执行完毕！");
                    });
                });

            })
        })
    </script>
</head>
<body>
<button>自定义动画</button>
<div></div>
</body>
</html>
```





## 4.2  操作

####   1. 对属性的操作

```js
attr()
removeAttr()

prop()
removeProp()

prop方法获取的是当前节点对象的属性  单选操作
attr()获取的是标签上的属性
```

#### 2. 文档的操作

```js
增 改 删 替换
后置追加
父.append(子)
子.appendTo(父)
前置追加
prepend()
prependTo()

参考的元素.before(要添加的元素)
要添加的元素.insertBefore(参考的元素)
after()
inserAfter()

替换操作
replaceWith()
replaceAll()

删除操作
remove()即删除节点，事件也删除
detach()仅删除节点
empty() 清空元素
html(' ')
```

### 3. 事件

```js
.click();单击事件
jQuery解决单双击的冲突问题
setTimeout()用它解决
```

### 4. JQuery  的属性操作

1. ### attr()

- 设置属性值或者返回被选元素的属性

  ```html
  <!DOCTYPE html>
  <html lang="en">
  <head>
      <meta charset="UTF-8">
      <title>Title</title>
  
  </head>
  <body>
      <div id = 'box' class="boxs">你好</div>
      <script src="js/jquery.js" type="text/javascript" charset="utf-8"></script>
      <script>
          //获取值：attr设置一个属性值的时候，只是获取值。获取id值。
          var id = $('div').attr('id');
          console.log(id);//box
          //获取class值
          var cla = $('div').attr('class');
          console.log(cla);//boxs
  
          //设置一个值
          $('div').attr('id','thebox');//此时id值为thebox
          //设置多个值
          $('div').attr({'id':'manybox','class':'happy'});
  
      </script>
  </body>
  </html>
  ```

##### 2. removeattr

- 移除属性

  ```html
  <!DOCTYPE html>
  <html lang="en">
  <head>
      <meta charset="UTF-8">
      <title>Title</title>
  
  </head>
  <body>
      <div id = 'box' class="boxs" title="say">你好</div>
      <script src="js/jquery.js" type="text/javascript" charset="utf-8"></script>
      <script>
          //移除一个属性
          $("#box").removeAttr('title');
  
          //移除多个属性，如移除class  id属性
          $('#box').removeAttr('class id');
      </script>
  
  </body>
  </html>
  ```

##### 3. prop()

- 设置或者返回被选元素的属性和值。
- 当该方法用于**返回**属性值时，则返回第一个匹配元素的值。
- 当该方法用于**设置**属性值时，则为匹配元素集合设置一个或多个属性/值对。
  - 注意不是标签能加的属性值不能设置

```html
<body>
    <div id = 'box' class="boxs" title="say">你好</div>
    <span></span>
    <script src="js/jquery.js" type="text/javascript" charset="utf-8"></script>
    <script>
        //设置单个属性和值
        $('span').prop('class','like');
		//设置多个属性和值
		$('span').prop({'title':'like','id':"line"});
    </script>
</body>
</html>
```

- attr 和prop的区别

  ```html
  <!DOCTYPE html>
  <html lang="en">
  <head>
      <meta charset="UTF-8">
      <title></title>
  </head>
  <body>
      男<input type="radio" id='test' name="sex"  checked/>
      女<input type="radio" id='test2' name="sex" />
      <button>提交</button>
  
      <script type="text/javascript" src="js/jquery.js"></script>
      <script type="text/javascript">
          $(function(){
              //获取第一个input
              var el = $('input').first();
              //undefined  因为attr是获取的这个对象属性节点的值，很显然此时没有这个属性节点，自然输出undefined
              console.log(el.attr('style'));
              // 输出CSSStyleDeclaration对象，对于一个DOM对象，是具有原生的style对象属性的，所以输出了style对象
              console.log(el.prop('style'));
              console.log(document.getElementById('test').style);
  
              $('button').click(function(){
                  alert(el.prop("checked") ? "男":"女");
              })
          })
      </script>
  
  </body>
  </html>
  ```

- 什么时候使用attr(),什么时候使用prop()?
  - 有true,false两个属性使用prop();
  - 其他则使用attr()

##### 4.  addclass(添加多个类)

##### 5.removeclass（）

- 从所有匹配的元素中删除全部或者指定的类，移除指定类（1个或者多个）。

  - 移除一个

    ```javascript
        <script>
            $('div').removeClass('box');
            console.log($('div').attr('class'));//box2
        </script>
    ```

  - 移除全部

    ```javascript
        <script src="js/jquery.js" type="text/javascript" charset="utf-8"></script>
        <script>
            $('div').removeClass();
            console.log($('div').attr('class'));//''
        </script>
    ```

  - 小案例

    ```javascript
    <!DOCTYPE html>
    <html lang="en">
    <head>
        <meta charset="UTF-8">
        <title>Title</title>
        <style>
            .active{
                color:red;
            }
        </style>
    </head>
    <body>
        <ul>
            <li>1</li>
            <li>2</li>
            <li>3</li>
            <li>4</li>
        </ul>
        <script src="js/jquery.js" type="text/javascript" charset="utf-8"></script>
        <script>
            //通过增加类值，改变其CSS渲染
            $(function(){
                $('ul li').click(function(){
                    $(this).addClass('active').siblings('li').removeClass('active');
                })
            })
        </script>
    </body>
    </html>
    ```

##### 6.toggleClass

- 如果存在（不存在）就删除（添加）一个类。
- 语法：toggleClass('box')

```javascript
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Title</title>
</head>
<body>
    <span>123</span>
    <script src="js/jquery.js" type="text/javascript" charset="utf-8"></script>
    <script>
        //span标签没有class属性，点击之后span标签添加class='active'
        $('span').click(function(){
            $(this).toggleClass('active');
        })
    </script>
</body>
</html>
```



##### 7.html

- html() 是获取选中标签元素中所有的内容

  ```javascript
  $('#box').html();
  ```

- 示例：

  - 

    ```javascript
    <!DOCTYPE html>
    <html lang="en">
    <head>
        <meta charset="UTF-8">
        <title>Title</title>
    </head>
    <body>
        <div id="box">123</div>
        <script src="js/jquery.js" type="text/javascript" charset="utf-8"></script>
        <script>
            //点击div获取事件，将123改为$('#box').html('<a href="https://www.baidu.com">百度一下</a>')
            $('div').click(function(){
                $('#box').html('<a href="https://www.baidu.com">百度一下</a>');
            })
        </script>
    </body>
    </html>
    ```

  

##### 8.text

- text() 获取匹配元素包含的文本内容。

- 语法：

  ```javascript
  $('#box').text();
  ```

- 示例：

  ```js
  <!DOCTYPE html>
  <html lang="en">
  <head>
      <meta charset="UTF-8">
      <title>Title</title>
  </head>
  <body>
      <div id="box">123</div>
      <script src="js/jquery.js" type="text/javascript" charset="utf-8"></script>
      <script>
          $('#box').text("<a href='https://www.baidu.com'>百度一下</a>")
      </script>
  </body>
  </html>
  ```

##### 9.val

- 获取值：val()用于表单控件中获取值，比如input textarea select等等。

- 语法：

  - ```javascript
    $('input').val('设置表单控件值')
    ```

  - 示例：

    ```javascript
    <!DOCTYPE html>
    <html lang="en">
    <head>
        <meta charset="UTF-8">
        <title>Title</title>
    </head>
    <body>
        <input type="text" value="你好">
        <script src="js/jquery.js" type="text/javascript" charset="utf-8"></script>
        <script>
            $('input').val('替换表单的值');
        </script>
    </body>
    </html>
    ```

### 1.JQuery文档插入操作

#### 1.append

- 语法：父元素.append(子元素)
- 解释：追加某元素，在父元素中添加新的子元素。子元素可以为：stirng | element（js对象） | jquery元素。
- 如果追加的是jquery对象那么这些元素将从原位置上消失。简言之，就是一个移动操作。

```javascript
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Title</title>
</head>
<body>
    <div id="app">div标签</div>
    <div id = 'box'>id为box</div>
    <ul>
    </ul>
    <script type="text/javascript" src="js/jquery.js"></script>
    <script>
        //此种写法，待文档加载完成之后，调用回调函数中的代码
        $(function(){
            //给ul标签下添加子标签li
            $('ul').append(`<li>你好</li>`);
            // 给ul下添加Jquery对象
            $('ul').append($('div'));
            //给ul下添加element对象
        	var h3l = document.createElement('h3');
        	h3l.innerText='张三';
        	$('ul').append(h3l);
        })
    </script>
</body>
</html>
```

#### 2. appendTo

- 子元素.appendTo(父元素)
- 追加到某元素 子元素添加到父元素
  - 要添加的元素同样既可以是stirng 、element(js对象) 、 jquery元素

```javascript
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Title</title>
</head>
<body>
    <div id="app">div标签</div>
    <div id = 'box'>id为box</div>
    <ul>
    </ul>
    <script type="text/javascript" src="js/jquery.js"></script>
    <script>
        //将<li>好好学习</li>添加到ul标签内，并在li签添加class=active,渲染字体颜色为红色
        $(`<li>好好学习</li>`).appendTo($('ul')).addClass('active').css('color','red');
        //添加Jquery id=app 的div的 jquery元素到ul签下
        $($('div#app')).appendTo($('ul'));
		//创建事件h4，给事件添加值，再appendTo给ul下的签
		var h4l = document.createElement('h4');
        $(h4l).text('hello world');
        $($(h4l)).appendTo($('ul'));
    </script>
</body>
</html>
```

#### 3.prepend

- 前置添加，添加到父元素的第一个位置

- 父元素.prepend(子元素);

  ```javascript
  <!DOCTYPE html>
  <html lang="en">
  <head>
      <meta charset="UTF-8">
      <title>Title</title>
  </head>
  <body>
      <div id="box">div的box标签
          <p id="1">p1</p>
          <p id="2">p2</p>
      </div>
      <script src="js/jquery.js" type="text/javascript"></script>
      <script>
          //在id为box的标签前面插一句文本，不换行
          $('#box').prepend('我是box之前的一句话，');
          // 在p标签，id=1之前插一句标签
          $('p#1').prepend(`<p>我才是p1</p>`);
          
      </script>
  </body>
  </html>
  ```

#### 4.prependTo

- 前置添加到父元素的第一个位置

- 子元素.prependTo(父元素)

  ```javascript
  <!DOCTYPE html>
  <html lang="en">
  <head>
      <meta charset="UTF-8">
      <title>Title</title>
  </head>
  <body>
      <div id="box">div的box标签
          <p id="1">p1</p>
          <p id="2">p2</p>
  
      </div>
      <script src="js/jquery.js" type="text/javascript"></script>
      <script>
          //不能添加文档只能添加标签
          $(`<a href="">你好</a>`).prependTo('#box');
          //换行显示，p标签特性，独占一行
          $(`<p>p钱钱钱</p>`).prependTo('#box');
  
      </script>
  </body>
  </html>
  ```

#### 5.after、before

- 在匹配的元素之后插入元素

- 兄弟元素.after(要插入的兄弟元素)

  ```javascript
  <!DOCTYPE html>
  <html lang="en">
  <head>
      <meta charset="UTF-8">
      <title>Title</title>
  </head>
  <body>
      <div id="box">div的box标签
          <p id="1">p1</p>
          <p id="2">p2</p>
      <ul class="ul1">
          <li id="li1">li的第一个标签</li>
          <li id="li2">li的第二个标签</li>
          <li id="li3">li的第三个标签</li>
          <li id="li4">li的第四个标签</li>
      </ul>
      </div>
      <script src="js/jquery.js" type="text/javascript"></script>
      <script>
          //在id=li2标签后面插入li标签
          res = $('#li2').after(`<li id="new_li3">li2的后面</li>`);
          console.log(res);//li2 的jquery
          //在id=li3标签前面插入li标签
          $('#li3').before(`<li>li3的前面</li>`);
  
      </script>
  </body>
  </html>
  ```

#### 6.insertBefore,insertAfter

- 注意只能插入标签

```javascript
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Title</title>
</head>
<body>
    <div id="box">div的box标签
        <p id="1">p1</p>
        <p id="2">p2</p>
    <ul class="ul1">
        <li id="li1">li的第一个标签</li>
        <li id="li2">li的第二个标签</li>
        <li id="li3">li的第三个标签</li>
        <li id="li4">li的第四个标签</li>
    </ul>
    </div>
    <script src="js/jquery.js" type="text/javascript"></script>
    <script>
        //在id=li2标签后面插入li标签
        res = $(`<li id="new_li3">li2的后面</li>`).insertAfter('#li2');
        //在id=li3标签前面插入li标签
        $(`<li>li3的前面</li>`).insertBefore('#li3');
    </script>
</body>
</html>
```

### 2.克隆

- 克隆匹配的DOM元素

- $(选择器).clone()

  ```javascript
  //克隆id=1标签，然后插入id=li4标签后面
  result = ($('#1').clone())
  $("#li4").after(result);
  ```

### 3.修改操作

#### 1.replaceWith replaceAll

- 语法：$(selector).replaceWith(content);
- 将所有匹配的元素替换成指定的string ,js对象，Jquery对象

```javascript
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Title</title>
</head>
<body>
    <div class="box">box盒子</div>
    <div class="box">box盒子</div>
    <div class="box">box盒子</div>
    <div class="box">box盒子</div>
    <div class="box">box盒子</div>
    <div class="box">box盒子</div>
    <div class="box">box盒子</div>
    <div class="box">box盒子</div>
    <div class="box">box盒子</div>
    <ul>
        <li>1</li>
        <li>2</li>
        <li>3</li>
        <li>4</li>
    </ul>
    <script src="js/jquery.js" type="text/javascript"></script>
    <script>
        $(function () {
            //将所有li标签换成p标签
            $('li').replaceWith(`<p>这是p标签</p>`);
            //再将所有p标签换成div标签
            $('p').replaceWith(`<div>这是div标签</div>`);
            //将所有div标签替换成class为box的标签
            $('div').replaceWith($('.box'));
        	//将所有class = 'box'的盒子转换成按钮
            $(`<button>box盒子成按钮</button>`).replaceAll('.box');
        })
    </script>
</body>
</html>
```

### 4.删除操作：

#### 1.remove()

- 即删除节点后，事件也会删除（相当删除了整个标签）

- $(selector).remove();

  ```javascript
  <!DOCTYPE html>
  <html lang="en">
  <head>
      <meta charset="UTF-8">
      <title>Title</title>
  </head>
  <body>
      <ul>
          <li>1</li>
          <li>2</li>
          <li>3</li>
      </ul>
      <div>hello python</div>
      <button  id = 'btn'>按钮</button>
      <script src="js/jquery.js" type="text/javascript"></script>
      <script>
          $(function(){
              $('#btn').click(function(){
                  //把自己移除
                  // $(this).remove();
                  //div移除
                  $('div').remove();
              })
          });
  
      </script>
  </body>
  </html>
  ```

#### 2. detach()

- 删除节点后，事件会保留

- $(selector).detach(); 

  ```javascript
  <!DOCTYPE html>
  <html lang="en">
  <head>
      <meta charset="UTF-8">
      <title>Title</title>
  </head>
  <body>
      <div id ='box'>
          <ul>
              <li>1</li>
              <li>2</li>
              <li>3</li>
          </ul>
          <div>hello python</div>
      </div>
      <button  id = 'btn'>按钮</button>
      <script src="js/jquery.js" type="text/javascript"></script>
      <script>
          $(function(){
              $('#btn').click(function () {
                  //删除id=box的元素，但事件会保留
                  $('#box').detach();
              })
          });
  
      </script>
  </body>
  </html>
  ```

  ```javascript
  <!DOCTYPE html>
  <html lang="en">
  <head>
      <meta charset="UTF-8">
      <title>Title</title>
  </head>
  <body>
      <div id="box1">xjk</div>
      
      
      <div id="box2">xjk2</div>
      <button id="btn">按钮</button>
      <script src="js/jquery.js" type="text/javascript" charset="utf-8"></script>
      <script>
          $(function () {
              $("#btn").click(
                  function () {
                      //删除节点事件保留提升到box1后面
                      $('#box1').after($(this)).detach();
                  }
              )
          })
      </script>
  </body>
  </html>
  ```

  

#### 3.empty()

- 清空选中元素中的所有后代节点

- 语法：$(selector).empty(); 

  ```javascript
  <!DOCTYPE html>
  <html lang="en">
  <head>
      <meta charset="UTF-8">
      <title>Title</title>
  </head>
  <body>
      <ul>
          <li>1</li>
          <li>2</li>
          <li>3</li>
          <li>4</li>
      </ul>
      <button>按钮</button>
      <script src="js/jquery.js" type="text/javascript"></script>
      <script>
          $(function () {
              //按钮清空ul下所有标签
              $('button').click(function () {
                  $('ul').empty();
              });
  
          })
      </script>
  </body>
  </html>
  ```

  

### 5.事件

#### 1鼠标事件

- 当鼠标指针穿过元素时，会发生 mouseenter 事件,鼠标指针移出元素会发生 mouseleave 事件。与 mouseover 事件不同，只有在鼠标指针穿过被选元素时，才会触发 mouseenter 事件。如果鼠标指针穿过任何子元素，同样会触发 mouseover 事件。

```javascript
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Title</title>
    <style>
        #box{
            width: 100px;
            height: 40px;
            background-color: red;
            position: relative;
        }
        #child{
            width: 400px;
            height: 100px;
            background-color: blue;
            position: absolute;
            top: 40px; /*子框与父框出现间隙，会出现子框复弹现象*/
            display: none;
        }
    </style>
</head>
<body>
    <div id="box">
        <div id="child"></div>
    </div>
    <!--<input type="text">-->
    <script src="js/jquery.js" type="text/javascript" charset="utf-8"></script>
    <script>
        //法一：常规操作，但鼠标移入child 弹出框会收回
        // $('#box').mouseover(function () {
        //     console.log('进来了');
        //     $('#child').slideDown(1000);
        // });
        // $('#box').mouseout(function () {
        //     console.log('出来了');
        //     $('#child').slideUp(1000);
        // })
        //法二：优化后子框可以被选中。
        // $('#box').mouseenter(function () {
        //     console.log('进来了');
        //     //上来先stop之前动作，再执行。解决上面鼠标连续滑动后，鼠标停止滑动，而动画仍然执行问题
        //     $('#child').stop().slideDown(1000);
        // });
        // $('#box').mouseleave(function () {
        //     console.log("出来了");
        //     $('#child').stop().slideUp(1000);
        // });
        //法三：代码更加简洁
        $('#box').hover(function () {
            $('#child').stop().slideDown(1000);
        },function(){
            $('#child').stop().slideUp(1000);
        });

    </script>
</body>
</html>
```

#### 2.聚焦失焦



```javascript
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Title</title>
</head>
<body>
    <input type="text">
    <script src="js/jquery.js" type="text/javascript" charset="utf-8"></script>
    <script>
        //默认加载页面聚焦行为
        $('input[type=text]').focus(function () {
            //获取聚焦
            console.log('聚焦了');
        });
        $('input[type=text]').blur(function(){
            //获取失焦
            console.log('失去焦点了');
        })

    </script>

</body>
</html>
```

- 聚焦

  ![1559819830358](C:\Users\Xu jk\AppData\Roaming\Typora\typora-user-images\1559819830358.png)

- 失焦

  ![1559819853616](C:\Users\Xu jk\AppData\Roaming\Typora\typora-user-images\1559819853616.png)

#### 3.鼠标点击事件onmousedown

- onmousedown 事件会在鼠标按键被按下时发生。
- 语法：onmousedown="SomeJavaScriptCode"
  - SomeJavaScriptCode 规定该事件发生时执行的 JavaScript

```javascript
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Title</title>
    <style>
        #box{
            width: 200px;
            height: 200px;
            background-color:red;
        }
    </style>
</head>
<body>
    <div id ='box'></div>
    <script src="js/jquery.js" type="text/javascript" charset="utf-8"></script>
    <script>
        var chicks = 0;
        $('#box').mousedown(function(){
            setTimeout('done()',300);
            chicks++
        });
        function done() {
            if (chicks ===1){
                console.log('单击事件');
                chicks = 0
            }else if (chicks===2){
                console.log('双击事件');
                chicks = 0
            }

        }
    </script>
</body>
</html>
```



#### 4.获取键盘字母

- keydown按键被按下，keyup按键被松开。当按钮被按下时，发生 keydown 事件。
- keydown() /keyup()方法触发 keydown  /keyup()事件，或规定当发生 keydown  /keyup()事件时运行的函数。
- 语法：$(selector).keydown(function)

```javascript
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Title</title>
</head>
<body>
    <input type="text">
    <script src="js/jquery.js" type="text/javascript" charset="utf-8"></script>
    <script>
    $('input[type=text]').keydown(function(e){
            console.log(e);
        	console.log(e.keyCode,e.key);
        })

    </script>

</body>
</html>
//65 'a'
```

- 当鼠标按下触发一个JQuery事件

![1559820817057](C:\Users\Xu jk\AppData\Roaming\Typora\typora-user-images\1559820817057.png)

- 通过e.key 可以得到输入的字母，通过e.keyCode可以获得ASCII码值

#### 4.表单提交的preventDefault

- 该方法将通知 Web 浏览器不要执行与事件关联的默认动作（如果存在这样的动作）。例如，如果 type 属性是 "submit"，在事件传播的任意阶段可以调用任意的事件句柄，通过调用该方法，可以阻止提交表单。

```javascript
<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<title></title>
	</head>
	<body>
		<form action="">
			<input type="text" placeholder="用户名">
			<input type="text" placeholder="密码" >
			<input type="submit">
		</form>
		<script src="js/jquery.js" type="text/javascript" charset="utf-8"></script>
		<script type="text/javascript">
			$('form').submit(function (e) {
				e.preventDefault();//取消事件的默认动作（刷新网页）。
				console.log('111');
			})
		</script>
	</body>
</html>
```

### 6.AJAX





```javascript
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Title</title>
</head>
<body>
    <div id="header">
        <ul>

        </ul>
    </div>

    <div class="content">
        <ul>
        </ul>
    </div>
    <script type="text/javascript" src="js/jquery.js"></script>
    <script>
        ///////////////////通过AJAX显示标题信息//////////////////////
        $(function(){
            var sub_category = 0;
            $.ajax({
                url:'https://www.luffycity.com/api/v1/course_sub/category/list/',
                methods:'get',
                success:function(res){
                    console.log(res);
                    res.data.unshift({
                       id:sub_category,//把id给全局变量，让其执行getCourseList函数
                       name:'all'
                    });
                    if (res.error_no === 0){
                        res.data.forEach(function (item,index) {
                            $(`<li>${item.name}</li>`).appendTo("#header ul").attr('id',item.id);

                        })

                    }
                }
            }
            );
            //////////////////////////////////////////////////////
            getCourseList(sub_category);//调用函数将每一个标题子内容显示出来


            function getCourseList(sub_category) {
                $.ajax({
                    url:`https://www.luffycity.com/api/v1/courses/?sub_category=${sub_category}&ordering=`,
                    methods:'get',
                    success:function (res) {
                        console.log(res);
                        if (res.error_no===0){
                            $('.content ul').empty();
                            res.data.forEach(function (item,index) {
                                $(`<li><h3>${item.name}</h3></li>`).appendTo('.content ul');
                            })
                        }
                    }
                })

            }
            //获取点击事件通过id获取相应id子标签
            $('#header ul').on('click','li',function(){
                console.log($(this));
                sub_category =  $(this).attr('id');
                getCourseList(sub_category);
            })
        })
    </script>
</body>
</html>
```

















### 4.3 JQuery插件

1. ##### bootstrap框架

2. ##### bootstrap插件

###### 1 . 全局CSS

```js
.container 固定宽度容器
.container-fluid 100%宽度的容器
 栅格系统
.row
.col-lg- .col-md- .col-sm- .col-xs

文本颜色
text-muted
text-primary
text-success
text-danger
text-waring
text-info

背景颜色
bg-primary
bg-success
....

按钮
btn btn-default
btn btn-link
btn btn-success
btn btn-primary
....

对齐
.text-left
.text-right
.text-center
.text-justify 两端对齐 适应于英文

图片设置
.img-rounded
.img-circle
.img-thumbnail 

三角符号
.caret
关闭按钮
<button type="button" class="close" aria-label="Close"><span aria-hidden="true">&times;</span></button>
显示和隐藏内容
show/hidden

快速浮动
.pull-left 左浮动
.pull-right 右浮动
清除浮动
.clearfix
内容块居中
.center-block


表格
给table添加.table的类。默认给表格赋予少量的内补和边框
.table-striped 条纹状的
.table-bordered 带边框
.table-hover 状态类


表单
form
每组表单控件都会添加一个.form-group类中，表单控件通常都由.form-control
```

###### 2.  组件

组件（html+css+js的整合）化开发

组件里面找去

全局组件

局部组件





