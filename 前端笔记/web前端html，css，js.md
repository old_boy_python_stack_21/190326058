

# web前端基础

[TOC]



[TOC]

# web前端：

## 1.web前端：

- 什么是web?

  ```
  web 就是网页，是一种基于B/S的应用程序
  B：Browser浏览器
  S：Server服务器
  ```

- web组成？

  ```
  浏览器：代替用户向服务器发送请求
  服务器：接收用户请求并响应
  通信协议：规范数据在网络中是如何打包传送的。
  HTTP(HyperText transfer protocal)：超文本传输协议
  ```

- web服务器

  ```
  作用：
  	接收用户请求并左幅响应
  	存储web信息
  	具备安全性能
  
  ```

- web浏览器

  ```
  产品：
  	Apache
  	Tomcat
  	IIS
  	Nginx
  
  作用：
  	代替用户向服务器发送请求
  	做为响应数据的解释引擎，向用户呈现界面
  主流浏览器产品：
  	根据浏览器的内核、引擎进行划分
  	1. Microsoft IE
  	2.Google chrome
  	3.Mozilla FireFox
  	4.Apple Safari
  	5.Apple Safari
  浏览器引擎：
  	渲染：解析HTML,CSS控制页面渲染效果
  	JS引擎：解释网页脚本文件
  前端开发技术
  	HTML,CSS,JavaScript
  ```

## 2.HTML概述

### 1.HTML介绍

- 什么是HTML？

```
HyperText Markup language
 超文本    标记    语言
作用：书写网页结构
```

- 标记

```
也叫标签，主要用来标记网页中的内容，以实现网页的布局和JS操作
```

### 2.HTML在计算机中如何表现

- 所有的网页文件在计算机中以.html或。htm文件存储。
- 开发工具：

```
1.记事本
2.Editplus Pycharm
3.Sublime Webstorm VSCode Atom...
```

- 运行工具：
  - Chrome 浏览器
  - FireFox 浏览器
- 调试方法：F12打开网页源码。

## 3.HTML基础语法

#### 1.标签/标记

- 在网页中具有特殊功能的符号，HTML中所有的标签都以<>为标志，作用区分普通文本。

- 标签分类

  1.双标签

  ​		成对出现，有开始有结束。

  ​		eg:`<开始标签> 标签内容 </结束标签>`

  2.单标签

  ​		只有开始标签，没有结束标签，可以手动添加、表示结束。

  ​		eg:`<标签名>`

- 标签嵌套

  - 在成对的标签中出现其他标签。

  - 嵌套结构中，外层元素程为父元素，内层元素称为子元素

    ```html
    <a>
        <b>你好</b>
    </a>
    <!--a标签是超链接标签-->
    <!--b标签是文本加粗标签-->
    ```

  - 书写规范
    
    - HTML不区分大小写，保持适当缩进增强代码可读性

#### 2.标签属性

- 标签的属性主要用于修饰标签的显示效果。
- 相关语法：
  - 属性由属性名和属性值组成：属性名="属性值"
  - 属性的声明必须写在开始标签中：<开始标签 属性名='属性值'>..</结束标签>
  - 每个标签都可以设置多个属性，属性之间使用空格隔开：<开始标签 属性名='属性值' 属性名='属性值' 属性名='属性值'>..</结束标签>

#### 3.HTML注释

- 注释语句：<!--注释内容-->
- 注意：
  - 注释不能卸载标签里，如<p <!-- -->></p>
  - 注释不能嵌套

#### 4.HTML结构组成

- 文档的类型声明方式：开头第一行<!doctype html>      html5声明方式

  - 作用 ：告诉浏览器当前使用的HTML版本，以便浏览器能正确解析HTML标签和渲染样式。
  - 书写位置：文档最开始位置。

- 文档的开始和结束

  - 在文档类型声明之后，使用一对<html></html>标签标示文档的开始和结束

  - 在HTML标签中，包含两对子元素

    ```html
    <html>
    	<head></head>
    	<body></body>
    </html>
    
    <head>标签标示网页的头部信息，包含网页的标题，选项卡的图标，网页的关键字，作者，描述等信息，还可以引入外部的资源文件
    <body>标签表示网页的主体信息，网页所呈现的内容都要写在body里
    ```

  - `<head>`标签中包含的子元素
    - 设置网页的标题 `<title></title>`
    - 设置网页的字符编码`<meta charset="utf-8">`

#### 5.文本标签

##### 1.字符实体（具有特殊意义的符号）

- `&nbsp;` 表示一个空格
- ` &lt;`     表示小于号 <
- `&gt;`     表示大于号 >
- '&copy ;' 表示版权符号
- `&yen;`    表示人民币符号¥

##### 2.文本样式

- `<i></i> `  斜体显示文本
- `<u></u>`  文本添加下划线
- `<b></b> `  文本加粗效果
- `<s></s>`  文本添加删除线
- `<sup></sup>`  上标显示文本
- `<sub></sub>`  下标显示文本
- 特点：这些标签可以与其他的标签或文本共行显示

##### 3.标题标签

- 以不同的文字大小和加粗方式显示文本

- 语法：`<hn></hn> `n取值 1- 6，字体大小是逐级递减的

  ```html
  <h1>这是一级标题</h1>
  <h2>这是二级标题</h2>
  <h3>这是三级标题</h3>
  <h4>这是四级标题</h4>
  <h5>这是五级标题</h5>
  <h6>这是六级标题</h6>
  ```

- 特点：

  - 会改变文字的大小并且具有加粗效果
  - 每个标题都会具备垂直的空白距离
  - 每个标题都独占一行，不与其他元素共行显示
  - 每个标题都可以添加属性 align
    - 取值：left / center / right
    - 设置文本的水平对齐方式，默认居左对齐
    - `<h1 align="center">一级标题<h1>`

##### 4.段落标签

- 作用：突出显示一段文本，每段文本都独占一行或一块，不予其他元素共行显示，并且也具备垂直的空白距离
- 语法：
  - `<p></p>` 
  - 属性：align 设置文本的水平对齐方式
  - 取值： left / center / right

##### 5.格式标签

- 换行标签：`<br>`
- 水平线标签:`<hr>`

##### 6.分区标签

- 块分区
  - `<div></div>`
  - 作用：划分页面结构，配合css实现网页布局
  - 特点：独占一块显示
- 行分区元素
  - `<span></span>`
  - 作用：设置同一行文本的不同样式，结合CSS
  - 特点：可以与其他元素或文本共行显示，允许在一行文本中使用多个span

##### 7.标签分类

- 块级元素
  - 只要在网页中独占一行，不予其他元素共行显示的元素都是块级元素。
  - 作用：都网页可以做布局
  - 特点：都添加align属性，设置内容水平对齐方式
  - eg:h1~h6,ul,ol,li,form,table,tr,p,div
- 行内元素/内联元素
  - 可以与其他元素共行显示。
  - eg:i b strong sub sup span

#### 6.列表标签

- 作用：按照从上到下的方式来排列数据

- 列表的组成：

  - 有序列表`<ol><ol>`

    - 默认是按照数字排序

  - 无序列表`<ul></ul>`

    - 默认以实心原点作为标识

  - 列表项

    - 列表中的每一条数据都是一个列表项
    - 语法 ：<li></li>
    - 注意 ：列表项要嵌套在列表标签中使用

  - 列表属性

    - 有序列表--ol

      - type属性：设置项目符号

      ```
       1 ：按照数字排列显示，默认值
       a : 按照小写字母顺序排列
       A ：按照大写字母排列
       i : 按照罗马数字排列
       I : 按照罗马数字排列
      ```

      - start属性：设置从第几个字符开始排序
        - 取值：数字

    - 无序列表--ul

      - type属性：设置项目符号

        ```
        disc 实心圆点（默认）
        circle 空心圆点
        square 实心方块
        none 不显示项目符号
        ```

  - 列表嵌套

    - 在一个列表中出现其他列表。

      ```html
      <!DOCTYPE html>
      <html lang="en">
      <head>
          <meta charset="UTF-8">
          <title>Title</title>
      </head>
      <body>
          <ul type="circle">
              <li>山东
                  <ol type="I">
                      <li>德州</li>
                      <li>青岛</li>
                      <li>济南</li>
                      <li>淄博</li>
                  </ol>
              </li>
              <li>吉林
                  <ol type="I">
                      <li>长春</li>
                      <li>吉林</li>
                      <li>白城</li>
                      <li>敦化</li>
                  </ol>
              </li>
              <li>北京
                  <ol type="I">
                      <li>朝阳</li>
                      <li>昌平</li>
                      <li>西城</li>
                      <li>东城</li>
                  </ol>
              </li>
          </ul>
      
      </body>
      </html>
      ```

#### 7.图像与超链接标签

- URL
  - 统一资源定位符 ：用来标识网络中资源的位置，俗称路径URL 组成 ：协议 域名 文件目录及文件名。
  - URL分类：
    - 绝对路经：
      - 从根目录开始查找，常用于网络文件路经。
    - 相对路经：
      - 从当前文件所在的文件夹开始查找
    - 根相对路径 （了解）：
      - 从Web程序所在的根目录开始查找资源文件
      - 注意：网络URL中不能出现中文，URL是严格区分大小写的。
- 图像标签
  - 在网页中插入一张图片
  - 语法 ：<img>
  - 属性 ：
    - src :指定要显示的图片路径
    - width ：设置图片的宽度，以像素px为单位，也可以省略单位
    - height ：设置图片的高度
      注意 : src 为必填属性，宽高可以省略，省略宽高的话，图片将以原始尺寸显示在网页中
    - title ：用来设置图片的标题，当鼠标悬停在图片上方时出现
    - alt : 用来设置图片加载失败之后的提示文本

#### 8.超链接标签

- 什么是超链接标签

  - 能够实现从当前文件跳转到其他文件的标签

- 语法：

  - `<a></a>`

  - 标签属性：

    - href :必填属性，指定链接地址，以路径形式给出,#表示当前页，不会发生页面刷新操作，如果属性为"",也表示当前页，但是包含了网络请求，相当于刷新页面。
    - target :可选属性，设置目标文件的打开方式。
      - _self ：默认值，表示在当前窗口打开
      - _blank:表示新建窗口打开

    ```html
    <!DOCTYPE html>
    <html lang="en">
    <head>
        <meta charset="UTF-8">
        <title>Title</title>
    </head>
    <body>
        <a href="https://www.baidu.com/">百度一下</a>
        <a href="">回到顶部</a>
    </body>
    </html>
    ```

- 锚点链接

  - 链接到当前文件的指定位置

    ```html
    1. 设置锚点<a name="anchor1"></a>
    2. 设置跳转<a href="#anchor1">早年经历</a>
    
    <body>
        <a href="https://www.baidu.com/" name="ac1">百度一下</a>
        <a href="#ac1">回到顶部</a>
    </body>
    ```

#### 9.表格

##### 1.标签介绍：

- 表格标签：`<table></table>`
- 行标签：`<tr></tr>`
- 单元格标签：`<td></td>`

- 创建顺序：

  - 在表格标签中嵌套行标签，每一个 tr 就代表一行
  - 在行标签中创建单元格标签，用来存放数据

  ```html
      <table>
          <tr><!--代表一行-->
              <td>id</td><!--代表单元格-->
              <td>name</td>
              <td>age</td>
          </tr>
          <tr>
              <td>1</td>
              <td>xjk</td>
              <td>18</td>
          </tr>
          <tr>
              <td>2</td>
              <td>half</td>
              <td>15</td>
          </tr>
      </table>
  ```

##### 2.标签属性

- table 属性
  - border ：设置边框，取值以px为单位的数值（px可以省略）
  - width ：设置宽度
  - height ：设置高度
  - align ：设置表格在其父元素中的水平对齐方式
  - cellpadding : 设置单元格的内边距（内容与边框之间的距离），取值为px单位的数值
  - cellspacing : 设置单元格的外边距（单元格与单元格之间的距离，或者单元格与表格边框之间的距离），取值像素为单位的数值
  - bgcolor : 设置表格的背景颜色，取值可以是英文的颜色名称

```html
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Title</title>
</head>
<body>
    <a href="https://www.baidu.com/" name="ac1">百度一下</a>
    <a href="#ac1">回到顶部</a>
    <table border="1"  align="center" cellpadding="10" cellspacing="1" bgcolor="red">
        <tr>
            <td>hello</td>
            <td>html</td>
        </tr>
    </table>
</body>
</html>
```

- tr 属性
  - bgcolor : 设置当前行的背景颜色
  - align ：设置当前行中内容的水平对齐方式
    取值 ： left / center / right
  - valign :设置当前行内容的垂直对齐方式
    		取值 ：top / middle / bottom,默认垂直居中。
- td属性
  - width 设置单元格的宽度
  - height 设置单元格的高度
  - align 单元格内容的水平对齐方式
  - valign 单元格内容的垂直对齐方式
  - bgcolor 单元格的背景颜色
- 单元格的合并
  - 单元格独有的属性 colspan rowspan
  - 单元格的跨列合并
    - 从当前单元格的位置开始，横向向右合并几个单元格colspan = "3" ->跨3列进行合并(包含自身)
  - 单元格的跨行合并
    - 从当前的单元格开始，纵向向下合并单元格rowspan = "3" ->向下跨3行合并单元格
  - 注意：一旦发生单元格合并，跨列合并，要删除当前行中多余的单元格跨行合并，要删除其后行中多余的单元格始终保持表格结构完整。

```html
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Title</title>
</head>
<body>
    <table border="1" cellspacing="0" width="400" height="400">
        <tr>
            <td colspan="2" bgcolor="red"></td>
            <!--<td>2</td>-->
            <td rowspan="2" bgcolor="#00008b"></td>
        </tr>
        <tr>
            <td rowspan="2" bgcolor="#663399"></td>
            <td bgcolor="#ff1493"></td>
            <!--<td>6</td>-->
        </tr>
        <tr>
            <!--<td>7</td>-->
            <td colspan="2" bgcolor="#adff2f"></td>
            <!--<td>9</td>-->
        </tr>
    </table>
</body>
</html>
```

- 行分组

  - 允许将表格中的一行或者是若干行划分为一组，便于管理。

  - 语法

    ```html
    1. 表头行分组
    <thead>
        <tr>
        	<td></td>
        </tr>
    </thead>
    2. 表尾行分组
    <tfoot>
        <tr>
        	<td></td>
        </tr>
    </tfoot>
    3. 表主体信息
    <tbody>
        <tr>
        	<td></td>
        </tr>
    </tbody
    ```

#### 10.表单

##### 1.用于接收用户的数据并且提交给服务器

- 表单2个要素
  - form表单元素
  - 表单控件
    - 提供了能够跟用户交互的可视化组件

##### 2.form元素

- 注意：form元素本身是不可见的，却不能省略，因为数据的提交功能要由 form 元素完成

- 语法：`<form></form>`

- form标签属性

  - action
    - 指定数据提交的目的地址
  - method
    - 数据请求方式 get /post (默认get)
    - get
      - 通常用于向服务器端获取数据
      - 特点：
        - 提交的数据会以参数的形式拼接在URL后面
        - 安全性较低
        - 提交数据最大2KB
    - post
      - 将数据提交给服务器处理
      - 特点：
        - 隐式提交，看不到提交数据
        - 安全性较高
        - 没有数据大小限制

  ```html
  <!DOCTYPE html>
  <html lang="en">
  <head>
      <meta charset="UTF-8">
      <title>Title</title>
  </head>
  <body>
      <form action="">
          <input type="text" name="user">
          <input type="password" name="pwd">
          <input type="submit" value="提交">
      </form>
  </body>
  </html>
  ##################################################################################
  file:///D:/code/day43.html
  
  file:///D:/code/day43.html?user=alex&pwd=123
  ```

##### 3.表单控件

- 作用：提供与用户交互可视化组件（这里注意只有放在表单元素中的表单控件才允许被提交）

- 分类：

  - **文本框与密码框**

    - 语法：

      - 文本框：`<input type="text">`
      - 密码框：`<input type="password">`

    - 属性：

      - name 属性 定义当前控件的名称，缺少的话无法提交。

        name = "uname"

      - value 属性，要提交给服务器的值，同时也是默认显示在控件上的值。
      - maxlength 用来限制用户输入最大字符串。
      - placeholder  用户输入之前显示在框中的提示文本。

      ```html
      <!DOCTYPE html>
      <html lang="en">
      <head>
          <meta charset="UTF-8">
          <title>Title</title>
      </head>
      <body>
          <input type="text" name="user" maxlength="5" placeholder="输入至少5位数字">
          <input type="password" name="pwd" maxlength="10" placeholder="输入至少10位的密码">
      </body>
      </html>
      ```

  - **单选框和复选框**

    - 单选按钮`<input type="radio">`

    - 复选框`<input type="checkbox">`

    - 属性：

      - name  定义控件名称，还起到分组的作用，一组中的按钮名称必须保持一致。
      - value 属性  设置当前控件的值，最终提交给服务器。
      - checked 属性 设置预选中状态 可以省略属性值，也可以使用"checked" 作为值。

      ```html
      <!--单选框-->
      <!DOCTYPE html>
      <html lang="en">
      <head>
          <meta charset="UTF-8">
          <title>Title</title>
      </head>
      <body>
          <input type="radio" name="sex" checked="checked">男
          <input type="radio" name="sex">女
      </body>
      </html>
      <!--多选框-->
      <!DOCTYPE html>
      <html lang="en">
      <head>
          <meta charset="UTF-8">
          <title>Title</title>
      </head>
      <body>
          <input type="checkbox">抽烟
          <input type="checkbox">喝酒
          <input type="checkbox">烫头
      </body>
      </html>
      ```

  - **隐藏域和文件选择框**

    - 隐藏域

      - 作用：需要提交给服务器但是却不需要呈现给用户的数据，都可以放在隐藏域中。
      - 语法：`<input type='hidden'>`

      - 属性：
        - name  控件名称
        - value  控件的值

    - 文件选择框

      - 作用：选择文件上传，发送给服务器

      - 语法：`<input type="file">`

      - 属性：name 定义控件名称。

        

  - **下拉选择框**

    - `<select name="province">`

      - 属性：
        - multiple 可进行多选(ctrl+鼠标左键)

      ```html
          <select name="pro" id="" multiple>
              <option value="hebei">河北</option>
              <option value="heilongjiang">黑龙江</option>
              <option value="hunan">湖南</option>
          </select>
      ```

  - **文本域**

    - 支持用户多行文本

    - 语法：`<textarea></textarea>`

    - 属性：

      - name 控件名称
      - cols  指定文本默认显示的列数，一行中能显示的英文字符量，中文减半
      - rows 指定文本域能够显示的行数

    - 注意：文本域可以由用户调整大小

      ```html
      <textarea name="wenben" id="123" cols="30" rows="10"></textarea>
      ```

  - **按钮**

    - 提交按钮

      ```html
      <input name='' id='' type="submit">
      ```

    - 重置按钮

      ```html
      <form action="">
          <input type="text" name="123">
      	<input name='' id='' type="reset" value="重置按钮">
      </form>
      ```

    - 普通按钮

      ```html
      <input type="button" value="点击">
      ```

    - 按钮显示文本

      ```html
      <button>按钮显示文本</button>
      ```

      - 注意：
        - 按钮标签可以在任何地方使用，不局限在form表单中使用
        - 按钮标签使用在form中，默认具有提交功能，等同于input  submit
        - 可以添加属性type  取值 submit/reset/button  进行区分（非必填）
        - 在表单外做为普通按钮使用时，需要通过JS，动态绑定事件

  - label**特殊方法**

    - 使用label标签包裹表单控件要显示的文本信息，为label标签添加for属性，属性值与所要绑定的表单控件的ID属性值保持一致，实现文本与控件的绑定。

      ```html
      <label for="male">男</label>
      <input type="radio" name="gender" value="male" id="male">
      ```

## 4.练习题：

1.字体标签包含哪些？

```
h1~h6  b  strong  i em
```

2.超链接标签a标签中href属性有什么用？

```
链接到一个新的地址
回到顶部
跳转邮箱
下载文件
```

3.img中标签中src和alt属性有什么？

```
src 提供一个链接图片资源
alt 图片失败的时候显示的标题
```

4.如何创建一个简易的有边框的表格？

```
<table border='1' cellspacing=0>
	<th>
		<td>id</td>
		<td>name</td>
	</th>
	<tr>
		<td>1</td>
		<td>mjj</td>
	</tr>
<table>
```

5.form 标签中的action属性和method属性的作用？

```
action:提交到服务器的地址，如果是空的字符串，它表示当前服务器地址 
method:提交的方式。get和post
- get
  - 通常用于向服务器端获取数据
  - 特点：
    - 提交的数据会以参数的形式拼接在URL后面
    - 安全性较低
    - 提交数据最大2KB
- post
  - 将数据提交给服务器处理
  - 特点：
    - 隐式提交，看不到提交数据
    - 安全性较高
    - 没有数据大小限制
```

6.在form标签中表单控件input中type类型有哪些？并分别说明他们代指的含义

```
text 单行文本输入框 
password 密码输入框 
radio 单选框 
	产生互斥效果，给每个单选按钮设置相同的name属性值
	如何默认选中，给单选按钮添加checked属性 
checkbox 多选框 默认选中添加checked属性 
submit 提交按钮 ﬁle 上传文件 
datetime

下拉列表
<select>    
	<option name   value selected >抽烟</option>    
	<option name   value selected >喝酒</option>    
	<option name   value selected >烫头</option> 
</select>	
多行文本输入框 textarea 
cols 列 rows 行
```

7.表单控件中的name属性和value属性有什么意义？

```
name属性值：提交到当前服务器的名称 
value属性值：提交到当前服务器的值 以后给服务器来使用
```

8. 用socket 开启一个服务端，客户端访问服务器？

9. 在一行内显示的标签有哪些？

   ```
   b strong i em a img input td
   ```

10. 独占一行的标签有哪些？

    ```
    h1~h6 ul ol li form table tr p div
    ```

## 5.CSS

### 1.CSS介绍

- CSS ：Cascading Style Sheet 层叠样式表
- 作用 ：修饰和美化页面元素，实现网页排版布局

### 2.CSS使用

#### 1行内样式/内联样式

- 特点 ：在具体的标签中使用style属性，引入CSS样式代码

- 语法：

  - <标签 style="CSS 样式声明">
  - CSS 样式声明 / 语句：
    - 对当前元素添加样式
    - 语法：CSS 属性 ：值；
  - 注意：CSS 样式声明可以是多条，<标签 style="属性:值；属性:值；"

- 常见的CSS属性

  - 设置字体大小：

    1.属性:font-size

    2.取值 以像素为单位的数值，浏览器默认的字体大小是 16px

  - 设置字体颜色:

    1.属性：color

    2.取值：颜色的英文单词或RGM(数值,数值,数值)或#b0b0b0（十六进制）

  - 设置背景颜色
    - background-color
    - 取值：颜色的英文单词或RGM(数值,数值,数值)或#b0b0b0（十六进制）

  ```html
  <!--行内样式-->
  <!DOCTYPE html>
  <html lang="en">
  <head>
      <meta charset="UTF-8">
      <title>Title</title>
  </head>
  <body>
      <p style="background: red;color:RGB(123,145,164);font-size: 19px;">asd</p>
  </body>
  </html>
  ```

#### 2.文档内嵌/内部样式表

- 特点：将CSS代码与具体的标签相分离，在HTML文档中使用<style></style>标签引入CSS代码。

- 语法：

  ```html
  		<style>
  			选择器1
  			选择器2
  			选择器3
  			...
  		</style>
  ```

- 选择器：

  1.使用文档内嵌方式引入CSS样式表时，实现了结构与样式相分离，但是需要自己定义选择器来匹配文档中元素，为其应用样式。

  2.作用 ： 匹配文档元素为其应用样式。

  3.语法：选择器实际上由两部分组成

  选择器（符）{
  						属性 ：值；
  						属性 ：值；
  					}

  ```html
  et :
  	标签选择器/元素选择器：
  	使用标签名作为选择符，匹配文档中所有的该标签，并应用样式
  	p {
  		color :green;
  		font-size :20px;
  	}
  ```

  - 注意：<style></style>可以书写在文档中的任意位置，但是基于样式优先的原则，一般写在<head></head>中。

#### 3.外链方式/外部样式表

- 定义外部的.css文件，在HTML中引入即可，真正实现文档与样式表分离。
- 语法：
  - 在外部的样式表中使用选择器定义样式；在HTML文档中使用<link>引入CSS文件
  - <link rel="stylesheet" href="url">

### 3.样式表特征

#### 1继承性

- 继承性
  - 大部分的CSS属性都是可以被继承的
  - 子元素或者后代元素可以继承父元素中的样式
  - 所有的文本属性都可以被继承，块元素的宽度与父元素保持一致

#### 2.层叠性

- 允许为元素定义多个样式，共同起作用。

  ```html
  p{
  	color:red;
  	background-color:blue;
  }
  ```

#### 3.样式表的优先级

- 从低到高

- 从低到高 ：
  - 浏览器缺省设置 ：浏览器默认样式

  - 文档内嵌/外链方式 ：在不发生样式冲突时，样式共同起作用；
    如果发生样式冲突，后来者居上，最后定义的样式最终显示

  - 行内样式的优先级最高

### 4.CSS选择器

- 作用：规范页面中哪些元素能够应用声明好的样式
- 目的 ：匹配页面元素

#### 1.标签选择器/元素选择器

- 特点：将标签名作为选择符，来匹配文档中所有的该标签，包含后代元素

- 语法：

  ```
  标签名{
  	属性：值
  }
  标签名如a,div
  ```

  text-decoration : none;取消下划线

  ```html
  <!DOCTYPE html>
  <html lang="en">
  <head>
      <meta charset="UTF-8">
      <title>Title</title>
      <style>
          div{
              background-color: #aaffff;
              font-size:20px;
          }
          a{
              text-decoration:none;
          }
      </style>
  </head>
  <body>
      <div>这是标签</div>
      <a href="">点一下</a>
  </body>
  </html>
  ```

#### 2.类选择器

- 特点：通过元素的class属性值进行匹配

- 语法：

  ```
  .cl{
  	样式
  }
  <p class="cl">p文本</p>
  以英文.开头，跟上class属性值，中间没有空格
  ```

```html
创建文件 class-selector
创建几个元素 div p span h1
使用类选择器为上述元素添加样式
1. 文本大小为24px
2. 背景颜色为橘色
3. 文本斜体显示 font-style : italic;

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Title</title>
    <style>
        .cl{
            font-size:24px;
            background-color: orange;
            font-style:italic;
        }
    </style>
</head>
<body>
    <div>这是标签</div>
    <p class="cl">p文本</p>
</body>
</html>
```

#### 3.ID选择器

- id 作用：HTML中所有的元素都有一个id属性，主要用来表示该元素在文档中的标志，具有唯一性。
- id 选择器：通过元素的id属性值进行匹配

- 语法：

  ```html
  属性值{
  	样式
  }
  ```

  注意：通常网页中外围的结构化标签，都使用id进行标识，具有唯一性。

  ```html
  <!DOCTYPE html>
  <html lang="en">
  <head>
      <meta charset="UTF-8">
      <title>Title</title>
      <style>
          #seq{
              width:100%;
              height:50px;
              background-color: pink;
          }
      </style>
  </head>
  <body>
      <span id="seq">这是一个span文本</span>
  </body>
  </html>
  ```

#### 4.群组选择器

- 作用：为多个选择器设置共同的样式。

- 语法：

  ```html
  选择器1，选择器2，选择器3{
  	样式
  }
  <!DOCTYPE html>
  <html lang="en">
  <head>
      <meta charset="UTF-8">
      <title>Title</title>
      <style>
          div,h1,p{
              background-color: gray;
              margin:0;<!--外边距为0-->
          }
      </style>
  </head>
  <body>
      <h1>标题1</h1>
      <div>div</div>
      <p>p</p>
  </body>
  </html>
  ```

#### 5.后代选择器

- 作用：依托于元素的后代关系来匹配元素（既包含直接子元素，也包含后代元素）。

- 语法：

  ```
  选择器1 选择器2{
  	样式
  ｝
  ```

- 在选择器1对应的元素中匹配后代元素，后代元素需满足选择器2。

```html
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Title</title>
    <style>
        #nav span{
            border-radius:2px;
            background-color:RGB(204,204,204);
            padding-left:10px;
        }
    </style>
</head>
<body>
    <p id="nav">
        <span>你好</span>
        <span>
            <span>在那里</span>
        </span>
        <span>再见</span>
    </p>
</body>
</html>
```

#### 6.子代选择器

- 作用：依托元素的子代关系进行匹配，只匹配直接子元素。

- 语法：

  ```
  选择器1>选择器2 {
  ｝
  ```

  ```html
  <!DOCTYPE html>
  <html lang="en">
  <head>
      <meta charset="UTF-8">
      <title>Title</title>
      <style>
          #nav>ul>li{
              color:red;
          }
      </style>
  </head>
  <body>
      <div id ="nav">
          <ul>
              <li>你好</li>
              <li>你好<span>good<span>good</span></span></li>
          </ul>
      </div>
  </body>
  </html>
  ```

#### 7.伪装选择器

- 分类：

  - 超链接伪类选择器
  - 动态伪类选择器

- 作用 ：

  - 主要是针对元素的不同状态去设置样式

  - 超链接的不同状态

    - 访问前 :link
    - 访问后 :visited
    - 激活（鼠标点按不松）:active
    - 鼠标滑过 :hover

  - 其他元素具备

    - 鼠标滑过  :hover
    - 鼠标点按  :active

  - 表单控件

    - 获取到焦点时的状态 :focus
    - 对文本框和密码框而言，当用户点击输入时，都视为获取焦点状态

  - 伪类选择器需要与其他选择器结合使用，设置元素在不同状态下的样式。

    ```html
    <!DOCTYPE html>
    <html lang="en">
    <head>
        <meta charset="UTF-8">
        <title>Title</title>
        <style>
            a:link{
                color: #8e3fff;
                text-decoration:none;
            }
            a:hover{
                color: #ff45aa;
                text-decoration:none;
            }
            a:visited{
                color: greenyellow;
                text-decoration:none;
            }
            a:active{
                color:blue;
                text-decoration:none;
            }
    
        </style>
    </head>
    <body>
        <a href="#">哈哈</a>
    </body>
    </html>
    ```

注意：如果要给超链接添加四种状态下的样式，必须按照以下顺序书写伪类选择器

```
	:link
	:visited
	:hover
	:active
	简称 “爱恨原则 LoVe / HAte ”
```

#### 8.属性选择器

- 通过元素的tpye实现CSS。

  ```html
  <!DOCTYPE html>
  <html lang="en">
  <head>
      <meta charset="UTF-8">
      <title>Title</title>
      <style>
          input[type="text"]{
              background-color: red;
          }
      </style>
  </head>
  <body>
      <input type="text">
  </body>
  </html>
  ```

#### 9.伪元素选择器

- 在文本前后增加内容

```html
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Title</title>
    <style>
        /*在p标签前添加元素*/
        p::before{
            content:"抽烟";
        }
        /*在p标签后添加元素*/
        p::after{
            content:"烫头";
        }
    </style>
</head>
<body>
    <p>喝酒</p>
</body>
</html>
#设置文本第一个字符
#通过伪元素添加内部为行内元素
```

### 5.选择器的优先级

- 当多个选择器的样式同时应用到一个元素上时，要按照不同选择器的优先级来应用样式。

  ```html
  div{
  	color:red;
  }
  #box{
  	color:green;
  }
  .c1{
  	color:blue;
  }
  <div id="box" class="c1">文本</div>
  ```

- 判断选择器的优先级，主要看选择器的权重（权值）数值越大，优先级越高。

  ```html
  选择器          权值
  标签选择器       1
  类选择器/伪类    10
  id选择器         100
  行内样式         1000
  ```

- 如果是复杂的选择器 （后代，子代），选择器最终的权值是各选择器权值之和 

```
权重比较:        
1.数选择器数量： id 类 标签  谁大它的优先级越高，如果一样大，后面的会覆盖掉前面的属性        
2.选中的标签的属性优先级用于大于继承来的属性，它们是没有可比性     
3.同是继承来的属性            
	3.1 谁描述的近，谁的优先级越高            
	3.2 描述的一样近，这个时候才回归到数选择器的数
```

### 6.练习题：

1.div和span标签在网页中的作用？

```html
div将网站分割成独立的逻辑区域division分割
span:小区域标签，在不影响文本正常显示的情况下，单独设置对应的样式。
<style>
	span.active{
	font-weight:bold;
	}
</style>
<p>
	<span>内容</span>
</p>
```

2.CSS基础选择器和高级选择器有哪些？

```
- 基础选择器
  - 类选择器
  - 标签选择器
  - id选择器
- 高级选择器
  - 后代选择器
  - 子代选择器
  - 组合选择器
  - 交集选择器
  - 伪装选择器      （link visited hover active）
  - 伪元素选择器
```

3.盒子模型的属性有哪些？并说明属性的含义，画出盒子模型图？

```
- width:内容宽度
- height:内容的高度
- border:边框
- padding:内边距
- margin:外边距
```

4.如何让文本垂直和水平居中?

```html
<style>
    div{
        width:200px;
        height:60px;
        text-align:center;
        line-height:60px;
    }
</style>
<div>
    wusir
</div>

让行高等于盒模型的高度实现垂直居中
使用text-align:center;实现文本水平居中
```

5.如何清除a标签的下划线？

```
text-decoration:none;
none:无线，underline:下划线,overline:上划线,line-through:删除线.
```

6.如何重置网页样式？

```html
html,body,p,ul,ol{
    margin: 0;
    padding: 0;
}
/*通配符选择器 */
*{
    margin: 0;
    padding: 0;
}
a{
    text-decoration: none;
}
input,textarea{
    border: none;
    outline: none;
}
```

7.如何清除input和textarea标签的默认边框和外线？

```
border:0;
outline:0;
```

8.在css中哪些属性是可以继承下来的？

```
1.字体系列属性：
font-family:字体系列
font-weight:字体的粗细
font-size:字体的大小
fnot-style:字体的风格

2.文本系列属性
text-indent:文本缩进
text-align:文本水平对齐
line-height:行高
word-spacing:单词之间的间距
letter-spacing:中文或者字母之间的间距
text-transform:控制文本大小
color:文本颜色

3.元素可见性
visibility:控制元素显示隐藏

4.列表布局属性
list-style列表风格，包括list-style-type,list-style-image

5.光标属性：
cursor:光标显示为何种形态



color,text-xxx,font-xxx,line-height,letter-spacing,word-spacing
```

9.如何正确比较css中的权重？

```
如果选中了标签
	数选择器的数量  id  class 标签 谁大优先级越高 如果一样大，后面优先级越大
    如果没有选中标签，当前属性是被继承下来，他们的权重为0，与选中的标签没有可比性
    都是继承来的，谁描述的近，就显示谁的属性（就近（选中的标签越近）原则），如果描述的一样近，继续数选择器的数量。
    !important 它设置当前属性的权重为无限大，大不过行内样式的优先级
```

10.块级标签和行内标签?

```
块级标签:
1.可以设置高度，如果不设置宽度，默认是父标签的100%的宽度
2.独占一行
p
div
ul
ol
li
h1~h6
table
tr
form

行内标签：
1.不可以设置宽高
2.在一行内显示
a 
span
b
strong
em
i

行内块标签
1.可以设置宽高
2.在一行内显示
input
img
```

### 7.常用格式化排版

#### 1.字体的属性

- 设置字体属性：

  ```html
  body{font-family:"Microsoft Yahei"}
  
  body{font-family:"Microsoft Yahei","宋体","黑体"}
  #备选字体可以有无数个，那么浏览器在去解析这个代码的时候，是从左往右解析的，如果没有微软雅黑，再去找宋体，最后黑体。
  ```

- 字体大小：

  - 最常用的像素单位：px、em、rem，这里咱们先介绍一种单位，px。
  - **px**:像素是指由图像的小方格组成的，这些小方块都有一个明确的位置和被分配的色彩数值，小方格颜色和位置就决定该图像所呈现出来的样子。

- 字体颜色：

  - 颜色表示方法在css中有三种方式：
    - 英文单词表示法：red/green/blue
    - rgb表示法
    - 十六进制表示法

- 字体样式font-style

  - 网站中的字体分为了普通字体和斜体字体，我们可以使用font-style属性来设置对应的字体样式。

  - normal正常,italic斜体,oblique斜体

    ```
    <!DOCTYPE html>
    <html lang="en">
    <head>
        <meta charset="UTF-8">
        <title>Title</title>
        <style>
            p{
                font-style:italic;
            }
        </style>
    </head>
    <body>
        <p>你好</p>
    
    </body>
    </html>
    ```

- 字体粗细

  - font-weight

    | 属性值  | 描述                 |
    | ------- | -------------------- |
    | normal  | 普通的字体粗细，默认 |
    | bold    | 加粗的字体粗细       |
    | lighter | 比普通字体更细的字体 |
    | bolder  | 比bold更粗的字体     |
    | 100~900 | 400表示normal        |

- 文本属性：

  - text-decoration

    | 属性值       | 描述                               |
    | ------------ | ---------------------------------- |
    | none         | 无文本的修饰                       |
    | underline    | 文本下划线                         |
    | overline     | 文本上划线                         |
    | line-through | 穿过文本的线，~~可以模拟删除线~~。 |

- 文本缩进
  - 我们通常写文章的时候，首字母要空两格。那么我们需要使用
  - text-indent,它的属性值是像素(px、em、rem)单位。

- 行间距
  
- line-height:数值px/em
  
- 纵纹字间距/字母间距

  - letter-spacing:数值px;
  - word-spacing:数值px;

- 文本对齐

  - text-align

    | 属性值 | 描述             |
    | ------ | ---------------- |
    | left   | 文本左对齐，默认 |
    | right  | 文本右对齐       |
    | center | 中心对齐         |

#### 2.px,em,rem

- px单位的名称为像素，它是一个固定大小的单元，像素的计算是针对（电脑/手机）屏幕的，一个像素（1px）就是（电脑/手机）屏幕上的一个点，即屏幕分辨率的最小分割。

- 它是相对于当前对象内文本的字体尺寸；如果没有人为设置当前对象内文本的字体尺寸，那么它相对的是浏览器默认的字体尺寸

- rem只相对于根目录，即HTML元素。所以只要在html标签上设置字体大小，文档中的字体大小都会以此为参照标准，一般用于自适应布局。

  ```html
  <!DOCTYPE html>
  <html lang="en">
  <head>
      <meta charset="UTF-8">
      <title>Title</title>
      <style>
          *{
              padding:0;
              margin:0;
          }
          html{
              font-size:20px
          }
          .box{
              font-size:12px;
              width:20rem;
              height:20rem;
              background-color: red;
          }
      </style>
  </head>
  <body>
      <div class="box"></div>
  </body>
  </html>
  ```

  

### 8.CSS盒模型

- 常用块元素由：

  ```
  p,h1-h6,div,ul,ol,tr,li,form
  ```

- 常用内联元素由：

  ```
  a,span,em,i,strong,u,
  ```

- 常见的内联块元素

  ```
  input img
  ```

| 标签类别   | 特点                                          |
| ---------- | --------------------------------------------- |
| 块状元素   | 1.独自占据整一行 2.可以设置宽高               |
| 内联元素   | 1.所有的内联元素在一行内显示 2.不可以设置宽高 |
| 行内块元素 | 1.在一行内显示 2.可以设置宽高                 |

- 盒模型常用属性：
  - 盒子模型中有四个属性：内容的宽高、内边距、外边距、边框。

![1559118680417](C:\Users\Xu jk\AppData\Roaming\Typora\typora-user-images\1559118680417.png)

#### 1.padding

- padding有四个方向，分别描述四个方向的padding。

  ```
  padding-top:10px;
  padding-right:3px;
  padding-bottom:50px;
  padding-left:70px;
  ```

- 综合属性，多个属性用空格隔开。

  ```html
  /*上 右 下 左 四个方向*/
  padding: 20px 30px 40px 50px ;
  /*上 左右  下*/
  padding: 20px 30px 40px;
  /* 上下 左右*/
  padding: 20px 30px;
  /*上下左右*/
  padding: 20px;
  ```

#### 2.border

- 盒子模型的边框，在样式表中称为border。我们知道呢，我们用的手机，都会有手机壳。手机壳的样式、颜色、薄厚程度等都是这个壳的形态。同样呢，盒模型的边框也有三要素：**粗细 线性样式 颜色**。

  ```html
  <!DOCTYPE html>
  <html lang="en">
  <head>
      <meta charset="UTF-8">
      <title>border的使用</title>
      <style type="text/css">
          .box{
              width: 200px;
              height: 200px;
              /*1像素实线且红色的边框*/
              border: 1px solid red;
          }
      </style>
  </head>
  <body>
      <div class="box"></div>
  </body>
  </html>
  ```

  - **按照三要素书写border**

  ```html
  border-width:3px;
  border-style:solid;
  border-color:red;
  /*上面三句代码相当于一句代码：border:3px solid red;*/
  /*同样,也可以分别设置边框四个方向的粗细 线性样式 颜色,跟padding的四个方向一样。*/
  /*上下5px  左右10px*/
  border-width:5px 10px;
  /*上：实现  右：点状  下：双线 左：虚线*/
  border-style: solid dotted double dashed;
  /*上：红色 左右：绿色 下：黄色*/
  border-color: red green yellow;
  ```

  - 按照方向划分

  ```
  border-top-width: 10px;
  border-top-color: red;
  border-top-style: solid;
  border-right-width: 10px;
  border-right-color: red;
  border-right-style: solid;
  border-bottom-width: 10px;
  border-bottom-color: red;
  border-bottom-style: solid;
  border-left-width: 10px;
  border-left-color: red;
  border-left-style:solid;
  
  相当于border:10px solid red;
  ```

- 清除默认边框：border:none;或者border:0;         outline:none;

#### 3.margin

- 在盒子模型中称为外边距，样式表中叫margin。表示盒子到另一个盒子的距离。既然是两者之间的距离，那么就会产生水平之间的距离和垂直之间的距离。同样情况下，外边距也有四个方向，跟padding类似。

- **水平方向的外边距**

  ```html
  <!DOCTYPE html>
  <html lang="en">
  <head>
      <meta charset="UTF-8">
      <title>水平方向外边距</title>
      <style type="text/css">
          span:nth-child(1){
              background-color: green;
              margin-right: 20px;
          }
          span:nth-child(2){
              background-color: red;
              margin-left: 30px;
          }
      </style>
  </head>
  <body>
      <span class="box_l">左盒子</span><span class="box_r">右盒子</span>
  </body>
  </html>
  #nth-child(1)，获取span子类，参数为第1个。
  ```

- **垂直方向外边距**

  ```html
  <!DOCTYPE html>
  <html lang="en">
  <head>
      <meta charset="UTF-8">
      <title>Title</title>
      <style>
          div{
              width: 200px;
              height: 200px;
  
          }
          #box1{
              background-color: red;
              margin-bottom: 30px;
          }
          #box2{
              background-color: black;
              margin-top: 100px
          }
          #box3{
              background-color: yellowgreen;
          }
          #box3 p{
              background-color: goldenrod;
              margin-top: 30px;
          }
      </style>
  </head>
  <body>
  <!-- margin 在垂直方向上会出现外边距合并现象，塌陷。以设置的最大的magrin距离为基准-->
    <div id="box1"></div>
      <div id="box2"></div>
  ```
```
  
  注意：盒模型的外边距水平方向上不会出现问题，在垂直方向上会出现“**外边距合并**”的现象。
  
```
  什么是外边距合并呢？在有些文献中说这种现象叫塌陷问题。只有在垂直方向上，当两个同级的盒子，在垂直方向上设置了margin之后，那么以较大者为准。

  在网页排版中，外边距合并的问题也会时常出现，我们如何避免出现这种问题呢？

  很简单，我们如果想让上下的两个盒子中间有间距，只需要设置一个盒子的一个方向即可。没必要去碰触外边距塌陷的问题，还得找解决问题的方法，得不偿失。

### 9.浮动

- 浮动是网页布局中非常重要的一个属性。那么`浮动`这个属性一开始设计的初衷是为了网页的`文字环绕效果`。
- 文字环绕现象

  ```html

  <!DOCTYPE html>
  <html lang="en">
  <head>
      <meta charset="UTF-8">
      <title>水平方向外边距</title>
      <style type="text/css">
          html{
              font-size:24px;
          }
          img{
              width:20rem;
          }
          #sep{
              float: left;
          }
          p{
              border:1px solid red;
              text-indent:2rem;
              font-size:1rem;
          }
      </style>
  </head>
  <body>
      <div id="sep">
          <img src="https://i1.mifile.cn/a4/xmad_15590487108559_JOpcA.jpg" alt="加载失败" title="王源">
      </div>
      <p>
          我去我去我去我去我去我去我去我去我去我去我去我去我去我去我去我去我去我去我去我去我去我去我去我
          我去我去我去我去我去我去我去我去我去我去我去我去我去我去我去我去我去我去我去我去我去我去我去我
          我去我去我去我去我去我去我去我去我去我去我去我去我去我去我去我去我去我去我去我去我去我去我去我
          我去我去我去我去我去我去我去我去我去我去我去我去我去我去我去我去我去我去我去我去我去我去我去我
          我去我去我去我去我去我去我去我去我去我去我去我去我去我去我去我去我去我去我去我去我去我去我去我
          我去我去我去我去我去我去我去我去我去我去我去我去我去我去我去我去我去我去我去我去我去我去我去我
          我去我去我去我去我去我去我去我去我去我去我去我去我去我去我去我去我去我去我去我去我去我去我去我
          我去我去我去我去我去我去我去我去我去我去我去我去我去我去我去我去我去我去我去我去我去我去我去我
          我去我去我去我去我去我去我去我去我去我去我去我去我去我去我去我去我去我去我去我去我去我去我去我
          我去我去我去我去我去我去我去我去我去我去我去我去我去我去我去我去我去我去我去我去我去我去我去我
          我去我去我去我去我去我去我去我去我去我去我去我去我去我去我去我去我去我去我去我去我去我去我去我
          我去我去我去我去我去我去我去我去我去我去我去我去我去我去我去我去我去我去我去我去我去我去我去我
          我去我去我去我去我去我去我去我去我去我去我去我去我去我去我去我去我去我去我去我去我去我去我去我
          我去我去我去我去我去我去我去我去我去我去我去我去我去我去我去我去我去我去我去我去我去我去我去我
      </p>
  </body>
  </html>
  ```

#### 1.浮动属性

- css样式表中用float来表示，它有

| 属性值  | 描述                                         |
| ------- | -------------------------------------------- |
| none    | 表示不浮动，所有之前讲解的HTML标签默认不浮动 |
| left    | 左浮动                                       |
| right   | 右浮动                                       |
| inherit | 继承父元素的浮动属性                         |

```html
    <meta charset="UTF-8"/>
    <title>浮动属性用法</title>
    <style type="text/css">
        .left{
            width: 200px;
            height: 200px;
            background-color: red;
            color: #fff;
            /*左浮动*/
             float:left;
        }
        .right{
            width: 200px;
            height: 200px;
            background-color: green;
            color: #fff;
            /*右浮动*/
             float:right;
        }
    </style>
    <div class="left">左边的盒子</div>
    <div class="right">右边的盒子</div>
```

#### 2.浮动现象

```
我们之前说浮动的设计初衷是为了做”文字环绕效果“。那么我们就来看一下如果对盒子设置了浮动，会产生什么现象？
1浮动的元素脱离了标准文档流，即脱标
2浮动的元素互相贴靠
3浮动的元素会产生”字围“效果
4浮动元素有收缩效果
```

#### 3.浮动的破坏性

- 浮动之后效果展示：
  ![1559203907316](C:\Users\Xu jk\AppData\Roaming\Typora\typora-user-images\1559203907316.png)

- 由此可见，浮动之后，盒子因为脱离了标准文档流，它撑不起父盒子的高度，导致父盒子`高度塌陷`。如果网页中出现了这种问题，会导致我们整个网页的布局紊乱。我们一定要去解决这种父盒子高度塌陷的问题。

- 那么如何解决*浮动给网页带来的问题*？

#### 4.清除浮动的方式

我们知道浮动具有破坏性，它能使父盒子高度塌陷、导致页面紊乱。那么在css布局中对于浮动的解决方案有四种：

##### 1.父盒子设置固定高度

- 给父盒子设置固定高度，缺点不灵活，后期不易维护。应用领域导航栏。

##### 2.内墙法

- 在浮动元素的后面加一个空的块级元素(通常是div),并且该元素设置clear:both属性。
- clear属性，字面意思就是清除，那么both,就是清除浮动元素对我左右两边的影响。如下示例

```html
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Title</title>
    <style>
        *{
            padding: 0;
            margin: 0;
        }
        .father{
            width: 800px;
            margin: 100px auto;
            border: 1px solid #000;
        }
        .child1{
            width: 200px;
            height: 400px;
            background-color: red;
            float: left;
        }
        .child2{
            width: 300px;
            height: 200px;
            background-color: green;
            float: right;
        }
        .clear{
            clear: both;
        }
    </style>
</head>
<body>
<!--内墙法：给最后一个浮动元素的
后面添加一个空的块级标签，并且设
置该标签的属性为clear:both;-->
    <div class="father">
        <div class="child1">A盒子</div>
        <div class="child2">B盒子</div>
        <div class="clear"></div>

    </div>
```

- 存在问题：冗余过长。

##### 3.伪元素清除法（推荐使用）

- 伪元素选择器很简单。就像伪类一样，让伪元素添加了选择器，但不是描述特殊的状态，他们允许您为元素的某些部分设置样式。在这里只介绍一个。

- 语法：

  ```html
  p::after{
      /*p::after{}一定要有content。表示在p元素内部的最后面的添加内容*/
      content:'...'
  }
  ```

- 示例：

  ```html
   ...
   .clearfix::after{
              content:'';
              display: block;
              clear: both;
              /*visibility: hidden;*/
              /*height: 0;*/
          }
  
      </style>
  </head>
  <body>
      <div class="father clearfix">
          <div class="child1">盒子A</div>
          <div class="child2">盒子B</div>
  
      </div>
  ```

##### 4.overflow:hidden

- CSS属性overflow定义一个元素的内容太大而无法适应盒子的时候该做什么。它是overflow-x和overflow-y的简写属性

| 属性值  | 描述                                             |
| ------- | ------------------------------------------------ |
| visible | 默认值。内容不会被修剪，会呈现在元素框之外       |
| hidden  | 内容会被修剪，并且其余内容不可见                 |
| scroll  | 内容会被修剪，浏览器会显示滚动条以便查看其余内容 |
| auto    | 由浏览器定夺，如果内容被修剪，就会显示滚动条     |
| inherit | 规定从父元素继承overflow属性的值                 |

- hidden

```html
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Title</title>
    <style>
        body{
            overflow: hidden;
        }
        .box{
            width: 300px;
            height: 300px;
            border: 1px solid #000;
            overflow: hidden;

        }
    </style>
</head>
<body>
    <div class="box">
    此处有一篇1万字文章
    </div>
</body>
</html>
#注意：此处内容会被修建，并且其余内容不可见
```

- overflow:hidden||auto|scroll属性之后，它会形成一个BFC区域，我们叫做它为`块级格式化上下文`。BFC只是一个规则。它对浮动定位都很重要。浮动定位和清除浮动只会应用于同一个BFC的元素。
- 浮动不会影响其它BFC中元素的布局，而清除浮动只能清除同一BFC中在它前面的元素的浮动。

```html
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Title</title>
    <style>
        *{
            padding: 0;
            margin: 0;
        }
        .father{
            width: 800px;
            margin: 100px auto;
            border: 1px solid #000;
            /*一旦设置一个Box盒子 除了overflow：visible；
            它会形成一个BFC,BFC它有布局规则： 它会让内部浮动元素计算高度*/
            overflow: hidden;
        }
        .child1{
            width: 200px;
            height: 400px;
            background-color: red;
            float: left;
        }
        .child2{
            width: 300px;
            height: 200px;
            background-color: green;
            /*float: right;*/
            /*overflow: hidden;*/
            float: left;
        }


    </style>
</head>
<body>

    <div class="father ">
        <div class="child1">A盒子</div>
        <div class="child2">B盒子</div>
    </div>



</body>
</html>
```

#### 5.深入理解BFC：

- 了解BFC

  - （1）B: BOX即盒子，页面的基本构成元素。分为 inline 、 block 和 inline-block三种类型的BOX

    （2）FC: Formatting Context是W3C的规范中的一种概念。它是页面中的一块渲染区域，并且有一套渲染规则，它决定了其子元素将如何定位，以及和其他元素的关系和相互作用。

    （3）常见的 Formatting Context 有 Block fomatting context (简称BFC)和 Inline formatting context (简称IFC)

##### 1.BFC布局：

```
1.内部的Box会在垂直方向，一个接一个地放置。
2.Box垂直方向的距离由margin决定。属于同一个BFC的两个相邻Box的margin会发生重叠
3.每个元素的margin box的左边， 与包含块border box的左边相接触(对于从左往右的格式化，否则相反)。即使存在浮动也是如此。
4.BFC的区域不会与float 元素重叠。
5.BFC就是页面上的一个隔离的独立容器，容器里面的子元素不会影响到外面的元素。反之也如此。
6.计算BFC的高度时，浮动元素也参与计算
```

##### 2.哪那些元素会生成BFC

```
1.根元素
2.float属性不为none
3.position为absolute或fixed
4.display为inline-block
5.overflow不为visible
```

##### 3.display属性

- display属性设置元素如何显示

  | 值           | 描述                                                         |
  | ------------ | ------------------------------------------------------------ |
  | none         | 此元素不会被显示                                             |
  | block        | 此元素将显示为块级元素，此元素前后会带有换行                 |
  | inline       | 默认，此元素会被显示为内联元素，元素前后没有换行             |
  | inline-block | 行内块，将元素显示为行内块元素，设置属性后，其他的行内块级元素会排在同一行。 |
  | table-cell   | 此元素会作为一个表格单元格显示                               |

  

### 10.定位

- 定位是一个相当复杂的话题，在去深入理解定位之前，我们先来聊一下之前我们的标准文档流下的布局。
- 应用:网页中小广告，返回顶部的UI。

#### 1.Position属性

- 定位方式：top,right,bottom,left属性决定该元素的最终位置。

| 属性值   | 描述                                                         |
| -------- | ------------------------------------------------------------ |
| static   | **默认。静态定位**， 指定元素使用正常的布局行为，即元素在文档常规流中当前的布局位置。此时 `top`, `right`, `bottom`, `left` 和 `z-index`属性无效。 |
| relative | **相对定位**。 元素先放置在未添加定位时的位置，在不改变页面布局的前提下调整元素位置（因此会在此元素未添加定位时所在位置留下空白） |
| absolute | **绝对定位**。不为元素预留空间，通过指定元素相对于最近的非 static 定位祖先元素的偏移，来确定元素位置。绝对定位的元素可以设置外边距（margins），且不会与其他边距合并 |
| fixed    | **固定定位**。 不为元素预留空间，而是通过指定元素相对于屏幕视口（viewport）的位置来指定元素位置。元素的位置在屏幕滚动时不会改变 |

#### 2.静态定位

- 静态定位意味着“元素默认显示文档流的位置”。没有任何变化。

```html
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>静态定位</title>
    <style type="text/css">
        .positioned{
            position: static;
            background-color: red;
        }
    </style>
</head>
<body>
    <p class="positioned">我是静态定位的元素</p>
</body>
</html>
```

#### 3.相对定位

- 相对定位的元素是在文档中的正常位置的偏移，但是不会影响其他元素的偏移。
- 参考点：以自身原来的位置进行定位，可以使用top,left,right,bottom对元素进行偏移。
- 现象：
  - 不脱离标准文档流，单独设置盒子相对定位之后，。不用top,left,right,bottom对元素进行偏移，那么与普通的盒子没什么区别。
  - 有压盖现象。用`top,left,right,bottom`对元素进行偏移之后，明显定位的元素的层级高于没有定位的元素(用top,left,right,bottom层级低)。

#### 4.绝对定位

- 相对定位的元素并没有脱离标准文档流，而绝对定位的元素则脱离了文档流。在标准文档流中，如果一个盒子设置了绝对定位，那么该元素不占据空间。并且绝对定位元素相对于最近的非static祖先元素定位。当这样的祖先元素不存在时，则相对于根元素页面的左上角进行定位。
- 参考点：
  - 相对于最近的非static祖先元素定位，如果没有非static祖先元素，那么以页面左上角进行定位。

#### 5.应用

- 相对定位的盒子，一般用于“子绝父相”，布局模式参考

```html
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Title</title>
    <style>
        .box{
            width: 400px;
            height: 500px;
            background-color:red;
            position: relative;
        }
        div .c1{
            width: 200px;
            height: 200px;
            background-color:blue;
            position: absolute;
            top: 20px;
            left: 20px;
        }
        div .c2{
            width: 100px;
            height: 100px;
            background-color:greenyellow;
            position: absolute;
            top: 20px;
            left: 20px;
        }
    </style>
</head>
<body>
    <div class="box">
        <div class="c1">子1</div>
        <div class="c2">子2</div>

    </div>
</body>
</html>
```

- 子绝父相

#### 6.z-index:

- z-index有以下几个规则：
  - z-index只应用在定位元素，默认z-index:auto;
  - z-index取值为整数，数值越大，它的层级越高。
  - 如果元素设置了定位，没有设置z-index,那么谁写在后面的表示谁层级越高。
  - 从父现象，通常布局方案我们采用`子绝父相`，比较的是父元素的z-index值，哪个父元素的z-index值越大，表示子元素的层级越高。

### 11.背景属性和边框属性

- 背景属性值

| 属性值              | 属性值                     | 描述                             |
| ------------------- | -------------------------- | -------------------------------- |
| background-color    | d单侧颜色法，RGB，十六进制 | s设置元素的背景颜色              |
| background-image    | url("wy.png")              | 给一个元素设置一个或多个背景图像 |
| background-position | top,left,center,百分比，px | 为每一个背景图片设置初始位置     |
| background-repreat  | repeat-x                   | repeat-y                         |

- background-image设置背景图片

```html
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Title</title>
    <style>
        .bg{
            width: 400px;
            height: 400px;
            border:1px solid blue;
            background-image:url("wy.jpg")
        }
    </style>
</head>
<body>
    <div class="bg"></div>
</body>
</html>
```

- background-repeat背景图像

  - 属性值：

  | 数值值    | 描述                                       |
  | --------- | ------------------------------------------ |
  | repeat    | **默认值**。表示背景图水平和垂直方向都平铺 |
  | no-repeat | 表示背景图水平和处置方向都不平铺           |
  | repeat-x  | 表示背景图只有水平方向上平铺               |
  | repeat-y  | 表示背景图只有垂直方向上平铺               |

```css
.bg{
    width: 1000px;
    height: 1000px;
    border:1px solid blue;
    background-image:url("sj6.jpg");
    background-repeat:repeat-x;/*沿着轴方向平铺*/
}
```

- bacground-position背景图定位

  - 语法：

    ```
    background-position:x y;
    background-position:position position
    ```

  - 取值

    ```
    关键字取值：
    	top ,right,bottom,left,center
    长度值取值：
    	px,em
    百分比：
    	50%
    ```

  - 示例

    ```css
    background-position:0 0;
    /*左上角显示*/
    ```

    ```css
    background-position:top right; /*背景图像在右上角*/
    ```

    ```css
    background-position:top center; /*背景图像上方居中显示*/
    ```

    ```css
    background-position:center center;/*背景图像居中显示*/
    ```

    - 设置长度单位：

    ![1559378594189](C:\Users\Xu jk\AppData\Roaming\Typora\typora-user-images\1559378906931.png)

    ```css
    background-position:50px 100px;
    ```

    - 设置为负值：

    ```css
    background-position:-20px -30px;
    ```

#### 2.雪碧图：

- CSS雪碧图技术：即CSS Sprite,也有人叫它CSS精灵图，是一种图像拼合技术。该方法是将多个小图标和背景图像合并到一张图片上，然后利用css的背景定位来显示需要显示的图片部分。

![1559378940045](C:\Users\Xu jk\AppData\Roaming\Typora\typora-user-images\1559378940045.png)

- 优点：
  - 有效的减少HTTP请求数量
  - 加速内容
- 雪碧图的实现原理
  - 它通过css的背景属性的backrground-position的来控制雪碧图的显示。
  - 控制一个层，可显示的区域范围大消息，通过一个窗口，进行背景图的移动。

#### 3.border-radius

- 传统的圆角生成方案，必须使用多张图片作为背景图案。css3的出现，使得我们再也不必浪费时间去制作这些图片，并且还有其他多个优点：
  - 减少维护的工作量。图片文件的生成、更新、编写网页代码，这些工作都不再需要了。
  - 提高网页性能。由于不必再发出多条的HTTP请求，网页的载入速度将变快
  - 增加视觉可靠性。（网络拥堵、服务器出错、网速过慢等等），背景图片会下载失败，导致视觉效果不佳。CSS3就不会发生这种情况。

```html
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Title</title>
    <style>
        .bor-radius{
            width: 400px;
            height: 400px;
            border-radius:20px;
            background-color: red;
    }
    </style>

</head>
<body>
    <div class="bor-radius"></div>
</body>
</html>
```

- 显示效果如下：

  ![1559379358246](C:\Users\Xu jk\AppData\Roaming\Typora\typora-user-images\1559379358246.png)

- 单角设置：

  - border-top-left-radius
  - border-top-right-radius
  - border-bottom-right-radius
  - border-bottom-left-radius

  - 示例：

    ```
    border-bottom-left-radius：
    ```

    ![1559379506432](C:\Users\Xu jk\AppData\Roaming\Typora\typora-user-images\1559379506432.png)

- border-radius效果实现一个无边框圆

  ```html
  <!DOCTYPE html>
  <html lang="en">
  <head>
      <meta charset="UTF-8">
      <title>Title</title>
      <style>
          .cicle{
              width: 200px;
              height: 200px;
              background-color: red;
              border-radius:50%;
          }
      </style>
  
  </head>
  <body>
      <div class="cicle"></div>
  </body>
  </html>
  ```

- 制作一半的圆

  ```html
  <!DOCTYPE html>
  <html lang="en">
  <head>
      <meta charset="UTF-8">
      <title>Title</title>
      <style>
          .cicle{
              width: 200px;
              height: 100px;
              background-color: red;
              border-top-left-radius: 100px;
              border-top-right-radius: 100px;
          }
      </style>
  
  </head>
  <body>
      <div class="cicle"></div>
  </body>
  </html>
  ```

  ![1559379748903](C:\Users\Xu jk\AppData\Roaming\Typora\typora-user-images\1559379748903.png)

#### 4.盒子阴影

- 通过box-shadow属性设置元素的阴影

- 语法：

  ```css
  box-shadow: h-shadow v-shadow blur color inset;
  ```

  | 值       | 描述                                     |
  | -------- | ---------------------------------------- |
  | h-shadow | 必需。水平阴影的位置。允许负值           |
  | v-shadow | 必需。垂直阴影的位置。允许负值。         |
  | blur     | 可选。模糊距离。                         |
  | *color*  | 可选。阴影的颜色。                       |
  | inset    | 可选。将外部阴影 (outset) 改为内部阴影。 |

```css
        .bg{
            width: 400px;
            height: 400px;
            border:1px solid blue;
            background-image:url("sj6.jpg");
            background-repeat:no-repeat;
            background-position:50px 100px;
            box-shadow:5px 5px 20px red;
        }
```

### 12网页中常见问题

- css命名规范：

  参考链接<http://www.divcss5.com/jiqiao/j4.shtml#no2> 

- 项目目录规范：

  ![1559380169704](C:\Users\Xu jk\AppData\Roaming\Typora\typora-user-images\1559380169704.png)

- 确定错误位置
  
  - 假如错误影响了整体布局，则可以逐个删除div块，直到删除某个div块后显示恢复正常，即可确定错误发生的位置。这样我们可以更精准的找到错误点，进行排错。
- 是否重设了默认的样式?
  
  - 制作网页时，我们要清除掉默认的元素的padding和margin，使得我们更易去计算盒模型的大小。

### 13.显示方式

#### 1.行内元素水平居中显示

- 通过line-hinght + text-align

  ```
  <!DOCTYPE html>
  <html lang="en">
  <head>
      <meta charset="UTF-8">
      <title>Title</title>
      <style>
          p{
              width: 200px;
              height: 200px;
              background-color: #666;
              color:red;
              line-height: 200px;
              text-align:center;
          }
      </style>
  
  </head>
  <body>
      <p>
          帅气的阿凯哥
      </p>
  </body>
  </html>
  ```

  ![1559380463967](C:\Users\Xu jk\AppData\Roaming\Typora\typora-user-images\1559380463967.png)

- 通过给父元素设置display:table-cell,并且设置vertical-align:middle

  ```html
  <!DOCTYPE html>
  <html lang="en">
  <head>
      <meta charset="UTF-8">
      <title>Title</title>
      <style>
          span{
              width: 200px;
              height: 200px;
              background-color: #666;
              color:red;
              text-align:center;
              display: table-cell;
              vertical-align:middle;
          }
      </style>
  
  </head>
  <body>
      <span>
          <span>你好啊</span>
      </span>
  </body>
  </html>
  ```

  ![1559380796202](C:\Users\Xu jk\AppData\Roaming\Typora\typora-user-images\1559380796202.png)

#### 2.块级元素水平垂直居中

- 第一种：position+margin

  - position,子绝父相，margin自动设置外边距

  ```html
  <!DOCTYPE html>
  <html lang="en">
  <head>
      <meta charset="UTF-8">
      <title>Title</title>
      <style>
          .father{
              width: 200px;
              height: 200px;
              background-color: red;
              position: relative;
          }
          .child{
              position: absolute;
              width: 100px;
              height: 100px;
              background-color: blue;
              margin:auto;
              top:0;
              left:0;
              bottom:0;
              right:0;
          }
      </style>
  </head>
  <body>
      <div class="father">
          <div class="child">居中盒子</div>
      </div>
  </body>
  </html>
  ```

- 第二种：display:table-cell

  ```css
  <!DOCTYPE html>
  <html lang="en">
  <head>
      <meta charset="UTF-8">
      <title>Title</title>
      <style>
          .father{
              width: 200px;
              height: 200px;
              background-color: red;
              display:table-cell;
              vertical-align:middle;
              text-align:center;
          }
          .child{
              width: 100px;
              height: 100px;
              background-color: green;
              display:inline-block;
              vertical-align:middle;
          }
      </style>
  </head>
  <body>
      <div class="father">
          <div class="child">居中盒子</div>
      </div>
  </body>
  </html>
  ```

- 第三种：position

```css
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Title</title>
    <style type="text/css">
        .father{
            width: 200px;
            height: 200px;
            background-color: red;
            position: relative;
        }
        .child{
            width: 100px;
            height: 100px;
            background-color: green;
            position: absolute;
            left: 50%;
            top: 50%;
            margin-left: -50px;
        }
    </style>
</head>
<body>
    <div class="father">
        <div class="child">我是个居中的盒子</div>
    </div>
</body>
</html>
```

## 6.JavaScript

### 1.JavaScript引入

- 可以在html任意位置引入JavaScript

```html
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Title</title>

</head>
<body>
<!--行内js-->
	<p id="" class="" style="" onclick="console.log(2);">mjj</p>
<!--内嵌-->
<script type="text/javascript">

    //js代码


</script>
	<!--导入文件-->
	<script type="text/javascript" src="js/index.js"></script>

</body>
</html>
```

### 2.注释

- JavaScript对换行，缩进，空格不敏感。

- 单行注释（ctrl+/）

  ```javascript
  //我是单行注释
  ```

- 多行注释（ctrl+shift+/）

  ```javascript
  /*
  多行注释1
  多行注释2
  */
  ```

### 3.变量

#### 1.变量的定义：

- var是英语“variant”变量的缩写。后面要加一个空格，空格后面的东西就是“变量名”。
  - 定义变量：var就是一个**关键字**，用来定义变量。所谓关键字，就是有特殊功能的小词语。关键字后面一定要有空格隔开。
  - 变量的赋值：等号表示**赋值**，将等号右边的值，赋给左边的变量。
  - 变量名：我们可以给变量任意的取名字。

#### 2.变量的写法：

```javascript
var a = 100;
console.log(a);//输出100
```

#### 3.变量命名的规范：

- 变量名有命名规范：只能由英语字母、数字、下划线、美元符号$构成，且不能以数字开头，并且不能是JavaScript保留字，如下：

  ```
  abstract、boolean、byte、char、class、const、debugger、double、enum、export、extends、final、float、goto
  implements、import、int、interface、long、native、package、private、protected、public、short、static、super、synchronized、throws、transient、volatile
  ```

### 4.变量的类型

#### 1.数值型：number

```javascript
<script>
    var a= 100;
console.log(typeof a);
</script>
//number
```

- 通过typeof()  表示“获取变量的类型”，语法typeof变量

#### 2.字符串型：string

```javascript
    <script>
        var a = "abcde";
        var c= '1234';
        var d = '哈哈';
        var e = '';   //空字符串
        
        console.log(typeof a);//string
        console.log(typeof c);//string
        console.log(typeof d);//string
        console.log(typeof e);//string

    </script>
```

#### 3.Boolean布尔值

- 为JS中的Boolean的值也只有2中：true和false。他们通常被用于在适当的代码之后，测试条件是否成立，比如 `3 > 4`。这是个表达式，我们都知道3是不会大于4的，所以返回了false。

  ```javascript
  3>4;//false
  4>=3;//true
  ```

#### 4.字符串拼接

- 通过 + 拼接

  ```javascript
  <script>
  console.log('我'+"爱"+"吃"+"饭")
  </script>
  //我爱吃饭
  ```

- 通过`$`和· · （tab上面键拼接）

  ```javascript
      <script>
          var name = 'jk' , age = 20;
          var str = `${name}今年是${age}岁数了`;
          console.log(str);
      </script>
  //jk今年是20岁数了
  ```

#### 5.数学运算符

- Javascript有一套可用的全功能的数学功能，接下来我们就重点来学习Javascript中的数学运算吧。
- 算数运算符

| 运算符 | 名称                 | 作用                                                         | 示例                                  |
| ------ | -------------------- | ------------------------------------------------------------ | ------------------------------------- |
| `+`    | 加法                 | 两个数相加。                                                 | `6 + 9`                               |
| `-`    | 减法                 | 从左边减去右边的数。                                         | `20 - 15`                             |
| `*`    | 乘法                 | 两个数相乘。                                                 | `3 * 7`                               |
| `/`    | 除法                 | 用右边的数除左边的数                                         | `10 / 5`                              |
| `%`    | 求余(有时候也叫取模) | 在你将左边的数分成同右边数字相同的若干整数部分后，返回剩下的余数 | `8 % 3`(返回 2，将8分成3份，余下2 。) |

```javascript
5 + 4 * 3;
num % 8 *num;
```

- 运算符优先级

- Javascript中的运算符优先级与数学运算符优先级相同—乘法和除法总是先完成，然后再算加法和减法。

- 递增和递减运算

  - 增量（++）或递减（--）

  ```javascript
  var x = 3;
  x++;//相当于x = x + 1
  ```

  - 常见如下：

  | 运算符 | 名称     | 作用                                           | 示例             | 等价于              |
  | ------ | -------- | ---------------------------------------------- | ---------------- | ------------------- |
  | `+=`   | 递增赋值 | 右边的数值加上左边的变量，然后再返回新的变量。 | `x = 3;x += 4;`  | `x = 3;x = x + 4;`  |
  | `-=`   | 递减赋值 | 左边的变量减去右边的数值，然后再返回新的变量。 | `x = 6;x -= 3;`  | `x = 6;x = x - 3;`  |
  | `*=`   | 乘法赋值 | 左边的变量乘以右边的数值，然后再返回新的变量。 | `x = 2;x *= 3;`  | `x = 2;x = x * 3;`  |
  | `/=`   | 除法赋值 | 左边的变量除以右边的数值，然后再返回新的变量。 | `x = 10;x /= 5;` | `x = 10;x = x / 5;` |

- 小例题：

```javascript
//第一题：
<script>
        var a = 4;
    var c = a ++;
    console.log(c);//4
    console.log(a);//5
</script>
//先让a的值赋值给c,再对a++

//第二题：
<script>
    var a = 4;
    var c = ++a;
    console.log(c);//5
    console.log(a);//5
</script>
//想让a加法运算，再将a赋值给c

//第三题：
<script>
    var a = 2;
    var b = '2';
    console.log(a == b);//true
    console.log(a === b); //false
</script>
//== 比较的是值 console自动将a转换字符串进行比较，===比较是值和数据类型。
```

#### 6.数字和字符串之间的转换

- 数字转字符串

  ```javascript
  <script>
      var myNum = "" +234;
      console.log(typeof myNum);
  </script>
  //string
  ```

  ```javascript
  var myNum = 123;
  var myStr = myNum.toString();
  
  var num  = 1233.006;
  // 强制类型转换
  console.log(String(num));
  console.log(num.toString());
  // 隐式转换
  console.log(''.concat(num));
  // toFixed()方法会按照指定的小数位返回数值的字符串 四舍五入
  console.log(num.toFixed(2));
  ```

- 字符串转数字

  ```javascript
  <script>
      var myStr = '123';
      var myNum = Number(myStr);
      console.log(typeof myNum);
  </script>
  //number
  
  var str = '123.0000111';
  console.log(parseInt(str));//123
  console.log(typeof parseInt(str));//number
  console.log(parseFloat(str));//123.0000111
  console.log(typeof parseFloat(str));//number
  console.log(Number(str));//123.0000111
  ```

### 5.数组

#### 1.数组的创建

```javascript
var shop = ['apple','milk','banana','orange']
```

#### 2.访问和修改数组

```javascript
shop[0];//访问数组
shop[0] = 'pen'；//修改数组
```

#### 3.获取数组长度

```js
shop.length;
```

### 6.条件语句

#### 1.if...else语句

- 语法：

  ```javascript
  if (判断条件) {
  	//结果1
  }else{
      //结果2
  }
  ```

  - 关键字 if，并且后面跟随括号。
  - 测试的条件，放到括号里。这个条件会利用**比较运算符**进行比较，并且返回true或者false。
  - 一组花括号，在里面我们有一些代码——可以是任何我们喜欢的代码，并且只会在条件语句返回true的时候运行
  - 关键字else
  - 另一组花括号，在里面我们有一些代码——可以是任何我们喜欢的代码，并且当条件语句返回值不是true的话，它才会运行。

#### 2.else...if语句

- 上述例子提供我们两种选择或结果，但我们如果想要多个选择和结果可以用else if.

```javascript
var weather = 'sunny';
if(weather == 'sunny'){
    //天气非常棒，可以出去玩耍了
}else if(weather == 'rainy'){
    //天气下雨了，只能在家里呆着
}else if(weather == 'snowing'){
    //天气下雪了，可以出去滑雪了
}
```

#### 3.if ...else的嵌套

```javascript
var weather = 'sunny';
if(weather == 'sunny'){
    if(temperature > 30){
        //还是在家里吹空调吧
    }else if(temperature<=30){
        //天气非常棒，可以出去玩耍了
    }
}
```

#### 4.逻辑运算符：&&，||，！

- `&&` — 逻辑与; 使得并列两个或者更多的表达式成为可能，只有当这些表达式每一个都返回`true`时，整个表达式才会返回`true.`
- `||` — 逻辑或; 当两个或者更多表达式当中的任何一个返回 `true` 则整个表达式将会返回 `true`.
- ! — 逻辑非; 对一个布尔值取反, 非true返回false,非false返回true。

```javascript
var mathScore = 88;
var enlishScore = 90;
if(mathScore > 70 || enlishScore > 95){
    console.log('才可以玩游戏');
}else{
    console.log('在家里写作业');
}
```

#### 5.switch语句

```javascript
var weather = 'sunny';
switch(weather){
    case 'sunny':
        //天气非常棒，可以出去玩耍了
        break;
    case 'rainy':
       //天气下雨了，只能在家里呆着
        break;
    case 'snowing':
        //天气下雪了，可以出去滑雪了
        break;
    default:
        //哪里也不出去
}
```

### 7.三元运算符

- 三元运算符它解决了像**if..else**块较少的代码。如果你只有两个通过true/false条件选择。

```javascript
(codition) ? run this code : run this code instead;
```

```javascript
<script>
    var a = '';
    (1 > 2)? a='真的' : a='假的';
    console.log(a)

</script>
//假的
```

### 8.循环

#### 1.for循环

- Javascript的循环有两种，一种是for循环，通过**初始化条件**、**结束条件**和**递增条件**来循环执行语句块，语法如下：

```javascript
for (初始化条件;结束条件;递增条件){
	run this code
}
```

- 示例：

  ```javascript
      <script>
          var total = 0;
          var i;
          for (i = 1;i <= 1000; i++){
              total = total + i;
          }
          console.log(total);//500500
      </script>
  ```

#### 2.使用Break退出循环

- for循环的3个条件都是可以省略的，如果没有退出循环的判断条件，就必须使用break语句退出循环，否则就是死循环：

  ```javascript
      <script>
          var x = 0;
          for (;;){
              if (x>100) {
                  break;
              }
              x ++;
              console.log(x)
          }
      </script>
  //1.....101
  ```

#### 3.continue语句

- continue语句中断循环中的迭代,如果出现了指定的条件,然后继续循环中的下一个迭代。

  ```javascript
      <script>
          var x;
          for (var i = 0; i<=10; i++){
              if (i==3) continue;
              x ='这个数字是' + i;
              console.log(x)
          }
      </script>
  //这个数字是0......这个数字是10
  ```

#### 4.while循环

- Javascript中的另一种循环则是while循环，使用它最佳。

- 语法：

  ```javascript
  while (判断循环结束条件){
  	//code to run
  	递增条件
  }
  ```

- 示例：

  ```javascript
  <script>
      var a = 1;
      while (a <=100){
          console.log(a);
          a += 1;
      }
  </script>
  //1 2 3 .....100
  ```

#### 5.do...while

- 最后一种循环是`do { ... } while()`循环，它和`while`循环的唯一区别在于，不是在每次循环开始的时候判断条件，而是在每次循环完成的时候判断条件：

  ```javascript
  var n = 0;
  do {
      n = n + 1;
  } while (n < 100);
  n; // 100
  ```

- 用`do { ... } while()`循环要小心，循环体会至少执行1次，而`for`和`while`循环则可能一次都不执行。

### 9.函数

- 在Javascript中另一个基本概念是函数，它允许你在一个代码块中存储一段用于处理一个任务的代码，然后在任何你需要的时候用一个简短的命令来调用，而不是把相同的代码写很多次。

#### 1.函数定义

- 一个**函数定义**(也称为**函数声明**，或**函数语句**)由一些列的`function`关键字组成。例如，以下代码是一个简单的计算多组数的和的函数，我们给它命名为`sum`。

  ```javascript
  function sum(a,b) {
  	//函数体
  }
  ```

#### 2.函数的调用

- 函数定义好后，它是不能自动执行的，需要调用它，直接在需要的位置写函数名。

  ```javascript
  <script>
      function sum(a,b){
          console.log(a+b);
      }
      sum(3,5);
  </script>
  ```

- 我们把定义函数时传入的两个参数a和b叫**形参**(形式参数，一个将来被代替的参数)，调用函数时传入的两个参数叫**实参**(实际的参数)。

- 如果向在定义函数时添加多个形参。可以在后面添加多个，**注意参数之间用逗号隔开**。

#### 3.函数的返回值

- 我们在计算两个数之和的时候，可以给函数一个返回值，每次调用该函数，你就能计算出两个数之和。

  ```javascript
  <script>
      function sum(a,b){
          return a+b
      }
      var result = sum(3,5);
      console.log(result);
  </script>
  ```

#### 4.函数表达式

- 虽然上面的函数定义在语法上是一个语句，但函数可以由函数表达式创建。这样的函数可以是匿名的;它不必有一个名称。例如，函数sum也可以这样去定义：

  ```javascript
  var sum = function(a,b){
      return a + b;
  }
  sum(4,5);
  ```

#### 5.函数作用域冲突

- 处理函数时，**作用域**是非常重要的一个概念。当你创建函数时，函数内定义的变量和其它东西都在它们自己的单独的范围内，意味着它们被锁在自己独立的房间内。

- 所有的函数的最外层被称为**全局作用域**。在全局作用域内定义的值可以任意地方访问。

  ```javascript
  <!-- my HTML file -->
  <script src="first.js"></script>
  <script src="second.js"></script>
  <script>
    hello();
  </script>
  ```

  ```javascript
  //first.js
  var name = 'Tom';
  function hello() {
    alert('Hello ' + name);
  }
  ```

  ```javascript
  //second.js
  var name = 'Jack';
  function hello() {
    alert('Hello ' + name);
  }
  ```

  - 这两个函数都是用`hello()`形式调用，但是你只能访问到 `second.js`文件的`hello()`函数。`second.js` 在源代码中后应用到HTML中，所以它的变量和函数覆盖了 `first.js` 中的。我们称为这种行为叫**代码冲突**。

- 例题：

  ```javascript
      function fn() {
          switch (arguments.length){
              case 2:
                  console.log('2个参数');
                  break;
              case 3:
                  console.log('3个参数');
                  console.log(arguments);//Arguments(3) [2, 3, 4, callee: ƒ, Symbol(Symbol.iterator): ƒ]
                  break;
              default:
                  break;
          }
      }
      fn(2,3);//2个参数
      fn(2,3,4);//3个参数
  //arguments对象在函数中引用函数的参数。此对象包含传递给函数的每个参数.
  //arguments对象不是一个 Array 。它类似于Array，但除了length属性和索引元素之外没有任何Array属性。
  ```

#### 6.函数内部的函数

- 在任何地方你都可以调用函数，甚至可以在另一个函数中调用函数。这通常被用作保持代码整洁的方式。如果你有一个复杂的函数，如果将其分解成几个子函数，它更容易理解：

```javascript
function myBigFunction() {
  var myValue = 1;
  subFunction1(myValue);
  subFunction2(myValue);
  subFunction3(myValue);
}
function subFunction1(value) {
  console.log(value);
}
function subFunction2(value) {
  console.log(value);
}
function subFunction3(value) {
  console.log(value);
}
```

- 函数自执行

  ```
  （function(){
            //代码
          })();
  ```

### 10.对象（object）:

- 对象属性：它是属于这个对象的某个变量。比如字符串的长度、数组的长度和索引、图像的宽高等。

- 对象的方法：只有某个特定属性才能调用的函数。表单的提交、时间的获取等。

- 对象的创建方式：

  ##### 1.字面量表示法

  - 对象字面量是对象定义的一种简写形式，目的在于简化创建包含大量属性的对象的过程。

    ```javascript
    <script>
        var person = {
            name : 'xjk',
            age : 22,
            fav :function() {
                console.log('学JS')
            }
    
        };
        person.fav();//学JS
        console.log(person.name);//22
    </script>
    ```

    ```javascript
    var person = {};//与new Object()相同
    
    var obj2 = new Object();
    ```

  ##### 2.点语法(set和get)

  ```javascript
  <script>
      var person = {};
      person.name='xjk';
      person.fav = function(){
          console.log(this)
      };
      person.fav();//{name: "xjk", fav: ƒ}
      console.log(person.name);//xjk
  </script>
  ```

  ```javascript
  //set get  用法：
  //1、get与set是方法，因为是方法，所以可以进行判断。
  //2、get是得到 一般是要返回的   set 是设置 不用返回
  //3、如果调用对象内部的属性约定的命名方式是_age   
      var my = {
          name:'jk',
          work:function () {
              console.log('working');
          },
          _age:22,
          get age(){
              return this._age;
          },
          set age(val){
              if (val<0 || val>100) {
                  throw new Error('invalid value')
              }else{
                  this._age = val;
              }
          }
      };
      console.log(my.age);//22
      my.age = 18;
      console.log(my.age);//18
      my.age = 200;
      console.log(my.age);//Uncaught Error: invalid value
  ```

  

  ##### 3.括号表示法

  ```
  person['name'];
  ```

- ECMAScript6：

  ```javascript
  <script>
      class Person{
          constructor(name,age){
              //初始化相当于python__init__,this相当于self
              this.name = name;
              this.age = age;
          }
          fav(){
              console.log(this.name);
          }
      }
      var p = new Person('mjj',18);
      p.fav();
  </script>
  //与python相似
  ```

- 例题：

  ```javascript
      var obj = {};
      obj.name = 'mjj';
      obj.fav = function(){
          //obj
          console.log(this);
      };
      console.log(this);
  	//Window {postMessage: ƒ, blur: ƒ, focus: ƒ, close: ƒ, parent: Window, …}全局的。
      function  add(x,y) {
          console.log(this.name);
          console.log(x);
          console.log(y);
      }
      
      add.call(obj,1,2);
  	//obj传入add 函数，给this赋予局部函数，为obj.name，调用obj对象的name并打印。
  
  	add.apply(obj,[1,2]);
  	//apply 与call相似，第二个参数传入必须为数组
  
  
  
  //匿名函数
      (function () {
          console.log(this);
      })();
  //window
  ```

- 注意：首先要了解JS里的this对象，this对象是在运行时基于函数的执行环境绑定的，在全局函数中，this等于window，而当函数作为某个对象例如A的方法B被调用时，this等于A对象，这里有个例外，匿名函数的执行环境具有全局性，因此匿名函数的this对象通常指向window。用一句话总结，就是谁调用this，谁就被引用。

  ```javascript
  <script>
      var name = 'wusir';
      (function () {
          console.log(this.name);
      })();
  
  </script>
  //wusir
  ```

- 练习题：

  ```javascript
  <script>
      var name = 'window';
      var obj = {
          name:'my obj',
          getname:function () {
              var name = 'inner name';
              return this.name;
          }
      };
      alert(obj.getname());
  </script>
  //my obj
  //谁调用this，谁就被引用。
  ```

- apply:方法能劫持另外一个对象的方法，继承另外一个对象的属性.   Function.apply(obj,args)方法能接收两个参数      obj：这个对象将代替Function类里this对象      args：这个是数组，它将作为参数传给Function（args-->arguments）call:和apply的意思一样,只不过是参数列表不一样.   Function.call(obj,[param1[,param2[,…[,paramN]]]])      obj：这个对象将代替Function类里this对象

### 11.内置对象：

- 它提供了一些列预先定义好的对象，而我们可以把这些对象直接用在自己的脚本里。我们称这种对象叫**内置对象（navtive object）。**

#### 1.Array

- 其实我们已经见过一些Javascript内置对象了。数组就是一种Javascript内置对象中的一种。本节我们主要介绍数组常用的**属性**和**方法**。

##### 1.1数组创建：

```javascript
var colors = new Array();
```

##### 1.2使用字面量方式创建数组

```javascript
var colors = [];
```

##### 1.3  isArray

- 判断是否为数组

```javascript
<script>
    var  colors = ['red','green','blue'];
    if (Array.isArray(colors)){
        console.log("是数组")
    }
</script>
//是数组
```

##### 1.4 toString

- 数组中每个值得字符串形式拼接。

  ```javascript
  <script>
      var  colors = ['red','green','blue'];
      alert(colors.toString());
  </script>
  //red,green,blue
  ```

##### 1.5 join

- 分割字符串

  ```javascript
  <script>
      var  colors = ['red','green','blue'];
      res = colors.join("|");
      console.log(res);
  </script>
  //red|green|blue
  ```

##### 1.6栈方法push()   pop()

- 数组也可以像栈一样，既可以插入和删除项的数据结构。栈是一种LIFO(Last-In-First-Out,后进先出)的数据结构，也就是最新添加的那项元素最早被删除。而栈中项的插入（叫做**推入**）和移除(叫做**弹出**)，只发生在一个位置——栈的顶部。

  - push():可以接收任意数量的参数，把它们逐个添加到数组末尾，并返回修改后数组的长度。
  - pop():从数组末尾移除最后一项，减少数组的 length 值，然后返回移除的项 。

  ```javascript
   <script>
      var  colors = ['red','green','blue'];
      colors.push('yellow','black');
      console.log(colors);
  </script>
  //["red", "green", "blue", "yellow", "black"]
  ```

  ```javascript
  <script>
      var  colors = ['red','green','blue'];
      res = colors.pop();//res = 'blue'
      console.log(colors);
  </script>
  //["red", "green"]
  ```

##### 1.7队列方法 shift()  unshift()

- 队列数据结构的访问规则是 `FIFO(First-In-First-Out， 先进先出)`。队列在列表的末端添加项，从列表的前端移除项。

- shift()

  ```javascript
  <script>
      var  colors = ['red','green','blue'];
      var item = colors.shift();
      alert(item);
  </script>
  //red
  ```

- unshift()

  ```javascript
  var colors = [];
  var count  = colors.unshift('red','green'); //推入两项
  alert(count); //2
  console.log(colors); // ["red", "green"]
  ```

##### 1.8reverse()

- 反转数组。

  ```javascript
  var values = [1,2,3,4];
  values.reverse();
  alert(values);
  //4,3,2,1
  ```

##### 1.9sort()

- 方法按升序排列

  - 默认情况下，`sort()`方法按升序排列——即最小的值位于最前面，最大的值排在最后面。 为了实现排序，`sort()`方法会调用每个数组项的`toString()`转型方法，然后比较得到的字符串，以确定如何排序 。即使数组中的每一项都是数值，sort()方法比较的也是字符串。

  ```javascript
  var values = [0,1,5,10,15];
  varlus.sort();
  alert(values); //0,1,10,15,5
  //可见，即使例子中值的顺序没有问题，但 sort()方法也会根据测试字符串的结果改变原来的顺序。 因为数值 5 虽然小于 10，但在进行字符串比较时，”10”则位于”5”的前面，于是数组的顺序就被修改了。 不用说，这种排序方式在很多情况下都不是最佳方案。
  ```

##### 1.10 concat方法

- 数组合并方法，一个数组调用concat()方法去合并另一个数组，返回一个新的数组。concat()接收的参数是可以是任意的。
  - 参数为一个或多个数组，则该方法会将这些数组中每一项都添加到结果数组中。
  - 参数不是数组，这些值就会被简单地添加到结果数组的末尾

```javascript
var colors = ['red','blue','green'];
colors.concat('yello');//["red", "blue", "green", "yello"]
colors.concat({'name':'张三'});//["red", "blue", "green", {…}]
colors.concat({'name':'李四'},['black','brown']);// ["red", "blue", "green", {…}, "black", "brown"]
```

##### 1.11  slice()方法

- `slice()`方法，它能够基于当前数组中一个或多个项创建一个新数组。`slice()`方法可以接受一或两个参数，既要返回项的起始和结束位置。
  - 一个参数的情况下，slice()方法会返回从该参数指定位置开始到当前数组默认的所有项
  - 两个参数的情况下，该方法返回起始和结束位置之间的项——但不包括结束位置的项。

```javascript
var colors = ['red','blue','green','yellow','purple'];
colors.slice(-2,-1);//["yellow"] 
colors.slice(-1,-2);//[]
//解析colors.slice(-2,-1) 等于colors.slice(3,4)，都加colors.length.此时为5.
```

##### 1.12splice

- 用于数组删除，插入，替换。

  ```javascript
  //1.删除：可以删除任意数量的项，只需指定2个参数：要删除的第一项的位置和要删除的个数。例如splice(0,2)会删除数组中的前两项。
  <script>
      var colors = ['red','blue','green','yellow','purple'];
      colors.splice(0,2);
      console.log(colors);//["green", "yellow", "purple"]
  </script>
  //2.插入：可以向指定位置插入任意数量的项，只需提供3个参数：起始位置、0（要删除的个数）和要插入的项。如果要插入多个项，可以再传入第四、第五、以至任意多个项。例如，splice(2,0,'red','green')会从当前数组的位置2开始插入字符串'red'和'green'。
  <script>
      var colors = ['red','blue','green','yellow','purple'];
      colors.splice(2,0,'red','green');
      console.log(colors);//["red", "blue", "red", "green", "green", "yellow", "purple"]
  </script>
  //3.替换：可以向指定位置插入任意数量的项，且同时删除任意数量的项，只需指定 3 个参数:起始位置、要删除的项数和要插入的任意数量的项。插入的项数不必与删除的项数相等。例如，splice (2,1,"red","green")会删除当前数组位置 2 的项，然后再从位置 2 开始插入字符串"red"和"green"。
  <script>
      var colors = ['red','blue','green','yellow','purple'];
      colors.splice(2,1,'red','green');
      console.log(colors);//["red", "blue", "red", "green", "yellow", "purple"]
  </script>
  ```

##### 1.13 indexOf()  lastIndexOf()位置方法

- `indexOf()`和 `lastIndexOf()`。这两个方法都接收两个参数:要查找的项和(可选的)表示查找起点位置的索引。其中，**indexOf()方法从数组的开头(位置 0)开始向后查找，lastIndexOf()方法则从数组的末尾开始向前查找**。在没找到的情况下返回-1.

  ```javascript
  <script>
      var num = [1,2,3,4,5,4,3,2,1];
      alert(num.indexOf(4));//3 查找值为4的索引为3
  </script>
  
  <script>
      var num = [1,2,3,4,5,4,3,2,1];
      // alert(num.indexOf(4));//3 查找值为4的索引为3
      alert(num.lastIndexOf(4));//5 从后开始查找值为4的 索引为5（索引还是从前面开始算）
  </script>
  
  <script>
      var num = [1,2,3,4,5,4,3,2,1];
      alert(num.indexOf(4,4));//5 第一个参数要查找的值4，第二个参数为规定在字符串中开始检索的位置,从4好位置开始查找
      // alert(num.lastIndexOf(4));//5 从后开始查找值为4的 索引为5（索引还是从前面开始算）
  </script>
  
  
  ```

##### 1.14迭代方法  filter  map   forEach

- `filter()`函数，它利用指定的函数确定是否在返回的数组中包含某一项。

  ```javascript
  var numbers = [1,2,3,4,5,4,3,2,1];
  var filterResult = numbers.filter(function(item, index, array){
      return (item > 2);
  });
  alert(filterResult); //[3,4,5,4,3]
  ```

- map方法

  - 方法也返回一个数组，而这个数组的每一项都是在原始数组中的对应项上运行输入函数的结果。

  ```javascript
  var numbers = [1,2,3,4,5,4,3,2,1];
  var filterResult = numbers.map(function(item, index, array){
      return item * 2;
  });
  alert(filterResult); //[2,4,6,8,10,8,6,4,2]
  ```

- forEach()方法

  - 它只是对数组中的每一项运行传入的函数。这个方法没有返回值， 本质上与使用 for 循环迭代数组一样。

  ```javascript
  <script>
      var num = [1,2,3,4,5,4,3,2,1];
      var res = num.forEach(function(item,index,array){
          console.log(index,item);
  
      });
  </script>
  /*
  第一个索引index，第二个值item
  0 1
  1 2
  2 3
  3 4
  4 5
  5 4
  6 3
  7 2 
  8 1
  */
  ```

#### 2.String

##### 2.1 length方法

- 获取字符串长度。

```javascript
var stringValue = "hello world"; 
alert(stringValue.length);     //"11"
```

##### 2.2charAt()  charCodeAt()字符方法

- 这两个方法都接收一个 参数，即基于 0 的字符位置。其中，`charAt()`方法以单字符字符串的形式返回给定位置的那个字符。

  ```javascript
  <script>
      var stringValue = "hello world";
      alert(stringValue.charAt(1));//'e'
  </script>
  
  var stringValue = "hello world";
  alert(stringValue.charCodeAt(1)); //输出"101" 当前字符编码
  ```

##### 2.3 concat()字符串操作方法

- 用于将一或多个字符串拼接起来， 返回拼接得到的新字符串.concat可以接收任意多个参数。

  ```javascript
  <script>
      var stringValue = "hello ";
      var result = stringValue.concat("world",'!');
      alert(result);//hello world!
  </script>
  
  ```

##### 2.4子字符串创建新字符串

- slice()   substr()  substring()

- 与 concat()方法一样，slice()、substr()和 substring()也不会修改字符串本身的值——它们只是 返回一个基本类型的字符串值，对原始字符串没有任何影响。

  - slice()、 substring() 第二个参数指定的是字符串最后一个字符位置。
  - substr()第二个参数指定则是返回字符个数。

  ```javascript
  var stringValue = "hello world";
  alert(stringValue.slice(3));//"lo world"
  alert(stringValue.substring(3));//"lo world"
  alert(stringValue.substr(3));//"lo world"
  alert(stringValue.slice(3, 7));//"lo w"
  alert(stringValue.substring(3,7));//"lo w"
  alert(stringValue.substr(3, 7));//"lo worl"
  ```

##### 2.5indexOf()  lastIndexOf()字符串位置方法

- 有两个可以从字符串中查找子字符串的方法:`indexOf()`和 `lastIndexOf()`。这两个方法都是从 一个字符串中搜索给定的子字符串，然后返回子字符串的位置(如果没有找到该子字符串，则返回-1)。

  ```javascript
  var stringValue = "hello world";
  alert(stringValue.indexOf("o"));             //4
  alert(stringValue.lastIndexOf("o"));         //7
  alert(stringValue.indexOf("o", 6));         //7
  alert(stringValue.lastIndexOf("o", 6));     //4
  ```

##### 2.6trim()方法

- trim()方法,删除字符串的前后空格.

  ```javascript
  var stringValue = "   hello world   ";
  var trimmedStringValue = stringValue.trim();
  alert(stringValue);            //"   hello world   "
  alert(trimmedStringValue);     //"hello world"
  ```

##### 2.7字符串大小写转换

```javascript
var stringValue = "hello world";
alert(stringValue.toLocaleUpperCase());  //"HELLO WORLD"
alert(stringValue.toUpperCase());        //"HELLO WORLD"
alert(stringValue.toLocaleLowerCase());  //"hello world"
alert(stringValue.toLowerCase());        //"hello world"
```

### 12.date对象方法

![1559576872761](C:\Users\Xu jk\AppData\Roaming\Typora\typora-user-images\1559576872761.png)

```javascript
var date = new Date();
console.log(date);
//Mon Jun 03 2019 23:51:28 GMT+0800 (中国标准时间)
console.log(date.getDate());
//3 根据本地时间返回本月第几天
console.log(date.getMonth()+1);
//6 根据本地时间返回第几月
console.log(date.getFullYear());
//2019  返回年
console.log(date.getDay());
// 星期几
console.log(date.getHours());
//几时
console.log(date.getMinutes());
//几分
console.log(date.getSeconds());
//几秒
console.log(date.toLocaleString());
//2019/6/3 下午11:56:00
```

```javascript
var weeks = ['星期天','星期一','星期二','星期三','星期四','星期五','星期六'];
var date = new Date();
console.log(weeks[date.getDay()]);
var day = weeks[date.getDay()];
document.write(`<a href="#">${day}</a>`);
```

- 时钟案例

  ```javascript
  <h2 id="time"></h2>
  <script>
      var timeObj = document.getElementById('time');
      console.log(time);
  
      function getNowTime() {
          var time = new Date();
          var hour = time.getHours();
          var minute = time.getMinutes();
          var second = time.getSeconds();
          var temp = "" + ((hour > 12) ? hour - 12 : hour);
          if (hour == 0) {
              temp = "12";
          }
          temp += ((minute < 10) ? ":0" : ":") + minute;
          temp += ((second < 10) ? ":0" : ":") + second;
          temp += (hour >= 12) ? " P.M." : " A.M.";
          timeObj.innerText = temp;
      }
  
      setInterval(getNowTime, 20)
  </script>
  ```

### 13.window对象

- ECMAScript 虽然没有指出如何直接访问 Global 对象，但 Web 浏览器都是将这个全局对象作为 window 对象的一部分加以实现的。因此，在全局作用域中声明的所有变量和函数，就都成为了 window 对象的属性。来看下面的例子。

```javascript
var color = "red";
function sayColor(){
    alert(window.color);
}
window.sayColor();  //"red"
```

### 14.Math对象

#### 13.1max  min  最小值  最大值

```javascript
var max = Math.max(3,54,32,16);
alert(max);//54
var min = Math.min(3,54,32,16);
alert(min);//3
```

```javascript
var values = [1,2,36,23,43,3,41];
var max = Math.max.apply(null, values);
console.log(max);
```

#### 13.2舍入方法

- Math.ceil():执行向上舍入，即它总是将数值向上舍入为最接近的整数;
- Math.floor():执行向下舍入，即它总是将数值向下舍入为最接近的整数;
- Math.round():执行标准舍入，即它总是将数值四舍五入为最接近的整数(这也是我们在数学课上学到的舍入规则)。

```javascript
<script>
    var a = 1.523;
    console.log(Math.ceil(a));//2
    console.log(Math.floor(a));//1
    console.log(Math.round(a));//2
</script>
```

#### 13.3random()随机数

- 方法返回大于等于 0 小于 1 的一个随机数

- 生成100~400随机整数

  ```javascript
  <script>
      function random(min,max){
          return min +Math.floor(Math.random()*(max-min));
      }
      var res = random(100,400);
      console.log(res);
  
  </script>
  ```

### 15.小练习

#### ECMAScript基础语法

#### 1.基本数据类型和引用数据类型

```
基本数据类型number,string,boolean,undefined,null
引用数据类型：Array,Object,Function
```

#### 2.条件判断和循环

```
switch(name){
	case 'xxx';
		break;
	case 'xxx';
		break;
	default:
		break;
}

for (var i=0;i<10;i++){
	run to code
}
三元运算：
1>3? '真的':'假的';
```

#### 3.赋值运算符，逻辑运算符

```
&& || !
i ++
==比较值 ===比较值和数据类型
```

#### 4.字符串的常用方法

```javascript
slice() 切片 一个参数从当前位置切到最后，两个参数顾头不顾尾
substring()
substr()  如果有两个参数，第二个参数表示切字符串的个数
join()

字符串拼接
concat()
+
es6的模版字符串
	··插入变量用${变量名}

//获取索引
indexOf()
lastIndexOf()
//获取字符
charAt()
//获取字符ascii码
charCodeAt()

//转大写
toUppercase()
//转小写
tolowercase()

typeOf 校验当前变量的数据类型
trim 清除左右的空格
```

#### 5.数组的常用方法

```javascript
toString()
join('#')
concat()
//栈
push()
pop()
//堆
unshift()
shift()

splice(起始位置，删除的个数，添加的元素);
//增加，删除，替换
reverse()
sort()
slice()
length

//迭代方法
forEach() //仅能在数组对象中使用。
for
    
在函数中arguments  这个对象是伪数组
```

6.对象

```
function fn(name){
	var obj = {};
	obj[name]='mjj';
	return obj;
}
fn('age')
//遍历对象
for (var key in obj) {
	obj[key]
}
```

#### 7.函数

```javascript
1.创建方法
//普通函数
function fn(){

}
fn();
//函数表达式
var fn = function(){
	run to code
}
//匿名函数
(function(){})()//this指向一定指向window

全局作用域，函数作用域，函数作用域中this指向可以发生改变，可以使用call() 或者apply()
e.g.:
var obj = {name:'mjj'};
function fn(){
    console.log(this.name);
}
fn();//是空制，因为函数内容this指向window
fn.call(obj);//此时函数内部的this指向obj


构造函数
new object();
new Array();
new String();


父级定义一个属性必须使用prototype
```

#### 8.日期对象

```javascript
var data = new Data();

date.toLocaleString()  //2018/08/21 21:33:23
```

#### 9.Math数学对象

```javascript
Math.ceil(); 向上取整,天花板函数
Math.floor(); 向下取整，地板函数
Math.round(); 标准的四舍五入

随机数
Math.random();获取到0-1之间的数

function random(min,max){
    Math.floor(Math.random()*(max-min))+min;
}
```

#### 10.数值和字符串转换

```javascript
1.数值转字符串
toString()
数字+空字符串
2.字符串转数值
parseInt() 转整数
parseFloat() 转浮点型
Number()

NAN
isNaN()

Infinity 无限大
```

#### 11.let    var 区别

```
1.ES6可以用let定义块级作用域变量
2.let 配合for循环的独特应用
3.let没有变量提升与暂时性死区
4.let变量不能重复声明
```

####  12.看代码写结果

```javascript
var a = 99;            // 全局变量a
f();                   // f是函数，虽然定义在调用的后面，但是函数声明会提升到作用域的顶部。 
console.log(a);        // a=>99,  此时是全局变量的a
function f() {
  console.log(a);      
  var a = 10;
  console.log(a);      // a => 10
}
//undefined
//10
//99
```

### 16 BOM（浏览器对象模型）

- BOM的核心对象是Window,它表示浏览器的一个实例，在浏览器中，window对象有双重角色，即是JavaScript访问浏览器窗口的一个接口。

  又是ECMAScript规定Global对象。

#### 1.系统对话框方法

##### 1.alert() 警告框

```javascript
<body>
    <script type="text/javascript">
        window.alert('mjj');
    </script>
</body>
```

- 效果显示：

![1559630962449](C:\Users\Xu jk\AppData\Roaming\Typora\typora-user-images\1559630962449.png)

- 例题：

  ```javascript
  //例题：
  <script>
      console.log(window.location);
      var i=1;
      function f(){
          i++;
          console.log("打印 :  "+i);
          return k;
          function k() {
              return "I love you!"
          }
      }
      f();
      console.log("111111111111");
      f()();
      alert(f()());
  </script>
  //打印2
  //11111111111
  //打印3
  //打印4
  //警告框出现"I love you!"
  ```

##### 2.confirm()确认框

- 如果点击确定,返回`true`,点击取消,返回`false`。

```javascript
<body>
    <script type="text/javascript">
        var src = window.prompt("今天天气如何");
        switch (src) {
            case '晴天':
                console.log('出去打球');
                break;
            case '阴天':
                console.log('在家睡觉');
                break;
            default:
                console.log("什么都不做");
                break
        }
    </script>
</body>
```

- 效果显示：

![1559631345509](C:\Users\Xu jk\AppData\Roaming\Typora\typora-user-images\1559631345509.png)

##### 3.prompt()弹出框

- prompt默认接收两个参数，第一个参数显示文本，第二个参数默认的文本。

```javascript
<body>
    <script type="text/javascript">
        var src = window.prompt("今天天气如何",'晴天');
        switch (src) {
            case '晴天':
                console.log('出去打球');
                break;
            case '阴天':
                console.log('在家睡觉');
                break;
            default:
                console.log("什么都不做");
                break
        }
    </script>
</body>
```

- 效果显示

![1559631174563](C:\Users\Xu jk\AppData\Roaming\Typora\typora-user-images\1559631174563.png)

#### 2.定时方法**********

##### 1.一次性任务setTimeout()

- setTimeout()方法表示一次性定时任务做某件事情,它接收两个参数,第一个参数为执行的函数,第二个参数为时间(毫秒计时:1000毫秒==1秒)

```javascript
    <script type="text/javascript">
        window.setTimeout(function(){
            console.log('2秒后我将打印');
        },2000);
        console.log('立刻打印');
    </script>
//setTimeout(参数1，参数2);参数1为一个函数，参数2为时间以毫秒为单位。此处要知道setTimeout是异步。
```

![1559631768373](C:\Users\Xu jk\AppData\Roaming\Typora\typora-user-images\1559631768373.png)

- 一次性任务的清除

  ```javascript
  var timer = setTimeout(callback,1000);
  clearTimeout(timer);
  //清除定时器。
  ```

- href属性

  ```javascript
  //2秒后跳转 百度网页
  setTimeout(function () {
  	location.href = 'https://www.baidu.com';
  },2000);
  ```

- replace

  ```javascript
  //2秒后跳转 百度网页,在当前页面跳转
  setTimeout(function(){
      location.replace('https://www.baidu.com');
  },2000)
  ```

##### 2.周期性循环setInterval

- `setInterval()`方法表示周期性循环的定时任务.它接收的参数跟`setTimeout()`方法一样.

```javascript
<script>
    var num = 0;
	var timer = null;
	//开启定时器
	timer = window.setInterval(function(){
        num++;
        if (num===10){
            //当num===10  清除定时器
            clearInterval(timer);
        }
        console.log(num);//console打印出数字
    },1000);//1000ms=1s
</script>

```



```javascript
//每2秒刷新。
setInterval(function(){
    //做测试
    location.reload();
},2000);

//局部刷新  ajax技术，在不重载页面上，对网页进行操作
```



##### 3.locaion对象属性

- location是最有用的BOM对象之一,它提供了与当前窗口中加载的文档有关的信息，还提供了一 些导航功能。事实上，location 对象是很特别的一个对象，因为它既是 window 对象的属性，也是document 对象的属性;

```javascript
    <script>
        console.log(window.location);//获取属性
        setInterval(function(){
            location.reload();//刷新页面
        },2000);
    </script>
//局限较大，通常都是局部刷新，通过AJAX技术，在不重载页面的情况，对网页进行操作
```

- reload()方法

  ```javascript
  location.reload();//重新加载（有可能从缓存中加载）
  location.reload(true;)//重新加载（强制从服务器加载）
  ```

- history

  ```javascript
  history.go(-1);//后退一页
  history.go(1);//前进一页
  history.go(2);//前进两页
  ```

```javascript
console.log(window.location);
//获取属性
```

- 显示效果

![1559615554263](C:\Users\Xu jk\AppData\Roaming\Typora\typora-user-images\1559615554263.png)

### 2.DOM（文档对象模型）

- 顶层对象BOM---->文档------>文档内容

#### 2.1节点

- ##### 元素节点

  - 像`<body>`、`<p>`、`<ul>`之类的元素这些我们都称为叫元素节点（element node）。

- ##### 文本节点

  - 元素只是不同节点类型中的一种。如果一份文档完全由一些空白元素构成，它将有一个结构，但这份文档本身将不会包含什么内容。在网上，内容决定着一切，**没有内容的文档是没有任何价值的，而绝大数内容都是有文本提供**。

    ```javascript
    //示例：
    <p>这是一个文本</p>
    //‘这是一个文本’ 是一个文本节点
    ```

- ##### 属性节点

  - 还存在着其他的一些节点类型。例如，注释就是另外一种节点类型。但这里我们介绍的是属性节点。

    ```javascript
    //示例
    <p title='book'>这是一本书！</p>
    //title='book'是一个属性节点
    ```

#### 2.3获取节点对象的三种方式

##### 2.3.1  document.getElementById 方法

- getElementById 的方法只有一个参数，用于获得元素的id属性值，这个id值必须为字符串类型。

```javascript
<body>
    <div id="box">MJJ</div>
    <ul id="box2">
        <li class="active"></li>
        <li></li>
        <li></li>
    </ul>
        <script>
        var box = document.getElementById('box');
        console.log(box);
        console.dir(box);
    </script>
<body>
//
```

![1559633502634](C:\Users\Xu jk\AppData\Roaming\Typora\typora-user-images\1559633502634.png)

##### 2.3.2document.getElementsByTagName()方法

- 通过标签名字获取节点对象（获取是伪数组），这个方法将返回一个元素对象集合。

```javascript
//示例1：
<body>
    <div id="box">MJJ</div>
    <ul id="box2">
        <li class="active"></li>
        <li></li>
        <li></li>
    </ul>	
    <script>
        var box = document.getElementsByTagName('div');
        console.log(box);//HTMLCollection [div#box, box: div#box]   
    </script>
</body>
```



```javascript
//示例2
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Title</title>
	//css
    <style>
        ul li.active{
            color:red;
        }
    </style>
</head>
<body>
     //html
    <div id="box">MJJ</div>
    <ul id="box2">
        <li>1</li>
        <li>2</li>
        <li>3</li>
    </ul>
//JS:
    <script>
        var lis = document.getElementsByTagName('li');
        //三个li签
        console.log(lis);//HTMLCollection(3) [li, li, li]
        for (var i=0;i<lis.length;i++){//循环将li标签添加class 属性，让CSS渲染
            lis[i].className = 'active';
        }

    </script>
</body>
</html>

//示例3：
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Title</title>
	//css
    <style>
        ul li.active{
            color:red;
        }
    </style>
</head>
<body>
     //html
    <div id="box">MJJ</div>
    <ul id="box2">
        <li>1</li>
        <li>2</li>
        <li>3</li>
    </ul>
//JS:
    <script>
        var lis = document.getElementsByTagName('li');
        //三个li签
        console.log(lis);//HTMLCollection(3) [li, li, li]
        for (var i=0;i<lis.length;i++){//循环
            lis[i].onclick = function(){
                this.className = 'active';//鼠标点击获取事件，并将li标签添加class 属性，让CSS渲染
            };
        }

    </script>
</body>
</html>

//示例4：
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Title</title>
	//css
    <style>
        ul li.active{
            color:red;
        }
    </style>
</head>
<body>
     //html
    <div id="box">MJJ</div>
    <ul id="box2">
        <li>1</li>
        <li>2</li>
        <li>3</li>
    </ul>
//JS:
    <script>
        var lis = document.getElementsByTagName('li');
        //三个li签
        console.log(lis);//HTMLCollection(3) [li, li, li]
        for (var i=0;i<lis.length;i++){//循环
            lis[i].onclick = function(){
                for (var k=0;k<lis.length;k++){ 
                    lis[k].className = '';//排他思想，让其他li默认class=''
                }

                this.className= 'active';//鼠标点击获取事件，并将li标签添加class 属性，让CSS渲染
            };
        }

    </script>
</body>
</html>
```

##### 2.3.3document.getElementsByClassName()

- DOM中也提供了通过类名来获取某些DOM元素对象的方法.

```javascript
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Title</title>
    <style>
        ul li.active{
            color:red;
        }
    </style>
</head>
<body>
    <div id="box">MJJ</div>
    <ul id="box2">
        <li class="active">1</li>
        <li>2</li>
        <li>3</li>
    </ul>
        <script>
        var lis = document.getElementsByClassName('active');
        console.log(lis);//HTMLCollection [li.active]
        var box = document.getElementById("box2");
        console.log(box.children);//获得box2下所有的子元素   HTMLCollection(3) [li.active, li, li]
    </script>
</body>
</html>
```



```javascript
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Title</title>
    <style>
        ul li.active{
            color:red;
        }
        ul li.att{
            color:blue;
        }
    </style>
</head>
<body>
    <div id="box">MJJ</div>
    <ul id="box2">
        <li class="active">1</li>
        <li>2</li>
        <li>3</li>
    </ul>
//红色蓝色来回切换
    <script>
        var lis = document.getElementsByClassName('active')[0];
        var des = true;
        lis.onclick = function(){
            if (des){
                this.className = 'att';des=false;
            }else{this.className = 'active';
                des = true;
            }
        };
	</script>
</body>
</html>
```

#### 2.4对样式的操作

- box.style.属性(CSS属性带`-`，其后面字母大写)

```javascript
//示例1
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Title</title>
    <style>
    	#box{
            width: 200px;
            height: 200px;
            background-color: red;
        }
    </style>
</head>
<body>
    <div id="box">MJJ</div>
    <script type="text/javascript">
        // 1.获取事件源对象
        var box = document.getElementById('box');
        // console.log(box);
		//鼠标悬浮，更改样式
		//2.绑定事件
        box.onmouseover = function () {
            box.style.backgroundColor = "green";
            box.style.fontSize = '30px';
            box.style.width = "200px";
            box.style.height = '200px';
        };
		//鼠标离开更改样式
        box.onmouseout = function(){
            box.style.backgroundColor = 'red';
            box.style.fontSize = '15px';
            box.style.width = "400px";
            box.style.height = '400px';
        };
    </script>
</body>
</html>

//示例2：
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Title</title>
    <style>
        #box{
            width: 200px;
            height: 200px;
            background-color: red;
        }
    </style>
</head>
<body>
    <div id="box">MJJ</div>
    <script type="text/javascript">
        var box = document.getElementById('box');
        des = true;
        box.onclick = function(){
            if (des){
                box.style.backgroundColor = 'blue';
                des = false;
            }else{
                box.style.backgroundColor = 'green';
                des = true;
            }
        }
    </script>
</body>
</html>
```

#### 2.5对属性的操作

- getAttribute() 接收一个参数，打算查询的属性名字。
- setAttribute() 语序我们对属性节点的值做修改，传递两个参数，第一个参数为属性名，第二个参数为属性值。

```javascript
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Title</title>
    <style>
        .a{
            color:red;
            font-size:30px;
        }
        p.active{
            color:blue;
        }
    </style>
</head>
<body>
    <p title="my class" id = 's21' class="a">21</p>
    <script>
        var p1 = document.getElementById('s21');
        console.log(p1.getAttribute('title'));//my class
        console.log(p1.getAttribute('class'));//a
        p1.setAttribute('class','s20'); //更改属性class  值为s20
        p1.setAttribute('hello','python');//建立属性hello 值为python
        console.log(p1.getAttribute('hello'));//python
        console.log(p1.className);//s20
        console.log(p1.title);//my class
        console.dir(p1);//p#s21.s20 属性
        p1.onclick = function(){
            this.className = this.className + ' active';//加上' active' CSS可用 p.active
        };



    </script>
</body>
</html>
```

![1559638364371](C:\Users\Xu jk\AppData\Roaming\Typora\typora-user-images\1559638364371.png)

#### 2.6节点的创建添加删除

##### 2.6.1 createElement()创建节点

- 此方法可返回一个Element对象。

  ```javascript
  var newNode = document.createElement(tagName);
  ```

##### 2.6.2 innerText属性

- 仅仅对元素获取或者设置文本

  ```javascript
  newNode.innerText = '你好';
  ```

##### 2.6.3 innerHTML属性

- 既可以设置文本又可以设置标签

  ```javascript
  newNode.innerHTML = `<a>mjj</a>`
  ```

注意：如果想获取节点对象的文本内容，可以直接调用innerText或者innerHTML属性来获取。

```javascript
<body>
    <div id="box">MJJ
        <ul id="box2">
            <li class="active">1</li>
            <li>2</li>
            <li>3</li>
        </ul>
    </div>
    <script>
        var box = document.getElementById('box');
        console.log(box.innerText);
		console.log(box.innerHTML);
    </script>
 </body>

//这是innerText结果
//MJJ
//1
//2
//3

//这是innerHTML结果
//MJJ
//      <ul id="box2">
//          <li class="active">1</li>
//          <li>2</li>
//          <li>3</li>
//      </ul>
```

##### 2.6.4 appendChild() 插入节点

- 在指定的节点的最后一个子节点之后添加一个新的子节点。

  ```javascript
  appendChild(newNode);
  ```

##### 2.6.5insertBefore()  插入节点

- 方法可在已有的子节点前插入一个新的子节点

  ```javascript
  insertBefore(newNode,node);
  ```

##### 2.7 removeChild() 删除节点

- removeChild()方法从子节点列表中删除某个节点。如果删除成功，此方法可返回被删除的节点，如失败，则返回NULL。
- 例题：

```javascript
<body>
    <ul id = 'box'></ul>
    <script type = 'text/javascript'>
        //通过ID获取单个节点对象
        var ul = document.getElementById('box');
        var li1 = document.createElement('li');
        var li2 = document.createElement('li');

        //innerText 只能设置文本格式内容
        li2.innerText = '你好';
        //innerHTML 可以设置HTML格式，如标签
        li1.innerHTML =`<a href='#'>123</a>`;



        //给li1子标签(a)添加属性
        li1.children[0].style.color = 'blue';
        li1.children[0].style.fontSize = '28px';

        //给li2标签添加属性（也就是li）color
        // console.log(li2);//li
        li2.style.color = 'red';


        //将创建好的标签加入ul里。
        ul.appendChild(li1);
        ul.appendChild(li2);


        //将li2更改text值，并在li1前面，插入ul标签li2,
        li2.innerHTML = '第一个';
        ul.insertBefore(li2,li1);
        //将li2标签移除
        ul.removeChild(li2);
    </script>
</body>

```

### 3.事件

​     事件是您在编程时系统内发生的动作或者发生的事情，系统通过它来告诉您在您愿意的情况下您可以以某种方式对它做出回应。例如：如果您在网页上单击一个按钮，您可能想通过显示一个信息框来响应这个动作。

主要事件有：

|    事件     |         说明         |
| :---------: | :------------------: |
|   onclick   |     鼠标单击事件     |
| onmouseover |     鼠标经过事件     |
| onmouseout  |     鼠标移开事件     |
|  onchange   |  文本框内容改变事件  |
|  onselect   | 文本框内容被选中事件 |
|   onfocus   |     光标聚焦事件     |
|   onblur    |     光标失焦事件     |
|   onload    |     网页加载事件     |

#### 3.1 鼠标单击事件onclick

#### 3.2鼠标经过事件(onmouseover)&&鼠标移开事件(onmouseout)

#### 3.3光标聚焦事件(onfocus)&&光标失焦事件(onblur)

### 4.应用

#### 1.遍历数据操作页面

```javascript
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Title</title>
    <style>
        li p.name{
            color:red;
        }
        li span.price{
            color:blue;
        }
    </style>
</head>
<body>
   		<ul id="box"></ul>
	    <script>
        //通过ID获取单个节点对象
        var box = document.getElementById('box');
        var data = [
				{id:1,name:'小米8',subName:'你真好',price:98},
				{id:2,name:'小米6',subName:'你真好2',price:948},
				{id:3,name:'小米4',subName:'你真好3',price:100},
				{id:4,name:'小米2',subName:'你真好4',price:928},
				{id:5,name:'小米10',subName:'你真好5',price:918}
				];
        //遍历data，以便操作数组每个元素
        for (var i=0;i<data.length;i++){
            //创建标签li  创建节点
            var li = document.createElement('li');
            //插入数据
            li.innerHTML = `<p class="name">${data[i].name}</p>
            <p class="subName">${data[i].subName}</p>
            <span class="price">${data[i].price}元</span>
            `;
            //提交数据
            box.appendChild(li);
        }
    </script>
</body>
</html>
```

#### 2.切换图片



```javascript
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Title</title>
</head>
<body>
    <div id = 'box'>
        <img src="images/1.jpg" alt="" id = "imgBox">
    </div>
    <button id="prev">上一张</button>
    <button id="next">下一张</button>
    <script>
        //获取id = 'box'
        var imgBox = document.getElementById('imgBox');
        //获取id = 'next'
        var next = document.getElementById('next');
        //获取id = 'prev'
        var prev = document.getElementById('prev');
        var num = 1;
        //事件点击next,此时执行nextImg函数
        next.onclick = function () {
            nextImg();

        };
        //nextImg函数增加num值获得下一张图片，最后一张做判断，返回第一张
        function nextImg() {
            num ++;
            if (num===5){
                num = 1;
            }
            imgBox.src = `images/${num}.jpg`;

        }
        //事件点击prev 此时执行prevImg()函数
        prev.onclick = function(){
          prevImg();
        };
        //prevImg函数减少num值获得上一张图片，当num=0返回最后一张图片
        function prevImg() {
            num --;
            if (num===0){
                num = 4;
            }
            imgBox.src = `images/${num}.jpg`;
        }
    </script>
</body>
</html>
```













