# # 列举你了解的面向对象中的特殊成员，并为每个写代码示例。
# # __init__
# # __new__
# ''''''
# class People:
#     def __init__(self,name):
#         print('aaaa')
#         self.name = name
#     def __new__(cls, *args, **kwargs):
#         print('bbbbb')
#         return object.__new__(cls)
#     def __str__(self):
#         return self.name
#     def __call__(self, *args, **kwargs):
#         print('alex')
# peiqi = People('武sir')
# print(peiqi.name)
# '''
# 先调用new方法开辟内存空间创造空对象，然后在调用init初始化对象属性
# '''
# #__str__
# print(peiqi)
# '''
# 打印对象默认调用__init__方法
# '''
# # __call__方法
# '''
# 对象+括号执行call方法
# '''
# peiqi()
# #__dict__
# val = peiqi.__dict__
# print(val)
# '''
# 返回一个字典，实例化变量为建，值为值
# '''
# # ----------------------------------------------------------------------------
# class A:
#     def __enter__(self):
#         print('aaa')
#         self.x = open('a.txt','w',encoding='utf-8')
#         return self.x
#     def __exit__(self, exc_type, exc_val, exc_tb):
#         print('ccc')
#         self.x.close()
#
# a = A()
# with a as f:
#     print('sss')
# '''
# with 会 执行enter方法
# 然后执行内部
# 最后执行完缩进内部会执行exit 方法
# '''
# class CCC:
#     def __setitem__(self, key, value):
#         pass
#     def __getitem__(self, item):
#         return item + 'alex'
#     def __delitem__(self, key):
#         pass
# aaa = CCC()
# aaa['apple']  = 'alex'   #调用了setitem方法
# print(aaa['apple'])   #调用了getitem方法
# del aaa['apple']     #调用了delitem方法
# # --------------------------------------
# # __add__方法
# class Son:
#     def __add__(self, other):
#         return 111
# A = Son()
# B  = Son()
# val = A + B  #slef 带表A触发add方法 ，B为other
# print(val)

# 看代码写结果

# class Foo(object):
#
#     def __init__(self, age):
#         self.age = age
#
#     def display(self):
#         print(self.age)
#
#
# data_list = [Foo(8), Foo(9)]
# for item in data_list:
#     print(item.age, item.display())
# '''
# 8
# 8 None
# 9
# 9 None
# '''
# # 看代码写结果
#
#
# class Base(object):
#     def __init__(self, a1):
#         self.a1 = a1
#
#     def f2(self, arg):
#         print(self.a1, arg)
#
#
# class Foo(Base):
#     def f2(self, arg):
#         print('666')
#
#
# obj_list = [Base(1), Foo(2), Foo(3)]
# for obj in obj_list:
#     obj.f2('333')
# '''
# 1 333
# 666
# 666
# '''
# # 看代码写结果
#
# class StarkConfig(object):
#
#     def __init__(self,num):
#         self.num = num
#
#     def changelist(self,request):
#         print(self.num,request)
#
# class RoleConfig(StarkConfig):
#
#     def changelist(self,request):
#         print('666')
#
# config_obj_list = [StarkConfig(1),StarkConfig(2),RoleConfig(3)]
# for item in config_obj_list:
#     print(item.num)
# '''
# 1
# 2
# 3
# '''
# # 看代码写结果
#
# class StarkConfig(object):
#
#     def __init__(self,num):
#         self.num = num
#
#     def changelist(self,request):
#         print(self.num,request)
#
# class RoleConfig(StarkConfig):
#     pass
#
# config_obj_list = [StarkConfig(1),StarkConfig(2),RoleConfig(3)]
# for item in config_obj_list:
#     item.changelist(168)
# '''
# 1 168
# 2  168
# 3 168
# '''
# # 看代码写结果
#
# class StarkConfig(object):
#
#     def __init__(self,num):
#         self.num = num
#
#     def changelist(self,request):
#         print(self.num,request)
#
# class RoleConfig(StarkConfig):
#
#     def changelist(self,request):
#         print(666,self.num)
#
# config_obj_list = [StarkConfig(1),StarkConfig(2),RoleConfig(3)]
# for item in config_obj_list:
#     item.changelist(168)
# '''
# 1 168
# 2 168
# 666 3
# '''
# # 看代码写结果
#
# class StarkConfig(object):
#
#     def __init__(self,num):
#         self.num = num
#
#     def changelist(self,request):
#         print(self.num,request)
#
#     def run(self):
#         self.changelist(999)
#
# class RoleConfig(StarkConfig):
#
#     def changelist(self,request):
#         print(666,self.num)
#
# config_obj_list = [StarkConfig(1),StarkConfig(2),RoleConfig(3)]
# config_obj_list[1].run()
# config_obj_list[2].run()
# '''
# 2 999
# 666 3
# '''
# # 看代码写结果
#
# class StarkConfig(object):
#
#     def __init__(self,num):
#         self.num = num
#
#     def changelist(self,request):
#         print(self.num,request)
#
#     def run(self):
#         self.changelist(999)
#
# class RoleConfig(StarkConfig):
#
#     def changelist(self,request):
#         print(666,self.num)
#
#
# class AdminSite(object):
#     def __init__(self):
#         self._registry = {}
#
#     def register(self,k,v):
#         self._registry[k] = v
#
# site = AdminSite()
# print(len(site._registry))
# site.register('range',666)
# site.register('shilei',438)
# print(len(site._registry))
#
# site.register('lyd',StarkConfig(19))
# site.register('yjl',StarkConfig(20))
# site.register('fgz',RoleConfig(33))
#
# print(len(site._registry))
# print(site._registry)
'''
0
2
5
{'range': 666, 'shilei': 438, 'lyd':对象地址 ，'yjl',对象地址,'fgz':对象地址}
'''
# 看代码写结果

# class StarkConfig(object):
#
#     def __init__(self,num):
#         self.num = num
#
#     def changelist(self,request):
#         print(self.num,request)
#
#     def run(self):
#         self.changelist(999)
#
# class RoleConfig(StarkConfig):
#
#     def changelist(self,request):
#         print(666,self.num)
#
# class AdminSite(object):
#     def __init__(self):
#         self._registry = {}
#
#     def register(self,k,v):
#         self._registry[k] = v
#
# site = AdminSite()
# site.register('lyd',StarkConfig(19))
# site.register('yjl',StarkConfig(20))
# site.register('fgz',RoleConfig(33))
# print(len(site._registry)) # 3
#
# for k,row in site._registry.items():
#     row.changelist(5)
# '''
# 3
# 19 5
# 20 5
# 666 33
# '''
# # 看代码写结果
#
# class StarkConfig(object):
#
#     def __init__(self,num):
#         self.num = num
#
#     def changelist(self,request):
#         print(self.num,request)
#
#     def run(self):
#         self.changelist(999)
#
# class RoleConfig(StarkConfig):
#
#     def changelist(self,request):
#         print(666,self.num)
#
# class AdminSite(object):
#     def __init__(self):
#         self._registry = {}
#
#     def register(self,k,v):
#         self._registry[k] = v
#
# site = AdminSite()
# site.register('lyd',StarkConfig(19))
# site.register('yjl',StarkConfig(20))
# site.register('fgz',RoleConfig(33))
# print(len(site._registry)) # 3
#
# for k,row in site._registry.items():
#     row.run()
# '''
# 19 999
# 20 999
# 666 33
# '''
# # 看代码写结果
#
# class UserInfo(object):
#     pass
#
# class Department(object):
#     pass
#
# class StarkConfig(object):
#
#     def __init__(self,num):
#         self.num = num
#
#     def changelist(self,request):
#         print(self.num,request)
#
#     def run(self):
#         self.changelist(999)
#
# class RoleConfig(StarkConfig):
#
#     def changelist(self,request):
#         print(666,self.num)
#
# class AdminSite(object):
#     def __init__(self):
#         self._registry = {}
#
#     def register(self,k,v):
#         self._registry[k] = v(k)
#
# site = AdminSite()
# site.register(UserInfo,StarkConfig)
# site.register(Department,StarkConfig)
# print(len(site._registry))
# for k,row in site._registry.items():
#     row.run()
# '''
# 2
# .UserInfo' 999
# Department' 999
# '''
# # 看代码写结果
#
#
# class F3(object):
#     def f1(self):
#         ret = super().f1()
#         print(ret)
#         return 123
#
#
# class F2(object):
#     def f1(self):
#         print('123')
#
#
# class F1(F3, F2):
#     pass
#
#
# obj = F1()
# obj.f1()
# '''
# 123
# None
# '''
# # 看代码写结果
#
#
# class Base(object):
#     def __init__(self, name):
#         self.name1 = name
#
#
# class Foo(Base):
#     def __init__(self, name):
#         super().__init__(name)
#         self.name = "于大爷"
#
#
# obj1 = Foo('二爷')
# print(obj1.name)
# print(obj1.name1)
#
# obj2 = Base('三爷')
# print(obj2.name1)
# '''
# 于大爷
# 三爷
#
# '''
# # 看代码写结果
#
# class Base(object):
#     pass
#
# class Foo(Base):
#     pass
#
#
# obj = Foo()
#
# print(type(obj) == Foo)
# print(type(obj) == Base)
# print(isinstance(obj,Foo))
# print(isinstance(obj,Base))
# '''
# True
# Flase
# True
#
# True
# '''
# # 看代码写结果
#
# class StarkConfig(object):
#     def __init__(self, num):
#         self.num = num
#     def __call__(self, *args, **kwargs):
#         print(self.num)
# class RoleConfig(StarkConfig):
#     def __call__(self, *args, **kwargs):
#         print(self.num)
# v1 = StarkConfig(1)
# v2 = RoleConfig(11)
# v1()
# v2()
# '''
# 1
# 11
# '''
# # 看代码写结果
#
#
# class StarkConfig(object):
#     def __init__(self, num):
#         self.num = num
#
#     def run(self):
#         self()
#
#     def __call__(self, *args, **kwargs):
#         print(self.num)
#
#
# class RoleConfig(StarkConfig):
#     def __call__(self, *args, **kwargs):
#         print(345)
#
#     def __getitem__(self, item):
#         return self.num[item]
#
#
# v1 = RoleConfig('alex')
# v2 = StarkConfig("wupeiqi")
#
# print(v1[1])
# print(v2[2])
'''
l
wupeiqi
'''
# 补全代码

class Context:
    def __enter__(self):
        print('aaaa')
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
       print('bbbb')
    def do_something(self):
        print('abc')

with Context() as ctx:
    print('sss')
    ctx.do_something()
# 补全代码----------------------------------------------------------
class Stack(object):
    def __init__(self):
        self.data_list = []

    def push(self, val):
            return self.data_list.append(val)

    def pop(self):
        a = self.data_list.pop()
        return a
obj = Stack()
# 调用push方法，将数据加入到data_list中。
obj.push('alex')
obj.push('武沛齐')
obj.push('金老板')

# 调用pop讲数据从data_list获取并删掉，注意顺序(按照后进先出的格式)
v1 = obj.pop()  # 金老板
v2 = obj.pop()  # 武沛齐
v3 = obj.pop()  # alex

# 请补全Stack类中的push和pop方法，将obj的对象维护成 后进先出 的结构。


# 如何主动触发一个异常？
try:
    print('alex')
    raise Exception('cuocuocuo')
except Exception as e:
    print(e)
# 看代码写结果


def func(arg):
    try:
        int(arg)
    except Exception as e:
        print('异常')
    finally:
        print('哦')


func('123')
func('二货')
'''
哦
二货
哦
'''