days21

###### 1嵌套

###### 2. 特殊成员

```python
1.__init__

2.__new__方法
class Foo(object):
    def __init__(self):
        """
        用于给对象中赋值，初始化方法
        """
        self.x = 123
    def __new__(cls, *args, **kwargs):
        """
        用于创建空对象，构造方法
        :param args: 
        :param kwargs: 
        :return: 
        """
        return object.__new__(cls)
```



```python
3.__call__
用于对象+括号
class Foo:
	def __call__(slef,*args,**kwargs):
		print('执行call方法')
#obj = Foo（）
obj（）#执行call方法
Foo()()#执行call方法

```

```python
#4.__getitem__
#__setitem__
#__delitem__
class Foo(object):
    def __setitem__(self, key, value):
        pass
    def __getitem__(self, item):
        return item + 'uuu'
    def __delitem__(self, key):
        pass
obj1 = Foo()
obj1['k1'] = 123  # 内部会自动调用 __setitem__方法
val = obj1['xxx']  # 内部会自动调用 __getitem__方法
print(val)
del obj1['ttt']  # 内部会自动调用 __delitem__ 方法

```

```python
#__str__方法
class Foo(object):
    def __str__(self):
        """
        只有在打印对象时，会自动化调用此方法，并将其返回值在页面显示出来
        :return: 
        """
        return 'asdfasudfasdfsad'

obj = Foo()
print(obj)

#__dict__方法
class Foo(object):
    def __init__(self,name,age,email):
        self.name = name
        self.age = age
        self.email = email

obj = Foo('alex',19,'xxxx@qq.com')
print(obj)
print(obj.name)
print(obj.age)
print(obj.email)
val = obj.__dict__ # 去对象中找到所有变量并将其转换为字典
print(val)

```



```python
# class Context:
#     def __enter__(self):
#         print('进入')
#         return self
#
#     def __exit__(self, exc_type, exc_val, exc_tb):
#         print('推出')
#
#     def do_something(self):
#         print('内部执行')
#
# with Context() as ctx:
#     print('内部执行')
#     ctx.do_something()
```

```python
#2.8 两个对象相加
__add__
val = 5 + 8
print(val)

val = "alex" + "sb"
print(val)

class Foo(object):
    def __add__(self, other):
        return 123
    
obj1 = Foo()
obj2 = Foo()
val  = obj1 + obj2
print(val)
```

##### 		功能

​		特殊成员：就是为了能够快速实现执行某些方法而生

###### 	3 . 内置函数补充

```python
#1.type,查看类型
class Foo:
    pass

obj = Foo()

if type(obj) == Foo:
    print('obj是Foo类的对象')
```

```python
#issubclasss（a,b）   判断时候判断a是否为b的子类
class Base:
    pass

class Base1(Base):
    pass

class Foo(Base1):
    pass

class Bar:
    pass

print(issubclass(Bar,Base))
print(issubclass(Foo,Base))
```

```python
#isinstance 
class Base(object):
    pass

class Foo(Base):
    pass

obj = Foo()

print(isinstance(obj,Foo))  # 判断obj是否是Foo类或其基类的实例（对象）
print(isinstance(obj,Base)) # 判断obj是否是Foo类或其基类的实例（对象）
```

```python
#supper
class Base(object):
    def func(self):
        print('base.func')
        return 123


class Foo(Base):
    def func(self):
        v1 = super().func()
        print('foo.func',v1)

obj = Foo()
obj.func()
# super().func() 去父类中找func方法并执行
```

```python
class Bar(object):
    def func(self):
        print('bar.func')
        return 123

class Base(Bar):
    pass

class Foo(Base):
    def func(self):
        v1 = super().func()
        print('foo.func',v1)

obj = Foo()
obj.func()
super(Foo,obj).func()#只调用父类中的方法
# super().func() 根据类的继承关系，按照顺序挨个找func方法并执行(找到第一个就不在找了)
```

```python
class Base(object): # Base -> object
    def func(self):
        super().func()
        print('base.func')

class Bar(object):
    def func(self):
        print('bar.func')

class Foo(Base,Bar): # Foo -> Base -> Bar
    pass

obj = Foo()
obj.func()

# super().func() 根据self对象所属类的继承关系，按照顺序挨个找func方法并执行(找到第一个就不在找了)
```

###### 异常处理

- 基本格式

  ```python
  try:
      pass
  except Exception as e:
      pass
  ```

  ```python
      v = []
  try:
      v = []
      v[11111] # IndexError
  except ValueError as e:
      pass
  except IndexError as e:
      pass
  except Exception as e:
      print(e) # e是Exception类的对象，中有一个错误信息。
  ```

  ```python
  try:
      int('asdf')
  except Exception as e:
      print(e) # e是Exception类的对象，中有一个错误信息。
  finally:
      print('最后无论对错都会执行')
      
  # #################### 特殊情况 #########################
  def func():
      try:
          # v = 1
          # return 123
          int('asdf')
      except Exception as e:
          print(e) # e是Exception类的对象，中有一个错误信息。
          return 123
      finally:
          print('最后')
  
  func()
  ```

  

###### 主动抛出异常

```python
try:
    int('123')
    raise Exception('阿萨大大是阿斯蒂') # 代码中主动抛出异常
except Exception as e:
    print(e)
```

```python
def func():
    result = True
    try:
        with open('x.log',mode='r',encoding='utf-8') as f:
            data = f.read()
        if 'alex' not in data:
            raise Exception()
    except Exception as e:
        result = False
    return result
```

###### 自定义异常

```python
class MyException(Exception):
    pass

try:
    raise MyException('asdf')
except MyException as e:
    print(e)
```

```
class MyException(Exception):
    def __init__(self,message):
        super().__init__()
        self.message = message

try:
    raise MyException('asdf')
except MyException as e:
    print(e.message)
```

