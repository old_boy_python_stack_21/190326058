find



查找条件

- 按照名称来搜索

  - name：

    ```   
    find -name a 完全匹配
    find -name "a*"  a开头的所有文件
    find -name “a？”   a开头的2位长度
    find -name a[ab]   aa，ab
    ```

  - 忽略大小写
  - find -iname   a

- 指定最大的查找层数(在某目录下开始)

  - find -maxdepth  2 -name  a    

- 指定最小查找层数（在某目录下开始）

  - find -mindepth -name a

- 指定搜索类型

  - find -type f -name a    指定为文件  可不指定名字
  - f 普通文件
  - d 目录
  - l 符号链接
  - s 套接字
  - b 块设备
  - c 字符设备文件
  - p 管道文件

- 搜索空文件或者目录

  - find -empty
  - find  -type  f -empty   

- 属主和属组来搜索

  - -user username 查找属主为指定用户的文件
  - -group groupname 查找属组为指定组的文件
  - -uid userid 查找属主为指定的uid的文件
    - id 属主名字  得到uid
  - -gid groupid 查找属组为指定gid的文件
  - -nouser 查找没有属主的文件
    - 数字代表没有属主
  - -nogroup 查找没有属组的文件

- 组合条件

  - 与 -a
  - 或 -o
  - 非 -not
  - 非 ！

- 摩根定律

  - (非A) 或（非B）=非（A且B）
  - （非A）且（非B）=非（A或B）

- 排除目录

  - -path 

    ```python
    查找/etc/下，除/etc/sane.d目录的其它所有.conf后缀的文件
    find /etc -path ‘/etc/sane.d’ -a –prune -o -name “*.conf” 查找/etc/下，除/etc/sane.d和/etc/fonts两个目录的所有.conf后缀的文件
    find /etc \( -path "/etc/sane.d" -o -path "/etc/fonts" \) -a -prune -o - name "*.conf"
    ```

    

- 根据文件大小来查找

  - -size[+|-] #unit 常用单位：k,M,G,c(byte)
  - \#unit :(#-1,#] 不包括前面，包括后面
  - -#unit: [0,#-1] 0-#-1
  - +#unit:(#,….) #到无穷

- 根据时间戳：

  - 以“天”为单位
    - -atime             [+|-]#         访问时间
      - \#:             [#,#+1)     包括#  不包括#+1
      - +#:           [#+1,….]       包括#+1  到无穷
      - -#:            [0,#)              0-#  不包括3
    - -mtime
      - 创建时间
    - -ctime
      - 修改时间 
  - 以“分钟为单位“
    - -amin
    - -mmin
    - -cmin

  - 权限   
    - perm  777   -ls       

### 处理动作

-  -print   把搜索的结果打印到屏幕
- -ls 类似于查找到的文件执行“ls -l”命令
- -delete 删除查找到的文件
- -fls file 查找的所有文件的长格式信息保存至指定的文件中
- -ok command {} \;对查找到的每个文件执行command指定的命令，对于每个文件执行命令之前，都会交互式要求用户确认

- -exec command {} \; 对查找到的每个文件执行command命令      不需要用户确认
  - {} 用于引用查找到的文件名称本身
  - find 传递查找到的文件至后面指定的命令时，查找到所有符合条件的文件（一次性传递）给后面的命令

### xargs   

- 由于很多命令不支持管道来传递参数，而日常工作中有这个必要，所以就有了xargs命令

- xargs 把一个明亮的输出结果，一个一个的传递给后面要执行的命令

- 当有些命令不支持太多参数传递时，命令执行可能会失败，xargs可以借鉴

- ```SHELL
  ls f* |xargs rm
  find /sbin -perm +700 |ls -l 这个命令是错误的
  find /sbin -perm +7000 | xargs ls –l 查找特殊权限的文件
  ```

- find和xargs格式:find | xargs COMMAND

### grep

文本处理三剑客

grep ：Global search REgular expression and Print out the line   全局正则表达式搜索，并且打印符合条件的行

 - grep
 - sed-
 - awk

grep命令

```SHELL
-color=auto 对匹配到的文本进行颜色显示
-v 取反
-i 忽略大小写
-n 显示匹配到的行号
-c 统计匹配到的行数
-o 仅显示匹配到的字符串
-q 静默模式，不输出任何信息
echo $?    打印上一次命令结果的返回值，如果成功为0   失败为非0
-A # after，后#行
-B # before，前#行
-C # context 前后各#行
-e 实现多个选项的逻辑or关系    表示或者关系
	grep -e 'root'  
-E 扩展的正则表达式
-r  连带文件夹以下目录也查找   递归查找
-w 匹配整个单词
```

### 正则表达式

- 字符匹配

  ```python
  . 匹配任意单个字符
  [] 匹配指定范围内的任意单个字符 [alex] [0-9] [a-z] [a-zA-Z]
  取反
  [:alnum:] 字母和数字   [a-zA-Z0-9]
  [:alpha:] 大小写字母
  [:lower:] 小写字母
  [:upper:] 大写字母
  [:blank:] 空白字符
  [:digit:] 数字
  [:xdigit:] 十六进制数字
  [:punct:] 标点符号
  ```

- 匹配次数   (如何转义)

  - *0次或多次   贪婪匹配
  - \?  0次或多次
  - \ +  1次或多次
  - \\{n\\} 匹配n次  

  - \\{m,n\\}   最少m次，最多n次
  - \\{,n\\}   最多n次
  - \\{m,\\}   最少m次

- 位置锚定

  - ^首行锚定，以什么开始
  - $ 结尾
  - ^$  空行

- 向后引用

  - \\1    : 表示前面匹配之后产生的字符，在\\1的位置要在出现一次   第一个括号里
  - \\2     :  第二个括号

### egrep

​	egrep = grep +E

​	支持扩展正则表达式，与标准增长表达式的区别就是不需要 转义

​    但是向后引用还是\\1	

## 压缩

### gzip

gzip [option] ….file…     压缩 文件默认删除文件

gunzip   解压文件      

-d 解压缩，相当于gunzip

-c 结果输出之标准输出，保留原来文件，    默认是不保留源文件

-# 1-9，指定压缩比，值越大压缩比例越大

​	-  gzip -9 passwd   

zcat 不显式加压缩的前提下查看文本文件内容    查看压缩包内的文件

- zcat passwd.gz > passwd

```SHELL
gzip -c messages >messages.gz
gzip -c -d messages.gz > messages 
zcat messages.gz > messages
cat messages | gzip > m.gz
```

### bzip

bzip [option]…file...

-k 保留原文件

-d 解压缩                     bunzip2  文件名      解压

 -# 1-9 压缩比，默认是9

bzcat 不显式加压缩的前提下查看文本文件内容，查看压缩包内的文件

### XZ

xz  压缩

unxz  解压

-k保留原文件

xz -d 解压 

xz -9  指定压缩比

xzcat   查看压缩包内的文件

### tar    归档

tar -cvf  a.tar  a  b       c创建  v显示创建过程   f指定文件      

tar cvf  a.tar  a  b          将a，b 文件归档到a.tar 文件 （不压缩）

r  追加     tar -xf  a.tar   解压

-  -C  指定解压位置    tar xf a.tar  -C /opt
- j   使用bzip2来压缩   
- tar zcvf a.tar.gz b c d         使用gzip  压缩
- J     使用 xz压缩

#### 解压

tar  zxvf  a.tar.gz    使用gzip解压   用谁压缩就用谁解压

可以直接用xf解压 不指定解压方式

- tar xf a.tar.gz

split

split -b size file -d tarfile

-b 指定每一个文件的大小

-d 指定数字  默认是字母

合并： cat tarfile*   >file.tar.gz

