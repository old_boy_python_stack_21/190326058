### Linux完整版

### 安装centos

- 直接在机器上安装
- 双系统
- 通过虚拟软件在windows上安装linux

### 虚拟软件：通过软件来模拟生成硬件信息

- vmvare
- vbox
- mac

桥接：会跟你的windows机器获取同一个网段的ip地址

net：不会跟windows机器获取同一个网段的ip地址

2^32

2^64

密码要求：

- 12位及其以上
- 必须包含大写字母，小写字母，数字，特殊字符
- 3个月或者半年更换一次

弱口令： 



### linux用户

- root 用户
  - 超级管理员
  - 对系统有完全操作的权限
  - 误操作对系统的损害无限大
  - 尽量不要使用root登录
- 普通用户
  - 对系统的操作权限很小
  - 损害有限
  - 需要用普通用户登录

### 终端

- 图形终端
- 虚拟终端（）ctrl+alt+F1-6  /dev/tty#
- 物理终端
- 设备终端
- 串行终端
- 伪终端  /dev/pts/#
- tty 查看命令

### 远程连接工具

- xshell
- putty
- securecrt

命令

### 查看ip地址

```SHELL
ifconfig 查看ip地址
ip addr 
ip a
```

### 交互式接口

启动终端以后，在终端设备上会打开一个接口

- GUI 图形接口
- CLI
  - shell
  - powershell

### shell

用来在linux系统上的一个接口，用来将用户的输入发送给操作系统去执行，并把得到的结果输出出来

查看系统支持的shell cat  /etc/shells 

切换shell chsh -s shell

查看当前运行的shell echo $SHELL

### 命令提示符

```SHELL
[root@localhost ~]# 
# 超级管理员
$ 普通用户
[用户@主机名 目录]命令提示符
永久生效
echo 'PS1="\[\e[1;30;35m\][\u@\h \W]\\$\[\e[0m\]"' >> /etc/profile.d/ps.sh 
```

### 执行命令

写完命令后直接回车就可以

- 内部命令

  安装完系统以后自带的命令，就是内部命令

  通过help来获取内部命令的列表

- 外部命令

  - 第三方提供的，在某些地方可以直接找到执行文件

```SHELL
type 查看命令的类型
which 查找命令的路径
```

### alias 别名

```SHELL
alias 直接列出了系统里面所有的别名
alias cdetc='cd /etc' 设置别名
unalias cdetc 取消别名
#让命令一致生效
#对当前用户
[root@localhost ~]#echo "alias cdetc='cd /etc'" >> .bashrc
#对所有的用户都生效
echo "alias cdetc='cd /etc'" >> /etc/bashrc
ls 相当于list

```

### 执行原来本身的命令

- "ls"
- \ls
- 'ls'

### 单双引号的区别

"" 可以直接打印变量的值

'' 引号里面写什么就打印什么

### date

```SHELL
[root@localhost ~]#date
Mon Jul 29 12:18:14 CST 2019
[root@localhost ~]#date +%F
2019-07-29
[root@localhost ~]#date +%H（24小时制）
12
[root@localhost ~]#date +%I（12小时制）
12
[root@localhost ~]#date +%y
19
[root@localhost ~]#date +%m
07
[root@localhost ~]#date +%d
29
[root@localhost ~]#date +%M
22
[root@localhost ~]#date +%S
25
[root@localhost ~]#date +%a
Mon
[root@localhost ~]#date +%A
Monday
[root@localhost ~]#date +%T
12:23:31
[root@localhost ~]#date +%y-%m-%d
19-07-29
[root@localhost ~]#date +%Y-%m-%d
2019-07-29
unix元年
[root@localhost ~]#date +%s 时间戳
1564374331
[root@localhost ~]#date +%W 一年中的多少周
30
```

### 时区

```SHELL
[root@localhost ~]#timedatectl
[root@localhost ~]#timedatectl set-timezone  Asia/Shanghai
#显示所有时区
timedatectl list-timezones
#筛选亚洲的所有时区
timedatectl list-timezones |grep Asia
```

### 日历

```SHELL
cal 展示当月的日历
cal -y 展示当年的日历
cal -y # 显示#年的日历
```

### 关机重启

```SH
shutdown 默认是一分钟之后关机
shutdown -c 取消
shutdown -r 重启
TIME
	- now  立即
	hh：mm
	+# 表示多长时间以后重启
reboot 重启
       -p 切断电源
init 6 重启
init 0 关机
poweroff 关机

```

### 命令的格式

```SHELL
command [options] [args...]
选项：启用或者禁用某些功能的
	短选项：-a
	长选项：--all
参数：命令的作用对象，一般情况是目录，用户等等
注意：
	多个选项及参数和命令之间需要用空格隔开
	ctrl+c来取消命令的执行
	用；来隔开同时执行的多个命令
	使用\来将命令切换成多行
```

## 命令补全

tab键

内部命令：

外部命令：shell 会根据环境变量从左至右依次查找，找到第一个匹配的则返回

- 如果说给定的字符串只能搜索到一个的话，则直接显示
- 如果给定的字符串搜索到多个的话，则需要按两次tab键

目录补全

- 把用户给定的字符串当做路径的开始部分，来搜索
  - 如果只搜索到一个，则直接显示，直接一个tab
  - 如果说搜索到多个的话，列出一个列表，让用户自行选择，需要按两次tab键来获取列表

## echo 回显

输入什么就输出什么，并且加入了一个换行符

## 获取环境变量

```SHELL
[root@localhost ~]#echo $PATH
/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/root/bin
```

## 命令历史

- 可以通过键盘上的箭头来查找之前执行过的命令

- 通过history来获取之前执行过的命令记录

- 执行刚才执行过的命令

  - 键盘上的向上箭头
  - !!
  - !-1
  - ctrl+p

- 使用!:0来执行上一条命令（去掉参数）

- !# 来执行第多少条命令

  ```SHELL
  [root@localhost ~]#!123
  which cp
  alias cp='cp -i'
  	/usr/bin/cp
  ```

- !string 直接查找最近的一条包含string的命令

- ctrl+r 搜索之前执行过的命令

- ctrl+g来退出搜索

- 调用上一个命令的参数

  - esc .

- history 的命令历史

  ```shell
  history # 显示最后的#条命令
  history -c 清空命令记录
  ```

- 当用户登录系统之后，会读取家目录下的.bash_history文件

- 正常退出，会将之前执行过的命令写入到文件中

## 快捷键

ctrl+l 清屏，相当于clear

ctrl+s 锁定屏幕

ctrl+q 解开锁定

ctrl+c 总之命令

ctrl+a 移动到命令的行首 相当于home

ctrl+e 移动到行尾，相当于end

ctrl+xx 光标在命令行首和光标之间来回移动

ctrl +k  删除光标到结尾位置的字符

ctrl+u 删除光标到行首的字符

alt+r 删除正行

需要注意，alt会跟别的快捷键冲突

## 帮助信息

- 内部命令
  - help command
  - man bash
- 外部命令
  - command --help
  - command -h
  - man command     q退出
  - 官方文档

```shell
Usage: date [OPTION]... [+FORMAT]
  or:  date [-u|--utc|--universal] [MMDDhhmm[[CC]YY][.ss]]
  [] 代表可选
  ... 表示一个列表
  [-u|--utc|--universal] 任选其中一个
  -lh 代表-l -h
  date 052805271980 设置时间
ntpdate time.windows.com  自动与时间服务器同步时间
```



## man

箭头来控制输出

回车输出下一行

空格切换到下一屏

章节

1 用户的命令

2 系统的调用

3 c库的调用

4 设备文件或者特殊文件

5 配置文件

6 游戏

7 杂项

8 管理类的命令

9 linux内核API

## 目录结构

- 目录结构是一个倒置的树
- 目录从“/”开始
- 目录严格区分大小写
- 隐藏文件以.开头
- 路径的分隔符是/

# 文件名的命名规范

- 文件名最长为255字符
- 包裹路径在内最长4095个字符
- 除了/和NULL以外其他的字符都生效
- 名称大小写敏感

### 颜色的表示

- 蓝色表示目录
- 绿色表示可执行文件
- 红色表示压缩文件
- 蓝绿色 表示链接文件
- 白色  表示普通文件
- 灰色  表示其他文件

### 文件系统结构

- /boot      存放系统启动的引导文件，内核文件，引导的加载器放在该目录
- /bin    所有的用户都可以使用的一些基本命令
- /sbin  管理员可以使用的命令，管理类命令
- /lib    基本的一些库文件 （windows是dll，linux是so）
- /lib64  专门用于64位操作系统的一些辅助库文件
- /etc   配置文件目录   
- /home/Username    普通用户的家目录
- /root  超级管理员的家目录
- /media    便携式移动设备的挂载点
  - 例如U盘的一些东西
- /opt  第三方的安装程序
- /srv  系统上允许的服务用到的数据
- /tmp  存放临时文件的目录
  - 如果系统重启了，可能会丢失
  - 如果文件存在了很长时间超过一个月，系统会自动删除
- /usr 存放安装程序
- /var   存放经常变化的数据
  - var/log/   存放的日志
  - var/log/message   系统的启动日志
  - var/log/secure    链接记录  （那个用户在哪个IP链接了我的系统）
- /proc  用来存放一些内核  和进程相关的虚拟文件
- /dev  用来存放设备的、
- /mnt  临时文件挂载
- /run  系统或服务启动后生成的文件
- /sys   存放硬件设备相关的虚拟文件

### 程序的组成部分

- 二进制文件存放位置
  - /bin
  - /sbin
  - /usr/bin
  - /usr/sbin
  - /usr/local/bin
  - /usr/local/sbin
- 文件库
  - /lib
  - /lib64
  - /usr/lib
  - /usr/local/liv
  - /usr/local/lib64
- 配置文件
  - /etc
  - /etc/directory
  - /usr/local/etc
- 帮助文件
  - usr/share/man
  - /usr/share/doc    文档
  - /usr/local/share/man
  - /usr/local/share/doc

### 相对路径，绝对路径

绝对路径   ： 从根开始，完整的路径 ，可以找到需要的任何一个文件

相对路径： 相对于某个文件或目录 ，不是从根开始，可以用

- ..代表是父级目录
- . 代表当前路径

### 获取文件名和文件目录

basename  获取末尾文件名

dirname  获取文件目录

### cd

- change directory
- 可以使用 相对路径，也可以使用绝对路径

快速回到用户的家目录

直接敲cd

快速回到 上一次的目录

cd -



### pwd  

打印当前的工作目录

print working directory



### ls 

list  列出指定文件或指定文件夹

```sh
ls -a 显示所有文件，包括隐藏文件
ls -l  = ll 使用长格式显示文件相关信息
ls -R 递归显示目录
ls -d  显示目录本身
ls -1 竖着显示  目录下的所有文件
ls -lS  根据目录下文件大小降序排序
ls -lSr 升序排序  
ls -d */ 显示当前目录下的目录   不能指定目录，只显示当前目录
ls -lSh  加h，以人类易读的方式显示  大小带单位

```

### touch

```shell
touch a.txt  创建一个空文件
用来修改时间和创建空文件
如果文件存在则修改时间，如果不存在则创建空文件


```



### 命令的展开

touch a{1..10}             创建a1 -a10 10个文件

touch  a{1..10..2}        步长为2  生成 a1,a3,a5...     

touch  a{a..z}              创建aa-az 26个空文件

touch  a{a..z..2}         

seq 1 10

seq 1  2 10

### 命令引用



### 通配符

ab*    匹配0个或多个字符

ab？  匹配任意单个字符

ls~     代表家目录 

[0-9]   代表数字

[a-z]  从a-z  从A-Y

[A-Z]   从A-Z，从b-z

[abcdef]  表示其中的每一个

[:lower:]   小写字符      [[:lower:]]  每个小写字符

[:upper:]   大写字符        [[:upper:]]每个大写字符

[:digit:]     表示数字           [[:digit:]]     表示每个数字字符

[a-zA-Z]    [:alpha:]   结果相同    表示所有的a-z，A-Z

[:alnum：]   代表所有的字母和数字       

### stat   查看文件状态

```shell


访问时间：access			简称atime
修改时间：Modify     改变文件的内容  简称mtime
改变时间  change   简称ctime
```

### 复制文件和文件夹

```shell
cp   要复制的文件名  复制完的文件名
```

- 如果source是一个文件的话

  - 如果目标不存在，新进一个目标文件，并将数据写入到目标文件里面
  - 如果目标文件存在且，是一个目录，直接在目标目录下面新建一个跟源文件同名的文件
  - 如果目标文件是一个文件，直接就覆盖，为了安全起见，建议cp配合-i使用

- 如果源文件是多个文件的话

  - 目标文件如果是文件的话，直接报错
  - 目标文件是目录的话，直接复制进目

- 如果源文件是目录的话

  - 如果目标不存在，则创建指定的目录，必须有-r选项
  - 如果目录存在
    - 如果目录是个文件，报错
    - 如果目标是一个目录的话，则在目录下创一个新的同名目录，并把文件复制进去

  ### 常用参数

  ```shell
  -i 覆盖之前提示
  -n
  -r 递归复制，复制目录及目录下所有文件
  -f  强制 
  -v  显示复制过程
  -b  覆盖之前对原文件做备份
  cp  --backup=number  
  -p  保留原来的属性
  ```

  ### 移动或者是重命名

```SHELL
Usage: mv [OPTION]... [-T] SOURCE DEST
  or:  mv [OPTION]... SOURCE... DIRECTORY
  or:  mv [OPTION]... -t DIRECTORY SOURCE...
-i  交互式
-f  强制
-b  覆盖前做备份
-v 显示进度

```

## 删除

```SHELL
rm [OPTION]... FILE...
-i 交互式
-f 强制删除
-r 递归删除
rm -rf /* 慎用
rm -rf /* 慎用
rm -rf /* 慎用
cd /
rm -rf *

```

昨日补充

```SHELL
ls -ltu 按照atime时间排序
cp -n  不覆盖
```

## mkdir

```shell
mkdir s21
mkdir s21-{3..10}
mkdir -p a/b/c/d
mkdir -pv {s13,s14}/{ss11,ss12}/{sss11,sss12} 
-p 递归创建
-v 显示创建过程
```

## tree

```shell
yum install -y tree
tree name
-L 控制显示的层数
-b 只显示目录
```

## 文件类型

- \- 表示文件
- d表示目录
- l 表示链接
- b 块设备
- c 字符设备
- s 表示socket套接字

## rmdir 

只能删除空目录

```SHELL
mkdir -p s15/s16/s17/s18
rmdir -p s15/s16/s17/s18
```

## 链接

- 硬链接
  - ln 源文件 目标文件
  - 源文件发生改变，目标会发生改变
  - 将硬盘的引用次数+1
  - 删除
    - 将磁盘上的引用次数-1
    - 源文件删除对目标不会受影响
  - 不能对目录做硬链接
  - 不能跨越分区
- 软链接
  - 相当于windows的快捷方式
  - ln -s 可以生成软链接
  - 链接大小就是制定的源文件的字符数
  - 源文件发生改变，目标会发生改变
  - 删除
    - 源文件删除目标会收影响
  - 可以对目录做软链接
  - 可以跨域分区



## 文件权限

```shell
drwxr-xr-x. 4                          root root 30 Jul 31 08:55 s12
	权限     在磁盘上的引用次数（硬链接）    属主  属组 大小  atime      文件名 
```

## 输入 输出

### 输入

标准输入：接收来自键盘的输入 stdin 0

### 输出

标准输出：默认输出到终端  stdout 1

错误输出：默认输出到终端 stderr  2

## i/o重定向

把输出和错误信息重定向到文件或者别的地方

\> 覆盖

- \> 把stdout的数据重定向到文件里面
- 2> 把stderr信息重定向到文件里面
- &> 把所有的输出都同事重定向到文件

\>> 追加

- \>> 把标准的数据追加到文件中
- 2>> 把错误输出追加到文件中
- &>> 把所有的输出都同时追加到文件里面

## 分文件输出

```SHELL
ls b bbbbb > info.log 2> error.log
```

## 合并输出

- &> 
- &>>
- command > info.log 2>&1
- command > info.log 2>>&1
- /dev/null 无线接收的无底洞
- （）多个合并

## tr 替换或者删除字符

```SHELL
tr 'a-z' 'A-Z' </etc/issue
[root@localhost jiangyi]#tr ab 12
ab
12
[root@localhost jiangyi]#tr abc 12  如果后面的位数不足的话，则用最后一位补齐
abc
122
ab
12
tr -d abc < issue > issue2 从定向不能为原来的文件，如果说为原来的文件，则文件情况
-t 用来截断
[root@localhost jiangyi]#tr -t abcd 12
abcd
12cd
cd
cd
-s 压缩 去重
[root@localhost jiangyi]#tr -s abc
abc
abc
aaabbbccccccccccccccccccccccccccccc
abc
-c 取反
[root@localhost jiangyi]#tr -sc abc
aaaaaaaaaaaaaabbbbbbbbbbbbbbbcccccccccccccc
aaaaaaaaaaaaaabbbbbbbbbbbbbbbcccccccccccccc
aaaaaaaaaaaaaaaaaabbbbbbbbbbbbbcccccccccccccccccdddddddddddddeeeeeeeeeeeeffffffffffffff
aaaaaaaaaaaaaaaaaabbbbbbbbbbbbbcccccccccccccccccdef
aaaaaaaaaaaaaaaaaaaaaaaaaaabbbbbbbbbbbbbbbbbbbbbbbbbbbbbbccccccccc1111111111111111222222222222333333333333
aaaaaaaaaaaaaaaaaaaaaaaaaaabbbbbbbbbbbbbbbbbbbbbbbbbbbbbbccccccccc123
[root@localhost jiangyi]#tr -dc abc
aaaaaaaaaaaaabbbbbbbbbbbbccccccccccccccccccdddddddddddddddwqweqweqwqeqwqwqwq
wqqqqqqqqqqqqqqqqqqqqqqqqq
ctrl+d结束
[root@localhost jiangyi]#tr -dc "abc\n"
adsada
aaa
sadasdcxzczx
aacc
asdadwq
aa
[root@localhost jiangyi]#seq 1 10 >f1
[root@localhost jiangyi]#tr -d "\n" <f1
[root@localhost jiangyi]tr "\n" " "<f1
[root@localhost jiangyi] tr " " "\n" <f2

```

## 禁止覆盖

```shell
set -C 禁止覆盖
set +C 允许覆盖
```



## 多行输入

```shell
[root@localhost jiangyi]#cat >f1 <<EOF
> 1
> 2
> 3
> 4
> 5
> 6
> 7
> 
> 8
> 9
> EOF
[root@localhost jiangyi]# cat > f4
asdas
sad
asd
ctrl+d结束 ctrl+c也可以
两者区别
第一种方式输出结束，文件才会产生
第二方式，回车一次就会写入文件
EOF 约定俗成

```

## 管道

使用“|”来连接多个命令

命令1|命令2|命令3|。。。。

- 将命令的stdout发送给命令2的stdin，将命令2的stdout命令发送给命令3的stdin。。。。。
- stderr默认是不能通过管道传递

```SHELL
[root@localhost jiangyi]#ls /dadadasda|tr -s "a-z" "A-Z"
ls: cannot access /dadadasda: No such file or directory
[root@localhost jiangyi]#ls|tr "a-z" "A-Z"


```

## 查看当前的登录的用户信息

```SHELL
whoami  获取登录的用户
[jiangyi@localhost ~]$who am i
jiangyi（用户）  pts/4（登录的终端）        2019-07-31 08:27（登录的时间） (192.168.182.1（登录ip地址）)
w 可以查看当前登录的所有用户执行的命令


```



# 文件权限

## chown change owner

```shell
Usage: chown [OPTION]... [OWNER][:[GROUP]] FILE...
  or:  chown [OPTION]... --reference=RFILE FILE...
chown jiangyi d  修改属主
chown jiangyi:jiangyi d 修改属主和属组
chown root.root d
chown :jangyi d 只修改属组信息
chown -R jiangyi a 递归修改目录下的所有文件
chown --reference=b f3 指定源文件

```

### chgrp

```shell
chgrp jiangyi b
chgrp --reference=b f3 指定源文件

```

## 权限

rwxr-xr-x 

三位为一组

属主   属组     其他

u         g          o

r read 可以读这个文件或者文件夹

w write 可以对这个文件或者文件夹有写的权限

x excut 执行的权限

文件

- r 可以查看
- w 可以修改内容
- x 可以直接执行

目录：

- r 可以使用ls查看 可以cd进去
- w  可以在其中创建文件或者目录，可以删除目录中的文件或者是文件夹
- x 可以cd，如果没有x权限的话，w权限不会生效，r权限可以查看有哪些文件

## chmod

- 可以直接用+-来操作

  - 可以用[u|g|o]+ - = r w x
  - 可以什么都不写，表示全部 +-

- 还可以用数字表示

  \---

  r--    100 4

  -w- 010   2

  --x 001   1

  r:4

  w:2

  x:1

  r-xr-x--- 

- 建议：

  - 不要给文件或者文件夹设置成777权限
  - 不要给文件或者文件夹设置成777权限
  - 不要给文件或者文件夹设置成777权限

## 特殊权限

```shell
chattr +i 不能删除、改名、不能修改内容
chattr +a 只能追加，不能删除，不能改名
lsattr 查看属性

```

# 文本处理工具

## cat

```shell
Usage: cat [OPTION]... [FILE]...
-E 显示行的结束符号$
-n 显示每一行的行号
-b 给非空行编号
-s 折叠空行为一行

```

## tac 倒叙显示文件内容

## less 分屏显示

- 可以分屏显示
- 空格一屏  回车一行
- /来搜索
- n 向下搜索 N 向上搜索
- q来退出

## more

- 可以分屏显示
- 空格一屏 回车一行
- -d 显示翻页和推出信息
- 输出自己退出

## head  显示前多少行 默认是10行

- -# 显示前多少行的数据

## tail 显示后面多少行 默认是后10行

- ​	-# 显示后多少行的数据
- -f 追踪显示文件新加入的内容，常用于日志的跟踪
- tailf 相当于tail -f 命令，

## cut 抽取文件

```SHELL
-d 用来指定切割符号
-f 执行显示哪一个数据
# 显示指定的数据
#，#，#，# 离散数据
#-# 连续数据
cut -d: -f3 passwd 
cut -d: -f1,3-7 passwd 
cut -d: -f3,4,5,6 passwd
cut -d: -f3-6 passwd 
-c 按照字符切割
cut -c2-5 passwd

```

## paste 合并文件

```shell
-d 用来指定合并的符号，默认的是tab
-s 把所有的行合并成一行显示

```

# 分析文本的工具

## wc word count

```SHELL
[root@localhost jiangyi]#wc passwd 
  44   87 2301 passwd
 行数   单词个数  字节数  文件
 -l 统计行的个数
 -w 统计单词的个数
 -c 统计字节的个数
 -m 统计字符的个数
 -L 显示最长一行的长度

```

## sort 排序

```SHELL
默认按照字母
-n 按照数字来排序
-r 按照倒叙来排序
-R 随机排序
sort -t: -nk4 passwd 切割以后在排序
-t 指定切割符号
-k 指定按照第几行排序

```

## uniq 删除重复的行

```SHELL
-c 显示重复出现的次数
-d 只显示重复的行
-u 只显示不重复的行
连续且完全一样的才是重复
ss -tnp|cut -d: -f2|tr -s " "|cut -d" " -f2|sort -n|uniq -c

```

## diff 对比两个文件

```SHELL
[root@localhost jiangyi]#echo "abc" >b
[root@localhost jiangyi]#echo "abcd" >d
[root@localhost jiangyi]#diff b d
1c1
< abc
---
> abcd
[root@localhost jiangyi]#echo "abcde" >b
[root@localhost jiangyi]#diff b d
1c1
< abcde
---
> abcd
[root@localhost jiangyi]#echo "abcde" >> b
[root@localhost jiangyi]#diff b d
1,2c1
< abcde
< abcde
---
> abcd
[root@localhost jiangyi]#echo "abcd" >> b
[root@localhost jiangyi]#diff b d
1,2d0
< abcde
< abcde

```



### 打开文件

vim  opttion file

-e直接进入扩展命令行模式



### 关闭文件

 扩展命令行模式 

- ：q 退出
- ：q！ 强制退出
- ：wq 保存退出
- ：wq！ 强制保存退出
- ：x 保存退出

命令模式

- ZZ保存退出
- ZQ  不保存退出

扩展命令行模式

- ： 进入扩展命令行模式     或者叫末行模式
- rfile   读入一个文件在光标的下一行插入
- w + file 将当前的文件另存为另外一个文件
- ！+ 命令 得到执行结果，在按回车返回刚才执行的文件r
- r！命令   在光标的下一行输出命令执行结果

光标的移动

- 字符键的跳转

  - h  l j k   左右上下移动
  - #

- 单词间跳转

  - w下一个单词的首字母
  - e 当前或者下一个单词的词尾    （除了下划线，都认为是单词的分隔符）
  - b 当前单词或上一个单词的

- 当前页跳转

  - H 页首
  - M中间
  - L页底
  - zt 将光标所在的行，移动到屏幕的顶端
  - zb  将光标所在的行，移动到屏幕的底部
  - zz  将光标所在的行 ，移动到屏幕中间位置

- 行首行尾跳转

  - 0 跳转到行首
  - ^ 跳转至行首的第一个非空白字符
  - $ 跳转到行尾

- 行间移动

  - gg  1G  回到第一行
  - G 跳转到最后一行
  - ：+ 数字  扩展命令模式下跳转到数字行
  - 数字+G  跳转到数字行

- 段落间跳转

  - }下一段
  - { 上一段

- 翻屏操作

  - ctrl +f 向文件尾部翻一屏
  - ctrl+b 向文件首部翻一屏
  - ctrl+d 向文件尾部翻半屏
  - ctrl+u 向文件首部翻半屏

- 命令模式操作

  - 字符编辑

    - x删除光标处的字符
    - 数字+x 删除数字个字符
    - xp 交换光标所在处的字符与后面的字符
    - p 贴贴到光标所在位置
    - ~ 大小写转换
    - J删除当前行后的换行符

  - 替换

    - r 替换光标所在 处的字符
    - R 切换成替换模式  输入多少替换多少

  - 删除

    - d 删除，需要结合光标跳转字符
    - d0 删除到行首
    - d$  删除到行尾
    - d^ 删除光标到行首的第一个非空字符之间的字符
    - dw 删除一个单词
    - de 删除当前单词到词尾，或者下一个单词
    - db 删除当前单词到词首，或者上一个单词
    - dd  删除光标所在的行
    - 数字 + dd  删除数字行
    - dG 删除到文件的页尾
    - dgg  删除到文件 首部
    - D 相当于d$     

  - 复制

    - y 复制，结合光标跳转字符
    - y$
    - y0
    - y^
    - ye
    - yw
    - yb
    - \#command
    - yy 复制行
    - \#yy 复制多行
    - Y 复制整行

  - 粘贴

    - p如果是整行，则粘贴到当前光标的下一行，否则粘贴到光标所在位置的后边
    - P如果是整行，则粘贴到当前光标的上一行，否则铁铁到光标处

  - 修改命令

    - c   修改之后直接切换到插入模式
    - cc 删除当前行，并且输入新内容
    - 数字 + cc  删除数字行，并切换到插入模式

  - 数字+i     插入的文字   +esc  粘贴 数字次 输入的文字

  - 撤销

    - u  撤下最近 的更改
    - 数字+u  撤销数字次更改
    - U   撤销贯标所在行的操作
    - ctrl +r  撤销   撤销
    - . 重复前一个操作
    - 数字+.  重复之前的动作数字次

  - 搜索，查找

    - / pattern   从当前光标所在位置向下查找
    - ？pattern  从当前光标所在位置向上查找
    - n 与命令同方向
    - N与命令反方向
    - 可以写正则表达式

  - 地址定界

    - ：start，end   开始结尾

    - 数字   具体到第数字行

    - 数字，数字   从左侧数字表示起始行，到右侧数字表示结尾行

    - m，+n   从m开始到m+n行结束

    - .代表当前行

    - $ 最后一行

    - $2   倒数第三行    $1 倒数第二行

    - %   全部

    - ：数字，/匹配的值/   从数字行开始，到第一次匹配到的行

    - ：/匹配的值/，/匹配的值/  从第一个匹配的值开始到第二次匹配的行

    - ：/匹配的值/，$   从匹配到的行到结尾

      后面可以跟的指令

      - d   删除
      - y   复制
      - w file :将范围内的行另存到指定的文件中
      - r file 在指定位置插入指定文件中的内容

  - 查找并替换

    - ：地址定位符s/要查找的内容/要替换的内容/装饰器     可以用正则表达式
      - 要查找的内容可以使用正则表达式
      - 要替换的内容不可以使用正则表达式，但是可以使用\1,\2等
      - 也可以使用&来代替前面的内容
      - 装饰器
        - i  忽略大小写
        - g 全部替换
        - gc  提前之前要确认
          - y 确认
        - /可以替换成别的符号
          - #
          - @

  ### 可视化模式

  - v  对于字符操作的
  - V 面向块
  - ctrl+v     面向块
  - 配合键盘移动键使用    配合 d删除 w另存为 y 复制 使用
  - 突出显示的文件可以删除，复制，变更，过滤，替换等等等

  ### 多文件操作

  打开多文件

  vim 文件名  文件名   文件名  ..

  切换文件  ：next     切换到下一个

  ​		：prev   切换到上一个

  ​		：last  到最后一个

  ​		：first  到第一个

  全部退出

  w   q   x  all

  保存之后再切换文件

  ### 使用多个窗口

  多个文件的切割

  - vim  -o   a文件  b文件  c文件.
    - 水平切割
  - vim  -O   a   b  c ...
    - ctrl+w  光标切换每个文件窗口
    - 垂直切割

  单个文件的切割

  - ctrl +w 放手之后按s  水平分割
  - ctrl + w  放手之后按 v  垂直分割
  - ctrl +w 按方向键  切换光标所在窗口
  - ctrl-w,q 取消相邻窗口
  - ctrl-w,o 取消全部窗口

### 定制化vim

- 配置文件：
  - 全局：/etc/vimrc
  - 个人： ~/.vimrc
- 扩展命令模式：  关闭文件在重进就失效了
  - set nu  加行号
  - set nonu  取消行号
  - set ic   搜索忽略大小写  取消 set noic
  - 自动缩进  setai，与上一行对齐
  - set noai   取消掉自动缩进    
  - set hls  搜索高亮
  - set no hls  取消搜索高亮
  - syntax on   校验语法高亮   
  - syntax off  关闭语法高亮
  - set cul  光标所在行加下划线
  - set nocul   取消
  - set all  查看可以设置的功能，获取帮助信息
  - q退出

### linuex和windows 文件格式区别

- 转换成windows  set fileformat =dos
- 转换成linux   set fileformat=unix

### vim帮助信息

：help

：help name  

 - vimtutor

# find

格式： find [OPTION] .... [查找路径] [查找条件] [处理动作]

查找路径：可以指定具体的路径，默认是当前路径

查找条件：用来指定文件查找的标准，可以是文件名、大小、权限、类型等等

处理动作：对符合条件的文件进行的操作，默认是直接输出到屏幕上

## 查找条件

### 按照名称来搜索：

- name ：

  ```SHELL
  find -name a 完全匹配
  find -name "a*" 所有的以a开头的文件或者文件夹
  find -name "a?" 所有以a开头后面为一个字母的文件或者文件夹
  find -name "a[ab]" 以a开头后面是a或者b的文件或者文件夹
  
  ```

- iname 忽略大小写

  ```SHELL
  find -iname a
  ```

### 按照搜索层级

- -maxdepth level 指定最大的搜索层数，指定的目录为第一层

  ```SHELL
  find -maxdepth 2 -name a
  ```

- -mindepth level 指定最小的搜索层数

  ```SHELL
  find -mindepth 2 -name a
  ```

### 按照文件的类型来查找

- -type type
  - f 文件
  - d 目录
  - l 链接
  - s socket套接字
  - b 块设备
  - c 字符设备文件
  - p 管道文件

```SHELL
find -type f -name a 搜索文件
find -type d -name a 搜索目录
find -type l -name a 搜索软链接
```

### 空文件和空目录

- -empty

  ```SHELL
  find -empty
  find -empty -type d
  ```

### 根据属组，属主来搜索

- -user username 查找属主是username的文件或者文件夹
- -group groupname 查找属组是groupname的文件或者文件夹
- -uid uid 查找uid为uid的文件或者文件夹
- -gid gid 查找gid为gid的文件或者文件夹
- -nouser 查找没有属主的文件或者文件夹
- -nogroup 查找没有属组的文件或者文件夹

```shell
find -user jiangyi
chmod :xiaofeng jiangyi
chown :xiaofeng jiangy
find -group xiaofenf
find -group xiaofeng
find -uid 1000 
find -gid 1000 
find -gid 1001
find  -nouser
find -nogroup 
```

### 组合条件

- 与 -a
- 或 -o
- 非 -not !
- 摩根定律
  - （非A）或(非B)=非（A且B）
  - （非A）且（非B）=非（A或B）

```SHELL
find -not -user wupeiqi -a -not -user xiaofeng -ls|wc -l
find -not \( -user wupeiqi -o -user xiaofeng \) -ls|wc -l
```

### 排除目录

- -path

```SHELL
find /etc/ -path /etc/ssh -name *_config

```

### 文件大小来搜索

- -size[+|-] unit 常用单位：k，M，G，c(byte)
  - #unit：(#-1,#] 不包括#-1，但是包括#
  - -#：[0,#-1]，从0到#-1
  - +#：(#,......) 不包括#

### 文件时间戳

- 以“天"为单位
  - atime：[+|-] day
    - time [#,#+1）包括#，但是不包括#+1
    - +time：[#+1,.....]
    - -time:[0,#)
  - mtime
  - ctime
- 以“分钟”为单位
  - -amin
  - -mmin
  - -cmin

### 根据权限来搜索

- -perm 权限

```SHELL
find -perm 644  -ls
find -perm 777  -ls

```

## 处理动作

- -print 把搜索到的结果直接打印到屏幕上，默认的
- -ls 相当于执行`ls -l` 命令
- -delete 删除查找的文件
- -fls filename 将查找结果写入文件中
- -ok command {} \；对查找的文件执行command命令，但是每一次都需要用户确认
- -exec command {} \; 对查找到的文件执行command命令，不需要用户确认
  - {} 表示查找到的文件
  - find 传递的时候 是一次性传递的

# xargs

- 由于好多的命令不支持管道，但是工作有需要用到，这个时候xargs就可以派上用场
- xargs 把一个命令的输出结果，一个一个的传递给后面要执行的命令
- 有些命令不支持太多的字符，也可以使用xargs来传递

```shell
[root@localhost d]#echo a{1..1000000}|xargs touch
[root@localhost d]#rm a{1..1000000}
-bash: /usr/bin/rm: Argument list too long
[root@localhost d]#ls a*|xargs rm -f 
-bash: /usr/bin/ls: Argument list too long
[root@localhost d]#ls |xargs rm -f 

```

一般情况下 find|xargs command

# grep

三剑客

- grep
- sed
- awk

grep：全局用正则表达式搜索，并且打印符合条件的行

grep [option] .... pattern [file]

## 参数

- --color=auto 将匹配到的文本添加颜色显示
- -v 取反，显示没有匹配到
- -i 忽略大小写
- -n 显示匹配到的行的行号
- -c 只显示匹配到的行的个数
- -o 只显示匹配到的字符
- -q 静默模式，不输出东西
- -A # 输出后#行
- -B # 输出前#行
- -C # 前后各输出#行
- -e 表示或者
- -E 扩展正则表达式
- -r 递归查找

```SHELL
 grep 'root' passwd 
 grep -v "root" passwd 
 grep "root" passwd 
 grep -i "root" passwd 
 grep -n "root" passwd 
 grep -ni "root" passwd 
 grep -ci "root" passwd 
 grep -i "root" passwd 
 grep -o "root" passwd 
 grep -oi "root" passwd 
 grep -q "root" passwd 
 grep -q "qwertyuip;qwertyuo" passwd 
 echo $?
 grep -q "root" passwd 
 echo $?
 grep -nA 2 "root" passwd 
 grep -nB 2 "root" passwd 
 grep -nC 2 "root" passwd 
 grep -e "root" -e "mail" passwd 
 grep -r root /etc/ 

```

## 正则表达式

- 字符匹配
  - . 匹配任意单个字符
  - [abc] 匹配执行范围内的任意单个字符  [0-9]
  - [^abc] 取反
  - [:alnum:] 数字大小写 [a-zA-Z0-9] 
  - [:alpha:] 大小写字母 [a-zA-z]
  - [:lower:] 小写字母 [a-z]
  - [:upper:] 大写字母 [A-Z]
  - [:digit:] 数字 [0-9]
  - [:punct:] 标点符号
- 匹配次数
  - \* 0次或者多次，是贪婪匹配
  - \？0次或者一次
  - \\+ 最少一次
  - \\{n\\} 匹配n次
  - \\{m,n\\} 最少m次，最多n次
  - \\{,n\\} 最多n次
  - \\{m,\\} 最少m次
- 位置锚定
  - ^ 行首锚定
  - $ 结尾
  - ^$ 空行
- 向后引用
  - \1:表示前面第一个括号内匹配之后产生的字符，在\1的位置要在出现一次
  - \2:

## egrep

egrep =grep -E

支持扩展正则表达式，与标准增长表达式的区别就是不需要转义

## 压缩

## gzip

Usage: gzip [OPTION]... [FILE]...

```SHELL
gzip passwd 压缩文件 默认会删除文件
gunzip pass.gz 解压文件，默认也会删除文件
gzip -d passwd.gz 解压文件
-c 保留原来的文件
gzip -c passwd > passwd.gz 压缩
gzip -c -d passwd.gz > passwd 解压
-# 1-9 指定压缩比，值越大压缩比例越大 默认是9
zcat 查看压缩包内的文件
zcat passwd.gz > passwd

```

## bzip2

```SHELL
-k 保留原文件
-d 解压
bunzip2 解压
-# 1-9 默认的是9
bzcat 查看压缩包的文件

```

## xz

```shell
-k 保留源文件
-d 解压
unxz 解压
-# 1-9 默认的是9
xzcat 查看压缩包内的文件

```

## tar

```shell
tar cvf a.tar b c
c  创建
v 显示过程
f 指定文件
r 追加
x 解压
-C 指定解压位置
j 使用bzip2来压缩
z 使用gzip来压缩
J 使用xz来压缩
--exclude 排除
tar cvf a.tar b c
tar -r -f a.tar d
tar xf a.tar -C /opt
tar jcvf a.tar.bz b c d
tar zcvf a.tar.gz b c d
tar Jcvf a.tar.xz b c d

tar zcf etc.tar.gz --exclude=/etc/yum.repos.d --exclude=yum.conf /etc/

```

## 分卷压缩

```shell
split -b size file -d tarfile 
-b  指定每一个分卷的大小
-d 指定数字 默认是字母
-a 指定后缀个数
合并：
cat tarfile* > file.tar.gz
dd if=/dev/zero of=b bs=10M count=2
split -b 5M b b.tar.gz
split -b 5M b -d  b.tar.gz
split -b 5M b -d -a 3 b.tar.gz

```

# 用户和用户组

- 分类

  - 超级管理员 root  uid 0

  - 普通用户

    - 系统用户： 一般情况下用来启动服务或者运行进程，一般情况下系统用户是不可以登陆

      uid 1-999（centos7）1-499（centos6）

    - 可登陆用户：可以登陆系统的用户

      uid 1000-65535（centos7） 500-65535（centos6）

## useradd

只能用root账号来创建用户

```shell
-d 指定用户的家目录，不能创建在/tmp，默认用户的家目录不需要手动创建
-g 组信息   主组有且只能有一个
-G 指定附加组 可以有多个
-M 不创建家目录
-N 不创建组，默认继承至user组
-r 创建一个系统用户，id从1000依次递减
-s 登录以后使用的shell /sbin/nologin 可以登录看到提示，但是会立马被踢掉
-u 指定uid
/etc/default/useradd 默认的配置文件
-D 显示默认配置
useradd  -D -s /sbin/nologin 修改默认的登录后shell
useradd  -D -b /opt/ 修改默认的家目录
useradd  -D -g 3000 修改默认的组
```

### +登录用户出现‘’-bash-4.2$‘’的问题解决

2018年11月26日 11:25:57 灬紫荆灬 阅读数 3053
今天在linux服务器上创建的用户，登录后发现此用户的CRT的终端提示符显示的是-bash-4.2# 而不是user@主机名 + 路径的显示方式，以往一直用的脚本也不能执行起来；
原因是在用useradd添加普通用户时，有时会丢失家目录下的环境变量文件，丢失文件如下：
1、.bash_profile
2、.bashrc
以上这些文件是每个用户都必备的文件。
此时可以使用以下命令从主默认文件/etc/skel/下重新拷贝一份配置信息到此用户家目录下

cp /etc/skel/.bashrc  /home/user/
cp /etc/skel/.bash_profile   /home/user

## passwd

```shell
passwd [options] username 用来设置密码
-d 删除用户的密码 不能登录
-l 锁定用户
-u 解锁用户
-e 强制用户下次登录的时候修改密码
-x maxdays 密码的最长有效期
-n mindays 密码的最短有效期 
-w wandays 提前多长时间开始警告用户
-i days 密码过期多长时间以后账户被禁用
--stdin 从标准输入读入数据 echo "password" |passwd --stdin username
```

## chage 

交互式的修改密码的策略

```SHELL
-d 将密码修改时间设置为执行的时间
-E 设置用户的过期时间
-I 密码过期多长会时间以后账户被禁用
-l 显示密码的策略
-m 密码的最短使用期限
-M 密码的最长使用期限
-W 密码过期的警告天数
```

## passwd文件详解

- 用户名
- 密码占位符（x）
- 用户的id
- 组id
- 描述信息
- 家目录
- 登录后shell

## usermode

```shell
-L锁定
-U 解锁
-d 新的家目录不会自己创建，要想创建要使用-m选项
usermod -md /usr/local/alex alexdsb
-g 修改主组
-G 修改附加组
-a 追加附加组
usermod -a -G root alexdsb1
-l 改名
-s 修改登录后的shell 
-u 修改uid
```

## userdel

删除用户

```SHELL
-r  删除用户的时候删除用户的家目录
```

## shadow 文件格式

- 用户名
- 密码：一般情况是sha512加密 $加密方式$盐$加密之后的字符串
- 从1970年1月1日到密码最近一次被修改的时间
- 密码的最少使用期限
- 密码的最大使用期限
- 密码过期多长时间提示用（默认是7天）
- 密码过期多长时间之后被锁定
- 从1970年1月1日算起，多少天之后账户失效

## 切换用户

- su

  - su username 切换用户，切换用户要输入切换到用户密码
  - su - username 完全切换，会切换用户的目录还会切换用户的环境变量
  - root 切换到别的用户不需要输入密码

- su [-] username -c "command"  切换用户执行命令后再退回

- sudo command

  ```SHELL
  用root用户修改/etc/sudoers文件
  加上
  xiaofeng ALL=(ALL（命令）)      NOPASSWD（不需要输入密码）: ALL
  表示一个组
  %wheel  ALL=(ALL)       ALL
  ```

## 用户组

- 超级用户组  root 0
- 普通用户组
  - 系统组  gid 1-999（centos7）1-499（centos6）
  - 可登陆用户组 gid 1000-65535（centos7） 500-65535（centos6）

## groupadd

```SHELL
-g 用来指定gid
-r 用来指定系统组
```

## group文件格式

- 组名g
- 密码占位符
- gid
- 以当前组为附加组的用户

## groupmod 

```shell
-g 修改gid
-n 修改组的名称
```

## groupdel

删除用户组

## 登陆远程机器

两种认证方式：

- 用户名+密码
- 用户名+key

使用key登陆

```shell
ssh-keygen 生成key
公钥
私钥
非对称加密
ssh-copy-id 复制key到远程机器
公钥加密私钥解密

```



# 磁盘

mount 用来查看挂载信息

df 查看磁盘占用

​	-h 显示人类可读的信息

du 显示的目录的占用空间

​	-h 显示人类易读的信息

​	-s 显示的是目录本身

du -sh / 显示根的占用情况

du -sh /* 显示根下的每一个目录的占用情况

dd 复制文件生成文件

```shell
dd if=/dev/zero of=/dev/null bs=10M count=2
if 从哪复制     of 复制到拿   bs 一次复制文件的大小
if input file  
of  output file
bs block size 只能用整数，单位可以是K、M、G、T
count 次数

```

# RAID

## raid0（安装系统）

- 读写速度提升
- 可用空间 N*个数
- 没有容错能力
- 最少需要2块磁盘

## raid1（存放数据）

- 度的能力提升，写的性能稍微有点下架
- 可用空间N
- 有容错能力
- 最少要两块

## raid 5（目前比较流行）

- 读写性能提升
- 可用空间N*（个数-1）
- 有容错能力
- 最多可以坏1块（同时）
- 最少要3块

## raid6

- 读写性能有提高
- 可用空间N*（个数-2）
- 有容错能力
- 最多可以坏2块（同时）
- 最少需要4块

## raid10（土豪）

先做raid1 在做raid0

- 读写性能有提升
- 可用空间N*个数/2
- 有容错能力：一个组里面只能坏一块
- 最少需要4块

## raid01

先做raid0，在做raid1

# 包管理工具

介绍

windows exe

redhat rpm

rpm redhat package manager

yum 自己解决依赖关系

- 包
  - 安装包  yum install 
  - 清除缓存  yum clean
  - 列出所有的包 yum list
  - 更新包 yum update
  - 搜索  yum search
  - 详细信息 yum info
  - 列出yum仓库信息 yum repolist
  - 重新安装 yum reinstall
  - 卸载包 yum remove
  - 检查的依赖关系 yum deplist
  - 重建缓存 yum makecache
  - 搜索指定的命令由那个包生成 yum provides 
- 包组
  - 列出包组 yum group list
  - 安装包组 yum group install
  - 查看包组信息 yum group info
  - 卸载包的信息 yum group remove

rpm 命令

```shell
rpm -q package 检查这个包是否安装
-a 列出所有已经安装的包
-f 查询文件由那个包生成
rpm -qf /etc/redis.conf 
-l  查询包生成的文件
rpm -ql redis
-i 查询包的信息
rpm -qi redis
-c 查找包生成的配置文件
rpm -qc redis

```

包的命名规范	

```shell
python-2.7.5-80.el7_6.x86_64
name-version(大版本.小版本.修订版)-制作者的修订次数.应用系统.架构
noarch 不区分架构
架构
	x86_64
	Amd64
	i386,i486,i586,i686
	ppc(powerpc)

```

rpm 卸载包

```shell
rpm -e Package 卸载

```

## yum 参考的配置文件

位置:/etc/yum.repos.d/

后缀：repo

```shell
[epel] #名字                                                                                                           
name=Extra Packages for Enterprise Linux 7 - $basearch #描述信息
baseurl=http://mirrors.aliyun.com/epel/7/$basearch #仓库的地址 可以是http:// https:// ftp:// file://(本地)
failovermethod=priority # 设置访问规则
enabled=1  #是否禁用 0表示禁用 1表示启用
gpgcheck=0  # 要不要检查key，1表示检查 0表示不检查
gpgkey=file:///etc/pki/rpm-gpg/RPM-GPG-KEY-EPEL-7 
$release 系统版本
$basearch 架构

```

yum 选项

-y yes

-q 静默模式

## yum源的url

- 阿里云
- 网易
- 华为云
- 搜狐
- 腾讯云 <https://mirrors.cloud.tencent.com/>
- 各大高校



## 编译

- 优点：可以自定义功能
- 缺点：安装比较耗时



```shell
yum install zlib-devel   下载依赖包
（zlib-devel包中包含开发使用zlib压缩和解压缩库的程序所需的头文件和库。许多第三方软件包也使用它）
wget https://www.python.org/ftp/python/3.6.8/Python-3.6.8.tar.xz
tar xf Python-3.6.8.tar.xz
cd Python-3.6.8
./configure --prefix=/opt/python36  检查环节预处理，编译makefile文件
make 释放makefile文件
make install 安装

```

错误整理

```shell
configure: error: in `/root/test/Python-3.6.8':
configure: error: no acceptable C compiler found in $PATH
yum install gcc

```

添加环节变量

```shell
vim /etc/profile.d/python.sh
PATH=$PATH:/opt/python36/bin

```

