#1.简述解释性语言和编译型语言的区别？
'''
解释性语言   相当于解释器一句一句给你解释实时翻译
编译性语言   写完代码 交给编译器 编译器整合成一个文件  在交给硬件
'''

#2列举你了解的Python的数据类型？
# list,str,int,tuple,bool,

#写代码，有如下列表，按照要求实现每一个功能。
#
# ```python
# li = ["alex", "WuSir", "ritian", "barry", "wenzhou"]
# ```
#
# - 计算列表的长度并输出
# - 请通过步长获取索引为偶数的所有值，并打印出获取后的列表
# - 列表中追加元素"seven",并输出添加后的列表
# - 请在列表的第1个位置插入元素"Tony",并输出添加后的列表
# - 请修改列表第2个位置的元素为"Kelly",并输出修改后的列表
# - 请将列表的第3个位置的值改成 "太白"，并输出修改后的列表
# - 请将列表 l2=[1,"a",3,4,"heart"] 的每一个元素追加到列表li中，并输出添加后的列表
# - 请将字符串 s = "qwert"的每一个元素添加到列表li中，一行代码实现，不允许循环添加。
# - 请删除列表中的元素"ritian",并输出添加后的列表
# - 请删除列表中的第2个元素，并输出删除元素后的列表
# - 请删除列表中的第2至第4个元素，并输出删除元素后的列表
li = ["alex", "WuSir", "ritian", "barry", "wenzhou"]
'''

a = len(li)
print (a)
'''
'''
a = li[::2]
print (a)
'''
'''
li.append("seven")
print(li)
'''
'''
li.insert(0,"Tony")
print(li)
'''
'''
li[1] = "Kelly"
print(li)
'''
'''
li[2] = "太白"
print(li)
'''
'''
l2=[1,"a",3,4,"heart"]
for item in l2:
    li.append(item)
print(li)
'''
'''
s = "qwert"
s = s[::-1]
print(s)
'''


'''
li.remove("ritian")
print(li)
'''
'''
li.pop(1)
print(li)
'''
'''
for i in range(3):
    li.pop(1)
print(li)
'''

#请用三种方法实现字符串反转 name = "小黑半夜三点在被窝玩愤怒的小鸟"（步长、while、for）
name = "小黑半夜三点在被窝玩愤怒的小鸟"
# new_name = name[::-1]
# print(new_name)
'''
b = len(name)
c = ''
l = []
count = -1
for i in name:
    l.append(i)
for r in range(b):
    c += l[count]
    count -= 1
print(c)
'''

'''
count = -1
c = ''
while count >= -len(name):
    c += name[count]
    count -= 1
print(c)
'''
#写代码，有如下列表，利用切片实现每一个功能

# ```python
# li = [1, 3, 2, "a", 4, "b", 5,"c"]
# ```
#
# - 通过对li列表的切片形成新的列表 [1,3,2]
# - 通过对li列表的切片形成新的列表 ["a",4,"b"]
# - 通过对li列表的切片形成新的列表  [1,2,4,5]
# - 通过对li列表的切片形成新的列表 [3,"a","b"]
# - 通过对li列表的切片形成新的列表 [3,"a","b","c"]
# - 通过对li列表的切片形成新的列表  ["c"]
# - 通过对li列表的切片形成新的列表 ["b","a",3]
# - 通过对li列表的切片形成新的列表 ["b","a",3]
li = [1, 3, 2, "a", 4, "b", 5,"c"]
'''
new_list = li[:3]
print(new_list)
'''
'''
new_list = li[3:6]
print(new_list)
'''
'''
new_list = li[::2]
print(new_list)
'''
'''
new_list = li[1:-1:2]
print(new_list)
'''
'''
new_list = li[1::2]
print(new_list)
'''
'''
new_list = li[-1]
print(new_list)
'''
'''

new_list = li[-3::-2]
print(new_list)
'''
#请用代码实现循环输出元素和值：users = ["武沛齐","景女神","肖大侠"] ，如：
# 0 武沛齐
# 1 景女神
# 2 肖大侠
'''
users = ["武沛齐","景女神","肖大侠"]
for i in range (len(users)):
    print(i,users[i])
'''
#lis = [2, 3, "k", ["qwe", 20, ["k1", ["tt", 3, "1"]], 89], "ab", "adv"]
# - 将列表lis中的"k"变成大写，并打印列表。
# - 将列表中的数字3变成字符串"100"
# - 将列表中的字符串"tt"变成数字 101
# - 在 "qwe"前面插入字符串："火车头"

lis = [2, 3, "k", ["qwe", 20, ["k1", ["tt", 3, "1"]], 89], "ab", "adv"]
'''
lis[2] = 'K'
print(lis)
'''
'''
lis[1] = '100'
lis[3][2][1][1] = '100'
print(lis)
'''
'''
lis[3][2][1][0] = 101
print(lis)
'''
'''
lis[3].insert(0,'火车头')
print(lis)
'''
#写代码实现以下功能
# 如有变量 googs = ['汽车','飞机','火箭'] 提示用户可供选择的商品：
# 0,汽车
# 1,飞机
# 2,火箭
# 用户输入索引后，将指定商品的内容拼接打印，如：用户输入0，则打印 您选择的商品是汽车。
'''
googs = ['汽车','飞机','火箭']
for item in range(len(googs)):
    print(item,googs[item])
a = int(input('输入选择：'))
print('您选择的商品是：%s'%(googs[a]))
'''
#请用代码实现
#li = "alex"
# 利用下划线将列表的每一个元素拼接成字符串"a_l_e_x"
'''
li = "alex"

new_li = '_'.join(li)
print(new_li)
'''
#利用for循环和range找出 0 ~ 100 以内所有的偶数，并追加到一个列表
'''
l = []
for i in range(100):
    if i%2 ==0 :
        l.append(i)
print(l)
'''

# 利用for循环和range 找出 0 ~ 50 以内能被3整除的数，并追加到一个列表。
'''
l = []
for i in range(50):
    if i %3 == 0:
        l.append(i)
print(l)
'''

#利用for循环和range 找出 0 ~ 50 以内能被3整除的数，并插入到列表的第0个索引位置，最终结果如下：
# [48,45,42...]
'''
l = []
for i in range(50):
    if i %3 == 0:
        l.insert(0,i)
print(l)
'''

# 查找列表li中的元素，移除每个元素的空格，并找出以"a"开头，并添加到一个新列表中,最后循环打印这个新列表。
'''
li = ["TaiBai ", "alexC", "AbC ", "egon", " riTiAn", "WuSir", "  aqc"]
l = []
for item in li:
    item.strip()
    if item.startswith('a'):
        l.append(item)
print(item)
'''

#判断是否可以实现，如果可以请写代码实现。
# li = ["alex",[11,22,(88,99,100,),33] "WuSir", ("ritian", "barry",), "wenzhou"]
# 请将 "WuSir" 修改成 "武沛齐"
# - 请将 ("ritian", "barry",) 修改为 ['日天','日地']
# - 请将 88 修改为 87
# - 请将 "wenzhou" 删除，然后再在列表第0个索引位置插入 "文周"
li = ["alex",[11,22,(88,99,100,),33] ,"WuSir", ("ritian", "barry",), "wenzhou"]
'''
li[2] = '武沛齐'
'''
'''
li[3] = ['日天','日地']
print(li)
'''
'''
不能修改
'''
'''

li.pop()
li.insert(0,"文周")
print(li)
'''
