# 请将列表中的每个元素通过 "_" 链接起来。
''''
'''
'''
users = ['李少奇','李启航','渣渣辉']
a = '_'.join(users)
print(a)
'''
#请将列表中的每个元素通过 "_" 链接起来
'''
users = ['李少奇','李启航',666,'渣渣辉']
users[2] = '666'
a = '_'.join(users)
print(a)
'''
# 请将元组 v1 = (11,22,33) 中的所有元素追加到列表 v2 = [44,55,66] 中
'''
v1 = (11,22,33)
v2 = [44,55,66]
v2.extend(v1)
print(v2)
'''
# 请将元组 v1 = (11,22,33,44,55,66,77,88,99) 中的所有偶数索引位置的元素 追加到列表 v2 = [44,55,66] 中。
'''
v1 = (11,22,33,44,55,66,77,88,99)
v2 = [44,55,66]
v3 = v1[::2]
v2.extend(v3)
print(v2)
'''
# 将字典的键和值分别追加到 key_list 和 value_list 两个列表中，如：
'''
key_list = []
value_list = []
info = {'k1':'v1','k2':'v2','k3':'v3'}
for key in info:
    key_list.append(key)
for vale in info.values():
    value_list.append(vale)
print(key_list,value_list)
'''
# 字典dic = {'k1': "v1", "k2": "v2", "k3": [11,22,33]}
# 请循环输出所有的key
# b. 请循环输出所有的value
# c. 请循环输出所有的key和value
# d. 请在字典中添加一个键值对，"k4": "v4"，输出添加后的字典
# e. 请在修改字典中 "k1" 对应的值为 "alex"，输出修改后的字典
# f. 请在k3对应的值中追加一个元素 44，输出修改后的字典
# g. 请在k3对应的值的第 1 个位置插入个元素 18，输出修改后的字典
dic = {'k1': "v1", "k2": "v2", "k3": [11,22,33]}
'''
for key in dic:
    print(key)
'''
'''
for vale in dic.values():
    print(vale)
'''
'''
for key,vale in dic.items():
    print(key,vale)
'''
'''
dic["k4"] = "v4"
print(dic)
'''
'''
dic["k1"] = "alex"
print(dic)
'''
'''
dic["k3"].append(44)
print(dic)
'''
'''
dic["k3"].insert(0,18)
print(dic)
'''
# 请循环打印k2对应的值中的每个元素
'''
info = {
    'k1':'v1',
    'k2':[('alex'),('wupeiqi'),('oldboy')],
}
count = 0
while count < 3:
    print(info['k2'][count])
    count += 1
'''
# 有字符串"k: 1|k1:2|k2:3 |k3 :4" 处理成字典 {'k':1,'k1':2....}
'''
a = "k: 1|k1:2|k2:3 |k3 :4"
b = a.split('|')
dic = {}
for i in b:
    key,val = i.split(':')
    # print(key,val)
    key = key.strip()
    val = int(val)
    dic[key] = val
print(dic)
'''
# 有如下值 li= [11,22,33,44,55,66,77,88,99,90] ,将所有大于 66 的值保存至字典的第一个key对应的列表中，将小于 66 的值保存至第二个key对应的列表中。
'''
result = {'k1':[],'k2':[]}
li= [11,22,33,44,55,66,77,88,99,90]
for i in li:
    if i > 66:
        result['k1'].append(i)
    else:
        result['k2'].append(i)
print(result)
'''
# 输出商品列表，用户输入序号，显示用户选中的商品
"""
商品列表：
  goods = [
		{"name": "电脑", "price": 1999},
		{"name": "鼠标", "price": 10},
		{"name": "游艇", "price": 20},
		{"name": "美女", "price": 998}
	]
要求:
1：页面显示 序号 + 商品名称 + 商品价格，如：
      1 电脑 1999 
      2 鼠标 10
	  ...
2：用户输入选择的商品序号，然后打印商品名称及商品价格
3：如果用户输入的商品序号有误，则提示输入有误，并重新输入。
4：用户输入Q或者q，退出程序。
"""
'''
goods = [
		{"name": "电脑", "price": 1999},
		{"name": "鼠标", "price": 10},
		{"name": "游艇", "price": 20},
		{"name": "美女", "price": 998}
	]
for i in range(1,len(goods)+1):
    print(i,goods[i-1]["name"],goods[i-1]["price"])
while True:
    a = input('请选择商品号或输入Q退出：')
    if a.isdigit() :
        a = int(a)-1
        if a in range(len(goods)):
            print(goods[a]["name"],goods[a]["price"])
            break
        else:
            print('您输入有误，请重新输入')
            continue
    else:
        a = a.upper()
        if a == 'Q':
            print('退出程序')
            break
'''
#11
#   v = { 'users':9}