# 1. 数据类型

## 	1.列表

   1. 补充内容

      extend

      ```python
      - extend
          # 1. 有列表 users = ['李忠伟','堂开发']  people = ['李鹏','张思达']
                  users.extend(people) # users中增加
                  people.extend(users) # people中增加
          # 2. 有列表 users = ['李忠伟','堂开发']  people = ('李鹏','张思达')
                  users.extend(people) # users中增加
                  people.extend(users)  # 只有列表有extend功能，元组是没有的。
      - pop和del的区别
              li = ["alex", "WuSir", "ritian", "barry", "wenzhou"]
              del li[2] # 仅仅是删除
              
              deleted = li.pop(2) # 在列表中删除，并将删除的此数据赋值给deleted
              print(li)
              print(deleted)
      - 删除 li[1:8]
      ```

				2. 常见的类型转换

       				1. 字符串转数字
       				2. 数字转字符串
       				3. 列表转元祖
       				4. 元祖转列表
       				5. 只有 0 '' [] ()    转布尔值为flase

### 2. 字典

 1. 独有方法keys（），values（），items（）

 2. for i in dict.keys（）：   所有建

 3. for i in  dict  .values（）： 所有值

 4. for a,b in dict.items()：     建和值

    ### 共有功能

     	1. 索引
     	2. len
     	3. for
     	4. 修改
     	5. 删除