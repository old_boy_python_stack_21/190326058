



1. 猜数字，设定一个理想数字比如：66，让用户输入数字，如果比66大，则显示猜测的结果大了；如果比66小，则显示猜测的结果小了;只有等于66，显示猜测结果正确，然后退出循环。

   ```python
   while TRUE:
   	number = int(input('请输入理想数字：'))
   	if number == 66:
   		print('正确')
   		break
       elif number > 66:
           print('结果大了')
       else:
           print('结果小了')
   ```

   

2. 在上一题的基础，设置：给用户三次猜测机会，如果三次之内猜测对了，则显示猜测正确，退出循环，如果三次之内没有猜测正确，则自动退出循环，并显示‘大笨蛋’。

   ```python
   count = 1
   while count <= 3:
       number = int(input('请输入理想数字：'))
   	if number == 66:
   		print('正确')
           
   		break
       elif number > 66:
           print('结果大了')
       else:
           print('结果小了')
       count += 1
   else:
       
       print('大笨蛋')
       
   ```

   

3. 使用两种方法实现输出 1 2 3 4 5 6   8 9 10 。

   ```python
   count = 1
   while count <= 10:
   	if count ！= 7：
       	print(count)
       count += 1
   
   ```

   ```python
   count = 1
   while count <= 10:
   	if count == 7:
   		pass
   	else:
   		print(count)
   	count += 1
   ```

   

   ```python
   
   ```

   

4. 求1-100的所有数的和

   ```python
   sum = 0
   num = 1
   while num <= 100:
   	sum += num
   	num += 1
   print(sum)
   ```

   

5. 输出 1-100 内的所有奇数

   ```python
   number = 1
   while number <= 100:
   	if number % 2 != 0:
   		print(number)
   	number += 1
   ```

   

6. 输出 1-100 内的所有偶数

   ```python
   number = 1
   while number <= 100:
   	if number % 2 == 0:
   		print(number)
   	number += 1
   ```

   

   

7. 求1-2+3-4+5 ... 99的所有数的和

   ```python
   sum = 0
   number = 1
   while number < 100:
   	if number % 2 != 0:
   		sum += number
   	number += 1
   	else:
   		sum -= number
    print(sum)
    
   
   ```

   

   

8. ⽤户登陆（三次输错机会）且每次输错误时显示剩余错误次数（提示：使⽤字符串格式化）

   ```python
   
   count =  3
   while count > 0:
       name = input('输入你的账户：')
   	number = int(input('输入你的密码：'))
   	if name == 'alxe' and number == 123456:
       	print('登录成功')
       	break
        else :
           count -= 1
           print('您剩余的次数%d'%(count,))
           
       
   ```

    

9. 简述ASCII、Unicode、utf-8编码

   ascii  8位  支持数字字母符号

   Unicode  万国码  32位  支持所有的，还有剩余位置没用

   utf-8  万国码的压缩版     从左到右每8位全是0就舍去   汉字占3个字节

10. 简述位和字节的关系？

    8位 = 1字节

    

11. 猜年龄游戏 
    要求：允许用户最多尝试3次，3次都没猜对的话，就直接退出，如果猜对了，打印恭喜信息并退出

    ```python
    number = 66
    count = 3
    while count > 0:
    	num = int(input('请输入你猜的数字：'))
    	if num == number：
    		print('恭喜')
    		break
    	else：
    		print('不对')
    		count -= 1
    	
    ```

    

    

12. 猜年龄游戏升级版
    要求：允许用户最多尝试3次，每尝试3次后，如果还没猜对，就问用户是否还想继续玩，如果回答Y，就继续让其猜3次，以此往复，如果回答N，就退出程序，如何猜对了，就直接退出。

    ```python
    while True:
        number = 66
        count = 3
        while count > 0:
            num = int(input('请输入你猜的数字：'))
            if num == numbergvtfh
                print('恭喜')
                break
    
            else :
                print('不对')
                count -= 1
        if count != 0:
            break
        jixu = input('继续输入Y，退出输入N')
        if jixu == 'N':
            print('退出了')
            break
        else:
            pass
    
     	
     	
    ```

    

13. 判断下列逻辑语句的True,False
    - 1 > 1 or 3 < 4 or 4 > 5 and 2 > 1 and 9 > 8 or 7 < 6       ture
    - not 2 > 1 and 3 < 4 or 4 > 5 and 2 > 1 and 9 > 8 or 7 < 6         false

14. 求出下列逻辑语句的值。
    - 8 or 3 and 4 or 2 and 0 or 9 and 7       8
    - 0 or 2 and 3 and 4 or 6 and 0 or 3       4

15. 下列结果是什么？
    - 6 or 2 > 1                   6
    - 3 or 2 > 1	          3
    - 0 or 5 < 4                    flase
    - 5 < 4 or 3                     3
    - 2 > 1 or 6                   ture
    - 
    - 3 and 2 > 1                 true
    - 0 and 3 > 1                 0
    - 2 > 1 and 3                 3
    - 
    - 3 > 1 and 0                 0
    - 3 > 1 and 2 or 2 < 3 and 3 and 4 or 3 > 2        3>2 or 2<4 or 3>2                2