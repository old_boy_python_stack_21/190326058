### 挖矿与linux系统病毒攻击

物理服务器的linux界面,默认有7个终端

```python
1.如果你发现云服务器被矿机攻击,有一个进程占用100%cpu资源
2.kill -9 杀死进程 
3.检查定时任务 
crontab -l  检查 
crontab -e   编辑定时任务文件,可以删除 定时任务

4.全局搜索病毒文件,删除它(注意恶意病毒,篡改了文件名,了解即可)
	find / -name  病毒文件    
	找到后 rm -rf 删除 	
5.如果删不掉,很有可能文件被加锁了
lsattr filename.txt     #检查文件权限

chattr -a  -i  filename.txt  去掉文件的锁,即可删除
```

