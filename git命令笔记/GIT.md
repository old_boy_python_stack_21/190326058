# GIT

### 1. 命令总结

git init   初始化

git config --global user   创建仓库

git config --global email

git add  文件

git add .

git commit -m '描述'



git log         commit的记录

git reflog      显示记录

git reset --hard       回滚



git status  查看当前状态



git remote  add origin  ‘url地址’     将来要提交的仓库地址



git push origin master   推送到远程仓库master分支

git pull origin master   将远程master分支的拉取到本地

git clone url   将远程代码都克隆到本地



git branch  查看分支

git  branch  dev  创建dev的分支

git  branch -d debug    删除debug分支

git checkout  dev  切换到dev这个分支上面

git merge dev   合并



个人开发的流程

分支 master  dev

开发时在dev上开发

写完新的功能  合并到master上

如果master分支上有BUG

那就在创建个debug分支

git branch debug

在这个分支上修改

git checkout  debug

修改完成递交到debug

在切换到master

git checkout master

git merge debug    将debug和mater和并

在切换回dev上，git chechout dev

git merge matster  将master合并到dev上

在干掉debug   git  branch -d debug   删除分支

注意  ： 合并分支有冲突，手动解决冲突



团队合作开发

分支 master  dev  然后每个人一个自己的分支star

每个人在自己的分支上操作

开发完功能  本地代码推送到自己的分支上

创建pull  request  合并到dev上

领导审核代码  接受合并

最后dev的代码合并到master