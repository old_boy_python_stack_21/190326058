# 1. 运算符补充

​	1.in      

​		

```
		a = '面对疾风吧'
		b = '疾风'
		c = b in a
		print(c)   #c为一个布尔值    
```



 	2. not in
 	3. 优先级：
      	1. not > and > or



# 2整形

 	1. 在python 2中  存在整形跟长整形  int 跟 long
 	2. 在python3中只有整形
 	3. 在python2中   除法只会取整
 	4. 在python3中可以得出小数部分

# 3. 布尔值

​	1.整形中的0代表   flase   

 	2. 字符串中的‘’      代表flase

# 4. 字符串

​	1.独有的方法

  1. a.upper()      将a字符串中字母变成大写

  2. a.slower()      将a字符串中字母变小写

  3. b = a.isdight()      判断a中是否有数字   返回值为布尔值

  4. a.strip()    去掉两边的空格

     a.rstrip()   去掉右边的空格

     a.lstrip()   去掉左边的空格

     5. a.replace('需要替换的'，'替换成的'，数字替换次数)           
     6. b = a.split('切割的标志'，切割的次数)    返回一个列表

2. 共有方法

   len(a)      计算a的长度

   1. 索引

      a[0]   代表a的第一个元素

      a[n]    代表第n+1个元素

      a[-1]  代表最后一个元素

2. 切片

   a = 'asjkdha'

   a[:3]   代表前3个元素   asj

   a[2:3]  表示坐标为2的   j

   a[3:]     表示kdha

   a[3:-1] 表示  kdh