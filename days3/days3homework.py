"""
"""
'''
days3homework
'''
# 有变量name = "aleX leNb " 完成如下操作：
# # #
# # # - 移除 name 变量对应的值两边的空格,并输出处理结果
# 答：
name = "aleX leNb "
new_name = name.strip()
print(new_name)

# 判断 name 变量是否以 "al" 开头,并输出结果（用切片）
# if "al" == name[:2]:
#     print('是')
# else:
#     print('否')

# 判断name变量是否以"Nb"结尾,并输出结果（用切片）
# if "Nb" == name[-2:]:
#     print('yes')
# else:
#     print('no')
# 将 name 变量对应的值中的 所有的"l" 替换为 "p",并输出结果
"""
new_name = name.replace("l","p")
print(new_name)
"""
#将name变量对应的值中的第一个"l"替换成"p",并输出结果
"""
new_name = name.replace("l","p",1)
print(new_name)
"""
#将 name 变量对应的值根据 所有的"l" 分割,并输出结果
"""
new_name = name.split("l")
print(new_name)
"""
#将name变量对应的值根据第一个"l"分割,并输出结果
"""
new_name = name.split("l",1)
print(new_name)
"""
#将 name 变量对应的值变大写,并输出结果
"""
new_name = name.upper()
print(new_name)
"""
#将 name 变量对应的值变小写,并输出结果
"""
new_name = name.lower()
print(new_name)
"""
#请输出 name 变量对应的值的第 2 个字符?
"""
print(name[1:2])
"""
#请输出 name 变量对应的值的前 3 个字符?
"""
print(name[:3])
"""
#请输出 name 变量对应的值的后 2 个字符?
"""
print(name[-2:])
"""
# 2有字符串s = "123a4b5c"
#
# - 通过对s切片形成新的字符串 "123"
# - 通过对s切片形成新的字符串 "a4b"
# - 通过对s切片形成字符串s5,s5 = "c"
# - 通过对s切片形成字符串s6,s6 = "ba2"

s =  "123a4b5c"
"""
new_s = s[:3]
print(new_s)
"""
"""
new_s = s[3:-2]
print(new_s)
"""
"""
s5 = s[-1:]
print(s5)
"""
"""
s1 = s[5:6]
s2 = s[3:4]
s4 = s[1:2]
s6 = s1+ s2 + s4
print(s6)
"""
#3使用while循环字符串 s="asdfer" 中每个元素
"""
s = "asdfer"
count = 0
while count < len(s):
    print(s[count])
    count += 1
"""
#使用while循环对s="321"进行循环，打印的内容依次是："倒计时3秒"，"倒计时2秒"，"倒计时1秒"，"出发！"。
"""
s = "321"
count = 0
while count < len(s):
    print('倒计时%s秒'%(s[count]))
    count += 1
"""
#实现一个整数加法计算器(两个数相加)：

#如：content = input("请输入内容:") 用户输入：5+9或5+ 9或5 + 9（含空白），然后进行分割转换最终进行整数的计算得到结果。
'''
content = input("请输入内容:")
content = content.strip()
new_content = content.split('+')
print(new_content)
a = int(new_content[0].strip())
b = int(new_content[1].strip())
result = a + b
print('结果为%d'%(result,))
'''
#计算用户输入的内容中有几个 h 字符？

#如：content = input("请输入内容：")   # 如fhdal234slfh98769fjdla
'''
number = 0
count = 0
content = input("请输入内容：")
while count < len(content):
    if content[count] == 'h':
        number += 1
    count += 1
print(number)
'''
#计算用户输入的内容中有几个 h 或 H  字符？

#如：content = input("请输入内容：")   # 如fhdal234slfH9H769fjdla
'''
content = input("请输入内容：")
new_content = content.upper()
count = 0
number = 0
while count < len(content):
    if new_content[count] == 'H':
        number += 1
    count += 1
print(number)
'''

#使用while循环分别正向和反向对字符串 message = "伤情最是晚凉天，憔悴厮人不堪言。" 进行打印。
'''
message = "伤情最是晚凉天，憔悴厮人不堪言。"
count = 0
while count < len(message):
    print(message[count])
    count += 1
number = len(message)
while number > 0:
    print(message[number-1])
    number -= 1
'''

#获取用户输入的内容中 前4个字符中 有几个 A ？

#如：content = input("请输入内容：")   # 如fAdal234slfH9H769fjdla
'''
content = input("请输入内容：")
new_count = content[:4]
number = 0
count = 0
while count < len(new_count):
    if new_count[count] == 'A':
        number += 1
    count += 1
print(number)
'''

#获取用户输入的内容，并计算前四位"l"出现几次,并输出结果。
'''
content = input("请输入内容：")
new_content = content[:4]
number = 0
count = 0
while count < len(new_content):
    if new_content[count] == "l":
        number += 1
    count += 1
print(number)
'''

# 获取用户两次输入的内容，并将所有的数据获取并进行相加，如：
# """
# 要求：
# 	将num1中的的所有数字找到并拼接起来：1232312
# 	将num1中的的所有数字找到并拼接起来：1218323
# 	然后将两个数字进行相加。
# """
#
# num1 = input("请输入：") # asdfd123sf2312
# num2 = input("请输入：") # a12dfd183sf23
# 请补充代码
'''
num1 = input("请输入：")
num2 = input("请输入：")
count = 0
a = ''
b = ''
while count < len(num1):
    if num1[count].isdigit():
        a += num1[count]
    count += 1
count = 0
while count < len(num2):
    if num2[count].isdigit():
        b += num2[count]
    count += 1
c = int(a) + int(b)
print(c)
'''
