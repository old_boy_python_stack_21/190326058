 aN# 写函数，检查获取传入列表或元组对象的所有奇数位索引对应的元素，并将其作为新列表返回。
'''

'''
'''
def get_li(l):
    v = l[1::2]
    return v
'''
# 写函数，判断用户传入的一个对象（字符串或列表或元组任意）长度是否大于5，并返回真假。
'''
def num_length(l):
    if len(l) > 5:
        return True
    else:
        return False
'''
# 写函数，接收两个数字参数，返回比较大的那个数字。
'''
def number_li(a,b):
    if a > b:
        return a
    else:
        return b
'''
# 写函数，函数接收四个参数分别是：姓名，性别，年龄，学历。用户通过输入这四个内容，
# 然后将这四个内容传入到函数中，
# 此函数接收到这四个内容，将内容根据"*"拼接起来并追加到一个student_msg文件中。
'''
def person_data(name,sex,age,education):     #education  学历
    lis = []
    lis.append(name)
    lis.append(sex)
    lis.append(age)
    lis.append(education)
    str_lis = '*'.join(lis)
    with open('student_msg','a',encoding= 'utf-8')as f1:
        f1.write(str_lis)
'''
# 写函数，在函数内部生成如下规则的列表 [1,1,2,3,5,8,13,21,34,55…]
# （斐波那契数列），并返回。
# 注意：函数可接收一个参数用于指定列表中元素最大不可以超过的范围。
'''
def get_lis(num):
    li = []
    count = 0
    number = 1
    while number < num:
        li.append(number)
        number += count
        count = number -count
    return li
a = get_lis(1000)
print(a)
'''
# 写函数，验证用户名在文件 data.txt 中是否存在，
# 如果存在则返回True，否则返回False。（函数有一个参数，用于接收用户输入的用户名）
# data.txt文件格式如下：
#
# 1|alex|123123
# 2|eric|rwerwe
# 3|wupeiqi|pppp
'''
def check(name):        #check  校验
    with open('data.txt','r',encoding='utf-') as f1:
        for line in f1:
            line = line.strip()
            li = line.split('|')
            if li[1] == name:
                return True
    return False

a = check('alexd')
if a == True:
    print('在里面')
else:
    print('不在')
'''

