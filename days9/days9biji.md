# days9biji

## 三元运算符  三目运算符

```python  
v = '结果1' if 条件 else 结果
#如果if 成立  v= 结果1  不成立 v= 结果

```

## 函数.

为什么要写函数？

没有函数体的约束，编程性质为面向过程编程，可读性差，可重用性差。

对于函数编程：

- 本质：将N行代码拿到别处并给他起个名字，以后通过名字就可以找到这段代码并执行。
- 场景：
  - 代码重复执行。
  - 代码量特别多超过一屏，可以选择通过函数进行代码的分割。

# 面向过程编程
user_input = input('请输入角色：')

if user_input == '管理员':
    import smtplib
    from email.mime.text import MIMEText
    from email.utils import formataddr

    msg = MIMEText('管理员，我想演男一号，你想怎么着都行。', 'plain', 'utf-8')
    msg['From'] = formataddr(["李邵奇", '15776556369@163.com'])
    msg['To'] = formataddr(["管理员", '344522251@qq.com'])
    msg['Subject'] = "情爱的导演"
    
    server = smtplib.SMTP("smtp.163.com", 25)
    server.login("15776556369@163.com", "qq1105400511")
    server.sendmail('15776556369@163.com', ['管理员', ], msg.as_string())
    server.quit()
elif user_input == '业务员':
    import smtplib
    from email.mime.text import MIMEText
    from email.utils import formataddr

    msg = MIMEText('业务员，我想演男一号，你想怎么着都行。', 'plain', 'utf-8')
    msg['From'] = formataddr(["李邵奇", '15776556369@163.com'])
    msg['To'] = formataddr(["业务员", '业务员'])
    msg['Subject'] = "情爱的导演"
    
    server = smtplib.SMTP("smtp.163.com", 25)
    server.login("15776556369@163.com", "qq1105400511")
    server.sendmail('15776556369@163.com', ['业务员', ], msg.as_string())
    server.quit()
elif user_input == '老板':
    import smtplib
    from email.mime.text import MIMEText
    from email.utils import formataddr

    msg = MIMEText('老板，我想演男一号，你想怎么着都行。', 'plain', 'utf-8')
    msg['From'] = formataddr(["李邵奇", '15776556369@163.com'])
    msg['To'] = formataddr(["老板", '老板邮箱'])
    msg['Subject'] = "情爱的导演"
    
    server = smtplib.SMTP("smtp.163.com", 25)
    server.login("15776556369@163.com", "qq1105400511")
    server.sendmail('15776556369@163.com', ['老板邮箱', ], msg.as_string())
    server.quit()
函数式编程

def send_email():
	import smtplib
    from email.mime.text import MIMEText
    from email.utils import formataddr

    msg = MIMEText('老板，我想演男一号，你想怎么着都行。', 'plain', 'utf-8')
    msg['From'] = formataddr(["李邵奇", '15776556369@163.com'])
    msg['To'] = formataddr(["老板", '老板邮箱'])
    msg['Subject'] = "情爱的导演"
    
    server = smtplib.SMTP("smtp.163.com", 25)
    server.login("15776556369@163.com", "qq1105400511")
    server.sendmail('15776556369@163.com', ['老板邮箱', ], msg.as_string())
    server.quit()


user_input = input('请输入角色：')

if user_input == '管理员':
    send_email()
elif user_input == '业务员':
    send_email()
elif user_input == '老板':
    send_email()

### 2. 函数的参数

- 位置参数，关键字参数 

- 位置参数只能出现在关键字参数之前

  - *args （位置参数）   以一个元祖的形式   

  - **kwargs  （关键字参数）  以赋值形式  或者  ’ 星星 ‘   字典形式  见例子

    ```python
    def func(**kwargs):
        print(kwargs)
    func(**{'k1':'v2','k2':'v2'}) # kwargs={'k1':'v2','k2':'v2'}
    
    func(k1=1,k2="alex") # kwargs={'k1': 1, 'k2': 'alex'}
    func(k1={'k1':99,'k2':777},k2="alex") # kwargs={'k1': {'k1': 99, 'k2': 777}, 'k2': 'alex'}
    func(**{'k1':'v2','k2':'v2'}) # kwargs={'k1':'v2','k2':'v2'}
    ```

    

### 3. 函数的作用域

函数外部  ：全局作用域

函数内部：局部作用域

作用域查找规则，先在自己里的找，找不到去上一层找，在找不到再去上上层找，知道找到全局作用域，还没有就报错。

###### 	3.1关键字

- global    将全局变量中的变量重新赋值
- nonlocal   将上一层作用域中的变量重新赋值

#### 4. 潜规则

##### 	 全局变量命名以后全部改为大写字母   方便查看