
def get_power(data):
    dat = {}
    for i in data:
        if i['powers__menu_id']:
            if dat.get(i['powers__menu_id']):
                dat[i['powers__menu_id']]['children'].append(
                    {'title': i['powers__name'], 'url': i['powers__url_title']})
            else:
                dat[i['powers__menu_id']] = {
                    'title': i['powers__menu__title'],
                    'icon': i['powers__menu__i_con'],
                    'children': [{
                        'title': i['powers__name'],
                        'url': i['powers__url_title']
                    }]
                }
    print('*'*100,dat,'*'*100)
    return dat

