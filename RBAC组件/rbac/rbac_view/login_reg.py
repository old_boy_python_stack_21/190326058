from django.shortcuts import render, redirect,reverse
from rbac.service.init_permission import init_permission

from liupiapp import models

def login(request):
    if request.method == 'POST':
        username = request.POST.get('username')
        password = request.POST.get('password')
        obj = models.UserProfile.objects.filter(username=username, password=password).first()
        request.session['user_obj'] = obj
        if not obj:
            return render(request, 'login.html', {'error': '用户名或密码错误'})
        # 登录成功
        # 权限和菜单信息的初始化
        init_permission(request, obj)

        return redirect(reverse('index'))

    return render(request, 'login.html')


def index(request):


    return render(request,'layout.html')