# RBAC

基于权限的控制

1. 如何实现权限控制

   url代表权限

   表结构：

   权限表

   - url  权限  url的地址  正则表达式  无^$
   - title  标题

   角色表：

   - name
   - permissions   多对多  关联权限表

   用户表

   - usename 
   - password
   - roles 多对多  关联角色

   角色和权限的关系表

   用户和角色的关系表

一级菜单：

表结构：

权限表

- url  权限  url的地址  正则表达式  无^$
- title  标题
- is_menu   是否是菜单
- icon  图标

二级菜单：

菜单表

- title
- icon

权限表

- url  权限  url的地址  正则表达式  无^$
- title  标题
- menu   外键关联菜单表     有menu_id 当前权限是二级菜单，没有表示是普通权限

二级菜单  对一级菜单排序

- title  
- icon
- weight  整形  排序依据   权重

非菜单权限归属：

权限表

​	- url  权限  url的地址  正则表达式  无^$

​	- title  标题	

​	- menu   外键关联菜单表     有menu_id 当前权		限是二级菜单，没有表示是普通权限

 - parent  外键  关联权限表  子关联 有parent_id    当前权限子权限   没有parent_id   当前权限是父权限



流程+技术点

 1. 中间件

     1. 获取当前访问的URL地址

     2. 白名单

        - re 
        - settings

        	3. request.current_id = None

        	4. request.breadcrumb_list = [{首页：url}]

        	5.   验证登录状态

        没有登录跳转到登录页面

    5. 免认证地址校验

    6. 权限的校验

       1. 从session中获取当前用户的权限信息
       2. 循环权限字典  正则匹配
          1. 匹配成功  记录menu_id   return

       3. 拒绝请求  没有权限

2. 登录的视图

   1. 验证用户名和密码   验证成功 获取到用户对象

   2. 权限信息和菜单信息的初始化

      1. 获取当前用户的权限信息
         - 跨表
         - 去空
         - 去重

      2. 构建数据结构  权限  菜单

         权限：

         - 简单权限控制  

           permission_lis = [ {'ur':url } ]

         - 非菜单权限的归属

           permission_list = [ { url   id    pid} ]

         - 路径导航

           permission_list = [ {url  id  pid} ]

         菜单：

         ​	动态生成一级菜单

         ​	menu_lis = [ {url  title  icon} ]

         ​	二级菜单

         ​	menu_dict  {

         ​		一级菜单的ID：{

         title

         icon

         url

         weight

         children：[{

         url    title

         }]

         }}

      3. 保存权限和菜单信息 ，登录状态到session中    json序列化   字典当中数字会变字符串
      4. 重定向到首页

   3. 模板
      - 母版和继承
      - 动态生成菜单
        - inclusion_tag
        - 有序字典
        - sorted
        - js    css

   ​					

   

   