from django.apps import AppConfig


class LiupiappConfig(AppConfig):
    name = 'liupiapp'
