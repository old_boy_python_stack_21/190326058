from django.template import Library
from django.urls import reverse
from django.http.request import QueryDict
register = Library()
@register.simple_tag
def reverse_url(request, name, *args, **kwargs):
    # 获取当前的访问的路径
    next = request.get_full_path()
    print(next)
    url = reverse(name,args=args,kwargs=kwargs)
    qd = QueryDict(mutable=True)
    qd['next'] = next
    print(next,qd.urlencode(),url)
    return_url = "{}?{}".format(url,qd.urlencode())
    return return_url

@register.inclusion_tag('qxmenu.html')
def qxmenu(request):
    print(request.session['pom'])
    return {'menu_dict':request.session['pom']}
