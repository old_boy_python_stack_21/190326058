# 项目部署

- nginx + uwsgi + django + mysql

###### 1.  后端搞起

```python
1.上传crm项目到linux服务器
2.安装uwsgi命令,这是python的一个模块
激活一个虚拟环境去使用

virtualenv --no-site-packages --python=python3   s21uwsgi
pip3 install -i https://pypi.douban.com/simple  uwsgi 

pip3 install -i https://pypi.tuna.tsinghua.edu.cn/simple django==1.11.1
pip3 install -i https://pypi.tuna.tsinghua.edu.cn/simple pymysql 
pip3 install -i https://pypi.tuna.tsinghua.edu.cn/simple django-multiselectfield菜单..
```

###### 2.使用uwsgi的命令,参数形式启动  crm项目

```python
	以往的python3 manage.py runserver 调用wsgiref去启动django,性能很低,单进程web
	使用uwsgi启动django,可以支持并发,多进程,以及日志设置,多种功能
	
	#必须在django项目,第一层敲这个命令
	
	uwsgi --http :8000 --module mysite.wsgi
		--http 指定是http协议,去启动项目
		--module  指定django目录下的wsgi文件
	#uwsgi支持的热加载命令
	uwsgi --http :9000 --module Aida_crm.wsgi   --py-autoreload=1 
```

###### 3.uwsgi以配置文件的形式启动 ,就是把你的启动参数,写入到一个文件中,然后,执行这个文件即可

```python
配置文件名字可以叫做 uwsgi.ini ,内容如下,这个文件是手动生成的
touch uwsgi.ini ,写入如下内容

[uwsgi]
# Django-related settings
# the base directory (full path)
#填入项目的绝对路径 ,项目的第一层路径
chdir           = /opt/s21/Aida_crm

# Django's wsgi file
#指定第二层项目下的wsgi文件
module          = Aida_crm.wsgi

# the virtualenv (full path)
#找到虚拟环境的绝对路径
home            = /opt/s21/s21uwsgi


# process-related settings
# master
master          = true

# 以cpu核数来填写,uwsgi的工作进程数
processes       = 2

# the socket (use the full path to be safe
#这是以uwsgi_socket协议启动的项目,无法再去通过浏览器访问,必须通过nginx以uwsgi协议去反向代理
#这是以uwsgi_socket协议启动的项目,无法再去通过浏览器访问,必须通过nginx以uwsgi协议去反向代理
#这是以uwsgi_socket协议启动的项目,无法再去通过浏览器访问,必须通过nginx以uwsgi协议去反向代理
socket          = 0.0.0.0:8000

#也可以使用http协议去启动(仅用作调试使用)
#也可以使用http协议去启动(仅用作调试使用)
#也可以使用http协议去启动(仅用作调试使用)
#http = 0.0.0.0:9000

# ... with appropriate permissions - may be needed
# chmod-socket    = 664
# clear environment on exit
vacuum          = true

#后台运行参数,将uwsgi运行在后台,并且将django日志输出到uwsgi.log中
daemonize = uwsgi.log 
```

###### 4.指定配置文件启动django

```python
uwsgi --ini  uwsgi.ini 

nginx的配置,反向代理uwsgi
1.修改nginx.conf如下

    server {
        listen       80;
        server_name  www.oldchouhuo.com;
        #charset koi8-r;
        #access_log  logs/host.access.log  main;
        #access_log  "pipe:rollback logs/host.access_log interval=1d baknum=7 maxsize=2G"  main;
        location / {
        #转发请求的方式配置在这里
        #转发请求的方式配置在这里
        #转发请求的方式配置在这里
			include    uwsgi_params;
			uwsgi_pass 0.0.0.0:8000;
        }
		}  

2.重新加载nginx,访问nginx,查看是否反向代理

	去浏览器访问nginx的地址,查看能否访问到crm的内容

3.收集crm的所有静态文件,丢给nginx去解析

对django的settings.py配置修改如下

	添加如下参数
	STATIC_ROOT='/opt/s21static'                                                                                                                        
	STATIC_URL = '/static/'
	STATICFILES_DIRS = [
		os.path.join(BASE_DIR, 'static')
	]

	执行命令,收集django的所有静态文件,系统会自动创建'/opt/s21static'  这个文件夹
	python3 manage.py collectstatic

4,配置nginx,找到crm的这些静态资源
			location / {
				include    uwsgi_params;
				uwsgi_pass 0.0.0.0:8000;
			}
        #添加一个location,针对nginx的url进行匹配处理
        #当请求时 www.oldchouhuo.com/static/.....  这样的url的时候,nginx进行别名修改,去/opt/s21static底下去寻找资源文件
        #                                                                                                                            
			location  /static {
				alias /opt/s21static;
}
5.此时再次访问网站,查看是否处理了静态资源
www.oldchouhuo.com 
```

