### 1. Golang



###### 1. 变量声明赋值:

```go
1. var a int = 10
2. var  a,b int
3. var {
   a int
   b int
   }
4.   v3 := 1        :=声明并赋值     简化var的写法
5. var v1,v2,v3 int
	v1,v2,v3 = 1,2,3
```

###### 2. 函数调用

```go
import "fmt"

func test()(int,string){
    return 666,"h"
}

func main(){
    //调用函数,接收返回值
    //匿名函数,防止编译不通过
    i,_ = test()     //_相当于占位符(站位接收"h")
    fmt.Println(i)
}
```

###### 3.定义(声明) 常量

```go
1. func main() {
    //关键字 const   常量定义必须赋值
    const filename = "abcd.txt"
}
2. const filename1 = "pol.txt"
3. func cons() {
    const(
    filename = "asdas.txt"
    a,b = 1,2
    )
}
```

###### 4.枚举

```go
#定义 枚举类型
func enum() {
    const (
    	python = 0
    	java = 1
    	golang = 3
    )
    fmt.Println(python,java,golang)
}

func enum() {
    const (
    	python = 0
        //默认跟上面是一样的
    	java
    	golang = 3
        php
    )
    fmt.Println(python,java,golang,php) // 0,0,1,1
}
```

###### 5.  iota 常量生成器

```go
func enum() {
    const (
    	python = iota  //后面自增
    	java
    	golang
        php
    )
    fmt.Println(python,java,golang,php) // 0,1,2,3
}
```

```go
//iota 参与运算
func enum() {
    const (
        //位运算
        //1<< 0 相当于 1 * 2^0
        b = 1 << (10 * iota)
        kb
        mb
        gb
        tb
    )
    fmt.Println(b,kb,mb,gb,tb) //1,1024,107
}
```

###### 6. 基本数据类型

1. 整形

```go
int 有符号
uint 没符号
int8 代表占8位,1字节
int16 2字节
int 和 uint ,跟操作平台相关,若32系统占4字节,64系统占8字节
```

2. 浮点型 

```go
float32   4字节
float64   8 字节
// 浮点型不能和整形相加
```

3. 布尔类型

```python
var v1 bool
v1 = True

v2 := (1==2)
fmt.Println(v2,TypeOf(v2))

v3 := !v2    //v2的取反
fmt.Println(v3,TypeOf(v3))

if v3 == True && v2 == false{
    fmt.Println(a:"猜对了")
}
```

4. byte 类型

```go
func main() {
   var ch byte
	// 字符用单引号,字符串用双引号
	ch = 'a'
	fmt.Println(ch)   //97  ascill码的值
	fmt.Printf(format:"ch = %c\n",ch)  //格式化打印%c字符型 
}
```

5. 字符串类型

```go
func main() {
    var str string
    str = "abc"
    fmt.Println(str[0])    //97  ascill码的值
    //uint8与byte一样
    fmt.Println(reflect.TypeOf(str[0])
}
```

###### 7. formate 包

实际相当于python的字符串格式化  %

```python
func main(){
    a := 15
    fmt.Printf(format:"a=%d"%a)
}
```

###### 8. 类型别名

```go
func main() {
    //定义myint 类型
    type myint int
    //用于定义类型定义变量
    var i myint = 100
    fmt.Println(i)
    fmt.Println(reflect.Typeof(i))
}
```

###### 9. 类型转换

```python
//类型转换
func main() {
    var  ch byte = 'a'
    //强转
    var i int = int(ch)
    fmt.Println(i)
}
```

###### 10. switch

```python
func grade(score int) string {
    //定义空串用于返回
    g := ""
    suitch {
    	case score < 0 || score > 100:
        	g = "输入错误"
        case score < 60:
        	g = "凉凉"
        case score < 80:
        	g = "还行"
        case score <=100:
        	g = "厉害了"
    }
    return g
}

func main() {
    fmt.Println(grade(88))
}
```

### 2. 流程控制

###### 1. 选择结构

```go
package main

import (
	"fmt"
	"io/ioutil"
)
func main() {
	const filename = "abc.txt"	
	contents,err := ioutil.ReadFile(filename)
	if err != nil {
		fmt.Println(err)
	} else {
		fmt.Printf("%s\n",contents)
	}
}
```

```go

```



###### 8. 命令行编译与执行

1. go build   go执行文件    在当前文件生成执行文件.exe

2.  windows  直接输入 执行文件名.exe   执行

   mac ./  执行文件名.exe   执行

3. go install  go执行文件  将编译后的文件生成到配置文件的bin目录下

   cd 到bin目录  执行

4. 查看环境配置   go env    

###### 8.  循环语句

 ```go
//死循环
func main()  {
	for{
		println("ALEXDSB")
	}
}
 ```

```go
package main

//循环6次
func main()  {
	for i:=0;i<=5;i++{
		println("ALEXDSB")
	}
}
```

```go
package main

import "fmt"
//循环字符串
func main()  {
	var a = "alexdsb"
	for i,c := range a{
		fmt.Printf("下标%d,值为%c\n",i,c)

	}
}
```

###### 9. 跳转语句

```go
package main
import "fmt"
//循环字符串
func main()  {
	var a = "alexdsb"
	for i,c := range a{
		fmt.Printf("下标%d,值为%c\n",i,c)
		if i == 3 {
			fmt.Println("结束了")
			break
		}
	}
}
```

```go
package main
import "fmt"
//跳转到FLAG
func main()  {
	var a = "alexdsb"
	for i,c := range a{
		fmt.Printf("下标%d,值为%c\n",i,c)
		if i == 3 {
			fmt.Println("结束了")
			goto FLAG
		}
	}
FLAG:
	fmt.Println("8956")
}
```

### 3. 函数:

###### 1. 自定义函数

```go
函数和字段,名字首字母小写代表的是private,大写为public
```

练习:

```go
求1-100的和
package main
import "fmt"
func main() {
	var a = 0
	for i:=0;i<=100;i++ {
		a += i
	}
	fmt.Println(a)
}
```

###### 2. defer关键字

```go
1.	defer⽤于延迟一个函数或者方法的执行
2.	defer语句经常被用于处理成对的操作，如打开、关闭、连接、断开连接、加锁、释放锁
3.	通过defer机制，不论函数逻辑多复杂，都能保证在任何执行路径下，资源被释放
4.	释放资源的defer应该直接跟在请求资源的语句后，以免忘记释放资源
```

###### 3. 多个defer执行顺序

```go
package main
import "fmt"
func main() {
	var a = 0
	for i:=0;i<=100;i++ {
		a += i
		//最后执行defer,打印100次
		defer fmt.Println("aaaaa")
	}
	fmt.Println(a)
}
```

### 4. 复合函数

###### 1. 指针

```go
package main
import "fmt"
func main() {
	var a int = 20
	var ip *int
	ip = &a
	fmt.Printf("a的地址是:%x\n",&a)
	fmt.Printf("ip变量存储的指针地址:%x\n",ip)
	fmt.Printf("ip变量存储的指针地址:%x\n",&ip)
	fmt.Printf("*ip变量的值:%d\n",*ip)
}
//a的地址是:c000054080
//ip变量存储的指针地址:c000054080
//ip变量存储的指针地址:c000080018
//*ip变量的值:20
```

练习:

```go
1.	程序获取一个int变量num的地址并打印
2.	将num的地址赋给指针ptr，并通过ptr去修改num的值
```

```go
*指针=值
```

###### 2. 值传递和引用传递

```go
void pass_by_val(int a) {
	a++;
}
void pass_by_ref(int& a) {
	a++;
}
int main() {
	int a = 3;
	pass_by_val(a);
	printf("pass_by_val:%d\n",a); //传过去值
	pass_by_ref("pass_by_ref:%d\n",a);   //传过去引用
}
// 3  , 4   
```

###### 3. new()和make()

```go
1.	new()用来分配内存，但与其他语言中的同名函数不同，它不会初始化内存，只会将内存置零
2.	make(T)会返回一个指针，该指针指向新分配的，类型为T的零值，适用于创建结构体
3.	make()的目的不同于new()，它只能创建slice、map、channel，并返回类型为T（非指针）的已初始化（非零值）的值
```

```go
package main
import "fmt"
func main() {
	p := new([6]int)
	fmt.Println(p)
}
//&[0 0 0 0 0 0]
```

```go
package main
import "fmt"
func main() {
	p := make([]int,10,50) //列表长度为10,开辟空间最大50
	fmt.Println(p)
}
//&[0 0 0 0 0 0 0 0 0 0]
```

### 5. 数组

```go
声明数组：
var 数组名[数组长度] 数组类型
```

```go
//一维数组
func main(){
    var arr1 [5]int
    //声明定长的数组
    arr2 := [3]int{1,2,3}
    //声明变长的数组
    arr3 := [...]int{2,4,6,7,10}
    fmt.Println(arr1,arr2,arr3)
}
//二维数组
var grid [4][5]int
fmt.Println(grid)
```

```go
//遍历数组
for i:=0;i<len(arr3);i++{
    fmt.Println(arr3[i])
}
for i,v := range arr3 {
    fmt.Println(i,v)
}
```

```go
package main
import "fmt"
func printarr(arr *[5]int) {
	arr[0] = 18
	for i,v := range arr{
		fmt.Println(i,v)
	}
}
func main() {
	// 一、一维数组
	// 1.直接定义
	var arr1 [5]int
	// 2. :=
	arr2 := [3]int{1, 2, 3}
	// 3.省略大小
	arr3 := [...]int{2, 4, 6, 8, 10}
	fmt.Println(arr1, arr2, arr3)
	// 证明是值传递
	// 若要引用传递，则传地址
	printarr(&arr3)
	fmt.Println(arr3)
}

//传递引用
```

```go
package main

import "fmt"

func printarr(arr [5]int) {
	arr[0] = 18
	for i,v := range arr{
		fmt.Println(i,v)
	}
}
func main() {
	// 一、一维数组
	// 1.直接定义
	var arr1 [5]int
	// 2. :=
	arr2 := [3]int{1, 2, 3}
	// 3.省略大小
	arr3 := [...]int{2, 4, 6, 8, 10}
	fmt.Println(arr1, arr2, arr3)
	// 证明是值传递
	// 若要引用传递，则传地址
	printarr(arr3)
	fmt.Println(arr3)
}

//传递值
```

###### slice切片

```go
1. 	数组的长度在定义之后无法再次修改，go语言提供了数组切片（slice）来弥补数组的不足
2. 	创建切片的各种方式
```

```go
package main
import "fmt"
func fuc() {
	//切片只是比数组少了长度
	//声明切片的几种语法
	var s1 []int
	s2 := []int{}
	var s3 []int = make([]int,0,0)
	s4 := []int{1,2,3}
	fmt.Println(s1,s2,s3,s4)
}
//[] [] [] [1 2 3]
```

```go
func main() {
	//定义空切片
	var s1 []int
	nes1 := append(s1,6)
	fmt.Println(&nes1)
	fmt.Println(&s1)
}
```

基础操作

```go
package main
import "fmt"
// 切片
func main() {
   // 数组
   arr := [...]int{0, 1, 2, 3, 4, 5, 6, 7}
   s1 := arr[2:]
   fmt.Println(s1)
   // 修改
   // 是view的操作
   s1[0] = 100
   fmt.Println(s1)
   fmt.Println(arr)
   fmt.Println()

   s2 := arr[2:6]
   fmt.Println(s2)
   s3 := s2[3:]
   fmt.Println(s3)

   fmt.Println(len(s2))
   fmt.Println(cap(s2))
       s4 := append(s2,10)
   fmt.Println(s4)
   fmt.Println(arr)
   s5 := append(s4,11)
   fmt.Println(s5)
   fmt.Println(arr)
   s6 := append(s5,12)
   fmt.Println(s6)
   fmt.Println(arr)
}
```



###### 内建函数 copy():两个切片之间复制数据

```go
package main
import "fmt"
// 切片
func main() {
	arr := [...]int{0,1,2,3,4,5,6,7,8,9}
	s1 := arr[8:]
	s2 := arr[:5]
	//将第二个切片元素,拷贝到第一个里
	//从头开始覆盖
	copy(s2,s1)
	fmt.Println(s2)
	fmt.Println(arr)
}
```

###### MAP(python中的字典)

```go
1	Map 是go内置的数据结构，是一种无序的键值对的集合，可以通过key快速找到value的值
2	定义Map：
var 变量名 map[key的数据类型] value的数据类型
```

```go
//创建map
package main
import "fmt"
func main()  {
	var m1 map[int]string
	fmt.Println(m1)
	m2 := map[int]string{}
	m3 := make(map[int]string)
	m4 := make(map[int]string,10)  //指定容量
	fmt.Println(m2,m3,m4)
	m4[6] = "asdasda"
	fmt.Println(m4)
}
```

增删改查

```go
package main
import "fmt"
func main()  {
	var m1 map[int]string
	fmt.Println(m1)
	m2 := map[int]string{1:"上海",2:"杭州",3:"北京"}
	m2[3] = "广州"  //修改值
	m2[4] = "广东"   //加值
	//遍历
	for k,v := range m2{
		fmt.Printf("%d------>%s\n",k,v)
	}
	value,ok := m2[6]	fmt.Println("value=",value,",ok=",ok)
	//删除
	delete(m2,2)
	fmt.Println(m2)
}
```

### 6. 结构体(python的类)

```go
1	go语言没有class，只是个结构体struct
```

```go
结构体定义：
package main
import "fmt"
//定义学生类
type Student struct {
	id int
	name string
	sex byte
	age int
	addr string
} 

func main() {
	//顺序初始化
	var s1 Student = Student{1,"约翰",'f',20,"沙河"}
	//指定创建
	s2 := Student{id:2,age:20}
	//结构体作为指针变量初始化
	var s3 = &Student{3,"abc",'m',25,"昌平"}
	fmt.Println(s1,s2,s3)
	fmt.Println(s1.id)
}
```

###### 结构体参数

结构体可以作为函数参数传递

```go
package main

import "fmt"

// 定义学生的类
type Student struct {
   id   int
   name string
   sex  string
   age  int
   addr string
}

// 定义传递学生对象的方法
func temStudent(tmp Student) {
   // 修改对象的id
   tmp.id = 250
   fmt.Println(tmp)
}

// 传递指针对象
func temStudent2(p *Student) {
   // 修改对象的id
   p.id = 300
   fmt.Println(p)
}

func main() {
   // 1.顺序初始化
   var s1 Student = Student{1, "约翰", "female", 20, "shahe"}
   // 传递非指针
   temStudent(s1)
   fmt.Println("main", s1)
   // 传递指针
   temStudent2(&s1)
   fmt.Println(s1)
}
```

### 面对对象

###### 1. 简介

```go
	go语言对于面向对象的设计非常简洁而优雅
	没有封装、继承、多态这些概念，但同样通过别的方式实现这些特性
	封装：通过方法实现
	继承：通过匿名字段
	多态：通过接口
```

###### 2. 匿名字段(python的类的组合)

```go
go支持只提供类型而不写字段名的方式，也就是匿名字段，也称为嵌入字段
```

```go
package main
import "fmt"

type Person struct {
	name string
	sex string
	age int
}
type Student struct {
	Person
	id int
	addr string
}
func main()  {
	s1 := Student{Person{"alex","nv",21},2,"dsb"}
	s2 := Student{Person:Person{"peiqi","nv",20},id:2}
	fmt.Println(s1,s2)
}
```

###### 同名字段取值

```go
package main
import "fmt"
type Person struct {
	name string
	sex string
	age int
}
type Student struct {
	Person
	id int
	addr string
	name string
}
func main()  {
	s1 := Student{Person{"alex","nv",21},2,"dsb","xiaosb"}
	s2 := Student{Person:Person{"peiqi","nv",20},id:2}
	fmt.Println(s1,s2)
	fmt.Println(s1.name)
	fmt.Println(s1.Person.name)
}
```

###### 所有的内置类型和自定义类型都是可以作为匿名字段去使用

```go
package main

import "fmt"

// 类
type Person struct {
   name string
   sex  string
   age  int
}

// 自定义类型
type mystr string

type Student struct {
   // 匿名字段
   Person
   int
   mystr
}

func main() {
   s1 := Student{Person{"zs", "male", 18}, 1, "bj"}
   fmt.Println(s1)
   fmt.Println(s1.name)
}

```

###### 指针类型匿名字段

```go
package main
import "fmt"

// 类
type Person struct {
	name string
	sex  string
	age  int
}
type Student struct {
	// 匿名字段
	*Person
	id int
	addr string
}
func main() {
	s1 := Student{&Person{"zs", "male", 18}, 1, "bj"}
	fmt.Println(s1)
	fmt.Println(s1.name)
}
//{0xc00005c360 1 bj}
//zs
```

###### 方法

```go
	在面向对象编程中，一个对象其实也就是一个简单的值或者一个变量，在这个对象中会包含一些函数
	这种带有接收者的函数，我们称为方法，本质上，一个方法则是一个和特殊类型关联的函数
	方法的语法如下
func (接收参数名 接收类型) 方法名(参数列表)(返回值)
	可以给任意自定义类型（包括内置类型，但不包括指针类型）添加相应的方法
	为类型添加方法
	基础类型作为接收者
```

```go
package main
import "fmt"

// 方法：实现2个数相加
type MyInt int
// 面向过程的方式定义
func Add(a, b MyInt) MyInt {
   return a + b
}
// 面向对象的方式定义
func (a MyInt) Add(b MyInt) MyInt {
   return a + b
}
func main() {
   var a MyInt = 1
   var b MyInt = 1
   // 面向过程
   fmt.Println(Add(a, b))
   fmt.Println(a.Add(b))
}
```

###### 结构体作为接收者(给类添加方法)

```go
package main
import "fmt"

type Person struct {
	name string
	sex string
	age int
}
//给类添加方法
func (p Person)Prntinfo() {
	fmt.Println(p.name,p.sex,p.age)
}
func main() {
	p := Person{"alex","male",37}
	p.Prntinfo()
}
```

###### 值语义和引用语义(指针才会改变对象属性)

```go
package main
import "fmt"

// 值语义和引用语义
type Person struct {
	name string
	sex string
	age int
}
// 引用语义
func (p *Person)SetInfoPointer()  {
	(*p).name = "ls"
	p.sex = "male"
	p.age = 22
}

func (p Person)SetInfoValue()  {
	p.name = "zs"
	p.sex = "female"
	p.age = 25
}

func main() {
	p := Person{"ww","male",18}
	fmt.Println("函数调用前：",p)
	(&p).SetInfoPointer()
	fmt.Println("函数调用后：",p)

	// 值语义
	p2 := Person{"zl","female",25}
	fmt.Println("函数调用前：",p2)
	p2.SetInfoValue()
	fmt.Println("函数调用后：",p2)
}
//函数调用前： {ww male 18}
//函数调用后： {ls male 22}
//函数调用前： {zl female 25}
//函数调用后： {zl female 25}
```

###### 方法的继承

```go
package main

import "fmt"

type Person struct {
	name string
	sex string
	age int
}

func (p *Person)SetInfoPointer()  {
	(*p).name = "ls"
	p.sex = "male"
	p.age = 22
}

type Student struct {
	Person
}

func main() {
	p := Person{"alex","male",66}
	p.SetInfoPointer()
	s := Student{Person{"DSB","male",66}}
	s.SetInfoPointer()
	fmt.Println(p,s)
}
//{ls male 22} {{ls male 22}}
```

###### 方法的重写

```go
package main
import "fmt"

type Person struct {
   name string
   sex string
   age int
}
func (p *Person)PrintInfo()  {
   fmt.Println("Person:",p.name,p.sex,p.age)
}
type Student struct {
   Person
   id int
   addr string
}

// 方法重写
func (p *Student)PrintInfo()  {
   fmt.Println("Student:",p.name,p.sex,p.age)
}

func main() {
   p:= Person{"zs","male",18}
   p.PrintInfo()
   s := Student{Person{"ls","female",25},2,"bj"}
   s.PrintInfo()
   // s去调p的
   s.Person.PrintInfo()
}
```

###### 方法值和方法表达式(语法糖,了解即可)

```go
package main
import "fmt"

type Person struct {
   name string
   sex string
   age int
}
func (p *Person)PrintInfo()  {
   fmt.Println("Person:",p.name,p.sex,p.age)
}
func main() {
   p:= Person{"zs","male",18}
   p.PrintInfo()
   // 方法值
   pFunc1 := p.PrintInfo
   pFunc1()
   // 方法表达式
   pFunc2 := (*Person).PrintInfo
   pFunc2(&p)
```

###### 练习:创建属性的getter 和setter方法并进行调用(有毛病,不知道在干啥,没卵用)

```go
package main
import "fmt"

type Dog struct {
   name string
   sex  int
}
func (d *Dog) SetName(name string) {
   d.name = name
}
func (d *Dog) GetName() string {
   return d.name
}
func (d *Dog) bite() {
   fmt.Println("dog:", d.name)
}
func test01() {
   d := Dog{"二哈", 1}
   d.bite()
}
func main() {
   test01()
}
```

### 包和封装

```go
1. 	方法首字母大写：public
2. 	方法首字母小写：private
3. 	为结构体定义的方法必须放在同一个包内，可以是不同的文件
```

```go
工程下的某个文件夹点某个文件,直接回去导包
```

### 接口

```go
1.	go语言中，接口（interface）是一个自定义类型，描述了一系列方法的集合
2.	接口不能被实例化
3.	接口定义语法如下
type 接口名 interface{}
4.	PS：接口命名习惯以er结尾
```

###### 接口的定义与实现

```go
package main

import "fmt"

// 接口
type Humaner interface {
   // 方法
   Say()
}

// 结合多态
type Student struct {
   name  string
   score int
}

// 实现接口方法
func (s *Student) Say() {
   fmt.Printf("Student[%s,%d] 瞌睡不断\n", s.name, s.score)
}

type Teacher struct {
   name  string
   group string
}

func (t *Teacher) Say() {
   fmt.Printf("Teacher[%s,%s] 毁人不倦\n", t.name, t.group)
}

// 自定义类型
type MyStr string

func (str MyStr) Say() {
   fmt.Printf("MyStr[%s] 今晚加班，加班到后天\n", str)
}

// 多态的核心
// 参数为接口类型
func WhoSay(i Humaner) {
   i.Say()
}

func main() {
   s := &Student{"stu", 88}
   t := &Teacher{"tea", "golang"}
   var tmp MyStr = "abc"
   // 基本调用
   s.Say()
   t.Say()
   tmp.Say()
   // 多态
   WhoSay(s)
   WhoSay(t)
   WhoSay(tmp)
}
```

###### 接口继承,与类继承相似



###### 空接口:表示任意类型

```go
func Printf(format string,a...interface{})(n int,err error){
    return Fprintf(os.Stdout,format,a...)
}
```

```go
package main
import "fmt"

// 空接口
type Element interface{}

type Person struct {
   name string
   age  int
}

func main() {
   list := make([]Element, 3)
   list[0] = 1
   list[1] = "Hello"
   list[2] = Person{"zs", 18}
   // 循环
   for index, element := range list {
      // 类型断言：value,ok = element.(T)
      if value, ok := element.(int); ok {
         fmt.Printf("list[%d] 是int类型，值是%d\n", index, value)
      } else if value, ok := element.(string); ok {
         fmt.Printf("list[%d] 是string类型，值是%s\n", index, value)
      } else if value, ok := element.(Person); ok {
         fmt.Printf("list[%d] 是Person类型，值是[%s,%d]\n", index, value.name, value.age)
      } else {
         fmt.Println("其他类型")
      }
   }
}
```

### 异常处理

```go
1.系统抛
	-自己抛
	-系统抛
2.自己抛
```

```go
package main

import "fmt"

// 系统抛异常
func test01() {
	a := [5]int{0, 1, 2, 3, 4}
	//a[10] = 123
	index := 10
	a[index] = 123
}

// 自己抛异常
func test02() {
	getCirleArea(-5)
}

func getCirleArea(radius float32) (area float32) {
	if radius < 0 {
		// 抛异常
		panic("半径不能为负")
	}
	return 3.14 * radius * radius
}

func test03() {
	// 延迟到什么时候？出现错误立即执行
	// (1)函数结束
	// (2)异常发生时
	defer func() {
		// recover()：返回的是程序为什么挂了
		if err := recover(); err != nil {
			fmt.Println(err)
		}
	}()
	getCirleArea(-5)
	fmt.Println(123)
}

func test04()  {
	test03()
	fmt.Println(456)
}

func main() {
	//test01()
	test04()
}
```

###### 返回异常

```go
package main

import (
	"errors"
	"fmt"
)

func getCircleArea(radius float32) (ret float32, err error) {
	if radius < 0 {
		// 创建异常并返回
		err = errors.New("半径不能为负")
		return
	}
	ret = 3.14 * radius * radius
	return
}

func main() {
	ret, err := getCircleArea(-5)
	fmt.Println(ret,err)
	if err != nil {
		fmt.Println(err)
	} else {
		fmt.Println(ret)
	}
}
//0 半径不能为负
//半径不能为负
```

### 字符串原理

```go
1.字符串底层就是一个byte数组  []byte 可以与字符串 互相转换
2.字符串是由byte字节组成，字符串长度就是byte字节长度
3.字符串中的字符是不能直接修改的
```

```go
package main
import "fmt"

func main() {
	var str = "hello"
	fmt.Println(str[0])
	fmt.Println(len(str))
	// 不能直接修改
	//str[0] = 'a'
	// 若要修改，需要转为字节切片去改
	var bs []byte = []byte(str)
	bs[0] = 'a'
	// 切片转回str
	str = string(bs)
	fmt.Println(str)
}
```

###### -   rune类型用于表示utf8的字符，一个rune字符由1个或者多个byte组成

```go
package main
import "fmt"

func main() {
   str1 := "hello"
   fmt.Println(len(str1))
   // 每个中文占3个长度
   str2 := "hello你好"
   fmt.Println(len(str2))
   // 可以将字符串转为rune 切片
   var r []rune = []rune(str2)
   fmt.Println(len(r))
}
```

###### 字符串操作

```go
1.可以通过Go标准库中的strings和strconv两个包中的函数进行相应的操作
2.func len(v Type) int
```

```go
求字符串长度
func main() {
   fmt.Println(len("hello"))
}
```

```go
	func Contains(s, substr string) bool
	字符串s中是否包含substr，返回bool值
func main() {
   fmt.Println(strings.Contains("hello", "llo"))
}
```

```go
1.	func HasPrefix(s, prefix string) bool
2.	判断字符串s是否以prefix为开头
3.	HasSuffix是判断结尾
func main() {
   fmt.Println(strings.HasPrefix("hello","he"))
}
```

```go
	func Join(a []string, sep string) string
	字符串链接，把slice a通过sep链接起来
func main() {
   s := []string{"abc", "456", "999"}
   fmt.Println(strings.Join(s, "** "))
}
```

```go
	func Index(s, sep string) int
	在字符串s中查找sep所在的位置，返回位置值，找不到返回-1
	LastIndex是从后往前查找
func main() {
   fmt.Println(strings.Index("chicken", "ken"))
}
```

```go
	func Repeat(s string, count int) string
	重复s字符串count次，最后返回重复的字符串
func main() {
   fmt.Println("ba" + strings.Repeat("na", 2))
}
```

```go
	func Replace(s, old, new string, n int) string
	在s字符串中，把old字符串替换为new字符串，n表示替换的次数，小于0表示全部替换
func main() {
   fmt.Println(strings.Replace("ok ok ok", "k", "ky", 2))
}
```

```go
	func Split(s, sep string) []string
	把s字符串按照sep分割，返回slice
func main() {
   fmt.Printf("%q\n", strings.Split("a,b,c", ","))
}
```

```go
	func Trim(s string, cutset string) string
	在s字符串的头部和尾部去除cutset指定的字符串
func main() {
   fmt.Printf("[%q]", strings.Trim(" !哈!哈! ", "! "))
}
```

```go
	func Fields(s string) []string
	去除s字符串的空格符，并且按照空格分割返回slice
func main() {
   fmt.Println( strings.Fields("  a b  c   "))
}
```

###### 字符串转换

```go
	Append系列函数：将整数等转换为字符串后，添加到现有的字节数组中
package main

import (
	"fmt"
	"strconv"
)

func main() {
	str := make([]byte, 0, 100)
	//以10进制方式追加
	str = strconv.AppendInt(str, 4567, 10)
	str = strconv.AppendBool(str, false)
	str = strconv.AppendQuote(str, "abcdefg")
	str = strconv.AppendQuoteRune(str, '单')
	fmt.Println(string(str))
}
```

- Format系列函数：把其他类型的转换为字符串

```go
package main
import (
   "fmt"
   "strconv"
   "reflect"
)
func main() {
   a := strconv.FormatBool(false)
   b := strconv.FormatInt(-1234, 10)
   //Uint无符号
   c := strconv.FormatUint(1234, 10)
   //与FormatInt一样，简写
   d := strconv.Itoa(-2234)
   fmt.Println(a, b, c, d)
   fmt.Println(reflect.TypeOf(a))
}
```

### 时间和日期类型

```go
1	time包下的Time类型用来表示时间
2	可以用time.Now()获取当前时间
3	可以用time.Now().Unix()获取当前时间戳
```

```go
package main

import (
	"fmt"
	"time"
)

func main() {
	// 获取当前时间
	now := time.Now()
	fmt.Println(now)
	// 分别获取年月日等
	year := now.Year()
	month := now.Month()
	day := now.Day()
	hour := now.Hour()
	minute := now.Minute()
	send := now.Second()
	// 拼接打印
	// 02d代表不足2位的话，用0补
	fmt.Printf("%02d-%02d-%02d %02d-%02d-%02d\n",year,month,day,hour,minute,send)
	// 获取时间戳
	timestamp := time.Now().Unix()
	fmt.Println(timestamp)
}
```

###### 时间戳转Time类型

```go
package main
import (
	"fmt"
	"time"
)
func main()  {
	//获取时间戳
	timestamp := time.Now().Unix()
	fmt.Println(timestamp)
	//根据时间戳,转成时间
	timeobj := time.Unix(timestamp,0)
	timeobj.Year()
	timeobj.Month()
}
```

源码中的常量定义:

```go
const (
   // 纳秒
   Nanosecond  Duration = 1
   // 微秒
   Microsecond          = 1000 * Nanosecond
   // 毫秒
   Millisecond          = 1000 * Microsecond
   // 秒
   Second               = 1000 * Millisecond
   // 分
   Minute               = 60 * Second
   // 时
   Hour                 = 60 * Minute
)
	通过time.Now().Format()可以将时间格式化
```

###### l  通过time.Now().Format()可以将时间格式化

```go
package main

import (
	"fmt"
	"time"
)
func main()  {
	now := time.Now()
	fmt.Println(now)
	//global必须写死
	timestr := now.Format("2006/01/02 15:04:05")//必须是这个时间,这个时间是go语言诞生时间
	fmt.Printf("time:%s\n",timestr)
}
```













































































































## web 框架  

beego ,  gin



### 面试题

```python
func main() {
    const(
    	a= iota
        b
        c
        d
        e = 8
        f = 8
        g = iota
        h
        i
    )
    fmt.Println(a,b,c,d,e,f,g,h,i)
 }
```

