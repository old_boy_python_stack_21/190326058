# -*- coding: utf-8 -*-
# Generated by Django 1.11.21 on 2019-06-14 09:21
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Class',
            fields=[
                ('cid', models.AutoField(primary_key=True, serialize=False)),
                ('class_name', models.CharField(max_length=32, unique=True)),
            ],
        ),
    ]
