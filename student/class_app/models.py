from django.db import models

# Create your models here.


class Class(models.Model):
    cid = models.AutoField(primary_key=True)
    class_name = models.CharField(max_length=32,unique=True)

class Student(models.Model):
    student_name = models.CharField(max_length=32,unique=True)
    cla = models.ForeignKey(Class,on_delete=models.CASCADE)

class Teacher(models.Model):
    teacher_name = models.CharField(max_length=32)
    classes = models.ManyToManyField('Class')


