from django.shortcuts import render, HttpResponse, redirect
from class_app import models
IMG_LIST = ['\static\img\1.jpg',
            '\static\img\2.jpg',
            '\static\img\3.jpg'
            ,'\static\img\4.jpg'
            ,'\static\img\5.jpg'
            ,'\static\img\6.jpg'
            ,'\static\img\7.jpg'
            ,'\static\img\8.jpg'
            ,'\static\img\9.jpg'
            ,'\static\img\10.jpg'
            ,'\static\img\11.jpg'
            ,'\static\img\12.jpg']

def zhuye(request):
    class_obj_lis = models.Class.objects.all()
    if request.method == 'GET':
        print(class_obj_lis)
        return render(request, 'abcd.html', {'class_obj_lis': class_obj_lis})


def show_student(request):
    student_obj_lis = ''
    # if 'id' in request.GET.keys():
    #     cla_id = request.GET.get('id')
    #     print(cla_id)
    #     student_obj_lis = models.Student.objects.filter(cla_id=cla_id)
    if 'name' in request.GET.keys():
        cla_name = request.GET.get('name')
        global USER
        USER['user']=cla_name
        print(cla_name)
        class_id = models.Class.objects.filter(class_name=cla_name)[0].pk
        student_obj_lis = models.Student.objects.filter(cla_id=class_id)
    # student_obj_lis = models.Student.objects.all()
    return render(request, 'student.html', {'student_obj_lis': student_obj_lis,'IMG_LIST':IMG_LIST})


def add_class(request):
    all_class = models.Class.objects.all()
    flag = ''
    if request.method == 'POST':
        print(request.POST)
        classname = request.POST.get('class_name')
        print(classname)
        if not classname:
            flag = '不能为空'
            return render(request, 'add_class.html', {'flag': flag})
        for class_obj in all_class:
            if class_obj.class_name == classname:
                flag = '创建的班级以存在'
                return render(request, 'add_class.html', {'flag': flag})

        models.Class.objects.create(class_name=classname)
        return redirect('/zhuye/')
    return render(request, 'add_class.html', {'flag': flag})


def create_class(request):
    flag = ''
    class_name = request.GET.get('class_name')
    obj = models.Class.objects.get(class_name=class_name)
    print(obj.class_name)
    if request.method == 'POST':
        new_class_name = request.POST.get('new_class')
        print(new_class_name)
        allclass_obj = models.Class.objects.all()
        for class_obj in allclass_obj:
            if new_class_name == class_obj.class_name:
                flag = '名称已存在'
        if new_class_name == obj.class_name:
            flag = '您没有做出修改'
        else:
            print(obj.class_name, new_class_name)
            obj.class_name = new_class_name
            obj.save()
            return redirect('/zhuye/')
        return render(request, 'create_class.html', {'obj': obj, 'flag': flag})

    return render(request, 'create_class.html', {'obj': obj, 'flag': flag})


def del_class(request):
    class_name = request.GET.get('class_name')
    obj = models.Class.objects.filter(class_name=class_name)[0]
    obj.delete()
    return redirect('/zhuye/')

USER = {'user':'PY18'}
def add_student(request):
    if request.method == 'POST':
        class_name = request.POST.get('class_name')
        student_name = request.POST.get('student_name')
        class_obj = models.Class.objects.get(class_name=class_name)
        pk = class_obj.cid
        models.Student.objects.create(student_name=student_name,cla_id=pk)
        return redirect('/show_student/?name='+USER['user'])


    # class_name = request.GET.get('student')
    # print(class_name,type(class_name))
    # class_obj = models.Class.objects.get(class_name=class_name)
    # print(class_obj)
    # pk = class_obj.cid
    # # all_student = models.Class.objects.filter()
    # if request.method == 'POST':
    #     new_student_name = request.POST.get('new_student_name')
    #     models.Student.objects.create(student_name=new_student_name,cla_id=pk)
    return render(request,'add_student.html',{'USER':USER['user']})

def create_student(request):
    class_obj_lis = models.Class.objects.all()

    student_name = request.GET.get('student_name')
    student_obj = models.Student.objects.get(student_name=student_name)
    if request.method == 'POST':
        new_student_name = request.POST.get('student_name')
        class_name = request.POST.get('class_name')
        class_cid = models.Class.objects.get(class_name=class_name).cid
        student_obj.student_name = new_student_name
        student_obj.cla_id = class_cid
        student_obj.save()
        return redirect('/show_student/?name='+ USER['user'])


    return render(request,'create_student.html',{'student_name':student_name,'class_obj_lis':class_obj_lis,'student_obj':student_obj})

def del_student(request):
    student_name = request.GET.get('student_name')
    student_obj = models.Student.objects.get(student_name=student_name)
    student_obj.delete()
    return redirect('/show_student/?name='+ USER['user'])


def show_teacher(request):
    teacher_obj_lis = models.Teacher.objects.all()
    return render(request,'show_teacher.html',{'teacher_obj_lis':teacher_obj_lis})

def create_teacher(request):
    flag = ''
    teacher_id = request.GET.get('id')
    teacher_obj = models.Teacher.objects.get(pk=teacher_id)
    class_obj_lis = models.Class.objects.all()
    if request.method == 'POST':
        print(request.POST)
        new_teacher_name = request.POST.get('new_teacher_name')
        class_lis = request.POST.getlist('class_lis')
        for tea_obj in models.Teacher.objects.all():
            if tea_obj.pk == teacher_obj.pk:
                continue
            if new_teacher_name == tea_obj.teacher_name:
                flag = '修改的名字已存在'
                return render(request,'create_teacher.html',{'teacher_obj':teacher_obj,'class_obj_lis':class_obj_lis,'flag':flag})

            # flag = '修改的老师已存在'

        teacher_obj.teacher_name=new_teacher_name
        teacher_obj.save()
        teacher_obj.classes.set(class_lis)
        return redirect('/show_teacher/')

    return render(request,'create_teacher.html',{'teacher_obj':teacher_obj,'class_obj_lis':class_obj_lis,'flag':flag})



def delete_teacher(request):
    teacher_id = request.GET.get('id')
    teacher_obj = models.Teacher.objects.get(pk=teacher_id)
    teacher_obj.delete()
    return redirect('/show_teacher/')
    pass

def add_teacher(request):
    flag = ''
    teacher_obj_lis = models.Teacher.objects.all()
    class_obj_lis = models.Class.objects.all()
    if request.method == 'POST':
        teacher_name = request.POST.get('teacher_name')
        class_lis = request.POST.getlist('class_lis')
        for teacher_obj in teacher_obj_lis:
            if teacher_obj.teacher_name == teacher_name:
                flag = '添加的老师已存在'
                return render(request, 'add_teacher.html', {'class_obj_lis': class_obj_lis, 'flag': flag})
        teath_obj = models.Teacher.objects.create(teacher_name=teacher_name)
        teath_obj.save()
        teath_obj.classes.set(class_lis)
        return redirect('/show_teacher/')



    return render(request,'add_teacher.html',{'class_obj_lis':class_obj_lis,'flag':flag})