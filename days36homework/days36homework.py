# 1、程序从flag a执行到falg b的时间大致是多少秒？
# import threading
# import time
# def _wait():
#     time.sleep(60)
#     # b = time.time()
# # flag a
# # a = time.time()
# t = threading.Thread(target=_wait,daemon = False)
# t.start()
# b = time.time()
# print(b-a)
# flag b
'''
60
'''
# 2、程序从flag a执行到falg b的时间大致是多少秒？

# import threading
# import time
# def _wait():
# 	time.sleep(60)
# # flag a
# t = threading.Thread(target=_wait,daemon = True)
# t.start()
# # flag b
'''
0秒
'''
# 3、程序从flag a执行到falg b的时间大致是多少秒？

# import threading
# import time
# def _wait():
# 	time.sleep(60)
# # flag a
# t = threading.Thread(target=_wait,daemon = True)
# t.start()
# t.join()
# # flag b
'''
60秒
'''
# 4、读程序，请确认执行到最后number是否一定为0

# import threading
# loop = int(1E7)
# def _add(loop:int = 1):
# 	global number
# 	for _ in range(loop):
# 		number += 1
# def _sub(loop:int = 1):
# 	global number
# 	for _ in range(loop):
# 		number -= 1
# number = 0
# ta = threading.Thread(target=_add,args=(loop,))
# ts = threading.Thread(target=_sub,args=(loop,))
# ta.start()
# ts.start()
# ta.join()
# ts.join()
# print(number)

'''
我觉得理论上为0，但是由于两个线程同时对一个变量进行修改，造成的数据不安全，得到的随机数
'''
# 5、读程序，请确认执行到最后number是否一定为0

# import threading
# loop = int(1E7)
# def _add(loop:int = 1):
# 	global number
# 	for _ in range(loop):
# 		number += 1
# def _sub(loop:int = 1):
# 	global number
# 	for _ in range(loop):
# 		number -= 1
# number = 0
# ta = threading.Thread(target=_add,args=(loop,))
# ts = threading.Thread(target=_sub,args=(loop,))
# ta.start()
# ta.join()
# ts.start()
# ts.join()
# print(number)
'''
# 一定为0，因为因为俩线程是分开修改数据的
'''

# 7、读程序，请确认执行到最后number的长度是否一定为1
import time
import threading
# loop = int(1E7)
# print(loop)
# def _add(loop:int = 1):
# 	global numbers
# 	for _ in range(loop):
# 		numbers.append(0)
# def _sub(loop:int = 1):
# 	global numbers
# 	while not numbers:
# 		time.sleep(1E-8)
# 	numbers.pop()
# numbers = [0]
# ta = threading.Thread(target=_add,args=(loop,))
# ts = threading.Thread(target=_sub,args=(loop,))
# ta.start()
# ta.join()
# ts.start()
# ts.join()
# print(len(numbers))
'''
一定为10000000
'''
# 、读程序，请确认执行到最后number的长度是否一定为1

# import threading
# loop = int(1E7)
# def _add(loop:int = 1):
# 	global numbers
# 	for _ in range(loop):
# 		numbers.append(0)
# def _sub(loop:int = 1):
# 	global numbers
# 	while not numbers:
# 		time.sleep(1E-8)
# 	numbers.pop()
# numbers = [0]
# ta = threading.Thread(target=_add,args=(loop,))
# ts = threading.Thread(target=_sub,args=(loop,))
# ta.start()
# ts.start()
# ta.join()
# ts.join()
'''

一定为10000000
'''

# 1.携程
'''
能够在一个线程下来回切换的任务，我们就叫他携程
'''
# 2、协程中的join是用来做什么用的？它是如何发挥作用的？
'''
join是为了发生阻塞，执行携程任务
'''
# 3、使用协程实现并发的tcp server端

#4、在一个列表中有多个url，请使用协程访问所有url，将对应的网页内容写入文件保存

# --------------------------------------------------------------------------------------------------------
# 1. 1、进程和线程的区别
'''
# 进程是CPU最小的资源分配单位   进程 数据隔离  进程之间切换开销大

# 线程是cpu最小的调度单位  线程数据共享  线程切换开销小  线程是进程的一部分
'''
# 2、进程池、线程池的优势和特点
#系统提前开好几个线程或进程，任务来了可以直接使用 开好的进程，线程
# 减少了 开关进程，切换 的资源消耗

# 3.3、线程和协程的异同?
# 线程是cpu最小的调度单位 线程的IO切换由操作系统完成 线程数据共享  线程切换开销小  线程是进程的一部分  不能利用多核
#携程 携程是 线程的一部分，携程 的切换是 由用户设定的   用户级别   资源消耗小于线程   不能利用多核

# 4、请简述一下互斥锁和递归锁的异同
# 都是为了保证数据安全
# 互斥锁 ：一把锁不能在一个线程中连续acquire，开销小
# 递归锁 ：一把锁可以连续在一个线程中acquire多次，acquire多少次就release多少次，开销大

# 5、请列举一个python中数据安全的数据类型？
# queue
# 6、Python中如何使用线程池和进程池
# 调用模块
from concurrent.futures import ThreadPoolExecutor
from concurrent.futures import ProcessPoolExecutor
# 7、简述 进程、线程、协程的区别 以及应用场景？
# 进程  计算机最小的资源分配单位  数据隔离  资源耗费大 操作系统级别  可以利用多核   使用于  高计算
# 线程 cpu最小的调度单位  数据共享 资源耗费较小   操作系统级别   无法利用多核     爬虫
# 携程   资源耗费小，数据共享   用户级别 无法利用多核     爬虫


#8 并发 2个CPU 2个程序分别用一个CPU 同时执行，我们就说这样是并行
# 并发  2个程序 1个CPU  看似是同时执行  实则是在CPU是串行