import gevent
import time
import socket
from gevent import monkey
monkey.patch_all()

def func(conn):
    while True:
        msg = conn.recv(1024)
        time.sleep(0.1)
        conn.send(msg)
if __name__ == '__main__':
    sk = socket.socket()
    sk.bind(('127.0.0.1',9000))
    sk.listen()
    lis = []
    while True:
        conn,addr = sk.accept()
        g = gevent.spawn(func,conn)
        lis.append(g)

