from django.db.models import Avg, Count
from django.shortcuts import render

# Create your views here.
from lagou import models


def index(requests):
    jinrong_lis = []
    wenyu_lis = []
    xinxianquan_lis = []
    qiyefuwu_lis = []
    yiliiao_lis = []
    shuju_lis = []
    xiaofei_lis = []

    haidian = []
    chaoyang = []
    changping = []
    dongcheng = []
    xicheng = []
    qita = []
    # 北京地区
    ret = models.TriClub.objects.filter(city='北京').aggregate(Avg('salary'), Count('salary'))
    beijing_count = ret.get('salary__count')
    beijing_salary = int(ret.get('salary__avg')) * 1000
    # 上海地区
    ret = models.TriClub.objects.filter(city='上海').aggregate(Avg('salary'), Count('salary'))
    shanghai_count = ret.get('salary__count')
    shanghai_salary = int(ret.get('salary__avg')) * 1000
    # 杭州地区
    ret = models.TriClub.objects.filter(city='杭州').aggregate(Avg('salary'), Count('salary'))
    hangzhou_count = ret.get('salary__count')
    hangzhou_salary = int(ret.get('salary__avg')) * 1000
    # 所有
    all_obj = models.TriClub.objects.all()
    # salary top 10
    top_salary = models.TriClub.objects.all().order_by('-salary').values('city', 'companyName','positionname', 'salary')
    print(top_salary)

    for i in all_obj:
        if '金融' in i.companyfield:
            jinrong_lis.append(i)
        if '文娱' in i.companyfield:
            wenyu_lis.append(i)
        if '信息安全' in i.companyfield:
            xinxianquan_lis.append(i)
        if '企业服务' in i.companyfield:
            qiyefuwu_lis.append(i)
        if '医疗' in i.companyfield:
            yiliiao_lis.append(i)
        if '数据服务' in i.companyfield:
            shuju_lis.append(i)
        if '消费生活' in i.companyfield:
            xiaofei_lis.append(i)
        if '海淀区' == i.district:
            haidian.append(i)
        if '朝阳区' == i.district:
            chaoyang.append(i)
        if '东城区' == i.district:
            dongcheng.append(i)
        if '西城区' == i.district:
            xicheng.append(i)
        if '昌平区' == i.district:
            changping.append(i)

    else_district = beijing_count - len(haidian) - len(chaoyang) - len(dongcheng) - len(xicheng) - len(changping)

    avgsairly = models.TriClub.objects.all().aggregate(Avg('salary'))
    print(avgsairly.get('salary__avg') * 1000)

    res = models.TriClub.objects.values('pythondirection').annotate(num=Count('pythondirection')).values(
        'pythondirection', 'num').order_by('-num')
    direction = []
    for i in res[:12]:
        direction.append({'pythondirection': i.get('pythondirection'), 'num': i.get('num')})
    # print(direction)

    return render(requests, 'index.html', {'all_obj': len(all_obj),
                                           'jinrong_lis': jinrong_lis.__len__(),
                                           'wenyu_lis': wenyu_lis.__len__(),
                                           'xinxianquan_lis': xinxianquan_lis.__len__(),
                                           'qiyefuwu_lis': qiyefuwu_lis.__len__(),
                                           'yiliiao_lis': yiliiao_lis.__len__(),
                                           'shuju_lis': shuju_lis.__len__(),
                                           'xiaofei_lis': xiaofei_lis.__len__(),
                                           'avgsairly': int(avgsairly.get('salary__avg') * 1000),
                                           'haidian': len(haidian),
                                           'chaoyang': len(chaoyang),
                                           'dongcheng': len(dongcheng),
                                           'xicheng': len(xicheng),
                                           'changping': len(changping),
                                           'else_district': else_district,
                                           'first': direction[0],
                                           'second': direction[1],
                                           'third': direction[2],
                                           'four': direction[3],
                                           'five': direction[4],
                                           'six': direction[5],
                                           'beijing_salary': beijing_salary,
                                           'beijing_count': beijing_count,
                                           'shanghai_salary': shanghai_salary,
                                           'shanghai_count': shanghai_count,
                                           'hangzhou_salary': hangzhou_salary,
                                           'hangzhou_count': hangzhou_count,
                                           'top_salary': top_salary,
                                           })
