from django.db import models


# Create your models here.
class TriClub(models.Model):
    companyName = models.CharField(max_length=64, )
    city = models.CharField(max_length=10, )
    district = models.CharField(max_length=10, )
    address = models.CharField(max_length=64, )
    companyfield = models.CharField(max_length=20)
    positionname = models.CharField(max_length=20)
    pythondirection = models.CharField(max_length=20)
    salary = models.IntegerField()
    education = models.CharField(max_length=10)
    workyear = models.CharField(max_length=10)
    positiondesc = models.CharField(max_length=1600)
    requiremnents = models.CharField(max_length=10)
    companysize = models.CharField(max_length=20)


