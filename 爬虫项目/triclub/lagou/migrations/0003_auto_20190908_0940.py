# -*- coding: utf-8 -*-
# Generated by Django 1.11.23 on 2019-09-08 09:40
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('lagou', '0002_auto_20190908_0937'),
    ]

    operations = [
        migrations.AlterField(
            model_name='triclub',
            name='address',
            field=models.CharField(max_length=64),
        ),
    ]
