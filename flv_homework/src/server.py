''''''
import os
import sys
import json
import hashlib
import struct
import socketserver
from cofig.setting import serverfilepath
def get_md5(data):
    obj = hashlib.md5(data.encode('utf-8'))
    obj.update(data.encode('utf-8'))
    result = obj.hexdigest()
    return result

def surplus_size(user_path):
    '''
    用户剩余空间大小
    :param user_path: 用户的文件位置
    :return: 返回剩余空间大小
    '''
    size = 0
    ret = os.walk(user_path)
    for i, v, m in ret:
        for item in m:
            size += os.path.getsize(os.path.join(i, item))
    return 600000 - size
def my_send(my_dic,sk):
    str_dic = json.dumps(my_dic)
    number = struct.pack('i', len(str_dic.encode('utf-8')))
    print(number)
    sk.send(number)
    sk.send(str_dic.encode('utf-8'))
def my_recv(sk):
    msg = sk.recv(4)
    print(msg)
    number = struct.unpack('i',msg)[0]
    msg = sk.recv(number).decode('utf-8')
    rec_dic = json.loads(msg)
    print(rec_dic)
    return rec_dic
def download(user_dic,sk):

    filename = os.path.join(serverfilepath,user_dic['user'])
    everyfile = os.listdir(filename)
    user_dic['info'] = everyfile
    my_send(user_dic,sk)   #发送所有网盘中的文件名dic
    dic = my_recv(sk)      #接收要下载的文件名
    filename = os.path.join(filename, dic['info'])
    dic['size'] = os.path.getsize(filename)   #得到发送文件的大小
    my_send(dic,sk)     #将大小发送过去
    with open(filename,'rb')as f:
        while dic['size'] >2048:
            a = f.read(2048)
            sk.send(a)
            dic['size'] = dic['size'] - 2048
        else:
            while dic['size']:
                a = f.read(dic['size'])
                sk.send(a)
                return
def upload(dic,sk):
    file_dic = my_recv(sk)
    filenamepath = os.path.join(serverfilepath,dic['user'],file_dic['name'])
    with open(filenamepath,'wb')as f:
        while file_dic['size']>2048:
            con = sk.recv(2048)
            f.write(con)
            file_dic['size'] -= len(con)
        else:
            while file_dic['size']:
                con = sk.recv(file_dic['size'])
                f.write(con)
                return

def open_file(lis,dic,sk):
    '''
    打开文件夹
    :param dic:
    :param sk:
    :return:
    '''
    filename = os.path.join(lis[-1],dic['info'])
    print(filename)
    lis.append(filename)
    dic['info'] = os.listdir(filename)
    my_send(dic,sk)
    return filename
def del_file(lis,dic,sk):
    filename = os.path.join(lis[-1],dic['info'])
    os.remove(filename)
    return lis.pop()
def makefile(lis,dic,sk):
    filename = os.path.join(lis[-1],dic['info'])
    filename = os.makedirs(filename)
    return filename
def client_close(lis,dic,sk):
    lis.pop()
    if not lis:
        sys.exit(0)
    ret = os.listdir(lis[-1])
    dic['info'] = ret
    my_send(dic,sk)
def revise_file(dic,sk):
    '''
    查看修改文件目录路径
    :return:
    '''
    lis = []
    filename = os.path.join(serverfilepath,dic['user'])
    lis.append(filename)
    while True:
        everyfile = os.listdir(lis[-1])
        print(lis[-1])
        print(everyfile)
        dic['info'] = everyfile
        my_send(dic,sk)
        receive_dic = my_recv(sk)
        filename = getattr(sys.modules[__name__],receive_dic['operation'])(lis,receive_dic,sk)
        if filename == False:
            break


class Myserver(socketserver.BaseRequestHandler):
    def handle(self):  # 自动触发了handle方法，并且self.request == conn
        while True:
            dic = my_recv(self.request)
            '''注册代码检测'''
            if dic['info'] == 'reagest':
                with open(r'E:\homework\flv_homework\users\usermasage.txt','a+',encoding='utf-8')as f:
                    f.seek(0)
                    for line in f:
                        line = line.strip()
                        user_dic = json.loads(line)
                        if dic['user'] == user_dic['user']:
                            dic['info'] = False
                    my_send(dic,self.request)
                    if dic['info'] == False:
                        continue
                    else:
                        dic['info'] = 'vip'
                        dic['pasward'] = get_md5(dic['pasward'])
                        print(dic)
                        json.dump(dic,f)
                        f.write('\n')
                        filename = os.path.join(serverfilepath,dic['user'])
                        os.makedirs(filename)
                        continue
            '''登录代码检测'''
            if dic['info'] == 'login':
                with open(r'E:\homework\flv_homework\users\usermasage.txt','r',encoding='utf-8')as f:
                    for line in f:
                        line = line.strip()
                        user_dic = json.loads(line)
                        print(user_dic['pasward'],'   ',get_md5(dic['pasward']))
                        if user_dic['user'] == dic['user'] and user_dic['pasward'] == get_md5(dic['pasward']):
                            dic['info'] = True
                            print(dic,'666')
                            my_send(dic,self.request)
                            break
                        else:
                            my_send(dic,self.request)
            if dic['info'] == True:
                break
        while True:
            choice_dic = my_recv(self.request)
            print('555',choice_dic)
            print('666',user_dic)
            getattr(sys.modules[__name__],choice_dic['info'])(user_dic,self.request)

server = socketserver.ThreadingTCPServer(('127.0.0.1',9000),Myserver)
server.serve_forever()