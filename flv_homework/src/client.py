''''''
import sys
import os
import json
import struct
import socket
import random
from cofig.setting import clientfilepath,myfilepath
def processBar(num, total):
    """
    进度条
    :param num: 接收到字节数
    :param total: 总字节数
    :return:
    """
    rate = num / total
    rate_num = int(rate * 100)
    if total- num<=2408:
        r = "\r%s>%d%%\n" % ("=" * 100, 100,)
    else:
        r = "\r%s>%d%%" % ("=" * rate_num, rate_num)
    sys.stdout.write(r)
    sys.stdout.flush()
def myprint(a):
    lis = ['\033[1;31;0m', '\033[1;32;0m', '\033[1;33;0m', '\033[1;34;0m', '\033[1;35;0m', '\033[1;36;0m', '\033[1;37;0m']
    colour = random.randint(0,6)
    colour1 = random.randint(0,6)
    return print(lis[colour]+ str(a)),print('')
def myinput(a):
    lis = ['\033[1;31;0m', '\033[1;32;0m', '\033[1;33;0m', '\033[1;34;0m', '\033[1;35;0m', '\033[1;36;0m', '\033[1;37;0m']
    colour = random.randint(0, 6)
    return input(lis[colour] + a)

def my_send(my_dic,sk):
    str_dic = json.dumps(my_dic).encode('utf-8')
    number = struct.pack('i', len(str_dic))
    sk.send(number)
    sk.send(str_dic)
def my_recv(sk):
    msg = sk.recv(4)
    number = struct.unpack('i', msg)[0]
    msg = sk.recv(number).decode('utf-8')
    rec_dic = json.loads(msg)
    return rec_dic
def zhucedenglu(sk):
    while True:
        print('''
        1. 注册
        2. 登录
        ''')
        choice_dic = {'1':'reagest','2':'login'}
        my_dic = {}
        choice = input('请输入选择序号：')
        if not choice_dic.get(choice):
            myprint('您选择序号有误！')
            continue
        else:
            my_dic['user'] = myinput('输入昵称：')
            my_dic['pasward'] = myinput('输入密码：')
            my_dic['info'] = choice_dic[choice]
            my_send(my_dic,sk)    #发送过去的字典
        rec_dic = my_recv(sk)    #接收过来的字典
        if choice == '1':
            if rec_dic['info']:
                myprint('注册成功')
            else:
                myprint('注册失败')
            continue
        if choice == '2':
            myprint(rec_dic['info'])
            if rec_dic['info']== True:
                myprint('登录成功')
                return
            myprint('账号或密码错误，请重新输入')
def download(sk):
    server_dic = my_recv(sk)
    while True:
        my_dic = {}
        for i in server_dic['info']:
            myprint(i)
        file = myinput('输入选择下载的文件名或Q退出：')
        if file.upper() == 'Q':
            return 'Q'
        if file not in server_dic['info']:
            myprint('您输入的文件名有误，请重新输入')
            continue
        my_dic['info'] = file
        my_send(my_dic, sk)
        size_dic = my_recv(sk)  # 文件名，和文件大小的字典
        downloadfile = os.path.join(clientfilepath, my_dic['info'])
        with open(downloadfile, 'wb')as f:
            num = 0
            number = size_dic['size']
            while size_dic['size'] > 2048:
                con = sk.recv(2048)
                f.write(con)
                size_dic['size'] -= len(con)
                num += len(con)
                processBar(num,number)
            else:
                while size_dic['size']:
                    con = sk.recv(size_dic['size'])
                    f.write(con)
                    processBar(num,number)
                    myprint('文件下载完成')
                    return
def upload(sk):
    while True:
        file_dic = {}
        file_lis = os.listdir(myfilepath)
        for i in file_lis:
            print(i)
        choice_file = input('输入你要上传的文件名：')
        filenamepath = os.path.join(myfilepath,choice_file)
        if not os.path.exists(filenamepath):
            continue
        size = os.path.getsize(filenamepath)
        file_dic['size'] = size
        file_dic['name'] = choice_file
        my_send(file_dic,sk)    #发送要发的文件名和文件大小
        with open(filenamepath,'rb')as f1:
            while file_dic['size'] > 2048:
                a = f1.read(2048)
                sk.send(a)
                file_dic['size'] -= 2048
            else:
                while file_dic['size']:
                    a = f1.read(file_dic['size'])
                    sk.send(a)
                    myprint('文件上传完成')
                    return 'md5'
def open_file(sk):
    msg = my_recv(sk).decode('utf-8')
    for i in msg['info']:
        print(i)

def del_file(sk):
    pass
def makefile(sk):
    pass
def client_close(sk):
    sk.close()
    sys.exit(0)
def revise_file(sk):
    '''
    查看修改目录函数
    字典makenewfile为创建
    delfile为删除

    :param sk:
    :return:
    '''
    while True:
        my_dic = {}
        operation_dic = {'1':'open_file','2':'del_file','3':'download','4':'upload','5':'makefile','6':'client_close'}
        everyfile_dic = my_recv(sk)
        for i in everyfile_dic['info']:
            myprint(i)
        filename = myinput('输入你要操作的文件名或6返回：')
        if filename == '6':
            everyfile_dic['operation'] = 'client_close'
            my_send(everyfile_dic,sk)
            continue
        myprint('''
        1.打开
        2.删除
        3.下载
        4.上传
        5.新建文件夹
        6.退出
        ''')
        operation = myinput('输入你对文件操作的序号：')
        everyfile_dic['info'] = filename
        everyfile_dic['operation'] = operation_dic[operation]
        my_send(everyfile_dic,sk)



def star():
    sk = socket.socket()
    sk.connect(('127.0.0.1',9000))
    while True:
        print('''
        1. 注册
        2. 登录
        
        ''')
        choice_dic = {'1':'reagest','2':'login'}
        my_dic = {}
        choice = myinput('请输入选择序号：')
        if not choice_dic.get(choice):
            myprint('您选择序号有误！')
            continue
        else:
            my_dic['user'] = myinput('输入昵称：')
            my_dic['pasward'] = myinput('输入密码：')
            my_dic['info'] = choice_dic[choice]
            my_send(my_dic,sk)    #发送过去的字典
        rec_dic = my_recv(sk)    #接收过来的字典
        if choice == '1':
            if rec_dic['info']:
                myprint('注册成功')
            else:
                myprint('注册失败')
            continue
        if choice == '2':
            myprint(rec_dic['info'])
            print(rec_dic['info'])
            if rec_dic['info']== True:
                myprint('登录成功')
                break
            myprint('账号或密码错误，请重新输入')
    while True:
        dic = {'1':'download','2':'upload','3':'revise_file','4':'client_close'}
        myprint('''
        ***********************选择服务*************************
        1.下载
        2.上传
        3.查看修改文件目录
        4.退出
        ''')
        choice  = myinput('选择序号')
        if not dic[choice]:
            myprint('您输入的序号有误，请从新输入')
            continue
        choice_dic['info'] = dic[choice]
        my_send(choice_dic,sk)
        myprint(dic)
        a = getattr(sys.modules[__name__],dic[choice])(sk)
    myprint('aaa')






star()