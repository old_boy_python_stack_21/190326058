from django.db import models

# Create your models here.

class Menu(models.Model):
    title = models.CharField(max_length=32)
    i_con  = models.CharField(max_length=50)
    weight = models.IntegerField(default=1)
    def __str__(self):
        return self.title

class Power(models.Model):
    '''
    url 表
    权限表
    '''
    name = models.CharField(max_length=32,verbose_name='名称')
    revname = models.CharField(max_length=32,verbose_name='反解析名字',unique=True)
    url_title = models.CharField(max_length=32,verbose_name='权限URL')
    menu = models.ForeignKey('Menu',blank=True,null=True)
    fapower = models.ForeignKey('self',verbose_name='父级菜单',blank=True,null=True)

    def __str__(self):
        return '|'.join([self.url_title,self.name])

class Status(models.Model):
    '''
    职位表
    level 级别
    '''
    level = models.CharField(verbose_name='职位',max_length=32)
    powers = models.ManyToManyField('Power')
    def __str__(self):
        return self.level
class Userbod(models.Model):
    '''
    用户表
    '''
    username = models.CharField(verbose_name='账号',max_length=32)
    password = models.CharField(verbose_name='密码',max_length=32)
    status = models.ManyToManyField('Status')
    def __str__(self):
        return self.username