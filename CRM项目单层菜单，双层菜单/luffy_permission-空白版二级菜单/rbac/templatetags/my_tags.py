from django.template import Library
from collections import OrderedDict
import re

register = Library()


@register.inclusion_tag('qxmenu.html')
def qxmenu(request):
    od = OrderedDict()
    dat = request.session['pom']
    key_lis = sorted(dat, key=lambda x: dat[x]['weight'], reverse=True)
    print(key_lis)
    for i in key_lis:
        print(i)
        od[i] = dat[i]
    url = request.path
    for i in od.values():
        i['class'] = 'hide'
        for m in i['children']:
            if re.match(m['url'], url):
                i['class'] = ''
                m['class'] = 'active'
        for r in i['childrens']:
            if re.match(r['url'],url):
                i['class'] = ''
                r['class'] = 'active'
    print(od, '999999')
    return {'menu_dict': od}


@register.inclusion_tag('daohang.html')
def daohang(reuqest):
    return {'daohang': reuqest.daohang}


@register.filter()
def quanxianyincang(request,revname):
    lisr = []
    lis = request.session['sta']
    for i in lis:
        lisr.append(i['powers__revname'])
    if revname in lisr:
        return True
    return False