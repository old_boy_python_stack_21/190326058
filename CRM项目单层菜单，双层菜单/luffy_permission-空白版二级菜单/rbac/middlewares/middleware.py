from django.utils.deprecation import MiddlewareMixin
from django.urls import reverse
from django.shortcuts import redirect, HttpResponse
from django.conf import settings
import re


class Md1(MiddlewareMixin):
    def process_request(self, request):
        request.daohang = [{'首页': reverse('index')}]
        path = request.path_info
        if path in [reverse('login')]:
            return
        if re.match('/admin/', path):
            return
        is_login = request.session.get('is_login')
        if not is_login:
            return redirect(reverse('login'))

        if path in [reverse('index')]:
            return
        sta = request.session.get('sta')
        print(sta)
        for i in sta:
            if re.match(i['powers__url_title'], path):
                print(i['powers__url_title'], path)
                if not i['powers__fapower_id']:
                    request.daohang.append({i['powers__name']: i['powers__url_title']})
                else:
                    for s in sta:
                        if s['powers__id'] == i['powers__fapower_id']:
                            request.daohang.append({s['powers__name']: s['powers__url_title']})
                            request.daohang.append({i['powers__name']: i['powers__url_title']})
                print(request.daohang)
                return

        #
        # for i in pom.keys():
        #     for r in pom[i]['children']:
        #         if re.match(r['url'],path):
        #             return
        return HttpResponse('访问权限不够')
