from django.shortcuts import render,redirect,HttpResponse,reverse
from rbac import models
from rbac.forms_view import Powerform
from django.db.models import Q


# Create your views here.


def menu_lis(request):
    pk = request.GET.get('pk')
    dic = {}
    menu_obj_lis = models.Menu.objects.all().values('title','i_con','weight','power__id','id')
    power_obj_lis = models.Power.objects.all().values('id','name','url_title','menu','fapower_id','id','revname','fapower')
    if pk:
        power_obj_lis = models.Power.objects.filter(Q(menu_id=pk)|Q(fapower__menu_id=pk)).values('id', 'name', 'url_title', 'menu', 'fapower_id', 'id',
                                                          'revname', 'fapower')
    for obj in power_obj_lis:
        if obj['menu']:
            dic[obj['id']]={'name':obj['name'],'url':obj['url_title'],'id':obj['id'],'revname':obj['revname'],'menu':obj['menu'],'fapower_id':obj['fapower_id'],'fapower':obj['fapower'],'children':[]}
    for obj in power_obj_lis:
        if obj['fapower_id']:
            dic[obj['fapower_id']]['children'].append({'name':obj['name'],'url':obj['url_title'],'id':obj['id'],'revname':obj['revname'],'fapower':obj['fapower'],'fapower_id':obj['fapower_id']})

    print(dic)

    return render(request,'menu_lis.html',{'menu_obj_lis':menu_obj_lis,'dic':dic,'pk':pk})


def power_add(request,pk=None):
    obj = models.Menu.objects.filter(pk=pk).first()
    for_obj = Powerform(instance=obj)
    if request.method == 'POST':
        for_obj = Powerform(data=request.POST,instance=obj)
        if for_obj.is_valid():
            for_obj.save()
            return redirect(reverse('menu_list'))


    return render(request,'form.html',{'for_obj':for_obj})
