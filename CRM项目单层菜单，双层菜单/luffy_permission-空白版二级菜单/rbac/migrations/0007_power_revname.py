# -*- coding: utf-8 -*-
# Generated by Django 1.11.21 on 2019-07-16 08:28
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('rbac', '0006_menu_weight'),
    ]

    operations = [
        migrations.AddField(
            model_name='power',
            name='revname',
            field=models.CharField(default=1, max_length=32, verbose_name='反解析名字'),
            preserve_default=False,
        ),
    ]
