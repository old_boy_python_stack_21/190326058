# -*- coding: utf-8 -*-
# Generated by Django 1.11.21 on 2019-07-15 06:20
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('rbac', '0002_auto_20190712_1551'),
    ]

    operations = [
        migrations.AddField(
            model_name='power',
            name='fapower',
            field=models.ForeignKey(default=None, on_delete=django.db.models.deletion.CASCADE, to='rbac.Power', verbose_name='父级菜单'),
        ),
    ]
