from django.shortcuts import render, redirect, reverse
from django.http import FileResponse
from rbac.servers.quanxian import get_power

from rbac import models


def login(request):
    error = ''
    if request.method == 'POST':
        name = request.POST.get('username')
        password = request.POST.get('password')
        obj = models.Userbod.objects.filter(username=name, password=password).first()
        if obj:
            request.session['is_login'] = True
            url_i = obj.status.filter(powers__url_title__isnull=False).values('powers__url_title', 'powers__menu_id',
                                                                              'powers__fapower_id', 'powers__id',
                                                                              'powers__name','powers__revname').distinct()
            url = obj.status.filter(powers__url_title__isnull=False).values('powers__menu__weight', 'powers__url_title',
                                                                            'powers__name', 'powers__menu_id',
                                                                            'powers__menu__title',
                                                                            'powers__menu__i_con','powers__fapower__menu_id').distinct()

            lis = []
            print(url, '666666')

            for i in url_i:
                lis.append(i)
            print(lis, '**++' * 5)
            request.session['sta'] = lis
            dat = get_power(list(url))
            request.session['pom'] = dat

            return redirect(reverse('index'))
        error = '账号或密码错误'
    return render(request, 'login.html', {'error': error})


def index(request):
    return render(request, 'index.html')
