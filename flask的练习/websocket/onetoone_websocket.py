import json

from flask import Flask, request, render_template
from geventwebsocket.handler import WebSocketHandler
from geventwebsocket.server import WSGIServer
from geventwebsocket.websocket import WebSocket


app = Flask(__name__)

dic={}

@app.route('/ws/<username>')
def funa(username):
    print('666666666')
    my_socket = request.environ.get('wsgi.websocket')   #type:WebSocket
    dic[username] = my_socket
    print(my_socket,dic)
    while True:
        msg = my_socket.receive()
        print(msg)
        messg = json.loads(msg)
        sender = messg.get('sender')
        dic[sender].send(msg)

@app.route('/ind')
def index():
    return render_template('onetoone.html')


if __name__ == '__main__':
    http_server = WSGIServer(('127.0.0.1', 8827), app, handler_class=WebSocketHandler)
    http_server.serve_forever()
