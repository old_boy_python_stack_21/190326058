from flask import Blueprint, request, render_template, redirect, url_for, session
from pymongo import MongoClient
import random

use = Blueprint('app01', __name__)
MC = MongoClient('127.0.0.1', 27017)
MongoDB = MC['S21DAY93']
secret_key = 'askjdhalkdsalsdlaskjdlaskdas'
'''
Player:
{
    UserName:"Player1",
    ATC:20,
    DEF:20,
    LIFE:300,
    Equip:[]
    Package:[
        {name:"大砍刀",atc:20,def:-5,life:0},
        {name:"黄金甲",atc:-5,def:20,life:200},
        {name:"小红药",atc:5,def:0,life:100},
    ]
}
'''


@use.route('/reg', endpoint='reg', methods=['GET', 'POST'])
def reg():
    if request.method == 'GET':
        return render_template('reg.html')
    else:
        dic = request.form.to_dict()
        MongoDB.Users.insert_one(dic)
        MongoDB.Player.insert_one({'UserName': dic['name'],
                                   'ATC': 20,
                                   'DEF': 20,
                                   'LIFE': 300,
                                   'Equip': [],
                                   'Package': [
                                       {'name': "大砍刀", 'atc': 20, 'def': -5, 'life': 0},
                                       {'name': "黄金甲", 'atc': -5, 'def': 20, 'life': 200},
                                       {'name': "小红药", 'atc': 5, 'def': 0, 'life': 100},
                                   ]})
        return redirect(url_for('app01.login'))


@use.route('/login', endpoint='login', methods=['GET', 'POST'])
def login():
    if request.method == 'GET':
        return render_template('login.html')
    else:
        dic = request.form.to_dict()
        ret = MongoDB.Users.find(dic)
        if ret:
            print(dic['name'])
            session['name'] = dic['name']
            print(session)
            return redirect(url_for('app01.index'))
        else:
            return redirect(url_for('app01.login'))


@use.route('/index')
def index():
    lis = []
    dec = {'enu': []}
    name = session.get('name')
    print(name)
    ret = MongoDB.Player.find({'UserName': name})
    for i in ret:
        ret = i
    req = MongoDB.Player.find({'UserName': {'$ne': name}})
    for i in req:
        lis.append(i)
    return render_template('index.html', res=ret, req=lis)


@use.route('/put/<euq>')
def put(euq):
    name = session.get('name')
    ret = MongoDB.Player.find({'UserName': name})
    for i in ret:
        ret = i
    for i in (ret['Package']):
        if i.get('name') == euq:
            break
    print(i, '666', ret)
    ret['Package'].remove(i)
    ret['Equip'].append(i)
    ret['ATC'] += i['atc']
    ret['DEF'] += i['def']
    ret['LIFE'] += i['life']
    ret = MongoDB.Player.update({'UserName': name}, {'$set': ret})
    return redirect('/index')


@use.route('/take_off/<euq>')
def take_off(euq):
    name = session.get('name')
    ret = MongoDB.Player.find({'UserName': name})
    for i in ret:
        ret = i
    for i in (ret['Equip']):
        if i.get('name') == euq:
            break
    print(i, '666', ret)
    ret['Equip'].remove(i)
    ret['Package'].append(i)
    ret['ATC'] -= i['atc']
    ret['DEF'] -= i['def']
    ret['LIFE'] -= i['life']
    ret = MongoDB.Player.update({'UserName': name}, {'$set': ret})
    return redirect('/index')


@use.route('/play/<username>')
def play(username):
    name = session.get('name')
    ret = MongoDB.Player.find({'UserName': name})
    res = MongoDB.Player.find({'UserName': username})
    dic = {'PlayList': [name, username],
           'PK_list': [

           ]
           }
    while True:
        gong = ret['ATC'] * random.uniform(0, 2) - res['DEF'] * random.uniform(0, 2)
        if gong <= 0:
            gong = 0
        res['LIFE'] -= gong
        dic1 = {
            'atcplayer': name,
            'defplayer': username,
            'info': f"{name} atc {username} ,{username} life -{gong} "
        }
        dic['PK_list'].append(dic1)
        if res['LIFE'] <= 0:
            msg = f'{res["name"]},死亡'

            MongoDB.log.insert_one(dic)
            return render_template('result.html', dic=dic, msg=msg)
        gong = res['ATC'] * random.uniform(0, 2) - ret['DEF'] * random.uniform(0, 2)
        if gong <= 0:
            gong = 0
        ret['LIFE'] -= gong
        dic1 = {
            'atcplayer': username,
            'defplayer': name,
            'info': f"{username} atc {name} ,{name} life -{gong} "
        }
        dic['PK_list'].append(dic1)
        if ret['LIFE'] <= 0:
            msg = f'{res["name"]},死亡'
            MongoDB.log.insert_one(dic)
            return render_template('result.html', dic=dic, msg=msg)
