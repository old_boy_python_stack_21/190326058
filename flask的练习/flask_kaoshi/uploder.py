import json
import os
import time
from uuid import uuid4

from flask import Blueprint, request, render_template, redirect, session, jsonify

from Config import MongoDB,IMG,REDIS_CLI,RET,CHAT_PATH,IMG_PATH

loader = Blueprint('loader',__name__)


@loader.route('/to_uploader',methods=['post'])
def to_uploader():
    dic = {}
    a = uuid4()
    re_dic = request.form.to_dict()
    user_id = re_dic.get('user_id')
    to_user = re_dic.get('to_user')
    print(re_dic)
    if request.files.get('reco'):
        file = request.files.get('reco')
        print(file.filename)
        file_path = os.path.join(CHAT_PATH,f'{a}.mp3')
        file.save(file_path)
        dic['chat'] = f'{a}.mp3'
        RET['DATA'] = {'filename': f'{a}.mp3'}
    else:
        file = request.files.get('img')
        print(file.filename)
        file_path = os.path.join(IMG_PATH, f'{a}.img')
        file.save(file_path)
        dic['img'] = f'{a}.img'
        RET['DATA'] = {'filename': f'{a}.img'}

    dic['from_user'] = user_id
    dic['to_user'] = to_user

    dic['create_time'] = time.time()

    chat_obj = MongoDB.chats.update_one({'user_list':{'$all':[user_id,to_user]}},{'$push':{'chat_list':dic}})


    RET['CODE'] = 0
    RET['MSG'] = '上传成功'
    RET['TIME'] = time.time()

    return jsonify(RET)