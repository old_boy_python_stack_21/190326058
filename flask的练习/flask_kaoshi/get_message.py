import json
import os
import time
from uuid import uuid4

from flask import Blueprint, request, render_template, redirect, session, jsonify, send_file

from Config import MongoDB,IMG,REDIS_CLI,RET,CHAT_PATH,IMG_PATH

get_ms = Blueprint('get_ms',__name__)


@get_ms.route('/get_music/<name>')
def get_music(name):
    file_path = os.path.join(CHAT_PATH,name)
    return send_file(file_path)



@get_ms.route('/get_img/<name>')
def get_img(name):
    file_path = os.path.join(IMG_PATH,name)
    return send_file(file_path)