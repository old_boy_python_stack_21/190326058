import os

from flask import Blueprint, request, render_template,redirect,session

from Config import MongoDB,IMG

user = Blueprint('user',__name__)




@user.route("/reg", methods=["GET", "POST"])
def reg():
    if request.method == "GET":
        return render_template("reg.html")
    else:
        ret_dic = request.form.to_dict()
        username = ret_dic.get('username')
        password = ret_dic.get('password')
        file = request.files.get('file')
        file_path = os.path.join(IMG,username+'.jpg')
        file.save(file_path)
        dic = {}
        dic['username'] = username
        dic['password'] = password
        dic['avatar']  = username+'.jpg'
        obj = MongoDB.users.find_one({'username':username})
        if not obj:
            MongoDB.users.insert_one(dic)
            return redirect('/login')
        return '用户名已存在'






@user.route("/login", methods=["GET", "POST"])
def login():
    if request.method == "GET":
        return render_template("login.html")
    # 如果是 POST 请求 获取用户名密码 校验
    else:  # 405 请求方式不被允许
        ret_dic = request.form.to_dict()
        username = ret_dic.get('username')
        password = ret_dic.get('password')
        obj  = MongoDB.users.find_one({'username':username,'password':password})
        if obj:
            session['username'] = username
            return redirect('/index')
        return '账号或密码错误'

