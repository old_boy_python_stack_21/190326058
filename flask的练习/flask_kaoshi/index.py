import json
import os

from flask import Blueprint, request, render_template,redirect,session

from Config import MongoDB,IMG,REDIS_CLI

ind = Blueprint('ind',__name__)

dic = {}



@ind.route('/index')
def index():
    di=REDIS_CLI.get('di')
    num = []
    count = 0
    if di:
        num = json.loads(di)
        print(num)
        count = len(num)
    if session.get('username'):
        username = session.get('username')
        if username in num:
            num.remove(username)
        return render_template('index.html',username= username,count=count,num=num)
    return redirect('/login')

@ind.route('/one/<name>')
def one(name):
    if session.get('username'):
        username = session.get('username')
        dic = {}
        dic['user_list'] = [username,name]
        dic['chat_list'] = []
        obj = MongoDB.chats.find_one({'user_list':{'$all':[username,name]}})
        if not obj:
            MongoDB.chats.insert_one(dic)
        return render_template('onetoone.html',username=username,name=name)
    return redirect('/login')