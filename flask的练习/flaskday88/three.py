from flask import Blueprint, views, render_template, request, session, redirect, url_for

STUDENT_DICT = {
    1: {'name': 'Old', 'age': 38, 'gender': '中'},
    2: {'name': 'Boy', 'age': 73, 'gender': '男'},
    3: {'name': 'EDU', 'age': 84, 'gender': '女'},
}
ab = Blueprint('ab',__name__)
ac = Blueprint('ac',__name__)

class Login(views.MethodView):
    def get(self,*args,**kwargs):
        return render_template('login.html')
    def post(self,*args,**kwargs):
        username = request.form.get('username')
        password = request.form.get('password')
        session['user'] = username
        return redirect(url_for('ac.index'))

ab.add_url_rule('/login',view_func=Login.as_view(name='login'))



class Index(views.MethodView):
    def get(self,*args,**kwargs):
        if request.args.get('id'):
            id = request.args.get('id')
            a = STUDENT_DICT[int(request.args.get('id'), 1)]
            print(a)
            return render_template('index_pro.html',dic=STUDENT_DICT.get(int(id)), id=id )
        return render_template('index.html',dic=STUDENT_DICT)

ac.add_url_rule('/index',view_func=Index.as_view(name='index'))

