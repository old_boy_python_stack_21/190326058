from flask import Blueprint, request, render_template, session, redirect
from flask import views

STUDENT_DICT = {
    1: {'name': 'Old', 'age': 38, 'gender': '中'},
    2: {'name': 'Boy', 'age': 73, 'gender': '男'},
    3: {'name': 'EDU', 'age': 84, 'gender': '女'},
}
ac = Blueprint('app02', __name__)


class Index(views.MethodView):
    def get(self, *args, **kwargs):
        id = request.args.get('id')
        if id:
            print(id, type(id))
            dic = STUDENT_DICT.get(id)
            print(dic)
            return render_template('index_pro.html', dic=STUDENT_DICT.get(int(id)), id=id)
        return render_template('index.html', dic=STUDENT_DICT)


ac.add_url_rule('/index', view_func=Index.as_view(name='index'))

ab = Blueprint('app01', __name__)


class Login(views.MethodView):
    def get(self, *args, **kwargs):
        return render_template('login.html')

    def post(self, *args, **kwargs):
        dic = request.form.to_dict()
        username = dic.get('username')
        password = dic.get('password')
        if username == 'alex' and password == '123':
            session['user'] = username
            print(session, '*' * 32)
            return redirect('/index')
        return render_template('login.html', ereror='账号或密码错误')
ab.add_url_rule('/login',view_func=Login.as_view(name='login'))
