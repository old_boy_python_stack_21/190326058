from flask import Flask, views

app = Flask(__name__)


class Login(views.MethodView):

    def get(self, *args, **kwargs):
        return 'here is get'

    def post(self, *args, **kwargs):
        return 'here is post'

app.__call__()
app.add_url_rule('/login', view_func=Login.as_view(name='m_login',))

if __name__ == '__main__':
    app.run()