import json

from Config import REDIS_CLI,MongoDB

def set_count(sender,reciver):
    ret = REDIS_CLI.get(reciver)
    if ret:
        # print('设置+1条')
        ret = json.loads(ret)
        if ret.get(sender):
            ret[sender] += 1
        else:
            ret[sender] = 1
        REDIS_CLI.set(reciver,json.dumps(ret))
    else:
        # print('设置1条')
        ret = {sender:1}
        REDIS_CLI.set(reciver,json.dumps(ret))


def get_count(sender,reciver):
    count = 0
    if REDIS_CLI.get(reciver):
        print('*********************')
        ret = json.loads(REDIS_CLI.get(reciver))
        count = ret.get(sender,0)
    REDIS_CLI.set(reciver,json.dumps({sender:0}))
    print(count,'-+-+-+-+-+')
    return count



def get_count_all(reciver):
    count = 0
    if REDIS_CLI.get(reciver):
        red = json.loads(REDIS_CLI.get(reciver))
        for sender,count in red.items():
            if count != 0:
                red[sender] = 0
                REDIS_CLI.set(reciver,json.dumps(red))
                return  sender,count

    #     print('*********************')
    #     ret = json.loads(REDIS_CLI.get(reciver))
    #     count = ret.get(sender,0)
    # REDIS_CLI.set(reciver,json.dumps({sender:0}))
    # print(count,'-+-+-+-+-+')
    return '小伙伴',count