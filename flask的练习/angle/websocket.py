import json

from flask import Flask, render_template, request
from geventwebsocket.handler import WebSocketHandler
from geventwebsocket.server import WSGIServer
from geventwebsocket.websocket import WebSocket
app = Flask(__name__)
lis = []
dic = {}
@app.route('/toy/<toy_id>')
def toy_tongxun(toy_id):
    my_socket = request.environ.get('wsgi.websocket')  # type:WebSocket
    print(my_socket,'toy')
    dic[toy_id] = my_socket
    while True:
        msg = my_socket.receive()
        msge = json.loads(msg)
        print(msge)
        user = msge.get('to_user')
        music = msge.get('music')
        dic[user].send(msg)


@app.route('/app/<app_id>')
def app_tongxun(app_id):
    my_socket = request.environ.get('wsgi.websocket')  # type:WebSocket
    print(my_socket,'app')
    dic[app_id] = my_socket
    while True:
        msg = my_socket.receive()
        msge = json.loads(msg)
        print(msge)
        user = msge.get('to_user')
        music = msge.get('music')
        get_user = dic.get(user)
        get_user.send(msg)





if __name__ == '__main__':
    http_server = WSGIServer(('0.0.0.0', 9528), app, handler_class=WebSocketHandler)
    http_server.serve_forever()