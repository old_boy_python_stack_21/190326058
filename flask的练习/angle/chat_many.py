import json
import os

from flask import Blueprint, request, jsonify, send_file
from Config import MongoDB, COVER_PATH, QR_PATH,CHAT_PATH,REDIS_CLI
from bson import ObjectId

fried = Blueprint('fried', __name__)

RET = {}

@fried.route('/friend_list',methods = ['post'])
def friend_list():
    ret = request.form.to_dict()
    ret['_id'] = ObjectId(ret['_id'])
    res = MongoDB.Users.find_one(ret)

    RET['CODE'] = 0
    RET['MSG'] = '好友查询'
    RET['DATA'] = res.get('friend_list',{})
    print(RET)
    return RET

@fried.route('/chat_list',methods=['post'])
def chat_list():
    ret = request.form.to_dict()
    chat_dic = MongoDB.Chats.find_one({'_id':ObjectId(ret['chat_id'])})
    user_list = chat_dic.get('user_list')
    print('***',ret)
    print('---',user_list)
    print('+++++++++++',chat_dic)

    re_dic = REDIS_CLI.get(ret['to_user'])
    if re_dic:
        re_dic = json.loads(re_dic)
        re_dic[ret['from_user']] = 0
        REDIS_CLI.set(ret['to_user'],json.dumps(re_dic))

    RET['CODE'] = 0
    RET['MSG'] = '查询聊天记录'
    RET['DATA'] = chat_dic.get('chat_list')[-5:]

    return RET


@fried.route('/get_chat/<name>.mp3')
def get_chat(name):
    file_path = os.path.join(CHAT_PATH,name+'.mp3')
    print(file_path)
    return send_file(file_path)