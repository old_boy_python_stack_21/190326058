import json
import os

from flask import Blueprint, request, jsonify, send_file
from Config import MongoDB, COVER_PATH, QR_PATH
from bson import ObjectId
from redis_count import get_count,set_count
from Config import REDIS_CLI



use = Blueprint('use', __name__)


@use.route('/reg', methods=['POST'])
def reg():
    res_dic = request.form.to_dict()
    dic = res_dic.get('username')
    ret = MongoDB.Users.find_one({'username': dic})
    if ret:
        return jsonify({'CODE': 999, 'MSG': '用户名存在,注册失败', 'data': {}})
    res_dic['bind_toys'] = []
    res_dic['friend_list'] = []

    MongoDB.Users.insert_one(res_dic)
    return jsonify({'CODE': 0, 'MSG': '注册成功', 'data': {}})


@use.route('/login', methods=['POST'])
def login():
    res_dic = request.form.to_dict()

    ret = MongoDB.Users.find_one(res_dic, {'password': 0})
    if ret:
        ret['_id'] = str(ret['_id'])
        ret['avatar'] = 'baba.jpg' if ret['gender'] == '2' else 'mama.jpg'
        ret['friend_list'] = []
        ret['bind_toy'] = []
        print(ret)
        if  REDIS_CLI.get(ret['_id']):
            re_dic = {}
            redis_dic = REDIS_CLI.get(ret['_id'])
            redis_dic = json.loads(redis_dic)
            ret['chat'] = redis_dic
            ret['chat']['count'] = sum(re_dic.values())


        else:
            ret['chat'] = {'count':0}
        dic = {
            "CODE": 0,
            "MSG": "登录成功",
            "DATA": ret
        }
        print(dic)
        return jsonify(dic)
    dic = {
        "CODE": 999,
        "msg": "登录失败",
        "DATA": {}
    }
    return dic


@use.route('/auto_login', methods=['post'])
def auto_login():
    res = request.form.to_dict()
    res['_id'] = ObjectId(res['_id'])
    ret = MongoDB.Users.find_one(res)
    print(ret)
    if ret:
        ret['_id'] = str(ret['_id'])
        ret['avatar'] = 'baba.jpg' if ret['gender'] == '2' else 'mama.jpg'
        ret['friend_list'] = []
        ret['bind_toy'] = []
        if REDIS_CLI.get(ret['_id']):
            re_dic = {}
            redis_dic = REDIS_CLI.get(ret['_id'])
            redis_dic = json.loads(redis_dic)
            ret['chat'] = redis_dic
            ret['chat']['count'] = sum(re_dic.values())
        else:
            ret['chat'] = {'count': 0}
        dic = {
            "CODE": 0,
            "msg": "登录成功",
            "DATA": ret,
        }
        print(dic)
        return jsonify(dic)
    print('666')
    return jsonify({"code": 11,
            "msg": "登录失败",
            "data": {},})


@use.route('/bind_toy',methods=['post'])
def bind_toy():
    a = request.form.to_dict()
    print(a)
    dic = {
	"user_list" : [ ],
	"chat_list" : []
}
    user_obj = MongoDB.Users.find_one({'_id':ObjectId(a['user_id'])})
    chat = MongoDB.Chats.insert_one(dic)

    dic1 = {
	"toy_name" : a['toy_name'],
	"baby_name" : a['baby_name'],
	"device_key" : a['device_key'],
	"avatar" : "toy.jpg",
	"bind_user" :a['user_id'],
	"friend_list" : [
		{
			"friend_id" : a['user_id'],
			"friend_nick" : user_obj['nickname'],
			"friend_remark" :a['remark'],
			"friend_avatar" : 'baba.jpg' if user_obj['gender'] == '2' else 'mama.jpg',
			"friend_chat" : str(chat.inserted_id),
			"friend_type" : "app"
		}]
}
    print(chat.inserted_id)
    new_chat = MongoDB.Chats.find_one({'_id':ObjectId(chat.inserted_id)})
    toy_obj = MongoDB.Toys.insert_one(dic1)
    MongoDB.Chats.update(new_chat,{'$set':{'user_list':[a['user_id'],str(toy_obj.inserted_id)]}})
    new_dic = dic1['friend_list'][0]
    new_dic['friend_id'] = str(toy_obj.inserted_id)
    new_dic['friend_type'] = "toy"
    new_dic['friend_remark'] = a['toy_name']
    print(new_dic)
    print(toy_obj.inserted_id)
    MongoDB.Users.update({'_id':ObjectId(a['user_id'])},{'$push':{'bind_toys':str(toy_obj.inserted_id)}})
    MongoDB.Users.update({'_id': ObjectId(a['user_id'])},
                         {'$push': { 'friend_list': new_dic},})
    return jsonify({
	"CODE":0,
	"MSG":"绑定完成",
	"DATA":{}
})

@use.route('/toy_list',methods=['post'])
def toy_list():
    lir = []
    lis = []
    dic = request.form.to_dict()
    users = MongoDB.Toys.find({'bind_user':dic['_id']})
    for i in users:
        diq = {}
        diq['_id']= str(i['_id'])
        diq['device_key'] = i['device_key']
        diq['bind_user'] = i['bind_user']
        diq['toy_name'] = i['toy_name']
        diq['avatar'] = i['avatar']
        diq['baby_name'] = i['baby_name']
        diq['gender'] = '1'
        diq['friend_list'] = []
        for r in i['friend_list']:
            dip = {}
            dip['friend_nickname'] = r['friend_nick']
            dip['friend_avatar'] = r['friend_avatar']
            dip['friend_remark'] = r['friend_remark']
            dip['friend_chat'] = r['friend_chat']
            diq['friend_list'].append(dip)
        lis.append(diq)
    print(lis)
    response_dic = {
        'CODE':0,
        'MSG':'获取Toy列表',
        'DATA':lis
    }
    return response_dic

@use.route('/get_qr/<name>.jpg')
def get_qr(name):
    filepath = os.path.join(QR_PATH,name+'.jpg')
    print(filepath,'***')
    return send_file(filepath)

