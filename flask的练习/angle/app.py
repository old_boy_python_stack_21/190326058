from flask import Flask, render_template
from get_message import ge
from user import use
from refriend import fre
from toy_open import toyyes
from chat_many import fried
from Uploader_yourself import uploader
from add_friend import add_fre
from ai_yuyin import ai
from friend_request import friend_request




app = Flask(__name__)

app.register_blueprint(ge)
app.register_blueprint(use)
app.register_blueprint(fre)
app.register_blueprint(toyyes)
app.register_blueprint(fried)
app.register_blueprint(uploader)
app.register_blueprint(ai)
app.register_blueprint(add_fre)
app.register_blueprint(friend_request)




@app.route('/web')
def web():
    return render_template('WebToy.html')


if __name__ == '__main__':
    app.run('0.0.0.0',9527)