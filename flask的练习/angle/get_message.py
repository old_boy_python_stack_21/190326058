import os

from flask import Blueprint, request, jsonify, send_file, session
from Config import MongoDB, COVER_PATH,MUSIC_PATH

ge = Blueprint('ge',__name__)


@ge.route('/content_list',methods=['POST'])
def content_list():
    response_dic = {
        'code':0,
        'msg':'获取内容资源列表',
        'data':[],
    }
    ret = MongoDB.Content.find({})
    for index, item in enumerate(ret):
        item['_id'] = str(item['_id'])
        response_dic['data'].append(item)
    return jsonify(response_dic)



@ge.route('/get_cover/<name>')
def get_cover(name):
    file_path = os.path.join(COVER_PATH,name)
    return send_file(file_path)

@ge.route('/get_music/<name>')
def get_music(name):
    file_path = os.path.join(MUSIC_PATH,name)
    return send_file(file_path)

@ge.route('/scan_qr',methods=['POST'])
def scan_qr():
    user_id = session.get('_id')
    dic = request.form.to_dict()
    ret = MongoDB.Devices.find_one(dic)
    if ret:
        print(ret)
        res = MongoDB.Toys.find_one(dic)
        if res:

            response_dic = {
                "CODE": 2,
                "MSG": "设备已经进行绑定",
                "DATA":
                {"toy_id": str(res['_id'])}
            }
            print('555', response_dic)

            return response_dic
        response_dic = {
            "CODE": 0,
            "MSG": "二维码扫描成功",
            "DATA":
                {'device_key':dic['device_key']}
        }
        print('666',response_dic)
        return response_dic
    response_dic = {
        "CODE": 1,
        "MSG": "请扫描玩具二维码",
        "DATA":
            {}
    }
    print('777', response_dic)
    return response_dic
