import os

from flask import Blueprint, request, jsonify, send_file
from Config import MongoDB, COVER_PATH, QR_PATH
from bson import ObjectId



toyyes = Blueprint('toyyes',__name__)



@toyyes.route('/open_toy',methods=['post'])
def open_toy():
    ret = request.form.to_dict()
    print(ret)
    dev_obj = MongoDB.Devices.find_one(ret)
    toy_obj = MongoDB.Toys.find_one(ret)
    if dev_obj:
        if toy_obj:
            response_dic = {'code':0,"music":"我的名字.mp3","toy_id":str(toy_obj['_id']),"name":toy_obj['toy_name']}
            return response_dic
        response_dic = {"code":2,	"music":"123.mp3"}
        return response_dic
    return {"code":1,"music":"shibai.mp3"}


@toyyes.route('/get_music/<name>')
def get_music(name):
    res = request.form.to_dict()
    print(res)
    return