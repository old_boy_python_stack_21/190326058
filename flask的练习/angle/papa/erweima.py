from uuid import uuid4
import time
import hashlib
from Config import MongoDB


lis = []
for i in range(20):
    a = uuid4()
    qr = hashlib.md5(f'{a}{time.time()}{time.time()}{a}'.encode('utf-8')).hexdigest()
    dic = {'device_key':qr}
    lis.append(dic)


ret = MongoDB.Devices.insert_many(lis)

# print(ret.insert_ids)
