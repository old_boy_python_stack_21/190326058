import os

from flask import Blueprint, request, jsonify, send_file
from Config import MongoDB, COVER_PATH, QR_PATH,CHAT_PATH,RET
from bson import ObjectId

friend_request = Blueprint('friend_request', __name__)


@friend_request.route('/ref_req',methods=['post'])
def ref_req():
    _id = request.form.get('req_id')
    MongoDB.Request.update_one({'_id':ObjectId(_id)},{'$set':{'status':2}})

    RET['CODE'] = 0
    RET['MSG'] = '拒绝添加好友'
    RET['DATA'] = {}
    return jsonify(RET)


@friend_request.route('/acc_req',methods=['post'])
def acc_req():
    _id = request.form.get('req_id')
    remark = request.form.get('remark')
    print(remark,'返回服务器昵称')

    req_obj = MongoDB.Request.find_one({'_id':ObjectId(_id)})
    #创建一个对话chat_window,写入数据库
    chat_obj = MongoDB.Chats.insert_one({'user_list':[req_obj['toy_id'],req_obj['add_user']],'chat_list':[]})
    # 将好友列表写进被请求方数据库逻辑
    toy_obj = MongoDB.Toys.find_one({'_id':ObjectId(req_obj['toy_id'])})
    dic = {}
    dic['friend_id'] = req_obj['add_user']
    dic['friend_nick'] = req_obj['nickname']
    dic['friend_remark'] = remark
    dic['friend_avatar'] = req_obj['avatar']
    dic['friend_chat'] = str(chat_obj.inserted_id)
    dic['friend_type'] = req_obj['add_type']
    MongoDB.Toys.update_one({'_id': ObjectId(req_obj['toy_id'])},{'$push':{'friend_list':dic}})


    #发送请求方  数据库写入好友
    add_obj = MongoDB.Users.find_one({'_id':ObjectId(req_obj['add_user'])})  if req_obj['add_type'] == 'app'  else MongoDB.Toys.find_one({'_id':ObjectId(req_obj['add_user'])})
    dic = {}
    dic['friend_id'] = req_obj['toy_id']
    dic['friend_nick'] = add_obj['nickname'] if req_obj['add_type'] == 'app' else add_obj['baby_name']
    dic['friend_remark'] = req_obj['remark']
    dic['friend_avatar'] = add_obj['avatar']
    dic['friend_chat'] = str(chat_obj.inserted_id)
    dic['friend_type'] = 'toy'

    MongoDB.Users.update_one({'_id': ObjectId(req_obj['add_user'])},{'$push':{'friend_list':dic}}) if req_obj['add_type'] == 'app' else MongoDB.Toys.update_one({'_id': ObjectId(req_obj['add_user'])},{'$push':{'friend_list':dic}})

    #将好友请求数据status 设置为 1
    MongoDB.Request.update_one({'_id':ObjectId(_id)},{'$set':{'status':1}})

    RET['CODE'] = 0
    RET['MSG'] = '同意添加好友'
    RET['DATA'] = {}
    return jsonify(RET)


