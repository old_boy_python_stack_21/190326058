import os
import time
from uuid import uuid4
from bson import ObjectId
from flask import Blueprint, jsonify, send_file, request
from Config import  MongoDB, COVER_PATH, MUSIC_PATH, CHAT_PATH,RET
from make_redio.makeredio import makeaudios,makeaudios1
from redis_count import set_count,get_count,get_count_all

uploader = Blueprint("uploader", __name__)

LIS = []

@uploader.route("/app_uploader", methods=["POST"])
def app_uploader():
    print('++++++++++++++++++++++++++++++')
    dic = {}

    # 接收app 传递的参数
    ret = request.form.to_dict()
    user_list = [ret.get('to_user'),ret.get('user_id')]

    res = MongoDB.Chats.find_one({'user_list':{'$all':user_list}})
    print('666')

    file = request.files.get('reco_file')
    print(file.filename)

    file_path = os.path.join(CHAT_PATH,file.filename)
    file.save(file_path)

    os.system(f'ffmpeg -i {file_path} {file_path}.mp3')

    os.remove(file_path)
    dic['from_user'] = ret.get('user_id')
    dic['to_user'] = ret.get('to_user')
    dic['chat'] = f'{file.filename}.mp3'
    dic['createTime'] = time.time()

    MongoDB.Chats.update_one({'user_list':{'$all':user_list}},{'$push':{'chat_list':dic}})
    set_count(ret.get('user_id'),ret.get('to_user'))
    # user_id == sender  to_user = receiver
    # 通过 user_list 子集查询 $all  可以获取到 当前的聊天窗口 如果查询的话,会将所有的聊天数据全部查询到,能不能不查询直接更新数据呢?
    # chat_window = MongoDB.Chats.find_one({"user_list": {"$all": user_list}})    # print(chat_window)

    # 获取文件数据 request.files
    # 保存文件 .amr
    text = '某某人'
    user_dic = MongoDB.Users.find_one({'_id':ObjectId(ret.get('user_id'))})
    for i in user_dic['friend_list']:
        if i['friend_id'] == ret.get('to_user'):
            text = i['friend_nick']
            break

    mp3 = makeaudios(text)

    RET["CODE"] = 0
    RET["MSG"] = "上传成功"
    RET["DATA"] = {'filename':mp3,'friend_type':'app'}
    print(RET)
    return jsonify(RET)


@uploader.route("/toy_uploader", methods=["POST"])
def toy_uploader():
    dic = {}
    a = uuid4()

    ret = request.form.to_dict()

    user_list = [ret.get('to_user'), ret.get('user_id')]

    res = MongoDB.Chats.find_one({'user_list': {'$all': user_list}})

    file = request.files.get('reco')

    print(file.filename,'filename')
    file_path = os.path.join(CHAT_PATH, file.filename)
    print(file_path, 'diyici')
    file.save(file_path)
    print(file.filename)
    os.system(f'ffmpeg -i {file_path} {file_path}{a}.mp3')

    os.remove(file_path)

    dic['from_user'] = ret.get('user_id')
    dic['to_user'] = ret.get('to_user')
    dic['chat'] = f'{file.filename}{a}.mp3'
    dic['createTime'] = time.time()

    MongoDB.Chats.update_one({'user_list': {'$all': user_list}}, {'$push': {'chat_list': dic}})
    print(
        '******************************************************************************************************************')

    set_count(ret.get('user_id'),ret.get('to_user'))
    print(
        '******************************************************************************************************************')
    text = '某某人'
    user_dic = MongoDB.Toys.find_one({'_id': ObjectId(ret.get('to_user'))}) if ret['friend_type'] == 'toy' else MongoDB.Users.find_one({'_id': ObjectId(ret.get('to_user'))})
    print('*'*8,user_dic)

    for i in user_dic['friend_list']:
        if i['friend_id'] == ret.get('user_id'):
            text = i['friend_remark']
            break
    print(text)
    name = makeaudios(text)
    print('---------------------------------------------',name)
    RET["CODE"] = 0
    RET["MSG"] = "上传成功"
    RET["DATA"] = {'filename': name, 'friend_type': ret.get('friend_type')}  if ret.get('friend_type') == 'toy' else {'filename':dic['chat'] , 'friend_type': ret.get('friend_type')}

    return jsonify(RET)


@uploader.route('/recv_msg',methods = ['post'])
def recv_msg():
    re_dic = request.form.to_dict()
    print(re_dic)
    sender,count = get_count_all(re_dic.get('to_user'))
    user_list = [sender, re_dic.get('to_user')]
    chat_dic = MongoDB.Chats.find_one({'user_list': {'$all': user_list}})
    text = '小伙伴'
    if sender != '小伙伴':
        print('789789789789789',sender)
        user_dic = MongoDB.Toys.find_one({'_id':ObjectId(re_dic.get('to_user'))})
        for  i in user_dic['friend_list']:
            if i['friend_id'] == sender:
                text = i['friend_remark']
    mp3 = makeaudios1(text,count)
    print(mp3)
    dic = {}
    dic['from_user'] = re_dic.get('from_user')
    dic['to_user'] = re_dic.get('to_user')
    dic['chat'] = mp3
    dic['create_time'] = time.time()
    print(count, '*'*8)
    if count != 0:
        print("_____________________________________________________________________________________________")
        print(chat_dic['chat_list'])
        print(chat_dic['user_list'])
        response_lis = chat_dic['chat_list'][-count:]
        response_lis.insert(0,dic)
    else:
        response_lis = []
    response_lis.reverse()
    print(response_lis,'--------------------------------------')

    return jsonify(response_lis)