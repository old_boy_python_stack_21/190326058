import json

from flask import Flask,request

from geventwebsocket.server import WSGIServer
from geventwebsocket.handler import WebSocketHandler
from geventwebsocket.websocket import WebSocket


ws_app = Flask(__name__)

user_socket_dict = {}

@ws_app.route('/ws/<user_id>')
def ws(user_id):
    user_socket = request.environ.get('wsgi.websocket')
    if user_socket:
        user_socket_dict[user_id] = user_socket
    print(user_socket_dict)

    while True:
        msg = user_socket.receive()
        msg_dict = json.loads(msg)
        print(msg)
        receiver = msg_dict.get('receiver')
        receiver_socket = user_socket_dict.get(receiver)
        receiver_socket.send(msg)


if __name__ == '__main__':
    http_serv = WSGIServer(('0.0.0.0',9997),ws_app,handler_class=WebSocketHandler)
    http_serv.serve_forever()