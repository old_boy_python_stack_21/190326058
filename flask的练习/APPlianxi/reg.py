import os
AVATAR = 'static'
from flask import Flask, request, jsonify, send_file
from pymongo import MongoClient

MC = MongoClient('127.0.0.1', 27017)
MongoDB = MC['day94']

app = Flask(__name__)


@app.route('/reg', methods=['POST'])
def reg():
    res = request.form.to_dict()
    print(res)
    ret = MongoDB.user.find({'username': res['username']})
    if list(ret):
        return jsonify({'code': 999, 'error': '用户名已存在'})
    MongoDB.user.insert_one(res)
    return jsonify({'code': 0})


@app.route('/send_file', methods=['POST'])
def sen_file():
    file = request.files['my_img']
    file.save(file.filename)
    file_path = f'http://192.168.12.103:9998/get_file/{file.filename}'
    print(file_path)
    return file_path


@app.route('/get_file/<filename>')
def get_file(filename):
    print(filename)
    return send_file(filename)


@app.route('/login', methods=['POST'])
def login():
    dic = request.form.to_dict()
    ret = MongoDB.user.find(dic)
    if list(ret):
        return jsonify({'code': 0})
    return jsonify({'code': 999})


@app.route('/new_login', methods=['POST'])
def new_login():
    dic = request.form.to_dict()
    ret = MongoDB.user.find(dic)
    for i in ret:
        res = i
    img = res.get('img', '1')
    print(img,'666')
    return img

@app.route('/reco_upload',methods=['POST'])
def reco_uploder():
    reco = request.files.get('reco')
    print(reco.filename)
    reco_path = os.path.join(AVATAR,reco.filename)
    reco.save(reco_path)
    os.system(f'ffmpeg -i {reco_path} {reco_path}.mp3')
    ret = {
        'code': 0,
        'filename':reco.filename+'.mp3'
    }

    return ret
# @fm.route('/upload', methods=['post'])
#
# def upload():
#     avatar = request.files.get('avatar')
#     avatar_path = os.path.join(AVATAR, avatar.filename)
#     avatar.save(avatar_path)
#     ret = {
#         'code': 0,
#         'filename': avatar.filename,
#     }
#     # print(avatar_path)
#     return ret


@app.route('/get_chat/<filename>')
def get_chat(filename):
    file_path = os.path.join(AVATAR,filename)
    return send_file(file_path)

if __name__ == '__main__':
    app.run('192.168.12.103', 9998)
